<!-- 

O título da merge request deve resumir as mudanças.

 -->


<!--

Se esta merge request (MR) resolver algum issue, referencie o issue em questão. Por exemplo, se a MR resolver o issue #Y, adicione a seguinte linha à descrição da MR:

Closes #Y

Dessa forma, o issue será resolvido automaticamente após a MR ser efetivada.

-->


<!-- 

Descreva as mudanças, explique a motivação delas se necessário e cite possíveis desvantagens que podem aparecer com as novas mudanças;

 -->


<!--

Inicialmente, quando a MR estiver incompleta, inicie seu título com o prefixo 'Draft: ' ou 'WIP: '.

Quando a MR estiver pronta para ser revisada, remova o prefixo do título, adicione a label 'Pronto para revisão' e adicione pelo menos um membro para revisar a merge request.

-->


