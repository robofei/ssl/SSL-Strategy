import os
import numpy
from matplotlib import pyplot

pyplot.switch_backend('Qt5Agg')
pyplot.style.use('dark_background')

class KickModel:
    def __init__(self, strength):
        self.strength = strength
        self.vel_max = 0
        self.vel_corte = 0
        self.tempo_corte = 0
        self.tempo_max = 0
        self.dist_max = 0
        self.dist_coef = 0

def read_kick_model_file(file_path):
    models = dict()
    with open(file_path, 'r') as f:
        while True:
            f.readline()
            strength = f.readline()
            
            if strength is None or strength == '':
                break
            
            f.readline()
            
            km = KickModel(float(strength))
            
            km.vel_max = float(f.readline())
            km.vel_corte = float(f.readline())
            
            km.tempo_corte = float(f.readline())
            km.tempo_max = float(f.readline())
            
            km.dist_max = float(f.readline())
            km.dist_coef = float(f.readline())
            
            models[km.strength] = km
    
    return models

if __name__ == "__main__":
    
    modelos = {
        'Campo Real': 'modelo_campo_real.txt',
        'Simulação ER-Force': 'modelo_simulacao_erforce.txt',
        'Simulação grSim': 'modelo_simulacao_grsim.txt'
    }
    
    modelo_analise = 'Simulação grSim'
    file_name = modelos[modelo_analise]
    
    file_path = f'{os.path.dirname(os.path.realpath(__file__))}/{file_name}'
    models = read_kick_model_file(file_path)
    
    # Tempo para bola parar no chute mais forte
    t_max = max(a.tempo_max for a in models.values())

    # Vetor tempo discreto    
    t = numpy.linspace(0, t_max, 1000)
    
    # Análise Tempo versus Velocidade para várias forças
    for m in models.values():
        v = numpy.zeros(t.shape)
        for i, to in enumerate(t):
            if to < m.tempo_corte:
                v[i] = m.vel_max - to*(m.vel_max - m.vel_corte)/m.tempo_corte
            else:
                v[i] = m.vel_corte*m.tempo_max/(m.tempo_max-m.tempo_corte) -to*m.vel_corte/(m.tempo_max - m.tempo_corte)
            
            if v[i] < 0:
                v[i] = 0
        pyplot.plot(t, v, label=f'Força {m.strength}')
    
    pyplot.ylabel('Velocidade (m/s)')
    pyplot.xlabel('Tempo (s)')
    pyplot.legend()
    pyplot.show()
    
    # Análise Tempo versus Distância para várias forças
    for m in models.values():
        d = numpy.zeros(t.shape)
        for i, to in enumerate(t):
            d[i] = m.dist_max*(1 - numpy.exp(-m.dist_coef*to))
        pyplot.plot(t, d, label=f'Força {m.strength}')
            
    pyplot.ylabel('Distância (m)')
    pyplot.xlabel('Tempo (s)')
    pyplot.legend()
    pyplot.show()
    
    # Análise Distância versus Velocidade para várias forças
    for m in models.values():
        v = numpy.zeros(t.shape)
        d = numpy.zeros(t.shape)
        for i, to in enumerate(t):
            if to < m.tempo_corte:
                v[i] = m.vel_max - to*(m.vel_max - m.vel_corte)/m.tempo_corte
            else:
                v[i] = m.vel_corte*m.tempo_max/(m.tempo_max-m.tempo_corte) -to*m.vel_corte/(m.tempo_max - m.tempo_corte)
            
            if v[i] < 0:
                v[i] = 0
                
            d[i] = m.dist_max*(1 - numpy.exp(-m.dist_coef*to))

        pyplot.plot(d, v, label=f'Força {m.strength}')

    pyplot.ylabel('Velocidade (m/s)')
    pyplot.xlabel('Distância (m)')
    pyplot.legend()
    pyplot.show()
