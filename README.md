[**Leia-me em português**](https://gitlab.com/robofei/ssl/SSL-Strategy/-/blob/master/README_pt-br.md)

[**For docker usage, click here**](https://gitlab.com/robofei/ssl/SSL-Strategy/-/tree/master/docker)

# Strategy Software - RoboFEI SSL

![](./Media/Screenshots/Screenshot0.png)

[**Click here to access the documentation of the source code**](https://robofei.gitlab.io/ssl/SSL-Strategy)

# Related Repositories

Some features and tools related to this software are available in separate
separate repositories.

So far we have:

1. [Robot position PID controller](https://gitlab.com/robofei/ssl/ssl_pid_positioncontroller)
2. [Software for data acquisition and generation of robot motion models](https://gitlab.com/leo_costa/systemidentificationsoftware)
3. [Passive league match analyzer + log player](https://gitlab.com/robofei/ssl/LogAnalyserRoboFei-SSL)

# Installing dependencies

## Installing protobuf compiler

First, check if the protobuf compiler is already installed using the
following command:

```sh
which protoc
```

If you get the message `protoc not found', you can install it
with the command bellow:

```sh
sudo apt install protobuf-compiler # on ubuntu/debian
sudo pacman -S protobuf # in arch-linux/manjaro
```

------------

## Installing eigen3 library

```sh
sudo apt install libeigen3-dev ## on ubuntu/debian
sudo pacman -S eigen # in arch-linux/manjaro
```

------------

## Installing dlib and dependencies library

```sh
sudo apt install libdlib-dev libopenblas-dev liblapack-dev ## on ubuntu/debian
```

On Arch based systems, install the [dlib](https://aur.archlinux.org/packages/dlib) package on the AUR.

------------

## Installing OSQP

```sh
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh

~/miniconda3/bin/conda init bash
conda install -c conda-forge osqp-eigen
```


------------

## Installing Qt

You can install the latest version of qt from the [Qt mirror
Qt](http://download.qt.io/official_releases/) or using your package
manager. You must have a Qt version of at least `5.12.8`, so
installing it using the Qt installer is generally recommended.
However, for rolling release Distros (like Arch Linux based systems), this is no problem as the
packages are always very up-to-date. On Ubuntu 20.XX based systems,
the qt version should also be sufficient. If this is the case, install
Qt and the necessary Qt modules with the following command:
```sh
sudo pacman -S qt5-base qt5-multimedia qt5-serialport # arch-linux/manjaro
sudo apt install qt5-default libqt5network5 libqt5serialport5-dev qtmultimedia5-dev #ubuntu
```

To install Qt from the Qt Installer on Linux distributions, run the
commands below and install your preferred version of Qt.

```sh
wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
chmod +x qt-unified-linux-x64-online.run
./qt-unified-linux-x64-online.run
```

# Building

First install the necessary tools:

```sh
sudo apt install build-essential # ubuntu/debian
sudo pacman -S base-devel # arch-linux/manjaro
```

Find the qmake executable with the command:

```sh
locate bin/qmake
```

You must find a version inside the directory where qt is installed,
something like `/path_to_qt_dir/Qt/5.XX.X/gcc_64/bin/qmake`, or `/usr/bin/qmake`
if you have installed it from your package manager.

Create the `build` directory and enter into it:

```sh
mkdir build && cd build
```

Create the Makefile using qmake:

```sh
/path_to_qt_dir/Qt/5.XX.X/gcc_64/bin/qmake CONFIG+=release ..
```

And finally, run `make -jN` to finish. Replace the `N` parameter
in `-jN` for the number of parallel jobs you wish to use during the build process.

If everything checks out, the `RoboFeiSSL` binary will be generated. To
run the application, type:

```sh
./RoboFeiSSL
```
