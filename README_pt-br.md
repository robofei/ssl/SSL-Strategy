[**Para informações sobre a utilização do docker, clique aqui**](https://gitlab.com/robofei/ssl/SSL-Strategy/-/tree/master/docker)

# Software de Estratégia - RoboFEI SSL

![](./Media/Screenshots/Screenshot0.png)

[**Clique aqui para acessar a documentação do
código**](https://robofei.gitlab.io/ssl/SSL-Strategy)

# Repositórios relacionados

Algumas funcionalidades do software, ou ferramentas, estão disponíveis em
repositórios separados.

Até o momento temos:

1. [Controlador PID de posição dos robôs](https://gitlab.com/robofei/ssl/ssl_pid_positioncontroller)
2. [Software para aquisição de dados e geração dos modelos de movimentação do robô](https://gitlab.com/leo_costa/systemidentificationsoftware)
3. [Analisador passivo de partidas da liga + log player](https://gitlab.com/robofei/ssl/LogAnalyserRoboFei-SSL)

# Instalando dependências

## Instalando protobuf compiler

Primeiramente, verifique se o protobuf compiler já está instalado através do
seguinte comando:

```sh
which protoc
```

Se você receber a seguinte mensagem: `protoc not found`, é possível instalá-lo
a partir do seguinte comando:

```sh
sudo apt install protobuf-compiler # no ubuntu/debian
sudo pacman -S protobuf # no arch-linux/manjaro
```

------------

## Instalando eigen3 library

```sh
sudo apt install libeigen3-dev # no ubuntu/debian
sudo pacman -S eigen # no arch-linux/manjaro
```

------------

## Instalando dlib e dependências

```sh
sudo apt install libdlib-dev libopenblas-dev liblapack-dev ## no ubuntu/debian
```

Em sistemas baseados no Arch Linux, instale o pacote [dlib](https://aur.archlinux.org/packages/dlib) no AUR.

------------

## Setando o python environment

Antes de tudo é necessário instalar a versão de python utilizada. Foram testadas
as versões `3.7`, `3,8` e `3.9`; sendo mais recomendado as versões 3.8 e 3.9.
Nos próximos passos, substitua `3.X` pela versão que pretende utilizar.

Pode-se verificar a versão instalda com o comando `python --version`

Caso python não esteja instalado, instale o pacote com um dos comandos abaixo:

```sh
sudo apt install python3.8-dev # no ubuntu/debian
sudo pacman -S python # no arch-linux/manjaro
```

Agora deve ser possível executar scripts usando python. Para teste, no
terminal, digite:

```sh
python3.X
```

E execute os seguintes comandos:

```python
>>> import sys
>>> print("Python version: {}".format(sys.version))
```

O resultado deve ser similar ao mostrado abaixo:

```
Python version: 3.8.0 (default, Oct 28 2019, 16:14:01)
[GCC 8.3.0]
```

Instale as útimas dependências:

```sh
sudo apt install python3-pip python3-tk # no ubuntu/debian
sudo pacman -S python-pip tk # no arch-linux/manjaro
```

Usando pip, instale todos os módulos necessários com o seguinte comando:

```sh
python3.X -m pip install numpy Pillow sklearn twine wheel setuptools console_progressbar
```

Agora deve ser possível ver estes módulos e suas respectivas versões com o comando:

```sh
python3.X -m pip list
```

Lembre-se de setar corretamente o valor de `PYTHON_VERSION` na etapa do qmake mais para frente. Por exemplo, se você estivre usando python3.9, você usará o comando `qmake CONFIG+=release PYTHON_VERSION=3.9 ..`.

## Instalando o módulo em python utilizado no código

Do diretório atual, vá ao diretório `src/python/` :

```sh
cd src/python
```

Execute o comando:

```sh
python3.X setup.py check
python3.X create_models.py
```

E se nenhum erro ocorrer, digite:

```sh
python3.X -m pip install -e .
```

Agora o módulo `robofeissl` está instalado no diretório atual e qualquer mudança
nos arquivos daqui afetarão o comportamento do módulo. Note que caso o
repositório mude de local, será necessário desintalar o pacote e realizar os
últimos dois passos novamente no novo diretório. Para desintalar este pacote,
basta executar o comando:

```sh
python3.X -m pip uninstall robofeissl
```

------------

## Instalando pybind11

```sh
sudo pacman -S pybind11 # arch-linux/manjaro
sudo apt install pybind11-dev # ubuntu
```

Nota: A versão 2.7.0 de pybind11 não funciona como esperado no software. Para evitar problemas, por favor certifique-se de estar utilizando uma versão anterior (<=2.6.2).

------------

## Instalando Qt

É possível instalar a versão mais atualizada do qt no [mirror do
Qt](http://download.qt.io/official_releases/) ou utilizando seu package
manager. É necessário que a versão do Qt seja pelo menos `5.12.0`, por isso
geralmente é mais recomendada a instalação usando o Qt installer.
Entretanto, para sistemas Arch Linux based, isso não problema pois os
pacotes são sempre bastante atualizados. Em sistemas baseados em Ubuntu 20.XX
a versão do qt também deve ser suficiente. Caso esse seja seu caso, instale
o Qt e os módulos necessários com o seguinte comando:
```sh
sudo pacman -S qt5-base qt5-multimedia qt5-serialport # arch-linux/manjaro
sudo apt install qt5-default libqt5network5 libqt5serialport5-dev qtmultimedia5-dev #ubuntu
```

Para instalar o Qt pelo Qt Installer em distribuições Linux, execute os
comandos abaixo e instale a versão de preferência do Qt.

```sh
wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
chmod +x qt-unified-linux-x64-online.run
./qt-unified-linux-x64-online.run
```

# Building

Primeiro instale as ferramentas necessárias:

```sh
sudo apt install build-essential # ubuntu/debian
sudo pacman -S base-devel # arch-linux/manjaro
```

Ache o executável qmake com o comando:

```sh
locate bin/qmake
```

Você deve encontrar uma versão dentro do diretório onde o qt está instalado,
algo como `/path_to_qt_dir/Qt/5.XX.X/gcc_64/bin/qmake`, ou `/usr/bin/qmake`
caso você tenha instalado pelo seu package manager.

Crie o diretório `build` e entre nele:

```sh
mkdir build && cd build
```

Crie a Makefile usando qmake:

```sh
/path_to_qt_dir/Qt/5.XX.X/gcc_64/bin/qmake CONFIG+=release ..
```

E, por último, execute `make -jN` para finalizar. Substitua o parâmetro `N`
em `-jN` pelo número de processos paralelos a serem utilizados durante a compilação.

Se tudo ocorrer dentro dos conformes, será gerado um binário `RoboFeiSSL`. Para
executar a aplicação digite:

```sh
./RoboFeiSSL
```
