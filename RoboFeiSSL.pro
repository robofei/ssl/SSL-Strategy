#-------------------------------------------------
#
# Project created by QtCreator 2018-04-14T12:26:59
#
#-------------------------------------------------

# Evita warning de visibilidade com o pybind11.
# Veja: https://pybind11.readthedocs.io/en/stable/
QMAKE_CXXFLAGS += -fvisibility=hidden

QT += \
    core \
    gui \
    network \
    serialport \
    widgets \
    multimedia

CONFIG += c++14

# !defined(PYTHON_VERSION, var) {
#     PYTHON_VERSION = 3.8
# }

message(" ")
message(" ")
message(********************************************************)
message(Qt version: $$[QT_VERSION])
message(Qt is installed in $$[QT_INSTALL_PREFIX])
message(Qt resources can be found in the following locations:)
message(Documentation: $$[QT_INSTALL_DOCS])
message(Header files: $$[QT_INSTALL_HEADERS])
message(Libraries: $$[QT_INSTALL_LIBS])
message(Binary files (executables): $$[QT_INSTALL_BINS])
message(Plugins: $$[QT_INSTALL_PLUGINS])
message(Data files: $$[QT_INSTALL_DATA])
message(Translation files: $$[QT_INSTALL_TRANSLATIONS])
message(Settings: $$[QT_INSTALL_CONFIGURATION])
message(Examples: $$[QT_INSTALL_EXAMPLES])
message(Home: $$(HOME))
# message(Using python version $${PYTHON_VERSION})
message(********************************************************)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = RoboFeiSSL
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# Isso aqui era para resolver o problema com o pybind11, mas não deu certo...
# Definindo o NDEBUG ele não gerava o erro, mas não saía do lugar também.
#DEFINES += NDEBUG

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(include/protofiles/proto_compile.pri)
include(include/AdditionalInclude.pri)

HEADERS += \
    src/Ambiente/fieldzone.h \
    src/Ambiente/futbolenvironment.h \
    src/Ambiente/geometria.h \
    src/Ambiente/sslteam.h \
    src/Bola/bola.h \
    src/Config/globalconfig.h \
    src/Config/jsondata.h \
    src/Config/pidconfig.h \
    src/Config/playdeflectconfig.h \
    src/Config/robofeiconfig.h \
    src/Bola/visionball.h \
    src/Constantes_e_Funcoes_Auxiliares/auxiliar.h \
    src/Constantes_e_Funcoes_Auxiliares/constantes.h \
    src/Constantes_e_Funcoes_Auxiliares/hungarianalgorithm.h \
    src/Constantes_e_Funcoes_Auxiliares/macros.h \
#     src/Decisoes/decisoes.h \
    src/Debug/debugbestpositionattack.h \
    src/Debug/debugdata.h \
    src/Debug/debugdeflectplay.h \
    src/Debug/debuginfo.h \
    src/Debug/debugshootdecision.h \
    src/Debug/debugtypes.h \
    src/Estrategia/Coach/availablecoaches.h \
    src/Estrategia/Coach/coach.h \
    src/Estrategia/Coach/coachreferee.h \
    src/Estrategia/Coach/coachtesting.h \
    src/Estrategia/Plays/NormalPlays/playadvanceball.h \
    src/Estrategia/Plays/NormalPlays/playgetballaway.h \
    src/Estrategia/Plays/NormalPlays/playindividualdefense.h \
    src/Estrategia/Plays/NormalPlays/playpressuredefense.h \
    src/Estrategia/Plays/NormalPlays/playshoottogoal.h \
    src/Estrategia/Plays/NormalPlays/playusingpassattack.h \
    src/Estrategia/Plays/availableplays.h \
    src/Estrategia/Plays/play.h \
    src/Estrategia/Plays/RefereePlays/BallPlacement/playballplacementopp.h \
    src/Estrategia/Plays/RefereePlays/BallPlacement/playballplacement.h \
    src/Estrategia/Plays/RefereePlays/Freekick/playbasicfreekick.h \
    src/Estrategia/Plays/NormalPlays/playdeflectkick.h \
    src/Estrategia/Plays/RefereePlays/Freekick/playfreekickopp.h \
    src/Estrategia/Plays/RefereePlays/Halt/playhalt.h \
    src/Estrategia/Plays/RefereePlays/Kickoff/playkickoffally.h \
    src/Estrategia/Plays/RefereePlays/Kickoff/playkickoffallytake.h \
    src/Estrategia/Plays/RefereePlays/Kickoff/playkickoffopp.h \
    src/Estrategia/Plays/NormalPlays/playnormalfulldefense.h \
    src/Estrategia/Plays/RefereePlays/Penalty/playpenaltydefense.h \
    src/Estrategia/Plays/RefereePlays/Penalty/playpenaltydirectattack.h \
    src/Estrategia/Plays/RefereePlays/Stop/playstopdefensivo.h \
    src/Estrategia/Plays/RefereePlays/Stop/playstop.h \
    src/Estrategia/Plays/TestPlays/playtesting.h \
    src/Estrategia/Plays/TestPlays/playtesting2.h \
    src/Estrategia/Plays/RefereePlays/Timeout/playtimeout.h \
    src/Estrategia/Roles/AttackRoles/roleattackers.h \
    src/Estrategia/Roles/AttackRoles/rolefinisher.h \
    src/Estrategia/Roles/AttackRoles/rolereceiver.h \
    src/Estrategia/Roles/DefenseRoles/roleindividualdefender.h \
    src/Estrategia/Roles/DefenseRoles/rolepressuredefender.h \
    src/Estrategia/Roles/PassiveRoles/roleballplacersupport.h \
    src/Estrategia/Roles/DefenseRoles/rolenightygoalie.h \
    src/Estrategia/Roles/availableroles.h \
    src/Estrategia/Roles/role.h \
    src/Estrategia/Roles/AttackRoles/roleattacker.h \
    src/Estrategia/Roles/DefenseRoles/roleballchaserfreekick.h \
    src/Estrategia/Roles/DefenseRoles/roleballchaserstop.h \
    src/Estrategia/Roles/AttackRoles/rolebasicfreekickattacker.h \
    src/Estrategia/Roles/AttackRoles/rolebasicfreekicksupport.h \
    src/Estrategia/Roles/AttackRoles/roledeflecter.h \
    src/Estrategia/Roles/DefenseRoles/roledeltadefender.h \
    src/Estrategia/Roles/AttackRoles/rolefreekickshooter.h \
    src/Estrategia/Roles/DefenseRoles/rolegoalie.h \
    src/Estrategia/Roles/PassiveRoles/rolehalt.h \
    src/Estrategia/Roles/AttackRoles/rolekickoffreceiver.h \
    src/Estrategia/Roles/AttackRoles/rolenormalattacker.h \
    src/Estrategia/Roles/AttackRoles/rolenormalballthief.h \
    src/Estrategia/Roles/AttackRoles/rolepenaltyattacker.h \
    src/Estrategia/Roles/DefenseRoles/rolepenaltycone.h \
    src/Estrategia/Roles/AttackRoles/rolesupportattacker.h \
    src/Estrategia/Roles/AttackRoles/rolesupportdeflecter.h \
    src/Estrategia/Roles/PassiveRoles/roletesting.h \
    src/Estrategia/Roles/PassiveRoles/roleballplacer.h \
    src/Estrategia/Skills/availableskills.h \
    src/Estrategia/Skills/skill.h \
    src/Estrategia/Skills/skillaim.h \
    src/Estrategia/Skills/skillgetoppball.h \
    src/Estrategia/Skills/skillgetstaticball.h \
    src/Estrategia/Skills/skillgoto.h \
    src/Estrategia/Skills/skillgotozone.h \
    src/Estrategia/Skills/skilllookto.h \
    src/Estrategia/Skills/skillmovetopose.h \
    src/Estrategia/Skills/skillpullball.h \
    src/Estrategia/Skills/skillshoot.h \
    src/Estrategia/Skills/skillwaitball.h \
    src/Estrategia/Tactics/availabletactics.h \
    src/Estrategia/Tactics/tactic.h \
    src/Estrategia/Tactics/tacticdeflectkick.h \
    src/Estrategia/Tactics/tacticgotolookto.h \
    src/Estrategia/Tactics/tacticmovetopose.h \
    src/Estrategia/Tactics/tacticnormalshoottotarget.h \
    src/Estrategia/Tactics/tacticreceivepass.h \
    src/Estrategia/Tactics/tacticreceivepassalt.h \
    src/Estrategia/Tactics/tacticshoottotarget.h \
    src/Estrategia/Tactics/tacticstealball.h \
    src/Estrategia/Tactics/tacticgetball.h \
    src/Estrategia/estrategia.h \
    src/Estrategia/jogadasensaiadas.h \
    src/Goleiro/goleiro.h \
    src/Interface/decisoestesteinterface.h \
    src/Interface/joystickrobotcontrollerinterface.h \
    src/Interface/indicadorstatusrobo.h \
    src/Interface/movimentacaoclick.h \
    src/Interface/opcoesinterface.h \
    src/Interface/pathplanners.h \
    src/Interface/drawmap.h \
    src/Mapa/visibilitygraph.h \
    src/Movimentacao/Modelos/arxmodel.h \
    src/Movimentacao/motionfeedbackpacket.h \
    src/Movimentacao/motionpathfeedback.h \
    src/Movimentacao/pathtracking.h \
    src/Movimentacao/robotmovement.h \
    src/Movimentacao/velocidade.h \
    src/ObstacleAvoidance/prsbc.h \
    src/Path_Planning/a_star_vg.h \
    src/Path_Planning/path.h \
    src/Path_Planning/path_p_rrt_2.h \
    src/Path_Planning/pathplanninggeneric.h \
    src/Path_Planning/priorityqueue.h \
    src/Path_Planning/cell.h \
    src/Path_Planning/grid.h \
    src/Path_Planning/mrastar.h \
    src/Path_Planning/mrapath.h \
    src/Posicionamento/Attack/passpositionoptimizer.h \
    src/Posicionamento/Delta/deltadefense.h \
    src/Posicionamento/Delta/individualdefense.h \
    src/Posicionamento/Delta/pressuredefense.h \
    src/Posicionamento/Marking/marking.h \
    src/Posicionamento/Marking/normalattack.h \
    src/Posicionamento/Mines/mines.h \
    src/Posicionamento/positioning.h \
    src/Radio/radiobase.h \
    src/Radio/radiofeedback.h \
    src/Radio/robotcommands.h \
    src/Rede/logger.h \
    src/Referee/referee.h \
    src/Referee/refereepacket.h \
    src/Referee/refereereceiver.h \
    src/Referee/refereeteamdata.h \
    src/Robo/atributos.h \
    src/Robo/robot.h \
    src/Robo/visionrobot.h \
    src/Simulador/controlesimulacao.h \
    src/Simulador/simulator.h \
    src/SimuladorRefbox/simuladorrefbox.h \
    src/Testes_Robos/testesrobos.h \
    src/Visao/Kalman_Filter/kalmanfilter.h \
    src/Visao/visao.h \
    src/Visao/visionpacket.h \
    src/cli/robofei_cli.h \
    src/robofeissl.h

SOURCES += \
    src/Ambiente/fieldzone.cpp \
    src/Ambiente/futbolenvironment.cpp \
    src/Ambiente/geometria.cpp \
    src/Ambiente/sslteam.cpp \
    src/Bola/bola.cpp \
    src/Config/globalconfig.cpp \
    src/Config/jsondata.cpp \
    src/Config/pidconfig.cpp \
    src/Config/playdeflectconfig.cpp \
    src/Config/robofeiconfig.cpp \
    src/Bola/visionball.cpp \
    src/Constantes_e_Funcoes_Auxiliares/auxiliar.cpp \
    src/Constantes_e_Funcoes_Auxiliares/constantes.cpp \
    src/Constantes_e_Funcoes_Auxiliares/hungarianalgorithm.cpp \
#     src/Decisoes/decisoes.cpp \
    src/Debug/debugbestpositionattack.cpp \
    src/Debug/debugdata.cpp \
    src/Debug/debugdeflectplay.cpp \
    src/Debug/debuginfo.cpp \
    src/Debug/debugshootdecision.cpp \
    src/Estrategia/Coach/coach.cpp \
    src/Estrategia/Coach/coachreferee.cpp \
    src/Estrategia/Coach/coachtesting.cpp \
    src/Estrategia/Plays/NormalPlays/playadvanceball.cpp \
    src/Estrategia/Plays/NormalPlays/playgetballaway.cpp \
    src/Estrategia/Plays/NormalPlays/playindividualdefense.cpp \
    src/Estrategia/Plays/NormalPlays/playpressuredefense.cpp \
    src/Estrategia/Plays/NormalPlays/playshoottogoal.cpp \
    src/Estrategia/Plays/NormalPlays/playusingpassattack.cpp \
    src/Estrategia/Plays/play.cpp \
    src/Estrategia/Plays/RefereePlays/BallPlacement/playballplacementopp.cpp \
    src/Estrategia/Plays/RefereePlays/BallPlacement/playballplacement.cpp \
    src/Estrategia/Plays/RefereePlays/Freekick/playbasicfreekick.cpp \
    src/Estrategia/Plays/NormalPlays/playdeflectkick.cpp \
    src/Estrategia/Plays/RefereePlays/Freekick/playfreekickopp.cpp \
    src/Estrategia/Plays/RefereePlays/Halt/playhalt.cpp \
    src/Estrategia/Plays/RefereePlays/Kickoff/playkickoffally.cpp \
    src/Estrategia/Plays/RefereePlays/Kickoff/playkickoffallytake.cpp \
    src/Estrategia/Plays/RefereePlays/Kickoff/playkickoffopp.cpp \
    src/Estrategia/Plays/NormalPlays/playnormalfulldefense.cpp \
    src/Estrategia/Plays/RefereePlays/Penalty/playpenaltydefense.cpp \
    src/Estrategia/Plays/RefereePlays/Penalty/playpenaltydirectattack.cpp \
    src/Estrategia/Plays/RefereePlays/Stop/playstopdefensivo.cpp \
    src/Estrategia/Plays/RefereePlays/Stop/playstop.cpp \
    src/Estrategia/Plays/TestPlays/playtesting.cpp \
    src/Estrategia/Plays/TestPlays/playtesting2.cpp \
    src/Estrategia/Plays/RefereePlays/Timeout/playtimeout.cpp \
    src/Estrategia/Roles/AttackRoles/roleattackers.cpp \
    src/Estrategia/Roles/AttackRoles/rolefinisher.cpp \
    src/Estrategia/Roles/AttackRoles/rolereceiver.cpp \
    src/Estrategia/Roles/DefenseRoles/roleindividualdefender.cpp \
    src/Estrategia/Roles/DefenseRoles/rolepressuredefender.cpp \
    src/Estrategia/Roles/PassiveRoles/roleballplacersupport.cpp \
    src/Estrategia/Roles/DefenseRoles/rolenightygoalie.cpp \
    src/Estrategia/Roles/role.cpp \
    src/Estrategia/Roles/AttackRoles/roleattacker.cpp \
    src/Estrategia/Roles/DefenseRoles/roleballchaserfreekick.cpp \
    src/Estrategia/Roles/DefenseRoles/roleballchaserstop.cpp \
    src/Estrategia/Roles/AttackRoles/rolebasicfreekickattacker.cpp \
    src/Estrategia/Roles/AttackRoles/rolebasicfreekicksupport.cpp \
    src/Estrategia/Roles/AttackRoles/roledeflecter.cpp \
    src/Estrategia/Roles/DefenseRoles/roledeltadefender.cpp \
    src/Estrategia/Roles/AttackRoles/rolefreekickshooter.cpp \
    src/Estrategia/Roles/DefenseRoles/rolegoalie.cpp \
    src/Estrategia/Roles/PassiveRoles/rolehalt.cpp \
    src/Estrategia/Roles/AttackRoles/rolekickoffreceiver.cpp \
    src/Estrategia/Roles/AttackRoles/rolenormalattacker.cpp \
    src/Estrategia/Roles/AttackRoles/rolenormalballthief.cpp \
    src/Estrategia/Roles/AttackRoles/rolepenaltyattacker.cpp \
    src/Estrategia/Roles/DefenseRoles/rolepenaltycone.cpp \
    src/Estrategia/Roles/AttackRoles/rolesupportattacker.cpp \
    src/Estrategia/Roles/AttackRoles/rolesupportdeflecter.cpp \
    src/Estrategia/Roles/PassiveRoles/roletesting.cpp \
    src/Estrategia/Roles/PassiveRoles/roleballplacer.cpp \
    src/Estrategia/Skills/skill.cpp \
    src/Estrategia/Skills/skillaim.cpp \
    src/Estrategia/Skills/skillgetoppball.cpp \
    src/Estrategia/Skills/skillgetstaticball.cpp \
    src/Estrategia/Skills/skillgoto.cpp \
    src/Estrategia/Skills/skillgotozone.cpp \
    src/Estrategia/Skills/skilllookto.cpp \
    src/Estrategia/Skills/skillmovetopose.cpp \
    src/Estrategia/Skills/skillpullball.cpp \
    src/Estrategia/Skills/skillshoot.cpp \
    src/Estrategia/Skills/skillwaitball.cpp \
    src/Estrategia/Tactics/tactic.cpp \
    src/Estrategia/Tactics/tacticdeflectkick.cpp \
    src/Estrategia/Tactics/tacticgotolookto.cpp \
    src/Estrategia/Tactics/tacticmovetopose.cpp \
    src/Estrategia/Tactics/tacticnormalshoottotarget.cpp \
    src/Estrategia/Tactics/tacticreceivepass.cpp \
    src/Estrategia/Tactics/tacticreceivepassalt.cpp \
    src/Estrategia/Tactics/tacticshoottotarget.cpp \
    src/Estrategia/Tactics/tacticstealball.cpp \
    src/Estrategia/Tactics/tacticgetball.cpp \
    src/Estrategia/estrategia.cpp \
    src/Estrategia/jogadasensaiadas.cpp \
    src/Goleiro/goleiro.cpp \
    src/Interface/decisoestesteinterface.cpp \
    src/Interface/joystickrobotcontrollerinterface.cpp \
    src/Interface/indicadorstatusrobo.cpp \
    src/Interface/movimentacaoclick.cpp \
    src/Interface/opcoesinterface.cpp \
    src/Interface/pathplanners.cpp \
    src/Interface/drawmap.cpp \
    src/Mapa/visibilitygraph.cpp \
    src/Movimentacao/Modelos/arxmodel.cpp \
    src/Movimentacao/motionfeedbackpacket.cpp \
    src/Movimentacao/motionpathfeedback.cpp \
    src/Movimentacao/pathtracking.cpp \
    src/Movimentacao/robotmovement.cpp \
    src/Movimentacao/velocidade.cpp \
    src/ObstacleAvoidance/prsbc.cpp \
    src/Path_Planning/a_star_vg.cpp \
    src/Path_Planning/path.cpp \
    src/Path_Planning/path_p_rrt_2.cpp \
    src/Path_Planning/pathplanninggeneric.cpp \
    src/Path_Planning/priorityqueue.cpp \
    src/Path_Planning/cell.cpp \
    src/Path_Planning/grid.cpp \
    src/Path_Planning/mrastar.cpp \
    src/Path_Planning/mrapath.cpp \
    src/Posicionamento/Attack/passpositionoptimizer.cpp \
    src/Posicionamento/Delta/deltadefense.cpp \
    src/Posicionamento/Delta/individualdefense.cpp \
    src/Posicionamento/Delta/pressuredefense.cpp \
    src/Posicionamento/Marking/marking.cpp \
    src/Posicionamento/Marking/normalattack.cpp \
    src/Posicionamento/Mines/mines.cpp \
    src/Posicionamento/positioning.cpp \
    src/Radio/radiobase.cpp \
    src/Radio/radiofeedback.cpp \
    src/Radio/robotcommands.cpp \
    src/Rede/logger.cpp \
    src/Referee/referee.cpp \
    src/Referee/refereepacket.cpp \
    src/Referee/refereereceiver.cpp \
    src/Referee/refereeteamdata.cpp \
    src/Robo/robot.cpp \
    src/Robo/visionrobot.cpp \
    src/Simulador/controlesimulacao.cpp \
    src/Simulador/simulator.cpp \
    src/SimuladorRefbox/simuladorrefbox.cpp \
    src/Testes_Robos/testesrobos.cpp \
    src/Visao/Kalman_Filter/kalmanfilter.cpp \
    src/Visao/visao.cpp \
    src/Visao/visionpacket.cpp \
    src/cli/robofei_cli.cpp \
    src/robofeissl.cpp \
    src/main.cpp


LIBS += \
    -lprotobuf \
    -L$$(HOME)/miniconda3/lib -lOsqpEigen \
    -L$$(HOME)/miniconda3/lib -losqp

INCLUDEPATH += \
    src \
    include

#INCLUDEPATH += $$HOME/libs/OSQP/include
#DEPENDPATH += $$HOME/libs/OSQP/include

INCLUDEPATH += $$(HOME)/miniconda3/include
DEPENDPATH += $$(HOME)/miniconda3/include

unix:!macx: LIBS += -ldlib

INCLUDEPATH += $$(HOME)/miniconda3/include/osqp
INCLUDEPATH += /usr/include/eigen3
DEPENDPATH += $$(HOME)/miniconda3/include/osqp
DEPENDPATH += /usr/include/eigen3

RESOURCES += \
    Media/Figuras/figuras.qrc \
    Media/ModeloChute/modelo_chute.qrc \
    Media/Sons/sons.qrc

FORMS += \
    src/Interface/decisoestesteinterface.ui \
    src/Interface/joystickrobotcontrollerinterface.ui \
    src/Movimentacao/velocidade.ui \
    src/robofeissl.ui

#unix:!macx: PRE_TARGETDEPS += $$PWD/libs/OSQP/lib/libosqp.a
