# Docker

Imagem do docker do software [SSL-Strategy](https://gitlab.com/robofei/ssl/SSL-Strategy) da equipe [RoboFEI-SSL](https://gitlab.com/robofei/ssl).

## Uso

Para iniciar, faça do diretório `ubuntu20_04` seu diretório de trabalho.

### Instalação do docker

No ubuntu, rode o script ``installDocker-Ubuntu.sh``. Será necessária permissão de root.

No Manjaro/Arch Linux, execute os comandos:

```sh
# instala o docker
sudo pacman -S docker
# Inicia e habilita o serviço docker
sudo systemctl start docker.service
sudo systemctl enable docker.service
```

Opcionalmente, execute o comando abaixo para poder usar o docker sem a permissão de root (após executar o comando, é necessário reiniciar o sistema para a mudança fazer efeito):

```sh
sudo usermod -aG docker $USER
```

### Construindo (ou puxando) e rodando a imagem do docker

Para construir a imagem do docker, execute o comando abaixo:

```sh
docker build -t registry.gitlab.com/robofei/ssl/ssl-strategy . 
```

Outra alternativa é puxar a imagem do docker pronta. Para isso, execute o comando abaixo:

```sh
docker pull registry.gitlab.com/robofei/ssl/ssl-strategy:latest 
```

Após o processo ser finalizado, rode o script `rundocker.sh` para iniciar o docker.

### Compilando e rodando o SSL-Strategy

Para realizar o processo de compilação do software, execute os comandos abaixo:

```sh
git clone https://gitlab.com/robofei/ssl/SSL-Strategy.git
cd SSL-Strategy


cd src/python
python3 create_models.py
pip3 install -e .

cd ../..
mkdir build
cd build
qmake CONFIG+=release ..
make
```

O executável será gerado no diretório de trabalho. Para executá-lo, digite:

```sh
./RoboFeiSSL
```