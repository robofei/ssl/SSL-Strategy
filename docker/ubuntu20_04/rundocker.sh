DEFAULT_DOCKER_IMAGE="registry.gitlab.com/robofei/ssl/ssl-strategy"

# Executando o docker
docker run \
   --interactive \
   --tty \
   --net="host" \
   --volume="/dev:/dev" \
   --privileged \
   --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
   --rm \
   $DEFAULT_DOCKER_IMAGE
