#!/usr/bin/sh

sudo apt install -y libeigen3-dev python3-tk python3-pip pybind11-dev
pip3 install numpy Pillow sklearn twine wheel setuptools console_progressbar

cd ../src/python
python3 setup.py check
python3 create_models.py
python3 -m pip install -e .
