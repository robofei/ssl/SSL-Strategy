

greaterThan(QT_MAJOR_VERSION, 4)
include($$PWD/QtMessageFilter/QtMessageFilter.pri)

QT += \
    widgets \
    printsupport

HEADERS += \
    $$PWD/GoleiroNeural/rede_simuliink.h \
    $$PWD/Movimentacao/Controle/ControleFuzzy_private.h \
    $$PWD/Movimentacao/Controle/ControleFuzzy_types.h \
    $$PWD/Movimentacao/Controle/ControlePosicao.h \
    $$PWD/Movimentacao/Controle/ControleFuzzy.h \
    $$PWD/Movimentacao/Controle/ControlePosicao_private.h \
    $$PWD/Movimentacao/Controle/ControlePosicao_types.h \
    $$PWD/Movimentacao/Controle/multiword_types.h \
    $$PWD/Movimentacao/Controle/rt_nonfinite.h \
    $$PWD/Movimentacao/Controle/rtGetInf.h \
    $$PWD/Movimentacao/Controle/rtGetNaN.h \
    $$PWD/Movimentacao/Controle/rtmodel.h \
    $$PWD/Movimentacao/Controle/rtw_continuous.h \
    $$PWD/Movimentacao/Controle/rtw_extmode.h \
    $$PWD/Movimentacao/Controle/rtw_matlogging.h \
    $$PWD/Movimentacao/Controle/rtw_solver.h \
    $$PWD/Movimentacao/Controle/rtwtypes.h \
    $$PWD/Movimentacao/Controle/simstruc_types.h \
    $$PWD/Movimentacao/Controle/sl_sample_time_defs.h \
    $$PWD/Movimentacao/Controle/sl_types_def.h \
    $$PWD/Movimentacao/Controle/sysran_types.h \
    $$PWD/Movimentacao/Controle/tmwtypes.h \
    $$PWD/QCustomPlot/qcustomplot.h \
    $$PWD/QCustomPlot/QCustomPlot \
    $$PWD/VirtualJoystick/virtualjoystick.h

SOURCES += \
    $$PWD/GoleiroNeural/rede_simuliink.cpp \
    $$PWD/Movimentacao/Controle/ControlePosicao.cpp \
    $$PWD/Movimentacao/Controle/ControleFuzzy.cpp \
    $$PWD/Movimentacao/Controle/rt_nonfinite.cpp \
    $$PWD/Movimentacao/Controle/rtGetInf.cpp \
    $$PWD/Movimentacao/Controle/rtGetNaN.cpp \
    $$PWD/Movimentacao/Controle/ControleFuzzy_data.cpp \
    $$PWD/QCustomPlot/qcustomplot.cpp \
    $$PWD/VirtualJoystick/virtualjoystick.cpp
