/*
 * rede_simuliink.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "rede_simuliink".
 *
 * Model version              : 1.3
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C++ source code generated on : Wed Oct 23 13:09:27 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "rede_simuliink.h"
#include "rede_simuliink_private.h"

/* Model step function */
void rede_simuliinkModelClass::step()
{
  real_T x;
  real_T tmp[5];
  real_T tmp_0[7];
  real_T tmp_1[10];
  real_T tmp_2;
  real_T tmp_3;
  real_T tmp_4;
  real_T tmp_5;
  int32_T i;
  real_T rtb_netsum;
  real_T rtb_Sum1_h;
  real_T rtb_Sum1_d;
  real_T rtb_netsum_idx_0;
  real_T rtb_Sum1_idx_0;
  real_T rtb_Sum1_idx_1;

  /* Gain: '<S51>/range y // range x' */
  x = rede_simuliink_P.mapminmax_ymax - rede_simuliink_P.mapminmax_ymin;

  /* Bias: '<S51>/Add min y' incorporates:
   *  Bias: '<S51>/Subtract min x'
   *  Gain: '<S51>/range y // range x'
   *  Inport: '<Root>/posBolaX'
   */
  rtb_netsum = x / (rede_simuliink_P.mapminmax_xmax[0] -
                    rede_simuliink_P.mapminmax_xmin[0]) *
    (rede_simuliink_U.posBolaX[0] + -rede_simuliink_P.mapminmax_xmin[0]) +
    rede_simuliink_P.mapminmax_ymin;

  /* DotProduct: '<S12>/Dot Product' incorporates:
   *  Constant: '<S10>/IW{1,1}(1,:)''
   */
  tmp_2 = rede_simuliink_P.IW111_Value[0] * rtb_netsum;

  /* Bias: '<S51>/Add min y' incorporates:
   *  Bias: '<S51>/Subtract min x'
   *  Gain: '<S51>/range y // range x'
   *  Inport: '<Root>/posBolaX'
   */
  rtb_netsum_idx_0 = rtb_netsum;
  rtb_netsum = x / (rede_simuliink_P.mapminmax_xmax[1] -
                    rede_simuliink_P.mapminmax_xmin[1]) *
    (rede_simuliink_U.posBolaX[1] + -rede_simuliink_P.mapminmax_xmin[1]) +
    rede_simuliink_P.mapminmax_ymin;

  /* DotProduct: '<S12>/Dot Product' incorporates:
   *  Constant: '<S10>/IW{1,1}(1,:)''
   */
  tmp_2 += rede_simuliink_P.IW111_Value[1] * rtb_netsum;

  /* Sum: '<S11>/Sum1' incorporates:
   *  Constant: '<S11>/one'
   *  Constant: '<S11>/one1'
   *  Constant: '<S2>/b{1}'
   *  DotProduct: '<S12>/Dot Product'
   *  Gain: '<S11>/Gain'
   *  Gain: '<S11>/Gain1'
   *  Math: '<S11>/Exp'
   *  Math: '<S11>/Reciprocal'
   *  Sum: '<S11>/Sum'
   *  Sum: '<S2>/netsum'
   *
   * About '<S11>/Exp':
   *  Operator: exp
   *
   * About '<S11>/Reciprocal':
   *  Operator: reciprocal
   */
  tmp_2 = 1.0 / (std::exp((tmp_2 + rede_simuliink_P.b1_Value[0]) *
    rede_simuliink_P.Gain_Gain) + rede_simuliink_P.one_Value) *
    rede_simuliink_P.Gain1_Gain - rede_simuliink_P.one1_Value;

  /* DotProduct: '<S18>/Dot Product' incorporates:
   *  Constant: '<S16>/IW{2,1}(1,:)''
   */
  x = rede_simuliink_P.IW211_Value[0] * tmp_2;

  /* Sum: '<S11>/Sum1' incorporates:
   *  Constant: '<S10>/IW{1,1}(2,:)''
   *  Constant: '<S11>/one'
   *  Constant: '<S11>/one1'
   *  Constant: '<S2>/b{1}'
   *  DotProduct: '<S13>/Dot Product'
   *  Gain: '<S11>/Gain'
   *  Gain: '<S11>/Gain1'
   *  Math: '<S11>/Exp'
   *  Math: '<S11>/Reciprocal'
   *  Sum: '<S11>/Sum'
   *  Sum: '<S2>/netsum'
   *
   * About '<S11>/Exp':
   *  Operator: exp
   *
   * About '<S11>/Reciprocal':
   *  Operator: reciprocal
   */
  rtb_Sum1_idx_0 = tmp_2;
  tmp_2 = 1.0 / (std::exp(((rede_simuliink_P.IW112_Value[0] * rtb_netsum_idx_0 +
    rede_simuliink_P.IW112_Value[1] * rtb_netsum) + rede_simuliink_P.b1_Value[1])
    * rede_simuliink_P.Gain_Gain) + rede_simuliink_P.one_Value) *
    rede_simuliink_P.Gain1_Gain - rede_simuliink_P.one1_Value;

  /* DotProduct: '<S18>/Dot Product' incorporates:
   *  Constant: '<S16>/IW{2,1}(1,:)''
   */
  x += rede_simuliink_P.IW211_Value[1] * tmp_2;

  /* Sum: '<S11>/Sum1' incorporates:
   *  Constant: '<S10>/IW{1,1}(3,:)''
   *  Constant: '<S11>/one'
   *  Constant: '<S11>/one1'
   *  Constant: '<S2>/b{1}'
   *  DotProduct: '<S14>/Dot Product'
   *  Gain: '<S11>/Gain'
   *  Gain: '<S11>/Gain1'
   *  Math: '<S11>/Exp'
   *  Math: '<S11>/Reciprocal'
   *  Sum: '<S11>/Sum'
   *  Sum: '<S2>/netsum'
   *
   * About '<S11>/Exp':
   *  Operator: exp
   *
   * About '<S11>/Reciprocal':
   *  Operator: reciprocal
   */
  rtb_Sum1_idx_1 = tmp_2;
  tmp_2 = 1.0 / (std::exp(((rede_simuliink_P.IW113_Value[0] * rtb_netsum_idx_0 +
    rede_simuliink_P.IW113_Value[1] * rtb_netsum) + rede_simuliink_P.b1_Value[2])
    * rede_simuliink_P.Gain_Gain) + rede_simuliink_P.one_Value) *
    rede_simuliink_P.Gain1_Gain - rede_simuliink_P.one1_Value;

  /* DotProduct: '<S18>/Dot Product' incorporates:
   *  Constant: '<S16>/IW{2,1}(1,:)''
   */
  x += rede_simuliink_P.IW211_Value[2] * tmp_2;

  /* Sum: '<S3>/netsum' incorporates:
   *  Constant: '<S16>/IW{2,1}(2,:)''
   *  Constant: '<S16>/IW{2,1}(3,:)''
   *  Constant: '<S16>/IW{2,1}(4,:)''
   *  Constant: '<S16>/IW{2,1}(5,:)''
   *  DotProduct: '<S18>/Dot Product'
   *  DotProduct: '<S19>/Dot Product'
   *  DotProduct: '<S20>/Dot Product'
   *  DotProduct: '<S21>/Dot Product'
   *  DotProduct: '<S22>/Dot Product'
   */
  tmp[0] = x;
  tmp[1] = (rede_simuliink_P.IW212_Value[0] * rtb_Sum1_idx_0 +
            rede_simuliink_P.IW212_Value[1] * rtb_Sum1_idx_1) +
    rede_simuliink_P.IW212_Value[2] * tmp_2;
  tmp[2] = (rede_simuliink_P.IW213_Value[0] * rtb_Sum1_idx_0 +
            rede_simuliink_P.IW213_Value[1] * rtb_Sum1_idx_1) +
    rede_simuliink_P.IW213_Value[2] * tmp_2;
  tmp[3] = (rede_simuliink_P.IW214_Value[0] * rtb_Sum1_idx_0 +
            rede_simuliink_P.IW214_Value[1] * rtb_Sum1_idx_1) +
    rede_simuliink_P.IW214_Value[2] * tmp_2;
  tmp[4] = (rede_simuliink_P.IW215_Value[0] * rtb_Sum1_idx_0 +
            rede_simuliink_P.IW215_Value[1] * rtb_Sum1_idx_1) +
    rede_simuliink_P.IW215_Value[2] * tmp_2;

  /* DotProduct: '<S26>/Dot Product' */
  x = 0.0;

  /* DotProduct: '<S27>/Dot Product' */
  rtb_netsum = 0.0;

  /* DotProduct: '<S28>/Dot Product' */
  rtb_netsum_idx_0 = 0.0;

  /* DotProduct: '<S29>/Dot Product' */
  tmp_2 = 0.0;

  /* DotProduct: '<S30>/Dot Product' */
  rtb_Sum1_idx_0 = 0.0;

  /* DotProduct: '<S31>/Dot Product' */
  rtb_Sum1_idx_1 = 0.0;

  /* DotProduct: '<S32>/Dot Product' */
  tmp_3 = 0.0;
  for (i = 0; i < 5; i++) {
    /* Sum: '<S17>/Sum1' incorporates:
     *  Constant: '<S17>/one'
     *  Constant: '<S17>/one1'
     *  Constant: '<S3>/b{2}'
     *  Gain: '<S17>/Gain'
     *  Gain: '<S17>/Gain1'
     *  Math: '<S17>/Exp'
     *  Math: '<S17>/Reciprocal'
     *  Sum: '<S17>/Sum'
     *  Sum: '<S3>/netsum'
     *
     * About '<S17>/Exp':
     *  Operator: exp
     *
     * About '<S17>/Reciprocal':
     *  Operator: reciprocal
     */
    rtb_Sum1_h = 1.0 / (std::exp((tmp[i] + rede_simuliink_P.b2_Value[i]) *
      rede_simuliink_P.Gain_Gain_c) + rede_simuliink_P.one_Value_d) *
      rede_simuliink_P.Gain1_Gain_b - rede_simuliink_P.one1_Value_f;

    /* DotProduct: '<S26>/Dot Product' incorporates:
     *  Constant: '<S24>/IW{3,2}(1,:)''
     */
    x += rede_simuliink_P.IW321_Value[i] * rtb_Sum1_h;

    /* DotProduct: '<S27>/Dot Product' incorporates:
     *  Constant: '<S24>/IW{3,2}(2,:)''
     */
    rtb_netsum += rede_simuliink_P.IW322_Value[i] * rtb_Sum1_h;

    /* DotProduct: '<S28>/Dot Product' incorporates:
     *  Constant: '<S24>/IW{3,2}(3,:)''
     */
    rtb_netsum_idx_0 += rede_simuliink_P.IW323_Value[i] * rtb_Sum1_h;

    /* DotProduct: '<S29>/Dot Product' incorporates:
     *  Constant: '<S24>/IW{3,2}(4,:)''
     */
    tmp_2 += rede_simuliink_P.IW324_Value[i] * rtb_Sum1_h;

    /* DotProduct: '<S30>/Dot Product' incorporates:
     *  Constant: '<S24>/IW{3,2}(5,:)''
     */
    rtb_Sum1_idx_0 += rede_simuliink_P.IW325_Value[i] * rtb_Sum1_h;

    /* DotProduct: '<S31>/Dot Product' incorporates:
     *  Constant: '<S24>/IW{3,2}(6,:)''
     */
    rtb_Sum1_idx_1 += rede_simuliink_P.IW326_Value[i] * rtb_Sum1_h;

    /* DotProduct: '<S32>/Dot Product' incorporates:
     *  Constant: '<S24>/IW{3,2}(7,:)''
     */
    tmp_3 += rede_simuliink_P.IW327_Value[i] * rtb_Sum1_h;
  }

  /* Sum: '<S4>/netsum' incorporates:
   *  DotProduct: '<S26>/Dot Product'
   *  DotProduct: '<S27>/Dot Product'
   *  DotProduct: '<S28>/Dot Product'
   *  DotProduct: '<S29>/Dot Product'
   *  DotProduct: '<S30>/Dot Product'
   *  DotProduct: '<S31>/Dot Product'
   *  DotProduct: '<S32>/Dot Product'
   */
  tmp_0[0] = x;
  tmp_0[1] = rtb_netsum;
  tmp_0[2] = rtb_netsum_idx_0;
  tmp_0[3] = tmp_2;
  tmp_0[4] = rtb_Sum1_idx_0;
  tmp_0[5] = rtb_Sum1_idx_1;
  tmp_0[6] = tmp_3;

  /* DotProduct: '<S36>/Dot Product' */
  x = 0.0;

  /* DotProduct: '<S38>/Dot Product' */
  rtb_netsum = 0.0;

  /* DotProduct: '<S39>/Dot Product' */
  rtb_netsum_idx_0 = 0.0;

  /* DotProduct: '<S40>/Dot Product' */
  tmp_2 = 0.0;

  /* DotProduct: '<S41>/Dot Product' */
  rtb_Sum1_idx_0 = 0.0;

  /* DotProduct: '<S42>/Dot Product' */
  rtb_Sum1_idx_1 = 0.0;

  /* DotProduct: '<S43>/Dot Product' */
  tmp_3 = 0.0;

  /* DotProduct: '<S44>/Dot Product' */
  rtb_Sum1_h = 0.0;

  /* DotProduct: '<S45>/Dot Product' */
  tmp_4 = 0.0;

  /* DotProduct: '<S37>/Dot Product' */
  tmp_5 = 0.0;
  for (i = 0; i < 7; i++) {
    /* Sum: '<S25>/Sum1' incorporates:
     *  Constant: '<S25>/one'
     *  Constant: '<S25>/one1'
     *  Constant: '<S4>/b{3}'
     *  Gain: '<S25>/Gain'
     *  Gain: '<S25>/Gain1'
     *  Math: '<S25>/Exp'
     *  Math: '<S25>/Reciprocal'
     *  Sum: '<S25>/Sum'
     *  Sum: '<S4>/netsum'
     *
     * About '<S25>/Exp':
     *  Operator: exp
     *
     * About '<S25>/Reciprocal':
     *  Operator: reciprocal
     */
    rtb_Sum1_d = 1.0 / (std::exp((tmp_0[i] + rede_simuliink_P.b3_Value[i]) *
      rede_simuliink_P.Gain_Gain_m) + rede_simuliink_P.one_Value_h) *
      rede_simuliink_P.Gain1_Gain_o - rede_simuliink_P.one1_Value_g;

    /* DotProduct: '<S36>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(1,:)''
     */
    x += rede_simuliink_P.IW431_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S38>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(2,:)''
     */
    rtb_netsum += rede_simuliink_P.IW432_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S39>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(3,:)''
     */
    rtb_netsum_idx_0 += rede_simuliink_P.IW433_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S40>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(4,:)''
     */
    tmp_2 += rede_simuliink_P.IW434_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S41>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(5,:)''
     */
    rtb_Sum1_idx_0 += rede_simuliink_P.IW435_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S42>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(6,:)''
     */
    rtb_Sum1_idx_1 += rede_simuliink_P.IW436_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S43>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(7,:)''
     */
    tmp_3 += rede_simuliink_P.IW437_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S44>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(8,:)''
     */
    rtb_Sum1_h += rede_simuliink_P.IW438_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S45>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(9,:)''
     */
    tmp_4 += rede_simuliink_P.IW439_Value[i] * rtb_Sum1_d;

    /* DotProduct: '<S37>/Dot Product' incorporates:
     *  Constant: '<S34>/IW{4,3}(10,:)''
     */
    tmp_5 += rede_simuliink_P.IW4310_Value[i] * rtb_Sum1_d;
  }

  /* Sum: '<S5>/netsum' incorporates:
   *  DotProduct: '<S36>/Dot Product'
   *  DotProduct: '<S37>/Dot Product'
   *  DotProduct: '<S38>/Dot Product'
   *  DotProduct: '<S39>/Dot Product'
   *  DotProduct: '<S40>/Dot Product'
   *  DotProduct: '<S41>/Dot Product'
   *  DotProduct: '<S42>/Dot Product'
   *  DotProduct: '<S43>/Dot Product'
   *  DotProduct: '<S44>/Dot Product'
   *  DotProduct: '<S45>/Dot Product'
   */
  tmp_1[0] = x;
  tmp_1[1] = rtb_netsum;
  tmp_1[2] = rtb_netsum_idx_0;
  tmp_1[3] = tmp_2;
  tmp_1[4] = rtb_Sum1_idx_0;
  tmp_1[5] = rtb_Sum1_idx_1;
  tmp_1[6] = tmp_3;
  tmp_1[7] = rtb_Sum1_h;
  tmp_1[8] = tmp_4;
  tmp_1[9] = tmp_5;

  /* Gain: '<S52>/Divide by range y' */
  x = rede_simuliink_P.mapminmax_reverse_ymax -
    rede_simuliink_P.mapminmax_reverse_ymin;

  /* DotProduct: '<S49>/Dot Product' */
  tmp_2 = 0.0;

  /* DotProduct: '<S50>/Dot Product' */
  rtb_netsum = 0.0;
  for (i = 0; i < 10; i++) {
    /* Sum: '<S35>/Sum1' incorporates:
     *  Constant: '<S35>/one'
     *  Constant: '<S35>/one1'
     *  Constant: '<S5>/b{4}'
     *  Gain: '<S35>/Gain'
     *  Gain: '<S35>/Gain1'
     *  Math: '<S35>/Exp'
     *  Math: '<S35>/Reciprocal'
     *  Sum: '<S35>/Sum'
     *  Sum: '<S5>/netsum'
     *
     * About '<S35>/Exp':
     *  Operator: exp
     *
     * About '<S35>/Reciprocal':
     *  Operator: reciprocal
     */
    rtb_netsum_idx_0 = 1.0 / (std::exp((tmp_1[i] + rede_simuliink_P.b4_Value[i])
      * rede_simuliink_P.Gain_Gain_n) + rede_simuliink_P.one_Value_m) *
      rede_simuliink_P.Gain1_Gain_i - rede_simuliink_P.one1_Value_k;

    /* DotProduct: '<S49>/Dot Product' incorporates:
     *  Constant: '<S47>/IW{5,4}(1,:)''
     */
    tmp_2 += rede_simuliink_P.IW541_Value[i] * rtb_netsum_idx_0;

    /* DotProduct: '<S50>/Dot Product' incorporates:
     *  Constant: '<S47>/IW{5,4}(2,:)''
     */
    rtb_netsum += rede_simuliink_P.IW542_Value[i] * rtb_netsum_idx_0;
  }

  /* Outport: '<Root>/posRoboX' incorporates:
   *  Bias: '<S52>/Add min x'
   *  Bias: '<S52>/Subtract min y'
   *  Constant: '<S6>/b{5}'
   *  DotProduct: '<S49>/Dot Product'
   *  DotProduct: '<S50>/Dot Product'
   *  Gain: '<S52>/Divide by range y'
   *  Sum: '<S6>/netsum'
   */
  rede_simuliink_Y.posRoboX[0] = (rede_simuliink_P.mapminmax_reverse_xmax[0] -
    rede_simuliink_P.mapminmax_reverse_xmin[0]) / x * ((tmp_2 +
    rede_simuliink_P.b5_Value[0]) + -rede_simuliink_P.mapminmax_reverse_ymin) +
    rede_simuliink_P.mapminmax_reverse_xmin[0];
  rede_simuliink_Y.posRoboX[1] = (rede_simuliink_P.mapminmax_reverse_xmax[1] -
    rede_simuliink_P.mapminmax_reverse_xmin[1]) / x * ((rtb_netsum +
    rede_simuliink_P.b5_Value[1]) + -rede_simuliink_P.mapminmax_reverse_ymin) +
    rede_simuliink_P.mapminmax_reverse_xmin[1];
}

/* Model initialize function */
void rede_simuliinkModelClass::initialize()
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(getRTM(), (NULL));

  /* external inputs */
  (void)memset(&rede_simuliink_U, 0, sizeof(ExtU_rede_simuliink_T));

  /* external outputs */
  (void) memset(&rede_simuliink_Y.posRoboX[0], 0,
                2U*sizeof(real_T));
}

/* Model terminate function */
void rede_simuliinkModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
rede_simuliinkModelClass::rede_simuliinkModelClass()
{
  static const P_rede_simuliink_T rede_simuliink_P_temp = {
    /* Mask Parameter: mapminmax_xmax
     * Referenced by: '<S51>/range y // range x'
     */
    { 6260.88, 4789.94 },

    /* Mask Parameter: mapminmax_reverse_xmax
     * Referenced by: '<S52>/Divide by range y'
     */
    { -0.089701, 1217.39 },

    /* Mask Parameter: mapminmax_xmin
     * Referenced by:
     *   '<S51>/Subtract min x'
     *   '<S51>/range y // range x'
     */
    { -6274.17, -4789.91 },

    /* Mask Parameter: mapminmax_reverse_xmin
     * Referenced by:
     *   '<S52>/Add min x'
     *   '<S52>/Divide by range y'
     */
    { -6762.22, -787.681 },

    /* Mask Parameter: mapminmax_ymax
     * Referenced by: '<S51>/range y // range x'
     */
    1.0,

    /* Mask Parameter: mapminmax_reverse_ymax
     * Referenced by: '<S52>/Divide by range y'
     */
    1.0,

    /* Mask Parameter: mapminmax_ymin
     * Referenced by:
     *   '<S51>/Add min y'
     *   '<S51>/range y // range x'
     */
    -1.0,

    /* Mask Parameter: mapminmax_reverse_ymin
     * Referenced by:
     *   '<S52>/Subtract min y'
     *   '<S52>/Divide by range y'
     */
    -1.0,

    /* Expression: [0.08936453727003561275932241869668359868228435516357421875;-0.5101618503215885613855107294511981308460235595703125;0.53970556202535047152224478850257582962512969970703125;-0.182359553339748625688798711053095757961273193359375;-0.10797789193413021013867592046153731644153594970703125;-0.1785187666389378702280765764953684993088245391845703125;-0.166803214523228771071217124699614942073822021484375;-0.54162346436296815443967034298111684620380401611328125;0.61075716879408437076648397123790346086025238037109375;-0.165850898012949443671715243908693082630634307861328125]
     * Referenced by: '<S47>/IW{5,4}(1,:)''
     */
    { 0.089364537270035613, -0.51016185032158856, 0.53970556202535047,
      -0.18235955333974863, -0.10797789193413021, -0.17851876663893787,
      -0.16680321452322877, -0.54162346436296815, 0.61075716879408437,
      -0.16585089801294944 },

    /* Expression: [-0.0561507078805588422110162127864896319806575775146484375;-0.9709987446803769461922684058663435280323028564453125;0.48077479700115366423318619126803241670131683349609375;7.38776876186521302969367752666585147380828857421875;-0.023324336064701987891734091817852458916604518890380859375;-1.4224665722384475241568679848569445312023162841796875;0.155620721882586632300871087863924913108348846435546875]
     * Referenced by: '<S34>/IW{4,3}(1,:)''
     */
    { -0.056150707880558842, -0.970998744680377, 0.48077479700115366,
      7.387768761865213, -0.023324336064701988, -1.4224665722384475,
      0.15562072188258663 },

    /* Expression: [-0.54255423243801226274030113927437923848628997802734375;-0.5167100284729302384079119292437098920345306396484375;4.10203415039652252715995928156189620494842529296875;-4.76632334633455911898636259138584136962890625;-10.2828231693631888532536322600208222866058349609375]
     * Referenced by: '<S24>/IW{3,2}(1,:)''
     */
    { -0.54255423243801226, -0.51671002847293024, 4.1020341503965225,
      -4.7663233463345591, -10.282823169363189 },

    /* Expression: [-5.70340755951672928603102263878099620342254638671875;3.234744876113914369142321447725407779216766357421875;1.44370194943914054164224580745212733745574951171875]
     * Referenced by: '<S16>/IW{2,1}(1,:)''
     */
    { -5.7034075595167293, 3.2347448761139144, 1.4437019494391405 },

    /* Expression: [-1.6073053984531766591459245319128967821598052978515625;1.768211022151295441773299899068661034107208251953125]
     * Referenced by: '<S10>/IW{1,1}(1,:)''
     */
    { -1.6073053984531767, 1.7682110221512954 },

    /* Expression: [1.872504425208517897516458106110803782939910888671875;3.848673204603994424388702100259251892566680908203125]
     * Referenced by: '<S10>/IW{1,1}(2,:)''
     */
    { 1.8725044252085179, 3.8486732046039944 },

    /* Expression: [-1.8985793255364613596469780532061122357845306396484375;2.28447114822101138287280264194123446941375732421875]
     * Referenced by: '<S10>/IW{1,1}(3,:)''
     */
    { -1.8985793255364614, 2.2844711482210114 },

    /* Expression: [2.202044403199738820120501259225420653820037841796875;0.8473896768382978006428629669244401156902313232421875;-1.4491416617837729052808981577982194721698760986328125]
     * Referenced by: '<S2>/b{1}'
     */
    { 2.2020444031997388, 0.8473896768382978, -1.4491416617837729 },

    /* Expression: -2
     * Referenced by: '<S11>/Gain'
     */
    -2.0,

    /* Expression: 1
     * Referenced by: '<S11>/one'
     */
    1.0,

    /* Expression: 2
     * Referenced by: '<S11>/Gain1'
     */
    2.0,

    /* Expression: 1
     * Referenced by: '<S11>/one1'
     */
    1.0,

    /* Expression: [0.99193216556846064424490805322420783340930938720703125;-3.706758263141384901473429636098444461822509765625;0.1683802782464148550811700033591478131711483001708984375]
     * Referenced by: '<S16>/IW{2,1}(2,:)''
     */
    { 0.99193216556846064, -3.7067582631413849, 0.16838027824641486 },

    /* Expression: [-0.410861834488620292216154439302044920623302459716796875;-4.96557210019962891323075382388196885585784912109375;3.725654137258348708172661645221523940563201904296875]
     * Referenced by: '<S16>/IW{2,1}(3,:)''
     */
    { -0.41086183448862029, -4.9655721001996289, 3.7256541372583487 },

    /* Expression: [-0.87767798836806232287699458538554608821868896484375;-0.5519211069885423537328961174353025853633880615234375;-0.86768873062915707894404704347834922373294830322265625]
     * Referenced by: '<S16>/IW{2,1}(4,:)''
     */
    { -0.87767798836806232, -0.55192110698854235, -0.86768873062915708 },

    /* Expression: [-0.84887701424847994413624974185950122773647308349609375;-4.3500023545707851013730760314501821994781494140625;5.26609110901443866481486111297272145748138427734375]
     * Referenced by: '<S16>/IW{2,1}(5,:)''
     */
    { -0.84887701424847994, -4.3500023545707851, 5.2660911090144387 },

    /* Expression: [4.06983851579939237552707709255628287792205810546875;2.39770803303196000655361785902641713619232177734375;-2.204354256518528121233657657285220921039581298828125;1.6347408471291924314527932438068091869354248046875;-0.47585933576305639025605387359973974525928497314453125]
     * Referenced by: '<S3>/b{2}'
     */
    { 4.0698385157993924, 2.39770803303196, -2.2043542565185281,
      1.6347408471291924, -0.47585933576305639 },

    /* Expression: -2
     * Referenced by: '<S17>/Gain'
     */
    -2.0,

    /* Expression: 1
     * Referenced by: '<S17>/one'
     */
    1.0,

    /* Expression: 2
     * Referenced by: '<S17>/Gain1'
     */
    2.0,

    /* Expression: 1
     * Referenced by: '<S17>/one1'
     */
    1.0,

    /* Expression: [-3.86080370270397121856831290642730891704559326171875;-2.62873177238049127169006169424392282962799072265625;1.4116399942636272957230403335415758192539215087890625;-2.619120297758621429551340042962692677974700927734375;-2.1191519002754120037934626452624797821044921875]
     * Referenced by: '<S24>/IW{3,2}(2,:)''
     */
    { -3.8608037027039712, -2.6287317723804913, 1.4116399942636273,
      -2.6191202977586214, -2.119151900275412 },

    /* Expression: [0.65034541158874692445834853060659952461719512939453125;-1.7836609215233030223402010960853658616542816162109375;1.612588930526041242075052650761790573596954345703125;-1.6999593262510976909851478922064416110515594482421875;-0.92198649144857591775092942043556831777095794677734375]
     * Referenced by: '<S24>/IW{3,2}(3,:)''
     */
    { 0.65034541158874692, -1.783660921523303, 1.6125889305260412,
      -1.6999593262510977, -0.92198649144857592 },

    /* Expression: [-0.1707805822956424968150912491182680241763591766357421875;-1.748881562175201853648331962176598608493804931640625;-0.281049887198347392303077185715665109455585479736328125;-0.55193939094625843466701553552411496639251708984375;1.1781032959994657272062568154069595038890838623046875]
     * Referenced by: '<S24>/IW{3,2}(4,:)''
     */
    { -0.1707805822956425, -1.7488815621752019, -0.28104988719834739,
      -0.55193939094625843, 1.1781032959994657 },

    /* Expression: [-2.077998045022251716318351100198924541473388671875;-0.08292599300509129189862278508371673524379730224609375;-0.08115792069495135485635017857930506579577922821044921875;0.6282881550875796250466009951196610927581787109375;-1.84360956124673958100856907549314200878143310546875]
     * Referenced by: '<S24>/IW{3,2}(5,:)''
     */
    { -2.0779980450222517, -0.082925993005091292, -0.081157920694951355,
      0.62828815508757963, -1.8436095612467396 },

    /* Expression: [0.71055458880568023172230596173903904855251312255859375;9.173138753615450724510083091445267200469970703125;1.3338333592420976714265634655021131038665771484375;2.663987810222400387516472619608975946903228759765625;-6.60311874746318583362381104961968958377838134765625]
     * Referenced by: '<S24>/IW{3,2}(6,:)''
     */
    { 0.71055458880568023, 9.17313875361545, 1.3338333592420977,
      2.6639878102224004, -6.6031187474631858 },

    /* Expression: [-6.1914849166199825702960879425518214702606201171875;3.357009215085892339658357741427607834339141845703125;-0.82309969035453056651618908290402032434940338134765625;-14.51442939552739375130840926431119441986083984375;-2.695827973231632146422498408355750143527984619140625]
     * Referenced by: '<S24>/IW{3,2}(7,:)''
     */
    { -6.1914849166199826, 3.3570092150858923, -0.82309969035453057,
      -14.514429395527394, -2.6958279732316321 },

    /* Expression: [-1.9966870407009740251425000678864307701587677001953125;1.1682185529199256013299645928782410919666290283203125;-1.6208952508405547465741847190656699240207672119140625;0.65860856444346260740729803728754632174968719482421875;-1.131078676373210800676361031946726143360137939453125;-1.592418027194547835989624218200333416461944580078125;1.017362711463260627198224028688855469226837158203125]
     * Referenced by: '<S4>/b{3}'
     */
    { -1.996687040700974, 1.1682185529199256, -1.6208952508405547,
      0.65860856444346261, -1.1310786763732108, -1.5924180271945478,
      1.0173627114632606 },

    /* Expression: -2
     * Referenced by: '<S25>/Gain'
     */
    -2.0,

    /* Expression: 1
     * Referenced by: '<S25>/one'
     */
    1.0,

    /* Expression: 2
     * Referenced by: '<S25>/Gain1'
     */
    2.0,

    /* Expression: 1
     * Referenced by: '<S25>/one1'
     */
    1.0,

    /* Expression: [-2.695993050862083517671408117166720330715179443359375;2.60626918741059920847646935726515948772430419921875;2.611733699902209604459812908316962420940399169921875;0.907781759158780232610297389328479766845703125;-5.02456920869407941410145213012583553791046142578125;0.314150560058161276710819720392464660108089447021484375;0.77063053824157368154601499554701149463653564453125]
     * Referenced by: '<S34>/IW{4,3}(2,:)''
     */
    { -2.6959930508620835, 2.6062691874105992, 2.6117336999022096,
      0.90778175915878023, -5.0245692086940794, 0.31415056005816128,
      0.77063053824157368 },

    /* Expression: [3.864735788207287914275411822018213570117950439453125;3.284567192539810331908256557653658092021942138671875;2.243387722325452582339266882627271115779876708984375;-1.329062664367386847885654788115061819553375244140625;-2.16941624196750471043060315423645079135894775390625;0.2162798382018476794907968496772809885442256927490234375;-0.347720237481369986998203103212290443480014801025390625]
     * Referenced by: '<S34>/IW{4,3}(3,:)''
     */
    { 3.8647357882072879, 3.2845671925398103, 2.2433877223254526,
      -1.3290626643673868, -2.1694162419675047, 0.21627983820184768,
      -0.34772023748137 },

    /* Expression: [2.004363907785382981074917552177794277667999267578125;0.287673738858842842347485202481038868427276611328125;3.15344846094767472521880335989408195018768310546875;3.763151531030163088331619292148388922214508056640625;0.66010659411402439200600156254949979484081268310546875;0.309159495355692592877261404282762669026851654052734375;5.59886953084030469796061879605986177921295166015625]
     * Referenced by: '<S34>/IW{4,3}(4,:)''
     */
    { 2.004363907785383, 0.28767373885884284, 3.1534484609476747,
      3.7631515310301631, 0.66010659411402439, 0.30915949535569259,
      5.5988695308403047 },

    /* Expression: [-3.282735712910926917373899414087645709514617919921875;0.5811299926670463467104355004266835749149322509765625;-2.631609032834363315345171940862201154232025146484375;0.573123241362762936290664583793841302394866943359375;-0.137125044149172425900218286187737248837947845458984375;-1.393740230824583203883548776502721011638641357421875;-2.002809081281837766397302402765490114688873291015625]
     * Referenced by: '<S34>/IW{4,3}(5,:)''
     */
    { -3.2827357129109269, 0.58112999266704635, -2.6316090328343633,
      0.57312324136276294, -0.13712504414917243, -1.3937402308245832,
      -2.0028090812818378 },

    /* Expression: [-0.249219513429534023085665239705122075974941253662109375;-4.60994784731612039507808731286786496639251708984375;-0.1256753411512201712429970257289824075996875762939453125;-1.1594937104204616762359592030406929552555084228515625;3.383461082639399375437960770796053111553192138671875;1.8676548269360482290579739128588698804378509521484375;-1.9777570358726019872364076945814304053783416748046875]
     * Referenced by: '<S34>/IW{4,3}(6,:)''
     */
    { -0.24921951342953402, -4.60994784731612, -0.12567534115122017,
      -1.1594937104204617, 3.3834610826393994, 1.8676548269360482,
      -1.977757035872602 },

    /* Expression: [0.21885978782135551146126317689777351915836334228515625;-0.102799953046117387156499489719863049685955047607421875;-0.2987283569398775373571197633282281458377838134765625;-0.044387563706776426986078121217360603623092174530029296875;-0.04834244417154971718009193182297167368233203887939453125;-4.39454194697359579180329092196188867092132568359375;0.2096450760915211375401412396968225948512554168701171875]
     * Referenced by: '<S34>/IW{4,3}(7,:)''
     */
    { 0.21885978782135551, -0.10279995304611739, -0.29872835693987754,
      -0.044387563706776427, -0.048342444171549717, -4.3945419469735958,
      0.20964507609152114 },

    /* Expression: [-1.08661833203726931884602890932001173496246337890625;-5.45357742204567141897086912649683654308319091796875;-0.50591172066196665735304804911720566451549530029296875;-2.255397612725072775674561853520572185516357421875;-0.86228377857073545964539107444579713046550750732421875;0.7336652372291261325898403811152093112468719482421875;0.12711982264924326369026630345615558326244354248046875]
     * Referenced by: '<S34>/IW{4,3}(8,:)''
     */
    { -1.0866183320372693, -5.4535774220456714, -0.50591172066196666,
      -2.2553976127250728, -0.86228377857073546, 0.73366523722912613,
      0.12711982264924326 },

    /* Expression: [-0.918097002004703455213530105538666248321533203125;-4.92172067143732139271605774411000311374664306640625;0.94961283862937373445589628317975439131259918212890625;1.1991992552907728697419997843098826706409454345703125;3.099298567514465307937143734307028353214263916015625;-0.247841321021264049395682604881585575640201568603515625;0.17766560938377773393170855342759750783443450927734375]
     * Referenced by: '<S34>/IW{4,3}(9,:)''
     */
    { -0.91809700200470346, -4.9217206714373214, 0.94961283862937373,
      1.1991992552907729, 3.0992985675144653, -0.24784132102126405,
      0.17766560938377773 },

    /* Expression: [-4.06767927345018076579208354814909398555755615234375;-4.17480191218076157610994414426386356353759765625;4.10783681172902692679826941457577049732208251953125;0.603093363920731206917480449192225933074951171875;5.66198992998527206310654946719296276569366455078125;-3.40015730038663743783899917616508901119232177734375;0.52483462234898758946854968598927371203899383544921875]
     * Referenced by: '<S34>/IW{4,3}(10,:)''
     */
    { -4.0676792734501808, -4.1748019121807616, 4.1078368117290269,
      0.60309336392073121, 5.6619899299852721, -3.4001573003866374,
      0.52483462234898759 },

    /* Expression: [-1.026442379840882157537862440221942961215972900390625;-4.21146791083577820558048188104294240474700927734375;-2.09657495247015646100408048368990421295166015625;-0.79146173343722769910613124011433683335781097412109375;3.0957599895470782058737313491292297840118408203125;-1.425448919935949021464693942107260227203369140625;2.79701400928477550422712738509289920330047607421875;-0.651462251289939064946565849822945892810821533203125;1.18937770243021123661719684605486690998077392578125;-5.05484679483895593676834323559887707233428955078125]
     * Referenced by: '<S5>/b{4}'
     */
    { -1.0264423798408822, -4.2114679108357782, -2.0965749524701565,
      -0.7914617334372277, 3.0957599895470782, -1.425448919935949,
      2.7970140092847755, -0.65146225128993906, 1.1893777024302112,
      -5.0548467948389559 },

    /* Expression: -2
     * Referenced by: '<S35>/Gain'
     */
    -2.0,

    /* Expression: 1
     * Referenced by: '<S35>/one'
     */
    1.0,

    /* Expression: 2
     * Referenced by: '<S35>/Gain1'
     */
    2.0,

    /* Expression: 1
     * Referenced by: '<S35>/one1'
     */
    1.0,

    /* Expression: [-3.807809393667145325679257439333014190196990966796875;-3.338645165781727097709108420531265437602996826171875;2.1256554680048651562174200080335140228271484375;-0.045072862172447096529293020239492761902511119842529296875;0.158975370678875960184228688376606442034244537353515625;-0.199594333028209780511730286889360286295413970947265625;4.2115606854484184395914780907332897186279296875;-1.77584391875681024686173259397037327289581298828125;2.082674336344936261156135515193454921245574951171875;2.583255013232464758488049483275972306728363037109375]
     * Referenced by: '<S47>/IW{5,4}(2,:)''
     */
    { -3.8078093936671453, -3.3386451657817271, 2.1256554680048652,
      -0.045072862172447097, 0.15897537067887596, -0.19959433302820978,
      4.2115606854484184, -1.7758439187568102, 2.0826743363449363,
      2.5832550132324648 },

    /* Expression: [-0.75770691096910625095262048489530570805072784423828125;0.7517817598655245348027165164239704608917236328125]
     * Referenced by: '<S6>/b{5}'
     */
    { -0.75770691096910625, 0.75178175986552453 }
  };                                   /* Modifiable parameters */

  /* Initialize tunable parameters */
  rede_simuliink_P = rede_simuliink_P_temp;
}

/* Destructor */
rede_simuliinkModelClass::~rede_simuliinkModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_rede_simuliink_T * rede_simuliinkModelClass::getRTM()
{
  return (&rede_simuliink_M);
}
