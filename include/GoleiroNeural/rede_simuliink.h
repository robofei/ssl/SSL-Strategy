/*
 * rede_simuliink.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "rede_simuliink".
 *
 * Model version              : 1.3
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C++ source code generated on : Wed Oct 23 13:09:27 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_rede_simuliink_h_
#define RTW_HEADER_rede_simuliink_h_
#include <cmath>
#include <string.h>
#include <stddef.h>
#ifndef rede_simuliink_COMMON_INCLUDES_
# define rede_simuliink_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* rede_simuliink_COMMON_INCLUDES_ */

#include "rede_simuliink_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T posBolaX[2];                  /* '<Root>/posBolaX' */
} ExtU_rede_simuliink_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T posRoboX[2];                  /* '<Root>/posRoboX' */
} ExtY_rede_simuliink_T;

/* Parameters (default storage) */
struct P_rede_simuliink_T_ {
  real_T mapminmax_xmax[2];            /* Mask Parameter: mapminmax_xmax
                                        * Referenced by: '<S51>/range y // range x'
                                        */
  real_T mapminmax_reverse_xmax[2];    /* Mask Parameter: mapminmax_reverse_xmax
                                        * Referenced by: '<S52>/Divide by range y'
                                        */
  real_T mapminmax_xmin[2];            /* Mask Parameter: mapminmax_xmin
                                        * Referenced by:
                                        *   '<S51>/Subtract min x'
                                        *   '<S51>/range y // range x'
                                        */
  real_T mapminmax_reverse_xmin[2];    /* Mask Parameter: mapminmax_reverse_xmin
                                        * Referenced by:
                                        *   '<S52>/Add min x'
                                        *   '<S52>/Divide by range y'
                                        */
  real_T mapminmax_ymax;               /* Mask Parameter: mapminmax_ymax
                                        * Referenced by: '<S51>/range y // range x'
                                        */
  real_T mapminmax_reverse_ymax;       /* Mask Parameter: mapminmax_reverse_ymax
                                        * Referenced by: '<S52>/Divide by range y'
                                        */
  real_T mapminmax_ymin;               /* Mask Parameter: mapminmax_ymin
                                        * Referenced by:
                                        *   '<S51>/Add min y'
                                        *   '<S51>/range y // range x'
                                        */
  real_T mapminmax_reverse_ymin;       /* Mask Parameter: mapminmax_reverse_ymin
                                        * Referenced by:
                                        *   '<S52>/Subtract min y'
                                        *   '<S52>/Divide by range y'
                                        */
  real_T IW541_Value[10];              /* Expression: [0.08936453727003561275932241869668359868228435516357421875;-0.5101618503215885613855107294511981308460235595703125;0.53970556202535047152224478850257582962512969970703125;-0.182359553339748625688798711053095757961273193359375;-0.10797789193413021013867592046153731644153594970703125;-0.1785187666389378702280765764953684993088245391845703125;-0.166803214523228771071217124699614942073822021484375;-0.54162346436296815443967034298111684620380401611328125;0.61075716879408437076648397123790346086025238037109375;-0.165850898012949443671715243908693082630634307861328125]
                                        * Referenced by: '<S47>/IW{5,4}(1,:)''
                                        */
  real_T IW431_Value[7];               /* Expression: [-0.0561507078805588422110162127864896319806575775146484375;-0.9709987446803769461922684058663435280323028564453125;0.48077479700115366423318619126803241670131683349609375;7.38776876186521302969367752666585147380828857421875;-0.023324336064701987891734091817852458916604518890380859375;-1.4224665722384475241568679848569445312023162841796875;0.155620721882586632300871087863924913108348846435546875]
                                        * Referenced by: '<S34>/IW{4,3}(1,:)''
                                        */
  real_T IW321_Value[5];               /* Expression: [-0.54255423243801226274030113927437923848628997802734375;-0.5167100284729302384079119292437098920345306396484375;4.10203415039652252715995928156189620494842529296875;-4.76632334633455911898636259138584136962890625;-10.2828231693631888532536322600208222866058349609375]
                                        * Referenced by: '<S24>/IW{3,2}(1,:)''
                                        */
  real_T IW211_Value[3];               /* Expression: [-5.70340755951672928603102263878099620342254638671875;3.234744876113914369142321447725407779216766357421875;1.44370194943914054164224580745212733745574951171875]
                                        * Referenced by: '<S16>/IW{2,1}(1,:)''
                                        */
  real_T IW111_Value[2];               /* Expression: [-1.6073053984531766591459245319128967821598052978515625;1.768211022151295441773299899068661034107208251953125]
                                        * Referenced by: '<S10>/IW{1,1}(1,:)''
                                        */
  real_T IW112_Value[2];               /* Expression: [1.872504425208517897516458106110803782939910888671875;3.848673204603994424388702100259251892566680908203125]
                                        * Referenced by: '<S10>/IW{1,1}(2,:)''
                                        */
  real_T IW113_Value[2];               /* Expression: [-1.8985793255364613596469780532061122357845306396484375;2.28447114822101138287280264194123446941375732421875]
                                        * Referenced by: '<S10>/IW{1,1}(3,:)''
                                        */
  real_T b1_Value[3];                  /* Expression: [2.202044403199738820120501259225420653820037841796875;0.8473896768382978006428629669244401156902313232421875;-1.4491416617837729052808981577982194721698760986328125]
                                        * Referenced by: '<S2>/b{1}'
                                        */
  real_T Gain_Gain;                    /* Expression: -2
                                        * Referenced by: '<S11>/Gain'
                                        */
  real_T one_Value;                    /* Expression: 1
                                        * Referenced by: '<S11>/one'
                                        */
  real_T Gain1_Gain;                   /* Expression: 2
                                        * Referenced by: '<S11>/Gain1'
                                        */
  real_T one1_Value;                   /* Expression: 1
                                        * Referenced by: '<S11>/one1'
                                        */
  real_T IW212_Value[3];               /* Expression: [0.99193216556846064424490805322420783340930938720703125;-3.706758263141384901473429636098444461822509765625;0.1683802782464148550811700033591478131711483001708984375]
                                        * Referenced by: '<S16>/IW{2,1}(2,:)''
                                        */
  real_T IW213_Value[3];               /* Expression: [-0.410861834488620292216154439302044920623302459716796875;-4.96557210019962891323075382388196885585784912109375;3.725654137258348708172661645221523940563201904296875]
                                        * Referenced by: '<S16>/IW{2,1}(3,:)''
                                        */
  real_T IW214_Value[3];               /* Expression: [-0.87767798836806232287699458538554608821868896484375;-0.5519211069885423537328961174353025853633880615234375;-0.86768873062915707894404704347834922373294830322265625]
                                        * Referenced by: '<S16>/IW{2,1}(4,:)''
                                        */
  real_T IW215_Value[3];               /* Expression: [-0.84887701424847994413624974185950122773647308349609375;-4.3500023545707851013730760314501821994781494140625;5.26609110901443866481486111297272145748138427734375]
                                        * Referenced by: '<S16>/IW{2,1}(5,:)''
                                        */
  real_T b2_Value[5];                  /* Expression: [4.06983851579939237552707709255628287792205810546875;2.39770803303196000655361785902641713619232177734375;-2.204354256518528121233657657285220921039581298828125;1.6347408471291924314527932438068091869354248046875;-0.47585933576305639025605387359973974525928497314453125]
                                        * Referenced by: '<S3>/b{2}'
                                        */
  real_T Gain_Gain_c;                  /* Expression: -2
                                        * Referenced by: '<S17>/Gain'
                                        */
  real_T one_Value_d;                  /* Expression: 1
                                        * Referenced by: '<S17>/one'
                                        */
  real_T Gain1_Gain_b;                 /* Expression: 2
                                        * Referenced by: '<S17>/Gain1'
                                        */
  real_T one1_Value_f;                 /* Expression: 1
                                        * Referenced by: '<S17>/one1'
                                        */
  real_T IW322_Value[5];               /* Expression: [-3.86080370270397121856831290642730891704559326171875;-2.62873177238049127169006169424392282962799072265625;1.4116399942636272957230403335415758192539215087890625;-2.619120297758621429551340042962692677974700927734375;-2.1191519002754120037934626452624797821044921875]
                                        * Referenced by: '<S24>/IW{3,2}(2,:)''
                                        */
  real_T IW323_Value[5];               /* Expression: [0.65034541158874692445834853060659952461719512939453125;-1.7836609215233030223402010960853658616542816162109375;1.612588930526041242075052650761790573596954345703125;-1.6999593262510976909851478922064416110515594482421875;-0.92198649144857591775092942043556831777095794677734375]
                                        * Referenced by: '<S24>/IW{3,2}(3,:)''
                                        */
  real_T IW324_Value[5];               /* Expression: [-0.1707805822956424968150912491182680241763591766357421875;-1.748881562175201853648331962176598608493804931640625;-0.281049887198347392303077185715665109455585479736328125;-0.55193939094625843466701553552411496639251708984375;1.1781032959994657272062568154069595038890838623046875]
                                        * Referenced by: '<S24>/IW{3,2}(4,:)''
                                        */
  real_T IW325_Value[5];               /* Expression: [-2.077998045022251716318351100198924541473388671875;-0.08292599300509129189862278508371673524379730224609375;-0.08115792069495135485635017857930506579577922821044921875;0.6282881550875796250466009951196610927581787109375;-1.84360956124673958100856907549314200878143310546875]
                                        * Referenced by: '<S24>/IW{3,2}(5,:)''
                                        */
  real_T IW326_Value[5];               /* Expression: [0.71055458880568023172230596173903904855251312255859375;9.173138753615450724510083091445267200469970703125;1.3338333592420976714265634655021131038665771484375;2.663987810222400387516472619608975946903228759765625;-6.60311874746318583362381104961968958377838134765625]
                                        * Referenced by: '<S24>/IW{3,2}(6,:)''
                                        */
  real_T IW327_Value[5];               /* Expression: [-6.1914849166199825702960879425518214702606201171875;3.357009215085892339658357741427607834339141845703125;-0.82309969035453056651618908290402032434940338134765625;-14.51442939552739375130840926431119441986083984375;-2.695827973231632146422498408355750143527984619140625]
                                        * Referenced by: '<S24>/IW{3,2}(7,:)''
                                        */
  real_T b3_Value[7];                  /* Expression: [-1.9966870407009740251425000678864307701587677001953125;1.1682185529199256013299645928782410919666290283203125;-1.6208952508405547465741847190656699240207672119140625;0.65860856444346260740729803728754632174968719482421875;-1.131078676373210800676361031946726143360137939453125;-1.592418027194547835989624218200333416461944580078125;1.017362711463260627198224028688855469226837158203125]
                                        * Referenced by: '<S4>/b{3}'
                                        */
  real_T Gain_Gain_m;                  /* Expression: -2
                                        * Referenced by: '<S25>/Gain'
                                        */
  real_T one_Value_h;                  /* Expression: 1
                                        * Referenced by: '<S25>/one'
                                        */
  real_T Gain1_Gain_o;                 /* Expression: 2
                                        * Referenced by: '<S25>/Gain1'
                                        */
  real_T one1_Value_g;                 /* Expression: 1
                                        * Referenced by: '<S25>/one1'
                                        */
  real_T IW432_Value[7];               /* Expression: [-2.695993050862083517671408117166720330715179443359375;2.60626918741059920847646935726515948772430419921875;2.611733699902209604459812908316962420940399169921875;0.907781759158780232610297389328479766845703125;-5.02456920869407941410145213012583553791046142578125;0.314150560058161276710819720392464660108089447021484375;0.77063053824157368154601499554701149463653564453125]
                                        * Referenced by: '<S34>/IW{4,3}(2,:)''
                                        */
  real_T IW433_Value[7];               /* Expression: [3.864735788207287914275411822018213570117950439453125;3.284567192539810331908256557653658092021942138671875;2.243387722325452582339266882627271115779876708984375;-1.329062664367386847885654788115061819553375244140625;-2.16941624196750471043060315423645079135894775390625;0.2162798382018476794907968496772809885442256927490234375;-0.347720237481369986998203103212290443480014801025390625]
                                        * Referenced by: '<S34>/IW{4,3}(3,:)''
                                        */
  real_T IW434_Value[7];               /* Expression: [2.004363907785382981074917552177794277667999267578125;0.287673738858842842347485202481038868427276611328125;3.15344846094767472521880335989408195018768310546875;3.763151531030163088331619292148388922214508056640625;0.66010659411402439200600156254949979484081268310546875;0.309159495355692592877261404282762669026851654052734375;5.59886953084030469796061879605986177921295166015625]
                                        * Referenced by: '<S34>/IW{4,3}(4,:)''
                                        */
  real_T IW435_Value[7];               /* Expression: [-3.282735712910926917373899414087645709514617919921875;0.5811299926670463467104355004266835749149322509765625;-2.631609032834363315345171940862201154232025146484375;0.573123241362762936290664583793841302394866943359375;-0.137125044149172425900218286187737248837947845458984375;-1.393740230824583203883548776502721011638641357421875;-2.002809081281837766397302402765490114688873291015625]
                                        * Referenced by: '<S34>/IW{4,3}(5,:)''
                                        */
  real_T IW436_Value[7];               /* Expression: [-0.249219513429534023085665239705122075974941253662109375;-4.60994784731612039507808731286786496639251708984375;-0.1256753411512201712429970257289824075996875762939453125;-1.1594937104204616762359592030406929552555084228515625;3.383461082639399375437960770796053111553192138671875;1.8676548269360482290579739128588698804378509521484375;-1.9777570358726019872364076945814304053783416748046875]
                                        * Referenced by: '<S34>/IW{4,3}(6,:)''
                                        */
  real_T IW437_Value[7];               /* Expression: [0.21885978782135551146126317689777351915836334228515625;-0.102799953046117387156499489719863049685955047607421875;-0.2987283569398775373571197633282281458377838134765625;-0.044387563706776426986078121217360603623092174530029296875;-0.04834244417154971718009193182297167368233203887939453125;-4.39454194697359579180329092196188867092132568359375;0.2096450760915211375401412396968225948512554168701171875]
                                        * Referenced by: '<S34>/IW{4,3}(7,:)''
                                        */
  real_T IW438_Value[7];               /* Expression: [-1.08661833203726931884602890932001173496246337890625;-5.45357742204567141897086912649683654308319091796875;-0.50591172066196665735304804911720566451549530029296875;-2.255397612725072775674561853520572185516357421875;-0.86228377857073545964539107444579713046550750732421875;0.7336652372291261325898403811152093112468719482421875;0.12711982264924326369026630345615558326244354248046875]
                                        * Referenced by: '<S34>/IW{4,3}(8,:)''
                                        */
  real_T IW439_Value[7];               /* Expression: [-0.918097002004703455213530105538666248321533203125;-4.92172067143732139271605774411000311374664306640625;0.94961283862937373445589628317975439131259918212890625;1.1991992552907728697419997843098826706409454345703125;3.099298567514465307937143734307028353214263916015625;-0.247841321021264049395682604881585575640201568603515625;0.17766560938377773393170855342759750783443450927734375]
                                        * Referenced by: '<S34>/IW{4,3}(9,:)''
                                        */
  real_T IW4310_Value[7];              /* Expression: [-4.06767927345018076579208354814909398555755615234375;-4.17480191218076157610994414426386356353759765625;4.10783681172902692679826941457577049732208251953125;0.603093363920731206917480449192225933074951171875;5.66198992998527206310654946719296276569366455078125;-3.40015730038663743783899917616508901119232177734375;0.52483462234898758946854968598927371203899383544921875]
                                        * Referenced by: '<S34>/IW{4,3}(10,:)''
                                        */
  real_T b4_Value[10];                 /* Expression: [-1.026442379840882157537862440221942961215972900390625;-4.21146791083577820558048188104294240474700927734375;-2.09657495247015646100408048368990421295166015625;-0.79146173343722769910613124011433683335781097412109375;3.0957599895470782058737313491292297840118408203125;-1.425448919935949021464693942107260227203369140625;2.79701400928477550422712738509289920330047607421875;-0.651462251289939064946565849822945892810821533203125;1.18937770243021123661719684605486690998077392578125;-5.05484679483895593676834323559887707233428955078125]
                                        * Referenced by: '<S5>/b{4}'
                                        */
  real_T Gain_Gain_n;                  /* Expression: -2
                                        * Referenced by: '<S35>/Gain'
                                        */
  real_T one_Value_m;                  /* Expression: 1
                                        * Referenced by: '<S35>/one'
                                        */
  real_T Gain1_Gain_i;                 /* Expression: 2
                                        * Referenced by: '<S35>/Gain1'
                                        */
  real_T one1_Value_k;                 /* Expression: 1
                                        * Referenced by: '<S35>/one1'
                                        */
  real_T IW542_Value[10];              /* Expression: [-3.807809393667145325679257439333014190196990966796875;-3.338645165781727097709108420531265437602996826171875;2.1256554680048651562174200080335140228271484375;-0.045072862172447096529293020239492761902511119842529296875;0.158975370678875960184228688376606442034244537353515625;-0.199594333028209780511730286889360286295413970947265625;4.2115606854484184395914780907332897186279296875;-1.77584391875681024686173259397037327289581298828125;2.082674336344936261156135515193454921245574951171875;2.583255013232464758488049483275972306728363037109375]
                                        * Referenced by: '<S47>/IW{5,4}(2,:)''
                                        */
  real_T b5_Value[2];                  /* Expression: [-0.75770691096910625095262048489530570805072784423828125;0.7517817598655245348027165164239704608917236328125]
                                        * Referenced by: '<S6>/b{5}'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_rede_simuliink_T {
  const char_T *errorStatus;
};

/* Class declaration for model rede_simuliink */
class rede_simuliinkModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExtU_rede_simuliink_T rede_simuliink_U;

  /* External outputs */
  ExtY_rede_simuliink_T rede_simuliink_Y;

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  rede_simuliinkModelClass();

  /* Destructor */
  ~rede_simuliinkModelClass();

  /* Real-Time Model get method */
  RT_MODEL_rede_simuliink_T * getRTM();

  /* private data and function members */
 private:
  /* Tunable parameters */
  P_rede_simuliink_T rede_simuliink_P;

  /* Real-Time Model */
  RT_MODEL_rede_simuliink_T rede_simuliink_M;
};

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'rede_simuliink'
 * '<S1>'   : 'rede_simuliink/Feed-Forward Neural Network'
 * '<S2>'   : 'rede_simuliink/Feed-Forward Neural Network/Layer 1'
 * '<S3>'   : 'rede_simuliink/Feed-Forward Neural Network/Layer 2'
 * '<S4>'   : 'rede_simuliink/Feed-Forward Neural Network/Layer 3'
 * '<S5>'   : 'rede_simuliink/Feed-Forward Neural Network/Layer 4'
 * '<S6>'   : 'rede_simuliink/Feed-Forward Neural Network/Layer 5'
 * '<S7>'   : 'rede_simuliink/Feed-Forward Neural Network/Process Input 1'
 * '<S8>'   : 'rede_simuliink/Feed-Forward Neural Network/Process Output 1'
 * '<S9>'   : 'rede_simuliink/Feed-Forward Neural Network/Layer 1/Delays 1'
 * '<S10>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 1/IW{1,1}'
 * '<S11>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 1/tansig'
 * '<S12>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 1/IW{1,1}/dotprod1'
 * '<S13>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 1/IW{1,1}/dotprod2'
 * '<S14>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 1/IW{1,1}/dotprod3'
 * '<S15>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 2/Delays 1'
 * '<S16>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 2/LW{2,1}'
 * '<S17>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 2/tansig'
 * '<S18>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 2/LW{2,1}/dotprod1'
 * '<S19>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 2/LW{2,1}/dotprod2'
 * '<S20>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 2/LW{2,1}/dotprod3'
 * '<S21>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 2/LW{2,1}/dotprod4'
 * '<S22>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 2/LW{2,1}/dotprod5'
 * '<S23>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/Delays 1'
 * '<S24>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/LW{3,2}'
 * '<S25>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/tansig'
 * '<S26>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/LW{3,2}/dotprod1'
 * '<S27>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/LW{3,2}/dotprod2'
 * '<S28>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/LW{3,2}/dotprod3'
 * '<S29>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/LW{3,2}/dotprod4'
 * '<S30>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/LW{3,2}/dotprod5'
 * '<S31>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/LW{3,2}/dotprod6'
 * '<S32>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 3/LW{3,2}/dotprod7'
 * '<S33>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/Delays 1'
 * '<S34>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}'
 * '<S35>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/tansig'
 * '<S36>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod1'
 * '<S37>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod10'
 * '<S38>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod2'
 * '<S39>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod3'
 * '<S40>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod4'
 * '<S41>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod5'
 * '<S42>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod6'
 * '<S43>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod7'
 * '<S44>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod8'
 * '<S45>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 4/LW{4,3}/dotprod9'
 * '<S46>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 5/Delays 1'
 * '<S47>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 5/LW{5,4}'
 * '<S48>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 5/purelin'
 * '<S49>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 5/LW{5,4}/dotprod1'
 * '<S50>'  : 'rede_simuliink/Feed-Forward Neural Network/Layer 5/LW{5,4}/dotprod2'
 * '<S51>'  : 'rede_simuliink/Feed-Forward Neural Network/Process Input 1/mapminmax'
 * '<S52>'  : 'rede_simuliink/Feed-Forward Neural Network/Process Output 1/mapminmax_reverse'
 */
#endif                                 /* RTW_HEADER_rede_simuliink_h_ */
