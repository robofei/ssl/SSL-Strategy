/*
 * rede_simuliink_types.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "rede_simuliink".
 *
 * Model version              : 1.3
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C++ source code generated on : Wed Oct 23 13:09:27 2019
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_rede_simuliink_types_h_
#define RTW_HEADER_rede_simuliink_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (default storage) */
typedef struct P_rede_simuliink_T_ P_rede_simuliink_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_rede_simuliink_T RT_MODEL_rede_simuliink_T;

#endif                                 /* RTW_HEADER_rede_simuliink_types_h_ */
