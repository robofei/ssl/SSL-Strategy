/*
 * ControleFuzzy.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControleFuzzy".
 *
 * Model version              : 2.26
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Tue Oct 12 02:20:08 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "ControleFuzzy.h"
#include "ControleFuzzy_private.h"

/* Function for MATLAB Function: '<S1>/Evaluate Rule Antecedents' */
real_T ControleFuzzyModelClass::ControleFuzzy_trimf(real_T x, const real_T
  params[3])
{
  real_T y;
  y = 0.0;
  if ((params[0] != params[1]) && (params[0] < x) && (x < params[1])) {
    y = 1.0 / (params[1] - params[0]) * (x - params[0]);
  }

  if ((params[1] != params[2]) && (params[1] < x) && (x < params[2])) {
    y = 1.0 / (params[2] - params[1]) * (params[2] - x);
  }

  if (x == params[1]) {
    y = 1.0;
  }

  return y;
}

/* Function for MATLAB Function: '<S1>/Evaluate Rule Antecedents' */
real_T ControleFuzzyModelClass::ControleFuzzy_trapmf(real_T x, const real_T
  params[4])
{
  real_T b_y1;
  real_T y;
  b_y1 = 0.0;
  y = 0.0;
  if (x >= params[1]) {
    b_y1 = 1.0;
  }

  if (x < params[0]) {
    b_y1 = 0.0;
  }

  if ((params[0] <= x) && (x < params[1]) && (params[0] != params[1])) {
    b_y1 = 1.0 / (params[1] - params[0]) * (x - params[0]);
  }

  if (x <= params[2]) {
    y = 1.0;
  }

  if (x > params[3]) {
    y = 0.0;
  }

  if ((params[2] < x) && (x <= params[3]) && (params[2] != params[3])) {
    y = 1.0 / (params[3] - params[2]) * (params[3] - x);
  }

  if ((b_y1 < y) || rtIsNaN(y)) {
    y = b_y1;
  }

  return y;
}

/* Function for MATLAB Function: '<S1>/Evaluate Rule Consequents' */
void ControleFuzzyModelClass::ControleFuzzy_trimf_o(const real_T x[101], const
  real_T params[3], real_T y[101])
{
  real_T a;
  real_T b;
  real_T c;
  real_T x_0;
  int32_T i;
  a = params[0];
  b = params[1];
  c = params[2];
  for (i = 0; i < 101; i++) {
    x_0 = x[i];
    y[i] = 0.0;
    if ((a != b) && (a < x_0) && (x_0 < b)) {
      y[i] = 1.0 / (b - a) * (x_0 - a);
    }

    if ((b != c) && (b < x_0) && (x_0 < c)) {
      y[i] = 1.0 / (c - b) * (c - x_0);
    }

    if (x_0 == b) {
      y[i] = 1.0;
    }
  }
}

real_T rt_powd_snf(real_T u0, real_T u1)
{
  real_T tmp;
  real_T tmp_0;
  real_T y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else {
    tmp = std::abs(u0);
    tmp_0 = std::abs(u1);
    if (rtIsInf(u1)) {
      if (tmp == 1.0) {
        y = 1.0;
      } else if (tmp > 1.0) {
        if (u1 > 0.0) {
          y = (rtInf);
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = (rtInf);
      }
    } else if (tmp_0 == 0.0) {
      y = 1.0;
    } else if (tmp_0 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = (rtNaN);
    } else {
      y = std::pow(u0, u1);
    }
  }

  return y;
}

/* Function for MATLAB Function: '<S1>/Evaluate Rule Consequents' */
void ControleFuzzyModelClass::ControleFuzzy_gbellmf(const real_T x[101], const
  real_T params[3], real_T y[101])
{
  real_T tmp;
  int32_T i;
  for (i = 0; i < 101; i++) {
    tmp = (x[i] - params[2]) * (1.0 / params[0]);
    tmp *= tmp;
    tmp = rt_powd_snf(tmp, params[1]);
    y[i] = 1.0 / (tmp + 1.0);
  }
}

/* Function for MATLAB Function: '<S1>/Evaluate Rule Consequents' */
void ControleFuzzyModelClass::Cont_createMamdaniOutputMFCache(const real_T
  outputSamplePoints[101], real_T outputMFCache[606])
{
  static const real_T b[3] = { -0.19824924874791316, 0.48645075125208681,
    0.50745075125208683 };

  static const real_T c[3] = { 0.486, 14.2961186113655, 2.37 };

  static const real_T d[3] = { 1.5, 1.5, 2.2464859437751 };

  static const real_T e[3] = { 1.27, 23.7, -0.85193656093489145 };

  real_T tmp[101];
  real_T tmp_1[101];
  real_T tmp_2[101];
  real_T tmp_0[3];
  real_T outputSamplePoints_0;
  int32_T i;
  int8_T b_y1;
  int8_T y2;
  ControleFuzzy_trimf_o(outputSamplePoints, b, tmp);
  for (i = 0; i < 101; i++) {
    outputMFCache[6 * i] = tmp[i];
  }

  tmp_0[0] = 0.5;
  tmp_0[1] = 1.0;
  tmp_0[2] = 1.5;
  ControleFuzzy_trimf_o(outputSamplePoints, tmp_0, tmp);
  for (i = 0; i < 101; i++) {
    outputMFCache[6 * i + 1] = tmp[i];
  }

  ControleFuzzy_gbellmf(outputSamplePoints, c, tmp);
  ControleFuzzy_trimf_o(outputSamplePoints, d, tmp_1);
  ControleFuzzy_gbellmf(outputSamplePoints, e, tmp_2);
  for (i = 0; i < 101; i++) {
    outputSamplePoints_0 = outputSamplePoints[i];
    outputMFCache[6 * i + 2] = tmp[i];
    b_y1 = 0;
    y2 = 0;
    if (outputSamplePoints_0 >= -0.97913188647746241) {
      b_y1 = 1;
    }

    if (outputSamplePoints_0 < -0.97913188647746241) {
      b_y1 = 0;
    }

    if (outputSamplePoints_0 <= 0.040868113522537594) {
      y2 = 1;
    }

    if (outputSamplePoints_0 > 0.040868113522537594) {
      y2 = 0;
    }

    if (static_cast<real_T>(b_y1) < y2) {
      outputMFCache[6 * i + 3] = b_y1;
    } else {
      outputMFCache[6 * i + 3] = y2;
    }

    outputMFCache[6 * i + 4] = tmp_1[i];
    outputMFCache[6 * i + 5] = tmp_2[i];
  }
}

/* Model step function */
void ControleFuzzyModelClass::step()
{
  static const real_T g[4] = { 450.0, 600.0, 1000.0, 1375.0 };

  static const real_T h[4] = { -19.855592654424015, -19.155592654424016,
    11.644407345575985, 50.944407345575982 };

  static const real_T j[4] = { 100.0, 250.0, 1000.0, 1100.0 };

  static const real_T k[4] = { 180.0, 180.0, 300.0, 350.0 };

  static const real_T c[3] = { -37.5, 0.0, 20.0 };

  static const real_T d[3] = { 50.0, 90.0, 128.0 };

  static const real_T e[3] = { 100.0, 500.0, 500.0 };

  static const real_T f[3] = { 30.0, 152.7545909849751, 250.0 };

  static const real_T i[3] = { 169.98046744574282, 249.98046744574282,
    349.98046744574282 };

  static const real_T l[3] = { 366.7, 500.0, 500.0 };

  static const int8_T b_0[36] = { 0, 0, 0, 1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 2, 1, 2, 3, 3, 0, 0, 6, 4, 5, 0, -1, -1, -1, 0, 0, 1, 0, -1 };

  static const int8_T b_1[9] = { 1, 2, 3, 3, 1, 1, 6, 4, 5 };

  real_T rtb_aggregatedOutputs[101];
  real_T inputMFCache[14];
  real_T rtb_antecedentOutputs[9];
  real_T tmp[3];
  real_T mVal;
  real_T rtb_aggregatedOutputs_e;
  real_T rtb_antecedentOutputs_k;
  real_T sumAntecedentOutputs;
  int32_T mfIndex;
  int32_T ruleID;
  int8_T b;

  /* Outputs for Atomic SubSystem: '<Root>/Controlador' */
  /* MATLAB Function: '<S1>/Evaluate Rule Antecedents' incorporates:
   *  Inport: '<Root>/Input'
   *  Inport: '<Root>/Input1'
   *  Inport: '<Root>/Input2'
   *  Inport: '<Root>/Input3'
   */
  sumAntecedentOutputs = 0.0;
  inputMFCache[0] = ControleFuzzy_trimf(ControleFuzzy_U.desvio, c);
  tmp[0] = 15.0;
  tmp[1] = 37.5;
  tmp[2] = 60.0;
  inputMFCache[1] = ControleFuzzy_trimf(ControleFuzzy_U.desvio, tmp);
  inputMFCache[2] = ControleFuzzy_trimf(ControleFuzzy_U.desvio, d);
  tmp[0] = -68.75;
  tmp[1] = 0.0;
  tmp[2] = 68.75;
  inputMFCache[3] = ControleFuzzy_trimf(ControleFuzzy_U.deslocamento, tmp);
  inputMFCache[4] = ControleFuzzy_trimf(ControleFuzzy_U.deslocamento, e);
  inputMFCache[5] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_tot, f);
  tmp[0] = 200.0;
  tmp[1] = 350.0;
  tmp[2] = 500.0;
  inputMFCache[6] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_tot, tmp);
  inputMFCache[7] = ControleFuzzy_trapmf(ControleFuzzy_U.distancia_tot, g);
  inputMFCache[8] = ControleFuzzy_trapmf(ControleFuzzy_U.distancia_tot, h);
  inputMFCache[9] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_tot, i);
  inputMFCache[10] = ControleFuzzy_trapmf(ControleFuzzy_U.distancia_tot, j);
  inputMFCache[11] = ControleFuzzy_trapmf(ControleFuzzy_U.distancia_opo, k);
  tmp[0] = 206.7;
  tmp[1] = 340.0;
  tmp[2] = 473.3;
  inputMFCache[12] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_opo, tmp);
  inputMFCache[13] = ControleFuzzy_trimf(ControleFuzzy_U.distancia_opo, l);
  for (ruleID = 0; ruleID < 9; ruleID++) {
    rtb_antecedentOutputs_k = 1.0;
    b = b_0[ruleID];
    mfIndex = static_cast<int32_T>(std::abs(static_cast<real_T>(b)));
    if (mfIndex != 0) {
      mVal = inputMFCache[mfIndex - 1];
      if (b < 0) {
        mVal = 1.0 - mVal;
      }

      if (1.0 > mVal) {
        rtb_antecedentOutputs_k = mVal;
      }
    }

    b = b_0[ruleID + 9];
    mfIndex = static_cast<int32_T>(std::abs(static_cast<real_T>(b)));
    if (mfIndex != 0) {
      mVal = inputMFCache[mfIndex + 2];
      if (b < 0) {
        mVal = 1.0 - mVal;
      }

      if ((rtb_antecedentOutputs_k > mVal) || (rtIsNaN(rtb_antecedentOutputs_k) &&
           (!rtIsNaN(mVal)))) {
        rtb_antecedentOutputs_k = mVal;
      }
    }

    b = b_0[ruleID + 18];
    mfIndex = static_cast<int32_T>(std::abs(static_cast<real_T>(b)));
    if (mfIndex != 0) {
      mVal = inputMFCache[mfIndex + 4];
      if (b < 0) {
        mVal = 1.0 - mVal;
      }

      if ((rtb_antecedentOutputs_k > mVal) || (rtIsNaN(rtb_antecedentOutputs_k) &&
           (!rtIsNaN(mVal)))) {
        rtb_antecedentOutputs_k = mVal;
      }
    }

    b = b_0[ruleID + 27];
    mfIndex = static_cast<int32_T>(std::abs(static_cast<real_T>(b)));
    if (mfIndex != 0) {
      mVal = inputMFCache[mfIndex + 10];
      if (b < 0) {
        mVal = 1.0 - mVal;
      }

      if ((rtb_antecedentOutputs_k > mVal) || (rtIsNaN(rtb_antecedentOutputs_k) &&
           (!rtIsNaN(mVal)))) {
        rtb_antecedentOutputs_k = mVal;
      }
    }

    sumAntecedentOutputs += rtb_antecedentOutputs_k;
    rtb_antecedentOutputs[ruleID] = rtb_antecedentOutputs_k;
  }

  /* MATLAB Function: '<S1>/Evaluate Rule Consequents' incorporates:
   *  Constant: '<S1>/Output Sample Points'
   */
  std::memset(&rtb_aggregatedOutputs[0], 0, 101U * sizeof(real_T));
  Cont_createMamdaniOutputMFCache(ControleFuzzy_ConstP.OutputSamplePoints_Value,
    ControleFuzzy_B.outputMFCache);
  for (ruleID = 0; ruleID < 9; ruleID++) {
    rtb_antecedentOutputs_k = rtb_antecedentOutputs[ruleID];
    for (mfIndex = 0; mfIndex < 101; mfIndex++) {
      rtb_aggregatedOutputs_e = rtb_aggregatedOutputs[mfIndex];
      mVal = ControleFuzzy_B.outputMFCache[(6 * mfIndex + b_1[ruleID]) - 1];
      if ((mVal > rtb_antecedentOutputs_k) || (rtIsNaN(mVal) && (!rtIsNaN
            (rtb_antecedentOutputs_k)))) {
        mVal = rtb_antecedentOutputs_k;
      }

      if ((rtb_aggregatedOutputs_e < mVal) || (rtIsNaN(rtb_aggregatedOutputs_e) &&
           (!rtIsNaN(mVal)))) {
        rtb_aggregatedOutputs_e = mVal;
      }

      rtb_aggregatedOutputs[mfIndex] = rtb_aggregatedOutputs_e;
    }
  }

  /* End of MATLAB Function: '<S1>/Evaluate Rule Consequents' */

  /* MATLAB Function: '<S1>/Defuzzify Outputs' incorporates:
   *  Constant: '<S1>/Output Sample Points'
   *  MATLAB Function: '<S1>/Evaluate Rule Antecedents'
   */
  if (sumAntecedentOutputs == 0.0) {
    /* Outport: '<Root>/Output' */
    ControleFuzzy_Y.velocidade = 1.25;
  } else {
    sumAntecedentOutputs = 0.0;
    mVal = 0.0;
    for (ruleID = 0; ruleID < 101; ruleID++) {
      mVal += rtb_aggregatedOutputs[ruleID];
    }

    if (mVal == 0.0) {
      /* Outport: '<Root>/Output' */
      ControleFuzzy_Y.velocidade = 1.25;
    } else {
      for (ruleID = 0; ruleID < 101; ruleID++) {
        sumAntecedentOutputs +=
          ControleFuzzy_ConstP.OutputSamplePoints_Value[ruleID] *
          rtb_aggregatedOutputs[ruleID];
      }

      /* Outport: '<Root>/Output' incorporates:
       *  Constant: '<S1>/Output Sample Points'
       */
      ControleFuzzy_Y.velocidade = 1.0 / mVal * sumAntecedentOutputs;
    }
  }

  /* End of MATLAB Function: '<S1>/Defuzzify Outputs' */
  /* End of Outputs for SubSystem: '<Root>/Controlador' */
}

/* Model initialize function */
void ControleFuzzyModelClass::initialize()
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));
}

/* Model terminate function */
void ControleFuzzyModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
ControleFuzzyModelClass::ControleFuzzyModelClass() :
  ControleFuzzy_U(),
  ControleFuzzy_Y(),
  ControleFuzzy_B(),
  ControleFuzzy_M()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
ControleFuzzyModelClass::~ControleFuzzyModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_ControleFuzzy_T * ControleFuzzyModelClass::getRTM()
{
  return (&ControleFuzzy_M);
}
