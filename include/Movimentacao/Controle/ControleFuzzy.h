/*
 * ControleFuzzy.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControleFuzzy".
 *
 * Model version              : 2.26
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Tue Oct 12 02:20:08 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ControleFuzzy_h_
#define RTW_HEADER_ControleFuzzy_h_
#include <cmath>
#include <cstring>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "ControleFuzzy_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Block signals (default storage) */
typedef struct {
  real_T outputMFCache[606];
} B_ControleFuzzy_T;

/* Constant parameters (default storage) */
typedef struct {
  /* Expression: fis.outputSamplePoints
   * Referenced by: '<S1>/Output Sample Points'
   */
  real_T OutputSamplePoints_Value[101];
} ConstP_ControleFuzzy_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T desvio;                       /* '<Root>/Input' */
  real_T deslocamento;                 /* '<Root>/Input1' */
  real_T distancia_tot;                /* '<Root>/Input2' */
  real_T distancia_opo;                /* '<Root>/Input3' */
} ExtU_ControleFuzzy_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T velocidade;                   /* '<Root>/Output' */
} ExtY_ControleFuzzy_T;

/* Real-time Model Data Structure */
struct tag_RTM_ControleFuzzy_T {
  const char_T *errorStatus;
};

/* Constant parameters (default storage) */
extern const ConstP_ControleFuzzy_T ControleFuzzy_ConstP;

/* Class declaration for model ControleFuzzy */
class ControleFuzzyModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExtU_ControleFuzzy_T ControleFuzzy_U;

  /* External outputs */
  ExtY_ControleFuzzy_T ControleFuzzy_Y;

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  ControleFuzzyModelClass();

  /* Destructor */
  ~ControleFuzzyModelClass();

  /* Real-Time Model get method */
  RT_MODEL_ControleFuzzy_T * getRTM();

  /* private data and function members */
 private:
  /* Block signals */
  B_ControleFuzzy_T ControleFuzzy_B;

  /* Real-Time Model */
  RT_MODEL_ControleFuzzy_T ControleFuzzy_M;

  /* private member function(s) for subsystem '<Root>'*/
  real_T ControleFuzzy_trimf(real_T x, const real_T params[3]);
  real_T ControleFuzzy_trapmf(real_T x, const real_T params[4]);
  void ControleFuzzy_trimf_o(const real_T x[101], const real_T params[3], real_T
    y[101]);
  void ControleFuzzy_gbellmf(const real_T x[101], const real_T params[3], real_T
    y[101]);
  void Cont_createMamdaniOutputMFCache(const real_T outputSamplePoints[101],
    real_T outputMFCache[606]);
};

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S1>/InputConversion' : Eliminate redundant data type conversion
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ControleFuzzy'
 * '<S1>'   : 'ControleFuzzy/Controlador'
 * '<S2>'   : 'ControleFuzzy/Controlador/Defuzzify Outputs'
 * '<S3>'   : 'ControleFuzzy/Controlador/Evaluate Rule Antecedents'
 * '<S4>'   : 'ControleFuzzy/Controlador/Evaluate Rule Consequents'
 */
#endif                                 /* RTW_HEADER_ControleFuzzy_h_ */
