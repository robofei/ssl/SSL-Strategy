/*
 * ControleFuzzy_private.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControleFuzzy".
 *
 * Model version              : 2.26
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Tue Oct 12 02:20:08 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ControleFuzzy_private_h_
#define RTW_HEADER_ControleFuzzy_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"

extern real_T rt_powd_snf(real_T u0, real_T u1);

#endif                                 /* RTW_HEADER_ControleFuzzy_private_h_ */
