/*
 * ControlePosicao.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 2.4
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Wed May 11 17:01:01 2022
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#ifndef RTW_HEADER_ControlePosicao_h_
#define RTW_HEADER_ControlePosicao_h_
#include "ControlePosicao_types.h"
//#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rtwtypes.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm) ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val) ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct
{
    real_T Integrator_DSTATE;           /* '<S90>/Integrator' */
    real_T Filter_DSTATE;               /* '<S85>/Filter' */
    real_T TFPosicaoX_states[2];        /* '<Root>/TFPosicaoX' */
    real_T Integrator_DSTATE_i;         /* '<S140>/Integrator' */
    real_T Filter_DSTATE_b;             /* '<S135>/Filter' */
    real_T TFPosicaoY_states[2];        /* '<Root>/TFPosicaoY' */
    real_T Integrator_DSTATE_b;         /* '<S40>/Integrator' */
    real_T TFPosicaoAngular_states[2];  /* '<Root>/TFPosicaoAngular' */
    int8_T Integrator_PrevResetState;   /* '<S90>/Integrator' */
    int8_T Filter_PrevResetState;       /* '<S85>/Filter' */
    int8_T Integrator_PrevResetState_p; /* '<S140>/Integrator' */
    int8_T Filter_PrevResetState_o;     /* '<S135>/Filter' */
    int8_T Integrator_PrevResetState_m; /* '<S40>/Integrator' */
} DW_ControlePosicao_T;

/* External inputs (root inport signals with default storage) */
typedef struct
{
    real_T PosX;          /* '<Root>/PosX' */
    real_T PosY;          /* '<Root>/PosY' */
    real_T ReAlimX;       /* '<Root>/ReAlimX' */
    real_T ReAlimY;       /* '<Root>/ReAlimY' */
    real_T PosAngular;    /* '<Root>/PosAngular' */
    real_T ReAlimAngular; /* '<Root>/ReAlimAngular' */
} ExtU_ControlePosicao_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct
{
    real_T SinalControleX; /* '<Root>/SinalControleX' */
    real_T SinalControleY; /* '<Root>/SinalControleY' */
    real_T PosRoboX;       /* '<Root>/PosRoboX' */
    real_T PosRoboY;       /* '<Root>/PosRoboY' */
    real_T ErroX;          /* '<Root>/ErroX' */
    real_T ErroY;          /* '<Root>/ErroY' */
    real_T SinalControleW; /* '<Root>/SinalControleW' */
    real_T PosRoboAngular; /* '<Root>/PosRoboAngular' */
    real_T ErroW;          /* '<Root>/ErroW' */
} ExtY_ControlePosicao_T;

/* Real-time Model Data Structure */
struct tag_RTM_ControlePosicao_T
{
    const char_T* errorStatus;
};

/* Class declaration for model ControlePosicao */
class ControlePosicaoModelClass
{
    /* public data and function members */
public:
    double kPx;
    double kPy;
    double kPw;
    double kIx;
    double kIy;
    double kIw;
    double kDx;
    double kDy;
    double kDw;
    /* model initialize function */
    void initialize();

    /* model step function */
    void step();

    /* model terminate function */
    void terminate();

    /* Constructor */
    ControlePosicaoModelClass();

    /* Destructor */
    ~ControlePosicaoModelClass();

    /* Root-level structure-based inputs set method */

    /* Root inports set method */
    void setExternalInputs(
        const ExtU_ControlePosicao_T* pExtU_ControlePosicao_T)
    {
        ControlePosicao_U = *pExtU_ControlePosicao_T;
    }

    /* Root-level structure-based outputs get method */

    /* Root outports get method */
    const ExtY_ControlePosicao_T& getExternalOutputs() const
    {
        return ControlePosicao_Y;
    }

    /* Real-Time Model get method */
    RT_MODEL_ControlePosicao_T* getRTM();

    /* private data and function members */
private:
    /* Block states */
    DW_ControlePosicao_T ControlePosicao_DW;

    /* External inputs */
    ExtU_ControlePosicao_T ControlePosicao_U;

    /* External outputs */
    ExtY_ControlePosicao_T ControlePosicao_Y;

    /* Real-Time Model */
    RT_MODEL_ControlePosicao_T ControlePosicao_M;

    /* private member function(s) for subsystem '<S4>/If Action Subsystem'*/
    static void ControlePosic_IfActionSubsystem(real_T rtu_In1,
                                                real_T* rty_Out1);
};

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Scope' : Unused code path elimination
 * Block '<Root>/Scope1' : Unused code path elimination
 * Block '<Root>/Scope2' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ControlePosicao'
 * '<S1>'   : 'ControlePosicao/ControladorAngular'
 * '<S2>'   : 'ControlePosicao/ControladorX'
 * '<S3>'   : 'ControlePosicao/ControladorY'
 * '<S4>'   : 'ControlePosicao/PID_Reset'
 * '<S5>'   : 'ControlePosicao/PID_Reset1'
 * '<S6>'   : 'ControlePosicao/PID_Reset2'
 * '<S7>'   : 'ControlePosicao/ControladorAngular/Anti-windup'
 * '<S8>'   : 'ControlePosicao/ControladorAngular/D Gain'
 * '<S9>'   : 'ControlePosicao/ControladorAngular/Filter'
 * '<S10>'  : 'ControlePosicao/ControladorAngular/Filter ICs'
 * '<S11>'  : 'ControlePosicao/ControladorAngular/I Gain'
 * '<S12>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain'
 * '<S13>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain Fdbk'
 * '<S14>'  : 'ControlePosicao/ControladorAngular/Integrator'
 * '<S15>'  : 'ControlePosicao/ControladorAngular/Integrator ICs'
 * '<S16>'  : 'ControlePosicao/ControladorAngular/N Copy'
 * '<S17>'  : 'ControlePosicao/ControladorAngular/N Gain'
 * '<S18>'  : 'ControlePosicao/ControladorAngular/P Copy'
 * '<S19>'  : 'ControlePosicao/ControladorAngular/Parallel P Gain'
 * '<S20>'  : 'ControlePosicao/ControladorAngular/Reset Signal'
 * '<S21>'  : 'ControlePosicao/ControladorAngular/Saturation'
 * '<S22>'  : 'ControlePosicao/ControladorAngular/Saturation Fdbk'
 * '<S23>'  : 'ControlePosicao/ControladorAngular/Sum'
 * '<S24>'  : 'ControlePosicao/ControladorAngular/Sum Fdbk'
 * '<S25>'  : 'ControlePosicao/ControladorAngular/Tracking Mode'
 * '<S26>'  : 'ControlePosicao/ControladorAngular/Tracking Mode Sum'
 * '<S27>'  : 'ControlePosicao/ControladorAngular/Tsamp - Integral'
 * '<S28>'  : 'ControlePosicao/ControladorAngular/Tsamp - Ngain'
 * '<S29>'  : 'ControlePosicao/ControladorAngular/postSat Signal'
 * '<S30>'  : 'ControlePosicao/ControladorAngular/preSat Signal'
 * '<S31>'  : 'ControlePosicao/ControladorAngular/Anti-windup/Disc. Clamping
 * Parallel'
 * '<S32>'  : 'ControlePosicao/ControladorAngular/Anti-windup/Disc. Clamping
 * Parallel/Dead Zone'
 * '<S33>'  : 'ControlePosicao/ControladorAngular/Anti-windup/Disc. Clamping
 * Parallel/Dead Zone/Enabled'
 * '<S34>'  : 'ControlePosicao/ControladorAngular/D Gain/Disabled'
 * '<S35>'  : 'ControlePosicao/ControladorAngular/Filter/Disabled'
 * '<S36>'  : 'ControlePosicao/ControladorAngular/Filter ICs/Disabled'
 * '<S37>'  : 'ControlePosicao/ControladorAngular/I Gain/Internal Parameters'
 * '<S38>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain/Passthrough'
 * '<S39>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain Fdbk/Disabled'
 * '<S40>'  : 'ControlePosicao/ControladorAngular/Integrator/Discrete'
 * '<S41>'  : 'ControlePosicao/ControladorAngular/Integrator ICs/Internal IC'
 * '<S42>'  : 'ControlePosicao/ControladorAngular/N Copy/Disabled wSignal
 * Specification'
 * '<S43>'  : 'ControlePosicao/ControladorAngular/N Gain/Disabled'
 * '<S44>'  : 'ControlePosicao/ControladorAngular/P Copy/Disabled'
 * '<S45>'  : 'ControlePosicao/ControladorAngular/Parallel P Gain/Internal
 * Parameters'
 * '<S46>'  : 'ControlePosicao/ControladorAngular/Reset Signal/External Reset'
 * '<S47>'  : 'ControlePosicao/ControladorAngular/Saturation/Enabled'
 * '<S48>'  : 'ControlePosicao/ControladorAngular/Saturation Fdbk/Disabled'
 * '<S49>'  : 'ControlePosicao/ControladorAngular/Sum/Sum_PI'
 * '<S50>'  : 'ControlePosicao/ControladorAngular/Sum Fdbk/Disabled'
 * '<S51>'  : 'ControlePosicao/ControladorAngular/Tracking Mode/Disabled'
 * '<S52>'  : 'ControlePosicao/ControladorAngular/Tracking Mode Sum/Passthrough'
 * '<S53>'  : 'ControlePosicao/ControladorAngular/Tsamp - Integral/Passthrough'
 * '<S54>'  : 'ControlePosicao/ControladorAngular/Tsamp - Ngain/Passthrough'
 * '<S55>'  : 'ControlePosicao/ControladorAngular/postSat Signal/Forward_Path'
 * '<S56>'  : 'ControlePosicao/ControladorAngular/preSat Signal/Forward_Path'
 * '<S57>'  : 'ControlePosicao/ControladorX/Anti-windup'
 * '<S58>'  : 'ControlePosicao/ControladorX/D Gain'
 * '<S59>'  : 'ControlePosicao/ControladorX/Filter'
 * '<S60>'  : 'ControlePosicao/ControladorX/Filter ICs'
 * '<S61>'  : 'ControlePosicao/ControladorX/I Gain'
 * '<S62>'  : 'ControlePosicao/ControladorX/Ideal P Gain'
 * '<S63>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk'
 * '<S64>'  : 'ControlePosicao/ControladorX/Integrator'
 * '<S65>'  : 'ControlePosicao/ControladorX/Integrator ICs'
 * '<S66>'  : 'ControlePosicao/ControladorX/N Copy'
 * '<S67>'  : 'ControlePosicao/ControladorX/N Gain'
 * '<S68>'  : 'ControlePosicao/ControladorX/P Copy'
 * '<S69>'  : 'ControlePosicao/ControladorX/Parallel P Gain'
 * '<S70>'  : 'ControlePosicao/ControladorX/Reset Signal'
 * '<S71>'  : 'ControlePosicao/ControladorX/Saturation'
 * '<S72>'  : 'ControlePosicao/ControladorX/Saturation Fdbk'
 * '<S73>'  : 'ControlePosicao/ControladorX/Sum'
 * '<S74>'  : 'ControlePosicao/ControladorX/Sum Fdbk'
 * '<S75>'  : 'ControlePosicao/ControladorX/Tracking Mode'
 * '<S76>'  : 'ControlePosicao/ControladorX/Tracking Mode Sum'
 * '<S77>'  : 'ControlePosicao/ControladorX/Tsamp - Integral'
 * '<S78>'  : 'ControlePosicao/ControladorX/Tsamp - Ngain'
 * '<S79>'  : 'ControlePosicao/ControladorX/postSat Signal'
 * '<S80>'  : 'ControlePosicao/ControladorX/preSat Signal'
 * '<S81>'  : 'ControlePosicao/ControladorX/Anti-windup/Disc. Clamping Parallel'
 * '<S82>'  : 'ControlePosicao/ControladorX/Anti-windup/Disc. Clamping
 * Parallel/Dead Zone'
 * '<S83>'  : 'ControlePosicao/ControladorX/Anti-windup/Disc. Clamping
 * Parallel/Dead Zone/Enabled'
 * '<S84>'  : 'ControlePosicao/ControladorX/D Gain/Internal Parameters'
 * '<S85>'  : 'ControlePosicao/ControladorX/Filter/Disc. Forward Euler Filter'
 * '<S86>'  : 'ControlePosicao/ControladorX/Filter ICs/Internal IC - Filter'
 * '<S87>'  : 'ControlePosicao/ControladorX/I Gain/Internal Parameters'
 * '<S88>'  : 'ControlePosicao/ControladorX/Ideal P Gain/Passthrough'
 * '<S89>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk/Disabled'
 * '<S90>'  : 'ControlePosicao/ControladorX/Integrator/Discrete'
 * '<S91>'  : 'ControlePosicao/ControladorX/Integrator ICs/Internal IC'
 * '<S92>'  : 'ControlePosicao/ControladorX/N Copy/Disabled'
 * '<S93>'  : 'ControlePosicao/ControladorX/N Gain/Internal Parameters'
 * '<S94>'  : 'ControlePosicao/ControladorX/P Copy/Disabled'
 * '<S95>'  : 'ControlePosicao/ControladorX/Parallel P Gain/Internal Parameters'
 * '<S96>'  : 'ControlePosicao/ControladorX/Reset Signal/External Reset'
 * '<S97>'  : 'ControlePosicao/ControladorX/Saturation/Enabled'
 * '<S98>'  : 'ControlePosicao/ControladorX/Saturation Fdbk/Disabled'
 * '<S99>'  : 'ControlePosicao/ControladorX/Sum/Sum_PID'
 * '<S100>' : 'ControlePosicao/ControladorX/Sum Fdbk/Disabled'
 * '<S101>' : 'ControlePosicao/ControladorX/Tracking Mode/Disabled'
 * '<S102>' : 'ControlePosicao/ControladorX/Tracking Mode Sum/Passthrough'
 * '<S103>' : 'ControlePosicao/ControladorX/Tsamp - Integral/Passthrough'
 * '<S104>' : 'ControlePosicao/ControladorX/Tsamp - Ngain/Passthrough'
 * '<S105>' : 'ControlePosicao/ControladorX/postSat Signal/Forward_Path'
 * '<S106>' : 'ControlePosicao/ControladorX/preSat Signal/Forward_Path'
 * '<S107>' : 'ControlePosicao/ControladorY/Anti-windup'
 * '<S108>' : 'ControlePosicao/ControladorY/D Gain'
 * '<S109>' : 'ControlePosicao/ControladorY/Filter'
 * '<S110>' : 'ControlePosicao/ControladorY/Filter ICs'
 * '<S111>' : 'ControlePosicao/ControladorY/I Gain'
 * '<S112>' : 'ControlePosicao/ControladorY/Ideal P Gain'
 * '<S113>' : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk'
 * '<S114>' : 'ControlePosicao/ControladorY/Integrator'
 * '<S115>' : 'ControlePosicao/ControladorY/Integrator ICs'
 * '<S116>' : 'ControlePosicao/ControladorY/N Copy'
 * '<S117>' : 'ControlePosicao/ControladorY/N Gain'
 * '<S118>' : 'ControlePosicao/ControladorY/P Copy'
 * '<S119>' : 'ControlePosicao/ControladorY/Parallel P Gain'
 * '<S120>' : 'ControlePosicao/ControladorY/Reset Signal'
 * '<S121>' : 'ControlePosicao/ControladorY/Saturation'
 * '<S122>' : 'ControlePosicao/ControladorY/Saturation Fdbk'
 * '<S123>' : 'ControlePosicao/ControladorY/Sum'
 * '<S124>' : 'ControlePosicao/ControladorY/Sum Fdbk'
 * '<S125>' : 'ControlePosicao/ControladorY/Tracking Mode'
 * '<S126>' : 'ControlePosicao/ControladorY/Tracking Mode Sum'
 * '<S127>' : 'ControlePosicao/ControladorY/Tsamp - Integral'
 * '<S128>' : 'ControlePosicao/ControladorY/Tsamp - Ngain'
 * '<S129>' : 'ControlePosicao/ControladorY/postSat Signal'
 * '<S130>' : 'ControlePosicao/ControladorY/preSat Signal'
 * '<S131>' : 'ControlePosicao/ControladorY/Anti-windup/Disc. Clamping Parallel'
 * '<S132>' : 'ControlePosicao/ControladorY/Anti-windup/Disc. Clamping
 * Parallel/Dead Zone'
 * '<S133>' : 'ControlePosicao/ControladorY/Anti-windup/Disc. Clamping
 * Parallel/Dead Zone/Enabled'
 * '<S134>' : 'ControlePosicao/ControladorY/D Gain/Internal Parameters'
 * '<S135>' : 'ControlePosicao/ControladorY/Filter/Disc. Forward Euler Filter'
 * '<S136>' : 'ControlePosicao/ControladorY/Filter ICs/Internal IC - Filter'
 * '<S137>' : 'ControlePosicao/ControladorY/I Gain/Internal Parameters'
 * '<S138>' : 'ControlePosicao/ControladorY/Ideal P Gain/Passthrough'
 * '<S139>' : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk/Disabled'
 * '<S140>' : 'ControlePosicao/ControladorY/Integrator/Discrete'
 * '<S141>' : 'ControlePosicao/ControladorY/Integrator ICs/Internal IC'
 * '<S142>' : 'ControlePosicao/ControladorY/N Copy/Disabled'
 * '<S143>' : 'ControlePosicao/ControladorY/N Gain/Internal Parameters'
 * '<S144>' : 'ControlePosicao/ControladorY/P Copy/Disabled'
 * '<S145>' : 'ControlePosicao/ControladorY/Parallel P Gain/Internal Parameters'
 * '<S146>' : 'ControlePosicao/ControladorY/Reset Signal/External Reset'
 * '<S147>' : 'ControlePosicao/ControladorY/Saturation/Enabled'
 * '<S148>' : 'ControlePosicao/ControladorY/Saturation Fdbk/Disabled'
 * '<S149>' : 'ControlePosicao/ControladorY/Sum/Sum_PID'
 * '<S150>' : 'ControlePosicao/ControladorY/Sum Fdbk/Disabled'
 * '<S151>' : 'ControlePosicao/ControladorY/Tracking Mode/Disabled'
 * '<S152>' : 'ControlePosicao/ControladorY/Tracking Mode Sum/Passthrough'
 * '<S153>' : 'ControlePosicao/ControladorY/Tsamp - Integral/Passthrough'
 * '<S154>' : 'ControlePosicao/ControladorY/Tsamp - Ngain/Passthrough'
 * '<S155>' : 'ControlePosicao/ControladorY/postSat Signal/Forward_Path'
 * '<S156>' : 'ControlePosicao/ControladorY/preSat Signal/Forward_Path'
 * '<S157>' : 'ControlePosicao/PID_Reset/If Action Subsystem'
 * '<S158>' : 'ControlePosicao/PID_Reset/If Action Subsystem1'
 * '<S159>' : 'ControlePosicao/PID_Reset1/If Action Subsystem'
 * '<S160>' : 'ControlePosicao/PID_Reset1/If Action Subsystem1'
 * '<S161>' : 'ControlePosicao/PID_Reset2/If Action Subsystem'
 * '<S162>' : 'ControlePosicao/PID_Reset2/If Action Subsystem1'
 */
#endif /* RTW_HEADER_ControlePosicao_h_ */
