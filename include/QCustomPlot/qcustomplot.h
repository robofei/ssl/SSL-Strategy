/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011-2016 Emanuel Eichhammer                            **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Emanuel Eichhammer                                   **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 13.09.16                                             **
**          Version: 2.0.0-beta                                           **
****************************************************************************/

#ifndef QCUSTOMPLOT_H
#define QCUSTOMPLOT_H

#include <QtCore/qglobal.h>

// some Qt version/configuration dependent macros to include or exclude certain code paths:
#ifdef QCUSTOMPLOT_USE_OPENGL
#  if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#    define QCP_OPENGL_PBUFFER
#  else
#    define QCP_OPENGL_FBO
#  endif
#  if QT_VERSION >= QT_VERSION_CHECK(5, 3, 0)
#    define QCP_OPENGL_OFFSCREENSURFACE
#  endif
#endif

#if QT_VERSION >= QT_VERSION_CHECK(5, 4, 0)
  #define QCP_DEVICEPIXELRATIO_SUPPORTED
#endif

#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QSharedPointer>
#include <QtCore/QTimer>
#include <QtGui/QPainter>
#include <QtGui/QPaintEvent>
#include <QtGui/QMouseEvent>
#include <QtGui/QWheelEvent>
#include <QtGui/QPixmap>
#include <QtCore/QVector>
#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtCore/QMultiMap>
#include <QtCore/QFlags>
#include <QtCore/QDebug>
#include <QtCore/QStack>
#include <QtCore/QCache>
#include <QtCore/QMargins>
#include <qmath.h>
#include <limits>
#include <algorithm>
#ifdef QCP_OPENGL_FBO
#  include <QtGui/QOpenGLContext>
#  include <QtGui/QOpenGLFramebufferObject>
#  ifdef QCP_OPENGL_OFFSCREENSURFACE
#    include <QtGui/QOffscreenSurface>
#  else
#    include <QtGui/QWindow>
#  endif
#endif
#ifdef QCP_OPENGL_PBUFFER
#  include <QtOpenGL/QGLPixelBuffer>
#endif
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#  include <qnumeric.h>
#  include <QtGui/QWidget>
#  include <QtGui/QPrinter>
#  include <QtGui/QPrintEngine>
#else
#  include <QtNumeric>
#  include <QtWidgets/QWidget>
#  include <QtPrintSupport/QtPrintSupport>
#endif

class QCPPainter;
class QCustomPlot;
class QCPLayerable;
class QCPLayoutElement;
class QCPLayout;
class QCPAxis;
class QCPAxisRect;
class QCPAxisPainterPrivate;
class QCPAbstractPlottable;
class QCPGraph;
class QCPAbstractItem;
class QCPPlottableInterface1D;
class QCPLegend;
class QCPItemPosition;
class QCPLayer;
class QCPAbstractLegendItem;
class QCPSelectionRect;
class QCPColorMap;
class QCPColorScale;
class QCPBars;

/* including file 'src/global.h', size 16131                                 */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

// decl definitions for shared library compilation/usage:
#if defined(QCUSTOMPLOT_COMPILE_LIBRARY)
#  define QCP_LIB_DECL Q_DECL_EXPORT
#elif defined(QCUSTOMPLOT_USE_LIBRARY)
#  define QCP_LIB_DECL Q_DECL_IMPORT
#else
#  define QCP_LIB_DECL
#endif

// define empty macro for Q_DECL_OVERRIDE if it doesn't exist (Qt < 5)
#ifndef Q_DECL_OVERRIDE
#  define Q_DECL_OVERRIDE
#endif

/*!
  The QCP Namespace contains general enums, QFlags and functions used throughout the QCustomPlot
  library.
  
  It provides QMetaObject-based reflection of its enums and flags via \a QCP::staticMetaObject.
*/
#ifndef Q_MOC_RUN
namespace QCP {
#else
class QCP { // when in moc-run, make it look like a class, so we get Q_GADGET, Q_ENUMS/Q_FLAGS features in namespace
  Q_GADGET
  Q_ENUMS(ExportPen)
  Q_ENUMS(ResolutionUnit)
  Q_ENUMS(SignDomain)
  Q_ENUMS(MarginSide)
  Q_FLAGS(MarginSides)
  Q_ENUMS(AntialiasedElement)
  Q_FLAGS(AntialiasedElements)
  Q_ENUMS(PlottingHint)
  Q_FLAGS(PlottingHints)
  Q_ENUMS(Interaction)
  Q_FLAGS(Interactions)
  Q_ENUMS(SelectionRectMode)
  Q_ENUMS(SelectionType)
public:
#endif

/*!
  Defines the different units in which the image resolution can be specified in the export
  functions.

  \see QCustomPlot::savePng, QCustomPlot::saveJpg, QCustomPlot::saveBmp, QCustomPlot::saveRastered
*/
enum ResolutionUnit { ruDotsPerMeter       ///< Resolution is given in dots per meter (dpm)
                      ,ruDotsPerCentimeter ///< Resolution is given in dots per centimeter (dpcm)
                      ,ruDotsPerInch       ///< Resolution is given in dots per inch (DPI/PPI)
                    };

/*!
  Defines how cosmetic pens (pens with numerical width 0) are handled during export.

  \see QCustomPlot::savePdf
*/
enum ExportPen { epNoCosmetic     ///< Cosmetic pens are converted to pens with pixel width 1 when exporting
                 ,epAllowCosmetic ///< Cosmetic pens are exported normally (e.g. in PDF exports, cosmetic pens always appear as 1 pixel on screen, independent of viewer zoom level)
               };

/*!
  Represents negative and positive sign domain, e.g. for passing to \ref
  QCPAbstractPlottable::getKeyRange and \ref QCPAbstractPlottable::getValueRange.
  
  This is primarily needed when working with logarithmic axis scales, since only one of the sign
  domains can be visible at a time.
*/
enum SignDomain { sdNegative  ///< The negative sign domain, i.e. numbers smaller than zero
                  ,sdBoth     ///< Both sign domains, including zero, i.e. all numbers
                  ,sdPositive ///< The positive sign domain, i.e. numbers greater than zero
                };

/*!
  Defines the sides of a rectangular entity to which margins can be applied.
  
  \see QCPLayoutElement::setAutoMargins, QCPAxisRect::setAutoMargins
*/
enum MarginSide { msLeft     = 0x01 ///< <tt>0x01</tt> left margin
                  ,msRight   = 0x02 ///< <tt>0x02</tt> right margin
                  ,msTop     = 0x04 ///< <tt>0x04</tt> top margin
                  ,msBottom  = 0x08 ///< <tt>0x08</tt> bottom margin
                  ,msAll     = 0xFF ///< <tt>0xFF</tt> all margins
                  ,msNone    = 0x00 ///< <tt>0x00</tt> no margin
                };
/**
 * @brief
 *
 */
Q_DECLARE_FLAGS(MarginSides, MarginSide)

/*!
  Defines what objects of a plot can be forcibly drawn antialiased/not antialiased. If an object is
  neither forcibly drawn antialiased nor forcibly drawn not antialiased, it is up to the respective
  element how it is drawn. Typically it provides a \a setAntialiased function for this.
  
  \c AntialiasedElements is a flag of or-combined elements of this enum type.
  
  \see QCustomPlot::setAntialiasedElements, QCustomPlot::setNotAntialiasedElements
*/
enum AntialiasedElement { aeAxes           = 0x0001 ///< <tt>0x0001</tt> Axis base line and tick marks
                          ,aeGrid          = 0x0002 ///< <tt>0x0002</tt> Grid lines
                          ,aeSubGrid       = 0x0004 ///< <tt>0x0004</tt> Sub grid lines
                          ,aeLegend        = 0x0008 ///< <tt>0x0008</tt> Legend box
                          ,aeLegendItems   = 0x0010 ///< <tt>0x0010</tt> Legend items
                          ,aePlottables    = 0x0020 ///< <tt>0x0020</tt> Main lines of plottables
                          ,aeItems         = 0x0040 ///< <tt>0x0040</tt> Main lines of items
                          ,aeScatters      = 0x0080 ///< <tt>0x0080</tt> Scatter symbols of plottables (excluding scatter symbols of type ssPixmap)
                          ,aeFills         = 0x0100 ///< <tt>0x0100</tt> Borders of fills (e.g. under or between graphs)
                          ,aeZeroLine      = 0x0200 ///< <tt>0x0200</tt> Zero-lines, see \ref QCPGrid::setZeroLinePen
                          ,aeOther         = 0x8000 ///< <tt>0x8000</tt> Other elements that don't fit into any of the existing categories
                          ,aeAll           = 0xFFFF ///< <tt>0xFFFF</tt> All elements
                          ,aeNone          = 0x0000 ///< <tt>0x0000</tt> No elements
                        };
/**
 * @brief
 *
 */
Q_DECLARE_FLAGS(AntialiasedElements, AntialiasedElement)

/*!
  Defines plotting hints that control various aspects of the quality and speed of plotting.
  
  \see QCustomPlot::setPlottingHints
*/
enum PlottingHint { phNone              = 0x000 ///< <tt>0x000</tt> No hints are set
                    ,phFastPolylines    = 0x001 ///< <tt>0x001</tt> Graph/Curve lines are drawn with a faster method. This reduces the quality especially of the line segment
                                                ///<                joins, thus is most effective for pen sizes larger than 1. It is only used for solid line pens.
                    ,phImmediateRefresh = 0x002 ///< <tt>0x002</tt> causes an immediate repaint() instead of a soft update() when QCustomPlot::replot() is called with parameter \ref QCustomPlot::rpRefreshHint.
                                                ///<                This is set by default to prevent the plot from freezing on fast consecutive replots (e.g. user drags ranges with mouse).
                    ,phCacheLabels      = 0x004 ///< <tt>0x004</tt> axis (tick) labels will be cached as pixmaps, increasing replot performance.
                  };
/**
 * @brief
 *
 */
Q_DECLARE_FLAGS(PlottingHints, PlottingHint)

/*!
  Defines the mouse interactions possible with QCustomPlot.
  
  \c Interactions is a flag of or-combined elements of this enum type.
  
  \see QCustomPlot::setInteractions
*/
enum Interaction { iRangeDrag         = 0x001 ///< <tt>0x001</tt> Axis ranges are draggable (see \ref QCPAxisRect::setRangeDrag, \ref QCPAxisRect::setRangeDragAxes)
                   ,iRangeZoom        = 0x002 ///< <tt>0x002</tt> Axis ranges are zoomable with the mouse wheel (see \ref QCPAxisRect::setRangeZoom, \ref QCPAxisRect::setRangeZoomAxes)
                   ,iMultiSelect      = 0x004 ///< <tt>0x004</tt> The user can select multiple objects by holding the modifier set by \ref QCustomPlot::setMultiSelectModifier while clicking
                   ,iSelectPlottables = 0x008 ///< <tt>0x008</tt> Plottables are selectable (e.g. graphs, curves, bars,... see QCPAbstractPlottable)
                   ,iSelectAxes       = 0x010 ///< <tt>0x010</tt> Axes are selectable (or parts of them, see QCPAxis::setSelectableParts)
                   ,iSelectLegend     = 0x020 ///< <tt>0x020</tt> Legends are selectable (or their child items, see QCPLegend::setSelectableParts)
                   ,iSelectItems      = 0x040 ///< <tt>0x040</tt> Items are selectable (Rectangles, Arrows, Textitems, etc. see \ref QCPAbstractItem)
                   ,iSelectOther      = 0x080 ///< <tt>0x080</tt> All other objects are selectable (e.g. your own derived layerables, other layout elements,...)
                 };
/**
 * @brief
 *
 */
Q_DECLARE_FLAGS(Interactions, Interaction)

/*!
  Defines the behaviour of the selection rect.
  
  \see QCustomPlot::setSelectionRectMode, QCustomPlot::selectionRect, QCPSelectionRect
*/
enum SelectionRectMode { srmNone    ///< The selection rect is disabled, and all mouse events are forwarded to the underlying objects, e.g. for axis range dragging
                         ,srmZoom   ///< When dragging the mouse, a selection rect becomes active. Upon releasing, the axes that are currently set as range zoom axes (\ref QCPAxisRect::setRangeZoomAxes) will have their ranges zoomed accordingly.
                         ,srmSelect ///< When dragging the mouse, a selection rect becomes active. Upon releasing, plottable data points that were within the selection rect are selected, if the plottable's selectability setting permits. (See  \ref dataselection "data selection mechanism" for details.)
                         ,srmCustom ///< When dragging the mouse, a selection rect becomes active. It is the programmer's responsibility to connect according slots to the selection rect's signals (e.g. \ref QCPSelectionRect::accepted) in order to process the user interaction.
                       };

/*!
  Defines the different ways a plottable can be selected. These images show the effect of the
  different selection types, when the indicated selection rect was dragged:
  
  <center>
  <table>
  <tr>
    <td>\image html selectiontype-none.png stNone</td>
    <td>\image html selectiontype-whole.png stWhole</td>
    <td>\image html selectiontype-singledata.png stSingleData</td>
    <td>\image html selectiontype-datarange.png stDataRange</td>
    <td>\image html selectiontype-multipledataranges.png stMultipleDataRanges</td>
  </tr>
  </table>
  </center>
  
  \see QCPAbstractPlottable::setSelectable, QCPDataSelection::enforceType
*/
enum SelectionType { stNone                ///< The plottable is not selectable
                     ,stWhole              ///< Selection behaves like \ref stMultipleDataRanges, but if there are any data points selected, the entire plottable is drawn as selected.
                     ,stSingleData         ///< One individual data point can be selected at a time
                     ,stDataRange          ///< Multiple contiguous data points (a data range) can be selected
                     ,stMultipleDataRanges ///< Any combination of data points/ranges can be selected
                    };

/*! \internal
  
  Returns whether the specified \a value is considered an invalid data value for plottables (i.e.
  is \e nan or \e +/-inf). This function is used to check data validity upon replots, when the
  compiler flag \c QCUSTOMPLOT_CHECK_DATA is set.
*/
inline bool isInvalidData(double value)
{
  return qIsNaN(value) || qIsInf(value);
}

/*! \internal
  \overload
  
  Checks two arguments instead of one.
*/
inline bool isInvalidData(double value1, double value2)
{
  return isInvalidData(value1) || isInvalidData(value2);
}

/*! \internal
  
  Sets the specified \a side of \a margins to \a value
  
  \see getMarginValue
*/
inline void setMarginValue(QMargins &margins, QCP::MarginSide side, int value)
{
  switch (side)
  {
    case QCP::msLeft: margins.setLeft(value); break;
    case QCP::msRight: margins.setRight(value); break;
    case QCP::msTop: margins.setTop(value); break;
    case QCP::msBottom: margins.setBottom(value); break;
    case QCP::msAll: margins = QMargins(value, value, value, value); break;
    default: break;
  }
}

/*! \internal
  
  Returns the value of the specified \a side of \a margins. If \a side is \ref QCP::msNone or
  \ref QCP::msAll, returns 0.
  
  \see setMarginValue
*/
inline int getMarginValue(const QMargins &margins, QCP::MarginSide side)
{
  switch (side)
  {
    case QCP::msLeft: return margins.left();
    case QCP::msRight: return margins.right();
    case QCP::msTop: return margins.top();
    case QCP::msBottom: return margins.bottom();
    default: break;
  }
  return 0;
}


extern const QMetaObject staticMetaObject; // in moc-run we create a static meta object for QCP "fake" object. This line is the link to it via QCP::staticMetaObject in normal operation as namespace /**< TODO: describe */

} // end of namespace QCP
Q_DECLARE_OPERATORS_FOR_FLAGS(QCP::AntialiasedElements)
Q_DECLARE_OPERATORS_FOR_FLAGS(QCP::PlottingHints)
Q_DECLARE_OPERATORS_FOR_FLAGS(QCP::MarginSides)
Q_DECLARE_OPERATORS_FOR_FLAGS(QCP::Interactions)
Q_DECLARE_METATYPE(QCP::ExportPen)
Q_DECLARE_METATYPE(QCP::ResolutionUnit)
Q_DECLARE_METATYPE(QCP::SignDomain)
Q_DECLARE_METATYPE(QCP::MarginSide)
Q_DECLARE_METATYPE(QCP::AntialiasedElement)
Q_DECLARE_METATYPE(QCP::PlottingHint)
Q_DECLARE_METATYPE(QCP::Interaction)
Q_DECLARE_METATYPE(QCP::SelectionRectMode)
Q_DECLARE_METATYPE(QCP::SelectionType)

/* end of 'src/global.h' */


/* including file 'src/vector2d.h', size 4928                                */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPVector2D
{
public:
  /**
   * @brief
   *
   */
  QCPVector2D();
  /**
   * @brief
   *
   * @param x
   * @param y
   */
  QCPVector2D(double x, double y);
  /**
   * @brief
   *
   * @param point
   */
  QCPVector2D(const QPoint &point);
  /**
   * @brief
   *
   * @param point
   */
  QCPVector2D(const QPointF &point);
  
  // getters:
  /**
   * @brief
   *
   * @return double
   */
  double x() const { return mX; }
  /**
   * @brief
   *
   * @return double
   */
  double y() const { return mY; }
  /**
   * @brief
   *
   * @return double
   */
  double &rx() { return mX; }
  /**
   * @brief
   *
   * @return double
   */
  double &ry() { return mY; }
  
  // setters:
  /**
   * @brief
   *
   * @param x
   */
  void setX(double x) { mX = x; }
  /**
   * @brief
   *
   * @param y
   */
  void setY(double y) { mY = y; }
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return double
   */
  double length() const { return qSqrt(mX*mX+mY*mY); }
  /**
   * @brief
   *
   * @return double
   */
  double lengthSquared() const { return mX*mX+mY*mY; }
  /**
   * @brief
   *
   * @return QPoint
   */
  QPoint toPoint() const { return QPoint(mX, mY); }
  /**
   * @brief
   *
   * @return QPointF
   */
  QPointF toPointF() const { return QPointF(mX, mY); }
  
  /**
   * @brief
   *
   * @return bool
   */
  bool isNull() const { return qIsNull(mX) && qIsNull(mY); }
  /**
   * @brief
   *
   */
  void normalize();
  /**
   * @brief
   *
   * @return QCPVector2D
   */
  QCPVector2D normalized() const;
  /**
   * @brief
   *
   * @return QCPVector2D
   */
  QCPVector2D perpendicular() const { return QCPVector2D(-mY, mX); }
  /**
   * @brief
   *
   * @param vec
   * @return double
   */
  double dot(const QCPVector2D &vec) const { return mX*vec.mX+mY*vec.mY; }
  /**
   * @brief
   *
   * @param start
   * @param end
   * @return double
   */
  double distanceSquaredToLine(const QCPVector2D &start, const QCPVector2D &end) const;
  /**
   * @brief
   *
   * @param line
   * @return double
   */
  double distanceSquaredToLine(const QLineF &line) const;
  /**
   * @brief
   *
   * @param base
   * @param direction
   * @return double
   */
  double distanceToStraightLine(const QCPVector2D &base, const QCPVector2D &direction) const;
  
  /**
   * @brief
   *
   * @param factor
   * @return QCPVector2D &operator
   */
  QCPVector2D &operator*=(double factor);
  /**
   * @brief
   *
   * @param divisor
   * @return QCPVector2D &operator
   */
  QCPVector2D &operator/=(double divisor);
  /**
   * @brief
   *
   * @param vector
   * @return QCPVector2D &operator
   */
  QCPVector2D &operator+=(const QCPVector2D &vector);
  /**
   * @brief
   *
   * @param vector
   * @return QCPVector2D &operator
   */
  QCPVector2D &operator-=(const QCPVector2D &vector);
  
private:
  // property members:
  double mX, mY; /**< TODO: describe */
  
  /**
   * @brief
   *
   * @param factor
   * @param vec
   * @return const QCPVector2D operator
   */
  friend inline const QCPVector2D operator*(double factor, const QCPVector2D &vec);
  /**
   * @brief
   *
   * @param vec
   * @param factor
   * @return const QCPVector2D operator
   */
  friend inline const QCPVector2D operator*(const QCPVector2D &vec, double factor);
  /**
   * @brief
   *
   * @param vec
   * @param divisor
   * @return const QCPVector2D operator
   */
  friend inline const QCPVector2D operator/(const QCPVector2D &vec, double divisor);
  /**
   * @brief
   *
   * @param vec1
   * @param vec2
   * @return const QCPVector2D operator
   */
  friend inline const QCPVector2D operator+(const QCPVector2D &vec1, const QCPVector2D &vec2);
  /**
   * @brief
   *
   * @param vec1
   * @param vec2
   * @return const QCPVector2D operator
   */
  friend inline const QCPVector2D operator-(const QCPVector2D &vec1, const QCPVector2D &vec2);
  /**
   * @brief
   *
   * @param vec
   * @return const QCPVector2D operator
   */
  friend inline const QCPVector2D operator-(const QCPVector2D &vec);
};
Q_DECLARE_TYPEINFO(QCPVector2D, Q_MOVABLE_TYPE);

/**
 * @brief
 *
 * @param factor
 * @param vec
 * @return const QCPVector2D operator
 */
inline const QCPVector2D operator*(double factor, const QCPVector2D &vec) { return QCPVector2D(vec.mX*factor, vec.mY*factor); }
/**
 * @brief
 *
 * @param vec
 * @param factor
 * @return const QCPVector2D operator
 */
inline const QCPVector2D operator*(const QCPVector2D &vec, double factor) { return QCPVector2D(vec.mX*factor, vec.mY*factor); }
/**
 * @brief
 *
 * @param vec
 * @param divisor
 * @return const QCPVector2D operator
 */
inline const QCPVector2D operator/(const QCPVector2D &vec, double divisor) { return QCPVector2D(vec.mX/divisor, vec.mY/divisor); }
/**
 * @brief
 *
 * @param vec1
 * @param vec2
 * @return const QCPVector2D operator
 */
inline const QCPVector2D operator+(const QCPVector2D &vec1, const QCPVector2D &vec2) { return QCPVector2D(vec1.mX+vec2.mX, vec1.mY+vec2.mY); }
/**
 * @brief
 *
 * @param vec1
 * @param vec2
 * @return const QCPVector2D operator
 */
inline const QCPVector2D operator-(const QCPVector2D &vec1, const QCPVector2D &vec2) { return QCPVector2D(vec1.mX-vec2.mX, vec1.mY-vec2.mY); }
/**
 * @brief
 *
 * @param vec
 * @return const QCPVector2D operator
 */
inline const QCPVector2D operator-(const QCPVector2D &vec) { return QCPVector2D(-vec.mX, -vec.mY); }

/*! \relates QCPVector2D

  Prints \a vec in a human readable format to the qDebug output.
*/
inline QDebug operator<< (QDebug d, const QCPVector2D &vec)
{
    d.nospace() << "QCPVector2D(" << vec.x() << ", " << vec.y() << ")";
    return d.space();
}

/* end of 'src/vector2d.h' */


/* including file 'src/painter.h', size 4035                                 */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPPainter : public QPainter
{
  Q_GADGET
public:
  /*!
    Defines special modes the painter can operate in. They disable or enable certain subsets of features/fixes/workarounds,
    depending on whether they are wanted on the respective output device.
  */
  enum PainterMode { pmDefault       = 0x00   ///< <tt>0x00</tt> Default mode for painting on screen devices
                     ,pmVectorized   = 0x01   ///< <tt>0x01</tt> Mode for vectorized painting (e.g. PDF export). For example, this prevents some antialiasing fixes.
                     ,pmNoCaching    = 0x02   ///< <tt>0x02</tt> Mode for all sorts of exports (e.g. PNG, PDF,...). For example, this prevents using cached pixmap labels
                     ,pmNonCosmetic  = 0x04   ///< <tt>0x04</tt> Turns pen widths 0 to 1, i.e. disables cosmetic pens. (A cosmetic pen is always drawn with width 1 pixel in the vector image/pdf viewer, independent of zoom.)
                   };
  Q_ENUMS(PainterMode)
  Q_FLAGS(PainterModes)
  /**
   * @brief
   *
   */
  Q_DECLARE_FLAGS(PainterModes, PainterMode)
  
  /**
   * @brief
   *
   */
  QCPPainter();
  /**
   * @brief
   *
   * @param device
   */
  explicit QCPPainter(QPaintDevice *device);
  
  // getters:
  /**
   * @brief
   *
   * @return bool
   */
  bool antialiasing() const { return testRenderHint(QPainter::Antialiasing); }
  /**
   * @brief
   *
   * @return PainterModes
   */
  PainterModes modes() const { return mModes; }

  // setters:
  /**
   * @brief
   *
   * @param enabled
   */
  void setAntialiasing(bool enabled);
  /**
   * @brief
   *
   * @param mode
   * @param enabled
   */
  void setMode(PainterMode mode, bool enabled=true);
  /**
   * @brief
   *
   * @param modes
   */
  void setModes(PainterModes modes);

  // methods hiding non-virtual base class functions (QPainter bug workarounds):
  /**
   * @brief
   *
   * @param device
   * @return bool
   */
  bool begin(QPaintDevice *device);
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param color
   */
  void setPen(const QColor &color);
  /**
   * @brief
   *
   * @param penStyle
   */
  void setPen(Qt::PenStyle penStyle);
  /**
   * @brief
   *
   * @param line
   */
  void drawLine(const QLineF &line);
  /**
   * @brief
   *
   * @param p1
   * @param p2
   */
  void drawLine(const QPointF &p1, const QPointF &p2) {drawLine(QLineF(p1, p2));}
  /**
   * @brief
   *
   */
  void save();
  /**
   * @brief
   *
   */
  void restore();
  
  // non-virtual methods:
  /**
   * @brief
   *
   */
  void makeNonCosmetic();
  
protected:
  // property members:
  PainterModes mModes; /**< TODO: describe */
  bool mIsAntialiasing; /**< TODO: describe */
  
  // non-property members:
  QStack<bool> mAntialiasingStack; /**< TODO: describe */
};
Q_DECLARE_OPERATORS_FOR_FLAGS(QCPPainter::PainterModes)
Q_DECLARE_METATYPE(QCPPainter::PainterMode)

/* end of 'src/painter.h' */


/* including file 'src/paintbuffer.h', size 4958                             */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAbstractPaintBuffer
{
public:
  /**
   * @brief
   *
   * @param size
   * @param devicePixelRatio
   */
  explicit QCPAbstractPaintBuffer(const QSize &size, double devicePixelRatio);
  /**
   * @brief
   *
   */
  virtual ~QCPAbstractPaintBuffer();
  
  // getters:
  /**
   * @brief
   *
   * @return QSize
   */
  QSize size() const { return mSize; }
  /**
   * @brief
   *
   * @return bool
   */
  bool invalidated() const { return mInvalidated; }
  /**
   * @brief
   *
   * @return double
   */
  double devicePixelRatio() const { return mDevicePixelRatio; }
  
  // setters:
  /**
   * @brief
   *
   * @param size
   */
  void setSize(const QSize &size);
  /**
   * @brief
   *
   * @param invalidated
   */
  void setInvalidated(bool invalidated=true);
  /**
   * @brief
   *
   * @param ratio
   */
  void setDevicePixelRatio(double ratio);
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @return QCPPainter
   */
  virtual QCPPainter *startPainting() = 0;
  /**
   * @brief
   *
   */
  virtual void donePainting() {}
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) const = 0;
  /**
   * @brief
   *
   * @param color
   */
  virtual void clear(const QColor &color) = 0;
  
protected:
  // property members:
  QSize mSize; /**< TODO: describe */
  double mDevicePixelRatio; /**< TODO: describe */
  
  // non-property members:
  bool mInvalidated; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   */
  virtual void reallocateBuffer() = 0;
};


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPPaintBufferPixmap : public QCPAbstractPaintBuffer
{
public:
  /**
   * @brief
   *
   * @param size
   * @param devicePixelRatio
   */
  explicit QCPPaintBufferPixmap(const QSize &size, double devicePixelRatio);
  /**
   * @brief
   *
   */
  virtual ~QCPPaintBufferPixmap();
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @return QCPPainter
   */
  virtual QCPPainter *startPainting() Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param color
   */
  void clear(const QColor &color) Q_DECL_OVERRIDE;
  
protected:
  // non-property members:
  QPixmap mBuffer; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   */
  virtual void reallocateBuffer() Q_DECL_OVERRIDE;
};


#ifdef QCP_OPENGL_PBUFFER
class QCP_LIB_DECL QCPPaintBufferGlPbuffer : public QCPAbstractPaintBuffer
{
public:
  explicit QCPPaintBufferGlPbuffer(const QSize &size, double devicePixelRatio, int multisamples);
  virtual ~QCPPaintBufferGlPbuffer();
  
  // reimplemented virtual methods:
  virtual QCPPainter *startPainting() Q_DECL_OVERRIDE;
  virtual void draw(QCPPainter *painter) const Q_DECL_OVERRIDE;
  void clear(const QColor &color) Q_DECL_OVERRIDE;
  
protected:
  // non-property members:
  QGLPixelBuffer *mGlPBuffer;
  int mMultisamples;
  
  // reimplemented virtual methods:
  virtual void reallocateBuffer() Q_DECL_OVERRIDE;
};
#endif // QCP_OPENGL_PBUFFER


#ifdef QCP_OPENGL_FBO
class QCP_LIB_DECL QCPPaintBufferGlFbo : public QCPAbstractPaintBuffer
{
public:
  explicit QCPPaintBufferGlFbo(const QSize &size, double devicePixelRatio, QWeakPointer<QOpenGLContext> glContext, QWeakPointer<QOpenGLPaintDevice> glPaintDevice);
  virtual ~QCPPaintBufferGlFbo();
  
  // reimplemented virtual methods:
  virtual QCPPainter *startPainting() Q_DECL_OVERRIDE;
  virtual void donePainting() Q_DECL_OVERRIDE;
  virtual void draw(QCPPainter *painter) const Q_DECL_OVERRIDE;
  void clear(const QColor &color) Q_DECL_OVERRIDE;
  
protected:
  // non-property members:
  QWeakPointer<QOpenGLContext> mGlContext;
  QWeakPointer<QOpenGLPaintDevice> mGlPaintDevice;
  QOpenGLFramebufferObject *mGlFrameBuffer;
  
  // reimplemented virtual methods:
  virtual void reallocateBuffer() Q_DECL_OVERRIDE;
};
#endif // QCP_OPENGL_FBO

/* end of 'src/paintbuffer.h' */


/* including file 'src/layer.h', size 6885                                   */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPLayer : public QObject
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QCustomPlot* parentPlot READ parentPlot)
  Q_PROPERTY(QString name READ name)
  Q_PROPERTY(int index READ index)
  Q_PROPERTY(QList<QCPLayerable*> children READ children)
  Q_PROPERTY(bool visible READ visible WRITE setVisible)
  Q_PROPERTY(LayerMode mode READ mode WRITE setMode)
  /// \endcond
public:
  
  /*!
    Defines the different rendering modes of a layer. Depending on the mode, certain layers can be
    replotted individually, without the need to replot (possibly complex) layerables on other
    layers.

    \see setMode
  */
  enum LayerMode { lmLogical   ///< Layer is used only for rendering order, and shares paint buffer with all other adjacent logical layers.
                   ,lmBuffered ///< Layer has its own paint buffer and may be replotted individually (see \ref replot).
                 };
  Q_ENUMS(LayerMode)
  
  /**
   * @brief
   *
   * @param parentPlot
   * @param layerName
   */
  QCPLayer(QCustomPlot* parentPlot, const QString &layerName);
  /**
   * @brief
   *
   */
  virtual ~QCPLayer();
  
  // getters:
  /**
   * @brief
   *
   * @return QCustomPlot
   */
  QCustomPlot *parentPlot() const { return mParentPlot; }
  /**
   * @brief
   *
   * @return QString
   */
  QString name() const { return mName; }
  /**
   * @brief
   *
   * @return int
   */
  int index() const { return mIndex; }
  /**
   * @brief
   *
   * @return QList<QCPLayerable *>
   */
  QList<QCPLayerable*> children() const { return mChildren; }
  /**
   * @brief
   *
   * @return bool
   */
  bool visible() const { return mVisible; }
  /**
   * @brief
   *
   * @return LayerMode
   */
  LayerMode mode() const { return mMode; }
  
  // setters:
  /**
   * @brief
   *
   * @param visible
   */
  void setVisible(bool visible);
  /**
   * @brief
   *
   * @param mode
   */
  void setMode(LayerMode mode);
  
  // non-virtual methods:
  /**
   * @brief
   *
   */
  void replot();
  
protected:
  // property members:
  QCustomPlot *mParentPlot; /**< TODO: describe */
  QString mName; /**< TODO: describe */
  int mIndex; /**< TODO: describe */
  QList<QCPLayerable*> mChildren; /**< TODO: describe */
  bool mVisible; /**< TODO: describe */
  LayerMode mMode; /**< TODO: describe */
  
  // non-property members:
  QWeakPointer<QCPAbstractPaintBuffer> mPaintBuffer; /**< TODO: describe */
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  void draw(QCPPainter *painter);
  /**
   * @brief
   *
   */
  void drawToPaintBuffer();
  /**
   * @brief
   *
   * @param layerable
   * @param prepend
   */
  void addChild(QCPLayerable *layerable, bool prepend);
  /**
   * @brief
   *
   * @param layerable
   */
  void removeChild(QCPLayerable *layerable);
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPLayer)
  
  friend class QCustomPlot;
  friend class QCPLayerable;
};
Q_DECLARE_METATYPE(QCPLayer::LayerMode)

/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPLayerable : public QObject
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(bool visible READ visible WRITE setVisible)
  Q_PROPERTY(QCustomPlot* parentPlot READ parentPlot)
  Q_PROPERTY(QCPLayerable* parentLayerable READ parentLayerable)
  Q_PROPERTY(QCPLayer* layer READ layer WRITE setLayer NOTIFY layerChanged)
  Q_PROPERTY(bool antialiased READ antialiased WRITE setAntialiased)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param plot
   * @param targetLayer
   * @param parentLayerable
   */
  QCPLayerable(QCustomPlot *plot, QString targetLayer=QString(), QCPLayerable *parentLayerable=0);
  /**
   * @brief
   *
   */
  virtual ~QCPLayerable();
  
  // getters:
  /**
   * @brief
   *
   * @return bool
   */
  bool visible() const { return mVisible; }
  /**
   * @brief
   *
   * @return QCustomPlot
   */
  QCustomPlot *parentPlot() const { return mParentPlot; }
  /**
   * @brief
   *
   * @return QCPLayerable
   */
  QCPLayerable *parentLayerable() const { return mParentLayerable.data(); }
  /**
   * @brief
   *
   * @return QCPLayer
   */
  QCPLayer *layer() const { return mLayer; }
  /**
   * @brief
   *
   * @return bool
   */
  bool antialiased() const { return mAntialiased; }
  
  // setters:
  /**
   * @brief
   *
   * @param on
   */
  void setVisible(bool on);
  /**
   * @brief
   *
   * @param layer
   * @return bool
   */
  Q_SLOT bool setLayer(QCPLayer *layer);
  /**
   * @brief
   *
   * @param layerName
   * @return bool
   */
  bool setLayer(const QString &layerName);
  /**
   * @brief
   *
   * @param enabled
   */
  void setAntialiased(bool enabled);
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const;

  // non-property methods:
  /**
   * @brief
   *
   * @return bool
   */
  bool realVisibility() const;
  
signals:
  /**
   * @brief
   *
   * @param newLayer
   */
  void layerChanged(QCPLayer *newLayer);
  
protected:
  // property members:
  bool mVisible; /**< TODO: describe */
  QCustomPlot *mParentPlot; /**< TODO: describe */
  QPointer<QCPLayerable> mParentLayerable; /**< TODO: describe */
  QCPLayer *mLayer; /**< TODO: describe */
  bool mAntialiased; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param parentPlot
   */
  virtual void parentPlotInitialized(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   * @return QCP::Interaction
   */
  virtual QCP::Interaction selectionCategory() const;
  /**
   * @brief
   *
   * @return QRect
   */
  virtual QRect clipRect() const;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const = 0;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) = 0;
  // selection events:
  /**
   * @brief
   *
   * @param event
   * @param additive
   * @param details
   * @param selectionStateChanged
   */
  virtual void selectEvent(QMouseEvent *event, bool additive, const QVariant &details, bool *selectionStateChanged);
  /**
   * @brief
   *
   * @param selectionStateChanged
   */
  virtual void deselectEvent(bool *selectionStateChanged);
  // low-level mouse events:
  /**
   * @brief
   *
   * @param event
   * @param details
   */
  virtual void mousePressEvent(QMouseEvent *event, const QVariant &details);
  /**
   * @brief
   *
   * @param event
   * @param startPos
   */
  virtual void mouseMoveEvent(QMouseEvent *event, const QPointF &startPos);
  /**
   * @brief
   *
   * @param event
   * @param startPos
   */
  virtual void mouseReleaseEvent(QMouseEvent *event, const QPointF &startPos);
  /**
   * @brief
   *
   * @param event
   * @param details
   */
  virtual void mouseDoubleClickEvent(QMouseEvent *event, const QVariant &details);
  /**
   * @brief
   *
   * @param event
   */
  virtual void wheelEvent(QWheelEvent *event);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param parentPlot
   */
  void initializeParentPlot(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   * @param parentLayerable
   */
  void setParentLayerable(QCPLayerable* parentLayerable);
  /**
   * @brief
   *
   * @param layer
   * @param prepend
   * @return bool
   */
  bool moveToLayer(QCPLayer *layer, bool prepend);
  /**
   * @brief
   *
   * @param painter
   * @param localAntialiased
   * @param overrideElement
   */
  void applyAntialiasingHint(QCPPainter *painter, bool localAntialiased, QCP::AntialiasedElement overrideElement) const;
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPLayerable)
  
  friend class QCustomPlot;
  friend class QCPLayer;
  friend class QCPAxisRect;
};

/* end of 'src/layer.h' */


/* including file 'src/axis/range.h', size 5280                              */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPRange
{
public:
  double lower, upper; /**< TODO: describe */
  
  /**
   * @brief
   *
   */
  QCPRange();
  /**
   * @brief
   *
   * @param lower
   * @param upper
   */
  QCPRange(double lower, double upper);
  
  /**
   * @brief
   *
   * @param other
   * @return bool operator
   */
  bool operator==(const QCPRange& other) const { return lower == other.lower && upper == other.upper; }
  /**
   * @brief
   *
   * @param other
   * @return bool operator
   */
  bool operator!=(const QCPRange& other) const { return !(*this == other); }
  
  /**
   * @brief
   *
   * @param value
   * @return QCPRange &operator
   */
  QCPRange &operator+=(const double& value) { lower+=value; upper+=value; return *this; }
  /**
   * @brief
   *
   * @param value
   * @return QCPRange &operator
   */
  QCPRange &operator-=(const double& value) { lower-=value; upper-=value; return *this; }
  /**
   * @brief
   *
   * @param value
   * @return QCPRange &operator
   */
  QCPRange &operator*=(const double& value) { lower*=value; upper*=value; return *this; }
  /**
   * @brief
   *
   * @param value
   * @return QCPRange &operator
   */
  QCPRange &operator/=(const double& value) { lower/=value; upper/=value; return *this; }
  /**
   * @brief
   *
   * @param
   * @param double
   * @return const QCPRange operator
   */
  friend inline const QCPRange operator+(const QCPRange&, double);
  /**
   * @brief
   *
   * @param double
   * @param
   * @return const QCPRange operator
   */
  friend inline const QCPRange operator+(double, const QCPRange&);
  /**
   * @brief
   *
   * @param range
   * @param value
   * @return const QCPRange operator
   */
  friend inline const QCPRange operator-(const QCPRange& range, double value);
  /**
   * @brief
   *
   * @param range
   * @param value
   * @return const QCPRange operator
   */
  friend inline const QCPRange operator*(const QCPRange& range, double value);
  /**
   * @brief
   *
   * @param value
   * @param range
   * @return const QCPRange operator
   */
  friend inline const QCPRange operator*(double value, const QCPRange& range);
  /**
   * @brief
   *
   * @param range
   * @param value
   * @return const QCPRange operator
   */
  friend inline const QCPRange operator/(const QCPRange& range, double value);
  
  /**
   * @brief
   *
   * @return double
   */
  double size() const { return upper-lower; }
  /**
   * @brief
   *
   * @return double
   */
  double center() const { return (upper+lower)*0.5; }
  /**
   * @brief
   *
   */
  void normalize() { if (lower > upper) qSwap(lower, upper); }
  /**
   * @brief
   *
   * @param otherRange
   */
  void expand(const QCPRange &otherRange);
  /**
   * @brief
   *
   * @param includeCoord
   */
  void expand(double includeCoord);
  /**
   * @brief
   *
   * @param otherRange
   * @return QCPRange
   */
  QCPRange expanded(const QCPRange &otherRange) const;
  /**
   * @brief
   *
   * @param includeCoord
   * @return QCPRange
   */
  QCPRange expanded(double includeCoord) const;
  /**
   * @brief
   *
   * @param lowerBound
   * @param upperBound
   * @return QCPRange
   */
  QCPRange bounded(double lowerBound, double upperBound) const;
  /**
   * @brief
   *
   * @return QCPRange
   */
  QCPRange sanitizedForLogScale() const;
  /**
   * @brief
   *
   * @return QCPRange
   */
  QCPRange sanitizedForLinScale() const;
  /**
   * @brief
   *
   * @param value
   * @return bool
   */
  bool contains(double value) const { return value >= lower && value <= upper; }
  
  /**
   * @brief
   *
   * @param lower
   * @param upper
   * @return bool
   */
  static bool validRange(double lower, double upper);
  /**
   * @brief
   *
   * @param range
   * @return bool
   */
  static bool validRange(const QCPRange &range);
  static const double minRange; /**< TODO: describe */
  static const double maxRange; /**< TODO: describe */
  
};
Q_DECLARE_TYPEINFO(QCPRange, Q_MOVABLE_TYPE);

/*! \relates QCPRange

  Prints \a range in a human readable format to the qDebug output.
*/
inline QDebug operator<< (QDebug d, const QCPRange &range)
{
    d.nospace() << "QCPRange(" << range.lower << ", " << range.upper << ")";
    return d.space();
}

/*!
  Adds \a value to both boundaries of the range.
*/
inline const QCPRange operator+(const QCPRange& range, double value)
{
  QCPRange result(range);
  result += value;
  return result;
}

/*!
  Adds \a value to both boundaries of the range.
*/
inline const QCPRange operator+(double value, const QCPRange& range)
{
  QCPRange result(range);
  result += value;
  return result;
}

/*!
  Subtracts \a value from both boundaries of the range.
*/
inline const QCPRange operator-(const QCPRange& range, double value)
{
  QCPRange result(range);
  result -= value;
  return result;
}

/*!
  Multiplies both boundaries of the range by \a value.
*/
inline const QCPRange operator*(const QCPRange& range, double value)
{
  QCPRange result(range);
  result *= value;
  return result;
}

/*!
  Multiplies both boundaries of the range by \a value.
*/
inline const QCPRange operator*(double value, const QCPRange& range)
{
  QCPRange result(range);
  result *= value;
  return result;
}

/*!
  Divides both boundaries of the range by \a value.
*/
inline const QCPRange operator/(const QCPRange& range, double value)
{
  QCPRange result(range);
  result /= value;
  return result;
}

/* end of 'src/axis/range.h' */


/* including file 'src/selection.h', size 8579                               */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPDataRange
{
public:
  /**
   * @brief
   *
   */
  QCPDataRange();
  /**
   * @brief
   *
   * @param begin
   * @param end
   */
  QCPDataRange(int begin, int end);
  
  /**
   * @brief
   *
   * @param other
   * @return bool operator
   */
  bool operator==(const QCPDataRange& other) const { return mBegin == other.mBegin && mEnd == other.mEnd; }
  /**
   * @brief
   *
   * @param other
   * @return bool operator
   */
  bool operator!=(const QCPDataRange& other) const { return !(*this == other); }
  
  // getters:
  /**
   * @brief
   *
   * @return int
   */
  int begin() const { return mBegin; }
  /**
   * @brief
   *
   * @return int
   */
  int end() const { return mEnd; }
  /**
   * @brief
   *
   * @return int
   */
  int size() const { return mEnd-mBegin; }
  /**
   * @brief
   *
   * @return int
   */
  int length() const { return size(); }
  
  // setters:
  /**
   * @brief
   *
   * @param begin
   */
  void setBegin(int begin) { mBegin = begin; }
  /**
   * @brief
   *
   * @param end
   */
  void setEnd(int end)  { mEnd = end; }
  
  // non-property methods:
  /**
   * @brief
   *
   * @return bool
   */
  bool isValid() const { return (mEnd >= mBegin) && (mBegin >= 0); }
  /**
   * @brief
   *
   * @return bool
   */
  bool isEmpty() const { return length() == 0; }
  /**
   * @brief
   *
   * @param other
   * @return QCPDataRange
   */
  QCPDataRange bounded(const QCPDataRange &other) const;
  /**
   * @brief
   *
   * @param other
   * @return QCPDataRange
   */
  QCPDataRange expanded(const QCPDataRange &other) const;
  /**
   * @brief
   *
   * @param other
   * @return QCPDataRange
   */
  QCPDataRange intersection(const QCPDataRange &other) const;
  /**
   * @brief
   *
   * @param changeBegin
   * @param changeEnd
   * @return QCPDataRange
   */
  QCPDataRange adjusted(int changeBegin, int changeEnd) const { return QCPDataRange(mBegin+changeBegin, mEnd+changeEnd); }
  /**
   * @brief
   *
   * @param other
   * @return bool
   */
  bool intersects(const QCPDataRange &other) const;
  /**
   * @brief
   *
   * @param other
   * @return bool
   */
  bool contains(const QCPDataRange &other) const;
  
private:
  // property members:
  int mBegin, mEnd; /**< TODO: describe */

};
Q_DECLARE_TYPEINFO(QCPDataRange, Q_MOVABLE_TYPE);


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPDataSelection
{
public:
  /**
   * @brief
   *
   */
  explicit QCPDataSelection();
  /**
   * @brief
   *
   * @param range
   */
  explicit QCPDataSelection(const QCPDataRange &range);
  
  /**
   * @brief
   *
   * @param other
   * @return bool operator
   */
  bool operator==(const QCPDataSelection& other) const;
  /**
   * @brief
   *
   * @param other
   * @return bool operator
   */
  bool operator!=(const QCPDataSelection& other) const { return !(*this == other); }
  /**
   * @brief
   *
   * @param other
   * @return QCPDataSelection &operator
   */
  QCPDataSelection &operator+=(const QCPDataSelection& other);
  /**
   * @brief
   *
   * @param other
   * @return QCPDataSelection &operator
   */
  QCPDataSelection &operator+=(const QCPDataRange& other);
  /**
   * @brief
   *
   * @param other
   * @return QCPDataSelection &operator
   */
  QCPDataSelection &operator-=(const QCPDataSelection& other);
  /**
   * @brief
   *
   * @param other
   * @return QCPDataSelection &operator
   */
  QCPDataSelection &operator-=(const QCPDataRange& other);
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return const QCPDataSelection operator
   */
  friend inline const QCPDataSelection operator+(const QCPDataSelection& a, const QCPDataSelection& b);
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return const QCPDataSelection operator
   */
  friend inline const QCPDataSelection operator+(const QCPDataRange& a, const QCPDataSelection& b);
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return const QCPDataSelection operator
   */
  friend inline const QCPDataSelection operator+(const QCPDataSelection& a, const QCPDataRange& b);
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return const QCPDataSelection operator
   */
  friend inline const QCPDataSelection operator+(const QCPDataRange& a, const QCPDataRange& b);
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return const QCPDataSelection operator
   */
  friend inline const QCPDataSelection operator-(const QCPDataSelection& a, const QCPDataSelection& b);
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return const QCPDataSelection operator
   */
  friend inline const QCPDataSelection operator-(const QCPDataRange& a, const QCPDataSelection& b);
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return const QCPDataSelection operator
   */
  friend inline const QCPDataSelection operator-(const QCPDataSelection& a, const QCPDataRange& b);
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return const QCPDataSelection operator
   */
  friend inline const QCPDataSelection operator-(const QCPDataRange& a, const QCPDataRange& b);
  
  // getters:
  /**
   * @brief
   *
   * @return int
   */
  int dataRangeCount() const { return mDataRanges.size(); }
  /**
   * @brief
   *
   * @return int
   */
  int dataPointCount() const;
  /**
   * @brief
   *
   * @param index
   * @return QCPDataRange
   */
  QCPDataRange dataRange(int index=0) const;
  /**
   * @brief
   *
   * @return QList<QCPDataRange>
   */
  QList<QCPDataRange> dataRanges() const { return mDataRanges; }
  /**
   * @brief
   *
   * @return QCPDataRange
   */
  QCPDataRange span() const;
  
  // non-property methods:
  /**
   * @brief
   *
   * @param dataRange
   * @param simplify
   */
  void addDataRange(const QCPDataRange &dataRange, bool simplify=true);
  /**
   * @brief
   *
   */
  void clear();
  /**
   * @brief
   *
   * @return bool
   */
  bool isEmpty() const { return mDataRanges.isEmpty(); }
  /**
   * @brief
   *
   */
  void simplify();
  /**
   * @brief
   *
   * @param type
   */
  void enforceType(QCP::SelectionType type);
  /**
   * @brief
   *
   * @param other
   * @return bool
   */
  bool contains(const QCPDataSelection &other) const;
  /**
   * @brief
   *
   * @param other
   * @return QCPDataSelection
   */
  QCPDataSelection intersection(const QCPDataRange &other) const;
  /**
   * @brief
   *
   * @param other
   * @return QCPDataSelection
   */
  QCPDataSelection intersection(const QCPDataSelection &other) const;
  /**
   * @brief
   *
   * @param outerRange
   * @return QCPDataSelection
   */
  QCPDataSelection inverse(const QCPDataRange &outerRange) const;
  
private:
  // property members:
  QList<QCPDataRange> mDataRanges; /**< TODO: describe */
  
  /**
   * @brief
   *
   * @param a
   * @param b
   * @return bool
   */
  inline static bool lessThanDataRangeBegin(const QCPDataRange &a, const QCPDataRange &b) { return a.begin() < b.begin(); }
};
Q_DECLARE_METATYPE(QCPDataSelection)


/*!
  Return a \ref QCPDataSelection with the data points in \a a joined with the data points in \a b.
  The resulting data selection is already simplified (see \ref QCPDataSelection::simplify).
*/
inline const QCPDataSelection operator+(const QCPDataSelection& a, const QCPDataSelection& b)
{
  QCPDataSelection result(a);
  result += b;
  return result;
}

/*!
  Return a \ref QCPDataSelection with the data points in \a a joined with the data points in \a b.
  The resulting data selection is already simplified (see \ref QCPDataSelection::simplify).
*/
inline const QCPDataSelection operator+(const QCPDataRange& a, const QCPDataSelection& b)
{
  QCPDataSelection result(a);
  result += b;
  return result;
}

/*!
  Return a \ref QCPDataSelection with the data points in \a a joined with the data points in \a b.
  The resulting data selection is already simplified (see \ref QCPDataSelection::simplify).
*/
inline const QCPDataSelection operator+(const QCPDataSelection& a, const QCPDataRange& b)
{
  QCPDataSelection result(a);
  result += b;
  return result;
}

/*!
  Return a \ref QCPDataSelection with the data points in \a a joined with the data points in \a b.
  The resulting data selection is already simplified (see \ref QCPDataSelection::simplify).
*/
inline const QCPDataSelection operator+(const QCPDataRange& a, const QCPDataRange& b)
{
  QCPDataSelection result(a);
  result += b;
  return result;
}

/*!
  Return a \ref QCPDataSelection with the data points which are in \a a but not in \a b.
*/
inline const QCPDataSelection operator-(const QCPDataSelection& a, const QCPDataSelection& b)
{
  QCPDataSelection result(a);
  result -= b;
  return result;
}

/*!
  Return a \ref QCPDataSelection with the data points which are in \a a but not in \a b.
*/
inline const QCPDataSelection operator-(const QCPDataRange& a, const QCPDataSelection& b)
{
  QCPDataSelection result(a);
  result -= b;
  return result;
}

/*!
  Return a \ref QCPDataSelection with the data points which are in \a a but not in \a b.
*/
inline const QCPDataSelection operator-(const QCPDataSelection& a, const QCPDataRange& b)
{
  QCPDataSelection result(a);
  result -= b;
  return result;
}

/*!
  Return a \ref QCPDataSelection with the data points which are in \a a but not in \a b.
*/
inline const QCPDataSelection operator-(const QCPDataRange& a, const QCPDataRange& b)
{
  QCPDataSelection result(a);
  result -= b;
  return result;
}

/*! \relates QCPDataRange

  Prints \a dataRange in a human readable format to the qDebug output.
*/
inline QDebug operator<< (QDebug d, const QCPDataRange &dataRange)
{
    d.nospace() << "[" << dataRange.begin() << ".." << dataRange.end()-1 << "]";
    return d.space();
}

/*! \relates QCPDataSelection

  Prints \a selection in a human readable format to the qDebug output.
*/
inline QDebug operator<< (QDebug d, const QCPDataSelection &selection)
{
    d.nospace() << "QCPDataSelection(";
    for (int i=0; i<selection.dataRangeCount(); ++i)
    {
      if (i != 0)
        d << ", ";
      d << selection.dataRange(i);
    }
    d << ")";
    return d.space();
}



/* end of 'src/selection.h' */


/* including file 'src/selectionrect.h', size 3338                           */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPSelectionRect : public QCPLayerable
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPSelectionRect(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPSelectionRect();
  
  // getters:
  /**
   * @brief
   *
   * @return QRect
   */
  QRect rect() const { return mRect; }
  /**
   * @brief
   *
   * @param axis
   * @return QCPRange
   */
  QCPRange range(const QCPAxis *axis) const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return bool
   */
  bool isActive() const { return mActive; }
  
  // setters:
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  
  // non-property methods:
  /**
   * @brief
   *
   */
  Q_SLOT void cancel();
  
signals:
  /**
   * @brief
   *
   * @param event
   */
  void started(QMouseEvent *event);
  /**
   * @brief
   *
   * @param rect
   * @param event
   */
  void changed(const QRect &rect, QMouseEvent *event);
  /**
   * @brief
   *
   * @param rect
   * @param event
   */
  void canceled(const QRect &rect, QInputEvent *event);
  /**
   * @brief
   *
   * @param rect
   * @param event
   */
  void accepted(const QRect &rect, QMouseEvent *event);
  
protected:
  // property members:
  QRect mRect; /**< TODO: describe */
  QPen mPen; /**< TODO: describe */
  QBrush mBrush; /**< TODO: describe */
  // non-property members:
  bool mActive; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param event
   */
  virtual void startSelection(QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  virtual void moveSelection(QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  virtual void endSelection(QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  virtual void keyPressEvent(QKeyEvent *event);
  
  // reimplemented virtual methods
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  
  friend class QCustomPlot;
};

/* end of 'src/selectionrect.h' */


/* including file 'src/layout.h', size 13128                                 */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPMarginGroup : public QObject
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPMarginGroup(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPMarginGroup();
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param side
   * @return QList<QCPLayoutElement *>
   */
  QList<QCPLayoutElement*> elements(QCP::MarginSide side) const { return mChildren.value(side); }
  /**
   * @brief
   *
   * @return bool
   */
  bool isEmpty() const;
  /**
   * @brief
   *
   */
  void clear();
  
protected:
  // non-property members:
  QCustomPlot *mParentPlot; /**< TODO: describe */
  QHash<QCP::MarginSide, QList<QCPLayoutElement*> > mChildren; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param side
   * @return int
   */
  virtual int commonMargin(QCP::MarginSide side) const;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param side
   * @param element
   */
  void addChild(QCP::MarginSide side, QCPLayoutElement *element);
  /**
   * @brief
   *
   * @param side
   * @param element
   */
  void removeChild(QCP::MarginSide side, QCPLayoutElement *element);
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPMarginGroup)
  
  friend class QCPLayoutElement;
};


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPLayoutElement : public QCPLayerable
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QCPLayout* layout READ layout)
  Q_PROPERTY(QRect rect READ rect)
  Q_PROPERTY(QRect outerRect READ outerRect WRITE setOuterRect)
  Q_PROPERTY(QMargins margins READ margins WRITE setMargins)
  Q_PROPERTY(QMargins minimumMargins READ minimumMargins WRITE setMinimumMargins)
  Q_PROPERTY(QSize minimumSize READ minimumSize WRITE setMinimumSize)
  Q_PROPERTY(QSize maximumSize READ maximumSize WRITE setMaximumSize)
  /// \endcond
public:
  /*!
    Defines the phases of the update process, that happens just before a replot. At each phase,
    \ref update is called with the according UpdatePhase value.
  */
  enum UpdatePhase { upPreparation ///< Phase used for any type of preparation that needs to be done before margin calculation and layout
                     ,upMargins    ///< Phase in which the margins are calculated and set
                     ,upLayout     ///< Final phase in which the layout system places the rects of the elements
                   };
  Q_ENUMS(UpdatePhase)

  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPLayoutElement(QCustomPlot *parentPlot=0);
  /**
   * @brief
   *
   */
  virtual ~QCPLayoutElement();
  
  // getters:
  /**
   * @brief
   *
   * @return QCPLayout
   */
  QCPLayout *layout() const { return mParentLayout; }
  /**
   * @brief
   *
   * @return QRect
   */
  QRect rect() const { return mRect; }
  /**
   * @brief
   *
   * @return QRect
   */
  QRect outerRect() const { return mOuterRect; }
  /**
   * @brief
   *
   * @return QMargins
   */
  QMargins margins() const { return mMargins; }
  /**
   * @brief
   *
   * @return QMargins
   */
  QMargins minimumMargins() const { return mMinimumMargins; }
  /**
   * @brief
   *
   * @return QCP::MarginSides
   */
  QCP::MarginSides autoMargins() const { return mAutoMargins; }
  /**
   * @brief
   *
   * @return QSize
   */
  QSize minimumSize() const { return mMinimumSize; }
  /**
   * @brief
   *
   * @return QSize
   */
  QSize maximumSize() const { return mMaximumSize; }
  /**
   * @brief
   *
   * @param side
   * @return QCPMarginGroup
   */
  QCPMarginGroup *marginGroup(QCP::MarginSide side) const { return mMarginGroups.value(side, (QCPMarginGroup*)0); }
  /**
   * @brief
   *
   * @return QHash<QCP::MarginSide, QCPMarginGroup *>
   */
  QHash<QCP::MarginSide, QCPMarginGroup*> marginGroups() const { return mMarginGroups; }
  
  // setters:
  /**
   * @brief
   *
   * @param rect
   */
  void setOuterRect(const QRect &rect);
  /**
   * @brief
   *
   * @param margins
   */
  void setMargins(const QMargins &margins);
  /**
   * @brief
   *
   * @param margins
   */
  void setMinimumMargins(const QMargins &margins);
  /**
   * @brief
   *
   * @param sides
   */
  void setAutoMargins(QCP::MarginSides sides);
  /**
   * @brief
   *
   * @param size
   */
  void setMinimumSize(const QSize &size);
  /**
   * @brief
   *
   * @param width
   * @param height
   */
  void setMinimumSize(int width, int height);
  /**
   * @brief
   *
   * @param size
   */
  void setMaximumSize(const QSize &size);
  /**
   * @brief
   *
   * @param width
   * @param height
   */
  void setMaximumSize(int width, int height);
  /**
   * @brief
   *
   * @param sides
   * @param group
   */
  void setMarginGroup(QCP::MarginSides sides, QCPMarginGroup *group);
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param phase
   */
  virtual void update(UpdatePhase phase);
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize minimumSizeHint() const;
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize maximumSizeHint() const;
  /**
   * @brief
   *
   * @param recursive
   * @return QList<QCPLayoutElement *>
   */
  virtual QList<QCPLayoutElement*> elements(bool recursive) const;
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
protected:
  // property members:
  QCPLayout *mParentLayout; /**< TODO: describe */
  QSize mMinimumSize, mMaximumSize; /**< TODO: describe */
  QRect mRect, mOuterRect; /**< TODO: describe */
  QMargins mMargins, mMinimumMargins; /**< TODO: describe */
  QCP::MarginSides mAutoMargins; /**< TODO: describe */
  QHash<QCP::MarginSide, QCPMarginGroup*> mMarginGroups; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param side
   * @return int
   */
  virtual int calculateAutoMargin(QCP::MarginSide side);
  /**
   * @brief
   *
   */
  virtual void layoutChanged();
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE { Q_UNUSED(painter) }
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE { Q_UNUSED(painter) }
  /**
   * @brief
   *
   * @param parentPlot
   */
  virtual void parentPlotInitialized(QCustomPlot *parentPlot) Q_DECL_OVERRIDE;

private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPLayoutElement)
  
  friend class QCustomPlot;
  friend class QCPLayout;
  friend class QCPMarginGroup;
};
Q_DECLARE_METATYPE(QCPLayoutElement::UpdatePhase)


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPLayout : public QCPLayoutElement
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   */
  explicit QCPLayout();
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param phase
   */
  virtual void update(UpdatePhase phase) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param recursive
   * @return QList<QCPLayoutElement *>
   */
  virtual QList<QCPLayoutElement*> elements(bool recursive) const Q_DECL_OVERRIDE;
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @return int
   */
  virtual int elementCount() const = 0;
  /**
   * @brief
   *
   * @param index
   * @return QCPLayoutElement
   */
  virtual QCPLayoutElement* elementAt(int index) const = 0;
  /**
   * @brief
   *
   * @param index
   * @return QCPLayoutElement
   */
  virtual QCPLayoutElement* takeAt(int index) = 0;
  /**
   * @brief
   *
   * @param element
   * @return bool
   */
  virtual bool take(QCPLayoutElement* element) = 0;
  /**
   * @brief
   *
   */
  virtual void simplify();
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param index
   * @return bool
   */
  bool removeAt(int index);
  /**
   * @brief
   *
   * @param element
   * @return bool
   */
  bool remove(QCPLayoutElement* element);
  /**
   * @brief
   *
   */
  void clear();
  
protected:
  // introduced virtual methods:
  /**
   * @brief
   *
   */
  virtual void updateLayout();
  
  // non-virtual methods:
  /**
   * @brief
   *
   */
  void sizeConstraintsChanged() const;
  /**
   * @brief
   *
   * @param el
   */
  void adoptElement(QCPLayoutElement *el);
  /**
   * @brief
   *
   * @param el
   */
  void releaseElement(QCPLayoutElement *el);
  /**
   * @brief
   *
   * @param maxSizes
   * @param minSizes
   * @param stretchFactors
   * @param totalSize
   * @return QVector<int>
   */
  QVector<int> getSectionSizes(QVector<int> maxSizes, QVector<int> minSizes, QVector<double> stretchFactors, int totalSize) const;
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPLayout)
  friend class QCPLayoutElement;
};


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPLayoutGrid : public QCPLayout
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(int rowCount READ rowCount)
  Q_PROPERTY(int columnCount READ columnCount)
  Q_PROPERTY(QList<double> columnStretchFactors READ columnStretchFactors WRITE setColumnStretchFactors)
  Q_PROPERTY(QList<double> rowStretchFactors READ rowStretchFactors WRITE setRowStretchFactors)
  Q_PROPERTY(int columnSpacing READ columnSpacing WRITE setColumnSpacing)
  Q_PROPERTY(int rowSpacing READ rowSpacing WRITE setRowSpacing)
  Q_PROPERTY(FillOrder fillOrder READ fillOrder WRITE setFillOrder)
  Q_PROPERTY(int wrap READ wrap WRITE setWrap)
  /// \endcond
public:
  
  /*!
    Defines in which direction the grid is filled when using \ref addElement(QCPLayoutElement*).
    The column/row at which wrapping into the next row/column occurs can be specified with \ref
    setWrap.

    \see setFillOrder
  */
  enum FillOrder { foRowsFirst    ///< Rows are filled first, and a new element is wrapped to the next column if the row count would exceed \ref setWrap.
                  ,foColumnsFirst ///< Columns are filled first, and a new element is wrapped to the next row if the column count would exceed \ref setWrap.
                };
  Q_ENUMS(FillOrder)
  
  /**
   * @brief
   *
   */
  explicit QCPLayoutGrid();
  /**
   * @brief
   *
   */
  virtual ~QCPLayoutGrid();
  
  // getters:
  /**
   * @brief
   *
   * @return int
   */
  int rowCount() const { return mElements.size(); }
  /**
   * @brief
   *
   * @return int
   */
  int columnCount() const { return mElements.size() > 0 ? mElements.first().size() : 0; }
  /**
   * @brief
   *
   * @return QList<double>
   */
  QList<double> columnStretchFactors() const { return mColumnStretchFactors; }
  /**
   * @brief
   *
   * @return QList<double>
   */
  QList<double> rowStretchFactors() const { return mRowStretchFactors; }
  /**
   * @brief
   *
   * @return int
   */
  int columnSpacing() const { return mColumnSpacing; }
  /**
   * @brief
   *
   * @return int
   */
  int rowSpacing() const { return mRowSpacing; }
  /**
   * @brief
   *
   * @return int
   */
  int wrap() const { return mWrap; }
  /**
   * @brief
   *
   * @return FillOrder
   */
  FillOrder fillOrder() const { return mFillOrder; }
  
  // setters:
  /**
   * @brief
   *
   * @param column
   * @param factor
   */
  void setColumnStretchFactor(int column, double factor);
  /**
   * @brief
   *
   * @param factors
   */
  void setColumnStretchFactors(const QList<double> &factors);
  /**
   * @brief
   *
   * @param row
   * @param factor
   */
  void setRowStretchFactor(int row, double factor);
  /**
   * @brief
   *
   * @param factors
   */
  void setRowStretchFactors(const QList<double> &factors);
  /**
   * @brief
   *
   * @param pixels
   */
  void setColumnSpacing(int pixels);
  /**
   * @brief
   *
   * @param pixels
   */
  void setRowSpacing(int pixels);
  /**
   * @brief
   *
   * @param count
   */
  void setWrap(int count);
  /**
   * @brief
   *
   * @param order
   * @param rearrange
   */
  void setFillOrder(FillOrder order, bool rearrange=true);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   */
  virtual void updateLayout() Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return int
   */
  virtual int elementCount() const Q_DECL_OVERRIDE { return rowCount()*columnCount(); }
  /**
   * @brief
   *
   * @param index
   * @return QCPLayoutElement
   */
  virtual QCPLayoutElement* elementAt(int index) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param index
   * @return QCPLayoutElement
   */
  virtual QCPLayoutElement* takeAt(int index) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param element
   * @return bool
   */
  virtual bool take(QCPLayoutElement* element) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param recursive
   * @return QList<QCPLayoutElement *>
   */
  virtual QList<QCPLayoutElement*> elements(bool recursive) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   */
  virtual void simplify() Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize minimumSizeHint() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize maximumSizeHint() const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param row
   * @param column
   * @return QCPLayoutElement
   */
  QCPLayoutElement *element(int row, int column) const;
  /**
   * @brief
   *
   * @param row
   * @param column
   * @param element
   * @return bool
   */
  bool addElement(int row, int column, QCPLayoutElement *element);
  /**
   * @brief
   *
   * @param element
   * @return bool
   */
  bool addElement(QCPLayoutElement *element);
  /**
   * @brief
   *
   * @param row
   * @param column
   * @return bool
   */
  bool hasElement(int row, int column);
  /**
   * @brief
   *
   * @param newRowCount
   * @param newColumnCount
   */
  void expandTo(int newRowCount, int newColumnCount);
  /**
   * @brief
   *
   * @param newIndex
   */
  void insertRow(int newIndex);
  /**
   * @brief
   *
   * @param newIndex
   */
  void insertColumn(int newIndex);
  /**
   * @brief
   *
   * @param row
   * @param column
   * @return int
   */
  int rowColToIndex(int row, int column) const;
  /**
   * @brief
   *
   * @param index
   * @param row
   * @param column
   */
  void indexToRowCol(int index, int &row, int &column) const;
  
protected:
  // property members:
  QList<QList<QCPLayoutElement*> > mElements; /**< TODO: describe */
  QList<double> mColumnStretchFactors; /**< TODO: describe */
  QList<double> mRowStretchFactors; /**< TODO: describe */
  int mColumnSpacing, mRowSpacing; /**< TODO: describe */
  int mWrap; /**< TODO: describe */
  FillOrder mFillOrder; /**< TODO: describe */
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param minColWidths
   * @param minRowHeights
   */
  void getMinimumRowColSizes(QVector<int> *minColWidths, QVector<int> *minRowHeights) const;
  /**
   * @brief
   *
   * @param maxColWidths
   * @param maxRowHeights
   */
  void getMaximumRowColSizes(QVector<int> *maxColWidths, QVector<int> *maxRowHeights) const;
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPLayoutGrid)
};
Q_DECLARE_METATYPE(QCPLayoutGrid::FillOrder)


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPLayoutInset : public QCPLayout
{
  Q_OBJECT
public:
  /*!
    Defines how the placement and sizing is handled for a certain element in a QCPLayoutInset.
  */
  enum InsetPlacement { ipFree            ///< The element may be positioned/sized arbitrarily, see \ref setInsetRect
                        ,ipBorderAligned  ///< The element is aligned to one of the layout sides, see \ref setInsetAlignment
                      };
  Q_ENUMS(InsetPlacement)
  
  /**
   * @brief
   *
   */
  explicit QCPLayoutInset();
  /**
   * @brief
   *
   */
  virtual ~QCPLayoutInset();
  
  // getters:
  /**
   * @brief
   *
   * @param index
   * @return InsetPlacement
   */
  InsetPlacement insetPlacement(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return Qt::Alignment
   */
  Qt::Alignment insetAlignment(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return QRectF
   */
  QRectF insetRect(int index) const;
  
  // setters:
  /**
   * @brief
   *
   * @param index
   * @param placement
   */
  void setInsetPlacement(int index, InsetPlacement placement);
  /**
   * @brief
   *
   * @param index
   * @param alignment
   */
  void setInsetAlignment(int index, Qt::Alignment alignment);
  /**
   * @brief
   *
   * @param index
   * @param rect
   */
  void setInsetRect(int index, const QRectF &rect);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   */
  virtual void updateLayout() Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return int
   */
  virtual int elementCount() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param index
   * @return QCPLayoutElement
   */
  virtual QCPLayoutElement* elementAt(int index) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param index
   * @return QCPLayoutElement
   */
  virtual QCPLayoutElement* takeAt(int index) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param element
   * @return bool
   */
  virtual bool take(QCPLayoutElement* element) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   */
  virtual void simplify() Q_DECL_OVERRIDE {}
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param element
   * @param alignment
   */
  void addElement(QCPLayoutElement *element, Qt::Alignment alignment);
  /**
   * @brief
   *
   * @param element
   * @param rect
   */
  void addElement(QCPLayoutElement *element, const QRectF &rect);
  
protected:
  // property members:
  QList<QCPLayoutElement*> mElements; /**< TODO: describe */
  QList<InsetPlacement> mInsetPlacement; /**< TODO: describe */
  QList<Qt::Alignment> mInsetAlignment; /**< TODO: describe */
  QList<QRectF> mInsetRect; /**< TODO: describe */
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPLayoutInset)
};
Q_DECLARE_METATYPE(QCPLayoutInset::InsetPlacement)

/* end of 'src/layout.h' */


/* including file 'src/lineending.h', size 4426                              */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPLineEnding
{
  Q_GADGET
public:
  /*!
    Defines the type of ending decoration for line-like items, e.g. an arrow.
    
    \image html QCPLineEnding.png
    
    The width and length of these decorations can be controlled with the functions \ref setWidth
    and \ref setLength. Some decorations like \ref esDisc, \ref esSquare, \ref esDiamond and \ref esBar only
    support a width, the length property is ignored.
    
    \see QCPItemLine::setHead, QCPItemLine::setTail, QCPItemCurve::setHead, QCPItemCurve::setTail, QCPAxis::setLowerEnding, QCPAxis::setUpperEnding
  */
  enum EndingStyle { esNone          ///< No ending decoration
                     ,esFlatArrow    ///< A filled arrow head with a straight/flat back (a triangle)
                     ,esSpikeArrow   ///< A filled arrow head with an indented back
                     ,esLineArrow    ///< A non-filled arrow head with open back
                     ,esDisc         ///< A filled circle
                     ,esSquare       ///< A filled square
                     ,esDiamond      ///< A filled diamond (45 degrees rotated square)
                     ,esBar          ///< A bar perpendicular to the line
                     ,esHalfBar      ///< A bar perpendicular to the line, pointing out to only one side (to which side can be changed with \ref setInverted)
                     ,esSkewedBar    ///< A bar that is skewed (skew controllable via \ref setLength)
                   };
  Q_ENUMS(EndingStyle)
  
  /**
   * @brief
   *
   */
  QCPLineEnding();
  /**
   * @brief
   *
   * @param style
   * @param width
   * @param length
   * @param inverted
   */
  QCPLineEnding(EndingStyle style, double width=8, double length=10, bool inverted=false);
  
  // getters:
  /**
   * @brief
   *
   * @return EndingStyle
   */
  EndingStyle style() const { return mStyle; }
  /**
   * @brief
   *
   * @return double
   */
  double width() const { return mWidth; }
  /**
   * @brief
   *
   * @return double
   */
  double length() const { return mLength; }
  /**
   * @brief
   *
   * @return bool
   */
  bool inverted() const { return mInverted; }
  
  // setters:
  /**
   * @brief
   *
   * @param style
   */
  void setStyle(EndingStyle style);
  /**
   * @brief
   *
   * @param width
   */
  void setWidth(double width);
  /**
   * @brief
   *
   * @param length
   */
  void setLength(double length);
  /**
   * @brief
   *
   * @param inverted
   */
  void setInverted(bool inverted);
  
  // non-property methods:
  /**
   * @brief
   *
   * @return double
   */
  double boundingDistance() const;
  /**
   * @brief
   *
   * @return double
   */
  double realLength() const;
  /**
   * @brief
   *
   * @param painter
   * @param pos
   * @param dir
   */
  void draw(QCPPainter *painter, const QCPVector2D &pos, const QCPVector2D &dir) const;
  /**
   * @brief
   *
   * @param painter
   * @param pos
   * @param angle
   */
  void draw(QCPPainter *painter, const QCPVector2D &pos, double angle) const;
  
protected:
  // property members:
  EndingStyle mStyle; /**< TODO: describe */
  double mWidth, mLength; /**< TODO: describe */
  bool mInverted; /**< TODO: describe */
};
Q_DECLARE_TYPEINFO(QCPLineEnding, Q_MOVABLE_TYPE);
Q_DECLARE_METATYPE(QCPLineEnding::EndingStyle)

/* end of 'src/lineending.h' */


/* including file 'src/axis/axisticker.h', size 4177                         */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAxisTicker
{
  Q_GADGET
public:
  /*!
    Defines the strategies that the axis ticker may follow when choosing the size of the tick step.
    
    \see setTickStepStrategy
  */
  enum TickStepStrategy
  {
    tssReadability    ///< A nicely readable tick step is prioritized over matching the requested number of ticks (see \ref setTickCount)
    ,tssMeetTickCount ///< Less readable tick steps are allowed which in turn facilitates getting closer to the requested tick count
  };
  Q_ENUMS(TickStepStrategy)
  
  /**
   * @brief
   *
   */
  QCPAxisTicker();
  /**
   * @brief
   *
   */
  virtual ~QCPAxisTicker();
  
  // getters:
  /**
   * @brief
   *
   * @return TickStepStrategy
   */
  TickStepStrategy tickStepStrategy() const { return mTickStepStrategy; }
  /**
   * @brief
   *
   * @return int
   */
  int tickCount() const { return mTickCount; }
  /**
   * @brief
   *
   * @return double
   */
  double tickOrigin() const { return mTickOrigin; }
  
  // setters:
  /**
   * @brief
   *
   * @param strategy
   */
  void setTickStepStrategy(TickStepStrategy strategy);
  /**
   * @brief
   *
   * @param count
   */
  void setTickCount(int count);
  /**
   * @brief
   *
   * @param origin
   */
  void setTickOrigin(double origin);
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param range
   * @param locale
   * @param formatChar
   * @param precision
   * @param ticks
   * @param subTicks
   * @param tickLabels
   */
  virtual void generate(const QCPRange &range, const QLocale &locale, QChar formatChar, int precision, QVector<double> &ticks, QVector<double> *subTicks, QVector<QString> *tickLabels);
  
protected:
  // property members:
  TickStepStrategy mTickStepStrategy; /**< TODO: describe */
  int mTickCount; /**< TODO: describe */
  double mTickOrigin; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param range
   * @return double
   */
  virtual double getTickStep(const QCPRange &range);
  /**
   * @brief
   *
   * @param tickStep
   * @return int
   */
  virtual int getSubTickCount(double tickStep);
  /**
   * @brief
   *
   * @param tick
   * @param locale
   * @param formatChar
   * @param precision
   * @return QString
   */
  virtual QString getTickLabel(double tick, const QLocale &locale, QChar formatChar, int precision);
  /**
   * @brief
   *
   * @param tickStep
   * @param range
   * @return QVector<double>
   */
  virtual QVector<double> createTickVector(double tickStep, const QCPRange &range);
  /**
   * @brief
   *
   * @param subTickCount
   * @param ticks
   * @return QVector<double>
   */
  virtual QVector<double> createSubTickVector(int subTickCount, const QVector<double> &ticks);
  /**
   * @brief
   *
   * @param ticks
   * @param locale
   * @param formatChar
   * @param precision
   * @return QVector<QString>
   */
  virtual QVector<QString> createLabelVector(const QVector<double> &ticks, const QLocale &locale, QChar formatChar, int precision);
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param range
   * @param ticks
   * @param keepOneOutlier
   */
  void trimTicks(const QCPRange &range, QVector<double> &ticks, bool keepOneOutlier) const;
  /**
   * @brief
   *
   * @param target
   * @param candidates
   * @return double
   */
  double pickClosest(double target, const QVector<double> &candidates) const;
  /**
   * @brief
   *
   * @param input
   * @param magnitude
   * @return double
   */
  double getMantissa(double input, double *magnitude=0) const;
  /**
   * @brief
   *
   * @param input
   * @return double
   */
  double cleanMantissa(double input) const;
};
Q_DECLARE_METATYPE(QCPAxisTicker::TickStepStrategy)
Q_DECLARE_METATYPE(QSharedPointer<QCPAxisTicker>)

/* end of 'src/axis/axisticker.h' */


/* including file 'src/axis/axistickerdatetime.h', size 3289                 */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAxisTickerDateTime : public QCPAxisTicker
{
public:
  /**
   * @brief
   *
   */
  QCPAxisTickerDateTime();
  
  // getters:
  /**
   * @brief
   *
   * @return QString
   */
  QString dateTimeFormat() const { return mDateTimeFormat; }
  /**
   * @brief
   *
   * @return Qt::TimeSpec
   */
  Qt::TimeSpec dateTimeSpec() const { return mDateTimeSpec; }
  
  // setters:
  /**
   * @brief
   *
   * @param format
   */
  void setDateTimeFormat(const QString &format);
  /**
   * @brief
   *
   * @param spec
   */
  void setDateTimeSpec(Qt::TimeSpec spec);
  /**
   * @brief
   *
   * @param origin
   */
  void setTickOrigin(double origin); // hides base class method but calls baseclass implementation ("using" throws off IDEs and doxygen)
  /**
   * @brief
   *
   * @param origin
   */
  void setTickOrigin(const QDateTime &origin);
  
  // static methods:
  /**
   * @brief
   *
   * @param key
   * @return QDateTime
   */
  static QDateTime keyToDateTime(double key);
  /**
   * @brief
   *
   * @param dateTime
   * @return double
   */
  static double dateTimeToKey(const QDateTime dateTime);
  /**
   * @brief
   *
   * @param date
   * @return double
   */
  static double dateTimeToKey(const QDate date);
  
protected:
  // property members:
  QString mDateTimeFormat; /**< TODO: describe */
  Qt::TimeSpec mDateTimeSpec; /**< TODO: describe */
  
  // non-property members:
  /**
   * @brief
   *
   */
  enum DateStrategy {dsNone, dsUniformTimeInDay, dsUniformDayInMonth} mDateStrategy;
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param range
   * @return double
   */
  virtual double getTickStep(const QCPRange &range) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tickStep
   * @return int
   */
  virtual int getSubTickCount(double tickStep) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tick
   * @param locale
   * @param formatChar
   * @param precision
   * @return QString
   */
  virtual QString getTickLabel(double tick, const QLocale &locale, QChar formatChar, int precision) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tickStep
   * @param range
   * @return QVector<double>
   */
  virtual QVector<double> createTickVector(double tickStep, const QCPRange &range) Q_DECL_OVERRIDE;
};

/* end of 'src/axis/axistickerdatetime.h' */


/* including file 'src/axis/axistickertime.h', size 3288                     */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAxisTickerTime : public QCPAxisTicker
{
  Q_GADGET
public:
  /*!
    Defines the logical units in which fractions of time spans can be expressed.
    
    \see setFieldWidth, setTimeFormat
  */
  enum TimeUnit { tuMilliseconds
                  ,tuSeconds
                  ,tuMinutes
                  ,tuHours
                  ,tuDays
                };
  Q_ENUMS(TimeUnit)
  
  /**
   * @brief
   *
   */
  QCPAxisTickerTime();

  // getters:
  /**
   * @brief
   *
   * @return QString
   */
  QString timeFormat() const { return mTimeFormat; }
  /**
   * @brief
   *
   * @param unit
   * @return int
   */
  int fieldWidth(TimeUnit unit) const { return mFieldWidth.value(unit); }
  
  // setters:
  /**
   * @brief
   *
   * @param format
   */
  void setTimeFormat(const QString &format);
  /**
   * @brief
   *
   * @param unit
   * @param width
   */
  void setFieldWidth(TimeUnit unit, int width);
  
protected:
  // property members:
  QString mTimeFormat; /**< TODO: describe */
  QHash<TimeUnit, int> mFieldWidth; /**< TODO: describe */
  
  // non-property members:
  TimeUnit mSmallestUnit, mBiggestUnit; /**< TODO: describe */
  QHash<TimeUnit, QString> mFormatPattern; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param range
   * @return double
   */
  virtual double getTickStep(const QCPRange &range) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tickStep
   * @return int
   */
  virtual int getSubTickCount(double tickStep) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tick
   * @param locale
   * @param formatChar
   * @param precision
   * @return QString
   */
  virtual QString getTickLabel(double tick, const QLocale &locale, QChar formatChar, int precision) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param text
   * @param unit
   * @param value
   */
  void replaceUnit(QString &text, TimeUnit unit, int value) const;
};
Q_DECLARE_METATYPE(QCPAxisTickerTime::TimeUnit)

/* end of 'src/axis/axistickertime.h' */


/* including file 'src/axis/axistickerfixed.h', size 3308                    */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAxisTickerFixed : public QCPAxisTicker
{
  Q_GADGET
public:
  /*!
    Defines how the axis ticker may modify the specified tick step (\ref setTickStep) in order to
    control the number of ticks in the axis range.
    
    \see setScaleStrategy
  */
  enum ScaleStrategy { ssNone      ///< Modifications are not allowed, the specified tick step is absolutely fixed. This might cause a high tick density and overlapping labels if the axis range is zoomed out.
                       ,ssMultiples ///< An integer multiple of the specified tick step is allowed. The used factor follows the base class properties of \ref setTickStepStrategy and \ref setTickCount.
                       ,ssPowers    ///< An integer power of the specified tick step is allowed.
                     };
  Q_ENUMS(ScaleStrategy)
  
  /**
   * @brief
   *
   */
  QCPAxisTickerFixed();
  
  // getters:
  /**
   * @brief
   *
   * @return double
   */
  double tickStep() const { return mTickStep; }
  /**
   * @brief
   *
   * @return ScaleStrategy
   */
  ScaleStrategy scaleStrategy() const { return mScaleStrategy; }
  
  // setters:
  /**
   * @brief
   *
   * @param step
   */
  void setTickStep(double step);
  /**
   * @brief
   *
   * @param strategy
   */
  void setScaleStrategy(ScaleStrategy strategy);
  
protected:
  // property members:
  double mTickStep; /**< TODO: describe */
  ScaleStrategy mScaleStrategy; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param range
   * @return double
   */
  virtual double getTickStep(const QCPRange &range) Q_DECL_OVERRIDE;
};
Q_DECLARE_METATYPE(QCPAxisTickerFixed::ScaleStrategy)

/* end of 'src/axis/axistickerfixed.h' */


/* including file 'src/axis/axistickertext.h', size 3085                     */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAxisTickerText : public QCPAxisTicker
{
public:
  /**
   * @brief
   *
   */
  QCPAxisTickerText();
  
  // getters:
  /**
   * @brief
   *
   * @return QMap<double, QString>
   */
  QMap<double, QString> &ticks() { return mTicks; }
  /**
   * @brief
   *
   * @return int
   */
  int subTickCount() const { return mSubTickCount; }
  
  // setters:
  /**
   * @brief
   *
   * @param QMap<double
   * @param ticks
   */
  void setTicks(const QMap<double, QString> &ticks);
  /**
   * @brief
   *
   * @param positions
   * @param labels
   */
  void setTicks(const QVector<double> &positions, const QVector<QString> labels);
  /**
   * @brief
   *
   * @param subTicks
   */
  void setSubTickCount(int subTicks);
  
  // non-virtual methods:
  /**
   * @brief
   *
   */
  void clear();
  /**
   * @brief
   *
   * @param position
   * @param label
   */
  void addTick(double position, QString label);
  /**
   * @brief
   *
   * @param QMap<double
   * @param ticks
   */
  void addTicks(const QMap<double, QString> &ticks);
  /**
   * @brief
   *
   * @param positions
   * @param labels
   */
  void addTicks(const QVector<double> &positions, const QVector<QString> &labels);
  
protected:
  // property members:
  QMap<double, QString> mTicks; /**< TODO: describe */
  int mSubTickCount; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param range
   * @return double
   */
  virtual double getTickStep(const QCPRange &range) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tickStep
   * @return int
   */
  virtual int getSubTickCount(double tickStep) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tick
   * @param locale
   * @param formatChar
   * @param precision
   * @return QString
   */
  virtual QString getTickLabel(double tick, const QLocale &locale, QChar formatChar, int precision) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tickStep
   * @param range
   * @return QVector<double>
   */
  virtual QVector<double> createTickVector(double tickStep, const QCPRange &range) Q_DECL_OVERRIDE;
  
};

/* end of 'src/axis/axistickertext.h' */


/* including file 'src/axis/axistickerpi.h', size 3911                       */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAxisTickerPi : public QCPAxisTicker
{
  Q_GADGET
public:
  /*!
    Defines how fractions should be displayed in tick labels.
    
    \see setFractionStyle
  */
  enum FractionStyle { fsFloatingPoint     ///< Fractions are displayed as regular decimal floating point numbers, e.g. "0.25" or "0.125".
                       ,fsAsciiFractions   ///< Fractions are written as rationals using ASCII characters only, e.g. "1/4" or "1/8"
                       ,fsUnicodeFractions ///< Fractions are written using sub- and superscript UTF-8 digits and the fraction symbol.
                     };
  Q_ENUMS(FractionStyle)
  
  /**
   * @brief
   *
   */
  QCPAxisTickerPi();
  
  // getters:
  /**
   * @brief
   *
   * @return QString
   */
  QString piSymbol() const { return mPiSymbol; }
  /**
   * @brief
   *
   * @return double
   */
  double piValue() const { return mPiValue; }
  /**
   * @brief
   *
   * @return bool
   */
  bool periodicity() const { return mPeriodicity; }
  /**
   * @brief
   *
   * @return FractionStyle
   */
  FractionStyle fractionStyle() const { return mFractionStyle; }
  
  // setters:
  /**
   * @brief
   *
   * @param symbol
   */
  void setPiSymbol(QString symbol);
  /**
   * @brief
   *
   * @param pi
   */
  void setPiValue(double pi);
  /**
   * @brief
   *
   * @param multiplesOfPi
   */
  void setPeriodicity(int multiplesOfPi);
  /**
   * @brief
   *
   * @param style
   */
  void setFractionStyle(FractionStyle style);
  
protected:
  // property members:
  QString mPiSymbol; /**< TODO: describe */
  double mPiValue; /**< TODO: describe */
  int mPeriodicity; /**< TODO: describe */
  FractionStyle mFractionStyle; /**< TODO: describe */
  
  // non-property members:
  double mPiTickStep; // size of one tick step in units of mPiValue /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param range
   * @return double
   */
  virtual double getTickStep(const QCPRange &range) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tickStep
   * @return int
   */
  virtual int getSubTickCount(double tickStep) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tick
   * @param locale
   * @param formatChar
   * @param precision
   * @return QString
   */
  virtual QString getTickLabel(double tick, const QLocale &locale, QChar formatChar, int precision) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param numerator
   * @param denominator
   */
  void simplifyFraction(int &numerator, int &denominator) const;
  /**
   * @brief
   *
   * @param numerator
   * @param denominator
   * @return QString
   */
  QString fractionToString(int numerator, int denominator) const;
  /**
   * @brief
   *
   * @param numerator
   * @param denominator
   * @return QString
   */
  QString unicodeFraction(int numerator, int denominator) const;
  /**
   * @brief
   *
   * @param number
   * @return QString
   */
  QString unicodeSuperscript(int number) const;
  /**
   * @brief
   *
   * @param number
   * @return QString
   */
  QString unicodeSubscript(int number) const;
};
Q_DECLARE_METATYPE(QCPAxisTickerPi::FractionStyle)

/* end of 'src/axis/axistickerpi.h' */


/* including file 'src/axis/axistickerlog.h', size 2663                      */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAxisTickerLog : public QCPAxisTicker
{
public:
  /**
   * @brief
   *
   */
  QCPAxisTickerLog();
  
  // getters:
  /**
   * @brief
   *
   * @return double
   */
  double logBase() const { return mLogBase; }
  /**
   * @brief
   *
   * @return int
   */
  int subTickCount() const { return mSubTickCount; }
  
  // setters:
  /**
   * @brief
   *
   * @param base
   */
  void setLogBase(double base);
  /**
   * @brief
   *
   * @param subTicks
   */
  void setSubTickCount(int subTicks);
  
protected:
  // property members:
  double mLogBase; /**< TODO: describe */
  int mSubTickCount; /**< TODO: describe */
  
  // non-property members:
  double mLogBaseLnInv; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param range
   * @return double
   */
  virtual double getTickStep(const QCPRange &range) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tickStep
   * @return int
   */
  virtual int getSubTickCount(double tickStep) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param tickStep
   * @param range
   * @return QVector<double>
   */
  virtual QVector<double> createTickVector(double tickStep, const QCPRange &range) Q_DECL_OVERRIDE;
};

/* end of 'src/axis/axistickerlog.h' */


/* including file 'src/axis/axis.h', size 20230                              */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPGrid :public QCPLayerable
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(bool subGridVisible READ subGridVisible WRITE setSubGridVisible)
  Q_PROPERTY(bool antialiasedSubGrid READ antialiasedSubGrid WRITE setAntialiasedSubGrid)
  Q_PROPERTY(bool antialiasedZeroLine READ antialiasedZeroLine WRITE setAntialiasedZeroLine)
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen subGridPen READ subGridPen WRITE setSubGridPen)
  Q_PROPERTY(QPen zeroLinePen READ zeroLinePen WRITE setZeroLinePen)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentAxis
   */
  explicit QCPGrid(QCPAxis *parentAxis);
  
  // getters:
  /**
   * @brief
   *
   * @return bool
   */
  bool subGridVisible() const { return mSubGridVisible; }
  /**
   * @brief
   *
   * @return bool
   */
  bool antialiasedSubGrid() const { return mAntialiasedSubGrid; }
  /**
   * @brief
   *
   * @return bool
   */
  bool antialiasedZeroLine() const { return mAntialiasedZeroLine; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen subGridPen() const { return mSubGridPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen zeroLinePen() const { return mZeroLinePen; }
  
  // setters:
  /**
   * @brief
   *
   * @param visible
   */
  void setSubGridVisible(bool visible);
  /**
   * @brief
   *
   * @param enabled
   */
  void setAntialiasedSubGrid(bool enabled);
  /**
   * @brief
   *
   * @param enabled
   */
  void setAntialiasedZeroLine(bool enabled);
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSubGridPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setZeroLinePen(const QPen &pen);
  
protected:
  // property members:
  bool mSubGridVisible; /**< TODO: describe */
  bool mAntialiasedSubGrid, mAntialiasedZeroLine; /**< TODO: describe */
  QPen mPen, mSubGridPen, mZeroLinePen; /**< TODO: describe */
  
  // non-property members:
  QCPAxis *mParentAxis; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  void drawGridLines(QCPPainter *painter) const;
  /**
   * @brief
   *
   * @param painter
   */
  void drawSubGridLines(QCPPainter *painter) const;
  
  friend class QCPAxis;
};


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPAxis : public QCPLayerable
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(AxisType axisType READ axisType)
  Q_PROPERTY(QCPAxisRect* axisRect READ axisRect)
  Q_PROPERTY(ScaleType scaleType READ scaleType WRITE setScaleType NOTIFY scaleTypeChanged)
  Q_PROPERTY(QCPRange range READ range WRITE setRange NOTIFY rangeChanged)
  Q_PROPERTY(bool rangeReversed READ rangeReversed WRITE setRangeReversed)
  Q_PROPERTY(QSharedPointer<QCPAxisTicker> ticker READ ticker WRITE setTicker)
  Q_PROPERTY(bool ticks READ ticks WRITE setTicks)
  Q_PROPERTY(bool tickLabels READ tickLabels WRITE setTickLabels)
  Q_PROPERTY(int tickLabelPadding READ tickLabelPadding WRITE setTickLabelPadding)
  Q_PROPERTY(QFont tickLabelFont READ tickLabelFont WRITE setTickLabelFont)
  Q_PROPERTY(QColor tickLabelColor READ tickLabelColor WRITE setTickLabelColor)
  Q_PROPERTY(double tickLabelRotation READ tickLabelRotation WRITE setTickLabelRotation)
  Q_PROPERTY(LabelSide tickLabelSide READ tickLabelSide WRITE setTickLabelSide)
  Q_PROPERTY(QString numberFormat READ numberFormat WRITE setNumberFormat)
  Q_PROPERTY(int numberPrecision READ numberPrecision WRITE setNumberPrecision)
  Q_PROPERTY(QVector<double> tickVector READ tickVector)
  Q_PROPERTY(QVector<QString> tickVectorLabels READ tickVectorLabels)
  Q_PROPERTY(int tickLengthIn READ tickLengthIn WRITE setTickLengthIn)
  Q_PROPERTY(int tickLengthOut READ tickLengthOut WRITE setTickLengthOut)
  Q_PROPERTY(bool subTicks READ subTicks WRITE setSubTicks)
  Q_PROPERTY(int subTickLengthIn READ subTickLengthIn WRITE setSubTickLengthIn)
  Q_PROPERTY(int subTickLengthOut READ subTickLengthOut WRITE setSubTickLengthOut)
  Q_PROPERTY(QPen basePen READ basePen WRITE setBasePen)
  Q_PROPERTY(QPen tickPen READ tickPen WRITE setTickPen)
  Q_PROPERTY(QPen subTickPen READ subTickPen WRITE setSubTickPen)
  Q_PROPERTY(QFont labelFont READ labelFont WRITE setLabelFont)
  Q_PROPERTY(QColor labelColor READ labelColor WRITE setLabelColor)
  Q_PROPERTY(QString label READ label WRITE setLabel)
  Q_PROPERTY(int labelPadding READ labelPadding WRITE setLabelPadding)
  Q_PROPERTY(int padding READ padding WRITE setPadding)
  Q_PROPERTY(int offset READ offset WRITE setOffset)
  Q_PROPERTY(SelectableParts selectedParts READ selectedParts WRITE setSelectedParts NOTIFY selectionChanged)
  Q_PROPERTY(SelectableParts selectableParts READ selectableParts WRITE setSelectableParts NOTIFY selectableChanged)
  Q_PROPERTY(QFont selectedTickLabelFont READ selectedTickLabelFont WRITE setSelectedTickLabelFont)
  Q_PROPERTY(QFont selectedLabelFont READ selectedLabelFont WRITE setSelectedLabelFont)
  Q_PROPERTY(QColor selectedTickLabelColor READ selectedTickLabelColor WRITE setSelectedTickLabelColor)
  Q_PROPERTY(QColor selectedLabelColor READ selectedLabelColor WRITE setSelectedLabelColor)
  Q_PROPERTY(QPen selectedBasePen READ selectedBasePen WRITE setSelectedBasePen)
  Q_PROPERTY(QPen selectedTickPen READ selectedTickPen WRITE setSelectedTickPen)
  Q_PROPERTY(QPen selectedSubTickPen READ selectedSubTickPen WRITE setSelectedSubTickPen)
  Q_PROPERTY(QCPLineEnding lowerEnding READ lowerEnding WRITE setLowerEnding)
  Q_PROPERTY(QCPLineEnding upperEnding READ upperEnding WRITE setUpperEnding)
  Q_PROPERTY(QCPGrid* grid READ grid)
  /// \endcond
public:
  /*!
    Defines at which side of the axis rect the axis will appear. This also affects how the tick
    marks are drawn, on which side the labels are placed etc.
  */
  enum AxisType { atLeft    = 0x01  ///< <tt>0x01</tt> Axis is vertical and on the left side of the axis rect
                  ,atRight  = 0x02  ///< <tt>0x02</tt> Axis is vertical and on the right side of the axis rect
                  ,atTop    = 0x04  ///< <tt>0x04</tt> Axis is horizontal and on the top side of the axis rect
                  ,atBottom = 0x08  ///< <tt>0x08</tt> Axis is horizontal and on the bottom side of the axis rect
                };
  Q_ENUMS(AxisType)
  Q_FLAGS(AxisTypes)
  /**
   * @brief
   *
   */
  Q_DECLARE_FLAGS(AxisTypes, AxisType)
  /*!
    Defines on which side of the axis the tick labels (numbers) shall appear.
    
    \see setTickLabelSide
  */
  enum LabelSide { lsInside    ///< Tick labels will be displayed inside the axis rect and clipped to the inner axis rect
                   ,lsOutside  ///< Tick labels will be displayed outside the axis rect
                 };
  Q_ENUMS(LabelSide)
  /*!
    Defines the scale of an axis.
    \see setScaleType
  */
  enum ScaleType { stLinear       ///< Linear scaling
                   ,stLogarithmic ///< Logarithmic scaling with correspondingly transformed axis coordinates (possibly also \ref setTicker to a \ref QCPAxisTickerLog instance).
                 };
  Q_ENUMS(ScaleType)
  /*!
    Defines the selectable parts of an axis.
    \see setSelectableParts, setSelectedParts
  */
  enum SelectablePart { spNone        = 0      ///< None of the selectable parts
                        ,spAxis       = 0x001  ///< The axis backbone and tick marks
                        ,spTickLabels = 0x002  ///< Tick labels (numbers) of this axis (as a whole, not individually)
                        ,spAxisLabel  = 0x004  ///< The axis label
                      };
  Q_ENUMS(SelectablePart)
  Q_FLAGS(SelectableParts)
  /**
   * @brief
   *
   */
  Q_DECLARE_FLAGS(SelectableParts, SelectablePart)
  
  /**
   * @brief
   *
   * @param parent
   * @param type
   */
  explicit QCPAxis(QCPAxisRect *parent, AxisType type);
  /**
   * @brief
   *
   */
  virtual ~QCPAxis();
  
  // getters:
  /**
   * @brief
   *
   * @return AxisType
   */
  AxisType axisType() const { return mAxisType; }
  /**
   * @brief
   *
   * @return QCPAxisRect
   */
  QCPAxisRect *axisRect() const { return mAxisRect; }
  /**
   * @brief
   *
   * @return ScaleType
   */
  ScaleType scaleType() const { return mScaleType; }
  /**
   * @brief
   *
   * @return const QCPRange
   */
  const QCPRange range() const { return mRange; }
  /**
   * @brief
   *
   * @return bool
   */
  bool rangeReversed() const { return mRangeReversed; }
  /**
   * @brief
   *
   * @return QSharedPointer<QCPAxisTicker>
   */
  QSharedPointer<QCPAxisTicker> ticker() const { return mTicker; }
  /**
   * @brief
   *
   * @return bool
   */
  bool ticks() const { return mTicks; }
  /**
   * @brief
   *
   * @return bool
   */
  bool tickLabels() const { return mTickLabels; }
  /**
   * @brief
   *
   * @return int
   */
  int tickLabelPadding() const;
  /**
   * @brief
   *
   * @return QFont
   */
  QFont tickLabelFont() const { return mTickLabelFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor tickLabelColor() const { return mTickLabelColor; }
  /**
   * @brief
   *
   * @return double
   */
  double tickLabelRotation() const;
  /**
   * @brief
   *
   * @return LabelSide
   */
  LabelSide tickLabelSide() const;
  /**
   * @brief
   *
   * @return QString
   */
  QString numberFormat() const;
  /**
   * @brief
   *
   * @return int
   */
  int numberPrecision() const { return mNumberPrecision; }
  /**
   * @brief
   *
   * @return QVector<double>
   */
  QVector<double> tickVector() const { return mTickVector; }
  /**
   * @brief
   *
   * @return QVector<QString>
   */
  QVector<QString> tickVectorLabels() const { return mTickVectorLabels; }
  /**
   * @brief
   *
   * @return int
   */
  int tickLengthIn() const;
  /**
   * @brief
   *
   * @return int
   */
  int tickLengthOut() const;
  /**
   * @brief
   *
   * @return bool
   */
  bool subTicks() const { return mSubTicks; }
  /**
   * @brief
   *
   * @return int
   */
  int subTickLengthIn() const;
  /**
   * @brief
   *
   * @return int
   */
  int subTickLengthOut() const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen basePen() const { return mBasePen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen tickPen() const { return mTickPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen subTickPen() const { return mSubTickPen; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont labelFont() const { return mLabelFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor labelColor() const { return mLabelColor; }
  /**
   * @brief
   *
   * @return QString
   */
  QString label() const { return mLabel; }
  /**
   * @brief
   *
   * @return int
   */
  int labelPadding() const;
  /**
   * @brief
   *
   * @return int
   */
  int padding() const { return mPadding; }
  /**
   * @brief
   *
   * @return int
   */
  int offset() const;
  /**
   * @brief
   *
   * @return SelectableParts
   */
  SelectableParts selectedParts() const { return mSelectedParts; }
  /**
   * @brief
   *
   * @return SelectableParts
   */
  SelectableParts selectableParts() const { return mSelectableParts; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont selectedTickLabelFont() const { return mSelectedTickLabelFont; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont selectedLabelFont() const { return mSelectedLabelFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor selectedTickLabelColor() const { return mSelectedTickLabelColor; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor selectedLabelColor() const { return mSelectedLabelColor; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedBasePen() const { return mSelectedBasePen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedTickPen() const { return mSelectedTickPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedSubTickPen() const { return mSelectedSubTickPen; }
  /**
   * @brief
   *
   * @return QCPLineEnding
   */
  QCPLineEnding lowerEnding() const;
  /**
   * @brief
   *
   * @return QCPLineEnding
   */
  QCPLineEnding upperEnding() const;
  /**
   * @brief
   *
   * @return QCPGrid
   */
  QCPGrid *grid() const { return mGrid; }
  
  // setters:
  /**
   * @brief
   *
   * @param type
   */
  Q_SLOT void setScaleType(QCPAxis::ScaleType type);
  /**
   * @brief
   *
   * @param range
   */
  Q_SLOT void setRange(const QCPRange &range);
  /**
   * @brief
   *
   * @param lower
   * @param upper
   */
  void setRange(double lower, double upper);
  /**
   * @brief
   *
   * @param position
   * @param size
   * @param alignment
   */
  void setRange(double position, double size, Qt::AlignmentFlag alignment);
  /**
   * @brief
   *
   * @param lower
   */
  void setRangeLower(double lower);
  /**
   * @brief
   *
   * @param upper
   */
  void setRangeUpper(double upper);
  /**
   * @brief
   *
   * @param reversed
   */
  void setRangeReversed(bool reversed);
  /**
   * @brief
   *
   * @param ticker
   */
  void setTicker(QSharedPointer<QCPAxisTicker> ticker);
  /**
   * @brief
   *
   * @param show
   */
  void setTicks(bool show);
  /**
   * @brief
   *
   * @param show
   */
  void setTickLabels(bool show);
  /**
   * @brief
   *
   * @param padding
   */
  void setTickLabelPadding(int padding);
  /**
   * @brief
   *
   * @param font
   */
  void setTickLabelFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setTickLabelColor(const QColor &color);
  /**
   * @brief
   *
   * @param degrees
   */
  void setTickLabelRotation(double degrees);
  /**
   * @brief
   *
   * @param side
   */
  void setTickLabelSide(LabelSide side);
  /**
   * @brief
   *
   * @param formatCode
   */
  void setNumberFormat(const QString &formatCode);
  /**
   * @brief
   *
   * @param precision
   */
  void setNumberPrecision(int precision);
  /**
   * @brief
   *
   * @param inside
   * @param outside
   */
  void setTickLength(int inside, int outside=0);
  /**
   * @brief
   *
   * @param inside
   */
  void setTickLengthIn(int inside);
  /**
   * @brief
   *
   * @param outside
   */
  void setTickLengthOut(int outside);
  /**
   * @brief
   *
   * @param show
   */
  void setSubTicks(bool show);
  /**
   * @brief
   *
   * @param inside
   * @param outside
   */
  void setSubTickLength(int inside, int outside=0);
  /**
   * @brief
   *
   * @param inside
   */
  void setSubTickLengthIn(int inside);
  /**
   * @brief
   *
   * @param outside
   */
  void setSubTickLengthOut(int outside);
  /**
   * @brief
   *
   * @param pen
   */
  void setBasePen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setTickPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSubTickPen(const QPen &pen);
  /**
   * @brief
   *
   * @param font
   */
  void setLabelFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setLabelColor(const QColor &color);
  /**
   * @brief
   *
   * @param str
   */
  void setLabel(const QString &str);
  /**
   * @brief
   *
   * @param padding
   */
  void setLabelPadding(int padding);
  /**
   * @brief
   *
   * @param padding
   */
  void setPadding(int padding);
  /**
   * @brief
   *
   * @param offset
   */
  void setOffset(int offset);
  /**
   * @brief
   *
   * @param font
   */
  void setSelectedTickLabelFont(const QFont &font);
  /**
   * @brief
   *
   * @param font
   */
  void setSelectedLabelFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setSelectedTickLabelColor(const QColor &color);
  /**
   * @brief
   *
   * @param color
   */
  void setSelectedLabelColor(const QColor &color);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedBasePen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedTickPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedSubTickPen(const QPen &pen);
  /**
   * @brief
   *
   * @param selectableParts
   */
  Q_SLOT void setSelectableParts(const QCPAxis::SelectableParts &selectableParts);
  /**
   * @brief
   *
   * @param selectedParts
   */
  Q_SLOT void setSelectedParts(const QCPAxis::SelectableParts &selectedParts);
  /**
   * @brief
   *
   * @param ending
   */
  void setLowerEnding(const QCPLineEnding &ending);
  /**
   * @brief
   *
   * @param ending
   */
  void setUpperEnding(const QCPLineEnding &ending);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  // non-property methods:
  /**
   * @brief
   *
   * @return Qt::Orientation
   */
  Qt::Orientation orientation() const { return mOrientation; }
  /**
   * @brief
   *
   * @return int
   */
  int pixelOrientation() const { return rangeReversed() != (orientation()==Qt::Vertical) ? -1 : 1; }
  /**
   * @brief
   *
   * @param diff
   */
  void moveRange(double diff);
  /**
   * @brief
   *
   * @param factor
   */
  void scaleRange(double factor);
  /**
   * @brief
   *
   * @param factor
   * @param center
   */
  void scaleRange(double factor, double center);
  /**
   * @brief
   *
   * @param otherAxis
   * @param ratio
   */
  void setScaleRatio(const QCPAxis *otherAxis, double ratio=1.0);
  /**
   * @brief
   *
   * @param onlyVisiblePlottables
   */
  void rescale(bool onlyVisiblePlottables=false);
  /**
   * @brief
   *
   * @param value
   * @return double
   */
  double pixelToCoord(double value) const;
  /**
   * @brief
   *
   * @param value
   * @return double
   */
  double coordToPixel(double value) const;
  /**
   * @brief
   *
   * @param pos
   * @return SelectablePart
   */
  SelectablePart getPartAt(const QPointF &pos) const;
  /**
   * @brief
   *
   * @return QList<QCPAbstractPlottable *>
   */
  QList<QCPAbstractPlottable*> plottables() const;
  /**
   * @brief
   *
   * @return QList<QCPGraph *>
   */
  QList<QCPGraph*> graphs() const;
  /**
   * @brief
   *
   * @return QList<QCPAbstractItem *>
   */
  QList<QCPAbstractItem*> items() const;
  
  /**
   * @brief
   *
   * @param side
   * @return AxisType
   */
  static AxisType marginSideToAxisType(QCP::MarginSide side);
  /**
   * @brief
   *
   * @param type
   * @return Qt::Orientation
   */
  static Qt::Orientation orientation(AxisType type) { return type==atBottom||type==atTop ? Qt::Horizontal : Qt::Vertical; }
  /**
   * @brief
   *
   * @param type
   * @return AxisType
   */
  static AxisType opposite(AxisType type);
  
signals:
  /**
   * @brief
   *
   * @param newRange
   */
  void rangeChanged(const QCPRange &newRange);
  /**
   * @brief
   *
   * @param newRange
   * @param oldRange
   */
  void rangeChanged(const QCPRange &newRange, const QCPRange &oldRange);
  /**
   * @brief
   *
   * @param scaleType
   */
  void scaleTypeChanged(QCPAxis::ScaleType scaleType);
  /**
   * @brief
   *
   * @param parts
   */
  void selectionChanged(const QCPAxis::SelectableParts &parts);
  /**
   * @brief
   *
   * @param parts
   */
  void selectableChanged(const QCPAxis::SelectableParts &parts);

protected:
  // property members:
  // axis base:
  AxisType mAxisType; /**< TODO: describe */
  QCPAxisRect *mAxisRect; /**< TODO: describe */
  //int mOffset; // in QCPAxisPainter
  int mPadding; /**< TODO: describe */
  Qt::Orientation mOrientation; /**< TODO: describe */
  SelectableParts mSelectableParts, mSelectedParts; /**< TODO: describe */
  QPen mBasePen, mSelectedBasePen; /**< TODO: describe */
  //QCPLineEnding mLowerEnding, mUpperEnding; // in QCPAxisPainter
  // axis label:
  //int mLabelPadding; // in QCPAxisPainter
  QString mLabel; /**< TODO: describe */
  QFont mLabelFont, mSelectedLabelFont; /**< TODO: describe */
  QColor mLabelColor, mSelectedLabelColor; /**< TODO: describe */
  // tick labels:
  //int mTickLabelPadding; // in QCPAxisPainter
  bool mTickLabels; /**< TODO: describe */
  //double mTickLabelRotation; // in QCPAxisPainter
  QFont mTickLabelFont, mSelectedTickLabelFont; /**< TODO: describe */
  QColor mTickLabelColor, mSelectedTickLabelColor; /**< TODO: describe */
  int mNumberPrecision; /**< TODO: describe */
  QLatin1Char mNumberFormatChar; /**< TODO: describe */
  bool mNumberBeautifulPowers; /**< TODO: describe */
  //bool mNumberMultiplyCross; // QCPAxisPainter
  // ticks and subticks:
  bool mTicks; /**< TODO: describe */
  bool mSubTicks; /**< TODO: describe */
  //int mTickLengthIn, mTickLengthOut, mSubTickLengthIn, mSubTickLengthOut; // QCPAxisPainter
  QPen mTickPen, mSelectedTickPen; /**< TODO: describe */
  QPen mSubTickPen, mSelectedSubTickPen; /**< TODO: describe */
  // scale and range:
  QCPRange mRange; /**< TODO: describe */
  bool mRangeReversed; /**< TODO: describe */
  ScaleType mScaleType; /**< TODO: describe */
  
  // non-property members:
  QCPGrid *mGrid; /**< TODO: describe */
  QCPAxisPainterPrivate *mAxisPainter; /**< TODO: describe */
  QSharedPointer<QCPAxisTicker> mTicker; /**< TODO: describe */
  QVector<double> mTickVector; /**< TODO: describe */
  QVector<QString> mTickVectorLabels; /**< TODO: describe */
  QVector<double> mSubTickVector; /**< TODO: describe */
  bool mCachedMarginValid; /**< TODO: describe */
  int mCachedMargin; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @return int
   */
  virtual int calculateMargin();
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QCP::Interaction
   */
  virtual QCP::Interaction selectionCategory() const Q_DECL_OVERRIDE;
  // events:
  /**
   * @brief
   *
   * @param event
   * @param additive
   * @param details
   * @param selectionStateChanged
   */
  virtual void selectEvent(QMouseEvent *event, bool additive, const QVariant &details, bool *selectionStateChanged) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param selectionStateChanged
   */
  virtual void deselectEvent(bool *selectionStateChanged) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   */
  void setupTickVectors();
  /**
   * @brief
   *
   * @return QPen
   */
  QPen getBasePen() const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen getTickPen() const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen getSubTickPen() const;
  /**
   * @brief
   *
   * @return QFont
   */
  QFont getTickLabelFont() const;
  /**
   * @brief
   *
   * @return QFont
   */
  QFont getLabelFont() const;
  /**
   * @brief
   *
   * @return QColor
   */
  QColor getTickLabelColor() const;
  /**
   * @brief
   *
   * @return QColor
   */
  QColor getLabelColor() const;
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPAxis)
  
  friend class QCustomPlot;
  friend class QCPGrid;
  friend class QCPAxisRect;
};
Q_DECLARE_OPERATORS_FOR_FLAGS(QCPAxis::SelectableParts)
Q_DECLARE_OPERATORS_FOR_FLAGS(QCPAxis::AxisTypes)
Q_DECLARE_METATYPE(QCPAxis::AxisType)
Q_DECLARE_METATYPE(QCPAxis::LabelSide)
Q_DECLARE_METATYPE(QCPAxis::ScaleType)
Q_DECLARE_METATYPE(QCPAxis::SelectablePart)


/**
 * @brief
 *
 */
class QCPAxisPainterPrivate
{
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPAxisPainterPrivate(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPAxisPainterPrivate();
  
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter);
  /**
   * @brief
   *
   * @return int
   */
  virtual int size() const;
  /**
   * @brief
   *
   */
  void clearCache();
  
  /**
   * @brief
   *
   * @return QRect
   */
  QRect axisSelectionBox() const { return mAxisSelectionBox; }
  /**
   * @brief
   *
   * @return QRect
   */
  QRect tickLabelsSelectionBox() const { return mTickLabelsSelectionBox; }
  /**
   * @brief
   *
   * @return QRect
   */
  QRect labelSelectionBox() const { return mLabelSelectionBox; }
  
  // public property members:
  QCPAxis::AxisType type; /**< TODO: describe */
  QPen basePen; /**< TODO: describe */
  QCPLineEnding lowerEnding, upperEnding; // directly accessed by QCPAxis setters/getters /**< TODO: describe */
  int labelPadding; // directly accessed by QCPAxis setters/getters /**< TODO: describe */
  QFont labelFont; /**< TODO: describe */
  QColor labelColor; /**< TODO: describe */
  QString label; /**< TODO: describe */
  int tickLabelPadding; // directly accessed by QCPAxis setters/getters /**< TODO: describe */
  double tickLabelRotation; // directly accessed by QCPAxis setters/getters /**< TODO: describe */
  QCPAxis::LabelSide tickLabelSide; // directly accessed by QCPAxis setters/getters /**< TODO: describe */
  bool substituteExponent; /**< TODO: describe */
  bool numberMultiplyCross; // directly accessed by QCPAxis setters/getters /**< TODO: describe */
  int tickLengthIn, tickLengthOut, subTickLengthIn, subTickLengthOut; // directly accessed by QCPAxis setters/getters /**< TODO: describe */
  QPen tickPen, subTickPen; /**< TODO: describe */
  QFont tickLabelFont; /**< TODO: describe */
  QColor tickLabelColor; /**< TODO: describe */
  QRect axisRect, viewportRect; /**< TODO: describe */
  double offset; // directly accessed by QCPAxis setters/getters /**< TODO: describe */
  bool abbreviateDecimalPowers; /**< TODO: describe */
  bool reversedEndings; /**< TODO: describe */
  
  QVector<double> subTickPositions; /**< TODO: describe */
  QVector<double> tickPositions; /**< TODO: describe */
  QVector<QString> tickLabels; /**< TODO: describe */
  
protected:
  /**
   * @brief
   *
   */
  struct CachedLabel
  {
    QPointF offset; /**< TODO: describe */
    QPixmap pixmap; /**< TODO: describe */
  };
  /**
   * @brief
   *
   */
  struct TickLabelData
  {
    QString basePart, expPart, suffixPart; /**< TODO: describe */
    QRect baseBounds, expBounds, suffixBounds, totalBounds, rotatedTotalBounds; /**< TODO: describe */
    QFont baseFont, expFont; /**< TODO: describe */
  };
  QCustomPlot *mParentPlot; /**< TODO: describe */
  QByteArray mLabelParameterHash; // to determine whether mLabelCache needs to be cleared due to changed parameters /**< TODO: describe */
  QCache<QString, CachedLabel> mLabelCache; /**< TODO: describe */
  QRect mAxisSelectionBox, mTickLabelsSelectionBox, mLabelSelectionBox; /**< TODO: describe */
  
  /**
   * @brief
   *
   * @return QByteArray
   */
  virtual QByteArray generateLabelParameterHash() const;
  
  /**
   * @brief
   *
   * @param painter
   * @param position
   * @param distanceToAxis
   * @param text
   * @param tickLabelsSize
   */
  virtual void placeTickLabel(QCPPainter *painter, double position, int distanceToAxis, const QString &text, QSize *tickLabelsSize);
  /**
   * @brief
   *
   * @param painter
   * @param x
   * @param y
   * @param labelData
   */
  virtual void drawTickLabel(QCPPainter *painter, double x, double y, const TickLabelData &labelData) const;
  /**
   * @brief
   *
   * @param font
   * @param text
   * @return TickLabelData
   */
  virtual TickLabelData getTickLabelData(const QFont &font, const QString &text) const;
  /**
   * @brief
   *
   * @param labelData
   * @return QPointF
   */
  virtual QPointF getTickLabelDrawOffset(const TickLabelData &labelData) const;
  /**
   * @brief
   *
   * @param font
   * @param text
   * @param tickLabelsSize
   */
  virtual void getMaxTickLabelSize(const QFont &font, const QString &text, QSize *tickLabelsSize) const;
};

/* end of 'src/axis/axis.h' */


/* including file 'src/scatterstyle.h', size 7275                            */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPScatterStyle
{
  Q_GADGET
public:
  /*!
    Represents the various properties of a scatter style instance. For example, this enum is used
    to specify which properties of \ref QCPSelectionDecorator::setScatterStyle will be used when
    highlighting selected data points.

    Specific scatter properties can be transferred between \ref QCPScatterStyle instances via \ref
    setFromOther.
  */
  enum ScatterProperty { spNone  = 0x00  ///< <tt>0x00</tt> None
                         ,spPen   = 0x01  ///< <tt>0x01</tt> The pen property, see \ref setPen
                         ,spBrush = 0x02  ///< <tt>0x02</tt> The brush property, see \ref setBrush
                         ,spSize  = 0x04  ///< <tt>0x04</tt> The size property, see \ref setSize
                         ,spShape = 0x08  ///< <tt>0x08</tt> The shape property, see \ref setShape
                         ,spAll   = 0xFF  ///< <tt>0xFF</tt> All properties
                       };
  Q_ENUMS(ScatterProperty)
  Q_FLAGS(ScatterProperties)
  /**
   * @brief
   *
   */
  Q_DECLARE_FLAGS(ScatterProperties, ScatterProperty)

  /*!
    Defines the shape used for scatter points.

    On plottables/items that draw scatters, the sizes of these visualizations (with exception of
    \ref ssDot and \ref ssPixmap) can be controlled with the \ref setSize function. Scatters are
    drawn with the pen and brush specified with \ref setPen and \ref setBrush.
  */
  enum ScatterShape { ssNone       ///< no scatter symbols are drawn (e.g. in QCPGraph, data only represented with lines)
                      ,ssDot       ///< \enumimage{ssDot.png} a single pixel (use \ref ssDisc or \ref ssCircle if you want a round shape with a certain radius)
                      ,ssCross     ///< \enumimage{ssCross.png} a cross
                      ,ssPlus      ///< \enumimage{ssPlus.png} a plus
                      ,ssCircle    ///< \enumimage{ssCircle.png} a circle
                      ,ssDisc      ///< \enumimage{ssDisc.png} a circle which is filled with the pen's color (not the brush as with ssCircle)
                      ,ssSquare    ///< \enumimage{ssSquare.png} a square
                      ,ssDiamond   ///< \enumimage{ssDiamond.png} a diamond
                      ,ssStar      ///< \enumimage{ssStar.png} a star with eight arms, i.e. a combination of cross and plus
                      ,ssTriangle  ///< \enumimage{ssTriangle.png} an equilateral triangle, standing on baseline
                      ,ssTriangleInverted ///< \enumimage{ssTriangleInverted.png} an equilateral triangle, standing on corner
                      ,ssCrossSquare      ///< \enumimage{ssCrossSquare.png} a square with a cross inside
                      ,ssPlusSquare       ///< \enumimage{ssPlusSquare.png} a square with a plus inside
                      ,ssCrossCircle      ///< \enumimage{ssCrossCircle.png} a circle with a cross inside
                      ,ssPlusCircle       ///< \enumimage{ssPlusCircle.png} a circle with a plus inside
                      ,ssPeace     ///< \enumimage{ssPeace.png} a circle, with one vertical and two downward diagonal lines
                      ,ssPixmap    ///< a custom pixmap specified by \ref setPixmap, centered on the data point coordinates
                      ,ssCustom    ///< custom painter operations are performed per scatter (As QPainterPath, see \ref setCustomPath)
                    };
  Q_ENUMS(ScatterShape)

  /**
   * @brief
   *
   */
  QCPScatterStyle();
  /**
   * @brief
   *
   * @param shape
   * @param size
   */
  QCPScatterStyle(ScatterShape shape, double size=6);
  /**
   * @brief
   *
   * @param shape
   * @param color
   * @param size
   */
  QCPScatterStyle(ScatterShape shape, const QColor &color, double size);
  /**
   * @brief
   *
   * @param shape
   * @param color
   * @param fill
   * @param size
   */
  QCPScatterStyle(ScatterShape shape, const QColor &color, const QColor &fill, double size);
  /**
   * @brief
   *
   * @param shape
   * @param pen
   * @param brush
   * @param size
   */
  QCPScatterStyle(ScatterShape shape, const QPen &pen, const QBrush &brush, double size);
  /**
   * @brief
   *
   * @param pixmap
   */
  QCPScatterStyle(const QPixmap &pixmap);
  /**
   * @brief
   *
   * @param customPath
   * @param pen
   * @param brush
   * @param size
   */
  QCPScatterStyle(const QPainterPath &customPath, const QPen &pen, const QBrush &brush=Qt::NoBrush, double size=6);
  
  // getters:
  /**
   * @brief
   *
   * @return double
   */
  double size() const { return mSize; }
  /**
   * @brief
   *
   * @return ScatterShape
   */
  ScatterShape shape() const { return mShape; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return QPixmap
   */
  QPixmap pixmap() const { return mPixmap; }
  /**
   * @brief
   *
   * @return QPainterPath
   */
  QPainterPath customPath() const { return mCustomPath; }

  // setters:
  /**
   * @brief
   *
   * @param other
   * @param properties
   */
  void setFromOther(const QCPScatterStyle &other, ScatterProperties properties);
  /**
   * @brief
   *
   * @param size
   */
  void setSize(double size);
  /**
   * @brief
   *
   * @param shape
   */
  void setShape(ScatterShape shape);
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param pixmap
   */
  void setPixmap(const QPixmap &pixmap);
  /**
   * @brief
   *
   * @param customPath
   */
  void setCustomPath(const QPainterPath &customPath);

  // non-property methods:
  /**
   * @brief
   *
   * @return bool
   */
  bool isNone() const { return mShape == ssNone; }
  /**
   * @brief
   *
   * @return bool
   */
  bool isPenDefined() const { return mPenDefined; }
  /**
   * @brief
   *
   */
  void undefinePen();
  /**
   * @brief
   *
   * @param painter
   * @param defaultPen
   */
  void applyTo(QCPPainter *painter, const QPen &defaultPen) const;
  /**
   * @brief
   *
   * @param painter
   * @param pos
   */
  void drawShape(QCPPainter *painter, const QPointF &pos) const;
  /**
   * @brief
   *
   * @param painter
   * @param x
   * @param y
   */
  void drawShape(QCPPainter *painter, double x, double y) const;

protected:
  // property members:
  double mSize; /**< TODO: describe */
  ScatterShape mShape; /**< TODO: describe */
  QPen mPen; /**< TODO: describe */
  QBrush mBrush; /**< TODO: describe */
  QPixmap mPixmap; /**< TODO: describe */
  QPainterPath mCustomPath; /**< TODO: describe */
  
  // non-property members:
  bool mPenDefined; /**< TODO: describe */
};
Q_DECLARE_TYPEINFO(QCPScatterStyle, Q_MOVABLE_TYPE);
Q_DECLARE_OPERATORS_FOR_FLAGS(QCPScatterStyle::ScatterProperties)
Q_DECLARE_METATYPE(QCPScatterStyle::ScatterProperty)
Q_DECLARE_METATYPE(QCPScatterStyle::ScatterShape)

/* end of 'src/scatterstyle.h' */


/* including file 'src/datacontainer.h', size 4535                           */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

/*! \relates QCPDataContainer
  Returns whether the sort key of \a a is less than the sort key of \a b.

  \see QCPDataContainer::sort
*/
template <class DataType>
inline bool qcpLessThanSortKey(const DataType &a, const DataType &b) { return a.sortKey() < b.sortKey(); }

template <class DataType>
/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPDataContainer
{
public:
  /**
   * @brief
   *
   */
  typedef typename QVector<DataType>::const_iterator const_iterator;
  /**
   * @brief
   *
   */
  typedef typename QVector<DataType>::iterator iterator;
  
  /**
   * @brief
   *
   */
  QCPDataContainer();
  
  // getters:
  /**
   * @brief
   *
   * @return int
   */
  int size() const { return mData.size()-mPreallocSize; }
  /**
   * @brief
   *
   * @return bool
   */
  bool isEmpty() const { return size() == 0; }
  /**
   * @brief
   *
   * @return bool
   */
  bool autoSqueeze() const { return mAutoSqueeze; }
  
  // setters:
  /**
   * @brief
   *
   * @param enabled
   */
  void setAutoSqueeze(bool enabled);
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param data
   */
  void set(const QCPDataContainer<DataType> &data);
  /**
   * @brief
   *
   * @param data
   * @param alreadySorted
   */
  void set(const QVector<DataType> &data, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param data
   */
  void add(const QCPDataContainer<DataType> &data);
  /**
   * @brief
   *
   * @param data
   * @param alreadySorted
   */
  void add(const QVector<DataType> &data, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param data
   */
  void add(const DataType &data);
  /**
   * @brief
   *
   * @param sortKey
   */
  void removeBefore(double sortKey);
  /**
   * @brief
   *
   * @param sortKey
   */
  void removeAfter(double sortKey);
  /**
   * @brief
   *
   * @param sortKeyFrom
   * @param sortKeyTo
   */
  void remove(double sortKeyFrom, double sortKeyTo);
  /**
   * @brief
   *
   * @param sortKey
   */
  void remove(double sortKey);
  /**
   * @brief
   *
   */
  void clear();
  /**
   * @brief
   *
   */
  void sort();
  /**
   * @brief
   *
   * @param preAllocation
   * @param postAllocation
   */
  void squeeze(bool preAllocation=true, bool postAllocation=true);
  
  /**
   * @brief
   *
   * @return const_iterator
   */
  const_iterator constBegin() const { return mData.constBegin()+mPreallocSize; }
  /**
   * @brief
   *
   * @return const_iterator
   */
  const_iterator constEnd() const { return mData.constEnd(); }
  /**
   * @brief
   *
   * @return iterator
   */
  iterator begin() { return mData.begin()+mPreallocSize; }
  /**
   * @brief
   *
   * @return iterator
   */
  iterator end() { return mData.end(); }
  /**
   * @brief
   *
   * @param sortKey
   * @param expandedRange
   * @return const_iterator
   */
  const_iterator findBegin(double sortKey, bool expandedRange=true) const;
  /**
   * @brief
   *
   * @param sortKey
   * @param expandedRange
   * @return const_iterator
   */
  const_iterator findEnd(double sortKey, bool expandedRange=true) const;
  /**
   * @brief
   *
   * @param index
   * @return const_iterator
   */
  const_iterator at(int index) const { return constBegin()+qBound(0, index, size()); }
  /**
   * @brief
   *
   * @param foundRange
   * @param signDomain
   * @return QCPRange
   */
  QCPRange keyRange(bool &foundRange, QCP::SignDomain signDomain=QCP::sdBoth);
  /**
   * @brief
   *
   * @param foundRange
   * @param signDomain
   * @param inKeyRange
   * @return QCPRange
   */
  QCPRange valueRange(bool &foundRange, QCP::SignDomain signDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange());
  /**
   * @brief
   *
   * @return QCPDataRange
   */
  QCPDataRange dataRange() const { return QCPDataRange(0, size()); }
  /**
   * @brief
   *
   * @param begin
   * @param end
   * @param dataRange
   */
  void limitIteratorsToDataRange(const_iterator &begin, const_iterator &end, const QCPDataRange &dataRange) const;
  
protected:
  // property members:
  bool mAutoSqueeze; /**< TODO: describe */
  
  // non-property memebers:
  QVector<DataType> mData; /**< TODO: describe */
  int mPreallocSize; /**< TODO: describe */
  int mPreallocIteration; /**< TODO: describe */
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param minimumPreallocSize
   */
  void preallocateGrow(int minimumPreallocSize);
  /**
   * @brief
   *
   */
  void performAutoSqueeze();
};

// include implementation in header since it is a class template:

/* including file 'src/datacontainer.cpp', size 31224                        */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPDataContainer
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPDataContainer
  \brief The generic data container for one-dimensional plottables

  This class template provides a fast container for data storage of one-dimensional data. The data
  type is specified as template parameter (called \a DataType in the following) and must provide
  some methods as described in the \ref qcpdatacontainer-datatype "next section".

  The data is stored in a sorted fashion, which allows very quick lookups by the sorted key as well
  as retrieval of ranges (see \ref findBegin, \ref findEnd, \ref keyRange) using binary search. The
  container uses a preallocation and a postallocation scheme, such that appending and prepending
  data (with respect to the sort key) is very fast and minimizes reallocations. If data is added
  which needs to be inserted between existing keys, the merge usually can be done quickly too,
  using the fact that existing data is always sorted. The user can further improve performance by
  specifying that added data is already itself sorted by key, if he can guarantee that this is the
  case (see for example \ref add(const QVector<DataType> &data, bool alreadySorted)).

  The data can be accessed with the provided const iterators (\ref constBegin, \ref constEnd). If
  it is necessary to alter existing data in-place, the non-const iterators can be used (\ref begin,
  \ref end). Changing data members that are not the sort key (for most data types called \a key) is
  safe from the container's perspective.

  Great care must be taken however if the sort key is modified through the non-const iterators. For
  performance reasons, the iterators don't automatically cause a re-sorting upon their
  manipulation. It is thus the responsibility of the user to leave the container in a sorted state
  when finished with the data manipulation, before calling any other methods on the container. A
  complete re-sort (e.g. after finishing all sort key manipulation) can be done by calling \ref
  sort. Failing to do so can not be detected by the container efficiently and will cause both
  rendering artifacts and potential data loss.

  Implementing one-dimensional plottables that make use of a \ref QCPDataContainer<T> is usually
  done by subclassing from \ref QCPAbstractPlottable1D "QCPAbstractPlottable1D<T>", which
  introduces an according \a mDataContainer member and some convenience methods.

  \section qcpdatacontainer-datatype Requirements for the DataType template parameter

  The template parameter <tt>DataType</tt> is the type of the stored data points. It must be
  trivially copyable and have the following public methods, preferably inline:

  \li <tt>double sortKey() const</tt>\n Returns the member variable of this data point that is the
  sort key, defining the ordering in the container. Often this variable is simply called \a key.

  \li <tt>static DataType fromSortKey(double sortKey)</tt>\n Returns a new instance of the data
  type initialized with its sort key set to \a sortKey.

  \li <tt>static bool sortKeyIsMainKey()</tt>\n Returns true if the sort key is equal to the main
  key (see method \c mainKey below). For most plottables this is the case. It is not the case for
  example for \ref QCPCurve, which uses \a t as sort key and \a key as main key. This is the reason
  why QCPCurve unlike QCPGraph can display parametric curves with loops.

  \li <tt>double mainKey() const</tt>\n Returns the variable of this data point considered the main
  key. This is commonly the variable that is used as the coordinate of this data point on the key
  axis of the plottable. This method is used for example when determining the automatic axis
  rescaling of key axes (\ref QCPAxis::rescale).

  \li <tt>double mainValue() const</tt>\n Returns the variable of this data point considered the
  main value. This is commonly the variable that is used as the coordinate of this data point on
  the value axis of the plottable.

  \li <tt>QCPRange valueRange() const</tt>\n Returns the range this data point spans in the value
  axis coordinate. If the data is single-valued (e.g. QCPGraphData), this is simply a range with
  both lower and upper set to the main data point value. However if the data points can represent
  multiple values at once (e.g QCPFinancialData with its \a high, \a low, \a open and \a close
  values at each \a key) this method should return the range those values span. This method is used
  for example when determining the automatic axis rescaling of value axes (\ref
  QCPAxis::rescale).
*/

/* start documentation of inline functions */

/*! \fn int QCPDataContainer<DataType>::size() const
  
  Returns the number of data points in the container.
*/

/*! \fn bool QCPDataContainer<DataType>::isEmpty() const
  
  Returns whether this container holds no data points.
*/

/*! \fn QCPDataContainer::const_iterator QCPDataContainer<DataType>::constBegin() const
  
  Returns a const iterator to the first data point in this container.
*/

/*! \fn QCPDataContainer::const_iterator QCPDataContainer<DataType>::constEnd() const
  
  Returns a const iterator to the element past the last data point in this container.
*/

/*! \fn QCPDataContainer::iterator QCPDataContainer<DataType>::begin() const
  
  Returns a non-const iterator to the first data point in this container.

  You can manipulate the data points in-place through the non-const iterators, but great care must
  be taken when manipulating the sort key of a data point, see \ref sort, or the detailed
  description of this class.
*/

/*! \fn QCPDataContainer::iterator QCPDataContainer<DataType>::end() const
  
  Returns a non-const iterator to the element past the last data point in this container.
  
  You can manipulate the data points in-place through the non-const iterators, but great care must
  be taken when manipulating the sort key of a data point, see \ref sort, or the detailed
  description of this class.
*/

/*! \fn QCPDataContainer::const_iterator QCPDataContainer<DataType>::at(int index) const

  Returns a const iterator to the element with the specified \a index. If \a index points beyond
  the available elements in this container, returns \ref constEnd, i.e. an iterator past the last
  valid element.

  You can use this method to easily obtain iterators from a \ref QCPDataRange, see the \ref
  dataselection-accessing "data selection page" for an example.
*/

/*! \fn QCPDataRange QCPDataContainer::dataRange() const

  Returns a \ref QCPDataRange encompassing the entire data set of this container. This means the
  begin index of the returned range is 0, and the end index is \ref size.
*/

/* end documentation of inline functions */

/*!
  Constructs a QCPDataContainer used for plottable classes that represent a series of key-sorted
  data
*/
template <class DataType>
QCPDataContainer<DataType>::QCPDataContainer() :
  mAutoSqueeze(true),
  mPreallocSize(0),
  mPreallocIteration(0)
{
}

/*!
  Sets whether the container automatically decides when to release memory from its post- and
  preallocation pools when data points are removed. By default this is enabled and for typical
  applications shouldn't be changed.
  
  If auto squeeze is disabled, you can manually decide when to release pre-/postallocation with
  \ref squeeze.
*/
template <class DataType>
void QCPDataContainer<DataType>::setAutoSqueeze(bool enabled)
{
  if (mAutoSqueeze != enabled)
  {
    mAutoSqueeze = enabled;
    if (mAutoSqueeze)
      performAutoSqueeze();
  }
}

/*! \overload
  
  Replaces the current data in this container with the provided \a data.
  
  \see add, remove
*/
template <class DataType>
void QCPDataContainer<DataType>::set(const QCPDataContainer<DataType> &data)
{
  clear();
  add(data);
}

/*! \overload
  
  Replaces the current data in this container with the provided \a data

  If you can guarantee that the data points in \a data have ascending order with respect to the
  DataType's sort key, set \a alreadySorted to true to avoid an unnecessary sorting run.
  
  \see add, remove
*/
template <class DataType>
void QCPDataContainer<DataType>::set(const QVector<DataType> &data, bool alreadySorted)
{
  mData = data;
  mPreallocSize = 0;
  mPreallocIteration = 0;
  if (!alreadySorted)
    sort();
}

/*! \overload
  
  Adds the provided \a data to the current data in this container.
  
  \see set, remove
*/
template <class DataType>
void QCPDataContainer<DataType>::add(const QCPDataContainer<DataType> &data)
{
  if (data.isEmpty())
    return;
  
  const int n = data.size();
  const int oldSize = size();
  
  if (oldSize > 0 && !qcpLessThanSortKey<DataType>(*constBegin(), *(data.constEnd()-1))) // prepend if new data keys are all smaller than or equal to existing ones
  {
    if (mPreallocSize < n)
      preallocateGrow(n);
    mPreallocSize -= n;
    std::copy(data.constBegin(), data.constEnd(), begin());
  } else // don't need to prepend, so append and merge if necessary
  {
    mData.resize(mData.size()+n);
    std::copy(data.constBegin(), data.constEnd(), end()-n);
    if (oldSize > 0 && !qcpLessThanSortKey<DataType>(*(constEnd()-n-1), *(constEnd()-n))) // if appended range keys aren't all greater than existing ones, merge the two partitions
      std::inplace_merge(begin(), end()-n, end(), qcpLessThanSortKey<DataType>);
  }
}

/*!
  Adds the provided data points in \a data to the current data.
  
  If you can guarantee that the data points in \a data have ascending order with respect to the
  DataType's sort key, set \a alreadySorted to true to avoid an unnecessary sorting run.
  
  \see set, remove
*/
template <class DataType>
void QCPDataContainer<DataType>::add(const QVector<DataType> &data, bool alreadySorted)
{
  if (data.isEmpty())
    return;
  if (isEmpty())
  {
    set(data, alreadySorted);
    return;
  }
  
  const int n = data.size();
  const int oldSize = size();
  
  if (alreadySorted && oldSize > 0 && !qcpLessThanSortKey<DataType>(*constBegin(), *(data.constEnd()-1))) // prepend if new data is sorted and keys are all smaller than or equal to existing ones
  {
    if (mPreallocSize < n)
      preallocateGrow(n);
    mPreallocSize -= n;
    std::copy(data.constBegin(), data.constEnd(), begin());
  } else // don't need to prepend, so append and then sort and merge if necessary
  {
    mData.resize(mData.size()+n);
    std::copy(data.constBegin(), data.constEnd(), end()-n);
    if (!alreadySorted) // sort appended subrange if it wasn't already sorted
      std::sort(end()-n, end(), qcpLessThanSortKey<DataType>);
    if (oldSize > 0 && !qcpLessThanSortKey<DataType>(*(constEnd()-n-1), *(constEnd()-n))) // if appended range keys aren't all greater than existing ones, merge the two partitions
      std::inplace_merge(begin(), end()-n, end(), qcpLessThanSortKey<DataType>);
  }
}

/*! \overload
  
  Adds the provided single data point to the current data.
  
  \see remove
*/
template <class DataType>
void QCPDataContainer<DataType>::add(const DataType &data)
{
  if (isEmpty() || !qcpLessThanSortKey<DataType>(data, *(constEnd()-1))) // quickly handle appends if new data key is greater or equal to existing ones
  {
    mData.append(data);
  } else if (qcpLessThanSortKey<DataType>(data, *constBegin()))  // quickly handle prepends using preallocated space
  {
    if (mPreallocSize < 1)
      preallocateGrow(1);
    --mPreallocSize;
    *begin() = data;
  } else // handle inserts, maintaining sorted keys
  {
    QCPDataContainer<DataType>::iterator insertionPoint = std::lower_bound(begin(), end(), data, qcpLessThanSortKey<DataType>);
    mData.insert(insertionPoint, data);
  }
}

/*!
  Removes all data points with (sort-)keys smaller than or equal to \a sortKey.
  
  \see removeAfter, remove, clear
*/
template <class DataType>
void QCPDataContainer<DataType>::removeBefore(double sortKey)
{
  QCPDataContainer<DataType>::iterator it = begin();
  QCPDataContainer<DataType>::iterator itEnd = std::lower_bound(begin(), end(), DataType::fromSortKey(sortKey), qcpLessThanSortKey<DataType>);
  mPreallocSize += itEnd-it; // don't actually delete, just add it to the preallocated block (if it gets too large, squeeze will take care of it)
  if (mAutoSqueeze)
    performAutoSqueeze();
}

/*!
  Removes all data points with (sort-)keys greater than or equal to \a sortKey.

  \see removeBefore, remove, clear
*/
template <class DataType>
void QCPDataContainer<DataType>::removeAfter(double sortKey)
{
  QCPDataContainer<DataType>::iterator it = std::upper_bound(begin(), end(), DataType::fromSortKey(sortKey), qcpLessThanSortKey<DataType>);
  QCPDataContainer<DataType>::iterator itEnd = end();
  mData.erase(it, itEnd); // typically adds it to the postallocated block
  if (mAutoSqueeze)
    performAutoSqueeze();
}

/*!
  Removes all data points with (sort-)keys between \a sortKeyFrom and \a sortKeyTo. if \a
  sortKeyFrom is greater or equal to \a sortKeyTo, the function does nothing. To remove a single
  data point with known (sort-)key, use \ref remove(double sortKey).
  
  \see removeBefore, removeAfter, clear
*/
template <class DataType>
void QCPDataContainer<DataType>::remove(double sortKeyFrom, double sortKeyTo)
{
  if (sortKeyFrom >= sortKeyTo || isEmpty())
    return;
  
  QCPDataContainer<DataType>::iterator it = std::lower_bound(begin(), end(), DataType::fromSortKey(sortKeyFrom), qcpLessThanSortKey<DataType>);
  QCPDataContainer<DataType>::iterator itEnd = std::upper_bound(it, end(), DataType::fromSortKey(sortKeyTo), qcpLessThanSortKey<DataType>);
  mData.erase(it, itEnd);
  if (mAutoSqueeze)
    performAutoSqueeze();
}

/*! \overload
  
  Removes a single data point at \a sortKey. If the position is not known with absolute (binary)
  precision, consider using \ref remove(double sortKeyFrom, double sortKeyTo) with a small
  fuzziness interval around the suspected position, depeding on the precision with which the
  (sort-)key is known.
  
  \see removeBefore, removeAfter, clear
*/
template <class DataType>
void QCPDataContainer<DataType>::remove(double sortKey)
{
  QCPDataContainer::iterator it = std::lower_bound(begin(), end(), DataType::fromSortKey(sortKey), qcpLessThanSortKey<DataType>);
  if (it != end() && it->sortKey() == sortKey)
  {
    if (it == begin())
      ++mPreallocSize; // don't actually delete, just add it to the preallocated block (if it gets too large, squeeze will take care of it)
    else
      mData.erase(it);
  }
  if (mAutoSqueeze)
    performAutoSqueeze();
}

/*!
  Removes all data points.
  
  \see remove, removeAfter, removeBefore
*/
template <class DataType>
void QCPDataContainer<DataType>::clear()
{
  mData.clear();
  mPreallocIteration = 0;
  mPreallocSize = 0;
}

/*!
  Re-sorts all data points in the container by their sort key.

  When setting, adding or removing points using the QCPDataContainer interface (\ref set, \ref add,
  \ref remove, etc.), the container makes sure to always stay in a sorted state such that a full
  resort is never necessary. However, if you choose to directly manipulate the sort key on data
  points by accessing and modifying it through the non-const iterators (\ref begin, \ref end), it
  is your responsibility to bring the container back into a sorted state before any other methods
  are called on it. This can be achieved by calling this method immediately after finishing the
  sort key manipulation.
*/
template <class DataType>
void QCPDataContainer<DataType>::sort()
{
  std::sort(begin(), end(), qcpLessThanSortKey<DataType>);
}

/*!
  Frees all unused memory that is currently in the preallocation and postallocation pools.
  
  Note that QCPDataContainer automatically decides whether squeezing is necessary, if \ref
  setAutoSqueeze is left enabled. It should thus not be necessary to use this method for typical
  applications.
  
  The parameters \a preAllocation and \a postAllocation control whether pre- and/or post allocation
  should be freed, respectively.
*/
template <class DataType>
void QCPDataContainer<DataType>::squeeze(bool preAllocation, bool postAllocation)
{
  if (preAllocation)
  {
    if (mPreallocSize > 0)
    {
      std::copy(begin(), end(), mData.begin());
      mData.resize(size());
      mPreallocSize = 0;
    }
    mPreallocIteration = 0;
  }
  if (postAllocation)
    mData.squeeze();
}

/*!
  Returns an iterator to the data point with a (sort-)key that is equal to, just below, or just
  above \a sortKey. If \a expandedRange is true, the data point just below \a sortKey will be
  considered, otherwise the one just above.

  This can be used in conjunction with \ref findEnd to iterate over data points within a given key
  range, including or excluding the bounding data points that are just beyond the specified range.

  If \a expandedRange is true but there are no data points below \a sortKey, \ref constBegin is
  returned.

  If the container is empty, returns \ref constEnd.

  \see findEnd, QCPPlottableInterface1D::findBegin
*/
template <class DataType>
typename QCPDataContainer<DataType>::const_iterator QCPDataContainer<DataType>::findBegin(double sortKey, bool expandedRange) const
{
  if (isEmpty())
    return constEnd();
  
  QCPDataContainer<DataType>::const_iterator it = std::lower_bound(constBegin(), constEnd(), DataType::fromSortKey(sortKey), qcpLessThanSortKey<DataType>);
  if (expandedRange && it != constBegin()) // also covers it == constEnd case, and we know --constEnd is valid because mData isn't empty
    --it;
  return it;
}

/*!
  Returns an iterator to the element after the data point with a (sort-)key that is equal to, just
  above or just below \a sortKey. If \a expandedRange is true, the data point just above \a sortKey
  will be considered, otherwise the one just below.

  This can be used in conjunction with \ref findBegin to iterate over data points within a given
  key range, including the bounding data points that are just below and above the specified range.

  If \a expandedRange is true but there are no data points above \a sortKey, \ref constEnd is
  returned.

  If the container is empty, \ref constEnd is returned.

  \see findBegin, QCPPlottableInterface1D::findEnd
*/
template <class DataType>
typename QCPDataContainer<DataType>::const_iterator QCPDataContainer<DataType>::findEnd(double sortKey, bool expandedRange) const
{
  if (isEmpty())
    return constEnd();
  
  QCPDataContainer<DataType>::const_iterator it = std::upper_bound(constBegin(), constEnd(), DataType::fromSortKey(sortKey), qcpLessThanSortKey<DataType>);
  if (expandedRange && it != constEnd())
    ++it;
  return it;
}

/*!
  Returns the range encompassed by the (main-)key coordinate of all data points. The output
  parameter \a foundRange indicates whether a sensible range was found. If this is false, you
  should not use the returned QCPRange (e.g. the data container is empty or all points have the
  same key).
  
  Use \a signDomain to control which sign of the key coordinates should be considered. This is
  relevant e.g. for logarithmic plots which can mathematically only display one sign domain at a
  time.
  
  If the DataType reports that its main key is equal to the sort key (\a sortKeyIsMainKey), as is
  the case for most plottables, this method uses this fact and finds the range very quickly.
  
  \see valueRange
*/
template <class DataType>
QCPRange QCPDataContainer<DataType>::keyRange(bool &foundRange, QCP::SignDomain signDomain)
{
  if (isEmpty())
  {
    foundRange = false;
    return QCPRange();
  }
  QCPRange range;
  bool haveLower = false;
  bool haveUpper = false;
  double current;
  
  QCPDataContainer<DataType>::const_iterator it = constBegin();
  QCPDataContainer<DataType>::const_iterator itEnd = constEnd();
  if (signDomain == QCP::sdBoth) // range may be anywhere
  {
    if (DataType::sortKeyIsMainKey()) // if DataType is sorted by main key (e.g. QCPGraph, but not QCPCurve), use faster algorithm by finding just first and last key with non-NaN value
    {
      while (it != itEnd) // find first non-nan going up from left
      {
        if (!qIsNaN(it->mainValue()))
        {
          range.lower = it->mainKey();
          haveLower = true;
          break;
        }
        ++it;
      }
      it = itEnd;
      while (it != constBegin()) // find first non-nan going down from right
      {
        --it;
        if (!qIsNaN(it->mainValue()))
        {
          range.upper = it->mainKey();
          haveUpper = true;
          break;
        }
      }
    } else // DataType is not sorted by main key, go through all data points and accordingly expand range
    {
      while (it != itEnd)
      {
        if (!qIsNaN(it->mainValue()))
        {
          current = it->mainKey();
          if (current < range.lower || !haveLower)
          {
            range.lower = current;
            haveLower = true;
          }
          if (current > range.upper || !haveUpper)
          {
            range.upper = current;
            haveUpper = true;
          }
        }
        ++it;
      }
    }
  } else if (signDomain == QCP::sdNegative) // range may only be in the negative sign domain
  {
    while (it != itEnd)
    {
      if (!qIsNaN(it->mainValue()))
      {
        current = it->mainKey();
        if ((current < range.lower || !haveLower) && current < 0)
        {
          range.lower = current;
          haveLower = true;
        }
        if ((current > range.upper || !haveUpper) && current < 0)
        {
          range.upper = current;
          haveUpper = true;
        }
      }
      ++it;
    }
  } else if (signDomain == QCP::sdPositive) // range may only be in the positive sign domain
  {
    while (it != itEnd)
    {
      if (!qIsNaN(it->mainValue()))
      {
        current = it->mainKey();
        if ((current < range.lower || !haveLower) && current > 0)
        {
          range.lower = current;
          haveLower = true;
        }
        if ((current > range.upper || !haveUpper) && current > 0)
        {
          range.upper = current;
          haveUpper = true;
        }
      }
      ++it;
    }
  }
  
  foundRange = haveLower && haveUpper;
  return range;
}

/*!
  Returns the range encompassed by the value coordinates of the data points in the specified key
  range (\a inKeyRange), using the full \a DataType::valueRange reported by the data points. The
  output parameter \a foundRange indicates whether a sensible range was found. If this is false,
  you should not use the returned QCPRange (e.g. the data container is empty or all points have the
  same value).

  If \a inKeyRange has both lower and upper bound set to zero (is equal to <tt>QCPRange()</tt>),
  all data points are considered, without any restriction on the keys.

  Use \a signDomain to control which sign of the value coordinates should be considered. This is
  relevant e.g. for logarithmic plots which can mathematically only display one sign domain at a
  time.

  \see keyRange
*/
template <class DataType>
QCPRange QCPDataContainer<DataType>::valueRange(bool &foundRange, QCP::SignDomain signDomain, const QCPRange &inKeyRange)
{
  if (isEmpty())
  {
    foundRange = false;
    return QCPRange();
  }
  QCPRange range;
  const bool restrictKeyRange = inKeyRange != QCPRange();
  bool haveLower = false;
  bool haveUpper = false;
  QCPRange current;
  QCPDataContainer<DataType>::const_iterator itBegin = constBegin();
  QCPDataContainer<DataType>::const_iterator itEnd = constEnd();
  if (DataType::sortKeyIsMainKey() && restrictKeyRange)
  {
    itBegin = findBegin(inKeyRange.lower);
    itEnd = findEnd(inKeyRange.upper);
  }
  if (signDomain == QCP::sdBoth) // range may be anywhere
  {
    for (QCPDataContainer<DataType>::const_iterator it = itBegin; it != itEnd; ++it)
    {
      if (restrictKeyRange && (it->mainKey() < inKeyRange.lower || it->mainKey() > inKeyRange.upper))
        continue;
      current = it->valueRange();
      if ((current.lower < range.lower || !haveLower) && !qIsNaN(current.lower))
      {
        range.lower = current.lower;
        haveLower = true;
      }
      if ((current.upper > range.upper || !haveUpper) && !qIsNaN(current.upper))
      {
        range.upper = current.upper;
        haveUpper = true;
      }
    }
  } else if (signDomain == QCP::sdNegative) // range may only be in the negative sign domain
  {
    for (QCPDataContainer<DataType>::const_iterator it = itBegin; it != itEnd; ++it)
    {
      if (restrictKeyRange && (it->mainKey() < inKeyRange.lower || it->mainKey() > inKeyRange.upper))
        continue;
      current = it->valueRange();
      if ((current.lower < range.lower || !haveLower) && current.lower < 0 && !qIsNaN(current.lower))
      {
        range.lower = current.lower;
        haveLower = true;
      }
      if ((current.upper > range.upper || !haveUpper) && current.upper < 0 && !qIsNaN(current.upper))
      {
        range.upper = current.upper;
        haveUpper = true;
      }
    }
  } else if (signDomain == QCP::sdPositive) // range may only be in the positive sign domain
  {
    for (QCPDataContainer<DataType>::const_iterator it = itBegin; it != itEnd; ++it)
    {
      if (restrictKeyRange && (it->mainKey() < inKeyRange.lower || it->mainKey() > inKeyRange.upper))
        continue;
      current = it->valueRange();
      if ((current.lower < range.lower || !haveLower) && current.lower > 0 && !qIsNaN(current.lower))
      {
        range.lower = current.lower;
        haveLower = true;
      }
      if ((current.upper > range.upper || !haveUpper) && current.upper > 0 && !qIsNaN(current.upper))
      {
        range.upper = current.upper;
        haveUpper = true;
      }
    }
  }
  
  foundRange = haveLower && haveUpper;
  return range;
}

/*!
  Makes sure \a begin and \a end mark a data range that is both within the bounds of this data
  container's data, as well as within the specified \a dataRange.

  This function doesn't require for \a dataRange to be within the bounds of this data container's
  valid range.
*/
template <class DataType>
void QCPDataContainer<DataType>::limitIteratorsToDataRange(const_iterator &begin, const_iterator &end, const QCPDataRange &dataRange) const
{
  QCPDataRange iteratorRange(begin-constBegin(), end-constBegin());
  iteratorRange = iteratorRange.bounded(dataRange.bounded(this->dataRange()));
  begin = constBegin()+iteratorRange.begin();
  end = constBegin()+iteratorRange.end();
}

/*! \internal
  
  Increases the preallocation pool to have a size of at least \a minimumPreallocSize. Depending on
  the preallocation history, the container will grow by more than requested, to speed up future
  consecutive size increases.
  
  if \a minimumPreallocSize is smaller than or equal to the current preallocation pool size, this
  method does nothing.
*/
template <class DataType>
void QCPDataContainer<DataType>::preallocateGrow(int minimumPreallocSize)
{
  if (minimumPreallocSize <= mPreallocSize)
    return;
  
  int newPreallocSize = minimumPreallocSize;
  newPreallocSize += (1u<<qBound(4, mPreallocIteration+4, 15)) - 12; // do 4 up to 32768-12 preallocation, doubling in each intermediate iteration
  ++mPreallocIteration;
  
  int sizeDifference = newPreallocSize-mPreallocSize;
  mData.resize(mData.size()+sizeDifference);
  std::copy_backward(mData.begin()+mPreallocSize, mData.end()-sizeDifference, mData.end());
  mPreallocSize = newPreallocSize;
}

/*! \internal
  
  This method decides, depending on the total allocation size and the size of the unused pre- and
  postallocation pools, whether it is sensible to reduce the pools in order to free up unused
  memory. It then possibly calls \ref squeeze to do the deallocation.
  
  If \ref setAutoSqueeze is enabled, this method is called automatically each time data points are
  removed from the container (e.g. \ref remove).
  
  \note when changing the decision parameters, care must be taken not to cause a back-and-forth
  between squeezing and reallocation due to the growth strategy of the internal QVector and \ref
  preallocateGrow. The hysteresis between allocation and deallocation should be made high enough
  (at the expense of possibly larger unused memory from time to time).
*/
template <class DataType>
void QCPDataContainer<DataType>::performAutoSqueeze()
{
  const int totalAlloc = mData.capacity();
  const int postAllocSize = totalAlloc-mData.size();
  const int usedSize = size();
  bool shrinkPostAllocation = false;
  bool shrinkPreAllocation = false;
  if (totalAlloc > 650000) // if allocation is larger, shrink earlier with respect to total used size
  {
    shrinkPostAllocation = postAllocSize > usedSize*1.5; // QVector grow strategy is 2^n for static data. Watch out not to oscillate!
    shrinkPreAllocation = mPreallocSize*10 > usedSize;
  } else if (totalAlloc > 1000) // below 10 MiB raw data be generous with preallocated memory, below 1k points don't even bother
  {
    shrinkPostAllocation = postAllocSize > usedSize*5;
    shrinkPreAllocation = mPreallocSize > usedSize*1.5; // preallocation can grow into postallocation, so can be smaller
  }
  
  if (shrinkPreAllocation || shrinkPostAllocation)
    squeeze(shrinkPreAllocation, shrinkPostAllocation);
}
/* end of 'src/datacontainer.cpp' */


/* end of 'src/datacontainer.h' */


/* including file 'src/plottable.h', size 8312                               */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPSelectionDecorator
{
  Q_GADGET
public:
  /**
   * @brief
   *
   */
  QCPSelectionDecorator();
  /**
   * @brief
   *
   */
  virtual ~QCPSelectionDecorator();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return QCPScatterStyle
   */
  QCPScatterStyle scatterStyle() const { return mScatterStyle; }
  /**
   * @brief
   *
   * @return QCPScatterStyle::ScatterProperties
   */
  QCPScatterStyle::ScatterProperties usedScatterProperties() const { return mUsedScatterProperties; }
  
  // setters:
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param scatterStyle
   * @param usedProperties
   */
  void setScatterStyle(const QCPScatterStyle &scatterStyle, QCPScatterStyle::ScatterProperties usedProperties=QCPScatterStyle::spPen);
  /**
   * @brief
   *
   * @param properties
   */
  void setUsedScatterProperties(const QCPScatterStyle::ScatterProperties &properties);
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  void applyPen(QCPPainter *painter) const;
  /**
   * @brief
   *
   * @param painter
   */
  void applyBrush(QCPPainter *painter) const;
  /**
   * @brief
   *
   * @param unselectedStyle
   * @return QCPScatterStyle
   */
  QCPScatterStyle getFinalScatterStyle(const QCPScatterStyle &unselectedStyle) const;
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param other
   */
  virtual void copyFrom(const QCPSelectionDecorator *other);
  /**
   * @brief
   *
   * @param painter
   * @param selection
   */
  virtual void drawDecoration(QCPPainter *painter, QCPDataSelection selection);
  
protected:
  // property members:
  QPen mPen; /**< TODO: describe */
  QBrush mBrush; /**< TODO: describe */
  QCPScatterStyle mScatterStyle; /**< TODO: describe */
  QCPScatterStyle::ScatterProperties mUsedScatterProperties; /**< TODO: describe */
  // non-property members:
  QCPAbstractPlottable *mPlottable; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param plottable
   * @return bool
   */
  virtual bool registerWithPlottable(QCPAbstractPlottable *plottable);
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPSelectionDecorator)
  friend class QCPAbstractPlottable;
};
Q_DECLARE_METATYPE(QCPSelectionDecorator*)


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPAbstractPlottable : public QCPLayerable
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QString name READ name WRITE setName)
  Q_PROPERTY(bool antialiasedFill READ antialiasedFill WRITE setAntialiasedFill)
  Q_PROPERTY(bool antialiasedScatters READ antialiasedScatters WRITE setAntialiasedScatters)
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QBrush brush READ brush WRITE setBrush)
  Q_PROPERTY(QCPAxis* keyAxis READ keyAxis WRITE setKeyAxis)
  Q_PROPERTY(QCPAxis* valueAxis READ valueAxis WRITE setValueAxis)
  Q_PROPERTY(QCP::SelectionType selectable READ selectable WRITE setSelectable NOTIFY selectableChanged)
  Q_PROPERTY(QCPDataSelection selection READ selection WRITE setSelection NOTIFY selectionChanged)
  Q_PROPERTY(QCPSelectionDecorator* selectionDecorator READ selectionDecorator WRITE setSelectionDecorator)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  QCPAbstractPlottable(QCPAxis *keyAxis, QCPAxis *valueAxis);
  /**
   * @brief
   *
   */
  virtual ~QCPAbstractPlottable();
  
  // getters:
  /**
   * @brief
   *
   * @return QString
   */
  QString name() const { return mName; }
  /**
   * @brief
   *
   * @return bool
   */
  bool antialiasedFill() const { return mAntialiasedFill; }
  /**
   * @brief
   *
   * @return bool
   */
  bool antialiasedScatters() const { return mAntialiasedScatters; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return QCPAxis
   */
  QCPAxis *keyAxis() const { return mKeyAxis.data(); }
  /**
   * @brief
   *
   * @return QCPAxis
   */
  QCPAxis *valueAxis() const { return mValueAxis.data(); }
  /**
   * @brief
   *
   * @return QCP::SelectionType
   */
  QCP::SelectionType selectable() const { return mSelectable; }
  /**
   * @brief
   *
   * @return bool
   */
  bool selected() const { return !mSelection.isEmpty(); }
  /**
   * @brief
   *
   * @return QCPDataSelection
   */
  QCPDataSelection selection() const { return mSelection; }
  /**
   * @brief
   *
   * @return QCPSelectionDecorator
   */
  QCPSelectionDecorator *selectionDecorator() const { return mSelectionDecorator; }
  
  // setters:
  /**
   * @brief
   *
   * @param name
   */
  void setName(const QString &name);
  /**
   * @brief
   *
   * @param enabled
   */
  void setAntialiasedFill(bool enabled);
  /**
   * @brief
   *
   * @param enabled
   */
  void setAntialiasedScatters(bool enabled);
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param axis
   */
  void setKeyAxis(QCPAxis *axis);
  /**
   * @brief
   *
   * @param axis
   */
  void setValueAxis(QCPAxis *axis);
  /**
   * @brief
   *
   * @param selectable
   */
  Q_SLOT void setSelectable(QCP::SelectionType selectable);
  /**
   * @brief
   *
   * @param selection
   */
  Q_SLOT void setSelection(QCPDataSelection selection);
  /**
   * @brief
   *
   * @param decorator
   */
  void setSelectionDecorator(QCPSelectionDecorator *decorator);

  // introduced virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const = 0;
  /**
   * @brief
   *
   * @return QCPPlottableInterface1D
   */
  virtual QCPPlottableInterface1D *interface1D() { return 0; }
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @return QCPRange
   */
  virtual QCPRange getKeyRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth) const = 0;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @param inKeyRange
   * @return QCPRange
   */
  virtual QCPRange getValueRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange()) const = 0;
  
  // non-property methods:
  /**
   * @brief
   *
   * @param key
   * @param value
   * @param x
   * @param y
   */
  void coordsToPixels(double key, double value, double &x, double &y) const;
  /**
   * @brief
   *
   * @param key
   * @param value
   * @return const QPointF
   */
  const QPointF coordsToPixels(double key, double value) const;
  /**
   * @brief
   *
   * @param x
   * @param y
   * @param key
   * @param value
   */
  void pixelsToCoords(double x, double y, double &key, double &value) const;
  /**
   * @brief
   *
   * @param pixelPos
   * @param key
   * @param value
   */
  void pixelsToCoords(const QPointF &pixelPos, double &key, double &value) const;
  /**
   * @brief
   *
   * @param onlyEnlarge
   */
  void rescaleAxes(bool onlyEnlarge=false) const;
  /**
   * @brief
   *
   * @param onlyEnlarge
   */
  void rescaleKeyAxis(bool onlyEnlarge=false) const;
  /**
   * @brief
   *
   * @param onlyEnlarge
   * @param inKeyRange
   */
  void rescaleValueAxis(bool onlyEnlarge=false, bool inKeyRange=false) const;
  /**
   * @brief
   *
   * @param legend
   * @return bool
   */
  bool addToLegend(QCPLegend *legend);
  /**
   * @brief
   *
   * @return bool
   */
  bool addToLegend();
  /**
   * @brief
   *
   * @param legend
   * @return bool
   */
  bool removeFromLegend(QCPLegend *legend) const;
  /**
   * @brief
   *
   * @return bool
   */
  bool removeFromLegend() const;
  
signals:
  /**
   * @brief
   *
   * @param selected
   */
  void selectionChanged(bool selected);
  /**
   * @brief
   *
   * @param selection
   */
  void selectionChanged(const QCPDataSelection &selection);
  /**
   * @brief
   *
   * @param selectable
   */
  void selectableChanged(QCP::SelectionType selectable);
  
protected:
  // property members:
  QString mName; /**< TODO: describe */
  bool mAntialiasedFill, mAntialiasedScatters; /**< TODO: describe */
  QPen mPen; /**< TODO: describe */
  QBrush mBrush; /**< TODO: describe */
  QPointer<QCPAxis> mKeyAxis, mValueAxis; /**< TODO: describe */
  QCP::SelectionType mSelectable; /**< TODO: describe */
  QCPDataSelection mSelection; /**< TODO: describe */
  QCPSelectionDecorator *mSelectionDecorator; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @return QRect
   */
  virtual QRect clipRect() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE = 0;
  /**
   * @brief
   *
   * @return QCP::Interaction
   */
  virtual QCP::Interaction selectionCategory() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  // events:
  /**
   * @brief
   *
   * @param event
   * @param additive
   * @param details
   * @param selectionStateChanged
   */
  virtual void selectEvent(QMouseEvent *event, bool additive, const QVariant &details, bool *selectionStateChanged) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param selectionStateChanged
   */
  virtual void deselectEvent(bool *selectionStateChanged) Q_DECL_OVERRIDE;
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param painter
   * @param rect
   */
  virtual void drawLegendIcon(QCPPainter *painter, const QRectF &rect) const = 0;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  void applyFillAntialiasingHint(QCPPainter *painter) const;
  /**
   * @brief
   *
   * @param painter
   */
  void applyScattersAntialiasingHint(QCPPainter *painter) const;

private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPAbstractPlottable)
  
  friend class QCustomPlot;
  friend class QCPAxis;
  friend class QCPPlottableLegendItem;
};


/* end of 'src/plottable.h' */


/* including file 'src/item.h', size 9368                                    */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemAnchor
{
  Q_GADGET
public:
  /**
   * @brief
   *
   * @param parentPlot
   * @param parentItem
   * @param name
   * @param anchorId
   */
  QCPItemAnchor(QCustomPlot *parentPlot, QCPAbstractItem *parentItem, const QString &name, int anchorId=-1);
  /**
   * @brief
   *
   */
  virtual ~QCPItemAnchor();
  
  // getters:
  /**
   * @brief
   *
   * @return QString
   */
  QString name() const { return mName; }
  /**
   * @brief
   *
   * @return QPointF
   */
  virtual QPointF pixelPosition() const;
  
protected:
  // property members:
  QString mName; /**< TODO: describe */
  
  // non-property members:
  QCustomPlot *mParentPlot; /**< TODO: describe */
  QCPAbstractItem *mParentItem; /**< TODO: describe */
  int mAnchorId; /**< TODO: describe */
  QSet<QCPItemPosition*> mChildrenX, mChildrenY; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @return QCPItemPosition
   */
  virtual QCPItemPosition *toQCPItemPosition() { return 0; }
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param pos
   */
  void addChildX(QCPItemPosition* pos); // called from pos when this anchor is set as parent
  /**
   * @brief
   *
   * @param pos
   */
  void removeChildX(QCPItemPosition *pos); // called from pos when its parent anchor is reset or pos deleted
  /**
   * @brief
   *
   * @param pos
   */
  void addChildY(QCPItemPosition* pos); // called from pos when this anchor is set as parent
  /**
   * @brief
   *
   * @param pos
   */
  void removeChildY(QCPItemPosition *pos); // called from pos when its parent anchor is reset or pos deleted
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPItemAnchor)
  
  friend class QCPItemPosition;
};



/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPItemPosition : public QCPItemAnchor
{
  Q_GADGET
public:
  /*!
    Defines the ways an item position can be specified. Thus it defines what the numbers passed to
    \ref setCoords actually mean.
    
    \see setType
  */
  enum PositionType { ptAbsolute        ///< Static positioning in pixels, starting from the top left corner of the viewport/widget.
                      ,ptViewportRatio  ///< Static positioning given by a fraction of the viewport size. For example, if you call setCoords(0, 0), the position will be at the top
                                        ///< left corner of the viewport/widget. setCoords(1, 1) will be at the bottom right corner, setCoords(0.5, 0) will be horizontally centered and
                                        ///< vertically at the top of the viewport/widget, etc.
                      ,ptAxisRectRatio  ///< Static positioning given by a fraction of the axis rect size (see \ref setAxisRect). For example, if you call setCoords(0, 0), the position will be at the top
                                        ///< left corner of the axis rect. setCoords(1, 1) will be at the bottom right corner, setCoords(0.5, 0) will be horizontally centered and
                                        ///< vertically at the top of the axis rect, etc. You can also go beyond the axis rect by providing negative coordinates or coordinates larger than 1.
                      ,ptPlotCoords     ///< Dynamic positioning at a plot coordinate defined by two axes (see \ref setAxes).
                    };
  Q_ENUMS(PositionType)
  
  /**
   * @brief
   *
   * @param parentPlot
   * @param parentItem
   * @param name
   */
  QCPItemPosition(QCustomPlot *parentPlot, QCPAbstractItem *parentItem, const QString &name);
  /**
   * @brief
   *
   */
  virtual ~QCPItemPosition();
  
  // getters:
  /**
   * @brief
   *
   * @return PositionType
   */
  PositionType type() const { return typeX(); }
  /**
   * @brief
   *
   * @return PositionType
   */
  PositionType typeX() const { return mPositionTypeX; }
  /**
   * @brief
   *
   * @return PositionType
   */
  PositionType typeY() const { return mPositionTypeY; }
  /**
   * @brief
   *
   * @return QCPItemAnchor
   */
  QCPItemAnchor *parentAnchor() const { return parentAnchorX(); }
  /**
   * @brief
   *
   * @return QCPItemAnchor
   */
  QCPItemAnchor *parentAnchorX() const { return mParentAnchorX; }
  /**
   * @brief
   *
   * @return QCPItemAnchor
   */
  QCPItemAnchor *parentAnchorY() const { return mParentAnchorY; }
  /**
   * @brief
   *
   * @return double
   */
  double key() const { return mKey; }
  /**
   * @brief
   *
   * @return double
   */
  double value() const { return mValue; }
  /**
   * @brief
   *
   * @return QPointF
   */
  QPointF coords() const { return QPointF(mKey, mValue); }
  /**
   * @brief
   *
   * @return QCPAxis
   */
  QCPAxis *keyAxis() const { return mKeyAxis.data(); }
  /**
   * @brief
   *
   * @return QCPAxis
   */
  QCPAxis *valueAxis() const { return mValueAxis.data(); }
  /**
   * @brief
   *
   * @return QCPAxisRect
   */
  QCPAxisRect *axisRect() const;
  /**
   * @brief
   *
   * @return QPointF
   */
  virtual QPointF pixelPosition() const;
  
  // setters:
  /**
   * @brief
   *
   * @param type
   */
  void setType(PositionType type);
  /**
   * @brief
   *
   * @param type
   */
  void setTypeX(PositionType type);
  /**
   * @brief
   *
   * @param type
   */
  void setTypeY(PositionType type);
  /**
   * @brief
   *
   * @param parentAnchor
   * @param keepPixelPosition
   * @return bool
   */
  bool setParentAnchor(QCPItemAnchor *parentAnchor, bool keepPixelPosition=false);
  /**
   * @brief
   *
   * @param parentAnchor
   * @param keepPixelPosition
   * @return bool
   */
  bool setParentAnchorX(QCPItemAnchor *parentAnchor, bool keepPixelPosition=false);
  /**
   * @brief
   *
   * @param parentAnchor
   * @param keepPixelPosition
   * @return bool
   */
  bool setParentAnchorY(QCPItemAnchor *parentAnchor, bool keepPixelPosition=false);
  /**
   * @brief
   *
   * @param key
   * @param value
   */
  void setCoords(double key, double value);
  /**
   * @brief
   *
   * @param coords
   */
  void setCoords(const QPointF &coords);
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  void setAxes(QCPAxis* keyAxis, QCPAxis* valueAxis);
  /**
   * @brief
   *
   * @param axisRect
   */
  void setAxisRect(QCPAxisRect *axisRect);
  /**
   * @brief
   *
   * @param pixelPosition
   */
  void setPixelPosition(const QPointF &pixelPosition);
  
protected:
  // property members:
  PositionType mPositionTypeX, mPositionTypeY; /**< TODO: describe */
  QPointer<QCPAxis> mKeyAxis, mValueAxis; /**< TODO: describe */
  QPointer<QCPAxisRect> mAxisRect; /**< TODO: describe */
  double mKey, mValue; /**< TODO: describe */
  QCPItemAnchor *mParentAnchorX, *mParentAnchorY; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @return QCPItemPosition
   */
  virtual QCPItemPosition *toQCPItemPosition() Q_DECL_OVERRIDE { return this; }
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPItemPosition)
  
};
Q_DECLARE_METATYPE(QCPItemPosition::PositionType)


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPAbstractItem : public QCPLayerable
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(bool clipToAxisRect READ clipToAxisRect WRITE setClipToAxisRect)
  Q_PROPERTY(QCPAxisRect* clipAxisRect READ clipAxisRect WRITE setClipAxisRect)
  Q_PROPERTY(bool selectable READ selectable WRITE setSelectable NOTIFY selectableChanged)
  Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectionChanged)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPAbstractItem(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPAbstractItem();
  
  // getters:
  /**
   * @brief
   *
   * @return bool
   */
  bool clipToAxisRect() const { return mClipToAxisRect; }
  /**
   * @brief
   *
   * @return QCPAxisRect
   */
  QCPAxisRect *clipAxisRect() const;
  /**
   * @brief
   *
   * @return bool
   */
  bool selectable() const { return mSelectable; }
  /**
   * @brief
   *
   * @return bool
   */
  bool selected() const { return mSelected; }
  
  // setters:
  /**
   * @brief
   *
   * @param clip
   */
  void setClipToAxisRect(bool clip);
  /**
   * @brief
   *
   * @param rect
   */
  void setClipAxisRect(QCPAxisRect *rect);
  /**
   * @brief
   *
   * @param selectable
   */
  Q_SLOT void setSelectable(bool selectable);
  /**
   * @brief
   *
   * @param selected
   */
  Q_SLOT void setSelected(bool selected);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE = 0;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QList<QCPItemPosition *>
   */
  QList<QCPItemPosition*> positions() const { return mPositions; }
  /**
   * @brief
   *
   * @return QList<QCPItemAnchor *>
   */
  QList<QCPItemAnchor*> anchors() const { return mAnchors; }
  /**
   * @brief
   *
   * @param name
   * @return QCPItemPosition
   */
  QCPItemPosition *position(const QString &name) const;
  /**
   * @brief
   *
   * @param name
   * @return QCPItemAnchor
   */
  QCPItemAnchor *anchor(const QString &name) const;
  /**
   * @brief
   *
   * @param name
   * @return bool
   */
  bool hasAnchor(const QString &name) const;
  
signals:
  /**
   * @brief
   *
   * @param selected
   */
  void selectionChanged(bool selected);
  /**
   * @brief
   *
   * @param selectable
   */
  void selectableChanged(bool selectable);
  
protected:
  // property members:
  bool mClipToAxisRect; /**< TODO: describe */
  QPointer<QCPAxisRect> mClipAxisRect; /**< TODO: describe */
  QList<QCPItemPosition*> mPositions; /**< TODO: describe */
  QList<QCPItemAnchor*> mAnchors; /**< TODO: describe */
  bool mSelectable, mSelected; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @return QCP::Interaction
   */
  virtual QCP::Interaction selectionCategory() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QRect
   */
  virtual QRect clipRect() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE = 0;
  // events:
  /**
   * @brief
   *
   * @param event
   * @param additive
   * @param details
   * @param selectionStateChanged
   */
  virtual void selectEvent(QMouseEvent *event, bool additive, const QVariant &details, bool *selectionStateChanged) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param selectionStateChanged
   */
  virtual void deselectEvent(bool *selectionStateChanged) Q_DECL_OVERRIDE;
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param anchorId
   * @return QPointF
   */
  virtual QPointF anchorPixelPosition(int anchorId) const;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param rect
   * @param pos
   * @param filledRect
   * @return double
   */
  double rectDistance(const QRectF &rect, const QPointF &pos, bool filledRect) const;
  /**
   * @brief
   *
   * @param name
   * @return QCPItemPosition
   */
  QCPItemPosition *createPosition(const QString &name);
  /**
   * @brief
   *
   * @param name
   * @param anchorId
   * @return QCPItemAnchor
   */
  QCPItemAnchor *createAnchor(const QString &name, int anchorId);
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPAbstractItem)
  
  friend class QCustomPlot;
  friend class QCPItemAnchor;
};

/* end of 'src/item.h' */


/* including file 'src/core.h', size 14797                                   */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCustomPlot : public QWidget
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QRect viewport READ viewport WRITE setViewport)
  Q_PROPERTY(QPixmap background READ background WRITE setBackground)
  Q_PROPERTY(bool backgroundScaled READ backgroundScaled WRITE setBackgroundScaled)
  Q_PROPERTY(Qt::AspectRatioMode backgroundScaledMode READ backgroundScaledMode WRITE setBackgroundScaledMode)
  Q_PROPERTY(QCPLayoutGrid* plotLayout READ plotLayout)
  Q_PROPERTY(bool autoAddPlottableToLegend READ autoAddPlottableToLegend WRITE setAutoAddPlottableToLegend)
  Q_PROPERTY(int selectionTolerance READ selectionTolerance WRITE setSelectionTolerance)
  Q_PROPERTY(bool noAntialiasingOnDrag READ noAntialiasingOnDrag WRITE setNoAntialiasingOnDrag)
  Q_PROPERTY(Qt::KeyboardModifier multiSelectModifier READ multiSelectModifier WRITE setMultiSelectModifier)
  Q_PROPERTY(bool openGl READ openGl WRITE setOpenGl)
  /// \endcond
public:
  /*!
    Defines how a layer should be inserted relative to an other layer.

    \see addLayer, moveLayer
  */
  enum LayerInsertMode { limBelow  ///< Layer is inserted below other layer
                         ,limAbove ///< Layer is inserted above other layer
                       };
  Q_ENUMS(LayerInsertMode)
  
  /*!
    Defines with what timing the QCustomPlot surface is refreshed after a replot.

    \see replot
  */
  enum RefreshPriority { rpImmediateRefresh ///< Replots immediately and repaints the widget immediately by calling QWidget::repaint() after the replot
                         ,rpQueuedRefresh   ///< Replots immediately, but queues the widget repaint, by calling QWidget::update() after the replot. This way multiple redundant widget repaints can be avoided.
                         ,rpRefreshHint     ///< Whether to use immediate or queued refresh depends on whether the plotting hint \ref QCP::phImmediateRefresh is set, see \ref setPlottingHints.
                         ,rpQueuedReplot    ///< Queues the entire replot for the next event loop iteration. This way multiple redundant replots can be avoided. The actual replot is then done with \ref rpRefreshHint priority.
                       };
  Q_ENUMS(RefreshPriority)
  
  /**
   * @brief
   *
   * @param parent
   */
  explicit QCustomPlot(QWidget *parent = 0);
  /**
   * @brief
   *
   */
  virtual ~QCustomPlot();
  
  // getters:
  /**
   * @brief
   *
   * @return QRect
   */
  QRect viewport() const { return mViewport; }
  /**
   * @brief
   *
   * @return double
   */
  double bufferDevicePixelRatio() const { return mBufferDevicePixelRatio; }
  /**
   * @brief
   *
   * @return QPixmap
   */
  QPixmap background() const { return mBackgroundPixmap; }
  /**
   * @brief
   *
   * @return bool
   */
  bool backgroundScaled() const { return mBackgroundScaled; }
  /**
   * @brief
   *
   * @return Qt::AspectRatioMode
   */
  Qt::AspectRatioMode backgroundScaledMode() const { return mBackgroundScaledMode; }
  /**
   * @brief
   *
   * @return QCPLayoutGrid
   */
  QCPLayoutGrid *plotLayout() const { return mPlotLayout; }
  /**
   * @brief
   *
   * @return QCP::AntialiasedElements
   */
  QCP::AntialiasedElements antialiasedElements() const { return mAntialiasedElements; }
  /**
   * @brief
   *
   * @return QCP::AntialiasedElements
   */
  QCP::AntialiasedElements notAntialiasedElements() const { return mNotAntialiasedElements; }
  /**
   * @brief
   *
   * @return bool
   */
  bool autoAddPlottableToLegend() const { return mAutoAddPlottableToLegend; }
  /**
   * @brief
   *
   * @return const QCP::Interactions
   */
  const QCP::Interactions interactions() const { return mInteractions; }
  /**
   * @brief
   *
   * @return int
   */
  int selectionTolerance() const { return mSelectionTolerance; }
  /**
   * @brief
   *
   * @return bool
   */
  bool noAntialiasingOnDrag() const { return mNoAntialiasingOnDrag; }
  /**
   * @brief
   *
   * @return QCP::PlottingHints
   */
  QCP::PlottingHints plottingHints() const { return mPlottingHints; }
  /**
   * @brief
   *
   * @return Qt::KeyboardModifier
   */
  Qt::KeyboardModifier multiSelectModifier() const { return mMultiSelectModifier; }
  /**
   * @brief
   *
   * @return QCP::SelectionRectMode
   */
  QCP::SelectionRectMode selectionRectMode() const { return mSelectionRectMode; }
  /**
   * @brief
   *
   * @return QCPSelectionRect
   */
  QCPSelectionRect *selectionRect() const { return mSelectionRect; }
  /**
   * @brief
   *
   * @return bool
   */
  bool openGl() const { return mOpenGl; }
  
  // setters:
  /**
   * @brief
   *
   * @param rect
   */
  void setViewport(const QRect &rect);
  /**
   * @brief
   *
   * @param ratio
   */
  void setBufferDevicePixelRatio(double ratio);
  /**
   * @brief
   *
   * @param pm
   */
  void setBackground(const QPixmap &pm);
  /**
   * @brief
   *
   * @param pm
   * @param scaled
   * @param mode
   */
  void setBackground(const QPixmap &pm, bool scaled, Qt::AspectRatioMode mode=Qt::KeepAspectRatioByExpanding);
  /**
   * @brief
   *
   * @param brush
   */
  void setBackground(const QBrush &brush);
  /**
   * @brief
   *
   * @param scaled
   */
  void setBackgroundScaled(bool scaled);
  /**
   * @brief
   *
   * @param mode
   */
  void setBackgroundScaledMode(Qt::AspectRatioMode mode);
  /**
   * @brief
   *
   * @param antialiasedElements
   */
  void setAntialiasedElements(const QCP::AntialiasedElements &antialiasedElements);
  /**
   * @brief
   *
   * @param antialiasedElement
   * @param enabled
   */
  void setAntialiasedElement(QCP::AntialiasedElement antialiasedElement, bool enabled=true);
  /**
   * @brief
   *
   * @param notAntialiasedElements
   */
  void setNotAntialiasedElements(const QCP::AntialiasedElements &notAntialiasedElements);
  /**
   * @brief
   *
   * @param notAntialiasedElement
   * @param enabled
   */
  void setNotAntialiasedElement(QCP::AntialiasedElement notAntialiasedElement, bool enabled=true);
  /**
   * @brief
   *
   * @param on
   */
  void setAutoAddPlottableToLegend(bool on);
  /**
   * @brief
   *
   * @param interactions
   */
  void setInteractions(const QCP::Interactions &interactions);
  /**
   * @brief
   *
   * @param interaction
   * @param enabled
   */
  void setInteraction(const QCP::Interaction &interaction, bool enabled=true);
  /**
   * @brief
   *
   * @param pixels
   */
  void setSelectionTolerance(int pixels);
  /**
   * @brief
   *
   * @param enabled
   */
  void setNoAntialiasingOnDrag(bool enabled);
  /**
   * @brief
   *
   * @param hints
   */
  void setPlottingHints(const QCP::PlottingHints &hints);
  /**
   * @brief
   *
   * @param hint
   * @param enabled
   */
  void setPlottingHint(QCP::PlottingHint hint, bool enabled=true);
  /**
   * @brief
   *
   * @param modifier
   */
  void setMultiSelectModifier(Qt::KeyboardModifier modifier);
  /**
   * @brief
   *
   * @param mode
   */
  void setSelectionRectMode(QCP::SelectionRectMode mode);
  /**
   * @brief
   *
   * @param selectionRect
   */
  void setSelectionRect(QCPSelectionRect *selectionRect);
  /**
   * @brief
   *
   * @param enabled
   * @param multisampling
   */
  void setOpenGl(bool enabled, int multisampling=16);
  
  // non-property methods:
  // plottable interface:
  /**
   * @brief
   *
   * @param index
   * @return QCPAbstractPlottable
   */
  QCPAbstractPlottable *plottable(int index);
  /**
   * @brief
   *
   * @return QCPAbstractPlottable
   */
  QCPAbstractPlottable *plottable();
  /**
   * @brief
   *
   * @param plottable
   * @return bool
   */
  bool removePlottable(QCPAbstractPlottable *plottable);
  /**
   * @brief
   *
   * @param index
   * @return bool
   */
  bool removePlottable(int index);
  /**
   * @brief
   *
   * @return int
   */
  int clearPlottables();
  /**
   * @brief
   *
   * @return int
   */
  int plottableCount() const;
  /**
   * @brief
   *
   * @return QList<QCPAbstractPlottable *>
   */
  QList<QCPAbstractPlottable*> selectedPlottables() const;
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @return QCPAbstractPlottable
   */
  QCPAbstractPlottable *plottableAt(const QPointF &pos, bool onlySelectable=false) const;
  /**
   * @brief
   *
   * @param plottable
   * @return bool
   */
  bool hasPlottable(QCPAbstractPlottable *plottable) const;
 
  // specialized interface for QCPGraph:
  /**
   * @brief
   *
   * @param index
   * @return QCPGraph
   */
  QCPGraph *graph(int index) const;
  /**
   * @brief
   *
   * @return QCPGraph
   */
  QCPGraph *graph() const;
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   * @return QCPGraph
   */
  QCPGraph *addGraph(QCPAxis *keyAxis=0, QCPAxis *valueAxis=0);
  /**
   * @brief
   *
   * @param graph
   * @return bool
   */
  bool removeGraph(QCPGraph *graph);
  /**
   * @brief
   *
   * @param index
   * @return bool
   */
  bool removeGraph(int index);
  /**
   * @brief
   *
   * @return int
   */
  int clearGraphs();
  /**
   * @brief
   *
   * @return int
   */
  int graphCount() const;
  /**
   * @brief
   *
   * @return QList<QCPGraph *>
   */
  QList<QCPGraph*> selectedGraphs() const;

  // item interface:
  /**
   * @brief
   *
   * @param index
   * @return QCPAbstractItem
   */
  QCPAbstractItem *item(int index) const;
  /**
   * @brief
   *
   * @return QCPAbstractItem
   */
  QCPAbstractItem *item() const;
  /**
   * @brief
   *
   * @param item
   * @return bool
   */
  bool removeItem(QCPAbstractItem *item);
  /**
   * @brief
   *
   * @param index
   * @return bool
   */
  bool removeItem(int index);
  /**
   * @brief
   *
   * @return int
   */
  int clearItems();
  /**
   * @brief
   *
   * @return int
   */
  int itemCount() const;
  /**
   * @brief
   *
   * @return QList<QCPAbstractItem *>
   */
  QList<QCPAbstractItem*> selectedItems() const;
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @return QCPAbstractItem
   */
  QCPAbstractItem *itemAt(const QPointF &pos, bool onlySelectable=false) const;
  /**
   * @brief
   *
   * @param item
   * @return bool
   */
  bool hasItem(QCPAbstractItem *item) const;
  
  // layer interface:
  /**
   * @brief
   *
   * @param name
   * @return QCPLayer
   */
  QCPLayer *layer(const QString &name) const;
  /**
   * @brief
   *
   * @param index
   * @return QCPLayer
   */
  QCPLayer *layer(int index) const;
  /**
   * @brief
   *
   * @return QCPLayer
   */
  QCPLayer *currentLayer() const;
  /**
   * @brief
   *
   * @param name
   * @return bool
   */
  bool setCurrentLayer(const QString &name);
  /**
   * @brief
   *
   * @param layer
   * @return bool
   */
  bool setCurrentLayer(QCPLayer *layer);
  /**
   * @brief
   *
   * @return int
   */
  int layerCount() const;
  /**
   * @brief
   *
   * @param name
   * @param otherLayer
   * @param insertMode
   * @return bool
   */
  bool addLayer(const QString &name, QCPLayer *otherLayer=0, LayerInsertMode insertMode=limAbove);
  /**
   * @brief
   *
   * @param layer
   * @return bool
   */
  bool removeLayer(QCPLayer *layer);
  /**
   * @brief
   *
   * @param layer
   * @param otherLayer
   * @param insertMode
   * @return bool
   */
  bool moveLayer(QCPLayer *layer, QCPLayer *otherLayer, LayerInsertMode insertMode=limAbove);
  
  // axis rect/layout interface:
  /**
   * @brief
   *
   * @return int
   */
  int axisRectCount() const;
  /**
   * @brief
   *
   * @param index
   * @return QCPAxisRect
   */
  QCPAxisRect* axisRect(int index=0) const;
  /**
   * @brief
   *
   * @return QList<QCPAxisRect *>
   */
  QList<QCPAxisRect*> axisRects() const;
  /**
   * @brief
   *
   * @param pos
   * @return QCPLayoutElement
   */
  QCPLayoutElement* layoutElementAt(const QPointF &pos) const;
  /**
   * @brief
   *
   * @param pos
   * @return QCPAxisRect
   */
  QCPAxisRect* axisRectAt(const QPointF &pos) const;
  /**
   * @brief
   *
   * @param onlyVisiblePlottables
   */
  Q_SLOT void rescaleAxes(bool onlyVisiblePlottables=false);
  
  /**
   * @brief
   *
   * @return QList<QCPAxis *>
   */
  QList<QCPAxis*> selectedAxes() const;
  /**
   * @brief
   *
   * @return QList<QCPLegend *>
   */
  QList<QCPLegend*> selectedLegends() const;
  /**
   * @brief
   *
   */
  Q_SLOT void deselectAll();
  
  /**
   * @brief
   *
   * @param fileName
   * @param width
   * @param height
   * @param exportPen
   * @param pdfCreator
   * @param pdfTitle
   * @return bool
   */
  bool savePdf(const QString &fileName, int width=0, int height=0, QCP::ExportPen exportPen=QCP::epAllowCosmetic, const QString &pdfCreator=QString(), const QString &pdfTitle=QString());
  /**
   * @brief
   *
   * @param fileName
   * @param width
   * @param height
   * @param scale
   * @param quality
   * @param resolution
   * @param resolutionUnit
   * @return bool
   */
  bool savePng(const QString &fileName, int width=0, int height=0, double scale=1.0, int quality=-1, int resolution=96, QCP::ResolutionUnit resolutionUnit=QCP::ruDotsPerInch);
  /**
   * @brief
   *
   * @param fileName
   * @param width
   * @param height
   * @param scale
   * @param quality
   * @param resolution
   * @param resolutionUnit
   * @return bool
   */
  bool saveJpg(const QString &fileName, int width=0, int height=0, double scale=1.0, int quality=-1, int resolution=96, QCP::ResolutionUnit resolutionUnit=QCP::ruDotsPerInch);
  /**
   * @brief
   *
   * @param fileName
   * @param width
   * @param height
   * @param scale
   * @param resolution
   * @param resolutionUnit
   * @return bool
   */
  bool saveBmp(const QString &fileName, int width=0, int height=0, double scale=1.0, int resolution=96, QCP::ResolutionUnit resolutionUnit=QCP::ruDotsPerInch);
  /**
   * @brief
   *
   * @param fileName
   * @param width
   * @param height
   * @param scale
   * @param format
   * @param quality
   * @param resolution
   * @param resolutionUnit
   * @return bool
   */
  bool saveRastered(const QString &fileName, int width, int height, double scale, const char *format, int quality=-1, int resolution=96, QCP::ResolutionUnit resolutionUnit=QCP::ruDotsPerInch);
  /**
   * @brief
   *
   * @param width
   * @param height
   * @param scale
   * @return QPixmap
   */
  QPixmap toPixmap(int width=0, int height=0, double scale=1.0);
  /**
   * @brief
   *
   * @param painter
   * @param width
   * @param height
   */
  void toPainter(QCPPainter *painter, int width=0, int height=0);
  /**
   * @brief
   *
   * @param refreshPriority
   */
  Q_SLOT void replot(QCustomPlot::RefreshPriority refreshPriority=QCustomPlot::rpRefreshHint);
  
  QCPAxis *xAxis, *yAxis, *xAxis2, *yAxis2; /**< TODO: describe */
  QCPLegend *legend; /**< TODO: describe */
  
signals:
  /**
   * @brief
   *
   * @param event
   */
  void mouseDoubleClick(QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  void mousePress(QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  void mouseMove(QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  void mouseRelease(QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  void mouseWheel(QWheelEvent *event);
  
  /**
   * @brief
   *
   * @param plottable
   * @param dataIndex
   * @param event
   */
  void plottableClick(QCPAbstractPlottable *plottable, int dataIndex, QMouseEvent *event);
  /**
   * @brief
   *
   * @param plottable
   * @param dataIndex
   * @param event
   */
  void plottableDoubleClick(QCPAbstractPlottable *plottable, int dataIndex, QMouseEvent *event);
  /**
   * @brief
   *
   * @param item
   * @param event
   */
  void itemClick(QCPAbstractItem *item, QMouseEvent *event);
  /**
   * @brief
   *
   * @param item
   * @param event
   */
  void itemDoubleClick(QCPAbstractItem *item, QMouseEvent *event);
  /**
   * @brief
   *
   * @param axis
   * @param part
   * @param event
   */
  void axisClick(QCPAxis *axis, QCPAxis::SelectablePart part, QMouseEvent *event);
  /**
   * @brief
   *
   * @param axis
   * @param part
   * @param event
   */
  void axisDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part, QMouseEvent *event);
  /**
   * @brief
   *
   * @param legend
   * @param item
   * @param event
   */
  void legendClick(QCPLegend *legend, QCPAbstractLegendItem *item, QMouseEvent *event);
  /**
   * @brief
   *
   * @param legend
   * @param item
   * @param event
   */
  void legendDoubleClick(QCPLegend *legend,  QCPAbstractLegendItem *item, QMouseEvent *event);
  
  /**
   * @brief
   *
   */
  void selectionChangedByUser();
  /**
   * @brief
   *
   */
  void beforeReplot();
  /**
   * @brief
   *
   */
  void afterReplot();
  
protected:
  // property members:
  QRect mViewport; /**< TODO: describe */
  double mBufferDevicePixelRatio; /**< TODO: describe */
  QCPLayoutGrid *mPlotLayout; /**< TODO: describe */
  bool mAutoAddPlottableToLegend; /**< TODO: describe */
  QList<QCPAbstractPlottable*> mPlottables; /**< TODO: describe */
  QList<QCPGraph*> mGraphs; // extra list of plottables also in mPlottables that are of type QCPGraph /**< TODO: describe */
  QList<QCPAbstractItem*> mItems; /**< TODO: describe */
  QList<QCPLayer*> mLayers; /**< TODO: describe */
  QCP::AntialiasedElements mAntialiasedElements, mNotAntialiasedElements; /**< TODO: describe */
  QCP::Interactions mInteractions; /**< TODO: describe */
  int mSelectionTolerance; /**< TODO: describe */
  bool mNoAntialiasingOnDrag; /**< TODO: describe */
  QBrush mBackgroundBrush; /**< TODO: describe */
  QPixmap mBackgroundPixmap; /**< TODO: describe */
  QPixmap mScaledBackgroundPixmap; /**< TODO: describe */
  bool mBackgroundScaled; /**< TODO: describe */
  Qt::AspectRatioMode mBackgroundScaledMode; /**< TODO: describe */
  QCPLayer *mCurrentLayer; /**< TODO: describe */
  QCP::PlottingHints mPlottingHints; /**< TODO: describe */
  Qt::KeyboardModifier mMultiSelectModifier; /**< TODO: describe */
  QCP::SelectionRectMode mSelectionRectMode; /**< TODO: describe */
  QCPSelectionRect *mSelectionRect; /**< TODO: describe */
  bool mOpenGl; /**< TODO: describe */
  
  // non-property members:
  QList<QSharedPointer<QCPAbstractPaintBuffer> > mPaintBuffers; /**< TODO: describe */
  QPoint mMousePressPos; /**< TODO: describe */
  bool mMouseHasMoved; /**< TODO: describe */
  QPointer<QCPLayerable> mMouseEventLayerable; /**< TODO: describe */
  QVariant mMouseEventLayerableDetails; /**< TODO: describe */
  bool mReplotting; /**< TODO: describe */
  bool mReplotQueued; /**< TODO: describe */
  int mOpenGlMultisamples; /**< TODO: describe */
  QCP::AntialiasedElements mOpenGlAntialiasedElementsBackup; /**< TODO: describe */
  bool mOpenGlCacheLabelsBackup; /**< TODO: describe */
#ifdef QCP_OPENGL_FBO
  QSharedPointer<QOpenGLContext> mGlContext;
  QSharedPointer<QSurface> mGlSurface;
  QSharedPointer<QOpenGLPaintDevice> mGlPaintDevice;
#endif
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize minimumSizeHint() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize sizeHint() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter);
  /**
   * @brief
   *
   */
  virtual void updateLayout();
  /**
   * @brief
   *
   * @param axis
   */
  virtual void axisRemoved(QCPAxis *axis);
  /**
   * @brief
   *
   * @param legend
   */
  virtual void legendRemoved(QCPLegend *legend);
  /**
   * @brief
   *
   * @param rect
   * @param event
   */
  Q_SLOT virtual void processRectSelection(QRect rect, QMouseEvent *event);
  /**
   * @brief
   *
   * @param rect
   * @param event
   */
  Q_SLOT virtual void processRectZoom(QRect rect, QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  Q_SLOT virtual void processPointSelection(QMouseEvent *event);
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param plottable
   * @return bool
   */
  bool registerPlottable(QCPAbstractPlottable *plottable);
  /**
   * @brief
   *
   * @param graph
   * @return bool
   */
  bool registerGraph(QCPGraph *graph);
  /**
   * @brief
   *
   * @param item
   * @return bool
   */
  bool registerItem(QCPAbstractItem* item);
  /**
   * @brief
   *
   */
  void updateLayerIndices() const;
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param selectionDetails
   * @return QCPLayerable
   */
  QCPLayerable *layerableAt(const QPointF &pos, bool onlySelectable, QVariant *selectionDetails=0) const;
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param selectionDetails
   * @return QList<QCPLayerable *>
   */
  QList<QCPLayerable*> layerableListAt(const QPointF &pos, bool onlySelectable, QList<QVariant> *selectionDetails=0) const;
  /**
   * @brief
   *
   * @param painter
   */
  void drawBackground(QCPPainter *painter);
  /**
   * @brief
   *
   */
  void setupPaintBuffers();
  /**
   * @brief
   *
   * @return QCPAbstractPaintBuffer
   */
  QCPAbstractPaintBuffer *createPaintBuffer();
  /**
   * @brief
   *
   * @return bool
   */
  bool hasInvalidatedPaintBuffers();
  /**
   * @brief
   *
   * @return bool
   */
  bool setupOpenGl();
  /**
   * @brief
   *
   */
  void freeOpenGl();
  
  friend class QCPLegend;
  friend class QCPAxis;
  friend class QCPLayer;
  friend class QCPAxisRect;
  friend class QCPAbstractPlottable;
  friend class QCPGraph;
  friend class QCPAbstractItem;
};
Q_DECLARE_METATYPE(QCustomPlot::LayerInsertMode)
Q_DECLARE_METATYPE(QCustomPlot::RefreshPriority)

/* end of 'src/core.h' */


/* including file 'src/plottable1d.h', size 4250                             */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPPlottableInterface1D
{
public:
  // introduced pure virtual methods:
  /**
   * @brief
   *
   * @return int
   */
  virtual int dataCount() const = 0;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataMainKey(int index) const = 0;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataSortKey(int index) const = 0;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataMainValue(int index) const = 0;
  /**
   * @brief
   *
   * @param index
   * @return QCPRange
   */
  virtual QCPRange dataValueRange(int index) const = 0;
  /**
   * @brief
   *
   * @param index
   * @return QPointF
   */
  virtual QPointF dataPixelPosition(int index) const = 0;
  /**
   * @brief
   *
   * @return bool
   */
  virtual bool sortKeyIsMainKey() const = 0;
  /**
   * @brief
   *
   * @param rect
   * @param onlySelectable
   * @return QCPDataSelection
   */
  virtual QCPDataSelection selectTestRect(const QRectF &rect, bool onlySelectable) const = 0;
  /**
   * @brief
   *
   * @param sortKey
   * @param expandedRange
   * @return int
   */
  virtual int findBegin(double sortKey, bool expandedRange=true) const = 0;
  /**
   * @brief
   *
   * @param sortKey
   * @param expandedRange
   * @return int
   */
  virtual int findEnd(double sortKey, bool expandedRange=true) const = 0;
};

template <class DataType>
/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPAbstractPlottable1D : public QCPAbstractPlottable, public QCPPlottableInterface1D
{
  // No Q_OBJECT macro due to template class
  
public:
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  QCPAbstractPlottable1D(QCPAxis *keyAxis, QCPAxis *valueAxis);
  /**
   * @brief
   *
   */
  virtual ~QCPAbstractPlottable1D();
  
  // virtual methods of 1d plottable interface:
  /**
   * @brief
   *
   * @return int
   */
  virtual int dataCount() const;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataMainKey(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataSortKey(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataMainValue(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return QCPRange
   */
  virtual QCPRange dataValueRange(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return QPointF
   */
  virtual QPointF dataPixelPosition(int index) const;
  /**
   * @brief
   *
   * @return bool
   */
  virtual bool sortKeyIsMainKey() const;
  /**
   * @brief
   *
   * @param rect
   * @param onlySelectable
   * @return QCPDataSelection
   */
  virtual QCPDataSelection selectTestRect(const QRectF &rect, bool onlySelectable) const;
  /**
   * @brief
   *
   * @param sortKey
   * @param expandedRange
   * @return int
   */
  virtual int findBegin(double sortKey, bool expandedRange=true) const;
  /**
   * @brief
   *
   * @param sortKey
   * @param expandedRange
   * @return int
   */
  virtual int findEnd(double sortKey, bool expandedRange=true) const;
  
  // virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const;
  /**
   * @brief
   *
   * @return QCPPlottableInterface1D
   */
  virtual QCPPlottableInterface1D *interface1D() { return this; }
  
protected:
  // property members:
  QSharedPointer<QCPDataContainer<DataType> > mDataContainer; /**< TODO: describe */
  
  // helpers for subclasses:
  /**
   * @brief
   *
   * @param selectedSegments
   * @param unselectedSegments
   */
  void getDataSegments(QList<QCPDataRange> &selectedSegments, QList<QCPDataRange> &unselectedSegments) const;
  /**
   * @brief
   *
   * @param painter
   * @param lineData
   */
  void drawPolyline(QCPPainter *painter, const QVector<QPointF> &lineData) const;

private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPAbstractPlottable1D)
  
};

// include implementation in header since it is a class template:

/* including file 'src/plottable1d.cpp', size 22240                          */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPPlottableInterface1D
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPPlottableInterface1D
  \brief Defines an abstract interface for one-dimensional plottables

  This class contains only pure virtual methods which define a common interface to the data
  of one-dimensional plottables.

  For example, it is implemented by the template class \ref QCPAbstractPlottable1D (the preferred
  base class for one-dimensional plottables). So if you use that template class as base class of
  your one-dimensional plottable, you won't have to care about implementing the 1d interface
  yourself.

  If your plottable doesn't derive from \ref QCPAbstractPlottable1D but still wants to provide a 1d
  interface (e.g. like \ref QCPErrorBars does), you should inherit from both \ref
  QCPAbstractPlottable and \ref QCPPlottableInterface1D and accordingly reimplement the pure
  virtual methods of the 1d interface, matching your data container. Also, reimplement \ref
  QCPAbstractPlottable::interface1D to return the \c this pointer.

  If you have a \ref QCPAbstractPlottable pointer, you can check whether it implements this
  interface by calling \ref QCPAbstractPlottable::interface1D and testing it for a non-zero return
  value. If it indeed implements this interface, you may use it to access the plottable's data
  without needing to know the exact type of the plottable or its data point type.
*/

/* start documentation of pure virtual functions */

/*! \fn virtual int QCPPlottableInterface1D::dataCount() const = 0;
  
  Returns the number of data points of the plottable.
*/

/*! \fn virtual QCPDataSelection QCPPlottableInterface1D::selectTestRect(const QRectF &rect, bool onlySelectable) const = 0;
  
  Returns a data selection containing all the data points of this plottable which are contained (or
  hit by) \a rect. This is used mainly in the selection rect interaction for data selection (\ref
  dataselection "data selection mechanism").
  
  If \a onlySelectable is true, an empty QCPDataSelection is returned if this plottable is not
  selectable (i.e. if \ref QCPAbstractPlottable::setSelectable is \ref QCP::stNone).
  
  \note \a rect must be a normalized rect (positive or zero width and height). This is especially
  important when using the rect of \ref QCPSelectionRect::accepted, which is not necessarily
  normalized. Use <tt>QRect::normalized()</tt> when passing a rect which might not be normalized.
*/

/*! \fn virtual double QCPPlottableInterface1D::dataMainKey(int index) const = 0
  
  Returns the main key of the data point at the given \a index.
  
  What the main key is, is defined by the plottable's data type. See the \ref
  qcpdatacontainer-datatype "QCPDataContainer DataType" documentation for details about this naming
  convention.
*/

/*! \fn virtual double QCPPlottableInterface1D::dataSortKey(int index) const = 0
  
  Returns the sort key of the data point at the given \a index.
  
  What the sort key is, is defined by the plottable's data type. See the \ref
  qcpdatacontainer-datatype "QCPDataContainer DataType" documentation for details about this naming
  convention.
*/

/*! \fn virtual double QCPPlottableInterface1D::dataMainValue(int index) const = 0
  
  Returns the main value of the data point at the given \a index.
  
  What the main value is, is defined by the plottable's data type. See the \ref
  qcpdatacontainer-datatype "QCPDataContainer DataType" documentation for details about this naming
  convention.
*/

/*! \fn virtual QCPRange QCPPlottableInterface1D::dataValueRange(int index) const = 0
  
  Returns the value range of the data point at the given \a index.
  
  What the value range is, is defined by the plottable's data type. See the \ref
  qcpdatacontainer-datatype "QCPDataContainer DataType" documentation for details about this naming
  convention.
*/

/*! \fn virtual QPointF QCPPlottableInterface1D::dataPixelPosition(int index) const = 0

  Returns the pixel position on the widget surface at which the data point at the given \a index
  appears.

  Usually this corresponds to the point of \ref dataMainKey/\ref dataMainValue, in pixel
  coordinates. However, depending on the plottable, this might be a different apparent position
  than just a coord-to-pixel transform of those values. For example, \ref QCPBars apparent data
  values can be shifted depending on their stacking, bar grouping or configured base value.
*/

/*! \fn virtual bool QCPPlottableInterface1D::sortKeyIsMainKey() const = 0

  Returns whether the sort key (\ref dataSortKey) is identical to the main key (\ref dataMainKey).

  What the sort and main keys are, is defined by the plottable's data type. See the \ref
  qcpdatacontainer-datatype "QCPDataContainer DataType" documentation for details about this naming
  convention.
*/

/*! \fn virtual int QCPPlottableInterface1D::findBegin(double sortKey, bool expandedRange) const = 0

  Returns the index of the data point with a (sort-)key that is equal to, just below, or just above
  \a sortKey. If \a expandedRange is true, the data point just below \a sortKey will be considered,
  otherwise the one just above.

  This can be used in conjunction with \ref findEnd to iterate over data points within a given key
  range, including or excluding the bounding data points that are just beyond the specified range.

  If \a expandedRange is true but there are no data points below \a sortKey, 0 is returned.

  If the container is empty, returns 0 (in that case, \ref findEnd will also return 0, so a loop
  using these methods will not iterate over the index 0).

  \see findEnd, QCPDataContainer::findBegin
*/

/*! \fn virtual int QCPPlottableInterface1D::findEnd(double sortKey, bool expandedRange) const = 0

  Returns the index one after the data point with a (sort-)key that is equal to, just above, or
  just below \a sortKey. If \a expandedRange is true, the data point just above \a sortKey will be
  considered, otherwise the one just below.

  This can be used in conjunction with \ref findBegin to iterate over data points within a given
  key range, including the bounding data points that are just below and above the specified range.

  If \a expandedRange is true but there are no data points above \a sortKey, the index just above the
  highest data point is returned.

  If the container is empty, returns 0.

  \see findBegin, QCPDataContainer::findEnd
*/

/* end documentation of pure virtual functions */


////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPAbstractPlottable1D
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPAbstractPlottable1D
  \brief A template base class for plottables with one-dimensional data

  This template class derives from \ref QCPAbstractPlottable and from the abstract interface \ref
  QCPPlottableInterface1D. It serves as a base class for all one-dimensional data (i.e. data with
  one key dimension), such as \ref QCPGraph and QCPCurve.

  The template parameter \a DataType is the type of the data points of this plottable (e.g. \ref
  QCPGraphData or \ref QCPCurveData). The main purpose of this base class is to provide the member
  \a mDataContainer (a shared pointer to a \ref QCPDataContainer "QCPDataContainer<DataType>") and
  implement the according virtual methods of the \ref QCPPlottableInterface1D, such that most
  subclassed plottables don't need to worry about this anymore.

  Further, it provides a convenience method for retrieving selected/unselected data segments via
  \ref getDataSegments. This is useful when subclasses implement their \ref draw method and need to
  draw selected segments with a different pen/brush than unselected segments (also see \ref
  QCPSelectionDecorator).

  This class implements basic functionality of \ref QCPAbstractPlottable::selectTest and \ref
  QCPPlottableInterface1D::selectTestRect, assuming point-like data points, based on the 1D data
  interface. In spite of that, most plottable subclasses will want to reimplement those methods
  again, to provide a more accurate hit test based on their specific data visualization geometry.
*/

/* start documentation of inline functions */

/*! \fn QCPPlottableInterface1D *QCPAbstractPlottable1D::interface1D()
  
  Returns a \ref QCPPlottableInterface1D pointer to this plottable, providing access to its 1D
  interface.
  
  \seebaseclassmethod
*/

/* end documentation of inline functions */

/*!
  Forwards \a keyAxis and \a valueAxis to the \ref QCPAbstractPlottable::QCPAbstractPlottable
  "QCPAbstractPlottable" constructor and allocates the \a mDataContainer.
*/
template <class DataType>
QCPAbstractPlottable1D<DataType>::QCPAbstractPlottable1D(QCPAxis *keyAxis, QCPAxis *valueAxis) :
  QCPAbstractPlottable(keyAxis, valueAxis),
  mDataContainer(new QCPDataContainer<DataType>)
{
}

template <class DataType>
/**
 * @brief
 *
 */
QCPAbstractPlottable1D<DataType>::~QCPAbstractPlottable1D()
{
}

/*!
  \copydoc QCPPlottableInterface1D::dataCount
*/
template <class DataType>
int QCPAbstractPlottable1D<DataType>::dataCount() const
{
  return mDataContainer->size();
}

/*!
  \copydoc QCPPlottableInterface1D::dataMainKey
*/
template <class DataType>
double QCPAbstractPlottable1D<DataType>::dataMainKey(int index) const
{
  if (index >= 0 && index < mDataContainer->size())
  {
    return (mDataContainer->constBegin()+index)->mainKey();
  } else
  {
    qDebug() << Q_FUNC_INFO << "Index out of bounds" << index;
    return 0;
  }
}

/*!
  \copydoc QCPPlottableInterface1D::dataSortKey
*/
template <class DataType>
double QCPAbstractPlottable1D<DataType>::dataSortKey(int index) const
{
  if (index >= 0 && index < mDataContainer->size())
  {
    return (mDataContainer->constBegin()+index)->sortKey();
  } else
  {
    qDebug() << Q_FUNC_INFO << "Index out of bounds" << index;
    return 0;
  }
}

/*!
  \copydoc QCPPlottableInterface1D::dataMainValue
*/
template <class DataType>
double QCPAbstractPlottable1D<DataType>::dataMainValue(int index) const
{
  if (index >= 0 && index < mDataContainer->size())
  {
    return (mDataContainer->constBegin()+index)->mainValue();
  } else
  {
    qDebug() << Q_FUNC_INFO << "Index out of bounds" << index;
    return 0;
  }
}

/*!
  \copydoc QCPPlottableInterface1D::dataValueRange
*/
template <class DataType>
QCPRange QCPAbstractPlottable1D<DataType>::dataValueRange(int index) const
{
  if (index >= 0 && index < mDataContainer->size())
  {
    return (mDataContainer->constBegin()+index)->valueRange();
  } else
  {
    qDebug() << Q_FUNC_INFO << "Index out of bounds" << index;
    return QCPRange(0, 0);
  }
}

/*!
  \copydoc QCPPlottableInterface1D::dataPixelPosition
*/
template <class DataType>
QPointF QCPAbstractPlottable1D<DataType>::dataPixelPosition(int index) const
{
  if (index >= 0 && index < mDataContainer->size())
  {
    const typename QCPDataContainer<DataType>::const_iterator it = mDataContainer->constBegin()+index;
    return coordsToPixels(it->mainKey(), it->mainValue());
  } else
  {
    qDebug() << Q_FUNC_INFO << "Index out of bounds" << index;
    return QPointF();
  }
}

/*!
  \copydoc QCPPlottableInterface1D::sortKeyIsMainKey
*/
template <class DataType>
bool QCPAbstractPlottable1D<DataType>::sortKeyIsMainKey() const
{
  return DataType::sortKeyIsMainKey();
}

/*!
  Implements a rect-selection algorithm assuming the data (accessed via the 1D data interface) is
  point-like. Most subclasses will want to reimplement this method again, to provide a more
  accurate hit test based on the true data visualization geometry.

  \seebaseclassmethod
*/
template <class DataType>
QCPDataSelection QCPAbstractPlottable1D<DataType>::selectTestRect(const QRectF &rect, bool onlySelectable) const
{
  QCPDataSelection result;
  if ((onlySelectable && mSelectable == QCP::stNone) || mDataContainer->isEmpty())
    return result;
  if (!mKeyAxis || !mValueAxis)
    return result;
  
  // convert rect given in pixels to ranges given in plot coordinates:
  double key1, value1, key2, value2;
  pixelsToCoords(rect.topLeft(), key1, value1);
  pixelsToCoords(rect.bottomRight(), key2, value2);
  QCPRange keyRange(key1, key2); // QCPRange normalizes internally so we don't have to care about whether key1 < key2
  QCPRange valueRange(value1, value2);
  typename QCPDataContainer<DataType>::const_iterator begin = mDataContainer->constBegin();
  typename QCPDataContainer<DataType>::const_iterator end = mDataContainer->constEnd();
  if (DataType::sortKeyIsMainKey()) // we can assume that data is sorted by main key, so can reduce the searched key interval:
  {
    begin = mDataContainer->findBegin(keyRange.lower, false);
    end = mDataContainer->findEnd(keyRange.upper, false);
  }
  if (begin == end)
    return result;
  
  int currentSegmentBegin = -1; // -1 means we're currently not in a segment that's contained in rect
  for (typename QCPDataContainer<DataType>::const_iterator it=begin; it!=end; ++it)
  {
    if (currentSegmentBegin == -1)
    {
      if (valueRange.contains(it->mainValue()) && keyRange.contains(it->mainKey())) // start segment
        currentSegmentBegin = it-mDataContainer->constBegin();
    } else if (!valueRange.contains(it->mainValue()) || !keyRange.contains(it->mainKey())) // segment just ended
    {
      result.addDataRange(QCPDataRange(currentSegmentBegin, it-mDataContainer->constBegin()), false);
      currentSegmentBegin = -1;
    }
  }
  // process potential last segment:
  if (currentSegmentBegin != -1)
    result.addDataRange(QCPDataRange(currentSegmentBegin, end-mDataContainer->constBegin()), false);
  
  result.simplify();
  return result;
}

/*!
  \copydoc QCPPlottableInterface1D::findBegin
*/
template <class DataType>
int QCPAbstractPlottable1D<DataType>::findBegin(double sortKey, bool expandedRange) const
{
  return mDataContainer->findBegin(sortKey, expandedRange)-mDataContainer->constBegin();
}

/*!
  \copydoc QCPPlottableInterface1D::findEnd
*/
template <class DataType>
int QCPAbstractPlottable1D<DataType>::findEnd(double sortKey, bool expandedRange) const
{
  return mDataContainer->findEnd(sortKey, expandedRange)-mDataContainer->constBegin();
}

/*!
  Implements a point-selection algorithm assuming the data (accessed via the 1D data interface) is
  point-like. Most subclasses will want to reimplement this method again, to provide a more
  accurate hit test based on the true data visualization geometry.

  \seebaseclassmethod
*/
template <class DataType>
double QCPAbstractPlottable1D<DataType>::selectTest(const QPointF &pos, bool onlySelectable, QVariant *details) const
{
  if ((onlySelectable && mSelectable == QCP::stNone) || mDataContainer->isEmpty())
    return -1;
  if (!mKeyAxis || !mValueAxis)
    return -1;
  
  QCPDataSelection selectionResult;
  double minDistSqr = std::numeric_limits<double>::max();
  int minDistIndex = mDataContainer->size();
  
  typename QCPDataContainer<DataType>::const_iterator begin = mDataContainer->constBegin();
  typename QCPDataContainer<DataType>::const_iterator end = mDataContainer->constEnd();
  if (DataType::sortKeyIsMainKey()) // we can assume that data is sorted by main key, so can reduce the searched key interval:
  {
    // determine which key range comes into question, taking selection tolerance around pos into account:
    double posKeyMin, posKeyMax, dummy;
    pixelsToCoords(pos-QPointF(mParentPlot->selectionTolerance(), mParentPlot->selectionTolerance()), posKeyMin, dummy);
    pixelsToCoords(pos+QPointF(mParentPlot->selectionTolerance(), mParentPlot->selectionTolerance()), posKeyMax, dummy);
    if (posKeyMin > posKeyMax)
      qSwap(posKeyMin, posKeyMax);
    begin = mDataContainer->findBegin(posKeyMin, true);
    end = mDataContainer->findEnd(posKeyMax, true);
  }
  if (begin == end)
    return -1;
  QCPRange keyRange(mKeyAxis->range());
  QCPRange valueRange(mValueAxis->range());
  for (typename QCPDataContainer<DataType>::const_iterator it=begin; it!=end; ++it)
  {
    const double mainKey = it->mainKey();
    const double mainValue = it->mainValue();
    if (keyRange.contains(mainKey) && valueRange.contains(mainValue)) // make sure data point is inside visible range, for speedup in cases where sort key isn't main key and we iterate over all points
    {
      const double currentDistSqr = QCPVector2D(coordsToPixels(mainKey, mainValue)-pos).lengthSquared();
      if (currentDistSqr < minDistSqr)
      {
        minDistSqr = currentDistSqr;
        minDistIndex = it-mDataContainer->constBegin();
      }
    }
  }
  if (minDistIndex != mDataContainer->size())
    selectionResult.addDataRange(QCPDataRange(minDistIndex, minDistIndex+1), false);
  
  selectionResult.simplify();
  if (details)
    details->setValue(selectionResult);
  return qSqrt(minDistSqr);
}

/*!
  Splits all data into selected and unselected segments and outputs them via \a selectedSegments
  and \a unselectedSegments, respectively.

  This is useful when subclasses implement their \ref draw method and need to draw selected
  segments with a different pen/brush than unselected segments (also see \ref
  QCPSelectionDecorator).

  \see setSelection
*/
template <class DataType>
void QCPAbstractPlottable1D<DataType>::getDataSegments(QList<QCPDataRange> &selectedSegments, QList<QCPDataRange> &unselectedSegments) const
{
  selectedSegments.clear();
  unselectedSegments.clear();
  if (mSelectable == QCP::stWhole) // stWhole selection type draws the entire plottable with selected style if mSelection isn't empty
  {
    if (selected())
      selectedSegments << QCPDataRange(0, dataCount());
    else
      unselectedSegments << QCPDataRange(0, dataCount());
  } else
  {
    QCPDataSelection sel(selection());
    sel.simplify();
    selectedSegments = sel.dataRanges();
    unselectedSegments = sel.inverse(QCPDataRange(0, dataCount())).dataRanges();
  }
}

/*!
  A helper method which draws a line with the passed \a painter, according to the pixel data in \a
  lineData. NaN points create gaps in the line, as expected from QCustomPlot's plottables (this is
  the main difference to QPainter's regular drawPolyline, which handles NaNs by lagging or
  crashing).

  Further it uses a faster line drawing technique based on \ref QCPPainter::drawLine rather than \c
  QPainter::drawPolyline if the configured \ref QCustomPlot::setPlottingHints() and \a painter
  style allows.
*/
template <class DataType>
void QCPAbstractPlottable1D<DataType>::drawPolyline(QCPPainter *painter, const QVector<QPointF> &lineData) const
{
  // if drawing solid line and not in PDF, use much faster line drawing instead of polyline:
  if (mParentPlot->plottingHints().testFlag(QCP::phFastPolylines) &&
      painter->pen().style() == Qt::SolidLine &&
      !painter->modes().testFlag(QCPPainter::pmVectorized) &&
      !painter->modes().testFlag(QCPPainter::pmNoCaching))
  {
    int i = 0;
    bool lastIsNan = false;
    const int lineDataSize = lineData.size();
    while (i < lineDataSize && (qIsNaN(lineData.at(i).y()) || qIsNaN(lineData.at(i).x()))) // make sure first point is not NaN
      ++i;
    ++i; // because drawing works in 1 point retrospect
    while (i < lineDataSize)
    {
      if (!qIsNaN(lineData.at(i).y()) && !qIsNaN(lineData.at(i).x())) // NaNs create a gap in the line
      {
        if (!lastIsNan)
          painter->drawLine(lineData.at(i-1), lineData.at(i));
        else
          lastIsNan = false;
      } else
        lastIsNan = true;
      ++i;
    }
  } else
  {
    int segmentStart = 0;
    int i = 0;
    const int lineDataSize = lineData.size();
    while (i < lineDataSize)
    {
      if (qIsNaN(lineData.at(i).y()) || qIsNaN(lineData.at(i).x()) || qIsInf(lineData.at(i).y())) // NaNs create a gap in the line. Also filter Infs which make drawPolyline block
      {
        painter->drawPolyline(lineData.constData()+segmentStart, i-segmentStart); // i, because we don't want to include the current NaN point
        segmentStart = i+1;
      }
      ++i;
    }
    // draw last segment:
    painter->drawPolyline(lineData.constData()+segmentStart, lineDataSize-segmentStart);
  }
}
/* end of 'src/plottable1d.cpp' */


/* end of 'src/plottable1d.h' */


/* including file 'src/colorgradient.h', size 6243                           */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPColorGradient
{
  Q_GADGET
public:
  /*!
    Defines the color spaces in which color interpolation between gradient stops can be performed.
    
    \see setColorInterpolation
  */
  enum ColorInterpolation { ciRGB  ///< Color channels red, green and blue are linearly interpolated
                            ,ciHSV ///< Color channels hue, saturation and value are linearly interpolated (The hue is interpolated over the shortest angle distance)
                          };
  Q_ENUMS(ColorInterpolation)
  
  /*!
    Defines the available presets that can be loaded with \ref loadPreset. See the documentation
    there for an image of the presets.
  */
  enum GradientPreset { gpGrayscale  ///< Continuous lightness from black to white (suited for non-biased data representation)
                        ,gpHot       ///< Continuous lightness from black over firey colors to white (suited for non-biased data representation)
                        ,gpCold      ///< Continuous lightness from black over icey colors to white (suited for non-biased data representation)
                        ,gpNight     ///< Continuous lightness from black over weak blueish colors to white (suited for non-biased data representation)
                        ,gpCandy     ///< Blue over pink to white
                        ,gpGeography ///< Colors suitable to represent different elevations on geographical maps
                        ,gpIon       ///< Half hue spectrum from black over purple to blue and finally green (creates banding illusion but allows more precise magnitude estimates)
                        ,gpThermal   ///< Colors suitable for thermal imaging, ranging from dark blue over purple to orange, yellow and white
                        ,gpPolar     ///< Colors suitable to emphasize polarity around the center, with blue for negative, black in the middle and red for positive values
                        ,gpSpectrum  ///< An approximation of the visible light spectrum (creates banding illusion but allows more precise magnitude estimates)
                        ,gpJet       ///< Hue variation similar to a spectrum, often used in numerical visualization (creates banding illusion but allows more precise magnitude estimates)
                        ,gpHues      ///< Full hue cycle, with highest and lowest color red (suitable for periodic data, such as angles and phases, see \ref setPeriodic)
                      };
  Q_ENUMS(GradientPreset)
  
  /**
   * @brief
   *
   */
  QCPColorGradient();
  /**
   * @brief
   *
   * @param preset
   */
  QCPColorGradient(GradientPreset preset);
  /**
   * @brief
   *
   * @param other
   * @return bool operator
   */
  bool operator==(const QCPColorGradient &other) const;
  /**
   * @brief
   *
   * @param other
   * @return bool operator
   */
  bool operator!=(const QCPColorGradient &other) const { return !(*this == other); }
  
  // getters:
  /**
   * @brief
   *
   * @return int
   */
  int levelCount() const { return mLevelCount; }
  /**
   * @brief
   *
   * @return QMap<double, QColor>
   */
  QMap<double, QColor> colorStops() const { return mColorStops; }
  /**
   * @brief
   *
   * @return ColorInterpolation
   */
  ColorInterpolation colorInterpolation() const { return mColorInterpolation; }
  /**
   * @brief
   *
   * @return bool
   */
  bool periodic() const { return mPeriodic; }
  
  // setters:
  /**
   * @brief
   *
   * @param n
   */
  void setLevelCount(int n);
  /**
   * @brief
   *
   * @param QMap<double
   * @param colorStops
   */
  void setColorStops(const QMap<double, QColor> &colorStops);
  /**
   * @brief
   *
   * @param position
   * @param color
   */
  void setColorStopAt(double position, const QColor &color);
  /**
   * @brief
   *
   * @param interpolation
   */
  void setColorInterpolation(ColorInterpolation interpolation);
  /**
   * @brief
   *
   * @param enabled
   */
  void setPeriodic(bool enabled);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param data
   * @param range
   * @param scanLine
   * @param n
   * @param dataIndexFactor
   * @param logarithmic
   */
  void colorize(const double *data, const QCPRange &range, QRgb *scanLine, int n, int dataIndexFactor=1, bool logarithmic=false);
  /**
   * @brief
   *
   * @param data
   * @param alpha
   * @param range
   * @param scanLine
   * @param n
   * @param dataIndexFactor
   * @param logarithmic
   */
  void colorize(const double *data, const unsigned char *alpha, const QCPRange &range, QRgb *scanLine, int n, int dataIndexFactor=1, bool logarithmic=false);
  /**
   * @brief
   *
   * @param position
   * @param range
   * @param logarithmic
   * @return QRgb
   */
  QRgb color(double position, const QCPRange &range, bool logarithmic=false);
  /**
   * @brief
   *
   * @param preset
   */
  void loadPreset(GradientPreset preset);
  /**
   * @brief
   *
   */
  void clearColorStops();
  /**
   * @brief
   *
   * @return QCPColorGradient
   */
  QCPColorGradient inverted() const;
  
protected:
  // property members:
  int mLevelCount; /**< TODO: describe */
  QMap<double, QColor> mColorStops; /**< TODO: describe */
  ColorInterpolation mColorInterpolation; /**< TODO: describe */
  bool mPeriodic; /**< TODO: describe */
  
  // non-property members:
  QVector<QRgb> mColorBuffer; // have colors premultiplied with alpha (for usage with QImage::Format_ARGB32_Premultiplied) /**< TODO: describe */
  bool mColorBufferInvalidated; /**< TODO: describe */
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return bool
   */
  bool stopsUseAlpha() const;
  /**
   * @brief
   *
   */
  void updateColorBuffer();
};
Q_DECLARE_METATYPE(QCPColorGradient::ColorInterpolation)
Q_DECLARE_METATYPE(QCPColorGradient::GradientPreset)

/* end of 'src/colorgradient.h' */


/* including file 'src/selectiondecorator-bracket.h', size 4426              */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPSelectionDecoratorBracket : public QCPSelectionDecorator
{
  Q_GADGET
public:
  
  /*!
    Defines which shape is drawn at the boundaries of selected data ranges.
    
    Some of the bracket styles further allow specifying a height and/or width, see \ref
    setBracketHeight and \ref setBracketWidth.
  */
  enum BracketStyle { bsSquareBracket ///< A square bracket is drawn.
                      ,bsHalfEllipse   ///< A half ellipse is drawn. The size of the ellipse is given by the bracket width/height properties.
                      ,bsEllipse       ///< An ellipse is drawn. The size of the ellipse is given by the bracket width/height properties.
                      ,bsPlus         ///< A plus is drawn.
                      ,bsUserStyle    ///< Start custom bracket styles at this index when subclassing and reimplementing \ref drawBracket.
  };
  Q_ENUMS(BracketStyle)
  
  /**
   * @brief
   *
   */
  QCPSelectionDecoratorBracket();
  /**
   * @brief
   *
   */
  virtual ~QCPSelectionDecoratorBracket();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen bracketPen() const { return mBracketPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush bracketBrush() const { return mBracketBrush; }
  /**
   * @brief
   *
   * @return int
   */
  int bracketWidth() const { return mBracketWidth; }
  /**
   * @brief
   *
   * @return int
   */
  int bracketHeight() const { return mBracketHeight; }
  /**
   * @brief
   *
   * @return BracketStyle
   */
  BracketStyle bracketStyle() const { return mBracketStyle; }
  /**
   * @brief
   *
   * @return bool
   */
  bool tangentToData() const { return mTangentToData; }
  /**
   * @brief
   *
   * @return int
   */
  int tangentAverage() const { return mTangentAverage; }
  
  // setters:
  /**
   * @brief
   *
   * @param pen
   */
  void setBracketPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBracketBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param width
   */
  void setBracketWidth(int width);
  /**
   * @brief
   *
   * @param height
   */
  void setBracketHeight(int height);
  /**
   * @brief
   *
   * @param style
   */
  void setBracketStyle(BracketStyle style);
  /**
   * @brief
   *
   * @param enabled
   */
  void setTangentToData(bool enabled);
  /**
   * @brief
   *
   * @param pointCount
   */
  void setTangentAverage(int pointCount);
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param painter
   * @param direction
   */
  virtual void drawBracket(QCPPainter *painter, int direction) const;
  
  // virtual methods:
  /**
   * @brief
   *
   * @param painter
   * @param selection
   */
  virtual void drawDecoration(QCPPainter *painter, QCPDataSelection selection);
  
protected:
  // property members:
  QPen mBracketPen; /**< TODO: describe */
  QBrush mBracketBrush; /**< TODO: describe */
  int mBracketWidth; /**< TODO: describe */
  int mBracketHeight; /**< TODO: describe */
  BracketStyle mBracketStyle; /**< TODO: describe */
  bool mTangentToData; /**< TODO: describe */
  int mTangentAverage; /**< TODO: describe */
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param interface1d
   * @param dataIndex
   * @param direction
   * @return double
   */
  double getTangentAngle(const QCPPlottableInterface1D *interface1d, int dataIndex, int direction) const;
  /**
   * @brief
   *
   * @param interface1d
   * @param dataIndex
   * @return QPointF
   */
  QPointF getPixelCoordinates(const QCPPlottableInterface1D *interface1d, int dataIndex) const;
  
};
Q_DECLARE_METATYPE(QCPSelectionDecoratorBracket::BracketStyle)

/* end of 'src/selectiondecorator-bracket.h' */


/* including file 'src/layoutelements/layoutelement-axisrect.h', size 7528   */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAxisRect : public QCPLayoutElement
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPixmap background READ background WRITE setBackground)
  Q_PROPERTY(bool backgroundScaled READ backgroundScaled WRITE setBackgroundScaled)
  Q_PROPERTY(Qt::AspectRatioMode backgroundScaledMode READ backgroundScaledMode WRITE setBackgroundScaledMode)
  Q_PROPERTY(Qt::Orientations rangeDrag READ rangeDrag WRITE setRangeDrag)
  Q_PROPERTY(Qt::Orientations rangeZoom READ rangeZoom WRITE setRangeZoom)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   * @param setupDefaultAxes
   */
  explicit QCPAxisRect(QCustomPlot *parentPlot, bool setupDefaultAxes=true);
  /**
   * @brief
   *
   */
  virtual ~QCPAxisRect();
  
  // getters:
  /**
   * @brief
   *
   * @return QPixmap
   */
  QPixmap background() const { return mBackgroundPixmap; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush backgroundBrush() const { return mBackgroundBrush; }
  /**
   * @brief
   *
   * @return bool
   */
  bool backgroundScaled() const { return mBackgroundScaled; }
  /**
   * @brief
   *
   * @return Qt::AspectRatioMode
   */
  Qt::AspectRatioMode backgroundScaledMode() const { return mBackgroundScaledMode; }
  /**
   * @brief
   *
   * @return Qt::Orientations
   */
  Qt::Orientations rangeDrag() const { return mRangeDrag; }
  /**
   * @brief
   *
   * @return Qt::Orientations
   */
  Qt::Orientations rangeZoom() const { return mRangeZoom; }
  /**
   * @brief
   *
   * @param orientation
   * @return QCPAxis
   */
  QCPAxis *rangeDragAxis(Qt::Orientation orientation);
  /**
   * @brief
   *
   * @param orientation
   * @return QCPAxis
   */
  QCPAxis *rangeZoomAxis(Qt::Orientation orientation);
  /**
   * @brief
   *
   * @param orientation
   * @return QList<QCPAxis *>
   */
  QList<QCPAxis*> rangeDragAxes(Qt::Orientation orientation);
  /**
   * @brief
   *
   * @param orientation
   * @return QList<QCPAxis *>
   */
  QList<QCPAxis*> rangeZoomAxes(Qt::Orientation orientation);
  /**
   * @brief
   *
   * @param orientation
   * @return double
   */
  double rangeZoomFactor(Qt::Orientation orientation);
  
  // setters:
  /**
   * @brief
   *
   * @param pm
   */
  void setBackground(const QPixmap &pm);
  /**
   * @brief
   *
   * @param pm
   * @param scaled
   * @param mode
   */
  void setBackground(const QPixmap &pm, bool scaled, Qt::AspectRatioMode mode=Qt::KeepAspectRatioByExpanding);
  /**
   * @brief
   *
   * @param brush
   */
  void setBackground(const QBrush &brush);
  /**
   * @brief
   *
   * @param scaled
   */
  void setBackgroundScaled(bool scaled);
  /**
   * @brief
   *
   * @param mode
   */
  void setBackgroundScaledMode(Qt::AspectRatioMode mode);
  /**
   * @brief
   *
   * @param orientations
   */
  void setRangeDrag(Qt::Orientations orientations);
  /**
   * @brief
   *
   * @param orientations
   */
  void setRangeZoom(Qt::Orientations orientations);
  /**
   * @brief
   *
   * @param horizontal
   * @param vertical
   */
  void setRangeDragAxes(QCPAxis *horizontal, QCPAxis *vertical);
  /**
   * @brief
   *
   * @param axes
   */
  void setRangeDragAxes(QList<QCPAxis*> axes);
  /**
   * @brief
   *
   * @param horizontal
   * @param vertical
   */
  void setRangeDragAxes(QList<QCPAxis*> horizontal, QList<QCPAxis*> vertical);
  /**
   * @brief
   *
   * @param horizontal
   * @param vertical
   */
  void setRangeZoomAxes(QCPAxis *horizontal, QCPAxis *vertical);
  /**
   * @brief
   *
   * @param axes
   */
  void setRangeZoomAxes(QList<QCPAxis*> axes);
  /**
   * @brief
   *
   * @param horizontal
   * @param vertical
   */
  void setRangeZoomAxes(QList<QCPAxis*> horizontal, QList<QCPAxis*> vertical);
  /**
   * @brief
   *
   * @param horizontalFactor
   * @param verticalFactor
   */
  void setRangeZoomFactor(double horizontalFactor, double verticalFactor);
  /**
   * @brief
   *
   * @param factor
   */
  void setRangeZoomFactor(double factor);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param type
   * @return int
   */
  int axisCount(QCPAxis::AxisType type) const;
  /**
   * @brief
   *
   * @param type
   * @param index
   * @return QCPAxis
   */
  QCPAxis *axis(QCPAxis::AxisType type, int index=0) const;
  /**
   * @brief
   *
   * @param types
   * @return QList<QCPAxis *>
   */
  QList<QCPAxis*> axes(QCPAxis::AxisTypes types) const;
  /**
   * @brief
   *
   * @return QList<QCPAxis *>
   */
  QList<QCPAxis*> axes() const;
  /**
   * @brief
   *
   * @param type
   * @param axis
   * @return QCPAxis
   */
  QCPAxis *addAxis(QCPAxis::AxisType type, QCPAxis *axis=0);
  /**
   * @brief
   *
   * @param types
   * @return QList<QCPAxis *>
   */
  QList<QCPAxis*> addAxes(QCPAxis::AxisTypes types);
  /**
   * @brief
   *
   * @param axis
   * @return bool
   */
  bool removeAxis(QCPAxis *axis);
  /**
   * @brief
   *
   * @return QCPLayoutInset
   */
  QCPLayoutInset *insetLayout() const { return mInsetLayout; }
  
  /**
   * @brief
   *
   * @param pixelRect
   */
  void zoom(const QRectF &pixelRect);
  /**
   * @brief
   *
   * @param pixelRect
   * @param affectedAxes
   */
  void zoom(const QRectF &pixelRect, const QList<QCPAxis*> &affectedAxes);
  /**
   * @brief
   *
   * @param connectRanges
   */
  void setupFullAxesBox(bool connectRanges=false);
  /**
   * @brief
   *
   * @return QList<QCPAbstractPlottable *>
   */
  QList<QCPAbstractPlottable*> plottables() const;
  /**
   * @brief
   *
   * @return QList<QCPGraph *>
   */
  QList<QCPGraph*> graphs() const;
  /**
   * @brief
   *
   * @return QList<QCPAbstractItem *>
   */
  QList<QCPAbstractItem*> items() const;
  
  // read-only interface imitating a QRect:
  /**
   * @brief
   *
   * @return int
   */
  int left() const { return mRect.left(); }
  /**
   * @brief
   *
   * @return int
   */
  int right() const { return mRect.right(); }
  /**
   * @brief
   *
   * @return int
   */
  int top() const { return mRect.top(); }
  /**
   * @brief
   *
   * @return int
   */
  int bottom() const { return mRect.bottom(); }
  /**
   * @brief
   *
   * @return int
   */
  int width() const { return mRect.width(); }
  /**
   * @brief
   *
   * @return int
   */
  int height() const { return mRect.height(); }
  /**
   * @brief
   *
   * @return QSize
   */
  QSize size() const { return mRect.size(); }
  /**
   * @brief
   *
   * @return QPoint
   */
  QPoint topLeft() const { return mRect.topLeft(); }
  /**
   * @brief
   *
   * @return QPoint
   */
  QPoint topRight() const { return mRect.topRight(); }
  /**
   * @brief
   *
   * @return QPoint
   */
  QPoint bottomLeft() const { return mRect.bottomLeft(); }
  /**
   * @brief
   *
   * @return QPoint
   */
  QPoint bottomRight() const { return mRect.bottomRight(); }
  /**
   * @brief
   *
   * @return QPoint
   */
  QPoint center() const { return mRect.center(); }
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param phase
   */
  virtual void update(UpdatePhase phase) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param recursive
   * @return QList<QCPLayoutElement *>
   */
  virtual QList<QCPLayoutElement*> elements(bool recursive) const Q_DECL_OVERRIDE;

protected:
  // property members:
  QBrush mBackgroundBrush; /**< TODO: describe */
  QPixmap mBackgroundPixmap; /**< TODO: describe */
  QPixmap mScaledBackgroundPixmap; /**< TODO: describe */
  bool mBackgroundScaled; /**< TODO: describe */
  Qt::AspectRatioMode mBackgroundScaledMode; /**< TODO: describe */
  QCPLayoutInset *mInsetLayout; /**< TODO: describe */
  Qt::Orientations mRangeDrag, mRangeZoom; /**< TODO: describe */
  QList<QPointer<QCPAxis> > mRangeDragHorzAxis, mRangeDragVertAxis; /**< TODO: describe */
  QList<QPointer<QCPAxis> > mRangeZoomHorzAxis, mRangeZoomVertAxis; /**< TODO: describe */
  double mRangeZoomFactorHorz, mRangeZoomFactorVert; /**< TODO: describe */
  
  // non-property members:
  QList<QCPRange> mDragStartHorzRange, mDragStartVertRange; /**< TODO: describe */
  QCP::AntialiasedElements mAADragBackup, mNotAADragBackup; /**< TODO: describe */
  QPoint mDragStart; /**< TODO: describe */
  bool mDragging; /**< TODO: describe */
  QHash<QCPAxis::AxisType, QList<QCPAxis*> > mAxes; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param side
   * @return int
   */
  virtual int calculateAutoMargin(QCP::MarginSide side) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   */
  virtual void layoutChanged() Q_DECL_OVERRIDE;
  // events:
  /**
   * @brief
   *
   * @param event
   * @param details
   */
  virtual void mousePressEvent(QMouseEvent *event, const QVariant &details) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   * @param startPos
   */
  virtual void mouseMoveEvent(QMouseEvent *event, const QPointF &startPos) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   * @param startPos
   */
  virtual void mouseReleaseEvent(QMouseEvent *event, const QPointF &startPos) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
  
  // non-property methods:
  /**
   * @brief
   *
   * @param painter
   */
  void drawBackground(QCPPainter *painter);
  /**
   * @brief
   *
   * @param type
   */
  void updateAxesOffset(QCPAxis::AxisType type);
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPAxisRect)
  
  friend class QCustomPlot;
};


/* end of 'src/layoutelements/layoutelement-axisrect.h' */


/* including file 'src/layoutelements/layoutelement-legend.h', size 10392    */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPAbstractLegendItem : public QCPLayoutElement
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QCPLegend* parentLegend READ parentLegend)
  Q_PROPERTY(QFont font READ font WRITE setFont)
  Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor)
  Q_PROPERTY(QFont selectedFont READ selectedFont WRITE setSelectedFont)
  Q_PROPERTY(QColor selectedTextColor READ selectedTextColor WRITE setSelectedTextColor)
  Q_PROPERTY(bool selectable READ selectable WRITE setSelectable NOTIFY selectionChanged)
  Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectableChanged)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit QCPAbstractLegendItem(QCPLegend *parent);
  
  // getters:
  /**
   * @brief
   *
   * @return QCPLegend
   */
  QCPLegend *parentLegend() const { return mParentLegend; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont font() const { return mFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor textColor() const { return mTextColor; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont selectedFont() const { return mSelectedFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor selectedTextColor() const { return mSelectedTextColor; }
  /**
   * @brief
   *
   * @return bool
   */
  bool selectable() const { return mSelectable; }
  /**
   * @brief
   *
   * @return bool
   */
  bool selected() const { return mSelected; }
  
  // setters:
  /**
   * @brief
   *
   * @param font
   */
  void setFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setTextColor(const QColor &color);
  /**
   * @brief
   *
   * @param font
   */
  void setSelectedFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setSelectedTextColor(const QColor &color);
  /**
   * @brief
   *
   * @param selectable
   */
  Q_SLOT void setSelectable(bool selectable);
  /**
   * @brief
   *
   * @param selected
   */
  Q_SLOT void setSelected(bool selected);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
signals:
  /**
   * @brief
   *
   * @param selected
   */
  void selectionChanged(bool selected);
  /**
   * @brief
   *
   * @param selectable
   */
  void selectableChanged(bool selectable);
  
protected:
  // property members:
  QCPLegend *mParentLegend; /**< TODO: describe */
  QFont mFont; /**< TODO: describe */
  QColor mTextColor; /**< TODO: describe */
  QFont mSelectedFont; /**< TODO: describe */
  QColor mSelectedTextColor; /**< TODO: describe */
  bool mSelectable, mSelected; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @return QCP::Interaction
   */
  virtual QCP::Interaction selectionCategory() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QRect
   */
  virtual QRect clipRect() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE = 0;
  // events:
  /**
   * @brief
   *
   * @param event
   * @param additive
   * @param details
   * @param selectionStateChanged
   */
  virtual void selectEvent(QMouseEvent *event, bool additive, const QVariant &details, bool *selectionStateChanged) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param selectionStateChanged
   */
  virtual void deselectEvent(bool *selectionStateChanged) Q_DECL_OVERRIDE;
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPAbstractLegendItem)
  
  friend class QCPLegend;
};


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPPlottableLegendItem : public QCPAbstractLegendItem
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parent
   * @param plottable
   */
  QCPPlottableLegendItem(QCPLegend *parent, QCPAbstractPlottable *plottable);
  
  // getters:
  /**
   * @brief
   *
   * @return QCPAbstractPlottable
   */
  QCPAbstractPlottable *plottable() { return mPlottable; }
  
protected:
  // property members:
  QCPAbstractPlottable *mPlottable; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize minimumSizeHint() const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen getIconBorderPen() const;
  /**
   * @brief
   *
   * @return QColor
   */
  QColor getTextColor() const;
  /**
   * @brief
   *
   * @return QFont
   */
  QFont getFont() const;
};


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPLegend : public QCPLayoutGrid
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPen borderPen READ borderPen WRITE setBorderPen)
  Q_PROPERTY(QBrush brush READ brush WRITE setBrush)
  Q_PROPERTY(QFont font READ font WRITE setFont)
  Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor)
  Q_PROPERTY(QSize iconSize READ iconSize WRITE setIconSize)
  Q_PROPERTY(int iconTextPadding READ iconTextPadding WRITE setIconTextPadding)
  Q_PROPERTY(QPen iconBorderPen READ iconBorderPen WRITE setIconBorderPen)
  Q_PROPERTY(SelectableParts selectableParts READ selectableParts WRITE setSelectableParts NOTIFY selectionChanged)
  Q_PROPERTY(SelectableParts selectedParts READ selectedParts WRITE setSelectedParts NOTIFY selectableChanged)
  Q_PROPERTY(QPen selectedBorderPen READ selectedBorderPen WRITE setSelectedBorderPen)
  Q_PROPERTY(QPen selectedIconBorderPen READ selectedIconBorderPen WRITE setSelectedIconBorderPen)
  Q_PROPERTY(QBrush selectedBrush READ selectedBrush WRITE setSelectedBrush)
  Q_PROPERTY(QFont selectedFont READ selectedFont WRITE setSelectedFont)
  Q_PROPERTY(QColor selectedTextColor READ selectedTextColor WRITE setSelectedTextColor)
  /// \endcond
public:
  /*!
    Defines the selectable parts of a legend
    
    \see setSelectedParts, setSelectableParts
  */
  enum SelectablePart { spNone        = 0x000 ///< <tt>0x000</tt> None
                        ,spLegendBox  = 0x001 ///< <tt>0x001</tt> The legend box (frame)
                        ,spItems      = 0x002 ///< <tt>0x002</tt> Legend items individually (see \ref selectedItems)
                      };
  Q_ENUMS(SelectablePart)
  Q_FLAGS(SelectableParts)
  /**
   * @brief
   *
   */
  Q_DECLARE_FLAGS(SelectableParts, SelectablePart)
  
  /**
   * @brief
   *
   */
  explicit QCPLegend();
  /**
   * @brief
   *
   */
  virtual ~QCPLegend();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen borderPen() const { return mBorderPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont font() const { return mFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor textColor() const { return mTextColor; }
  /**
   * @brief
   *
   * @return QSize
   */
  QSize iconSize() const { return mIconSize; }
  /**
   * @brief
   *
   * @return int
   */
  int iconTextPadding() const { return mIconTextPadding; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen iconBorderPen() const { return mIconBorderPen; }
  /**
   * @brief
   *
   * @return SelectableParts
   */
  SelectableParts selectableParts() const { return mSelectableParts; }
  /**
   * @brief
   *
   * @return SelectableParts
   */
  SelectableParts selectedParts() const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedBorderPen() const { return mSelectedBorderPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedIconBorderPen() const { return mSelectedIconBorderPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush selectedBrush() const { return mSelectedBrush; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont selectedFont() const { return mSelectedFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor selectedTextColor() const { return mSelectedTextColor; }
  
  // setters:
  /**
   * @brief
   *
   * @param pen
   */
  void setBorderPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param font
   */
  void setFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setTextColor(const QColor &color);
  /**
   * @brief
   *
   * @param size
   */
  void setIconSize(const QSize &size);
  /**
   * @brief
   *
   * @param width
   * @param height
   */
  void setIconSize(int width, int height);
  /**
   * @brief
   *
   * @param padding
   */
  void setIconTextPadding(int padding);
  /**
   * @brief
   *
   * @param pen
   */
  void setIconBorderPen(const QPen &pen);
  /**
   * @brief
   *
   * @param selectableParts
   */
  Q_SLOT void setSelectableParts(const SelectableParts &selectableParts);
  /**
   * @brief
   *
   * @param selectedParts
   */
  Q_SLOT void setSelectedParts(const SelectableParts &selectedParts);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedBorderPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedIconBorderPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setSelectedBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param font
   */
  void setSelectedFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setSelectedTextColor(const QColor &color);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param index
   * @return QCPAbstractLegendItem
   */
  QCPAbstractLegendItem *item(int index) const;
  /**
   * @brief
   *
   * @param plottable
   * @return QCPPlottableLegendItem
   */
  QCPPlottableLegendItem *itemWithPlottable(const QCPAbstractPlottable *plottable) const;
  /**
   * @brief
   *
   * @return int
   */
  int itemCount() const;
  /**
   * @brief
   *
   * @param item
   * @return bool
   */
  bool hasItem(QCPAbstractLegendItem *item) const;
  /**
   * @brief
   *
   * @param plottable
   * @return bool
   */
  bool hasItemWithPlottable(const QCPAbstractPlottable *plottable) const;
  /**
   * @brief
   *
   * @param item
   * @return bool
   */
  bool addItem(QCPAbstractLegendItem *item);
  /**
   * @brief
   *
   * @param index
   * @return bool
   */
  bool removeItem(int index);
  /**
   * @brief
   *
   * @param item
   * @return bool
   */
  bool removeItem(QCPAbstractLegendItem *item);
  /**
   * @brief
   *
   */
  void clearItems();
  /**
   * @brief
   *
   * @return QList<QCPAbstractLegendItem *>
   */
  QList<QCPAbstractLegendItem*> selectedItems() const;
  
signals:
  /**
   * @brief
   *
   * @param parts
   */
  void selectionChanged(QCPLegend::SelectableParts parts);
  /**
   * @brief
   *
   * @param parts
   */
  void selectableChanged(QCPLegend::SelectableParts parts);
  
protected:
  // property members:
  QPen mBorderPen, mIconBorderPen; /**< TODO: describe */
  QBrush mBrush; /**< TODO: describe */
  QFont mFont; /**< TODO: describe */
  QColor mTextColor; /**< TODO: describe */
  QSize mIconSize; /**< TODO: describe */
  int mIconTextPadding; /**< TODO: describe */
  SelectableParts mSelectedParts, mSelectableParts; /**< TODO: describe */
  QPen mSelectedBorderPen, mSelectedIconBorderPen; /**< TODO: describe */
  QBrush mSelectedBrush; /**< TODO: describe */
  QFont mSelectedFont; /**< TODO: describe */
  QColor mSelectedTextColor; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param parentPlot
   */
  virtual void parentPlotInitialized(QCustomPlot *parentPlot) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QCP::Interaction
   */
  virtual QCP::Interaction selectionCategory() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  // events:
  /**
   * @brief
   *
   * @param event
   * @param additive
   * @param details
   * @param selectionStateChanged
   */
  virtual void selectEvent(QMouseEvent *event, bool additive, const QVariant &details, bool *selectionStateChanged) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param selectionStateChanged
   */
  virtual void deselectEvent(bool *selectionStateChanged) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen getBorderPen() const;
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush getBrush() const;
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPLegend)
  
  friend class QCustomPlot;
  friend class QCPAbstractLegendItem;
};
Q_DECLARE_OPERATORS_FOR_FLAGS(QCPLegend::SelectableParts)
Q_DECLARE_METATYPE(QCPLegend::SelectablePart)

/* end of 'src/layoutelements/layoutelement-legend.h' */


/* including file 'src/layoutelements/layoutelement-textelement.h', size 5343 */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200  */

class QCP_LIB_DECL QCPTextElement : public QCPLayoutElement
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QString text READ text WRITE setText)
  Q_PROPERTY(QFont font READ font WRITE setFont)
  Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor)
  Q_PROPERTY(QFont selectedFont READ selectedFont WRITE setSelectedFont)
  Q_PROPERTY(QColor selectedTextColor READ selectedTextColor WRITE setSelectedTextColor)
  Q_PROPERTY(bool selectable READ selectable WRITE setSelectable NOTIFY selectableChanged)
  Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectionChanged)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPTextElement(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   * @param parentPlot
   * @param text
   */
  QCPTextElement(QCustomPlot *parentPlot, const QString &text);
  /**
   * @brief
   *
   * @param parentPlot
   * @param text
   * @param pointSize
   */
  QCPTextElement(QCustomPlot *parentPlot, const QString &text, double pointSize);
  /**
   * @brief
   *
   * @param parentPlot
   * @param text
   * @param fontFamily
   * @param pointSize
   */
  QCPTextElement(QCustomPlot *parentPlot, const QString &text, const QString &fontFamily, double pointSize);
  /**
   * @brief
   *
   * @param parentPlot
   * @param text
   * @param font
   */
  QCPTextElement(QCustomPlot *parentPlot, const QString &text, const QFont &font);
  
  // getters:
  /**
   * @brief
   *
   * @return QString
   */
  QString text() const { return mText; }
  /**
   * @brief
   *
   * @return int
   */
  int textFlags() const { return mTextFlags; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont font() const { return mFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor textColor() const { return mTextColor; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont selectedFont() const { return mSelectedFont; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor selectedTextColor() const { return mSelectedTextColor; }
  /**
   * @brief
   *
   * @return bool
   */
  bool selectable() const { return mSelectable; }
  /**
   * @brief
   *
   * @return bool
   */
  bool selected() const { return mSelected; }
  
  // setters:
  /**
   * @brief
   *
   * @param text
   */
  void setText(const QString &text);
  /**
   * @brief
   *
   * @param flags
   */
  void setTextFlags(int flags);
  /**
   * @brief
   *
   * @param font
   */
  void setFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setTextColor(const QColor &color);
  /**
   * @brief
   *
   * @param font
   */
  void setSelectedFont(const QFont &font);
  /**
   * @brief
   *
   * @param color
   */
  void setSelectedTextColor(const QColor &color);
  /**
   * @brief
   *
   * @param selectable
   */
  Q_SLOT void setSelectable(bool selectable);
  /**
   * @brief
   *
   * @param selected
   */
  Q_SLOT void setSelected(bool selected);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   * @param details
   */
  virtual void mousePressEvent(QMouseEvent *event, const QVariant &details) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   * @param startPos
   */
  virtual void mouseReleaseEvent(QMouseEvent *event, const QPointF &startPos) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   * @param details
   */
  virtual void mouseDoubleClickEvent(QMouseEvent *event, const QVariant &details) Q_DECL_OVERRIDE;
  
signals:
  /**
   * @brief
   *
   * @param selected
   */
  void selectionChanged(bool selected);
  /**
   * @brief
   *
   * @param selectable
   */
  void selectableChanged(bool selectable);
  /**
   * @brief
   *
   * @param event
   */
  void clicked(QMouseEvent *event);
  /**
   * @brief
   *
   * @param event
   */
  void doubleClicked(QMouseEvent *event);
  
protected:
  // property members:
  QString mText; /**< TODO: describe */
  int mTextFlags; /**< TODO: describe */
  QFont mFont; /**< TODO: describe */
  QColor mTextColor; /**< TODO: describe */
  QFont mSelectedFont; /**< TODO: describe */
  QColor mSelectedTextColor; /**< TODO: describe */
  QRect mTextBoundingRect; /**< TODO: describe */
  bool mSelectable, mSelected; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize minimumSizeHint() const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QSize
   */
  virtual QSize maximumSizeHint() const Q_DECL_OVERRIDE;
  // events:
  /**
   * @brief
   *
   * @param event
   * @param additive
   * @param details
   * @param selectionStateChanged
   */
  virtual void selectEvent(QMouseEvent *event, bool additive, const QVariant &details, bool *selectionStateChanged) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param selectionStateChanged
   */
  virtual void deselectEvent(bool *selectionStateChanged) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QFont
   */
  QFont mainFont() const;
  /**
   * @brief
   *
   * @return QColor
   */
  QColor mainTextColor() const;
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPTextElement)
};



/* end of 'src/layoutelements/layoutelement-textelement.h' */


/* including file 'src/layoutelements/layoutelement-colorscale.h', size 5907 */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */


class QCPColorScaleAxisRectPrivate : public QCPAxisRect
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parentColorScale
   */
  explicit QCPColorScaleAxisRectPrivate(QCPColorScale *parentColorScale);
protected:
  QCPColorScale *mParentColorScale; /**< TODO: describe */
  QImage mGradientImage; /**< TODO: describe */
  bool mGradientImageInvalidated; /**< TODO: describe */
  // re-using some methods of QCPAxisRect to make them available to friend class QCPColorScale
  using QCPAxisRect::calculateAutoMargin;
  using QCPAxisRect::mousePressEvent;
  using QCPAxisRect::mouseMoveEvent;
  using QCPAxisRect::mouseReleaseEvent;
  using QCPAxisRect::wheelEvent;
  using QCPAxisRect::update;
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter);
  /**
   * @brief
   *
   */
  void updateGradientImage();
  /**
   * @brief
   *
   * @param selectedParts
   */
  Q_SLOT void axisSelectionChanged(QCPAxis::SelectableParts selectedParts);
  /**
   * @brief
   *
   * @param selectableParts
   */
  Q_SLOT void axisSelectableChanged(QCPAxis::SelectableParts selectableParts);
  friend class QCPColorScale;
};


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPColorScale : public QCPLayoutElement
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QCPAxis::AxisType type READ type WRITE setType)
  Q_PROPERTY(QCPRange dataRange READ dataRange WRITE setDataRange NOTIFY dataRangeChanged)
  Q_PROPERTY(QCPAxis::ScaleType dataScaleType READ dataScaleType WRITE setDataScaleType NOTIFY dataScaleTypeChanged)
  Q_PROPERTY(QCPColorGradient gradient READ gradient WRITE setGradient NOTIFY gradientChanged)
  Q_PROPERTY(QString label READ label WRITE setLabel)
  Q_PROPERTY(int barWidth READ barWidth WRITE setBarWidth)
  Q_PROPERTY(bool rangeDrag READ rangeDrag WRITE setRangeDrag)
  Q_PROPERTY(bool rangeZoom READ rangeZoom WRITE setRangeZoom)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPColorScale(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPColorScale();
  
  // getters:
  /**
   * @brief
   *
   * @return QCPAxis
   */
  QCPAxis *axis() const { return mColorAxis.data(); }
  /**
   * @brief
   *
   * @return QCPAxis::AxisType
   */
  QCPAxis::AxisType type() const { return mType; }
  /**
   * @brief
   *
   * @return QCPRange
   */
  QCPRange dataRange() const { return mDataRange; }
  /**
   * @brief
   *
   * @return QCPAxis::ScaleType
   */
  QCPAxis::ScaleType dataScaleType() const { return mDataScaleType; }
  /**
   * @brief
   *
   * @return QCPColorGradient
   */
  QCPColorGradient gradient() const { return mGradient; }
  /**
   * @brief
   *
   * @return QString
   */
  QString label() const;
  /**
   * @brief
   *
   * @return int
   */
  int barWidth () const { return mBarWidth; }
  /**
   * @brief
   *
   * @return bool
   */
  bool rangeDrag() const;
  /**
   * @brief
   *
   * @return bool
   */
  bool rangeZoom() const;
  
  // setters:
  /**
   * @brief
   *
   * @param type
   */
  void setType(QCPAxis::AxisType type);
  /**
   * @brief
   *
   * @param dataRange
   */
  Q_SLOT void setDataRange(const QCPRange &dataRange);
  /**
   * @brief
   *
   * @param scaleType
   */
  Q_SLOT void setDataScaleType(QCPAxis::ScaleType scaleType);
  /**
   * @brief
   *
   * @param gradient
   */
  Q_SLOT void setGradient(const QCPColorGradient &gradient);
  /**
   * @brief
   *
   * @param str
   */
  void setLabel(const QString &str);
  /**
   * @brief
   *
   * @param width
   */
  void setBarWidth(int width);
  /**
   * @brief
   *
   * @param enabled
   */
  void setRangeDrag(bool enabled);
  /**
   * @brief
   *
   * @param enabled
   */
  void setRangeZoom(bool enabled);
  
  // non-property methods:
  /**
   * @brief
   *
   * @return QList<QCPColorMap *>
   */
  QList<QCPColorMap*> colorMaps() const;
  /**
   * @brief
   *
   * @param onlyVisibleMaps
   */
  void rescaleDataRange(bool onlyVisibleMaps);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param phase
   */
  virtual void update(UpdatePhase phase) Q_DECL_OVERRIDE;
  
signals:
  /**
   * @brief
   *
   * @param newRange
   */
  void dataRangeChanged(const QCPRange &newRange);
  /**
   * @brief
   *
   * @param scaleType
   */
  void dataScaleTypeChanged(QCPAxis::ScaleType scaleType);
  /**
   * @brief
   *
   * @param newGradient
   */
  void gradientChanged(const QCPColorGradient &newGradient);

protected:
  // property members:
  QCPAxis::AxisType mType; /**< TODO: describe */
  QCPRange mDataRange; /**< TODO: describe */
  QCPAxis::ScaleType mDataScaleType; /**< TODO: describe */
  QCPColorGradient mGradient; /**< TODO: describe */
  int mBarWidth; /**< TODO: describe */
  
  // non-property members:
  QPointer<QCPColorScaleAxisRectPrivate> mAxisRect; /**< TODO: describe */
  QPointer<QCPAxis> mColorAxis; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void applyDefaultAntialiasingHint(QCPPainter *painter) const Q_DECL_OVERRIDE;
  // events:
  /**
   * @brief
   *
   * @param event
   * @param details
   */
  virtual void mousePressEvent(QMouseEvent *event, const QVariant &details) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   * @param startPos
   */
  virtual void mouseMoveEvent(QMouseEvent *event, const QPointF &startPos) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   * @param startPos
   */
  virtual void mouseReleaseEvent(QMouseEvent *event, const QPointF &startPos) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param event
   */
  virtual void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPColorScale)
  
  friend class QCPColorScaleAxisRectPrivate;
};


/* end of 'src/layoutelements/layoutelement-colorscale.h' */


/* including file 'src/plottables/plottable-graph.h', size 8826              */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPGraphData
{
public:
  /**
   * @brief
   *
   */
  QCPGraphData();
  /**
   * @brief
   *
   * @param key
   * @param value
   */
  QCPGraphData(double key, double value);
  
  /**
   * @brief
   *
   * @return double
   */
  inline double sortKey() const { return key; }
  /**
   * @brief
   *
   * @param sortKey
   * @return QCPGraphData
   */
  inline static QCPGraphData fromSortKey(double sortKey) { return QCPGraphData(sortKey, 0); }
  /**
   * @brief
   *
   * @return bool
   */
  inline static bool sortKeyIsMainKey() { return true; }
  
  /**
   * @brief
   *
   * @return double
   */
  inline double mainKey() const { return key; }
  /**
   * @brief
   *
   * @return double
   */
  inline double mainValue() const { return value; }
  
  /**
   * @brief
   *
   * @return QCPRange
   */
  inline QCPRange valueRange() const { return QCPRange(value, value); }
  
  double key, value; /**< TODO: describe */
};
Q_DECLARE_TYPEINFO(QCPGraphData, Q_PRIMITIVE_TYPE);


/*! \typedef QCPGraphDataContainer
  
  Container for storing \ref QCPGraphData points. The data is stored sorted by \a key.
  
  This template instantiation is the container in which QCPGraph holds its data. For details about
  the generic container, see the documentation of the class template \ref QCPDataContainer.
  
  \see QCPGraphData, QCPGraph::setData
*/
typedef QCPDataContainer<QCPGraphData> QCPGraphDataContainer;

class QCP_LIB_DECL QCPGraph : public QCPAbstractPlottable1D<QCPGraphData>
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(LineStyle lineStyle READ lineStyle WRITE setLineStyle)
  Q_PROPERTY(QCPScatterStyle scatterStyle READ scatterStyle WRITE setScatterStyle)
  Q_PROPERTY(int scatterSkip READ scatterSkip WRITE setScatterSkip)
  Q_PROPERTY(QCPGraph* channelFillGraph READ channelFillGraph WRITE setChannelFillGraph)
  Q_PROPERTY(bool adaptiveSampling READ adaptiveSampling WRITE setAdaptiveSampling)
  /// \endcond
public:
  /*!
    Defines how the graph's line is represented visually in the plot. The line is drawn with the
    current pen of the graph (\ref setPen).
    \see setLineStyle
  */
  enum LineStyle { lsNone        ///< data points are not connected with any lines (e.g. data only represented
                                 ///< with symbols according to the scatter style, see \ref setScatterStyle)
                   ,lsLine       ///< data points are connected by a straight line
                   ,lsStepLeft   ///< line is drawn as steps where the step height is the value of the left data point
                   ,lsStepRight  ///< line is drawn as steps where the step height is the value of the right data point
                   ,lsStepCenter ///< line is drawn as steps where the step is in between two data points
                   ,lsImpulse    ///< each data point is represented by a line parallel to the value axis, which reaches from the data point to the zero-value-line
                 };
  Q_ENUMS(LineStyle)
  
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  explicit QCPGraph(QCPAxis *keyAxis, QCPAxis *valueAxis);
  /**
   * @brief
   *
   */
  virtual ~QCPGraph();
  
  // getters:
  /**
   * @brief
   *
   * @return QSharedPointer<QCPGraphDataContainer>
   */
  QSharedPointer<QCPGraphDataContainer> data() const { return mDataContainer; }
  /**
   * @brief
   *
   * @return LineStyle
   */
  LineStyle lineStyle() const { return mLineStyle; }
  /**
   * @brief
   *
   * @return QCPScatterStyle
   */
  QCPScatterStyle scatterStyle() const { return mScatterStyle; }
  /**
   * @brief
   *
   * @return int
   */
  int scatterSkip() const { return mScatterSkip; }
  /**
   * @brief
   *
   * @return QCPGraph
   */
  QCPGraph *channelFillGraph() const { return mChannelFillGraph.data(); }
  /**
   * @brief
   *
   * @return bool
   */
  bool adaptiveSampling() const { return mAdaptiveSampling; }
  
  // setters:
  /**
   * @brief
   *
   * @param data
   */
  void setData(QSharedPointer<QCPGraphDataContainer> data);
  /**
   * @brief
   *
   * @param keys
   * @param values
   * @param alreadySorted
   */
  void setData(const QVector<double> &keys, const QVector<double> &values, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param ls
   */
  void setLineStyle(LineStyle ls);
  /**
   * @brief
   *
   * @param style
   */
  void setScatterStyle(const QCPScatterStyle &style);
  /**
   * @brief
   *
   * @param skip
   */
  void setScatterSkip(int skip);
  /**
   * @brief
   *
   * @param targetGraph
   */
  void setChannelFillGraph(QCPGraph *targetGraph);
  /**
   * @brief
   *
   * @param enabled
   */
  void setAdaptiveSampling(bool enabled);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param keys
   * @param values
   * @param alreadySorted
   */
  void addData(const QVector<double> &keys, const QVector<double> &values, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param key
   * @param value
   */
  void addData(double key, double value);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @return QCPRange
   */
  virtual QCPRange getKeyRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @param inKeyRange
   * @return QCPRange
   */
  virtual QCPRange getValueRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange()) const Q_DECL_OVERRIDE;
  
protected:
  // property members:
  LineStyle mLineStyle; /**< TODO: describe */
  QCPScatterStyle mScatterStyle; /**< TODO: describe */
  int mScatterSkip; /**< TODO: describe */
  QPointer<QCPGraph> mChannelFillGraph; /**< TODO: describe */
  bool mAdaptiveSampling; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   * @param rect
   */
  virtual void drawLegendIcon(QCPPainter *painter, const QRectF &rect) const Q_DECL_OVERRIDE;
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param painter
   * @param lines
   */
  virtual void drawFill(QCPPainter *painter, QVector<QPointF> *lines) const;
  /**
   * @brief
   *
   * @param painter
   * @param scatters
   * @param style
   */
  virtual void drawScatterPlot(QCPPainter *painter, const QVector<QPointF> &scatters, const QCPScatterStyle &style) const;
  /**
   * @brief
   *
   * @param painter
   * @param lines
   */
  virtual void drawLinePlot(QCPPainter *painter, const QVector<QPointF> &lines) const;
  /**
   * @brief
   *
   * @param painter
   * @param lines
   */
  virtual void drawImpulsePlot(QCPPainter *painter, const QVector<QPointF> &lines) const;
  
  /**
   * @brief
   *
   * @param lineData
   * @param begin
   * @param end
   */
  virtual void getOptimizedLineData(QVector<QCPGraphData> *lineData, const QCPGraphDataContainer::const_iterator &begin, const QCPGraphDataContainer::const_iterator &end) const;
  /**
   * @brief
   *
   * @param scatterData
   * @param begin
   * @param end
   */
  virtual void getOptimizedScatterData(QVector<QCPGraphData> *scatterData, QCPGraphDataContainer::const_iterator begin, QCPGraphDataContainer::const_iterator end) const;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param begin
   * @param end
   * @param rangeRestriction
   */
  void getVisibleDataBounds(QCPGraphDataContainer::const_iterator &begin, QCPGraphDataContainer::const_iterator &end, const QCPDataRange &rangeRestriction) const;
  /**
   * @brief
   *
   * @param lines
   * @param dataRange
   */
  void getLines(QVector<QPointF> *lines, const QCPDataRange &dataRange) const;
  /**
   * @brief
   *
   * @param scatters
   * @param dataRange
   */
  void getScatters(QVector<QPointF> *scatters, const QCPDataRange &dataRange) const;
  /**
   * @brief
   *
   * @param data
   * @return QVector<QPointF>
   */
  QVector<QPointF> dataToLines(const QVector<QCPGraphData> &data) const;
  /**
   * @brief
   *
   * @param data
   * @return QVector<QPointF>
   */
  QVector<QPointF> dataToStepLeftLines(const QVector<QCPGraphData> &data) const;
  /**
   * @brief
   *
   * @param data
   * @return QVector<QPointF>
   */
  QVector<QPointF> dataToStepRightLines(const QVector<QCPGraphData> &data) const;
  /**
   * @brief
   *
   * @param data
   * @return QVector<QPointF>
   */
  QVector<QPointF> dataToStepCenterLines(const QVector<QCPGraphData> &data) const;
  /**
   * @brief
   *
   * @param data
   * @return QVector<QPointF>
   */
  QVector<QPointF> dataToImpulseLines(const QVector<QCPGraphData> &data) const;
  /**
   * @brief
   *
   * @param lines
   */
  void addFillBasePoints(QVector<QPointF> *lines) const;
  /**
   * @brief
   *
   * @param lines
   */
  void removeFillBasePoints(QVector<QPointF> *lines) const;
  /**
   * @brief
   *
   * @param lowerKey
   * @return QPointF
   */
  QPointF lowerFillBasePoint(double lowerKey) const;
  /**
   * @brief
   *
   * @param upperKey
   * @return QPointF
   */
  QPointF upperFillBasePoint(double upperKey) const;
  /**
   * @brief
   *
   * @param lines
   * @return const QPolygonF
   */
  const QPolygonF getChannelFillPolygon(const QVector<QPointF> *lines) const;
  /**
   * @brief
   *
   * @param data
   * @param x
   * @return int
   */
  int findIndexBelowX(const QVector<QPointF> *data, double x) const;
  /**
   * @brief
   *
   * @param data
   * @param x
   * @return int
   */
  int findIndexAboveX(const QVector<QPointF> *data, double x) const;
  /**
   * @brief
   *
   * @param data
   * @param y
   * @return int
   */
  int findIndexBelowY(const QVector<QPointF> *data, double y) const;
  /**
   * @brief
   *
   * @param data
   * @param y
   * @return int
   */
  int findIndexAboveY(const QVector<QPointF> *data, double y) const;
  /**
   * @brief
   *
   * @param pixelPoint
   * @param closestData
   * @return double
   */
  double pointDistance(const QPointF &pixelPoint, QCPGraphDataContainer::const_iterator &closestData) const;
  
  friend class QCustomPlot;
  friend class QCPLegend;
};
Q_DECLARE_METATYPE(QCPGraph::LineStyle)

/* end of 'src/plottables/plottable-graph.h' */


/* including file 'src/plottables/plottable-curve.h', size 7409              */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPCurveData
{
public:
  /**
   * @brief
   *
   */
  QCPCurveData();
  /**
   * @brief
   *
   * @param t
   * @param key
   * @param value
   */
  QCPCurveData(double t, double key, double value);
  
  /**
   * @brief
   *
   * @return double
   */
  inline double sortKey() const { return t; }
  /**
   * @brief
   *
   * @param sortKey
   * @return QCPCurveData
   */
  inline static QCPCurveData fromSortKey(double sortKey) { return QCPCurveData(sortKey, 0, 0); }
  /**
   * @brief
   *
   * @return bool
   */
  inline static bool sortKeyIsMainKey() { return false; }
  
  /**
   * @brief
   *
   * @return double
   */
  inline double mainKey() const { return key; }
  /**
   * @brief
   *
   * @return double
   */
  inline double mainValue() const { return value; }
  
  /**
   * @brief
   *
   * @return QCPRange
   */
  inline QCPRange valueRange() const { return QCPRange(value, value); }
  
  double t, key, value; /**< TODO: describe */
};
Q_DECLARE_TYPEINFO(QCPCurveData, Q_PRIMITIVE_TYPE);


/*! \typedef QCPCurveDataContainer
  
  Container for storing \ref QCPCurveData points. The data is stored sorted by \a t, so the \a
  sortKey() (returning \a t) is different from \a mainKey() (returning \a key).
  
  This template instantiation is the container in which QCPCurve holds its data. For details about
  the generic container, see the documentation of the class template \ref QCPDataContainer.
  
  \see QCPCurveData, QCPCurve::setData
*/
typedef QCPDataContainer<QCPCurveData> QCPCurveDataContainer;

class QCP_LIB_DECL QCPCurve : public QCPAbstractPlottable1D<QCPCurveData>
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QCPScatterStyle scatterStyle READ scatterStyle WRITE setScatterStyle)
  Q_PROPERTY(int scatterSkip READ scatterSkip WRITE setScatterSkip)
  Q_PROPERTY(LineStyle lineStyle READ lineStyle WRITE setLineStyle)
  /// \endcond
public:
  /*!
    Defines how the curve's line is represented visually in the plot. The line is drawn with the
    current pen of the curve (\ref setPen).
    \see setLineStyle
  */
  enum LineStyle { lsNone  ///< No line is drawn between data points (e.g. only scatters)
                   ,lsLine ///< Data points are connected with a straight line
                 };
  Q_ENUMS(LineStyle)
  
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  explicit QCPCurve(QCPAxis *keyAxis, QCPAxis *valueAxis);
  /**
   * @brief
   *
   */
  virtual ~QCPCurve();
  
  // getters:
  /**
   * @brief
   *
   * @return QSharedPointer<QCPCurveDataContainer>
   */
  QSharedPointer<QCPCurveDataContainer> data() const { return mDataContainer; }
  /**
   * @brief
   *
   * @return QCPScatterStyle
   */
  QCPScatterStyle scatterStyle() const { return mScatterStyle; }
  /**
   * @brief
   *
   * @return int
   */
  int scatterSkip() const { return mScatterSkip; }
  /**
   * @brief
   *
   * @return LineStyle
   */
  LineStyle lineStyle() const { return mLineStyle; }
  
  // setters:
  /**
   * @brief
   *
   * @param data
   */
  void setData(QSharedPointer<QCPCurveDataContainer> data);
  /**
   * @brief
   *
   * @param t
   * @param keys
   * @param values
   * @param alreadySorted
   */
  void setData(const QVector<double> &t, const QVector<double> &keys, const QVector<double> &values, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param keys
   * @param values
   */
  void setData(const QVector<double> &keys, const QVector<double> &values);
  /**
   * @brief
   *
   * @param style
   */
  void setScatterStyle(const QCPScatterStyle &style);
  /**
   * @brief
   *
   * @param skip
   */
  void setScatterSkip(int skip);
  /**
   * @brief
   *
   * @param style
   */
  void setLineStyle(LineStyle style);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param t
   * @param keys
   * @param values
   * @param alreadySorted
   */
  void addData(const QVector<double> &t, const QVector<double> &keys, const QVector<double> &values, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param keys
   * @param values
   */
  void addData(const QVector<double> &keys, const QVector<double> &values);
  /**
   * @brief
   *
   * @param t
   * @param key
   * @param value
   */
  void addData(double t, double key, double value);
  /**
   * @brief
   *
   * @param key
   * @param value
   */
  void addData(double key, double value);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @return QCPRange
   */
  virtual QCPRange getKeyRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @param inKeyRange
   * @return QCPRange
   */
  virtual QCPRange getValueRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange()) const Q_DECL_OVERRIDE;
  
protected:
  // property members:
  QCPScatterStyle mScatterStyle; /**< TODO: describe */
  int mScatterSkip; /**< TODO: describe */
  LineStyle mLineStyle; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   * @param rect
   */
  virtual void drawLegendIcon(QCPPainter *painter, const QRectF &rect) const Q_DECL_OVERRIDE;
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param painter
   * @param lines
   */
  virtual void drawCurveLine(QCPPainter *painter, const QVector<QPointF> &lines) const;
  /**
   * @brief
   *
   * @param painter
   * @param points
   * @param style
   */
  virtual void drawScatterPlot(QCPPainter *painter, const QVector<QPointF> &points, const QCPScatterStyle &style) const;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param lines
   * @param dataRange
   * @param penWidth
   */
  void getCurveLines(QVector<QPointF> *lines, const QCPDataRange &dataRange, double penWidth) const;
  /**
   * @brief
   *
   * @param scatters
   * @param dataRange
   * @param scatterWidth
   */
  void getScatters(QVector<QPointF> *scatters, const QCPDataRange &dataRange, double scatterWidth) const;
  /**
   * @brief
   *
   * @param key
   * @param value
   * @param keyMin
   * @param valueMax
   * @param keyMax
   * @param valueMin
   * @return int
   */
  int getRegion(double key, double value, double keyMin, double valueMax, double keyMax, double valueMin) const;
  /**
   * @brief
   *
   * @param prevRegion
   * @param prevKey
   * @param prevValue
   * @param key
   * @param value
   * @param keyMin
   * @param valueMax
   * @param keyMax
   * @param valueMin
   * @return QPointF
   */
  QPointF getOptimizedPoint(int prevRegion, double prevKey, double prevValue, double key, double value, double keyMin, double valueMax, double keyMax, double valueMin) const;
  /**
   * @brief
   *
   * @param prevRegion
   * @param currentRegion
   * @param prevKey
   * @param prevValue
   * @param key
   * @param value
   * @param keyMin
   * @param valueMax
   * @param keyMax
   * @param valueMin
   * @return QVector<QPointF>
   */
  QVector<QPointF> getOptimizedCornerPoints(int prevRegion, int currentRegion, double prevKey, double prevValue, double key, double value, double keyMin, double valueMax, double keyMax, double valueMin) const;
  /**
   * @brief
   *
   * @param prevRegion
   * @param currentRegion
   * @return bool
   */
  bool mayTraverse(int prevRegion, int currentRegion) const;
  /**
   * @brief
   *
   * @param prevKey
   * @param prevValue
   * @param key
   * @param value
   * @param keyMin
   * @param valueMax
   * @param keyMax
   * @param valueMin
   * @param crossA
   * @param crossB
   * @return bool
   */
  bool getTraverse(double prevKey, double prevValue, double key, double value, double keyMin, double valueMax, double keyMax, double valueMin, QPointF &crossA, QPointF &crossB) const;
  /**
   * @brief
   *
   * @param prevRegion
   * @param currentRegion
   * @param keyMin
   * @param valueMax
   * @param keyMax
   * @param valueMin
   * @param beforeTraverse
   * @param afterTraverse
   */
  void getTraverseCornerPoints(int prevRegion, int currentRegion, double keyMin, double valueMax, double keyMax, double valueMin, QVector<QPointF> &beforeTraverse, QVector<QPointF> &afterTraverse) const;
  /**
   * @brief
   *
   * @param pixelPoint
   * @param closestData
   * @return double
   */
  double pointDistance(const QPointF &pixelPoint, QCPCurveDataContainer::const_iterator &closestData) const;
  
  friend class QCustomPlot;
  friend class QCPLegend;
};
Q_DECLARE_METATYPE(QCPCurve::LineStyle)

/* end of 'src/plottables/plottable-curve.h' */


/* including file 'src/plottables/plottable-bars.h', size 8924               */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPBarsGroup : public QObject
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(SpacingType spacingType READ spacingType WRITE setSpacingType)
  Q_PROPERTY(double spacing READ spacing WRITE setSpacing)
  /// \endcond
public:
  /*!
    Defines the ways the spacing between bars in the group can be specified. Thus it defines what
    the number passed to \ref setSpacing actually means.
    
    \see setSpacingType, setSpacing
  */
  enum SpacingType { stAbsolute       ///< Bar spacing is in absolute pixels
                     ,stAxisRectRatio ///< Bar spacing is given by a fraction of the axis rect size
                     ,stPlotCoords    ///< Bar spacing is in key coordinates and thus scales with the key axis range
                   };
  Q_ENUMS(SpacingType)
  
  /**
   * @brief
   *
   * @param parentPlot
   */
  QCPBarsGroup(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPBarsGroup();
  
  // getters:
  /**
   * @brief
   *
   * @return SpacingType
   */
  SpacingType spacingType() const { return mSpacingType; }
  /**
   * @brief
   *
   * @return double
   */
  double spacing() const { return mSpacing; }
  
  // setters:
  /**
   * @brief
   *
   * @param spacingType
   */
  void setSpacingType(SpacingType spacingType);
  /**
   * @brief
   *
   * @param spacing
   */
  void setSpacing(double spacing);
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QList<QCPBars *>
   */
  QList<QCPBars*> bars() const { return mBars; }
  /**
   * @brief
   *
   * @param index
   * @return QCPBars
   */
  QCPBars* bars(int index) const;
  /**
   * @brief
   *
   * @return int
   */
  int size() const { return mBars.size(); }
  /**
   * @brief
   *
   * @return bool
   */
  bool isEmpty() const { return mBars.isEmpty(); }
  /**
   * @brief
   *
   */
  void clear();
  /**
   * @brief
   *
   * @param bars
   * @return bool
   */
  bool contains(QCPBars *bars) const { return mBars.contains(bars); }
  /**
   * @brief
   *
   * @param bars
   */
  void append(QCPBars *bars);
  /**
   * @brief
   *
   * @param i
   * @param bars
   */
  void insert(int i, QCPBars *bars);
  /**
   * @brief
   *
   * @param bars
   */
  void remove(QCPBars *bars);
  
protected:
  // non-property members:
  QCustomPlot *mParentPlot; /**< TODO: describe */
  SpacingType mSpacingType; /**< TODO: describe */
  double mSpacing; /**< TODO: describe */
  QList<QCPBars*> mBars; /**< TODO: describe */
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param bars
   */
  void registerBars(QCPBars *bars);
  /**
   * @brief
   *
   * @param bars
   */
  void unregisterBars(QCPBars *bars);
  
  // virtual methods:
  /**
   * @brief
   *
   * @param bars
   * @param keyCoord
   * @return double
   */
  double keyPixelOffset(const QCPBars *bars, double keyCoord);
  /**
   * @brief
   *
   * @param bars
   * @param keyCoord
   * @return double
   */
  double getPixelSpacing(const QCPBars *bars, double keyCoord);
  
private:
  /**
   * @brief
   *
   * @param
   */
  Q_DISABLE_COPY(QCPBarsGroup)
  
  friend class QCPBars;
};
Q_DECLARE_METATYPE(QCPBarsGroup::SpacingType)


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPBarsData
{
public:
  /**
   * @brief
   *
   */
  QCPBarsData();
  /**
   * @brief
   *
   * @param key
   * @param value
   */
  QCPBarsData(double key, double value);
  
  /**
   * @brief
   *
   * @return double
   */
  inline double sortKey() const { return key; }
  /**
   * @brief
   *
   * @param sortKey
   * @return QCPBarsData
   */
  inline static QCPBarsData fromSortKey(double sortKey) { return QCPBarsData(sortKey, 0); }
  /**
   * @brief
   *
   * @return bool
   */
  inline static bool sortKeyIsMainKey() { return true; }
  
  /**
   * @brief
   *
   * @return double
   */
  inline double mainKey() const { return key; }
  /**
   * @brief
   *
   * @return double
   */
  inline double mainValue() const { return value; }
  
  /**
   * @brief
   *
   * @return QCPRange
   */
  inline QCPRange valueRange() const { return QCPRange(value, value); } // note that bar base value isn't held in each QCPBarsData and thus can't/shouldn't be returned here
  
  double key, value; /**< TODO: describe */
};
Q_DECLARE_TYPEINFO(QCPBarsData, Q_PRIMITIVE_TYPE);


/*! \typedef QCPBarsDataContainer
  
  Container for storing \ref QCPBarsData points. The data is stored sorted by \a key.
  
  This template instantiation is the container in which QCPBars holds its data. For details about
  the generic container, see the documentation of the class template \ref QCPDataContainer.
  
  \see QCPBarsData, QCPBars::setData
*/
typedef QCPDataContainer<QCPBarsData> QCPBarsDataContainer;

class QCP_LIB_DECL QCPBars : public QCPAbstractPlottable1D<QCPBarsData>
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(double width READ width WRITE setWidth)
  Q_PROPERTY(WidthType widthType READ widthType WRITE setWidthType)
  Q_PROPERTY(QCPBarsGroup* barsGroup READ barsGroup WRITE setBarsGroup)
  Q_PROPERTY(double baseValue READ baseValue WRITE setBaseValue)
  Q_PROPERTY(double stackingGap READ stackingGap WRITE setStackingGap)
  Q_PROPERTY(QCPBars* barBelow READ barBelow)
  Q_PROPERTY(QCPBars* barAbove READ barAbove)
  /// \endcond
public:
  /*!
    Defines the ways the width of the bar can be specified. Thus it defines what the number passed
    to \ref setWidth actually means.
    
    \see setWidthType, setWidth
  */
  enum WidthType { wtAbsolute       ///< Bar width is in absolute pixels
                   ,wtAxisRectRatio ///< Bar width is given by a fraction of the axis rect size
                   ,wtPlotCoords    ///< Bar width is in key coordinates and thus scales with the key axis range
                 };
  Q_ENUMS(WidthType)
  
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  explicit QCPBars(QCPAxis *keyAxis, QCPAxis *valueAxis);
  /**
   * @brief
   *
   */
  virtual ~QCPBars();
  
  // getters:
  /**
   * @brief
   *
   * @return double
   */
  double width() const { return mWidth; }
  /**
   * @brief
   *
   * @return WidthType
   */
  WidthType widthType() const { return mWidthType; }
  /**
   * @brief
   *
   * @return QCPBarsGroup
   */
  QCPBarsGroup *barsGroup() const { return mBarsGroup; }
  /**
   * @brief
   *
   * @return double
   */
  double baseValue() const { return mBaseValue; }
  /**
   * @brief
   *
   * @return double
   */
  double stackingGap() const { return mStackingGap; }
  /**
   * @brief
   *
   * @return QCPBars
   */
  QCPBars *barBelow() const { return mBarBelow.data(); }
  /**
   * @brief
   *
   * @return QCPBars
   */
  QCPBars *barAbove() const { return mBarAbove.data(); }
  /**
   * @brief
   *
   * @return QSharedPointer<QCPBarsDataContainer>
   */
  QSharedPointer<QCPBarsDataContainer> data() const { return mDataContainer; }
  
  // setters:
  /**
   * @brief
   *
   * @param data
   */
  void setData(QSharedPointer<QCPBarsDataContainer> data);
  /**
   * @brief
   *
   * @param keys
   * @param values
   * @param alreadySorted
   */
  void setData(const QVector<double> &keys, const QVector<double> &values, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param width
   */
  void setWidth(double width);
  /**
   * @brief
   *
   * @param widthType
   */
  void setWidthType(WidthType widthType);
  /**
   * @brief
   *
   * @param barsGroup
   */
  void setBarsGroup(QCPBarsGroup *barsGroup);
  /**
   * @brief
   *
   * @param baseValue
   */
  void setBaseValue(double baseValue);
  /**
   * @brief
   *
   * @param pixels
   */
  void setStackingGap(double pixels);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param keys
   * @param values
   * @param alreadySorted
   */
  void addData(const QVector<double> &keys, const QVector<double> &values, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param key
   * @param value
   */
  void addData(double key, double value);
  /**
   * @brief
   *
   * @param bars
   */
  void moveBelow(QCPBars *bars);
  /**
   * @brief
   *
   * @param bars
   */
  void moveAbove(QCPBars *bars);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param rect
   * @param onlySelectable
   * @return QCPDataSelection
   */
  virtual QCPDataSelection selectTestRect(const QRectF &rect, bool onlySelectable) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @return QCPRange
   */
  virtual QCPRange getKeyRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @param inKeyRange
   * @return QCPRange
   */
  virtual QCPRange getValueRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange()) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param index
   * @return QPointF
   */
  virtual QPointF dataPixelPosition(int index) const Q_DECL_OVERRIDE;
  
protected:
  // property members:
  double mWidth; /**< TODO: describe */
  WidthType mWidthType; /**< TODO: describe */
  QCPBarsGroup *mBarsGroup; /**< TODO: describe */
  double mBaseValue; /**< TODO: describe */
  double mStackingGap; /**< TODO: describe */
  QPointer<QCPBars> mBarBelow, mBarAbove; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   * @param rect
   */
  virtual void drawLegendIcon(QCPPainter *painter, const QRectF &rect) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param begin
   * @param end
   */
  void getVisibleDataBounds(QCPBarsDataContainer::const_iterator &begin, QCPBarsDataContainer::const_iterator &end) const;
  /**
   * @brief
   *
   * @param key
   * @param value
   * @return QRectF
   */
  QRectF getBarRect(double key, double value) const;
  /**
   * @brief
   *
   * @param key
   * @param lower
   * @param upper
   */
  void getPixelWidth(double key, double &lower, double &upper) const;
  /**
   * @brief
   *
   * @param key
   * @param positive
   * @return double
   */
  double getStackedBaseValue(double key, bool positive) const;
  /**
   * @brief
   *
   * @param lower
   * @param upper
   */
  static void connectBars(QCPBars* lower, QCPBars* upper);
  
  friend class QCustomPlot;
  friend class QCPLegend;
  friend class QCPBarsGroup;
};
Q_DECLARE_METATYPE(QCPBars::WidthType)

/* end of 'src/plottables/plottable-bars.h' */


/* including file 'src/plottables/plottable-statisticalbox.h', size 7516     */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPStatisticalBoxData
{
public:
  /**
   * @brief
   *
   */
  QCPStatisticalBoxData();
  /**
   * @brief
   *
   * @param key
   * @param minimum
   * @param lowerQuartile
   * @param median
   * @param upperQuartile
   * @param maximum
   * @param outliers
   */
  QCPStatisticalBoxData(double key, double minimum, double lowerQuartile, double median, double upperQuartile, double maximum, const QVector<double>& outliers=QVector<double>());
  
  /**
   * @brief
   *
   * @return double
   */
  inline double sortKey() const { return key; }
  /**
   * @brief
   *
   * @param sortKey
   * @return QCPStatisticalBoxData
   */
  inline static QCPStatisticalBoxData fromSortKey(double sortKey) { return QCPStatisticalBoxData(sortKey, 0, 0, 0, 0, 0); }
  /**
   * @brief
   *
   * @return bool
   */
  inline static bool sortKeyIsMainKey() { return true; }
  
  /**
   * @brief
   *
   * @return double
   */
  inline double mainKey() const { return key; }
  /**
   * @brief
   *
   * @return double
   */
  inline double mainValue() const { return median; }
  
  /**
   * @brief
   *
   * @return QCPRange
   */
  inline QCPRange valueRange() const
  {
    QCPRange result(minimum, maximum);
    for (QVector<double>::const_iterator it = outliers.constBegin(); it != outliers.constEnd(); ++it)
      result.expand(*it);
    return result;
  }
  
  double key, minimum, lowerQuartile, median, upperQuartile, maximum; /**< TODO: describe */
  QVector<double> outliers; /**< TODO: describe */
};
Q_DECLARE_TYPEINFO(QCPStatisticalBoxData, Q_MOVABLE_TYPE);


/*! \typedef QCPStatisticalBoxDataContainer
  
  Container for storing \ref QCPStatisticalBoxData points. The data is stored sorted by \a key.
  
  This template instantiation is the container in which QCPStatisticalBox holds its data. For
  details about the generic container, see the documentation of the class template \ref
  QCPDataContainer.
  
  \see QCPStatisticalBoxData, QCPStatisticalBox::setData
*/
typedef QCPDataContainer<QCPStatisticalBoxData> QCPStatisticalBoxDataContainer;

class QCP_LIB_DECL QCPStatisticalBox : public QCPAbstractPlottable1D<QCPStatisticalBoxData>
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(double width READ width WRITE setWidth)
  Q_PROPERTY(double whiskerWidth READ whiskerWidth WRITE setWhiskerWidth)
  Q_PROPERTY(QPen whiskerPen READ whiskerPen WRITE setWhiskerPen)
  Q_PROPERTY(QPen whiskerBarPen READ whiskerBarPen WRITE setWhiskerBarPen)
  Q_PROPERTY(bool whiskerAntialiased READ whiskerAntialiased WRITE setWhiskerAntialiased)
  Q_PROPERTY(QPen medianPen READ medianPen WRITE setMedianPen)
  Q_PROPERTY(QCPScatterStyle outlierStyle READ outlierStyle WRITE setOutlierStyle)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  explicit QCPStatisticalBox(QCPAxis *keyAxis, QCPAxis *valueAxis);
  
  // getters:
  /**
   * @brief
   *
   * @return QSharedPointer<QCPStatisticalBoxDataContainer>
   */
  QSharedPointer<QCPStatisticalBoxDataContainer> data() const { return mDataContainer; }
  /**
   * @brief
   *
   * @return double
   */
  double width() const { return mWidth; }
  /**
   * @brief
   *
   * @return double
   */
  double whiskerWidth() const { return mWhiskerWidth; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen whiskerPen() const { return mWhiskerPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen whiskerBarPen() const { return mWhiskerBarPen; }
  /**
   * @brief
   *
   * @return bool
   */
  bool whiskerAntialiased() const { return mWhiskerAntialiased; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen medianPen() const { return mMedianPen; }
  /**
   * @brief
   *
   * @return QCPScatterStyle
   */
  QCPScatterStyle outlierStyle() const { return mOutlierStyle; }

  // setters:
  /**
   * @brief
   *
   * @param data
   */
  void setData(QSharedPointer<QCPStatisticalBoxDataContainer> data);
  /**
   * @brief
   *
   * @param keys
   * @param minimum
   * @param lowerQuartile
   * @param median
   * @param upperQuartile
   * @param maximum
   * @param alreadySorted
   */
  void setData(const QVector<double> &keys, const QVector<double> &minimum, const QVector<double> &lowerQuartile, const QVector<double> &median, const QVector<double> &upperQuartile, const QVector<double> &maximum, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param width
   */
  void setWidth(double width);
  /**
   * @brief
   *
   * @param width
   */
  void setWhiskerWidth(double width);
  /**
   * @brief
   *
   * @param pen
   */
  void setWhiskerPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setWhiskerBarPen(const QPen &pen);
  /**
   * @brief
   *
   * @param enabled
   */
  void setWhiskerAntialiased(bool enabled);
  /**
   * @brief
   *
   * @param pen
   */
  void setMedianPen(const QPen &pen);
  /**
   * @brief
   *
   * @param style
   */
  void setOutlierStyle(const QCPScatterStyle &style);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param keys
   * @param minimum
   * @param lowerQuartile
   * @param median
   * @param upperQuartile
   * @param maximum
   * @param alreadySorted
   */
  void addData(const QVector<double> &keys, const QVector<double> &minimum, const QVector<double> &lowerQuartile, const QVector<double> &median, const QVector<double> &upperQuartile, const QVector<double> &maximum, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param key
   * @param minimum
   * @param lowerQuartile
   * @param median
   * @param upperQuartile
   * @param maximum
   * @param outliers
   */
  void addData(double key, double minimum, double lowerQuartile, double median, double upperQuartile, double maximum, const QVector<double> &outliers=QVector<double>());
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param rect
   * @param onlySelectable
   * @return QCPDataSelection
   */
  virtual QCPDataSelection selectTestRect(const QRectF &rect, bool onlySelectable) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @return QCPRange
   */
  virtual QCPRange getKeyRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @param inKeyRange
   * @return QCPRange
   */
  virtual QCPRange getValueRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange()) const Q_DECL_OVERRIDE;
  
protected:
  // property members:
  double mWidth; /**< TODO: describe */
  double mWhiskerWidth; /**< TODO: describe */
  QPen mWhiskerPen, mWhiskerBarPen; /**< TODO: describe */
  bool mWhiskerAntialiased; /**< TODO: describe */
  QPen mMedianPen; /**< TODO: describe */
  QCPScatterStyle mOutlierStyle; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   * @param rect
   */
  virtual void drawLegendIcon(QCPPainter *painter, const QRectF &rect) const Q_DECL_OVERRIDE;
  
  // introduced virtual methods:
  /**
   * @brief
   *
   * @param painter
   * @param it
   * @param outlierStyle
   */
  virtual void drawStatisticalBox(QCPPainter *painter, QCPStatisticalBoxDataContainer::const_iterator it, const QCPScatterStyle &outlierStyle) const;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param begin
   * @param end
   */
  void getVisibleDataBounds(QCPStatisticalBoxDataContainer::const_iterator &begin, QCPStatisticalBoxDataContainer::const_iterator &end) const;
  /**
   * @brief
   *
   * @param it
   * @return QRectF
   */
  QRectF getQuartileBox(QCPStatisticalBoxDataContainer::const_iterator it) const;
  /**
   * @brief
   *
   * @param it
   * @return QVector<QLineF>
   */
  QVector<QLineF> getWhiskerBackboneLines(QCPStatisticalBoxDataContainer::const_iterator it) const;
  /**
   * @brief
   *
   * @param it
   * @return QVector<QLineF>
   */
  QVector<QLineF> getWhiskerBarLines(QCPStatisticalBoxDataContainer::const_iterator it) const;
  
  friend class QCustomPlot;
  friend class QCPLegend;
};

/* end of 'src/plottables/plottable-statisticalbox.h' */


/* including file 'src/plottables/plottable-colormap.h', size 7070           */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPColorMapData
{
public:
  /**
   * @brief
   *
   * @param keySize
   * @param valueSize
   * @param keyRange
   * @param valueRange
   */
  QCPColorMapData(int keySize, int valueSize, const QCPRange &keyRange, const QCPRange &valueRange);
  /**
   * @brief
   *
   */
  ~QCPColorMapData();
  /**
   * @brief
   *
   * @param other
   */
  QCPColorMapData(const QCPColorMapData &other);
  /**
   * @brief
   *
   * @param other
   * @return QCPColorMapData &operator
   */
  QCPColorMapData &operator=(const QCPColorMapData &other);
  
  // getters:
  /**
   * @brief
   *
   * @return int
   */
  int keySize() const { return mKeySize; }
  /**
   * @brief
   *
   * @return int
   */
  int valueSize() const { return mValueSize; }
  /**
   * @brief
   *
   * @return QCPRange
   */
  QCPRange keyRange() const { return mKeyRange; }
  /**
   * @brief
   *
   * @return QCPRange
   */
  QCPRange valueRange() const { return mValueRange; }
  /**
   * @brief
   *
   * @return QCPRange
   */
  QCPRange dataBounds() const { return mDataBounds; }
  /**
   * @brief
   *
   * @param key
   * @param value
   * @return double
   */
  double data(double key, double value);
  /**
   * @brief
   *
   * @param keyIndex
   * @param valueIndex
   * @return double
   */
  double cell(int keyIndex, int valueIndex);
  /**
   * @brief
   *
   * @param keyIndex
   * @param valueIndex
   * @return unsigned char
   */
  unsigned char alpha(int keyIndex, int valueIndex);
  
  // setters:
  /**
   * @brief
   *
   * @param keySize
   * @param valueSize
   */
  void setSize(int keySize, int valueSize);
  /**
   * @brief
   *
   * @param keySize
   */
  void setKeySize(int keySize);
  /**
   * @brief
   *
   * @param valueSize
   */
  void setValueSize(int valueSize);
  /**
   * @brief
   *
   * @param keyRange
   * @param valueRange
   */
  void setRange(const QCPRange &keyRange, const QCPRange &valueRange);
  /**
   * @brief
   *
   * @param keyRange
   */
  void setKeyRange(const QCPRange &keyRange);
  /**
   * @brief
   *
   * @param valueRange
   */
  void setValueRange(const QCPRange &valueRange);
  /**
   * @brief
   *
   * @param key
   * @param value
   * @param z
   */
  void setData(double key, double value, double z);
  /**
   * @brief
   *
   * @param keyIndex
   * @param valueIndex
   * @param z
   */
  void setCell(int keyIndex, int valueIndex, double z);
  /**
   * @brief
   *
   * @param keyIndex
   * @param valueIndex
   * @param alpha
   */
  void setAlpha(int keyIndex, int valueIndex, unsigned char alpha);
  
  // non-property methods:
  /**
   * @brief
   *
   */
  void recalculateDataBounds();
  /**
   * @brief
   *
   */
  void clear();
  /**
   * @brief
   *
   */
  void clearAlpha();
  /**
   * @brief
   *
   * @param z
   */
  void fill(double z);
  /**
   * @brief
   *
   * @param alpha
   */
  void fillAlpha(unsigned char alpha);
  /**
   * @brief
   *
   * @return bool
   */
  bool isEmpty() const { return mIsEmpty; }
  /**
   * @brief
   *
   * @param key
   * @param value
   * @param keyIndex
   * @param valueIndex
   */
  void coordToCell(double key, double value, int *keyIndex, int *valueIndex) const;
  /**
   * @brief
   *
   * @param keyIndex
   * @param valueIndex
   * @param key
   * @param value
   */
  void cellToCoord(int keyIndex, int valueIndex, double *key, double *value) const;
  
protected:
  // property members:
  int mKeySize, mValueSize; /**< TODO: describe */
  QCPRange mKeyRange, mValueRange; /**< TODO: describe */
  bool mIsEmpty; /**< TODO: describe */
  
  // non-property members:
  double *mData; /**< TODO: describe */
  unsigned char *mAlpha; /**< TODO: describe */
  QCPRange mDataBounds; /**< TODO: describe */
  bool mDataModified; /**< TODO: describe */
  
  /**
   * @brief
   *
   * @param initializeOpaque
   * @return bool
   */
  bool createAlpha(bool initializeOpaque=true);
  
  friend class QCPColorMap;
};


/**
 * @brief
 *
 */
class QCP_LIB_DECL QCPColorMap : public QCPAbstractPlottable
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QCPRange dataRange READ dataRange WRITE setDataRange NOTIFY dataRangeChanged)
  Q_PROPERTY(QCPAxis::ScaleType dataScaleType READ dataScaleType WRITE setDataScaleType NOTIFY dataScaleTypeChanged)
  Q_PROPERTY(QCPColorGradient gradient READ gradient WRITE setGradient NOTIFY gradientChanged)
  Q_PROPERTY(bool interpolate READ interpolate WRITE setInterpolate)
  Q_PROPERTY(bool tightBoundary READ tightBoundary WRITE setTightBoundary)
  Q_PROPERTY(QCPColorScale* colorScale READ colorScale WRITE setColorScale)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  explicit QCPColorMap(QCPAxis *keyAxis, QCPAxis *valueAxis);
  /**
   * @brief
   *
   */
  virtual ~QCPColorMap();
  
  // getters:
  /**
   * @brief
   *
   * @return QCPColorMapData
   */
  QCPColorMapData *data() const { return mMapData; }
  /**
   * @brief
   *
   * @return QCPRange
   */
  QCPRange dataRange() const { return mDataRange; }
  /**
   * @brief
   *
   * @return QCPAxis::ScaleType
   */
  QCPAxis::ScaleType dataScaleType() const { return mDataScaleType; }
  /**
   * @brief
   *
   * @return bool
   */
  bool interpolate() const { return mInterpolate; }
  /**
   * @brief
   *
   * @return bool
   */
  bool tightBoundary() const { return mTightBoundary; }
  /**
   * @brief
   *
   * @return QCPColorGradient
   */
  QCPColorGradient gradient() const { return mGradient; }
  /**
   * @brief
   *
   * @return QCPColorScale
   */
  QCPColorScale *colorScale() const { return mColorScale.data(); }
  
  // setters:
  /**
   * @brief
   *
   * @param data
   * @param copy
   */
  void setData(QCPColorMapData *data, bool copy=false);
  /**
   * @brief
   *
   * @param dataRange
   */
  Q_SLOT void setDataRange(const QCPRange &dataRange);
  /**
   * @brief
   *
   * @param scaleType
   */
  Q_SLOT void setDataScaleType(QCPAxis::ScaleType scaleType);
  /**
   * @brief
   *
   * @param gradient
   */
  Q_SLOT void setGradient(const QCPColorGradient &gradient);
  /**
   * @brief
   *
   * @param enabled
   */
  void setInterpolate(bool enabled);
  /**
   * @brief
   *
   * @param enabled
   */
  void setTightBoundary(bool enabled);
  /**
   * @brief
   *
   * @param colorScale
   */
  void setColorScale(QCPColorScale *colorScale);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param recalculateDataBounds
   */
  void rescaleDataRange(bool recalculateDataBounds=false);
  /**
   * @brief
   *
   * @param transformMode
   * @param thumbSize
   */
  Q_SLOT void updateLegendIcon(Qt::TransformationMode transformMode=Qt::SmoothTransformation, const QSize &thumbSize=QSize(32, 18));
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @return QCPRange
   */
  virtual QCPRange getKeyRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @param inKeyRange
   * @return QCPRange
   */
  virtual QCPRange getValueRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange()) const Q_DECL_OVERRIDE;
  
signals:
  /**
   * @brief
   *
   * @param newRange
   */
  void dataRangeChanged(const QCPRange &newRange);
  /**
   * @brief
   *
   * @param scaleType
   */
  void dataScaleTypeChanged(QCPAxis::ScaleType scaleType);
  /**
   * @brief
   *
   * @param newGradient
   */
  void gradientChanged(const QCPColorGradient &newGradient);
  
protected:
  // property members:
  QCPRange mDataRange; /**< TODO: describe */
  QCPAxis::ScaleType mDataScaleType; /**< TODO: describe */
  QCPColorMapData *mMapData; /**< TODO: describe */
  QCPColorGradient mGradient; /**< TODO: describe */
  bool mInterpolate; /**< TODO: describe */
  bool mTightBoundary; /**< TODO: describe */
  QPointer<QCPColorScale> mColorScale; /**< TODO: describe */
  
  // non-property members:
  QImage mMapImage, mUndersampledMapImage; /**< TODO: describe */
  QPixmap mLegendIcon; /**< TODO: describe */
  bool mMapImageInvalidated; /**< TODO: describe */
  
  // introduced virtual methods:
  /**
   * @brief
   *
   */
  virtual void updateMapImage();
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   * @param rect
   */
  virtual void drawLegendIcon(QCPPainter *painter, const QRectF &rect) const Q_DECL_OVERRIDE;
  
  friend class QCustomPlot;
  friend class QCPLegend;
};

/* end of 'src/plottables/plottable-colormap.h' */


/* including file 'src/plottables/plottable-financial.h', size 8622          */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPFinancialData
{
public:
  /**
   * @brief
   *
   */
  QCPFinancialData();
  /**
   * @brief
   *
   * @param key
   * @param open
   * @param high
   * @param low
   * @param close
   */
  QCPFinancialData(double key, double open, double high, double low, double close);
  
  /**
   * @brief
   *
   * @return double
   */
  inline double sortKey() const { return key; }
  /**
   * @brief
   *
   * @param sortKey
   * @return QCPFinancialData
   */
  inline static QCPFinancialData fromSortKey(double sortKey) { return QCPFinancialData(sortKey, 0, 0, 0, 0); }
  /**
   * @brief
   *
   * @return bool
   */
  inline static bool sortKeyIsMainKey() { return true; }
  
  /**
   * @brief
   *
   * @return double
   */
  inline double mainKey() const { return key; }
  /**
   * @brief
   *
   * @return double
   */
  inline double mainValue() const { return open; }
  
  /**
   * @brief
   *
   * @return QCPRange
   */
  inline QCPRange valueRange() const { return QCPRange(low, high); } // open and close must lie between low and high, so we don't need to check them
  
  double key, open, high, low, close; /**< TODO: describe */
};
Q_DECLARE_TYPEINFO(QCPFinancialData, Q_PRIMITIVE_TYPE);


/*! \typedef QCPFinancialDataContainer
  
  Container for storing \ref QCPFinancialData points. The data is stored sorted by \a key.
  
  This template instantiation is the container in which QCPFinancial holds its data. For details
  about the generic container, see the documentation of the class template \ref QCPDataContainer.
  
  \see QCPFinancialData, QCPFinancial::setData
*/
typedef QCPDataContainer<QCPFinancialData> QCPFinancialDataContainer;

class QCP_LIB_DECL QCPFinancial : public QCPAbstractPlottable1D<QCPFinancialData>
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(ChartStyle chartStyle READ chartStyle WRITE setChartStyle)
  Q_PROPERTY(double width READ width WRITE setWidth)
  Q_PROPERTY(WidthType widthType READ widthType WRITE setWidthType)
  Q_PROPERTY(bool twoColored READ twoColored WRITE setTwoColored)
  Q_PROPERTY(QBrush brushPositive READ brushPositive WRITE setBrushPositive)
  Q_PROPERTY(QBrush brushNegative READ brushNegative WRITE setBrushNegative)
  Q_PROPERTY(QPen penPositive READ penPositive WRITE setPenPositive)
  Q_PROPERTY(QPen penNegative READ penNegative WRITE setPenNegative)
  /// \endcond
public:
  /*!
    Defines the ways the width of the financial bar can be specified. Thus it defines what the
    number passed to \ref setWidth actually means.

    \see setWidthType, setWidth
  */
  enum WidthType { wtAbsolute       ///< width is in absolute pixels
                   ,wtAxisRectRatio ///< width is given by a fraction of the axis rect size
                   ,wtPlotCoords    ///< width is in key coordinates and thus scales with the key axis range
                 };
  Q_ENUMS(WidthType)
  
  /*!
    Defines the possible representations of OHLC data in the plot.
    
    \see setChartStyle
  */
  enum ChartStyle { csOhlc         ///< Open-High-Low-Close bar representation
                   ,csCandlestick  ///< Candlestick representation
                  };
  Q_ENUMS(ChartStyle)
  
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  explicit QCPFinancial(QCPAxis *keyAxis, QCPAxis *valueAxis);
  /**
   * @brief
   *
   */
  virtual ~QCPFinancial();
  
  // getters:
  /**
   * @brief
   *
   * @return QSharedPointer<QCPFinancialDataContainer>
   */
  QSharedPointer<QCPFinancialDataContainer> data() const { return mDataContainer; }
  /**
   * @brief
   *
   * @return ChartStyle
   */
  ChartStyle chartStyle() const { return mChartStyle; }
  /**
   * @brief
   *
   * @return double
   */
  double width() const { return mWidth; }
  /**
   * @brief
   *
   * @return WidthType
   */
  WidthType widthType() const { return mWidthType; }
  /**
   * @brief
   *
   * @return bool
   */
  bool twoColored() const { return mTwoColored; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brushPositive() const { return mBrushPositive; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brushNegative() const { return mBrushNegative; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen penPositive() const { return mPenPositive; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen penNegative() const { return mPenNegative; }
  
  // setters:
  /**
   * @brief
   *
   * @param data
   */
  void setData(QSharedPointer<QCPFinancialDataContainer> data);
  /**
   * @brief
   *
   * @param keys
   * @param open
   * @param high
   * @param low
   * @param close
   * @param alreadySorted
   */
  void setData(const QVector<double> &keys, const QVector<double> &open, const QVector<double> &high, const QVector<double> &low, const QVector<double> &close, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param style
   */
  void setChartStyle(ChartStyle style);
  /**
   * @brief
   *
   * @param width
   */
  void setWidth(double width);
  /**
   * @brief
   *
   * @param widthType
   */
  void setWidthType(WidthType widthType);
  /**
   * @brief
   *
   * @param twoColored
   */
  void setTwoColored(bool twoColored);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrushPositive(const QBrush &brush);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrushNegative(const QBrush &brush);
  /**
   * @brief
   *
   * @param pen
   */
  void setPenPositive(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setPenNegative(const QPen &pen);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param keys
   * @param open
   * @param high
   * @param low
   * @param close
   * @param alreadySorted
   */
  void addData(const QVector<double> &keys, const QVector<double> &open, const QVector<double> &high, const QVector<double> &low, const QVector<double> &close, bool alreadySorted=false);
  /**
   * @brief
   *
   * @param key
   * @param open
   * @param high
   * @param low
   * @param close
   */
  void addData(double key, double open, double high, double low, double close);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param rect
   * @param onlySelectable
   * @return QCPDataSelection
   */
  virtual QCPDataSelection selectTestRect(const QRectF &rect, bool onlySelectable) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @return QCPRange
   */
  virtual QCPRange getKeyRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @param inKeyRange
   * @return QCPRange
   */
  virtual QCPRange getValueRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange()) const Q_DECL_OVERRIDE;
  
  // static methods:
  /**
   * @brief
   *
   * @param time
   * @param value
   * @param timeBinSize
   * @param timeBinOffset
   * @return QCPFinancialDataContainer
   */
  static QCPFinancialDataContainer timeSeriesToOhlc(const QVector<double> &time, const QVector<double> &value, double timeBinSize, double timeBinOffset = 0);
  
protected:
  // property members:
  ChartStyle mChartStyle; /**< TODO: describe */
  double mWidth; /**< TODO: describe */
  WidthType mWidthType; /**< TODO: describe */
  bool mTwoColored; /**< TODO: describe */
  QBrush mBrushPositive, mBrushNegative; /**< TODO: describe */
  QPen mPenPositive, mPenNegative; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   * @param rect
   */
  virtual void drawLegendIcon(QCPPainter *painter, const QRectF &rect) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param painter
   * @param begin
   * @param end
   * @param isSelected
   */
  void drawOhlcPlot(QCPPainter *painter, const QCPFinancialDataContainer::const_iterator &begin, const QCPFinancialDataContainer::const_iterator &end, bool isSelected);
  /**
   * @brief
   *
   * @param painter
   * @param begin
   * @param end
   * @param isSelected
   */
  void drawCandlestickPlot(QCPPainter *painter, const QCPFinancialDataContainer::const_iterator &begin, const QCPFinancialDataContainer::const_iterator &end, bool isSelected);
  /**
   * @brief
   *
   * @param key
   * @param keyPixel
   * @return double
   */
  double getPixelWidth(double key, double keyPixel) const;
  /**
   * @brief
   *
   * @param pos
   * @param begin
   * @param end
   * @param closestDataPoint
   * @return double
   */
  double ohlcSelectTest(const QPointF &pos, const QCPFinancialDataContainer::const_iterator &begin, const QCPFinancialDataContainer::const_iterator &end, QCPFinancialDataContainer::const_iterator &closestDataPoint) const;
  /**
   * @brief
   *
   * @param pos
   * @param begin
   * @param end
   * @param closestDataPoint
   * @return double
   */
  double candlestickSelectTest(const QPointF &pos, const QCPFinancialDataContainer::const_iterator &begin, const QCPFinancialDataContainer::const_iterator &end, QCPFinancialDataContainer::const_iterator &closestDataPoint) const;
  /**
   * @brief
   *
   * @param begin
   * @param end
   */
  void getVisibleDataBounds(QCPFinancialDataContainer::const_iterator &begin, QCPFinancialDataContainer::const_iterator &end) const;
  /**
   * @brief
   *
   * @param it
   * @return QRectF
   */
  QRectF selectionHitBox(QCPFinancialDataContainer::const_iterator it) const;
  
  friend class QCustomPlot;
  friend class QCPLegend;
};
Q_DECLARE_METATYPE(QCPFinancial::ChartStyle)

/* end of 'src/plottables/plottable-financial.h' */


/* including file 'src/plottables/plottable-errorbar.h', size 7567           */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPErrorBarsData
{
public:
  /**
   * @brief
   *
   */
  QCPErrorBarsData();
  /**
   * @brief
   *
   * @param error
   */
  explicit QCPErrorBarsData(double error);
  /**
   * @brief
   *
   * @param errorMinus
   * @param errorPlus
   */
  QCPErrorBarsData(double errorMinus, double errorPlus);
  
  double errorMinus, errorPlus; /**< TODO: describe */
};
Q_DECLARE_TYPEINFO(QCPErrorBarsData, Q_PRIMITIVE_TYPE);


/*! \typedef QCPErrorBarsDataContainer

  Container for storing \ref QCPErrorBarsData points. It is a typedef for <tt>QVector<\ref
  QCPErrorBarsData></tt>.

  This is the container in which \ref QCPErrorBars holds its data. Unlike most other data
  containers for plottables, it is not based on \ref QCPDataContainer. This is because the error
  bars plottable is special in that it doesn't store its own key and value coordinate per error
  bar. It adopts the key and value from the plottable to which the error bars shall be applied
  (\ref QCPErrorBars::setDataPlottable). So the stored \ref QCPErrorBarsData doesn't need a
  sortable key, but merely an index (as \c QVector provides), which maps one-to-one to the indices
  of the other plottable's data.

  \see QCPErrorBarsData, QCPErrorBars::setData
*/
typedef QVector<QCPErrorBarsData> QCPErrorBarsDataContainer;

class QCP_LIB_DECL QCPErrorBars : public QCPAbstractPlottable, public QCPPlottableInterface1D
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QSharedPointer<QCPErrorBarsDataContainer> data READ data WRITE setData)
  Q_PROPERTY(QCPAbstractPlottable* dataPlottable READ dataPlottable WRITE setDataPlottable)
  Q_PROPERTY(ErrorType errorType READ errorType WRITE setErrorType)
  Q_PROPERTY(double whiskerWidth READ whiskerWidth WRITE setWhiskerWidth)
  Q_PROPERTY(double symbolGap READ symbolGap WRITE setSymbolGap)
  /// \endcond
public:
  
  /*!
    Defines in which orientation the error bars shall appear. If your data needs both error
    dimensions, create two \ref QCPErrorBars with different \ref ErrorType.

    \see setErrorType
  */
  enum ErrorType { etKeyError    ///< The errors are for the key dimension (bars appear parallel to the key axis)
                   ,etValueError ///< The errors are for the value dimension (bars appear parallel to the value axis)
  };
  Q_ENUMS(ErrorType)
  
  /**
   * @brief
   *
   * @param keyAxis
   * @param valueAxis
   */
  explicit QCPErrorBars(QCPAxis *keyAxis, QCPAxis *valueAxis);
  /**
   * @brief
   *
   */
  virtual ~QCPErrorBars();
  // getters:
  /**
   * @brief
   *
   * @return QSharedPointer<QCPErrorBarsDataContainer>
   */
  QSharedPointer<QCPErrorBarsDataContainer> data() const { return mDataContainer; }
  /**
   * @brief
   *
   * @return QCPAbstractPlottable
   */
  QCPAbstractPlottable *dataPlottable() const { return mDataPlottable.data(); }
  /**
   * @brief
   *
   * @return ErrorType
   */
  ErrorType errorType() const { return mErrorType; }
  /**
   * @brief
   *
   * @return double
   */
  double whiskerWidth() const { return mWhiskerWidth; }
  /**
   * @brief
   *
   * @return double
   */
  double symbolGap() const { return mSymbolGap; }
  
  // setters:
  /**
   * @brief
   *
   * @param data
   */
  void setData(QSharedPointer<QCPErrorBarsDataContainer> data);
  /**
   * @brief
   *
   * @param error
   */
  void setData(const QVector<double> &error);
  /**
   * @brief
   *
   * @param errorMinus
   * @param errorPlus
   */
  void setData(const QVector<double> &errorMinus, const QVector<double> &errorPlus);
  /**
   * @brief
   *
   * @param plottable
   */
  void setDataPlottable(QCPAbstractPlottable* plottable);
  /**
   * @brief
   *
   * @param type
   */
  void setErrorType(ErrorType type);
  /**
   * @brief
   *
   * @param pixels
   */
  void setWhiskerWidth(double pixels);
  /**
   * @brief
   *
   * @param pixels
   */
  void setSymbolGap(double pixels);
  
  // non-property methods:
  /**
   * @brief
   *
   * @param error
   */
  void addData(const QVector<double> &error);
  /**
   * @brief
   *
   * @param errorMinus
   * @param errorPlus
   */
  void addData(const QVector<double> &errorMinus, const QVector<double> &errorPlus);
  /**
   * @brief
   *
   * @param error
   */
  void addData(double error);
  /**
   * @brief
   *
   * @param errorMinus
   * @param errorPlus
   */
  void addData(double errorMinus, double errorPlus);
  
  // virtual methods of 1d plottable interface:
  /**
   * @brief
   *
   * @return int
   */
  virtual int dataCount() const;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataMainKey(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataSortKey(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return double
   */
  virtual double dataMainValue(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return QCPRange
   */
  virtual QCPRange dataValueRange(int index) const;
  /**
   * @brief
   *
   * @param index
   * @return QPointF
   */
  virtual QPointF dataPixelPosition(int index) const;
  /**
   * @brief
   *
   * @return bool
   */
  virtual bool sortKeyIsMainKey() const;
  /**
   * @brief
   *
   * @param rect
   * @param onlySelectable
   * @return QCPDataSelection
   */
  virtual QCPDataSelection selectTestRect(const QRectF &rect, bool onlySelectable) const;
  /**
   * @brief
   *
   * @param sortKey
   * @param expandedRange
   * @return int
   */
  virtual int findBegin(double sortKey, bool expandedRange=true) const;
  /**
   * @brief
   *
   * @param sortKey
   * @param expandedRange
   * @return int
   */
  virtual int findEnd(double sortKey, bool expandedRange=true) const;
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @return QCPPlottableInterface1D
   */
  virtual QCPPlottableInterface1D *interface1D() Q_DECL_OVERRIDE { return this; }
  
protected:
  // property members:
  QSharedPointer<QCPErrorBarsDataContainer> mDataContainer; /**< TODO: describe */
  QPointer<QCPAbstractPlottable> mDataPlottable; /**< TODO: describe */
  ErrorType mErrorType; /**< TODO: describe */
  double mWhiskerWidth; /**< TODO: describe */
  double mSymbolGap; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param painter
   * @param rect
   */
  virtual void drawLegendIcon(QCPPainter *painter, const QRectF &rect) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @return QCPRange
   */
  virtual QCPRange getKeyRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth) const Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param foundRange
   * @param inSignDomain
   * @param inKeyRange
   * @return QCPRange
   */
  virtual QCPRange getValueRange(bool &foundRange, QCP::SignDomain inSignDomain=QCP::sdBoth, const QCPRange &inKeyRange=QCPRange()) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param it
   * @param backbones
   * @param whiskers
   */
  void getErrorBarLines(QCPErrorBarsDataContainer::const_iterator it, QVector<QLineF> &backbones, QVector<QLineF> &whiskers) const;
  /**
   * @brief
   *
   * @param begin
   * @param end
   * @param rangeRestriction
   */
  void getVisibleDataBounds(QCPErrorBarsDataContainer::const_iterator &begin, QCPErrorBarsDataContainer::const_iterator &end, const QCPDataRange &rangeRestriction) const;
  /**
   * @brief
   *
   * @param pixelPoint
   * @param closestData
   * @return double
   */
  double pointDistance(const QPointF &pixelPoint, QCPErrorBarsDataContainer::const_iterator &closestData) const;
  // helpers:
  /**
   * @brief
   *
   * @param selectedSegments
   * @param unselectedSegments
   */
  void getDataSegments(QList<QCPDataRange> &selectedSegments, QList<QCPDataRange> &unselectedSegments) const;
  /**
   * @brief
   *
   * @param index
   * @return bool
   */
  bool errorBarVisible(int index) const;
  /**
   * @brief
   *
   * @param pixelRect
   * @param line
   * @return bool
   */
  bool rectIntersectsLine(const QRectF &pixelRect, const QLineF &line) const;
  
  friend class QCustomPlot;
  friend class QCPLegend;
};

/* end of 'src/plottables/plottable-errorbar.h' */


/* including file 'src/items/item-straightline.h', size 3117                 */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemStraightLine : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemStraightLine(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemStraightLine();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  
  // setters;
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  QCPItemPosition * const point1; /**< TODO: describe */
  QCPItemPosition * const point2; /**< TODO: describe */
  
protected:
  // property members:
  QPen mPen, mSelectedPen; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param point1
   * @param vec
   * @param rect
   * @return QLineF
   */
  QLineF getRectClippedStraightLine(const QCPVector2D &point1, const QCPVector2D &vec, const QRect &rect) const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
};

/* end of 'src/items/item-straightline.h' */


/* including file 'src/items/item-line.h', size 3407                         */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemLine : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  Q_PROPERTY(QCPLineEnding head READ head WRITE setHead)
  Q_PROPERTY(QCPLineEnding tail READ tail WRITE setTail)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemLine(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemLine();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  /**
   * @brief
   *
   * @return QCPLineEnding
   */
  QCPLineEnding head() const { return mHead; }
  /**
   * @brief
   *
   * @return QCPLineEnding
   */
  QCPLineEnding tail() const { return mTail; }
  
  // setters;
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  /**
   * @brief
   *
   * @param head
   */
  void setHead(const QCPLineEnding &head);
  /**
   * @brief
   *
   * @param tail
   */
  void setTail(const QCPLineEnding &tail);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  QCPItemPosition * const start; /**< TODO: describe */
  QCPItemPosition * const end; /**< TODO: describe */
  
protected:
  // property members:
  QPen mPen, mSelectedPen; /**< TODO: describe */
  QCPLineEnding mHead, mTail; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param start
   * @param end
   * @param rect
   * @return QLineF
   */
  QLineF getRectClippedLine(const QCPVector2D &start, const QCPVector2D &end, const QRect &rect) const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
};

/* end of 'src/items/item-line.h' */


/* including file 'src/items/item-curve.h', size 3379                        */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemCurve : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  Q_PROPERTY(QCPLineEnding head READ head WRITE setHead)
  Q_PROPERTY(QCPLineEnding tail READ tail WRITE setTail)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemCurve(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemCurve();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  /**
   * @brief
   *
   * @return QCPLineEnding
   */
  QCPLineEnding head() const { return mHead; }
  /**
   * @brief
   *
   * @return QCPLineEnding
   */
  QCPLineEnding tail() const { return mTail; }
  
  // setters;
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  /**
   * @brief
   *
   * @param head
   */
  void setHead(const QCPLineEnding &head);
  /**
   * @brief
   *
   * @param tail
   */
  void setTail(const QCPLineEnding &tail);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  QCPItemPosition * const start; /**< TODO: describe */
  QCPItemPosition * const startDir; /**< TODO: describe */
  QCPItemPosition * const endDir; /**< TODO: describe */
  QCPItemPosition * const end; /**< TODO: describe */
  
protected:
  // property members:
  QPen mPen, mSelectedPen; /**< TODO: describe */
  QCPLineEnding mHead, mTail; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
};

/* end of 'src/items/item-curve.h' */


/* including file 'src/items/item-rect.h', size 3688                         */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemRect : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  Q_PROPERTY(QBrush brush READ brush WRITE setBrush)
  Q_PROPERTY(QBrush selectedBrush READ selectedBrush WRITE setSelectedBrush)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemRect(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemRect();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush selectedBrush() const { return mSelectedBrush; }
  
  // setters;
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param brush
   */
  void setSelectedBrush(const QBrush &brush);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  QCPItemPosition * const topLeft; /**< TODO: describe */
  QCPItemPosition * const bottomRight; /**< TODO: describe */
  QCPItemAnchor * const top; /**< TODO: describe */
  QCPItemAnchor * const topRight; /**< TODO: describe */
  QCPItemAnchor * const right; /**< TODO: describe */
  QCPItemAnchor * const bottom; /**< TODO: describe */
  QCPItemAnchor * const bottomLeft; /**< TODO: describe */
  QCPItemAnchor * const left; /**< TODO: describe */
  
protected:
  /**
   * @brief
   *
   */
  enum AnchorIndex {aiTop, aiTopRight, aiRight, aiBottom, aiBottomLeft, aiLeft};
  
  // property members:
  QPen mPen, mSelectedPen; /**< TODO: describe */
  QBrush mBrush, mSelectedBrush; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param anchorId
   * @return QPointF
   */
  virtual QPointF anchorPixelPosition(int anchorId) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush mainBrush() const;
};

/* end of 'src/items/item-rect.h' */


/* including file 'src/items/item-text.h', size 5554                         */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemText : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QColor color READ color WRITE setColor)
  Q_PROPERTY(QColor selectedColor READ selectedColor WRITE setSelectedColor)
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  Q_PROPERTY(QBrush brush READ brush WRITE setBrush)
  Q_PROPERTY(QBrush selectedBrush READ selectedBrush WRITE setSelectedBrush)
  Q_PROPERTY(QFont font READ font WRITE setFont)
  Q_PROPERTY(QFont selectedFont READ selectedFont WRITE setSelectedFont)
  Q_PROPERTY(QString text READ text WRITE setText)
  Q_PROPERTY(Qt::Alignment positionAlignment READ positionAlignment WRITE setPositionAlignment)
  Q_PROPERTY(Qt::Alignment textAlignment READ textAlignment WRITE setTextAlignment)
  Q_PROPERTY(double rotation READ rotation WRITE setRotation)
  Q_PROPERTY(QMargins padding READ padding WRITE setPadding)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemText(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemText();
  
  // getters:
  /**
   * @brief
   *
   * @return QColor
   */
  QColor color() const { return mColor; }
  /**
   * @brief
   *
   * @return QColor
   */
  QColor selectedColor() const { return mSelectedColor; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush selectedBrush() const { return mSelectedBrush; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont font() const { return mFont; }
  /**
   * @brief
   *
   * @return QFont
   */
  QFont selectedFont() const { return mSelectedFont; }
  /**
   * @brief
   *
   * @return QString
   */
  QString text() const { return mText; }
  /**
   * @brief
   *
   * @return Qt::Alignment
   */
  Qt::Alignment positionAlignment() const { return mPositionAlignment; }
  /**
   * @brief
   *
   * @return Qt::Alignment
   */
  Qt::Alignment textAlignment() const { return mTextAlignment; }
  /**
   * @brief
   *
   * @return double
   */
  double rotation() const { return mRotation; }
  /**
   * @brief
   *
   * @return QMargins
   */
  QMargins padding() const { return mPadding; }
  
  // setters;
  /**
   * @brief
   *
   * @param color
   */
  void setColor(const QColor &color);
  /**
   * @brief
   *
   * @param color
   */
  void setSelectedColor(const QColor &color);
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param brush
   */
  void setSelectedBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param font
   */
  void setFont(const QFont &font);
  /**
   * @brief
   *
   * @param font
   */
  void setSelectedFont(const QFont &font);
  /**
   * @brief
   *
   * @param text
   */
  void setText(const QString &text);
  /**
   * @brief
   *
   * @param alignment
   */
  void setPositionAlignment(Qt::Alignment alignment);
  /**
   * @brief
   *
   * @param alignment
   */
  void setTextAlignment(Qt::Alignment alignment);
  /**
   * @brief
   *
   * @param degrees
   */
  void setRotation(double degrees);
  /**
   * @brief
   *
   * @param padding
   */
  void setPadding(const QMargins &padding);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  QCPItemPosition * const position; /**< TODO: describe */
  QCPItemAnchor * const topLeft; /**< TODO: describe */
  QCPItemAnchor * const top; /**< TODO: describe */
  QCPItemAnchor * const topRight; /**< TODO: describe */
  QCPItemAnchor * const right; /**< TODO: describe */
  QCPItemAnchor * const bottomRight; /**< TODO: describe */
  QCPItemAnchor * const bottom; /**< TODO: describe */
  QCPItemAnchor * const bottomLeft; /**< TODO: describe */
  QCPItemAnchor * const left; /**< TODO: describe */
  
protected:
  /**
   * @brief
   *
   */
  enum AnchorIndex {aiTopLeft, aiTop, aiTopRight, aiRight, aiBottomRight, aiBottom, aiBottomLeft, aiLeft};
  
  // property members:
  QColor mColor, mSelectedColor; /**< TODO: describe */
  QPen mPen, mSelectedPen; /**< TODO: describe */
  QBrush mBrush, mSelectedBrush; /**< TODO: describe */
  QFont mFont, mSelectedFont; /**< TODO: describe */
  QString mText; /**< TODO: describe */
  Qt::Alignment mPositionAlignment; /**< TODO: describe */
  Qt::Alignment mTextAlignment; /**< TODO: describe */
  double mRotation; /**< TODO: describe */
  QMargins mPadding; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param anchorId
   * @return QPointF
   */
  virtual QPointF anchorPixelPosition(int anchorId) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param rect
   * @param positionAlignment
   * @return QPointF
   */
  QPointF getTextDrawPoint(const QPointF &pos, const QRectF &rect, Qt::Alignment positionAlignment) const;
  /**
   * @brief
   *
   * @return QFont
   */
  QFont mainFont() const;
  /**
   * @brief
   *
   * @return QColor
   */
  QColor mainColor() const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush mainBrush() const;
};

/* end of 'src/items/item-text.h' */


/* including file 'src/items/item-ellipse.h', size 3868                      */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemEllipse : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  Q_PROPERTY(QBrush brush READ brush WRITE setBrush)
  Q_PROPERTY(QBrush selectedBrush READ selectedBrush WRITE setSelectedBrush)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemEllipse(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemEllipse();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush selectedBrush() const { return mSelectedBrush; }
  
  // setters;
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param brush
   */
  void setSelectedBrush(const QBrush &brush);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  QCPItemPosition * const topLeft; /**< TODO: describe */
  QCPItemPosition * const bottomRight; /**< TODO: describe */
  QCPItemAnchor * const topLeftRim; /**< TODO: describe */
  QCPItemAnchor * const top; /**< TODO: describe */
  QCPItemAnchor * const topRightRim; /**< TODO: describe */
  QCPItemAnchor * const right; /**< TODO: describe */
  QCPItemAnchor * const bottomRightRim; /**< TODO: describe */
  QCPItemAnchor * const bottom; /**< TODO: describe */
  QCPItemAnchor * const bottomLeftRim; /**< TODO: describe */
  QCPItemAnchor * const left; /**< TODO: describe */
  QCPItemAnchor * const center; /**< TODO: describe */
  
protected:
  /**
   * @brief
   *
   */
  enum AnchorIndex {aiTopLeftRim, aiTop, aiTopRightRim, aiRight, aiBottomRightRim, aiBottom, aiBottomLeftRim, aiLeft, aiCenter};
  
  // property members:
  QPen mPen, mSelectedPen; /**< TODO: describe */
  QBrush mBrush, mSelectedBrush; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param anchorId
   * @return QPointF
   */
  virtual QPointF anchorPixelPosition(int anchorId) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush mainBrush() const;
};

/* end of 'src/items/item-ellipse.h' */


/* including file 'src/items/item-pixmap.h', size 4373                       */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemPixmap : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPixmap pixmap READ pixmap WRITE setPixmap)
  Q_PROPERTY(bool scaled READ scaled WRITE setScaled)
  Q_PROPERTY(Qt::AspectRatioMode aspectRatioMode READ aspectRatioMode)
  Q_PROPERTY(Qt::TransformationMode transformationMode READ transformationMode)
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  /// \endcond
public:
  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemPixmap(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemPixmap();
  
  // getters:
  /**
   * @brief
   *
   * @return QPixmap
   */
  QPixmap pixmap() const { return mPixmap; }
  /**
   * @brief
   *
   * @return bool
   */
  bool scaled() const { return mScaled; }
  /**
   * @brief
   *
   * @return Qt::AspectRatioMode
   */
  Qt::AspectRatioMode aspectRatioMode() const { return mAspectRatioMode; }
  /**
   * @brief
   *
   * @return Qt::TransformationMode
   */
  Qt::TransformationMode transformationMode() const { return mTransformationMode; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  
  // setters;
  /**
   * @brief
   *
   * @param pixmap
   */
  void setPixmap(const QPixmap &pixmap);
  /**
   * @brief
   *
   * @param scaled
   * @param aspectRatioMode
   * @param transformationMode
   */
  void setScaled(bool scaled, Qt::AspectRatioMode aspectRatioMode=Qt::KeepAspectRatio, Qt::TransformationMode transformationMode=Qt::SmoothTransformation);
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  QCPItemPosition * const topLeft; /**< TODO: describe */
  QCPItemPosition * const bottomRight; /**< TODO: describe */
  QCPItemAnchor * const top; /**< TODO: describe */
  QCPItemAnchor * const topRight; /**< TODO: describe */
  QCPItemAnchor * const right; /**< TODO: describe */
  QCPItemAnchor * const bottom; /**< TODO: describe */
  QCPItemAnchor * const bottomLeft; /**< TODO: describe */
  QCPItemAnchor * const left; /**< TODO: describe */
  
protected:
  /**
   * @brief
   *
   */
  enum AnchorIndex {aiTop, aiTopRight, aiRight, aiBottom, aiBottomLeft, aiLeft};
  
  // property members:
  QPixmap mPixmap; /**< TODO: describe */
  QPixmap mScaledPixmap; /**< TODO: describe */
  bool mScaled; /**< TODO: describe */
  bool mScaledPixmapInvalidated; /**< TODO: describe */
  Qt::AspectRatioMode mAspectRatioMode; /**< TODO: describe */
  Qt::TransformationMode mTransformationMode; /**< TODO: describe */
  QPen mPen, mSelectedPen; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param anchorId
   * @return QPointF
   */
  virtual QPointF anchorPixelPosition(int anchorId) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @param finalRect
   * @param flipHorz
   * @param flipVert
   */
  void updateScaledPixmap(QRect finalRect=QRect(), bool flipHorz=false, bool flipVert=false);
  /**
   * @brief
   *
   * @param flippedHorz
   * @param flippedVert
   * @return QRect
   */
  QRect getFinalRect(bool *flippedHorz=0, bool *flippedVert=0) const;
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
};

/* end of 'src/items/item-pixmap.h' */


/* including file 'src/items/item-tracer.h', size 4762                       */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemTracer : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  Q_PROPERTY(QBrush brush READ brush WRITE setBrush)
  Q_PROPERTY(QBrush selectedBrush READ selectedBrush WRITE setSelectedBrush)
  Q_PROPERTY(double size READ size WRITE setSize)
  Q_PROPERTY(TracerStyle style READ style WRITE setStyle)
  Q_PROPERTY(QCPGraph* graph READ graph WRITE setGraph)
  Q_PROPERTY(double graphKey READ graphKey WRITE setGraphKey)
  Q_PROPERTY(bool interpolating READ interpolating WRITE setInterpolating)
  /// \endcond
public:
  /*!
    The different visual appearances a tracer item can have. Some styles size may be controlled with \ref setSize.
    
    \see setStyle
  */
  enum TracerStyle { tsNone        ///< The tracer is not visible
                     ,tsPlus       ///< A plus shaped crosshair with limited size
                     ,tsCrosshair  ///< A plus shaped crosshair which spans the complete axis rect
                     ,tsCircle     ///< A circle
                     ,tsSquare     ///< A square
                   };
  Q_ENUMS(TracerStyle)

  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemTracer(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemTracer();

  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush brush() const { return mBrush; }
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush selectedBrush() const { return mSelectedBrush; }
  /**
   * @brief
   *
   * @return double
   */
  double size() const { return mSize; }
  /**
   * @brief
   *
   * @return TracerStyle
   */
  TracerStyle style() const { return mStyle; }
  /**
   * @brief
   *
   * @return QCPGraph
   */
  QCPGraph *graph() const { return mGraph; }
  /**
   * @brief
   *
   * @return double
   */
  double graphKey() const { return mGraphKey; }
  /**
   * @brief
   *
   * @return bool
   */
  bool interpolating() const { return mInterpolating; }

  // setters;
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  /**
   * @brief
   *
   * @param brush
   */
  void setBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param brush
   */
  void setSelectedBrush(const QBrush &brush);
  /**
   * @brief
   *
   * @param size
   */
  void setSize(double size);
  /**
   * @brief
   *
   * @param style
   */
  void setStyle(TracerStyle style);
  /**
   * @brief
   *
   * @param graph
   */
  void setGraph(QCPGraph *graph);
  /**
   * @brief
   *
   * @param key
   */
  void setGraphKey(double key);
  /**
   * @brief
   *
   * @param enabled
   */
  void setInterpolating(bool enabled);

  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   */
  void updatePosition();

  QCPItemPosition * const position; /**< TODO: describe */

protected:
  // property members:
  QPen mPen, mSelectedPen; /**< TODO: describe */
  QBrush mBrush, mSelectedBrush; /**< TODO: describe */
  double mSize; /**< TODO: describe */
  TracerStyle mStyle; /**< TODO: describe */
  QCPGraph *mGraph; /**< TODO: describe */
  double mGraphKey; /**< TODO: describe */
  bool mInterpolating; /**< TODO: describe */

  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;

  // non-virtual methods:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
  /**
   * @brief
   *
   * @return QBrush
   */
  QBrush mainBrush() const;
};
Q_DECLARE_METATYPE(QCPItemTracer::TracerStyle)

/* end of 'src/items/item-tracer.h' */


/* including file 'src/items/item-bracket.h', size 3969                      */
/* commit 633339dadc92cb10c58ef3556b55570685fafb99 2016-09-13 23:54:56 +0200 */

class QCP_LIB_DECL QCPItemBracket : public QCPAbstractItem
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QPen pen READ pen WRITE setPen)
  Q_PROPERTY(QPen selectedPen READ selectedPen WRITE setSelectedPen)
  Q_PROPERTY(double length READ length WRITE setLength)
  Q_PROPERTY(BracketStyle style READ style WRITE setStyle)
  /// \endcond
public:
  /*!
    Defines the various visual shapes of the bracket item. The appearance can be further modified
    by \ref setLength and \ref setPen.
    
    \see setStyle
  */
  enum BracketStyle { bsSquare  ///< A brace with angled edges
                      ,bsRound  ///< A brace with round edges
                      ,bsCurly  ///< A curly brace
                      ,bsCalligraphic ///< A curly brace with varying stroke width giving a calligraphic impression
  };
  Q_ENUMS(BracketStyle)

  /**
   * @brief
   *
   * @param parentPlot
   */
  explicit QCPItemBracket(QCustomPlot *parentPlot);
  /**
   * @brief
   *
   */
  virtual ~QCPItemBracket();
  
  // getters:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen pen() const { return mPen; }
  /**
   * @brief
   *
   * @return QPen
   */
  QPen selectedPen() const { return mSelectedPen; }
  /**
   * @brief
   *
   * @return double
   */
  double length() const { return mLength; }
  /**
   * @brief
   *
   * @return BracketStyle
   */
  BracketStyle style() const { return mStyle; }
  
  // setters;
  /**
   * @brief
   *
   * @param pen
   */
  void setPen(const QPen &pen);
  /**
   * @brief
   *
   * @param pen
   */
  void setSelectedPen(const QPen &pen);
  /**
   * @brief
   *
   * @param length
   */
  void setLength(double length);
  /**
   * @brief
   *
   * @param style
   */
  void setStyle(BracketStyle style);
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param pos
   * @param onlySelectable
   * @param details
   * @return double
   */
  virtual double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const Q_DECL_OVERRIDE;
  
  QCPItemPosition * const left; /**< TODO: describe */
  QCPItemPosition * const right; /**< TODO: describe */
  QCPItemAnchor * const center; /**< TODO: describe */
  
protected:
  // property members:
  /**
   * @brief
   *
   */
  enum AnchorIndex {aiCenter};
  QPen mPen, mSelectedPen; /**< TODO: describe */
  double mLength; /**< TODO: describe */
  BracketStyle mStyle; /**< TODO: describe */
  
  // reimplemented virtual methods:
  /**
   * @brief
   *
   * @param painter
   */
  virtual void draw(QCPPainter *painter) Q_DECL_OVERRIDE;
  /**
   * @brief
   *
   * @param anchorId
   * @return QPointF
   */
  virtual QPointF anchorPixelPosition(int anchorId) const Q_DECL_OVERRIDE;
  
  // non-virtual methods:
  /**
   * @brief
   *
   * @return QPen
   */
  QPen mainPen() const;
};
Q_DECLARE_METATYPE(QCPItemBracket::BracketStyle)

/* end of 'src/items/item-bracket.h' */


#endif // QCUSTOMPLOT_H

