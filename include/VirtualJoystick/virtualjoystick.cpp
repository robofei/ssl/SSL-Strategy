/*
 * Copyright (c) 2015-2017 Alex Spataru <alex_spataru@outlook.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <math.h>
#include "virtualjoystick.h"



VirtualJoystick::VirtualJoystick(QObject *parent): QObject(parent)
{
    axisAD.reset(new VirtualJoystickAxis(VirtualJoystickAxisEvent(AXIS_AD, 0)));
    axisSW.reset(new VirtualJoystickAxis(VirtualJoystickAxisEvent(AXIS_SW, 0)));
    axisQE.reset(new VirtualJoystickAxis(VirtualJoystickAxisEvent(AXIS_QE, 0)));
    axisUO.reset(new VirtualJoystickAxis(VirtualJoystickAxisEvent(AXIS_UO, axisMinimumV)));
    axisJL.reset(new VirtualJoystickAxis(VirtualJoystickAxisEvent(AXIS_JL, axisMinimumV)));

    updateTimer.reset(new QTimer(this));


    emit SIGNAL_axisEvent(axisAD.get()->event);
    emit SIGNAL_axisEvent(axisSW.get()->event);
    emit SIGNAL_axisEvent(axisQE.get()->event);
    emit SIGNAL_axisEvent(axisUO.get()->event);
    emit SIGNAL_axisEvent(axisJL.get()->event);

    updateTimer.get()->start(10);

    axisStep = 500;

    qApp->installEventFilter(this);


    connect(updateTimer.get(), &QTimer::timeout,
            this, &VirtualJoystick::SLOT_updateAxis);

}

VirtualJoystick::~VirtualJoystick()
{

}

void VirtualJoystick::vResetAxis(qint8 id)
{
    switch (id)
    {
        case AXIS_AD:
            axisAD->event.value = 0;
            emit SIGNAL_axisEvent(axisAD->event);
            break;
        case AXIS_SW:
            axisSW->event.value = 0;
            emit SIGNAL_axisEvent(axisSW->event);
            break;
        case AXIS_QE:
            axisQE->event.value = 0;
            emit SIGNAL_axisEvent(axisQE->event);
            break;
        case AXIS_UO:
            axisUO->event.value = axisMinimumV;
            emit SIGNAL_axisEvent(axisUO->event);
            break;
        case AXIS_JL:
            axisJL->event.value = axisMinimumV;
            emit SIGNAL_axisEvent(axisJL->event);
            break;
    }
}

void VirtualJoystick::vResetAxis(const std::initializer_list<qint8>& ids)
{
    for(qint8 id : ids)
    {
        vResetAxis(id);
    }
}

void VirtualJoystick::vChangeAxisValue(VirtualJoystickAxis *axis){

    switch (axis->state) {

        case STILL:
            return;

        case INCREASE:

            if(axis->event.value > axisMaximumV - axisStep)
                axis->event.value = axisMaximumV;

            else
                axis->event.value += axisStep;

            break;

        case DECREASE:

            if(axis->event.value < axisMinimumV + axisStep)
                axis->event.value = axisMinimumV;

            else
                axis->event.value -= axisStep;

            break;
    }

    emit SIGNAL_axisEvent(axis->event);


}

void VirtualJoystick::SLOT_setStepValue(const qint16 newValue){
    axisStep = newValue;
}



void VirtualJoystick::SLOT_updateAxis()
{
    vChangeAxisValue(axisAD.get());
    vChangeAxisValue(axisSW.get());
    vChangeAxisValue(axisQE.get());
    vChangeAxisValue(axisUO.get());
    vChangeAxisValue(axisJL.get());
}



void VirtualJoystick::SLOT_readAxes (const Qt::Key key, const bool pressed){

    if (key == Qt::Key_D){

        if(pressed)
            axisAD->state = INCREASE;

        else
            axisAD->state = STILL;

    }
    else if (key == Qt::Key_A) {

        if(pressed)
            axisAD->state = DECREASE;

        else
            axisAD->state = STILL;

    }



    if (key == Qt::Key_W){

        if(pressed)
            axisSW->state = INCREASE;

        else
            axisSW->state = STILL;

    }
    else if (key == Qt::Key_S) {

        if(pressed)
            axisSW->state = DECREASE;

        else
            axisSW->state = STILL;

    }



    if (key == Qt::Key_E){

        if(pressed)
            axisQE->state = INCREASE;

        else
            axisQE->state = STILL;
    }

    else if (key == Qt::Key_Q) {

        if(pressed)
            axisQE->state = DECREASE;

        else
            axisQE->state = STILL;

    }



    if (key == Qt::Key_O){

        if(pressed)
            axisUO->state = INCREASE;

        else
            axisUO->state = STILL;
    }
    else if (key == Qt::Key_U) {

        if(pressed)
            axisUO->state = DECREASE;

        else
            axisUO->state = STILL;

    }



    if (key == Qt::Key_L){

        if(pressed)
            axisJL->state = INCREASE;

        else
            axisJL->state = STILL;
    }


    else if (key == Qt::Key_J) {

        if(pressed)
            axisJL->state = DECREASE;

        else
            axisJL->state = STILL;

    }

}



void VirtualJoystick::SLOT_readButtons (const Qt::Key key, const bool pressed)
{
    if(listOfButtons.contains(key)){
        VirtualJoystickButtonEvent event = VirtualJoystickButtonEvent (key, pressed);
        emit SIGNAL_buttonEvent( event );
    }
}


void VirtualJoystick::SLOT_processKeyEvent (const QKeyEvent *event, const bool pressed)
{
    SLOT_readAxes(static_cast<Qt::Key>(event->key()), pressed);
    SLOT_readButtons(static_cast<Qt::Key>(event->key()), pressed);
}


bool VirtualJoystick::eventFilter (QObject* object, QEvent* event)
{
    Q_UNUSED(object);

    switch (event->type()) {
        case QEvent::KeyPress:
            SLOT_processKeyEvent (static_cast <QKeyEvent*> (event), true);
            break;
        case QEvent::KeyRelease:
            SLOT_processKeyEvent (static_cast <QKeyEvent*> (event), false);
            break;
        default:
            break;
    }

    return false;
}


