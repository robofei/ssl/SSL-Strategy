#!/usr/bin/sh
rm -rf libs
mkdir libs
cd libs
mkdir OsqpEigen
mkdir OSQP

C_PATH="$PWD"

git clone --recursive https://github.com/osqp/osqp
cd osqp
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$C_PATH/OSQP -G "Unix Makefiles" ..
cmake --build .
cmake --build . --target install

cmake -G "Unix Makefiles" ..
cmake --build .
cmake --build . --target install
cd $C_PATH

git clone https://github.com/robotology/osqp-eigen.git
cd osqp-eigen
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$C_PATH/OsqpEigen ..
make
make install
cd $C_PATH

git clone https://gitlab.com/libeigen/eigen.git

cd $C_PATH
rm -rf osqp osqp-eigen

# sudo mkdir /lib/robofei
# cd /lib/robofei
cd libs
ln -s $C_PATH/OsqpEigen/lib/libOsqpEigen.so
