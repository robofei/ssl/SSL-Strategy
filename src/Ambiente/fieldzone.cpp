#include "fieldzone.h"
#include "qglobal.h"

FieldZone::FieldZone()
{
    bConfigured = false;
    type = RECTANGLE;
    rctZone = QRect(0, 0, 0, 0);
    circleZone = Circle(QPoint(0, 0), 0);
}

FieldZone::FieldZone(const QRect& rct)
{
    bConfigured = true;
    type = RECTANGLE;

    rctZone = rct;
}

FieldZone::FieldZone(const Circle& cir)
{
    bConfigured = true;
    type = CIRCLE;

    circleZone = cir;
}

bool FieldZone::bIsConfigured() const
{
    return bConfigured;
}

ZONE_TYPE FieldZone::ztZoneType() const
{
    return type;
}

void FieldZone::vConfigZone(QRect _zoneRect)
{
    bConfigured = true;
    type = RECTANGLE;

    rctZone = _zoneRect;
}

void FieldZone::vConfigZone(QPoint _center, float _radius)
{
    bConfigured = true;
    type = CIRCLE;

    circleZone.setCenter(_center);
    circleZone.setRadius(_radius);
}

void FieldZone::vConfigZone(const FieldZone& _other)
{
    bConfigured = _other.bIsConfigured();
    type = _other.ztZoneType();

    rctZone = _other.rctGetZone();
    circleZone = _other.circleGetZone();
}

float FieldZone::fDistanceToZone(const QVector2D &_position) const
{
    if(!bConfigured)
    {
        qWarning() << "[FieldZone] Trying to use without configuration";
        return 99999999.9;
    }
    if(type == RECTANGLE)
    {
        return _position.distanceToPoint(QVector2D(rctZone.center()));
    }
    else if(type == CIRCLE)
    {
        return _position.distanceToPoint(QVector2D(circleZone.getCenter()));
    }

    qWarning() << "[FieldZone] Invalid type " << type;
    return 99999999.9;
}

bool FieldZone::bInsideZone(const QVector2D &_position)
{
    if(type == RECTANGLE)
    {
        return rctZone.contains(_position.toPoint());
    }
    else if(type == CIRCLE)
    {
        return (_position.distanceToPoint(QVector2D(circleZone.getCenter())) <
                                                       circleZone.getRadius());
    }

    qWarning() << "[FieldZone] Invalid type " << type;
    return false;
}

QPoint FieldZone::ptZoneCenter() const
{
    if(type == RECTANGLE)
    {
        return rctZone.center();
    }
    else if(type == CIRCLE)
    {
        return circleZone.getCenter();
    }
    return {0,0};
}

int FieldZone::iSize() const
{
    if(type == RECTANGLE)
    {
        return qMin(rctZone.width(), rctZone.height());
    }
    else if(type == CIRCLE)
    {
        return circleZone.getRadius();
    }
    return 0;
}

QRect FieldZone::rctGetZone() const
{
    return rctZone;
}

Circle FieldZone::circleGetZone() const
{
    return circleZone;
}
