#ifndef FIELDZONE_H
#define FIELDZONE_H

#include <QDebug>
#include <QRect>
#include <QPoint>
#include <QVector2D>

/// TODO: Documentar

/**
 * @brief
 */
typedef enum ZONE_TYPE
{
    RECTANGLE = 0,
    CIRCLE    = 1
}ZONE_TYPE;

/**
 * @brief
 */
class Circle
{
    QPoint center;
    float radius;

public:
    /**
     * @brief
     */
    Circle() { }

    /**
     * @brief
     *
     * @param _center
     * @param _radius
     */
    Circle(QPoint _center, float _radius)
    {
        center = _center;
        radius = _radius;
    }

    /**
     * @brief
     *
     * @return
     */
    QPoint getCenter() const
    {
        return center;
    }

    /**
     * @brief
     *
     * @param _center
     */
    void setCenter(const QPoint &_center)
    {
        center = _center;
    }

    /**
     * @brief
     *
     * @return
     */
    float getRadius() const
    {
        return radius;
    }

    /**
     * @brief
     *
     * @param _radius
     */
    void setRadius(const float &_radius)
    {
        radius = _radius;
    }

    /**
     * @brief
     *
     * @param _position
     *
     * @return
     */
    float distanceToCenter(const QVector2D &_position)
    {
        return _position.distanceToPoint(QVector2D(center));
    }
};

class FieldZone
{
    QRect rctZone;
    Circle circleZone;

    ZONE_TYPE type;
    bool bConfigured;
public:
    FieldZone();
    FieldZone(const QRect& rct);
    FieldZone(const Circle& cir);

    /**
     * @brief
     *
     * @return
     */
    bool bIsConfigured() const;

    /**
     * @brief
     *
     * @return
     */
    ZONE_TYPE ztZoneType() const;

    /**
     * @brief
     *
     * @param _zoneRect
     */
    void vConfigZone(QRect _zoneRect);

    /**
     * @brief
     *
     * @param _center
     * @param _radius
     */
    void vConfigZone(QPoint _center, float _radius);

    /**
     * @brief
     *
     * @param _other
     */
    void vConfigZone(const FieldZone& _other);

    /**
     * @brief
     *
     * @param _position
     *
     * @return
     */
    float fDistanceToZone(const QVector2D &_position) const;

    /**
     * @brief Retorna se o ponto está dentro da zona
     *
     * @param _position
     *
     * @return
     */
    bool bInsideZone(const QVector2D &_position);

    /**
     * @brief
     *
     * @return
     */
    QPoint ptZoneCenter() const;

    /**
     * @brief Para zona circular retorna o raio, para zona retangular retorna
     * a menor dimensão.
     *
     * @return
     */
    int iSize() const;

    /**
     * @brief Retorna o retângulo da zona
     *
     * @return
     */
    QRect rctGetZone() const;

    /**
     * @brief Retorna o círculo da zona
     *
     * @return
     */
    Circle circleGetZone() const;
};

#endif // FIELDZONE_H
