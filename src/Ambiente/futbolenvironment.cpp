﻿/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "futbolenvironment.h"
#include "qmap.h"
#include <QDebug>
#include <QTimer>

AmbienteCampo::AmbienteCampo()
{
    
    iRobosEmCampo = 0;
    iRemainingPlayTime = 0;

    allies.reset(new SSLTeam<Robo>(0, XNegativo, timeAzul));
    opponents.reset(new SSLTeam<Robo>(0, XPositivo, timeAmarelo));

    vt3dPosicaoSimulacaoRobo.clear();
    vt3dVelocidadeSimulacaoRobo.clear();
    blBola.reset(new Bola());
    geoCampo.reset(new Geometria());
    debugInfo.reset(new DebugInfo());

    bTipoAmbiente = GR_SIM;
    sslrefComandoRefereeAtual = SSL_Referee_Command_HALT;

    tsDadosTeste.tstTesteAtual = tstNenhum;
    tsDadosTeste.vt2dDestinoTesteRobo = QVector2D(0, 0);
    tsDadosTeste.iIDRoboTeste = 0;
    tsDadosTeste.iTrajetosRobosMouse.clear();

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        for (int j = 0; j < iJogadasAtaque; ++j)
        {
            bMatrizJogadas[i][j] = true;
        }
    }
}

AmbienteCampo::AmbienteCampo(const AmbienteCampo& _acAmbiente)
{
    this->iRobosEmCampo = _acAmbiente.iRobosEmCampo;
    this->vSetRemainingPlayTime(_acAmbiente.iGetRemainingPlayTime());

    this->blBola.reset(new Bola(_acAmbiente.blPegaBola()));
    this->allies.reset(new SSLTeam<Robo>(*_acAmbiente.allies.get()));
    this->opponents.reset(new SSLTeam<Robo>(*_acAmbiente.opponents.get()));
    this->geoCampo.reset(new Geometria(*_acAmbiente.geoCampo.get()));

    this->debugInfo.reset(new DebugInfo(*_acAmbiente.debugInfo.get()));

    // Atualiza matriz de permissao das jogadas
    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        for (int j = 0; j < iJogadasAtaque; ++j)
        {
            this->bMatrizJogadas[i][j] = _acAmbiente.bMatrizJogadas[i][j];
        }
    }

    // Trajetos da movimentacao via mouse
    this->tsDadosTeste = _acAmbiente.tsDadosTeste;

    // teste
    this->vt3dPosicaoSimulacaoRobo.clear();
    this->vt3dPosicaoSimulacaoRobo.append(_acAmbiente.vt3dPosicaoSimulacaoRobo);
    this->vt3dVelocidadeSimulacaoRobo.clear();
    this->vt3dVelocidadeSimulacaoRobo.append(
        _acAmbiente.vt3dVelocidadeSimulacaoRobo);

    // Cor do time, tipo do ambiente (grSim/CampoReal) e lado defesa
    this->bTipoAmbiente = _acAmbiente.bTipoAmbiente;

    // Dimensoes campo

    //     this->vSetaTamanhoCampo(_acAmbiente.szPegaTamanhoCampo());
    //     this->vSetaRaioCirculoCentroCampo(_acAmbiente.iPegaRaioCirculoCentroCampo());
    //     this->vSetaAreaDefesa(_acAmbiente.szPegaAreaDefesa());
    //     this->vSetaTamanhoGol(_acAmbiente.szPegaGol());
    this->vt2dPosicaoTimeout = _acAmbiente.vt2dPosicaoTimeout;

    // Dados do referee e teste atual
    this->sslrefComandoRefereeAtual = _acAmbiente.sslrefComandoRefereeAtual;
}

AmbienteCampo::~AmbienteCampo()
{
}

void AmbienteCampo::vSetaBola(const Bola& _bola)
{
    blBola.reset(new Bola(_bola));
}

QVector2D AmbienteCampo::vt2dPosicaoBola() const
{
    return blBola->vt2dPosicaoAtual;
}

QVector2D AmbienteCampo::vt2dVelocidadeBola() const
{
    return blBola->vt2dVelocidade;
}

double AmbienteCampo::dVelocidadeBola() const
{
    return blBola->dVelocidade;
}

void AmbienteCampo::vResetaTodasJogadas(bool todos)
{
    Robo* n;
    if (todos)
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            n = allies->getPlayer(id);
            n->vSetaJogada(Nenhuma);
            n->vDesligaChute();
        }
    }
    else
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            n = allies->getPlayer(id);
            if (n->jJogadaAtual() != KickOff_Cobrador &&
                n->jJogadaAtual() != Penalty_Cobrador)
            {
                n->vSetaJogada(Nenhuma);
                n->vDesligaChute();
                n->vDesligaRoller();
            }
        }
    }
}

void AmbienteCampo::vSetaRobosEmCampo(int n)
{
    iRobosEmCampo = n;
}

int AmbienteCampo::iPegaRobosEmCampo() const
{
    int inField = 0;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        if (allies->getPlayer(id)->bRoboDetectado())
        {
            inField++;
        }
    }
    return inField;
}

const Bola& AmbienteCampo::blPegaBola() const
{
    return *blBola;
}

QVector<QVector3D> AmbienteCampo::vt3dPegaPosicaoTodosObjetos(
    TipoObjeto otObjeto, bool _bEnviarGoleiro,
    const QVector<int>& _iIDIgnorado) const
{
    QVector<QVector3D> vt3dPosicaoObjetos;
    vt3dPosicaoObjetos.clear();

    if (otObjeto == otAliado)
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            if (allies->getPlayer(id)->bRoboDetectado())
            {
                if (allies->getPlayer(id)->iID() == allies->iGetGoalieID() &&
                    _bEnviarGoleiro == true &&
                    _iIDIgnorado.contains(allies->getPlayer(id)->iID()) ==
                        false)
                {
                    vt3dPosicaoObjetos.append(QVector3D(
                        allies->getPlayer(id)->vt2dPosicaoAtual(), otAliado));
                }
                else if (allies->getPlayer(id)->iID() !=
                             allies->iGetGoalieID() &&
                         _iIDIgnorado.contains(allies->getPlayer(id)->iID()) ==
                             false)
                {
                    vt3dPosicaoObjetos.append(QVector3D(
                        allies->getPlayer(id)->vt2dPosicaoAtual(), otAliado));
                }
            }
        }
    }
    else if (otObjeto == otOponente)
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            if (opponents->getPlayer(id)->bRoboDetectado())
            {
                if (opponents->getPlayer(id)->iID() ==
                        opponents->iGetGoalieID() &&
                    _bEnviarGoleiro == true)
                {
                    vt3dPosicaoObjetos.append(
                        QVector3D(opponents->getPlayer(id)->vt2dPosicaoAtual(),
                                  otOponente));
                }
                else if (opponents->getPlayer(id)->iID() !=
                             opponents->iGetGoalieID() &&
                         _iIDIgnorado.contains(
                             opponents->getPlayer(id)->iID()) == false)
                {
                    vt3dPosicaoObjetos.append(
                        QVector3D(opponents->getPlayer(id)->vt2dPosicaoAtual(),
                                  otOponente));
                }
            }
        }
    }
    else if (otObjeto == otBola)
    {
        if (this->blBola->bEmCampo)
        {
            vt3dPosicaoObjetos.append(
                QVector3D(this->blBola->vt2dPosicaoAtual, otBola));
        }
    }

    return vt3dPosicaoObjetos;
}

QVector<QVector3D> AmbienteCampo::vt3dPegaPosicaoTodosObjetos(
    TipoObjeto otObjeto, int iRegiaoInicial, int iRegiaoFinal,
    int _iIDIgnorado) const
{
    QVector<QVector3D> vt3dPosicaoObjetos;
    vt3dPosicaoObjetos.clear();
    int iRegiaoAux = -1;

    if (otObjeto == otAliado)
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            iRegiaoAux = Auxiliar::iCalculaRegiaoPonto(
                allies->getPlayer(id)->vt2dPosicaoAtual(),
                geoCampo->szField().width(), globalConfig.fieldDivisionWidth);
            if (allies->getPlayer(id)->bRoboDetectado() &&
                (iRegiaoInicial <= iRegiaoAux && iRegiaoAux <= iRegiaoFinal) &&
                allies->getPlayer(id)->iID() != _iIDIgnorado)
            {
                vt3dPosicaoObjetos.append(QVector3D(
                    allies->getPlayer(id)->vt2dPosicaoAtual(), otAliado));
            }
        }
    }
    else if (otObjeto == otOponente)
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            iRegiaoAux = Auxiliar::iCalculaRegiaoPonto(
                opponents->getPlayer(id)->vt2dPosicaoAtual(),
                geoCampo->szField().width(), globalConfig.fieldDivisionWidth);
            if (opponents->getPlayer(id)->bRoboDetectado() &&
                (iRegiaoInicial <= iRegiaoAux && iRegiaoAux <= iRegiaoFinal) &&
                opponents->getPlayer(id)->iID() != _iIDIgnorado)
            {
                vt3dPosicaoObjetos.append(QVector3D(
                    opponents->getPlayer(id)->vt2dPosicaoAtual(), otOponente));
            }
        }
    }
    else if (otObjeto == otBola)
    {
        iRegiaoAux = Auxiliar::iCalculaRegiaoPonto(
            this->blBola->vt2dPosicaoAtual, geoCampo->szField().width(),
            globalConfig.fieldDivisionWidth);
        if (this->blBola->bEmCampo &&
            (iRegiaoInicial <= iRegiaoAux && iRegiaoAux <= iRegiaoFinal))
        {
            vt3dPosicaoObjetos.append(
                QVector3D(this->blBola->vt2dPosicaoAtual, otBola));
        }
    }

    return vt3dPosicaoObjetos;
}

QVector<QVector3D> AmbienteCampo::vt_vt3dPegaPosicaoDeTodosOsObjetosSemExcecao(
    const TipoObjeto otObjeto, QVector2D vt2dPosicaoObjetosForaDeCampo) const
{
    if (otObjeto == otAliado)
    {
        QVector<QVector3D> vt_vt3d = QVector<QVector3D>(
            globalConfig.robotsPerTeam,
            QVector3D(vt2dPosicaoObjetosForaDeCampo, otAliado));

        for (qint8 id = 0; id < globalConfig.robotsPerTeam; id++)
        {

            if (allies->getPlayer(id)->bRoboDetectado())
            {
                vt_vt3d.replace(
                    id, QVector3D(allies->getPlayer(id)->vt2dPosicaoAtual(),
                                  otAliado));
            }
        }
        return vt_vt3d;
    }
    if (otObjeto == otOponente)
    {
        QVector<QVector3D> vt_vt3d = QVector<QVector3D>(
            globalConfig.robotsPerTeam,
            QVector3D(vt2dPosicaoObjetosForaDeCampo, otOponente));

        for (qint8 id = 0; id < globalConfig.robotsPerTeam; id++)
        {

            if (opponents->getPlayer(id)->bRoboDetectado())
            {
                vt_vt3d.replace(
                    id, QVector3D(opponents->getPlayer(id)->vt2dPosicaoAtual(),
                                  otOponente));
            }
        }
        return vt_vt3d;
    }
    if (otObjeto == otBola)
    {
        if (blBola->bEmCampo)
        {
            QVector<QVector3D> vt_vt3d = QVector<QVector3D>(
                1, QVector3D(blBola->vt2dPosicaoAtual, otBola));
            return vt_vt3d;
        }
        else
        {
            QVector<QVector3D> vt_vt3d = QVector<QVector3D>(
                1, QVector3D(vt2dPosicaoObjetosForaDeCampo, otBola));
            return vt_vt3d;
        }
    }
    return QVector<QVector3D>{};
}

QVector<QVector3D> AmbienteCampo::vt3dPegaDestinosAliados(int iRegiaoInicial,
                                                          int iRegiaoFinal,
                                                          int _iIDRobo) const
{
    QVector<QVector3D> vt3dPosicaoObjetos;
    vt3dPosicaoObjetos.clear();
    int iRegiaoAux = -1;

    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        iRegiaoAux = Auxiliar::iCalculaRegiaoPonto(
            allies->getPlayer(id)->vt2dPosicaoAtual(),
            geoCampo->szField().width(), globalConfig.fieldDivisionWidth);
        if (allies->getPlayer(id)->bRoboDetectado() &&
            (iRegiaoInicial <= iRegiaoAux && iRegiaoAux <= iRegiaoFinal) &&
            allies->getPlayer(id)->iID() != _iIDRobo)
        {
            vt3dPosicaoObjetos.append(
                QVector3D(allies->getPlayer(id)->vt2dDestino(), otAliado));
        }
        if (allies->getPlayer(id)->iID() == _iIDRobo)
        {
            vt3dPosicaoObjetos.append(
                QVector3D(allies->getPlayer(id)->vt2dPosicaoAtual(), otAliado));
        }
    }
    return vt3dPosicaoObjetos;
}

QVector<QVector2D> AmbienteCampo::vt2dPegaPosicaoTodosObjetos(
    TipoObjeto otObjeto, bool _bEnviarGoleiro) const
{
    QVector<QVector3D> vetor3d =
        vt3dPegaPosicaoTodosObjetos(otObjeto, _bEnviarGoleiro);
    QVector<QVector2D> vetor2d(vetor3d.size());

    for (int i = 0; i < vetor3d.size(); ++i)
        vetor2d[i] = vetor3d.at(i).toVector2D();

    return vetor2d;
}

QVector<QVector3D> AmbienteCampo::vt3dPegaPosicaoTodosObjetos(
    TipoObjeto otObjeto, bool _bEnviarGoleiro) const
{
    QVector<QVector3D> vt3dPosicaoObjetos;
    vt3dPosicaoObjetos.clear();

    if (otObjeto == otAliado)
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            if (allies->getPlayer(id)->bRoboDetectado())
            {
                if (allies->getPlayer(id)->iID() == allies->iGetGoalieID() &&
                    _bEnviarGoleiro == true)
                {
                    vt3dPosicaoObjetos.append(QVector3D(
                        allies->getPlayer(id)->vt2dPosicaoAtual(), otAliado));
                }
                else if (allies->getPlayer(id)->iID() != allies->iGetGoalieID())
                {
                    vt3dPosicaoObjetos.append(QVector3D(
                        allies->getPlayer(id)->vt2dPosicaoAtual(), otAliado));
                }
            }
        }
    }
    else if (otObjeto == otOponente)
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            if (opponents->getPlayer(id)->bRoboDetectado())
            {
                if (opponents->getPlayer(id)->iID() ==
                        opponents->iGetGoalieID() &&
                    _bEnviarGoleiro == true)
                {
                    vt3dPosicaoObjetos.append(
                        QVector3D(opponents->getPlayer(id)->vt2dPosicaoAtual(),
                                  otOponente));
                }
                else if (opponents->getPlayer(id)->iID() !=
                         opponents->iGetGoalieID())
                {
                    vt3dPosicaoObjetos.append(
                        QVector3D(opponents->getPlayer(id)->vt2dPosicaoAtual(),
                                  otOponente));
                }
            }
        }
    }
    else if (otObjeto == otBola)
    {
        if (this->blBola->bEmCampo)
            vt3dPosicaoObjetos.append(
                QVector3D(this->blBola->vt2dPosicaoAtual, otBola));
    }

    return vt3dPosicaoObjetos;
}

int AmbienteCampo::iRoboMaisProximo(
    QVector2D _vt2dPonto, const QVector<int>& _iRobosPosicionados) const
{
    int id = -1;
    float menorDist = 9999999.f;

    Robo* n;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        n = allies->getPlayer(id);
        if (_vt2dPonto.distanceToPoint(n->vt2dPosicaoAtual()) < menorDist &&
            n->iID() != allies->iGetGoalieID() &&
            !_iRobosPosicionados.contains(n->iID()) &&
            n->bRoboDetectado() == true)
        {
            id = n->iID();
            menorDist = _vt2dPonto.distanceToPoint(n->vt2dPosicaoAtual());
        }
    }

    return id;
}

int AmbienteCampo::iRoboMaisProximo(
    const QVector<int>& robos, QVector2D _vt2dPonto,
    const QVector<int>& _iRobosPosicionados) const
{
    int id = -1;
    float menorDist = 9999999.f;

    for (int n = 0; n < robos.size(); ++n)
    {
        const Robo* roboAtual = allies->getPlayer(robos.at(n));

        if (_vt2dPonto.distanceToPoint(roboAtual->vt2dPosicaoAtual()) <
                menorDist &&
            roboAtual->iID() != allies->iGetGoalieID() &&
            !_iRobosPosicionados.contains(roboAtual->iID()) &&
            roboAtual->bRoboDetectado())
        {
            id = roboAtual->iID();
            menorDist =
                _vt2dPonto.distanceToPoint(roboAtual->vt2dPosicaoAtual());
        }
    }

    return id;
}

bool AmbienteCampo::bChecaPosicionamentoRobos() const
{
    Robo* n;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        n = allies->getPlayer(id);
        if (n->bRoboDetectado() == true)
        {
            if (n->vt2dPosicaoAtual().distanceToPoint(n->vt2dDestino()) >
                globalConfig.robotDiameter)
                return false;
        }
    }

    return true;
}

bool AmbienteCampo::bChecaPermissaoJogada(int _iIDRobo, ID_Jogadas _jogJogada)
    const // Retorna true se o robo pode executar a jogada
{
    /// \todo Essa função não está mais funcionando, deve ser corrigida.

    /// \todo Quando Essa função é chamda e retorna false, deve ser selecionado
    ///  qualquer outro robô para executar a jogada.

    return true;

    //    if(_iIDRobo != -1)
    //        return bMatrizJogadas[_iIDRobo][_jogJogada];
    //    return false;
}

int AmbienteCampo::iRoboPermitido(ID_Jogadas _jogadaAtual) const
{
    QVector<int> robosExcluidos;
    robosExcluidos.clear();
    robosExcluidos.append(allies->iGetGoalieID());
    int idSelecionado = -1;
    int tentativas = 0;

    while (idSelecionado == -1 && tentativas <= globalConfig.robotsPerTeam)
    {
        idSelecionado =
            iRoboMaisProximo(blBola->vt2dPosicaoAtual, robosExcluidos);
        if (bChecaPermissaoJogada(idSelecionado, _jogadaAtual) == false)
        { // Se o robo nao pode executar a jogada coloca ele na lista de
          // excluidos e procura outro
            robosExcluidos.append(idSelecionado);
            idSelecionado = -1;
        }
        tentativas++;
    }
    if (idSelecionado == -1)
        qDebug() << "Nenhum Robo pode executar a jogada: " << _jogadaAtual;

    return idSelecionado;
}

int AmbienteCampo::iRoboPermitido(ID_Jogadas _jogadaAtual,
                                  const QVector<int>& robosIgnorados) const
{
    QVector<int> robosExcluidos = robosIgnorados;
    qint8 idSelecionado = -1;
    qint8 tentativas = 0;

    while (idSelecionado == -1 && tentativas <= globalConfig.robotsPerTeam)
    {
        idSelecionado =
            iRoboMaisProximo(blBola->vt2dPosicaoAtual, robosExcluidos);
        if (bChecaPermissaoJogada(idSelecionado, _jogadaAtual) == false)
        { // Se o robo nao pode executar a jogada coloca ele na lista de
          // excluidos e procura outro
            robosExcluidos.append(idSelecionado);
            idSelecionado = -1;
        }
        tentativas++;
    }
    if (idSelecionado == -1)
        qDebug() << "Nenhum Robo pode executar a jogada: " << _jogadaAtual;

    return idSelecionado;
}

int AmbienteCampo::iAchaRoboProximoBola(const ID_Jogadas jogadaIgnorada) const
{
    float distRoboBola[globalConfig.robotsPerTeam];
    int iIDRoboMenorDist = -1;

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        if (allies->getPlayer(i)->bRoboDetectado() &&
            allies->getPlayer(i)->jJogadaAtual() != jogadaIgnorada)
        {
            distRoboBola[i] =
                allies->getPlayer(i)->vt2dPosicaoAtual().distanceToPoint(
                    vt2dPosicaoBola());
        }
        else
            distRoboBola[i] = 999999999999999999.f;
    }

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        if (distRoboBola[i] > 999999999999999.f)
            continue;

        if (iIDRoboMenorDist == -1 ||
            distRoboBola[iIDRoboMenorDist] > distRoboBola[i])
        {
            iIDRoboMenorDist = i;
        }
    }

    return iIDRoboMenorDist;
}

int AmbienteCampo::iAchaRoboProximoBola(
    const QVector<qint8>& idsIgnorados) const
{
    float distRoboBola[globalConfig.robotsPerTeam];
    int iIDRoboMenorDist = -1;

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        if (allies->getCPlayer(i)->bRoboDetectado() &&
            !idsIgnorados.contains(allies->getCPlayer(i)->iID()))
        {
            distRoboBola[i] =
                allies->getPlayer(i)->vt2dPosicaoAtual().distanceToPoint(
                    vt2dPosicaoBola());
        }
        else
            distRoboBola[i] = 999999999999999999.f;
    }

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        if (distRoboBola[i] > 999999999999999.f)
            continue;

        if (iIDRoboMenorDist == -1 ||
            distRoboBola[iIDRoboMenorDist] > distRoboBola[i])
        {
            iIDRoboMenorDist = i;
        }
    }

    return iIDRoboMenorDist;
}

qint8 AmbienteCampo::iAchaInimigoProximoBola() const
{
    return iAchaInimigoProximoPonto(blPegaBola().vt2dPosicaoAtual);
}

qint8 AmbienteCampo::iAchaInimigoProximoPonto(
    QVector2D ponto, const QVector<int>& vtiRobosIgnorados) const
{
    QVector<float> distPonto;
    QVector<int> iIDs;
    int iIDRoboMenorDist = 0;
    distPonto.clear();
    iIDs.clear();

    Robo* n;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        n = opponents->getPlayer(id);
        if (n->bRoboDetectado() == true &&
            !vtiRobosIgnorados.contains(n->iID()))
        {
            distPonto.append(ponto.distanceToPoint(n->vt2dPosicaoAtual()));
            iIDs.append(n->iID());
        }
    }
    if (iIDs.size() > 0)
    {
        iIDRoboMenorDist = iIDs.first();

        for (int i = 0; i < distPonto.size() - 1; ++i)
        {
            for (int j = i + 1; j < distPonto.size(); ++j)
            {
                if (iIDs.contains(iIDRoboMenorDist))
                    if (distPonto[iIDs.indexOf(iIDRoboMenorDist)] >
                        distPonto[j])
                        iIDRoboMenorDist = iIDs[j];
            }
        }

        return iIDRoboMenorDist;
    }

    return -1;
}

void AmbienteCampo::vAtualizaDadosVisao(const VisionPacket _packet)
{
    // Atualiza a bola
    blBola.reset(new Bola(*_packet.ball));
    this->vSetaRobosEmCampo(iPegaRobosEmCampo());

    geoCampo.reset(new Geometria(*_packet.fieldGeometry));

    if (allies->teamColor() == timeAzul)
    {
        allies->vUpdateTeamDetection(*_packet.blueTeam);
        opponents->vUpdateTeamDetection(*_packet.yellowTeam);
    }
    else
    {
        allies->vUpdateTeamDetection(*_packet.yellowTeam);
        opponents->vUpdateTeamDetection(*_packet.blueTeam);
    }
}

int AmbienteCampo::iAchaRoboComJogada(ID_Jogadas _Jogada) const
{
    Robo* n;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        n = allies->getPlayer(id);
        // So retorna o goleiro caso a jogada a ser procurada for a de goleiro
        if (n->jJogadaAtual() == _Jogada &&
            (n->iID() != allies->iGetGoalieID() || _Jogada == Robo_Goleiro) &&
            n->bRoboDetectado())
        {
            return n->iID();
        }
    }

    return -1;
}

int AmbienteCampo::iAchaRoboComJogada(ID_Jogadas _jogJogada,
                                      const QVector<int>& robosIgnorados) const
{
    Robo* n;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        n = allies->getPlayer(id);
        if (n->jJogadaAtual() == _jogJogada &&
            n->jJogadaAtual() != Robo_Goleiro && n->bRoboDetectado() == true &&
            robosIgnorados.contains(n->iID()) == false)
            return n->iID();
    }

    return -1;
}

QVector<qint8> AmbienteCampo::vtiRobosComJogada(ID_Jogadas jog) const
{
    QVector<qint8> robos;
    robos.clear();
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        if (allies->getPlayer(id)->jJogadaAtual() == jog)
        {
            robos.append(id);
        }
    }
    return robos;
}

void AmbienteCampo::vAjustaDestinoRoboDefesa(int _iIDRobo)
{
    // Area Quadrada
    if (_iIDRobo != allies->iGetGoalieID() &&
        allies->getPlayer(_iIDRobo)->jJogadaAtual() != Penalty_Cobrador &&
        allies->getPlayer(_iIDRobo)->jJogadaAtual() != BallPlacement_Auxiliar &&
        allies->getPlayer(_iIDRobo)->jJogadaAtual() != BallPlacement_Principal)
    {
        QVector2D vt2dDestino = allies->getPlayer(_iIDRobo)->vt2dDestino();
        QSize campo = geoCampo->szField(),
              areaDefesa = geoCampo->szDefenseArea();

        if ((vt2dDestino.x() < -(campo.width() / 2 - areaDefesa.width())) &&
            vt2dDestino.y() > -areaDefesa.height() / 2 &&
            vt2dDestino.y() < areaDefesa.height() / 2)
        {
            if (vt2dDestino.x() < 0)
                vt2dDestino.setX(-(campo.width() / 2 - areaDefesa.width() -
                                   globalConfig.robotDiameter * 2));
            else if (vt2dDestino.x() > 0)
                vt2dDestino.setX(campo.width() / 2 - areaDefesa.width() -
                                 globalConfig.robotDiameter * 2);

            allies->getPlayer(_iIDRobo)->vSetaDestino(vt2dDestino);
        }

        else if (vt2dDestino.x() > (campo.width() / 2 - areaDefesa.width()) &&
                 vt2dDestino.y() > -areaDefesa.height() / 2 &&
                 vt2dDestino.y() < areaDefesa.height() / 2)
        {
            if (vt2dDestino.x() < 0)
                vt2dDestino.setX(-(campo.width() / 2 - areaDefesa.width() -
                                   globalConfig.robotDiameter * 2));
            else if (vt2dDestino.x() > 0)
                vt2dDestino.setX(campo.width() / 2 - areaDefesa.width() -
                                 globalConfig.robotDiameter * 2);

            allies->getPlayer(_iIDRobo)->vSetaDestino(vt2dDestino);
        }
    }
}

void AmbienteCampo::vAjustaDestinoRoboRestricao(int _iIDRobo, int _ladoInfracao)
{
    QVector2D centroGol,
        posRobo = allies->getPlayer(_iIDRobo)->vt2dPosicaoAtual();

    if (_ladoInfracao == allies->iGetSide())
    {
        centroGol = geoCampo->vt2dGoalCenter(allies->iGetSide());
    }
    else
    {
        centroGol = geoCampo->vt2dGoalCenter(opponents->iGetSide());
    }

    //     do
    //     {
    //     posRobo = Auxiliar::vt2dReajustaPonto(centroGol, posRobo,
    //     posRobo.distanceToPoint(centroGol) + 1e3);
    posRobo = Auxiliar::vt2dReajustaPonto(centroGol, posRobo, 2e3);
    //     }while(geoCampo.bPontoDentroAreaRestricao(posRobo.toPoint(),
    //     _ladoInfracao));

    allies->getPlayer(_iIDRobo)->vSetaDestino(posRobo);
}

bool AmbienteCampo::bBolaDentroAreaDefesa(float fator) const
{
    return bPontoAreaDefesa(blBola->vt2dPosicaoAtual, fator);
}

bool AmbienteCampo::bBolaDentroAreaDefesaOponente(float fator) const
{
    return bBolaDentroAreaDefesa(fator) &&
           allies->iGetSide() * vt2dPosicaoBola().x() < 0;
}

bool AmbienteCampo::bBolaDentroAreaDefesaAliado(float fator) const
{
    return bBolaDentroAreaDefesa(fator) &&
           allies->iGetSide() * vt2dPosicaoBola().x() > 0;
}

bool AmbienteCampo::bPontoDentroAreaDefesaOponente(QVector2D pto,
                                                   float fator) const
{
    return bPontoAreaDefesa(pto, fator) && allies->iGetSide() * pto.x() < 0;
}

bool AmbienteCampo::bPontoDentroAreaDefesaAliado(QVector2D pto,
                                                 float fator) const
{
    return bPontoAreaDefesa(pto, fator) && allies->iGetSide() * pto.x() > 0;
}

bool AmbienteCampo::bGolContra(int _iIDRobo) const
{

    const Bola& blBola = blPegaBola();
    Robo* robo = allies->getPlayer(_iIDRobo);

    float anguloRobo = robo->fAnguloAtual();

    // Converte o angulo atual do robo para ser positivo e entre -180 e 180
    // graus
    if (qAbs(anguloRobo) > 2 * M_PI)
    {
        float fatorConversao =
            anguloRobo / (2 * M_PI) - trunc(anguloRobo / (2 * M_PI));
        anguloRobo = fatorConversao * 2 * M_PI;
    }
    if (anguloRobo < 0)
        anguloRobo += 2 * M_PI;
    if (anguloRobo > M_PI)
        anguloRobo = 2 * M_PI - anguloRobo;
    //

    if (allies->iGetSide() == XNegativo)
    {
        if (qAbs(anguloRobo) > (M_PI - (iAnguloGolContra / 2) * M_PI / 180) &&
            Auxiliar::iCalculaRegiaoPonto(robo->vt2dPosicaoAtual(),
                                          geoCampo->szField().width(),
                                          globalConfig.fieldDivisionWidth) < 3)
        {
            allies->getPlayer(_iIDRobo)->vDesligaChute();
            // qDebug() << "Protecao gol contra para o robo " << atbRobo.id;
            return true;
        }
    }
    else if (allies->iGetSide() == XPositivo)
    {
        const int regiaoPonto = Auxiliar::iCalculaRegiaoPonto(
            robo->vt2dPosicaoAtual(), geoCampo->szField().width(),
            globalConfig.fieldDivisionWidth);
        if (qAbs(anguloRobo) < (iAnguloGolContra / 2) * M_PI / 180 &&
            regiaoPonto >
                geoCampo->szField().width() / globalConfig.fieldDivisionWidth - 2)
        {
            allies->getPlayer(_iIDRobo)->vDesligaChute();
            // qDebug() << "Protecao gol contra para o robo " << atbRobo.id;
            return true;
        }
    }

    if (robo->vt2dPosicaoAtual().distanceToPoint(blBola.vt2dPosicaoAtual) >
        2 * globalConfig.robotDiameter)
    {
        robo->vDesligaChute();
    }

    return false;
}

bool AmbienteCampo::bRoboAreaDefesa(int _iIDRobo, TipoObjeto otTipo) const
{
    Robo* robo;
    int goleiro = 0;
    if (otTipo == otAliado)
    {
        robo = allies->getPlayer(_iIDRobo);
        goleiro = allies->iGetGoalieID();
    }
    else if (otTipo == otOponente)
    {
        robo = opponents->getPlayer(_iIDRobo);
        goleiro = opponents->iGetGoalieID();
    }

    // Area Quadrada
    if (_iIDRobo != goleiro && robo->jJogadaAtual() != Penalty_Cobrador &&
        robo->jJogadaAtual() != BallPlacement_Auxiliar &&
        robo->jJogadaAtual() != BallPlacement_Principal)
    {
        return bPontoAreaDefesa(robo->vt2dPosicaoAtual());
    }

    return false;
}

bool AmbienteCampo::bPontoAreaDefesa(QVector2D pos, float fator) const
{
    // TODO: Mover isso para a Geometria
    const QSize campo = geoCampo->szField(),
                areaDefesa = geoCampo->szDefenseArea();

    return Auxiliar::bPontoDentroArea(campo, areaDefesa * fator,
                                      allies->iGetSide(), pos) ||
           Auxiliar::bPontoDentroArea(campo, areaDefesa * fator,
                                      opponents->iGetSide(), pos);
}

bool AmbienteCampo::bPontoAreaRestricao(QVector2D pos, int ladoCampo) const
{
    // TODO: Mover isso para a Geometria
    return geoCampo->bPontoDentroAreaRestricao(pos.toPoint(), ladoCampo);
}

void AmbienteCampo::vAchaTimeComAPosseDeBola(const QVector2D& posBola,
                                             const qint8 iIDAliado,
                                             const qint8 iIDAdversario)
{

    const float distMenor = 250;
    const float distMaior = 500;

    if (bBolaDentroAreaDefesaAliado())
    {
        pdbTimeComABola = BOLA_ALIADO;
        return;
    }

    if (bBolaDentroAreaDefesaOponente())
    {
        pdbTimeComABola = BOLA_ADVERSARIO;
        return;
    }

    if (iIDAliado == -1)
    {
        pdbTimeComABola = BOLA_ADVERSARIO;
        return;
    }

    if (iIDAdversario == -1)
    {
        pdbTimeComABola = BOLA_ALIADO;
        return;
    }

    const float fDistanciaAliado = allies->getPlayer(iIDAliado)
                                       ->vt2dPosicaoAtual()
                                       .distanceToPoint(posBola),
                fDistanciaAdversario = opponents->getPlayer(iIDAdversario)
                                           ->vt2dPosicaoAtual()
                                           .distanceToPoint(posBola);

    if (fDistanciaAdversario < distMenor && fDistanciaAliado < distMenor)
    {
        pdbTimeComABola = BOLA_EM_DISPUTA;
        return;
    }

    if (fDistanciaAdversario >= distMaior && fDistanciaAliado >= distMaior)
    {
        pdbTimeComABola = BOLA_EM_DISPUTA;
        return;
    }

    if (fDistanciaAdversario < distMenor && fDistanciaAliado >= distMenor)
    {
        pdbTimeComABola = BOLA_ADVERSARIO;
        return;
    }

    if (fDistanciaAliado < distMenor && fDistanciaAdversario >= distMenor)
    {
        pdbTimeComABola = BOLA_ALIADO;
        return;
    }

    if (fDistanciaAdversario < distMaior && fDistanciaAliado >= distMaior)
    {
        pdbTimeComABola = BOLA_ADVERSARIO;
        return;
    }

    if (fDistanciaAliado < distMaior && fDistanciaAdversario >= distMaior)
    {
        pdbTimeComABola = BOLA_ALIADO;
        return;
    }

    pdbTimeComABola = BOLA_EM_DISPUTA;
    return;
}

POSSE_DE_BOLA AmbienteCampo::pdbAchaTimeComPosseDeBola() const
{
    if (bBolaDentroAreaDefesaAliado())
    {
        return BOLA_ALIADO;
    }

    if (bBolaDentroAreaDefesaOponente())
    {
        return BOLA_ADVERSARIO;
    }

    const QVector2D posBola = vt2dPosicaoBola();
    const qint8 idAliado = iAchaRoboProximoBola(QVector<qint8>());
    const qint8 idOponente = iAchaInimigoProximoBola();

    if (idAliado == -1)
    {
        return BOLA_ADVERSARIO;
    }

    if (idOponente == -1)
    {
        return BOLA_ALIADO;
    }

    const float distMenor = 250;
    const float distMaior = 500;

    const float fDistanciaAliado = allies->getPlayer(idAliado)
                                       ->vt2dPosicaoAtual()
                                       .distanceToPoint(posBola),
                fDistanciaAdversario = opponents->getPlayer(idOponente)
                                           ->vt2dPosicaoAtual()
                                           .distanceToPoint(posBola);

    if (fDistanciaAdversario < distMenor && fDistanciaAliado < distMenor)
    {
        return BOLA_EM_DISPUTA;
    }

    if (fDistanciaAdversario >= distMaior && fDistanciaAliado >= distMaior)
    {
        return BOLA_EM_DISPUTA;
    }

    if (fDistanciaAdversario < distMenor && fDistanciaAliado >= distMenor)
    {
        return BOLA_ADVERSARIO;
    }

    if (fDistanciaAliado < distMenor && fDistanciaAdversario >= distMenor)
    {
        return BOLA_ALIADO;
    }

    if (fDistanciaAdversario < distMaior && fDistanciaAliado >= distMaior)
    {
        return BOLA_ADVERSARIO;
    }

    if (fDistanciaAliado < distMaior && fDistanciaAdversario >= distMaior)
    {
        return BOLA_ALIADO;
    }

    return BOLA_EM_DISPUTA;
}

bool AmbienteCampo::bReceptorRecebendoPasse(int ID_Receptor) const
{
    // função usada para verificar se o receptor -- ou algum outro robô -- está
    // na trajetória da bola
    //

    if (ID_Receptor == -1)
        return false;

    if (dVelocidadeBola() < 1.0)
        return false;

    if (allies->getPlayer(ID_Receptor)
            ->vt2dPosicaoAtual()
            .distanceToPoint(vt2dPosicaoBola()) < 250)
        return true;

    return Auxiliar::bRoboNaRetaDaBola(
        allies->getPlayer(ID_Receptor)->vt2dPosicaoAtual(), vt2dPosicaoBola(),
        vt2dVelocidadeBola(), 30);
}

qint8 AmbienteCampo::iReceptorRecebendoPasse() const
{
    float distancia[globalConfig.robotsPerTeam];
    double angulo[globalConfig.robotsPerTeam];

    // Inicialmente pega o ângulo para recebimento da bola e distância de todos
    //  os robôs em campo.
    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        const Robo* r = allies->getPlayer(i);

        if (!r->bRoboDetectado() || r->iID() == allies->iGetGoalieID())
        {
            distancia[i] = 999999999999999999.f;
            angulo[i] = 999999999999999999.f;
            continue;
        }

        distancia[i] = r->vt2dPosicaoAtual().distanceToPoint(vt2dPosicaoBola());
        angulo[i] = fabs(Auxiliar::dAnguloVetor1Vetor2(
            r->vt2dPosicaoAtual() - vt2dPosicaoBola(), vt2dVelocidadeBola()));
    }

    // Faz a pontuação de cada robô
    QList<qint8> candidatos;
    // for (const float angle : {35, 30, 25, 20, 15})
    for (float angle = 35; angle >= 15; angle-= 5)
    {
        for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
        {
            if (angulo[i] < angle)
                candidatos.append(i);
        }
    }

    // Se nenhum robô tem ângulo menor que 30, não há nenhum receptor
    if (candidatos.isEmpty())
        return -1;

    qint8 maxPts = 0;

    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        qint8 pts = candidatos.count(i);
        if (pts > maxPts)
            maxPts = pts;
    }

    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        if (candidatos.count(i) != maxPts)
            candidatos.removeAll(i);
        else
        {
            candidatos.removeAll(i);
            candidatos.append(i);
        }
    }

    // Se apenas um robô tem a pontuação máxima, ele é o receptor
    if (candidatos.size() == 1)
        return candidatos.first();

    float menorDistancia = 99999999999999999999999999.f;
    qint8 idEscolhido = -1;

    // Se hão mais de um receptores para receber a bola,
    //  aquele mais próximo da mesma é escolhido como receptor
    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        if (!candidatos.contains(i))
            continue;

        if (distancia[i] < menorDistancia)
        {
            idEscolhido = i;
            menorDistancia = distancia[i];
        }
    }
    return idEscolhido;
}

void AmbienteCampo::vAtualizaPosicaoDesignada(QVector2D posDesignada)
{
    this->vt2dPosicaoBallPlacement = posDesignada;
}

bool AmbienteCampo::bRoboEstaComABola(qint8 id, bool usarDistancia) const
{

    if (id == -1)
        return false;

    if (allies->getCPlayer(id)->bKickSensor())
        return true;

    /// TODO: Se já esta usando uma constante para que somar +10? Isso só
    /// distorce as definições
    if (bDistanciaVerificaRoboComBola || usarDistancia)
    {
        float distRobotBall =
            allies->getPlayer(id)->vt2dPosicaoAtual().distanceToPoint(
                vt2dPosicaoBola());
        return distRobotBall < (globalConfig.robotDistanceToBall + 10);
    }

    return false;
}

bool AmbienteCampo::bChuteAoGolAliadoEmAndamento() const
{
    if (dVelocidadeBola() < 2)
        return false;

    QVector2D pontoCentroGolAdversario =
        geoCampo->vt2dGoalCenter(opponents->iGetSide());

    for (quint8 i = 0; i < 3; i++)
    {

        QVector2D pontoCentroDaRegiao =
            QVector2D(pontoCentroGolAdversario.x(),
                      (i - 1) * geoCampo->szGoal().height() / 2);

        if (Auxiliar::bRoboNaRetaDaBola(pontoCentroDaRegiao, vt2dPosicaoBola(),
                                        vt2dVelocidadeBola()))
            return true;
    }

    return false;
}

bool AmbienteCampo::bRoboForaDoCampo(qint8 id) const
{
    if (!allies->getPlayer(id)->bRoboDetectado())
        return false;

    return Auxiliar::bPontoForaCampo(geoCampo->szField(),
                                     allies->getPlayer(id)->vt2dPosicaoAtual(),
                                     globalConfig.robotDiameter);
}

bool AmbienteCampo::ballOutOfField() const
{
    return Auxiliar::bPontoForaCampo(geoCampo->szField(), vt2dPosicaoBola(),
                                     globalConfig.robotDiameter);
}

QVector<int> AmbienteCampo::vtiRobosAliadosNaoDetectadosPelaVisao() const
{
    QVector<int> vti = QVector<int>();

    Robo* r;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        r = allies->getPlayer(id);
        if (!r->bRoboDetectado())
            vti.append(r->iID());
    }

    return vti;
}

QVector<int> AmbienteCampo::vtiReceptoresProibidos(
    const QVector2D vt2dposicaoPassador, const float fDistanciaMinimaPasse,
    const float fDistanciaMaximaPasse, const qint8 idPassador) const
{
    QSet<int> setiIdsIgnorados;

    for (const int id : vtiRobosAliadosNaoDetectadosPelaVisao())
    {
        setiIdsIgnorados.insert(id);
    }

    setiIdsIgnorados.insert(allies->iGetGoalieID());

    setiIdsIgnorados.insert(idPassador);

    const REGIAO_CAMPO regiaoCampoBola = regiaoCampoPonto(vt2dPosicaoBola());

    Robo* rbt;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        rbt = allies->getPlayer(id);
        if (setiIdsIgnorados.contains(rbt->iID()))
            continue;

        if (rbt->fTempoDesdeUltimoChute() < iTempoRecargaChute * .75f &&
            !setiIdsIgnorados.contains(rbt->iID()))
        {
            setiIdsIgnorados.insert(rbt->iID());
        }

        // Ignora robôs próximos da bola
        else if (rbt->vt2dPosicaoAtual().distanceToPoint(vt2dposicaoPassador) <
                     fDistanciaMinimaPasse &&
                 !setiIdsIgnorados.contains(rbt->iID()))
        {
            setiIdsIgnorados.insert(rbt->iID());
        }

        // Ignora robôs muito distantes da bola
        else if (rbt->vt2dPosicaoAtual().distanceToPoint(vt2dposicaoPassador) >
                     fDistanciaMaximaPasse &&
                 !setiIdsIgnorados.contains(rbt->iID()))
        {
            setiIdsIgnorados.insert(rbt->iID());
        }

        // Evita passar para aliados próximos ao gol aliado
        else if (rbt->vt2dPosicaoAtual().distanceToPoint(
                     geoCampo->vt2dGoalCenter(allies->iGetSide())) <
                     fDistanciaGolAliadoEvitar &&
                 !setiIdsIgnorados.contains(rbt->iID()))
        {
            setiIdsIgnorados.insert(rbt->iID());
        }

        // Evita o robô passar a bola para outro robô em uma região do campo
        // mais defensiva
        else if (Auxiliar::regiaoCampoPonto(
                     rbt->vt2dPosicaoAtual(), allies->iGetSide(),
                     geoCampo->szField(),
                     geoCampo->szDefenseArea()) < regiaoCampoBola)
        {
            setiIdsIgnorados.insert(rbt->iID());
        }
    }

    return setiIdsIgnorados.values().toVector();
}

void AmbienteCampo::vAtribuiDestinosOtimizados(const QVector<qint8>& idsRobos,
                                               QVector<QVector2D>& destinos)
{
    QMap<qint8, QVector2D> solution =
        mapGetBestGoalAssignment(idsRobos, destinos);

    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        if (solution.contains(id))
        {
            allies->getPlayer(id)->vSetaDestino(solution.value(id));
        }
    }
}

QMap<qint8, QVector2D> AmbienteCampo::mapGetBestGoalAssignment(
    const QVector<qint8>& _robots, QVector<QVector2D>& _goals) const
{
    HungarianAlgorithm hungSolver(_robots.size());

    if (_goals.size() > _robots.size())
    {
        // qWarning() << "Quantidade de destinos maior que o número de robôs";

        while (_goals.size() > _robots.size())
        {
            _goals.pop_back();
        }
    }

    int goalID;
    float distToGoal = 0;
    for (int n = 0; n < _robots.size(); ++n)
    {
        goalID = 0;
        for (QVector2D goalPos : qAsConst(_goals))
        {
            distToGoal = goalPos.distanceToPoint(
                allies->getCPlayer(_robots.at(n))->vt2dPosicaoAtual());
            hungSolver.vSetMatrixValue(n, goalID, distToGoal);
            goalID++;
        }
    }

    QVector<Assignment> solution = hungSolver.runAlgorithm();
    QMap<qint8, QVector2D> mapSolution;
    mapSolution.clear();

    for (int i = 0; i < solution.size(); ++i)
    {
        if (solution.at(i).robot < _robots.size() &&
            solution.at(i).task < _goals.size())
        {
            mapSolution.insert(_robots.at(solution.at(i).robot),
                               _goals.at(solution.at(i).task));
        }
    }
    return mapSolution;
}

REGIAO_CAMPO AmbienteCampo::regiaoCampoPonto(const QVector2D pto) const
{
    return Auxiliar::regiaoCampoPonto(pto, allies->iGetSide(),
                                      geoCampo->szField(),
                                      geoCampo->szDefenseArea());
}

QVector2D AmbienteCampo::vt2dCoordenadaGenericaParaCampo(QVector2D gen) const
{
    return Auxiliar::vt2dCoordenadasGenericasParaCoordenadasCampo(
        gen, allies->iGetSide(), geoCampo->szField(),
        geoCampo->szDefenseArea());
}

QVector2D AmbienteCampo::vt2dCoordenadaCampoParaGenerica(
    QVector2D cordCampo) const
{
    return Auxiliar::vt2dCoordenadasCampoParaCoordenadasGenericas(
        cordCampo, allies->iGetSide(), geoCampo->szField(),
        geoCampo->szDefenseArea());
}

QVector2D AmbienteCampo::vt2dAllyPosition(qint8 id) const
{
    return allies->getCPlayer(id)->vt2dPosicaoAtual();
}

QVector2D AmbienteCampo::vt2dOppPosition(qint8 id) const
{
    return opponents->getCPlayer(id)->vt2dPosicaoAtual();
}

void AmbienteCampo::vSetRemainingPlayTime(int _time)
{
    iRemainingPlayTime = _time;
}

int AmbienteCampo::iGetRemainingPlayTime() const
{
    return iRemainingPlayTime;
}
