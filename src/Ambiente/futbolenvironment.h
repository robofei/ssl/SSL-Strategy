﻿/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FUTBOLENVIRONMENT_H
#define FUTBOLENVIRONMENT_H

///
/// \file futbolenvironment.h
/// \brief \a AmbienteCampo, \a RGB, e \a DADOS_TESTES
/// \details Detalhes
///

#include <QPair>

#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Robo/robot.h"
#include "geometria.h"

#include "Ambiente/sslteam.h"
#include "Debug/debuginfo.h"
#include "Referee/refereereceiver.h"

#include "Visao/visionpacket.h"
#include "qmap.h"
#include "qscopedpointer.h"

/**
 * @brief Estrutura utilizada para transmitir os dados dos testes.
 * @see TestesRobos.
 */
typedef struct DADOS_TESTES
{
    ID_Testes tstTesteAtual; /**< Função de teste atual */

    QVector2D
        vt2dDestinoTesteRobo; /**< Destino do robô no teste dos Path-Planners */
    QVector2D vt2dPosicaoInicialTeste; /**< Posição inicial do robô nos teste
                                          dos Path-Planners. */
    int iIDRoboTeste;          /**< ID do robô sendo testado, caso o teste seja
                                                           para um único robô. */
    float fDistanciaAvancoRRT; /**< Parâmetro do RRT durante o teste.
                                    @see RRT. */
    int iNumeroIteracoesRRT;   /**< Parâmetro do RRT durante o teste.
                                    @see RRT. */
    int iDistanciaMinimaRRT;   /**< Parâmetro do RRT durante o teste.
                                    @see RRT. */
    int iLeafSizeRRT;          /**< Parâmetro do RRT durante o teste.
                                    @see RRT. */
    int iPathPlannerUtilizado; /**< Indica qual o pathplanner deve ser utilizado
                                          para movimentar o robô durante o
                                  teste.
                                    - RRT  = 1.
                                    - AStar_VG = 2. */
    int iTipoTestePathPlanner; /**< Indica qual o teste feito pelo path-planner.
                                 - Teste 1;
                                 - Teste 2;
                                 - Teste 3. */
    bool bMovimentarRobos;     /**< Indica se é para movimentar o(s) robôs. */
    QList<QVector<int>>
        iTrajetosRobosMouse; /**< Lista que contém os trajetos de teste
                                 de todos os robôs (aliados ou oponentes).
Os trajetos são salvos da seguinte forma:
- ID Robô, Time, nPontos. Onde, cada ponto ocupa 2 espaços (X,Y).
Portanto, ao ler um trajeto, devemos retirar 2, para desconsiderar o ID e o
time, e dividir por 2, para saber o numero total de pontos.*/

    DADOS_TESTES()
    {
        tstTesteAtual = tstNenhum;
        vt2dDestinoTesteRobo = QVector2D(0, 0);
        vt2dPosicaoInicialTeste = QVector2D(0, 0);
        iIDRoboTeste = 0;
        bMovimentarRobos = false;
        fDistanciaAvancoRRT = 360;
        iNumeroIteracoesRRT = 200;
        iDistanciaMinimaRRT = 360;
        iLeafSizeRRT = 500;
        iPathPlannerUtilizado = 2;
        iTipoTestePathPlanner = 1;
    }

} DADOS_TESTES;

/**
 * @brief Estrutura que contém todos os componentes do ambiente de jogo, i.e.,
 * robôs, robôs adversários, bola, dimensões do campo, estado do jogo, quem está
 * com a bola e outros.
 *
 */
class AmbienteCampo
{

public:
    /**
     * @brief Construtor da classe.
     *
     */
    AmbienteCampo();

    /**
     * @brief Construtor por cópia da classe.
     *
     * @param _acAmbiente
     */
    AmbienteCampo(const AmbienteCampo& _acAmbiente);

    // AmbienteCampo &  operator= ( const AmbienteCampo &_acAmbiente);

    /**
     * @brief Destrutor da classe.
     *
     */
    ~AmbienteCampo();

    /**
     * @brief Seta o objeto da bola.
     *
     * @param _bola - Objeto que será atribuído.
     */
    void vSetaBola(const Bola& _bola);

    /**
     * @brief Retorna a posição atual da bola [mm].
     *
     * @return QVector2D - Posição atual da bola.
     */
    QVector2D vt2dPosicaoBola() const;

    /**
     * @brief Retorna a velocidade vetorial da bola [m/s].
     *
     * @return QVector2D - Velocidade vetorial da bola.
     */
    QVector2D vt2dVelocidadeBola() const;

    /**
     * @brief Retorna a velocidade escalar da bola [m/s].
     *
     * @return double - Velocidade escalar da bola.
     */
    double dVelocidadeBola() const;

    /**
     * @brief Reseta (Atribui Nenhuma) a jogada de todos os jogadores caso o
     * parâmetros seja True, senão, não reseta de quem está recebendo um passe
     * ou cobrando um kick off ou penalti.
     * @param todos - Determina se todos os robôs terão sua jogada resetada.
     */
    void vResetaTodasJogadas(bool todos = false);

    /**
     * @brief Retorna o objeto da bola.
     *
     * @return Bola - Objeto da bola.
     */
    const Bola& blPegaBola() const;

    /**
     * @brief Seta quantos robôs aliados estão sendo detectados no campo.
     *
     * @param n - Número de robôs aliados detectados.
     */
    void vSetaRobosEmCampo(int n);

    /**
     * @brief Retorna a quantidade de robôs aliados em campo.
     *
     * @return int - Quantidade de robôs aliados em campo.
     */
    int iPegaRobosEmCampo() const;

    /**
     * @brief Retorna a posição e tipo de todos os objetos de acordo com os
     * parâmetros fornecidos.
     *
     * @param otObjeto - Tipo do objeto, pode ser, otOponente(0), otAliado(1) ou
     * otBola(2).
     * @param _bEnviarGoleiro - Deve ser True para enviar o goleiro também.
     * @return QVector<QVector3D> - Posição X,Y dos objetos e tipo do objeto na
     * coordenada Z.
     */
    QVector<QVector3D> vt3dPegaPosicaoTodosObjetos(
        TipoObjeto otObjeto, bool _bEnviarGoleiro = true) const;

    /**
     * @brief Retorna a posição e tipo de todos os objetos de acordo com os
     * parâmetros fornecidos.
     *
     * @param otObjeto - Tipo do objeto, pode ser, otOponente(0), otAliado(1) ou
     * otBola(2).
     * @param _bEnviarGoleiro - Deve ser True para enviar o goleiro também.
     * @param _iIDIgnorado - Vetor de IDs a serem ignorados.
     * @return QVector<QVector3D> - Posição X,Y dos objetos e tipo do objeto na
     * coordenada Z.
     */
    QVector<QVector3D> vt3dPegaPosicaoTodosObjetos(
        TipoObjeto otObjeto, bool _bEnviarGoleiro,
        const QVector<int>& _iIDIgnorado) const;

    /**
     * @brief Retorna a posição e tipo de todos os objetos de acordo com os
     * parâmetros fornecidos.
     *
     * @param otObjeto  - Tipo do objeto, pode ser, otOponente(0), otAliado(1)
     * ou otBola(2).
     * @param iRegiaoInicial - Região inicial em que os objetos devem estar
     * contidos (linhas tracejadas no desenho do campo).
     * @param iRegiaoFinal - Região final em que os objetos devem estar contidos
     * (linhas tracejadas no desenho do campo).
     * @param _iIDIgnorado - ID do robô a ser ignorado.
     * @return QVector<QVector3D> - Posição X,Y dos objetos e tipo do objeto na
     * coordenada Z.
     */
    QVector<QVector3D> vt3dPegaPosicaoTodosObjetos(TipoObjeto otObjeto,
                                                   int iRegiaoInicial,
                                                   int iRegiaoFinal,
                                                   int _iIDIgnorado = -1) const;

    /**
     * @brief Retorna a posição de todos os objetos de um tipo sem exceções
     *
     * @param otObjeto - Tipo do objeto, pode ser #otOponente, #otAliado, ou
     * #otBola
     * @param vt2dPosicaoObjetosForaDeCampo - Posição considerada como fora do
     * campo
     *
     * @return QVector<QVector3D> - Posição X, Y dos objeto e tipo do objeto na
     * coordenada Z
     */
    QVector<QVector3D> vt_vt3dPegaPosicaoDeTodosOsObjetosSemExcecao(
        const TipoObjeto otObjeto,
        QVector2D vt2dPosicaoObjetosForaDeCampo = QVector2D(0, 1e38)) const;
    /**
     * @brief Retorna o destino de todos os aliados. *Esta função provavelmente
     * será removida*.
     *
     * @param iRegiaoInicial - Região inicial em que o robô deve estar contido
     * (linhas tracejadas no desenho do campo).
     * @param iRegiaoFinal - Região final em que o robô deve estar contido
     * (linhas tracejadas no desenho do campo).
     * @param _iIDRobo - O robô com este ID terá sua posição atual retornada ao
     * invés do destino.
     * @return QVector<QVector3D> Destino X,Y dos robôs e tipo do objeto na
     * coordenada Z.
     */
    QVector<QVector3D> vt3dPegaDestinosAliados(int iRegiaoInicial,
                                               int iRegiaoFinal,
                                               int _iIDRobo) const;

    /**
     * @brief Retorna somente a posição de todos os objetos de acordo com os
     * parâmetros fornecidos.
     *
     * @param otObjeto - Tipo do objeto, pode ser, otOponente(0), otAliado(1) ou
     * otBola(2).
     * @param _bEnviarGoleiro - Deve ser True para enviar o goleiro também.
     * @return QVector<QVector2D> - Posição X,Y de todos os objetos.
     */
    QVector<QVector2D> vt2dPegaPosicaoTodosObjetos(
        TipoObjeto otObjeto, bool _bEnviarGoleiro = true) const;

    /**
     * @brief Retorna o ID do robô mais próximo do ponto fornecido.
     *
     * @param _vt2dPonto - Ponto.
     * @param _iRobosPosicionados - Vetor de IDs dos robôs que serão ignorados.
     * @return int - ID do robô mais próximo do ponto. Pode ser -1 se nenhum
     * robô estiver próximo.
     */
    int iRoboMaisProximo(
        QVector2D _vt2dPonto,
        const QVector<int>& _iRobosPosicionados = QVector<int>()) const;

    /**
     * @brief Retorna o ID do robô mais próximo do ponto fornecido.
     *
     * @param robos - Vetor com os IDs dos robôs que devem ser levados em conta.
     * @param _vt2dPonto - Ponto.
     * @param _iRobosPosicionados - Vetor com os IDs dos robôs que serão
     * ignorados.
     * @return int - ID do robô mais próximo do ponto. Pode ser -1 se nenhum
     * robô estiver próximo.
     */
    int iRoboMaisProximo(const QVector<int>& robos, QVector2D _vt2dPonto,
                         const QVector<int>& _iRobosPosicionados) const;

    /**
     * @brief Retorna true se todos os robôs estão à uma distância menor que um
     * diâmetro de seu respectivo destino.
     *
     * @return bool - True se todos os robôs estão posicionados, senão, false.
     */
    bool bChecaPosicionamentoRobos() const;

    /**
     * @brief Checa se um determinado robô pode executar uma jogada.
     *
     * @param _iIDRobo - ID do robô a ser checado.
     * @param _jogJogada - Jogada.
     * @return bool - True se o robô estiver permitido a realizar a jogada,
     * senão, false.
     */
    bool bChecaPermissaoJogada(int _iIDRobo, ID_Jogadas _jogJogada) const;

    /**
     * @brief Retorna o robô mais próximo da bola que pode executar a jogada
     * fornecida, com exceção do goleiro.
     *
     * @param _jogadaAtual - Jogada.
     * @return int - ID do robô permitido para realizar a jogada. Pode ser -1 se
     * não houver robô permitido para realizar a jogada.
     */
    int iRoboPermitido(ID_Jogadas _jogadaAtual) const;
    /**
     * @brief Retorna o robô mais próximo da bola que pode executar a jogada
     * fornecida, com exceção do goleiro.
     *
     * @param _jogadaAtual - Jogada.
     * @param robosIgnorados - Vetor com os IDs dos robôs que devem ser
     * ignorados.
     * @return int - ID do robô permitido para realizar a jogada. Pode ser -1 se
     * não houver robô permitido para realizar a jogada.
     */
    int iRoboPermitido(ID_Jogadas _jogadaAtual,
                       const QVector<int>& robosIgnorados) const;

    //==========================================================================================================================================

    /**
     * @brief Retorna od ID do robô aliado mais próximo da bola.
     *
     * @return int - ID do robô aliado mais próximo da bola. Pode ser -1 caso
     * nenhum robô esteja próximo.
     */
    Q_DECL_DEPRECATED
    int iAchaRoboProximoBola(
        const ID_Jogadas jogadaIgnorada = Jogada_Invalida) const;

    int iAchaRoboProximoBola(const QVector<qint8>& idsIgnorados) const;

    /**
     * @brief Retorna o ID do robô oponente mais próximo da bola.
     *
     * @return qint8 - ID do robô oponente mais próximo da bola. Pode ser -1
     * caso nenhum robô esteja próximo.
     */
    qint8 iAchaInimigoProximoBola() const;

    /**
     * @brief Retorna o ID do primeiro robô em campo que não é o goleiro e
     * possui a jogada fornecida.
     *
     * @param _Jogada - Jogada.
     * @return int - ID do primeiro robô com a jogada fornecida. Pode ser -1
     * caso nenhum robô possua esta jogada.
     */
    int iAchaRoboComJogada(ID_Jogadas _Jogada) const;

    /**
     * @brief Retorna o ID do primeiro robô em campo que não é o goleiro e
     * possui a jogada fornecida.
     *
     * @param _jogJogada - Jogada.
     * @param robosIgnorados - Vetor com os IDs dos robôs que devem ser
     * ignorados.
     * @return int - ID do primeiro robô com a jogada fornecida. Pode ser -1
     * caso nenhum robô possua esta jogada.
     */
    int iAchaRoboComJogada(ID_Jogadas _jogJogada,
                           const QVector<int>& robosIgnorados) const;

    /**
     * @brief Retorna um vetor com os IDs de todos os robôs com a jogada
     * fornecida.
     * @param jog - Jogada.
     * @return  QVector<int> - Vetor com os IDs dos robôs com a jogada
     * fornecida.
     */
    QVector<qint8> vtiRobosComJogada(ID_Jogadas jog) const;

    /**
     * @brief Ajusta o destino do robô para fora da área de defesa caso ele não
     * seja o goleiro e não esteja cobrando um penalti.
     *
     * @param _iIDRobo - ID do robô a ter o destino ajustado.
     */
    void vAjustaDestinoRoboDefesa(int _iIDRobo);

    /**
     * @brief Ajusta o destino do robô para fora da área de restrição.
     *
     * @param _iIDRobo - ID do robô a ter o destino ajustado.
     * @param _ladoInfracao - Lado em que a infração está sendo feita.
     */
    void vAjustaDestinoRoboRestricao(int _iIDRobo, int _ladoInfracao);

    /**
     * @brief Retorna `true` se a bola estiver dentro da área de defesa aliado
     * ou oponente.
     *
     * @param fator - Fator de aumento da área de defesa. Se for maior que 1 a
     * área será aumentada antes de realizar a checagem. Por exemplo, 1.5 =
     * aumento de 50%.
     * @return bool - True se a bola estiver dentro da área de defesa, senão,
     * false.
     */
    bool bBolaDentroAreaDefesa(float fator = 1) const;

    bool bBolaDentroAreaDefesaOponente(float fator = 1) const;
    bool bBolaDentroAreaDefesaAliado(float fator = 1) const;

    bool bPontoDentroAreaDefesaOponente(QVector2D pto, float fator = 1) const;
    bool bPontoDentroAreaDefesaAliado(QVector2D pto, float fator = 1) const;

    /**
     * @brief Retorna true se o robô estiver virado para o lado aliado num
     * ângulo menor que o limite, e, cancela o chute caso ele esteja.
     *
     * @param _iIDRobo - ID do robô a ser testado.
     * @return bool - True se o robô estiver virado para o lado aliado num
     * ângulo menor que o limite, senão, false.
     */
    bool bGolContra(int _iIDRobo) const; // Evita gol contra

    /**
     * @brief Retorna true se o robô estiver dentro de qualquer área de defesa e
     * não for o goleiro.
     *
     * @param _iIDRobo - ID do robô a ser testado.
     * @param otTipo - Tipo do robô, otOponente ou otAliado.
     * @return bool - True se o robô estiver dentro da área de defesa, senão,
     * false.
     */
    bool bRoboAreaDefesa(int _iIDRobo, TipoObjeto otTipo = otAliado) const;

    /**
     * @brief Retorna true se o ponto fornecido estiver dentro da área de
     * defesa.
     *
     * @param pos - Ponto.
     * @return bool - True se o ponto estiver dentro da área de defesa, senão,
     * false.
     */
    bool bPontoAreaDefesa(QVector2D pos, float fator = 1) const;

    /**
     * @brief Retorna true se o ponto fornecido estiver dentro da área de
     * restrição.
     *
     * @param pos - Ponto.
     * @param ladoCampo - Lado do campo a ser checado.
     * @return bool - True se o ponto estiver dentro da área de restrição,
     * senão, false.
     */
    bool bPontoAreaRestricao(QVector2D pos, int ladoCampo) const;

    /**
     * @brief Retorna o ID do robô oponente mais próximo do ponto fornecido.
     * @param ponto - Ponto.
     * @return int - ID do robô oponente mais próximo do ponto. Pode ser -1 se
     * não tiver ningúem.
     */
    qint8 iAchaInimigoProximoPonto(
        QVector2D ponto,
        const QVector<int>& vtiRobosIgnorados = QVector<int>()) const;

    /** @brief Atualiza os dados que a thread da Visão produziu.
     *
     * Que no geral são, posições filtradas do robôs aliados, oponentes e bola,
     * e dados da geometria.
     *
     * @param _packet - Objeto do que possui esses dados.
     */
    void vAtualizaDadosVisao(const VisionPacket _packet);

    /// \brief Verifica posse de bola e altera a variável
    /// AmbienteCampo::pdbTimeComABola
    ///
    /// \param posBola - Posição da bola
    /// \param iIDAliado - ID do robô aliado mais próximo da bola
    /// \param posAdversario - Posição do adversário mais próximo da bola
    Q_DECL_DEPRECATED_X(
        "Use AmbienteCampo::pdbAchaTimeComPosseDeBola() no lugar.")
    void vAchaTimeComAPosseDeBola(const QVector2D& posBola,
                                  const qint8 iIDAliado,
                                  const qint8 iIDAdversario);

    POSSE_DE_BOLA pdbAchaTimeComPosseDeBola() const;

    ///
    /// \brief Função usada para verificar se o Robô está na trajetória da bola
    /// \param ID_Receptor - ID do robô analisado
    ///
    bool bReceptorRecebendoPasse(int ID_Receptor) const;

    qint8 iReceptorRecebendoPasse() const;

    /// \brief Função utilizada para atualizar a posição designada para o Ball
    /// Placement, fornecida pelo Referee \param posDesignada - Nova posição
    /// designada
    ///
    void vAtualizaPosicaoDesignada(QVector2D posDesignada);

    /// \brief Verifica se o robô está com a bola dominada (encostada no roller)
    /// \param id - ID do robô analisado
    ///
    bool bRoboEstaComABola(qint8 id, bool usarDistancia = false) const;

    ///
    /// \brief Função utilizada para verificar a ocorrência da jogada de chute a
    /// gol
    ///
    bool bChuteAoGolAliadoEmAndamento() const;

    ///
    /// \brief Verifica se o robô está fora do campo.
    /// \note se o robô está sendo detectado pela visão, porém está na região do
    /// offset, este é considerado fora de campo. \param id - ID do rob \return
    /// true se o robô está fora de campo.
    ///
    bool bRoboForaDoCampo(qint8 id) const;
    bool ballOutOfField() const;

    /**
     * @brief Retorna os IDs dos robôs não detectados pela visão
     *
     * @return QVector<int> - Vetor com os IDs
     */
    QVector<int> vtiRobosAliadosNaoDetectadosPelaVisao() const;

    ///
    /// \brief Retorna vetor com os ids dos robôs que não podem ser receptores
    /// \details Ignora robôs no delta, goleiro, robôs não detectados pela
    /// visão, robôs que acabaram de chutar
    ///  e robôs muito próximos da bola.
    /// \param vt2dposicaoPassador - Posição do robô com a bola
    /// \param fDistanceMinimaPasse - Distância mínima para realizar um passe,
    /// usado para ignorar robôs próximos \param idPassador - ID do passador
    /// \return
    ///
    QVector<int> vtiReceptoresProibidos(const QVector2D vt2dposicaoPassador,
                                        const float fDistanciaMinimaPasse,
                                        const float fDistanciaMaximaPasse,
                                        const qint8 idPassador) const;

    void vAtribuiDestinosOtimizados(const QVector<qint8>& idsRobos,
                                    QVector<QVector2D>& destinos);

    QMap<qint8, QVector2D> mapGetBestGoalAssignment(
        const QVector<qint8>& _robots, QVector<QVector2D>& _goals) const;

    ///
    /// \brief Determina a região de campo do ponto `pto`.
    /// \param pto
    /// \see Auxiliar::regiaoCampoPonto
    ///
    REGIAO_CAMPO regiaoCampoPonto(const QVector2D pto) const;

    QVector2D vt2dCoordenadaGenericaParaCampo(QVector2D gen) const;
    QVector2D vt2dCoordenadaCampoParaGenerica(QVector2D cordCampo) const;
    QVector2D vt2dAllyPosition(qint8 id) const;
    QVector2D vt2dOppPosition(qint8 id) const;
    void vSetRemainingPlayTime(int _time);
    int iGetRemainingPlayTime() const;

private:
    int iRobosEmCampo; /**< Indica quantos robôs aliados estão sendo vistos no
                          campo. */
    int iRemainingPlayTime; /**< Tempo restante para a execução da Play atual
                               [ms]. */

public:
    QScopedPointer<SSLTeam<Robo>> allies;
    QScopedPointer<SSLTeam<Robo>> opponents;
    QScopedPointer<Bola> blBola; /**< Objeto da bola. */
    QScopedPointer<Geometria> geoCampo;
    QScopedPointer<DebugInfo> debugInfo;

    QVector<QVector3D>
        vt3dPosicaoSimulacaoRobo; /**< Posições simuladas do robô [mm]. */
    QVector<QVector3D> vt3dVelocidadeSimulacaoRobo; /**< Velocidades simuladas
                                                       do robô [m/s]. */

    QVector2D vt2dPosicaoTimeout;       /**< Posição inicial do timeout.
                                              A partir deste ponto que os robôs irão
                                           se posicionar [mm]. */
    QVector2D vt2dPosicaoBallPlacement; /**< Posição designada para o Ball
                                           Placement [mm].*/

    bool bTipoAmbiente; /**< Tipo do ambiente de jogo, pode ser o #CAMPO_REAL ou
                           o #GR_SIM. */
    bool bMatrizJogadas[MAX_IDS]
                       [iJogadasAtaque]; /**< Matriz booleana que
                                  permite/restringe a permissão de um robô
                                  executar determinada jogada. */

    SSL_Referee_Command
        sslrefComandoRefereeAtual; /**< Comando atual do referee. */

    DADOS_TESTES tsDadosTeste; /**< Dados dos testes. */

    POSSE_DE_BOLA pdbTimeComABola; /**< O valor dessa variável mostra o time com
                                      a posse de bola*/
    QVector2D vt2dPosicaoDoBancoDeReservas; /// <BETA
};

Q_DECLARE_METATYPE(AmbienteCampo)
Q_DECLARE_METATYPE(DADOS_TESTES)

#endif // FUTBOLENVIRONMENT_H
