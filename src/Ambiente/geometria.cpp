#include "geometria.h"

Geometria::Geometria()
{
    szCampo = QSize(0, 0);
    vSetFieldSize(szCampo);
    szGol = QSize(0, 0);
    szAreaDefesa = QSize(0, 0);
    vUpdateRestrictionArea();

    iRaioCirculoCentro = 0;
}

Geometria::Geometria(const Geometria& _other)
{
    szAreaDefesa = _other.szDefenseArea();
    szCampo = _other.szField();
    vSetFieldSize(szCampo);
    szGol = _other.szGoal();
    iRaioCirculoCentro = _other.iCenterCircleRadius();

    vUpdateRestrictionArea();
}

void Geometria::vSetFieldSize(QSize _size)
{
    szCampo = _size;
    retField = QRect(-szCampo.width() / 2, szCampo.height() / 2,
                     szCampo.width(), -szCampo.height());
}

void Geometria::vSetDefenseAreaSize(QSize _size)
{
    szAreaDefesa = _size;
    vUpdateRestrictionArea();
}

void Geometria::vSetGoalSize(QSize _size)
{
    szGol = _size;
}

void Geometria::vSetCenterCircleSize(int _size)
{
    iRaioCirculoCentro = _size;
}

QSize Geometria::szField() const
{
    return szCampo;
}

QSize Geometria::szDefenseArea() const
{
    return szAreaDefesa;
}

QSize Geometria::szGoal() const
{
    return szGol;
}

int Geometria::iCenterCircleRadius() const
{
    return iRaioCirculoCentro;
}

bool Geometria::bPontoDentroAreaRestricao(QPoint ponto, int ladoCampo) const
{
    QRect area;
    if (ladoCampo == XNegativo)
    {
        area = retRestricaoNegativa;
    }
    else
    {
        area = retRestricaoPositiva;
    }

    return area.contains(ponto);
}

bool Geometria::bIsInsideField(QVector2D _point)
{
    return retField.contains(_point.toPoint());
}

QVector2D Geometria::vt2dGoalCenter(int _side) const
{
    QVector2D center(0, 0);
    center.setX(_side * szCampo.width() / 2.0);
    return center;
}

QRect Geometria::retPositiveRestriction() const
{
    return retRestricaoPositiva;
}

QRect Geometria::retNegativeRestriction() const
{
    return retRestricaoNegativa;
}

QRect Geometria::retRestriction(int _side) const
{
    if (_side == XPositivo)
    {
        return retPositiveRestriction();
    }
    return retNegativeRestriction();
}

void Geometria::vUpdateRestrictionArea()
{
    QSize negativeSize, positiveSize;
#if QT_VERSION > QT_VERSION_CHECK(5, 14, 0)
    negativeSize =
        szAreaDefesa.grownBy(QMargins(0, globalConfig.distanceToDefenseArea,
                                      globalConfig.distanceToDefenseArea,
                                      globalConfig.distanceToDefenseArea));

    positiveSize = szAreaDefesa.grownBy(QMargins(
        globalConfig.distanceToDefenseArea, globalConfig.distanceToDefenseArea,
        0, globalConfig.distanceToDefenseArea));
#else
    QSize auxSize = szAreaDefesa;
    auxSize.setWidth(auxSize.width() + globalConfig.distanceToDefenseArea);
    auxSize.setHeight(auxSize.height() +
                      globalConfig.distanceToDefenseArea * 2);
    negativeSize = positiveSize = auxSize;
#endif

    QPoint cantoNegativo(-szCampo.width() / 2 - globalConfig.fieldOffset,
                         szAreaDefesa.height() / 2 +
                             globalConfig.distanceToDefenseArea);

    retRestricaoNegativa =
        QRect(cantoNegativo, QPoint(cantoNegativo.x() + negativeSize.width() +
                                        globalConfig.fieldOffset,
                                    cantoNegativo.y() - negativeSize.height()));

    QPoint cantoPositivo(szCampo.width() / 2 - szAreaDefesa.width() -
                             globalConfig.distanceToDefenseArea,
                         szAreaDefesa.height() / 2 +
                             globalConfig.distanceToDefenseArea);

    retRestricaoPositiva =
        QRect(cantoPositivo, QPoint(cantoPositivo.x() + positiveSize.width() +
                                        globalConfig.fieldOffset,
                                    cantoPositivo.y() - positiveSize.height()));
}
