#ifndef GEOMETRIA_H
#define GEOMETRIA_H

#include <QSize>
#include <QRect>
#include <QVector2D>

#include "Constantes_e_Funcoes_Auxiliares/macros.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

class Geometria
{
    QSize szAreaDefesa;/**< Tamanho absoluto da area de defesa [mm]. */
    QRect retRestricaoNegativa; /**< Area de restrição do lado negativo [mm].*/
    QRect retRestricaoPositiva; /**< Area de restrição do lado positivo [mm].*/
    QRect retField;/**< Retângulo que representa o campo [mm].*/
    QSize szCampo;/**< Tamanho absoluto do campo [mm]. */
    QSize szGol;/**< Tamanho absoluto do gol [mm]. */
    int iRaioCirculoCentro; /**< Raio do circulo do centro de campo [mm]. */

public:
    Geometria();
    Geometria(const Geometria &_other);

    void vSetFieldSize(QSize _size);
    void vSetDefenseAreaSize(QSize _size);
    void vSetGoalSize(QSize _size);
    void vSetCenterCircleSize(int _size);

    /**
     * @brief Retorna o tamanho do campo
     *
     * Height -> Y
     * Width -> X
     *
     * @return
     */
    QSize szField() const;

    /**
     * @brief Retorna o tamanho da area de defesa
     *
     * Height -> Y
     * Width -> X
     *
     * @return
     */
    QSize szDefenseArea() const;

    /**
     * @brief Retorna o tamanho do gol
     *
     * Height -> Y
     * Width -> X
     *
     * @return
     */
    QSize szGoal() const;

    /**
     * @brief Retorna o tamanho do raio do circúlo do centro do campo
     *
     * @return
     */
    int iCenterCircleRadius() const;

    /**
     * @brief Retorna true se o ponto fornecido estiver dentro da área de
     * restrição.
     *
     * @param ponto
     * @param ladoCampo
     *
     * @return
     */
    bool bPontoDentroAreaRestricao(QPoint ponto, int ladoCampo) const;

    /**
     * @brief Retorna true se o ponto fornecido estiver dentro do campo
     *
     * @param _point
     *
     * @return
     */
    bool bIsInsideField(QVector2D _point);

    /**
     * @brief Retorna o centro do gol do time
     *
     * @param _teamSide - Lado do time
     *
     * @return
     */
    QVector2D vt2dGoalCenter(int _side) const;

    QRect retPositiveRestriction() const;

    QRect retNegativeRestriction() const;

    QRect retRestriction(int _side) const;

    /**
     * @brief Atualliza a dimensão e posição da área de restrição.
     */
    void vUpdateRestrictionArea();
};

#endif // GEOMETRIA_H
