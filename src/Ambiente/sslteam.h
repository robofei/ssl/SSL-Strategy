#ifndef SSLTEAM_H
#define SSLTEAM_H

#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Robo/robot.h"
#include "Robo/visionrobot.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

template<class ROBOT_T>
class SSLTeam
{
    static_assert(std::is_base_of<Robo, ROBOT_T>::value,
                  "ROBOT_T is not derived from Robo");

    qint8 iGoalieID;
    TeamColor color;
    int iFieldSide;
    int robotsDetected;
    QScopedPointer<ROBOT_T> robots[MAX_IDS];

public:

    /**
     * @brief
     *
     * @param _goalieID
     * @param _fieldSide
     * @param _color
     */
    SSLTeam<ROBOT_T>(int _goalieID, int _fieldSide, TeamColor _color)
    {
        iGoalieID = _goalieID;
        iFieldSide = _fieldSide;
        color = _color;
        robotsDetected = 0;

        for(qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            robots[id].reset(new ROBOT_T(id));
        }
    }

    /**
     * @brief
     *
     * @param _other
     */
    SSLTeam<ROBOT_T>(const SSLTeam &_other)
    {
        vSetGoalieID(_other.iGetGoalieID());
        vSetTeamColor(_other.teamColor());
        vSetSide(_other.iGetSide());

        for(qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            robots[id].reset(new ROBOT_T(*_other.getCPlayer(id)));
        }
        updateRobotsDetected();
    }

    /**
     * @brief Utilizado para fazer uma conversão de SSLTeam<VisionRobot> para
     * SSLTeam<Robo>
     *
     * @return
     */
    SSLTeam<Robo> toRobot() const
    {
        SSLTeam<Robo> team(iGoalieID, iFieldSide, color);
        for(qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            team.getPlayer(id)->vAtualizaDadosVisao(getCPlayer(id));
        }
        return team;
    }

    /**
     * @brief
     *
     * @return
     */
    qint8 iGetGoalieID() const
    {
        return iGoalieID;
    }

    /**
     * @brief
     *
     * @param _id
     */
    void vSetGoalieID(qint8 _id)
    {
        iGoalieID = _id;
    }

    /**
     * @brief
     *
     * @return
     */
    TeamColor teamColor() const
    {
        return color;
    }

    /**
     * @brief
     *
     * @param _color
     */
    void vSetTeamColor(TeamColor _color)
    {
        color = _color;
    }

    /**
     * @brief
     *
     * @return
     */
    int iGetSide() const
    {
        return iFieldSide;
    }

    /**
     * @brief
     *
     * @param _fieldSide
     */
    void vSetSide(int _fieldSide)
    {
        iFieldSide = _fieldSide;
    }

    /**
     * @brief Atualiza os dados de detecção (visão) dos robôs
     *
     * @see Visao, VisionPacket
     *
     * @param _other
     */
    void vUpdateTeamDetection(const SSLTeam<ROBOT_T> &_other)
    {
        for(qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            getPlayer(id)->vAtualizaDadosVisao(_other.getCPlayer(id));
        }
        updateRobotsDetected();
    }

    /**
     * @brief getPlayer
     * @param _id
     * @param file
     * @param fun
     * @param line
     * @return
     */
    ROBOT_T* getPlayer(qint8 _id,
                       const QString file = QString(__builtin_FILE()),
                       const QString fun = QString(__builtin_FUNCTION()),
                       const QString line = QString(__builtin_LINE()))
    {
        if(_id >= 0 && _id < globalConfig.robotsPerTeam)
        {
            return robots[_id].get();
        }
        qCritical() << "ID INVÁLIDO TENTANDO SER USADO" << "ID =" << _id;
        qCritical() << "is being called by " << fun << "() in "
                    << file << "line" << line.at(0).unicode();
        // Além do memory leak, isso pode ter efeitos inesperados, mas não sei
        // uma boa forma de retornar algo sem dar crash no programa
        //
        // O melhor seria retornar um nullptr e não usar a função de forma
        // encadeada
        // Ex: getPlayer(id)->someMethod();
        return new ROBOT_T(777);
    }

    /**
     * @brief getCPlayer
     * @param _id
     * @param file
     * @param fun
     * @param line
     * @return
     */
    const ROBOT_T* getCPlayer(qint8 _id,
                              const QString file = QString(__builtin_FILE()),
                              const QString fun = QString(__builtin_FUNCTION()),
                              const QString line = QString(__builtin_LINE())) const
    {
        const ROBOT_T* _robot;
        if(_id >= 0 && _id < globalConfig.robotsPerTeam)
        {
            _robot =  robots[_id].get();
        }
        else
        {
            qCritical() << "ID INVÁLIDO TENTANDO SER USADO" << "ID =" << _id;
            qCritical() << "is being called by " << fun << "() in "
                << file << "line" << line.at(0).unicode();
            // Além do memory leak, isso pode ter efeitos inesperados, mas não sei
            // uma boa forma de retornar algo sem dar crash no programa
            //
            // O melhor seria retornar um nullptr e não usar a função de forma
            // encadeada
            // Ex: getPlayer(id)->someMethod();
            _robot =  new ROBOT_T(777);
        }
        return _robot;
    }

    int getRobotsDetected() const
    {
        return robotsDetected;
    }

    void updateRobotsDetected()
    {
        robotsDetected = 0;

        for(qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            if(robots[id]->bRoboDetectado())
            {
                robotsDetected++;
            }
        }
    }

};
#endif // SSLTEAM_H
