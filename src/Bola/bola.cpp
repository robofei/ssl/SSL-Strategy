/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "bola.h"
#include "qvector2d.h"

Bola::Bola()
{
    vt2dPosicaoAtual      = QVector2D(0,0);
    vt2dPosicaoAnterior   = QVector2D(0,0);
    covariance = QVector2D(0, 0);

    prediction.position = QVector2D(0, 0);
    prediction.hasPrediction = false;

    dVelocidade = 0;
    dTempoBola  = 0;

    vt2dVelocidade = QVector2D(0,0);
    bEmCampo = false;

    vvt2dFiltered.clear();
    vvt2dPredicted.clear();
}

Bola::Bola(const Bola& outra)
{
    vt2dPosicaoAtual      = outra.vt2dPosicaoAtual;
    vt2dPosicaoAnterior   = outra.vt2dPosicaoAnterior;
    covariance = outra.covariance;
    prediction = outra.prediction;

    dVelocidade = outra.dVelocidade;
    dTempoBola  = outra.dTempoBola;

    vt2dVelocidade = outra.vt2dVelocidade;
    bEmCampo = outra.bEmCampo;

    vvt2dFiltered.clear();
    vvt2dFiltered.append(outra.vvt2dGetFilteredPositions());

    vvt2dPredicted.clear();
    vvt2dPredicted.append(outra.vvt2dGetPredictedPositions());
}

void Bola::vSetPosition(QVector2D _pos, QVector2D _covariance)
{
    vt2dPosicaoAnterior = vt2dPosicaoAtual;
    vt2dPosicaoAtual = _pos;
    covariance = _covariance;
}

void Bola::vSetVelocity(QVector2D _velocity)
{
    vt2dVelocidade = _velocity;
    dVelocidade = static_cast<double>(_velocity.length());

    predictPosition(0.5);
}

bool Bola::bDetected()const
{
    return bEmCampo;
}

void Bola::vAddTime(double _time)
{
    dTempoBola += _time;
    if(dTempoBola > 30)
        dTempoBola = 0;
}

void Bola::vAddPosition(QVector<QVector2D> &_vector, QVector2D _position)
{
    _vector.prepend(_position);
    if(_vector.size() > iTamanhoBufferBola)
    {
        _vector.pop_back();
    }
}

void Bola::predictPosition(float _finalSpeed)
{
    float ballV = this->dVelocidade,
//          ballDeceleration = qAbs(0.35),
          ballDeceleration = qAbs(0.709189),
          tTarget;
    tTarget = qMax((_finalSpeed - ballV) / (-ballDeceleration), 0.0f);
    QVector2D ballVelocity = this->vt2dVelocidade, ballTarget;

    // ===================================================== Método Cinemático
    // P_final = P_atual + V_atual * t_target + 0.5 * a * t_target^2
    QVector2D deceleration = -ballVelocity.normalized() * ballDeceleration;

    ballTarget = this->vt2dPosicaoAtual/1e3 + ballVelocity * tTarget + 0.5 * deceleration * tTarget * tTarget;
    ballTarget *= 1e3;

    if (tTarget > 0.5 && prediction.hasPrediction == false)
    {
        prediction.hasPrediction = true;
        calibrationTimer.start();
        initialSpeed = ballV;
    }
    else if (tTarget <= 0)
    {
        prediction.hasPrediction = false;
    }

    if (tTarget > 0)
    {
        // initialSpeed = qMax(initialSpeed, ballV);
        if (ballV > initialSpeed)
        {
            initialSpeed = ballV;
            calibrationTimer.start();
        }
        prediction.position = ballTarget;
    }

    if (qAbs(ballV) <= _finalSpeed && calibrationTimer.isValid())
    {
        float tReal = calibrationTimer.nsecsElapsed() / 1e9;
        calibrationTimer.invalidate();
        float measuredFriction = (initialSpeed - ballV) / tReal;
        qDebug() << "Calculated friction:" << measuredFriction;
        friction = measuredFriction;
    }

    // ===================================================== Método Energia
    // m * V_atual^2       m * V_final^2    \____Fat____/
    // -------------   -   -------------  = u_at * m * g * d_stop
    //      2                  2
    // const float mass = 43 / 1000.f,
    //             kinEnergy = (mass * qPow(ballV, 2) / 2.f - mass * qPow(_finalSpeed, 2) / 2.f),
    //             friction = 0.2;
    // float stopDist = kinEnergy / (friction * mass * 9.81);
    // ballTarget = this->vt2dPosicaoAtual + ballVelocity.normalized() * stopDist * 1000;
    //
    // if (stopDist > 0)
    // {
    //     prediction.hasPrediction = true;
    //     prediction.position = ballTarget;
    // }
}

void Bola::vAddFilteredPosition(QVector2D _position)
{
    vAddPosition(vvt2dFiltered, _position);
}

QVector<QVector2D> Bola::vvt2dGetFilteredPositions() const
{
    return vvt2dFiltered;
}

void Bola::vAddPredictedPosition(QVector2D _position)
{
    vAddPosition(vvt2dPredicted, _position);
}

QVector<QVector2D> Bola::vvt2dGetPredictedPositions() const
{
    return vvt2dPredicted;
}
