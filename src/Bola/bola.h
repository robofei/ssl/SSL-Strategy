/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file bola.h
 * @brief Arquivo header com a definição da classe da bola
 * @author Leonardo da Silva Costa
 * @version 1.0
 * @date 2020-08-27
 */
#ifndef BOLA_H
#define BOLA_H

#include <QVector>
#include <QVector2D>
#include <QElapsedTimer>

#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

/**
 * @brief Classe que representa a bola
 *
 */
class Bola
{
protected:
    QVector<QVector2D> vvt2dFiltered;
    QVector<QVector2D> vvt2dPredicted;

    void vAddPosition(QVector<QVector2D> &_vector, QVector2D _position);
    void predictPosition(float _finalSpeed);

public:

    /**
     * @brief Construtor da classe
     *
     */
    Bola();

    /**
     * @brief Construtor por cópia
     *
     * @param outra - Objeto de outra bola a ser utilizada para cópia
     */
    Bola(const Bola& outra);

    /**
     * @brief Seta a posição da bola [mm].
     *
     * @param _pos - Nova posição.
     */
    void vSetPosition(QVector2D _pos, QVector2D _covariance = QVector2D(0, 0));

    /**
     * @brief Seta a velocidade vetorial da bola [m/s].
     *
     * @param _velocity - Nova velocidade.
     */
    void vSetVelocity(QVector2D _velocity);

    /**
     * @brief Retorna se a bola está sendo detectada na visão.
     *
     * @return bool - Bola detectada -> True, senão False.
     */
    bool bDetected() const;

    /**
     * @brief Incrementa o tempo da bola na quantidade de tempo fornecida [s].
     *
     * @param _time - Incremento de tempo.
     */
    void vAddTime(double _time);

    void vAddFilteredPosition(QVector2D _position);
    QVector<QVector2D> vvt2dGetFilteredPositions() const;

    void vAddPredictedPosition(QVector2D _position);
    QVector<QVector2D> vvt2dGetPredictedPositions() const;

    QVector2D vt2dPosicaoAtual; /**< Posição atual da bola [mm]. */

    struct prediction 
    {
        QVector2D position;
        bool hasPrediction;
    } prediction;
    QElapsedTimer calibrationTimer;
    float friction;
    float initialSpeed;

    QVector2D covariance;
    QVector2D vt2dPosicaoAnterior; /**< Posição anterior da bola [mm]. */
    QVector2D vt2dVelocidade; /**< Velocidade X,Y da bola [m/s]. */

    double dVelocidade; /**< Módulo da velocidade atual da bola [m/s].*/
    double dTempoBola; /**< Tempo atual da bola [s]. */

    bool bEmCampo; /**< Mostra se a bola está sendo detectado pela visão. */
};

#endif // BOLA_H
