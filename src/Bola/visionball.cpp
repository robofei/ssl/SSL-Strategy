#include "visionball.h"

VisionBall::VisionBall()
{
    vvt2dRaw.clear();
    kfFilter.reset(new KalmanFilter());
    detectedFrames = 0;
}

void VisionBall::vAddRawPosition(QVector2D _position)
{
    vAddPosition(vvt2dRaw, _position);
}

QVector<QVector2D> VisionBall::vvt2dGetRawPositions() const
{
    return vvt2dRaw;
}

void VisionBall::vResetFrames()
{
    for(uint & i : iCameraFrame)
    {
        i = 0;
    }
}
