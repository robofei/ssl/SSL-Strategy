#ifndef VISIONBALL_H
#define VISIONBALL_H

#include "bola.h"
#include "Visao/Kalman_Filter/kalmanfilter.h"
#include <QScopedPointer>

class VisionBall : public Bola
{
    QVector<QVector2D> vvt2dRaw;

public:
    VisionBall();

    QScopedPointer<KalmanFilter> kfFilter;
    uint iCameraID; /**< Indica o ID da última câmera em que a bola foi vista. */
    uint iCameraFrame[iNumeroTotalCameras];/**< Guarda o número do frame em que
                                             a bola foi visto.
                                             @details Isso é utilizado para
                                             determinar quando a bola some da
                                             visão.*/
    int detectedFrames;

    void vAddRawPosition(QVector2D _position);
    QVector<QVector2D> vvt2dGetRawPositions() const;

    void vResetFrames();
};

#endif // VISIONBALL_H
