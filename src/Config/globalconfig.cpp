#include "globalconfig.h"
#include "Config/jsondata.h"

GlobalConfig::GlobalConfig(QString _name) : JSONData(_name)
{
    robotVelocities.linear.limit = 2.0;
    robotVelocities.linear.max = 2.5;

    robotVelocities.angular.limit = 6.0;
    robotVelocities.angular.max = 6.5;

    stopLinearVelocity = 1.5;

    delta.maxVelocity = 1.0;
    delta.spacing = 235;

    testLinearVelocity = 1.0;

    ballDiameter = 43;
    robotDiameter = 180;

    robotsPerTeam = 8;
    ballDistanceToPlay = 180;

    fieldOffset = 400;
    fieldDivisionWidth = 1500;

    safety.ally = 240;
    safety.opponent = 240;
    safety.distance = 180;
    safety.ball.normal = 300;
    safety.ball.stop = 500 + robotDiameter / 2;

    framesUntilMissing = 20;
    framesToAppear = 20;

    minAimSpace = 270;

    distanceToDefenseArea = 360;
    calibratedKickForce = 8;

    robotDistanceToFront = 80;
    robotDistanceToBall = 88;

    controlSimulation = false;

    network.vision.port = 10006;
    network.vision.ip = "224.5.23.2";
    network.referee.port = 10003;
    network.referee.ip = "224.5.23.1";
    network.blueTeam.cmdPort = 10301;
    network.blueTeam.statusPort = 30011;
    network.yellowTeam.cmdPort = 10302;
    network.yellowTeam.statusPort = 30012;
    network.simulation.cmdPort = 20011;
    network.simulation.controlPort = 10300;

    samplingTime.interface = 20;
    samplingTime.kalman = 5;
    samplingTime.strategy = 25;
    samplingTime.motion = 10;

    attributesNames << "robotVelocities"
                    << "stopLinearVelocity"
                    << "delta"
                    << "testLinearVelocity"
                    << "ballDiameter"
                    << "robotDiameter"
                    << "robotsPerTeam"
                    << "ballDistanceToPlay"
                    << "fieldOffset"
                    << "fieldDivisionWidth"
                    << "safety"
                    << "framesUntilMissing"
                    << "framesToAppear"
                    << "minAimSpace"
                    << "distanceToDefenseArea"
                    << "calibratedKickForce"
                    << "robotDistanceToFront"
                    << "robotDistanceToBall"
                    << "controlSimulation"
                    << "network"
                    << "samplingTime";
    attributes = attributesNames.size();
}

GlobalConfig::~GlobalConfig()
{
}

QJsonValue GlobalConfig::getAttribute(int _n) const
{
    switch (_n)
    {
    case 0: {
        QJsonObject jsonRobotVelocities, velocity;
        velocity["limit"] = robotVelocities.linear.limit;
        velocity["max"] = robotVelocities.linear.max;
        jsonRobotVelocities["linear"] = velocity;

        velocity["limit"] = robotVelocities.angular.limit;
        velocity["max"] = robotVelocities.angular.max;
        jsonRobotVelocities["angular"] = velocity;
        return jsonRobotVelocities;
    }
    break;
    case 1:
        return stopLinearVelocity;
        break;
    case 2: {
        QJsonObject jsonDelta;
        jsonDelta["maxVelocity"] = delta.maxVelocity;
        jsonDelta["spacing"] = delta.spacing;
        return jsonDelta;
    }
    break;
    case 3:
        return testLinearVelocity;
        break;
    case 4:
        return ballDiameter;
        break;
    case 5:
        return robotDiameter;
        break;
    case 6:
        return robotsPerTeam;
        break;
    case 7:
        return ballDistanceToPlay;
        break;
    case 8:
        return fieldOffset;
        break;
    case 9:
        return fieldDivisionWidth;
        break;
    case 10: {
        QJsonObject jsonSafety, ball;
        ball["normal"] = safety.ball.normal;
        ball["stop"] = safety.ball.stop;
        jsonSafety["ball"] = ball;
        jsonSafety["ally"] = safety.ally;
        jsonSafety["opponent"] = safety.opponent;
        jsonSafety["distance"] = safety.distance;
        return jsonSafety;
    }
    break;
    case 11:
        return framesUntilMissing;
        break;
    case 12:
        return framesToAppear;
        break;
    case 13:
        return minAimSpace;
        break;
    case 14:
        return distanceToDefenseArea;
        break;
    case 15:
        return calibratedKickForce;
        break;
    case 16:
        return robotDistanceToFront;
        break;
    case 17:
        return robotDistanceToBall;
        break;
    case 18:
        return controlSimulation;
        break;
    case 19: {
        QJsonObject jsonNetwork, vision, referee, teams, simulation;
        vision["port"] = network.vision.port;
        vision["ip"] = network.vision.ip;
        jsonNetwork["vision"] = vision;

        referee["port"] = network.referee.port;
        referee["ip"] = network.referee.ip;
        jsonNetwork["referee"] = referee;

        teams["cmdPort"] = network.blueTeam.cmdPort;
        teams["statusPort"] = network.blueTeam.statusPort;
        jsonNetwork["blueTeam"] = teams;

        teams["cmdPort"] = network.yellowTeam.cmdPort;
        teams["statusPort"] = network.yellowTeam.statusPort;
        jsonNetwork["yellowTeam"] = teams;

        simulation["cmdPort"] = network.simulation.cmdPort;
        simulation["controlPort"] = network.simulation.controlPort;
        jsonNetwork["simulation"] = simulation;

        return jsonNetwork;
    }
    break;
    case 20: {
        QJsonObject jsonSampling, sampling;
        sampling["interface"] = samplingTime.interface;
        sampling["kalman"] = samplingTime.kalman;
        sampling["strategy"] = samplingTime.strategy;
        sampling["motion"] = samplingTime.motion;
        jsonSampling["samplingTime"] = sampling;
        return jsonSampling;
    }
    break;
    default:
        return "Undefined";
        break;
    }
}

void GlobalConfig::setAttribute(int _n, QJsonValue _value)
{
    switch (_n)
    {
    case 0: {
        robotVelocities.linear.limit = _value["linear"]["limit"].toDouble();
        robotVelocities.linear.max = _value["linear"]["max"].toDouble();

        robotVelocities.angular.limit = _value["angular"]["limit"].toDouble();
        robotVelocities.angular.max = _value["angular"]["max"].toDouble();
    }
    break;
    case 1:
        stopLinearVelocity = _value.toDouble();
        break;
    case 2: {
        delta.maxVelocity = _value["maxVelocity"].toDouble();
        delta.spacing = _value["spacing"].toInt();
    }
    break;
    case 3:
        _value.toDouble();
        break;
    case 4:
        ballDiameter = _value.toInt();
        break;
    case 5:
        robotDiameter = _value.toInt();
        break;
    case 6:
        robotsPerTeam = _value.toInt();
        break;
    case 7:
        ballDistanceToPlay = _value.toInt();
        break;
    case 8:
        fieldOffset = _value.toInt();
        break;
    case 9:
        fieldDivisionWidth = _value.toInt();
        break;
    case 10: {
        safety.ball.normal = _value["ball"]["normal"].toInt();
        safety.ball.stop = _value["ball"]["stop"].toInt();
        safety.ally = _value["ally"].toInt();
        safety.opponent = _value["opponent"].toInt();
        safety.distance = _value["distance"].toInt();
    }
    break;
    case 11:
        framesUntilMissing = _value.toInt();
        break;
    case 12:
        framesToAppear = _value.toInt();
        break;
    case 13:
        minAimSpace = _value.toInt();
        break;
    case 14:
        distanceToDefenseArea = _value.toInt();
        break;
    case 15:
        calibratedKickForce = _value.toInt();
        break;
    case 16:
        robotDistanceToFront = _value.toInt();
        break;
    case 17:
        robotDistanceToBall = _value.toInt();
        break;
    case 18:
        controlSimulation = _value.toBool();
        break;
    case 19: {
        network.vision.port = _value["vision"]["port"].toInt();
        network.vision.ip = _value["vision"]["ip"].toString();

        network.referee.port = _value["referee"]["port"].toInt();
        network.referee.ip = _value["referee"]["ip"].toString();

        network.blueTeam.cmdPort = _value["blueTeam"]["cmdPort"].toInt();
        network.blueTeam.statusPort = _value["blueTeam"]["statusPort"].toInt();

        network.yellowTeam.cmdPort = _value["yellowTeam"]["cmdPort"].toInt();
        network.yellowTeam.statusPort =
            _value["yellowTeam"]["statusPort"].toInt();

        network.simulation.cmdPort = _value["simulation"]["cmdPort"].toInt();
        network.simulation.controlPort =
            _value["simulation"]["controlPort"].toInt();
    }
    break;
    case 20: {
        samplingTime.interface = _value["interface"].toInt();
        samplingTime.kalman = _value["kalman"].toInt();
        samplingTime.strategy = _value["strategy"].toInt();
        samplingTime.motion = _value["motion"].toInt();
    }
    break;
    default:
        break;
    }
}
