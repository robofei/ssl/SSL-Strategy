#ifndef GLOBALCONFIG_H
#define GLOBALCONFIG_H

#include "jsondata.h"

class GlobalConfig : public JSONData
{
public:
    GlobalConfig(QString _name);
    ~GlobalConfig();

    QJsonValue getAttribute(int _n) const override;
    void setAttribute(int _n, QJsonValue _value) override;

    struct
    {
        struct
        {
            float limit;
            float max;
        } linear;
        struct
        {
            float limit;
            float max;
        } angular;
    } robotVelocities;

    float stopLinearVelocity;

    struct
    {
        float maxVelocity;
        int spacing;
    } delta;

    float testLinearVelocity;

    int ballDiameter;
    int robotDiameter;

    int robotsPerTeam;
    int ballDistanceToPlay;

    int fieldOffset;
    int fieldDivisionWidth;

    struct
    {
        struct
        {
            int normal;
            int stop;
        } ball;

        int ally;
        int opponent;
        int distance;
    } safety;

    int framesUntilMissing;
    int framesToAppear;

    int minAimSpace;

    int distanceToDefenseArea;
    int calibratedKickForce;

    int robotDistanceToFront;
    int robotDistanceToBall;

    bool controlSimulation;

    struct
    {
        struct
        {
            int port;
            QString ip;
        } vision;

        struct
        {
            int port;
            QString ip;
        } referee;

        struct
        {
            int cmdPort;
            int statusPort;
        } blueTeam;

        struct
        {
            int cmdPort;
            int statusPort;
        } yellowTeam;

        struct
        {
            int cmdPort;
            int controlPort;
        } simulation;
    } network;

    struct
    {
        int interface;
        int kalman;
        int strategy;
        int motion;
    } samplingTime;
};

#endif // GLOBALCONFIG_H
