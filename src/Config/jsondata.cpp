#include "jsondata.h"

JSONData::JSONData()
{
}

JSONData::JSONData(QString _name)
{
    attributes = 0;
    name = _name;
}

QJsonValue JSONData::getAttribute(int _n) const
{
    Q_UNUSED(_n)
    return QJsonObject();
}

QString JSONData::getAttributeName(int _n) const
{
    if (_n < attributesNames.size())
    {
        return attributesNames.at(_n);
    }
    return "Undefined";
}

void JSONData::setAttribute(const QString& _key, QJsonValue _value)
{
    int i = attributesNames.indexOf(_key);

    if (i != -1)
    {
        setAttribute(i, _value);
    }
}

int JSONData::nAttributes() const
{
    return attributes;
}

void JSONData::create(const QString& path)
{
    QJsonObject content;
    QJsonObject jsonData;

    for (int i = 0; i < nAttributes(); i++)
    {
        content.insert(getAttributeName(i), getAttribute(i));
        qDebug() << "Inserting: " << getAttribute(i) << " into " << getAttributeName(i);
    }
    jsonData[name] = content;
    qDebug() << "Creating: " << jsonData;

    QJsonDocument document;
    document.setObject(jsonData);

    QByteArray bytes = document.toJson(QJsonDocument::Indented);
    QFile file(path);

    if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
    {
        QTextStream stream(&file);
        stream.setCodec("utf-8");
        stream << bytes;
        file.close();
    }
    else
    {
        qDebug() << "[JSON Config] File open failed: " << path;
    }
}

bool JSONData::read(const QString& path)
{
    QFile file(path);
    if (file.open(QIODevice::ReadOnly))
    {
        QByteArray bytes = file.readAll();
        file.close();

        QJsonParseError jsonError;
        QJsonDocument document = QJsonDocument::fromJson(bytes, &jsonError);

        if (jsonError.error != QJsonParseError::NoError)
        {
            qDebug() << "[JSON Config] fromJson failed: "
                     << jsonError.errorString();
            return false;
        }
        if (document.isObject())
        {
            QJsonObject jsonObj = document.object();
            qDebug() << "[JSON Config] Reading...\n" << jsonObj;

            if (jsonObj.contains(name))
            {
                QJsonObject obj = jsonObj.value(name).toObject();
                QStringList keys = obj.keys();

                for (const QString& key : qAsConst(keys))
                {
                    auto value = obj.take(key);
                    setAttribute(key, value);
                }
            }
        }
        return true;
    }
    else
    {
        qDebug() << "[JSON Config] Failed to read file " << path;
        return false;
    }
}
