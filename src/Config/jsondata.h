#ifndef JSONDATA_H
#define JSONDATA_H

#include <QJsonObject>
#include <QJsonDocument>
#include <QString>
#include <QFile>

class JSONData
{
public:
    JSONData();
    JSONData(QString _name);
    virtual ~JSONData() { };

    virtual QJsonValue getAttribute(int _n) const;
    QString getAttributeName(int _n) const;
    virtual void setAttribute(int _n, QJsonValue _value) = 0;
        
    void setAttribute(const QString& _key, QJsonValue _value);
    int nAttributes() const;

    void create(const QString& path);
    bool read(const QString& path);
        
protected:
    int attributes;
    QStringList attributesNames;
    QString name;
};

#endif // JSONDATA_H
