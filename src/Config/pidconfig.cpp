#include "pidconfig.h"

PIDConfig::PIDConfig(QString _name) : JSONData(_name)
{

    kPx = 1;
    kPy = 1;
    kPw = 1;

    kIx = 1;
    kIy = 1;
    kIw = 1;

    kDx = 1;
    kDy = 1;
    kDw = 1;

    attributesNames << "kPx"
                    << "kPy"
                    << "kPw"
                    << "kIx"
                    << "kIy"
                    << "kIw"
                    << "kDx"
                    << "kDy"
                    << "kDw";
    attributes = attributesNames.size();
}

PIDConfig::~PIDConfig()
{
}

QJsonValue PIDConfig::getAttribute(int _n) const
{
    switch (_n)
    {
    case 0:
        return kPx;
        break;
    case 1:
        return kPy;
        break;
    case 2:
        return kPw;
        break;
    case 3:
        return kIx;
        break;
    case 4:
        return kIy;
        break;
    case 5:
        return kIw;
        break;
    case 6:
        return kDx;
        break;
    case 7:
        return kDy;
        break;
    case 8:
        return kDw;
        break;
    }
    return "Undefined";
}

void PIDConfig::setAttribute(int _n, QJsonValue _value)
{
    switch (_n)
    {
    case 0:
        kPx = _value.toDouble();
        break;
    case 1:
        kPy = _value.toDouble();
        break;
    case 2:
        kPw = _value.toDouble();
        break;
    case 3:
        kIx = _value.toDouble();
        break;
    case 4:
        kIy = _value.toDouble();
        break;
    case 5:
        kIw = _value.toDouble();
        break;
    case 6:
        kDx = _value.toDouble();
        break;
    case 7:
        kDy = _value.toDouble();
        break;
    case 8:
        kDw = _value.toDouble();
        break;
    }
}

