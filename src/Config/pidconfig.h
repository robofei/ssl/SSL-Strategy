#ifndef PIDCONFIG_H
#define PIDCONFIG_H

#include "jsondata.h"

class PIDConfig : public JSONData
{
public:
    PIDConfig(QString _name);
    ~PIDConfig();
        
    QJsonValue getAttribute(int _n) const override;
    void setAttribute(int _n, QJsonValue _value) override;

    float kPx;
    float kPy;
    float kPw;

    float kIx;
    float kIy;
    float kIw;

    float kDx;
    float kDy;
    float kDw;
};

#endif // PIDCONFIG_H
