#include "playdeflectconfig.h"

PlayDeflectConfig::PlayDeflectConfig(QString _name) : JSONData(_name)
{
    passForce = 5;
    kickForce = 6.5;
    deflectPositionAngle = 40;

    attributesNames << "passForce"
                    << "kickForce"
                    << "deflectPositionAngle";
    
    attributes = attributesNames.size();
}

PlayDeflectConfig::~PlayDeflectConfig()
{
}

QJsonValue PlayDeflectConfig::getAttribute(int _n) const
{
    switch (_n)
    {
    case 0:
        return passForce;
        break;
    case 1:
        return kickForce;
        break;
    case 2:
        return deflectPositionAngle;
        break;
    }
    return "Undefined";
}

void PlayDeflectConfig::setAttribute(int _n, QJsonValue _value)
{
    switch (_n)
    {
    case 0:
        passForce = _value.toDouble();
        break;
    case 1:
        kickForce = _value.toDouble();
        break;
    case 2:
        deflectPositionAngle = _value.toDouble();
        break;
    }
}
