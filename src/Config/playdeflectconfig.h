#ifndef PLAYDEFLECTCONFIG_H
#define PLAYDEFLECTCONFIG_H

#include "Config/jsondata.h"

class PlayDeflectConfig : public JSONData
{
public:
    PlayDeflectConfig(QString _name);
    ~PlayDeflectConfig();

    QJsonValue getAttribute(int _n) const override;
    void setAttribute(int _n, QJsonValue _value) override;

    float passForce;
    float kickForce;
    float deflectPositionAngle;
};

#endif // PLAYDEFLECTCONFIG_H
