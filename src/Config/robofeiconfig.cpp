#include "robofeiconfig.h"

#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Interface/drawmap.h"
#include "qglobal.h"

RoboFeiConfig::RoboFeiConfig()
{
}

RoboFeiConfig::RoboFeiConfig(Ui::RoboFeiSSL* ui)
{
    lineEditsInt["NumeroCameras"] = ui->le_ConfigGerais_NumeroCameras;
    lineEditsInt["PosicaoTimeoutX"] = ui->le_ConfigGerais_PosicaoTimeoutX;
    lineEditsInt["PosicaoTimeoutY"] = ui->le_ConfigGerais_PosicaoTimeoutX;

    spinBoxes["FPS"] = ui->spin_ConfigGerais_FPS;
    spinBoxes["AnguloGolContra"] = ui->spin_ConfigGerais_AnguloGolContra;
    spinBoxes["DistanciaReceptorCobranca"] =
        ui->spin_ConfigGerais_DistanciaReceptorCobranca;
    spinBoxes["PacotesSumirVisao"] = ui->spin_ConfigGerais_PacotesSairVisao;
    spinBoxes["TamanhoBufferBola"] = ui->spin_ConfigGerais_TamanhoBufferBola;
    spinBoxes["TamanhoTraco"] = ui->spin_ConfigGerais_TamanhoTraco;
    spinBoxes["NotaChute"] = ui->spin_notaChute;
    spinBoxes["NotaMinimaPasse"] = ui->spin_notaMinimaPasse;
    spinBoxes["TempoRecargaChute"] = ui->spin_TempoRecargaChute;
    spinBoxes["DecisoesAnguloMinimoPasse"] =
        ui->sb_FatorAvaliacaoAnguloMinimoPasse;
    spinBoxes["DecisoesAnguloMinimoChute"] =
        ui->sb_FatorAvaliacaoAnguloMinimoChute;
    spinBoxes["DecisoesAnguloMaximoPasse"] =
        ui->sb_FatorAvaliacaoAnguloMaximoPasse;
    spinBoxes["DecisoesAnguloMaximoChute"] =
        ui->sb_FatorAvaliacaoAnguloMaximoChute;

    doubleSpinBoxes["DistanciaParaChutePuxadinha"] =
        ui->spin_distanciaParaChutePuxadinha;
    doubleSpinBoxes["TempoPosicionamentoFreeKick"] =
        ui->spin_tempoPosicionamentoFreeKick;
    doubleSpinBoxes["AnguloDeErroChuteNormal"] = ui->spin_anguloErroChuteNormal;
    doubleSpinBoxes["AnguloDeErroPasseNormal"] = ui->spin_anguloErroPasseNormal;
    doubleSpinBoxes["DistanciaCobradorBolaParada"] =
        ui->spin_distanciaCobradorBolaParada;
    doubleSpinBoxes["VelocidadeBolaChegadaPasse"] =
        ui->spin_ConfigGerais_VelocidadeBolaChegadaPasse;
    doubleSpinBoxes["DistanciaMinimaPasse"] = ui->spin_distanciaMinimaPasse;
    doubleSpinBoxes["DistanciaMaximaPasse"] = ui->spin_distanciaMaximaPasse;
    doubleSpinBoxes["DecisoesFatorControlePasse"] = ui->dsb_FatorControlePasse;
    doubleSpinBoxes["DecisoesFatorControleChute"] = ui->dsb_FatorControleChute;

    comboBoxesIndex["CorDoTime"] = ui->cb_ConfigGerais_CorTime;
    comboBoxesIndex["LadoDefesa"] = ui->cb_ConfigGerais_LadoDefesa;
    comboBoxesIndex["IDGoleiro"] = ui->cb_ConfigGerais_Goleiro;
    comboBoxesIndex["TesteMeioCampo"] = ui->cb_ConfigGerais_TesteMeioCampo;
    comboBoxesIndex["DecisoesModeloPasse"] = ui->cb_ModeloPasse;
    comboBoxesIndex["DecisoesModeloChute"] = ui->cb_ModeloChute;
    comboBoxesIndex["DribleDoRobo"] = ui->cb_DribleDoRobo;

    comboBoxesText["NetworkInterface"] = ui->cb_Rede_InterfaceRede;
    comboBoxesText["ModeloForcaChute"] = ui->cb_ModeloForcaChute;
    comboBoxesText["ComportamentoTatico"] = ui->cb_ComportamentoTatico;
    comboBoxesText["TipoDeltaNormal"] = ui->cb_TipoDeltaNormal;
    comboBoxesText["JogadaDefensorNormal"] = ui->cb_comportamentoDefensorNormal;
    comboBoxesText["TemaInterface"] = ui->cb_TemaInterface;

    checkBoxes["DecisoesFatorDeRotacao"] = ui->ckb_FatorDeRotacao;
    checkBoxes["UsarKickSensorNaVisao"] = ui->cbKickSensorInfluenciaVisao;
    checkBoxes["UtilizarJogadasEnsaiadas"] = ui->cb_utilizarJogadasEnsaiadas;
}

void RoboFeiConfig::salvar(QFile* arquivo, const OpcoesInterface& opcInterface)
{
    QTextStream stream(arquivo);

    for (auto it = lineEditsInt.constBegin(); it != lineEditsInt.constEnd();
         ++it)
    {
        stream << it.key() << ' ' << it.value()->text().toInt() << '\n';
    }

    for (auto it = spinBoxes.constBegin(); it != spinBoxes.constEnd(); ++it)
    {
        stream << it.key() << ' ' << it.value()->value() << '\n';
    }

    for (auto it = doubleSpinBoxes.constBegin();
         it != doubleSpinBoxes.constEnd(); ++it)
    {
        stream << it.key() << ' ' << it.value()->value() << '\n';
    }

    for (auto it = comboBoxesIndex.constBegin();
         it != comboBoxesIndex.constEnd(); ++it)
    {
        stream << it.key() << ' ' << it.value()->currentIndex() << '\n';
    }

    for (auto it = comboBoxesText.constBegin(); it != comboBoxesText.constEnd();
         ++it)
    {
        stream << it.key() << ' ' << it.value()->currentText() << '\n';
    }

    for (auto it = checkBoxes.constBegin(); it != checkBoxes.constEnd(); ++it)
    {
        stream << it.key() << ' ' << it.value()->isChecked() << '\n';
    }

    // especiais
    stream << "OpcoesInterface" << ' ' << opcInterface.strOpcoesAtivas()
           << '\n';

    stream << "CoresMapaCampo" << ' ' << DrawMap::corFundoCampo.name() << ';'
           << DrawMap::corRoboAmarelo.name() << ';'
           << DrawMap::corBordaRoboAmarelo.name() << ';'
           << DrawMap::corRoboAzul.name() << ';'
           << DrawMap::corBordaRoboAzul.name() << ';'
           << DrawMap::corBolaEmCampo.name() << ';'
           << DrawMap::corBolaForaDeCampo.name() << ';'
           << DrawMap::corLinhasCampo.name() << ';' << DrawMap::corIdRobo.name()
           << ';' << DrawMap::corFpsBaixo.name() << ';'
           << DrawMap::corFpsAlto.name() << '\n';

    stream << '\n';
}

void RoboFeiConfig::carregar(QFile* arquivo, OpcoesInterface& opcInterface)
{
    QTextStream stream(arquivo);

    while (!stream.atEnd())
    {
        QString s = stream.readLine();

        if (s.isEmpty() || s.at(0) == '#')
            continue;

        QStringList terms = s.split(" ");

        // Apenas considerar linhas que tenham apenas dois itens,
        //  o primeiro com o nome da variável e o seguinte com o respectivo
        //  valor
        // Caso no futuro seja relevante adicionar linhas com mais de dois
        // argumentos,
        //  adpte da seguinte forma:
        //
        //       if(terms.size() == 2)
        //       {
        //           ...
        //       }
        //       else if(terms.size() == 3)
        //       {
        //           ...
        //       }
        //       ...
        //       else
        //       {
        //           ...x
        //       }
        //

        if (terms.size() != 2)
        {
            qWarning() << "Não foi possível ler a linha \"" << qPrintable(s)
                       << "\" no arquivo de configurações\n"
                       << "Linha ignorada.";
            continue;
        }

        const QString key = terms.at(0);
        const QString value = terms.at(1);

        if (QLineEdit* le = lineEditsInt.value(key, nullptr))
        {
            le->setText(value);
        }
        else if (QSpinBox* spin = spinBoxes.value(key, nullptr))
        {
            spin->setValue(value.toInt());
        }
        else if (QDoubleSpinBox* spinD = doubleSpinBoxes.value(key, nullptr))
        {
            spinD->setValue(value.toDouble());
        }
        else if (QComboBox* cbI = comboBoxesIndex.value(key, nullptr))
        {
            cbI->setCurrentIndex(value.toInt());
        }
        else if (QComboBox* cbT = comboBoxesText.value(key, nullptr))
        {
            if (cbT->findText(value) != -1)
                cbT->setCurrentText(value);
        }
        else if (QCheckBox* ckb = checkBoxes.value(key, nullptr))
        {
            ckb->setChecked(value == "1");
        }
        // Especiais
        else if (key.compare("CoresMapaCampo", Qt::CaseInsensitive) == 0)
        {
            QStringList cores = value.split(';');

            if (cores.size() == 11)
            {
                DrawMap::corFundoCampo = QColor(cores.at(0));
                DrawMap::corRoboAmarelo = QColor(cores.at(1));
                DrawMap::corBordaRoboAmarelo = QColor(cores.at(2));
                DrawMap::corRoboAzul = QColor(cores.at(3));
                DrawMap::corBordaRoboAzul = QColor(cores.at(4));
                DrawMap::corBolaEmCampo = QColor(cores.at(5));
                DrawMap::corBolaForaDeCampo = QColor(cores.at(6));
                DrawMap::corLinhasCampo = QColor(cores.at(7));
                DrawMap::corIdRobo = QColor(cores.at(8));
                DrawMap::corFpsBaixo = QColor(cores.at(9));
                DrawMap::corFpsAlto = QColor(cores.at(10));
            }
        }
        else if (key.compare("OpcoesInterface", Qt::CaseInsensitive) == 0)
        {
            opcInterface.vSetaOpcoesFromString(value);
        }
        else
        {
            qWarning() << "Parâmetro \"" << qPrintable(key)
                       << "\" irreconhecível no arquivo de configurações\n"
                       << "Linha ignorada.";
        }
    }
}

