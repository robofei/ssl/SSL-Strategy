#ifndef ROBOFEICONFIG_H
#define ROBOFEICONFIG_H

#include <QFile>
#include <QJsonObject>
#include <QList>
#include <QMap>
#include <QString>
#include <QtWidgets>

#include "Interface/opcoesinterface.h"
#include "ui_robofeissl.h"

///
/// \brief Classe responsável por gerenciar a configuração da GUI principal
/// (#RoboFeiSSL) \details Essa classe permite o carregamento e salvamento de
/// parâmetros da GUI principal
///  do programa (#RoboFeiSSL). Com ela se torna possível guardar o valor dos
///  parâmetros através de um arquivo, de forma que o usuário sempre consiga
///  iniciar o programa com as mesmas configurações da sessão anterior.
///
/// \example Para adicionar um parâmetro, basta passar seu identificador junto
/// com o objeto
///  do parâmetro no construtor da classe. Exemplo:
/// ```
///     lineEditsInt["NomeParametro"] = ui->le_ObjetoDoParametro;
/// ```
///
class RoboFeiConfig
{
public:
    ///
    /// \brief Construtor padrão da classe
    ///
    RoboFeiConfig();

    ///
    /// \brief Construtor utilizado para inicializar a instância da classe
    /// \param ui - Objeto da interface principal
    ///
    RoboFeiConfig(Ui::RoboFeiSSL* ui);

    ///
    /// \brief Salva as configurações atuais no arquivo _arquivo_
    /// \param arquivo - Arquivo de destino (deve ser aberta antes da função ser
    /// utilizada) \param opcInterface - Instância das opções da interface
    ///
    void salvar(QFile* arquivo, const OpcoesInterface& opcInterface);

    ///
    /// \brief Carrega as configurações do arquivo _arquivo_
    /// \param arquivo - Arquivo de origem (deve ser aberta antes da função ser
    /// utilizada) \param opcInterface - Instância das opções da interface
    ///
    void carregar(QFile* arquivo, OpcoesInterface& opcInterface);

private:
    QMap<QString, QLineEdit*> lineEditsInt; /**< Utilizado com QLineEdit que
                                               guardam números inteiros. */
    QMap<QString, QSpinBox*> spinBoxes;     /**< Utilizado com QSpinBox. */
    QMap<QString, QDoubleSpinBox*>
        doubleSpinBoxes; /**< Utilizado com QDoubleSpinBox. */
    QMap<QString, QComboBox*>
        comboBoxesIndex; /**< Utilizado com QComboBox onde o
                              parâmetro guarda o índice do combobox. */
    QMap<QString, QComboBox*>
        comboBoxesText;                   /**< Utilizado com QComboBox onde o
                                               parâmetro guarda o texto do combobox. */
    QMap<QString, QCheckBox*> checkBoxes; /**< Utilizado com CheckBox. */
};

#endif // ROBOFEICONFIG_H
