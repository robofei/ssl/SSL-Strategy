/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "auxiliar.h"

// Gera um erro no compile-time quando algum switch case não é completamente
//  tratado. Usado na função `Auxiliar::strFromID_Jogadas`
#pragma GCC diagnostic error "-Wswitch"

bool Auxiliar::bChecaInterseccaoObjetosLinha(
    QVector2D _vt2dP1, QVector2D _vt2dP2, bool _bIgnorarBola,
    float _fDistanciaMinima, const QVector<QVector3D>& _vt3dObjetosEmCampo)
{
    // Checa se o ponto P3 intercepta o segmento de reta formado por P1 e P2
    QVector2D vt2dV, vt2dP3, vt2dU, vt2dW, vt2dA;
    float fDistanciaPontoLinha = 0;
    QVector2D vt2dP, Pa, Pb, Pc;

    vt2dV = _vt2dP2 - _vt2dP1; // V→ = P2 - P1

    for (auto& n : _vt3dObjetosEmCampo)
    {
        if (static_cast<TipoObjeto>(n.z()) == otBola &&
            _bIgnorarBola == bNaoConsideraBola)
            continue;

        else
        {
            vt2dP3 = n.toVector2D();

            vt2dU = vt2dP3 - _vt2dP1; // U→ = P3 - P1
            vt2dW = vt2dVetorPerpendicular(
                vt2dU, vt2dV); // vt2dU - ((vt2dU.dotProduct(vt2dU,
                               // vt2dV)/vt2dV.lengthSquared()) * vt2dV);// W→ =
                               // U→ - ( ( (U→ . V→)/ |V→|²) * V→)

            fDistanciaPontoLinha = qFloor(vt2dW.length()); // d = |W→|

            if (fDistanciaPontoLinha < _fDistanciaMinima)
            {
                float fP_PaPb = 0, fPaPb_PaPb = 0, fP_PcPb = 0, fPcPb_PcPb = 0;

                vt2dA = vt2dW;
                vt2dA.normalize(); // A^ = W→/|W→|
                vt2dA *= _fDistanciaMinima;

                Pa = vt2dA + _vt2dP1;
                Pb = _vt2dP2 + vt2dA;
                Pc = _vt2dP2 - vt2dA;
                vt2dP = vt2dP3 - Pb;

                fP_PaPb = vt2dP.dotProduct(vt2dP, (Pa - Pb));
                fPaPb_PaPb = vt2dP.dotProduct((Pa - Pb), (Pa - Pb));
                fP_PcPb = vt2dP.dotProduct(vt2dP, (Pc - Pb));
                fPcPb_PcPb = vt2dP.dotProduct((Pc - Pb), (Pc - Pb));

                if (fP_PaPb >= 0 &&
                    fP_PaPb <= fPaPb_PaPb && // Se 0 < P→ . (Pa-Pb)→ < |Pa-Pb|²
                                             // E 0 < P→ . (Pc-Pb)→ < |Pc-Pb|² o
                                             // ponto P3 está
                    fP_PcPb >= 0 &&
                    fP_PcPb <= fPcPb_PcPb) // dentro do retângulo
                    return true;
            }
            else if (fDistanciaPontoLinha <= 0)
            {
                if (vt2dP3.distanceToPoint(_vt2dP1) <
                        _vt2dP1.distanceToPoint(_vt2dP2) &&
                    vt2dP3.distanceToPoint(_vt2dP2) <
                        _vt2dP1.distanceToPoint(_vt2dP2))
                    return true;
            }
        }
    }
    return false;
}

bool Auxiliar::bChecaInterseccaoObjetosLinha(
    QVector2D _vt2dP1, QVector2D _vt2dP2, float _fDistanciaMinima,
    const QVector2D& _vt2dObjetoEmCampo)
{
    // Checa se o ponto P3 intercepta o segmento de reta formado por P1 e P2
    QVector2D vt2dV, vt2dP3, vt2dU, vt2dW, vt2dA;
    float fDistanciaPontoLinha = 0;

    vt2dV = _vt2dP2 - _vt2dP1; // V→ = P2 - P1
    vt2dP3 = _vt2dObjetoEmCampo;

    vt2dU = vt2dP3 - _vt2dP1; // U→ = P3 - P1
    vt2dW = vt2dVetorPerpendicular(
        vt2dU, vt2dV); // vt2dU - ((vt2dU.dotProduct(vt2dU,
                       // vt2dV)/vt2dV.lengthSquared()) * vt2dV);// W→ = U→ - (
                       // ( (U→ . V→)/ |V→|²) * V→)

    fDistanciaPontoLinha = qFloor(vt2dW.length()); // d = |W→|

    if(fDistanciaPontoLinha <= _fDistanciaMinima/*InterseccaoLinhaCirculo(_vt2dP1, _vt2dP2, _fDistanciaMinima, vt2dP3) == true*/)
    {
        QVector2D vt2dP, Pa, Pb, Pc;
        float fP_PaPb = 0, fPaPb_PaPb = 0, fP_PcPb = 0, fPcPb_PcPb = 0;

        vt2dA = vt2dW;
        vt2dA.normalize(); // A^ = W→/|W→|
        vt2dA *= _fDistanciaMinima;

        Pa = vt2dA + _vt2dP1;
        Pb = _vt2dP2 + vt2dA;
        Pc = _vt2dP2 - vt2dA;
        vt2dP = vt2dP3 - Pb;

        fP_PaPb = vt2dP.dotProduct(vt2dP, (Pa - Pb));
        fPaPb_PaPb = vt2dP.dotProduct((Pa - Pb), (Pa - Pb));
        fP_PcPb = vt2dP.dotProduct(vt2dP, (Pc - Pb));
        fPcPb_PcPb = vt2dP.dotProduct((Pc - Pb), (Pc - Pb));

        if (fP_PaPb >= 0 &&
            fP_PaPb <= fPaPb_PaPb && // Se 0 < P→ . (Pa-Pb)→ < |Pa-Pb|² E 0 < P→
                                     // . (Pc-Pb)→ < |Pc-Pb|² o ponto P3 está
            fP_PcPb >= 0 && fP_PcPb <= fPcPb_PcPb) // dentro do retângulo
            return true;
    }

    return false;
}

bool Auxiliar::bChecaInterseccaoObjetosLinha(
    QVector2D _vt2dP1, QVector2D _vt2dP2, float _fDistanciaMinima,
    const QVector<QVector3D>& _vt3dObjetoEmCampo)
{
    return bChecaInterseccaoObjetosLinha(_vt2dP1, _vt2dP2, bConsideraBola,
                                         _fDistanciaMinima, _vt3dObjetoEmCampo);
}

bool Auxiliar::bChecaInterseccaoObjetosLinha(
    QVector2D _vt2dP1, QVector2D _vt2dP2,
    const QVector<QVector4D>& _vt4dObjetoEmCampo)
{
    bool interseccao = false;
    for (int i = 0; i < _vt4dObjetoEmCampo.size(); ++i)
    {
        interseccao = bChecaInterseccaoObjetosLinha(
            _vt2dP1, _vt2dP2, _vt4dObjetoEmCampo.at(i).w() * 0.9,
            _vt4dObjetoEmCampo.at(i).toVector2D());
        if (interseccao == true)
            return true;
    }
    return false;
}

bool Auxiliar::bChecaInterseccaoLinhaLinha(QVector2D P11, QVector2D P21,
                                           QVector2D P12, QVector2D P22,
                                           QVector2D& Pint)
{
    //    Calculo por equacao matricial demora muito quando executado muitas
    //    vezes seguidas, utilizado na geracao dos grafos para o path-planner O
    //    calculo atual é a solucao da igualdade Y(x) - Y0 = M(x - X0) = Y1(x) -
    //    Y1 = M'(x - X1)

    //    Eigen::MatrixXd mAr(2,2), mBr(2,1), mCr(2,1);

    //    QVector2D U, B;
    //    U = P11 - P21;
    //    B = P12 - P22;

    //    mAr << U.x() , -B.x(),
    //           U.y() , -B.y();

    //    mCr << P12.x() - P11.x(),
    //           P12.y() - P11.y();

    //    mBr = mAr.inverse() * mCr;

    double M = (P11.y() - P21.y() + 0.01) / (P11.x() - P21.x() + 0.01);
    double Mlinha = (P12.y() - P22.y() + 0.01) / (P12.x() - P22.x() + 0.01);
    double X2 =
        (P12.y() - P11.y() + M * P11.x() - Mlinha * P12.x()) / (M - Mlinha);
    double Y2 = M * (X2 - P11.x()) + P11.y();

    if (/*qIsFinite(mBr(1,0))*/ qIsFinite(X2) && qIsFinite(Y2))
    {
        QVector2D PR(X2, Y2); // P11 + static_cast<float>(mBr(0,0))*U; // Pdelta
                              // = P1 + Alfa*U

        Pint = PR;

        if (P11.distanceToPoint(PR) < P11.distanceToPoint(P21) &&
            P21.distanceToPoint(PR) <
                P11.distanceToPoint(P21)) // Esta contido na reta 1
        {
            if (P12.distanceToPoint(PR) < P12.distanceToPoint(P22) &&
                P22.distanceToPoint(PR) <
                    P12.distanceToPoint(P22)) // Esta contido na reta 2
            {
                return true;
            }
        }
    }

    return false;
}

QVector<QVector2D> Auxiliar::vt2dIntervaloLivreGol(
    QVector2D _vt2dPosicaoDeChute, int _iLadoCampoAliado, int _iLarguraGol,
    int _iProfundidadeGol, QSize _szCampo,
    const QVector<QVector3D>& _vt3dPosicoesOponentes, int larguraMinimaDaMira)
{
    QVector2D vt2dPontoGol_1;
    QVector<QVector2D> vt2dIntervaloChute;
    QVector<QVector2D> vt2dIntervaloChuteFinal;
    vt2dIntervaloChuteFinal.clear();
    vt2dIntervaloChute.clear();
    _iProfundidadeGol = 0; // Retirado do calculo para que o robô mire na linha
                           // do gol e não atrás do gol

    // Devemos inverter o sinal do iLadoCampo pois ele sinaliza o lado que nós
    // defendemos, portanto, o lado que atacamos é o inverso dele
    vt2dPontoGol_1.setX((_szCampo.width() / 2 + _iProfundidadeGol) *
                        _iLadoCampoAliado * -1);
    vt2dPontoGol_1.setY(-_iLarguraGol / 2);

    for (int i = 0; i < _iLarguraGol / (globalConfig.ballDiameter / 2); ++i)
    {
        // Se o ponto está livre, adiciona-o na lista
        if (Auxiliar::bChecaInterseccaoObjetosLinha(
                _vt2dPosicaoDeChute, vt2dPontoGol_1, bNaoConsideraBola,
                larguraMinimaDaMira, _vt3dPosicoesOponentes) == false)
        {
            vt2dIntervaloChute.append(vt2dPontoGol_1);
        }
        else if (vt2dIntervaloChuteFinal.isEmpty())
        {
            vt2dIntervaloChuteFinal = vt2dIntervaloChute;
            vt2dIntervaloChute.clear();
        }
        else if (!vt2dIntervaloChute.isEmpty())
        {
            double anguloAtual = Auxiliar::dAnguloVetor1Vetor2(
                QVector2D(vt2dIntervaloChute.first().x(),
                          vt2dIntervaloChute.first().y() -
                              globalConfig.ballDiameter / 2) -
                    _vt2dPosicaoDeChute,
                QVector2D(vt2dIntervaloChute.last().x(),
                          vt2dIntervaloChute.last().y() +
                              globalConfig.ballDiameter / 2) -
                    _vt2dPosicaoDeChute);
            double anguloAtualMaximo = Auxiliar::dAnguloVetor1Vetor2(
                QVector2D(vt2dIntervaloChuteFinal.first().x(),
                          vt2dIntervaloChuteFinal.first().y() -
                              globalConfig.ballDiameter / 2) -
                    _vt2dPosicaoDeChute,
                QVector2D(vt2dIntervaloChuteFinal.last().x(),
                          vt2dIntervaloChuteFinal.last().y() +
                              globalConfig.ballDiameter / 2) -
                    _vt2dPosicaoDeChute);
            if (anguloAtual > anguloAtualMaximo)
                vt2dIntervaloChuteFinal = vt2dIntervaloChute;

            vt2dIntervaloChute.clear();
        }

        /*
        else//Fim de um intervalo livre
        {
            //Salva o maior intervalo de pontos
            if(vt2dIntervaloChute.size() > vt2dIntervaloChuteFinal.size())
            {
                vt2dIntervaloChuteFinal.clear();
                vt2dIntervaloChuteFinal = vt2dIntervaloChute;
                vt2dIntervaloChute.clear();
            }
        }*/

        vt2dPontoGol_1.setY(vt2dPontoGol_1.y() + globalConfig.ballDiameter / 2);
    }

    if (vt2dIntervaloChute.size() > vt2dIntervaloChuteFinal.size())
        vt2dIntervaloChuteFinal = vt2dIntervaloChute;

    return vt2dIntervaloChuteFinal;
}

int Auxiliar::iCalculaQuadrante(double _angulo)
{
    // -180,180
    //    if(_angulo >= 0 && _angulo < M_PI/2.0)//Primeiro
    //    {
    //        return 1;
    //    }
    //    else if(_angulo > M_PI/2.0 && _angulo < M_PI)//Segundo
    //    {
    //        return 2;
    //    }
    //    else if(_angulo < -M_PI/2.0 && _angulo > -M_PI)//Terceiro
    //    {
    //        return 3;
    //    }
    //    else if(_angulo < 0 && _angulo > -M_PI/2.0)//Quarto
    //    {
    //        return 4;
    //    }

    // 0 360
    if (_angulo >= 0 && _angulo < M_PI / 2.0) // Primeiro
    {
        return 1;
    }
    else if (_angulo > M_PI / 2.0 && _angulo < M_PI) // Segundo
    {
        return 2;
    }
    else if (_angulo < 3.0 * M_PI / 4.0 && _angulo > M_PI) // Terceiro
    {
        return 3;
    }
    else if (_angulo < 2 * M_PI && _angulo > 3.0 * M_PI / 4.0) // Quarto
    {
        return 4;
    }

    return 0;
}

QVector2D Auxiliar::vt2dCalculaDefesaGoleiro(QVector<QVector2D> _vtBufferBola,
                                             int _iLadoCampoAliado,
                                             int _iLarguraGol, QSize _szCampo,
                                             float _avancoGoleiro, bool delta,
                                             bool* _hasIntersection) //**
{
    float goalieX = _szCampo.width() / 2 - _avancoGoleiro;
    QVector2D pontoGoleiro(_iLadoCampoAliado * goalieX, 0);

    if (_vtBufferBola.isEmpty())
        return pontoGoleiro;

    QLineF linhaGol;
    linhaGol.setP1(
        QPointF(_iLadoCampoAliado * (_szCampo.width() / 2.0 - _avancoGoleiro),
                _iLarguraGol / 2.0 - globalConfig.robotDiameter / 2.0));
    linhaGol.setP2(
        QPointF(_iLadoCampoAliado * (_szCampo.width() / 2.0 - _avancoGoleiro),
                -_iLarguraGol / 2.0 + globalConfig.robotDiameter / 2.0));
    QLineF retaBola = Auxiliar::lineCalculaRetaMedia(_vtBufferBola);

    if (qRound(retaBola.length()) > 1) // A reta da bola tem mais de 1mm
    {
        bool intersection = Auxiliar::bChecaInterseccaoLinhaLinha(
            QVector2D(retaBola.p1()), QVector2D(retaBola.p2()),
            QVector2D(linhaGol.p1()), QVector2D(linhaGol.p2()), pontoGoleiro);
        if (_hasIntersection != nullptr)
        {
            *_hasIntersection = intersection;
        }

        if (qAbs(pontoGoleiro.y()) >
            _iLarguraGol / 2 + globalConfig.robotDiameter / 2)
        {
            pontoGoleiro = QVector2D(
                _iLadoCampoAliado * (_szCampo.width() / 2 - _avancoGoleiro), 0);
        }
    }
    else if (delta) // Se for o delta e a bolinha esta parada
    {
        pontoGoleiro = QVector2D(0, 0);
    }

    return pontoGoleiro;
}

bool Auxiliar::bPontoContidoRetangulo(QVector2D ponto, QVector2D R1,
                                      QVector2D R2)
{
    QRect retangulo(R1.toPoint(), R2.toPoint());
    return retangulo.contains(ponto.toPoint());
}

QVector4D Auxiliar::vt4dConverteLinearRotacao(QVector2D vt2dVel, int iVw)
{
    Eigen::MatrixXd W(4, 1);
    Eigen::MatrixXd MR(4, 3);
    Eigen::MatrixXd V(3, 1);
    double teta = 33 * M_PI / 180;

    W << 0, 0, 0, 0;

    MR << -sin(teta), cos(teta), 1, sin(teta), cos(teta), 1, sin(teta),
        -cos(teta), 1, -sin(teta), -cos(teta), 1;

    V << vt2dVel.x(), vt2dVel.y(), iVw;

    W = MR * V;

    double max =
        qMax(qMax(qAbs(W(3)), qAbs(W(2))), qMax(qAbs(W(1)), qAbs(W(0))));
    if (max > 127)
    {
        double ajuste = max / 127;
        W(3) /= ajuste;
        W(2) /= ajuste;
        W(1) /= ajuste;
        W(0) /= ajuste;
    }

    return QVector4D(W(3), W(2), W(1), W(0));
}

double Auxiliar::dAnguloVetor1Vetor2(QVector2D vetor1, QVector2D vetor2)
{
    double ang = (180 / M_PI) * qAcos(vetor1.dotProduct(vetor1, vetor2) /
                                      (vetor1.length() * vetor2.length()));
    if (qIsNaN(ang))
    {
        ang = 0;
    }
    return ang;
}

QVector2D Auxiliar::vt2dVetorPerpendicular(QVector2D V1, QVector2D V2)
{
    return (V1 - ((V1.dotProduct(V1, V2) / V2.lengthSquared()) *
                  V2)); // W→ = V1→ - ( ( (V1→ . V2→)/ |V2→|²) * V2→)
}

QVector2D Auxiliar::vt2dConverteOdometria(double odRoda1, double odRoda2,
                                          double odRoda3, double odRoda4)
{
    double teta = 33 * M_PI / 180;

    QVector2D V, V4 = QVector2D(1 / -sin(teta), 1 / cos(teta)),
                 V3 = QVector2D(1 / sin(teta), 1 / cos(teta)),
                 V2 = QVector2D(1 / sin(teta), 1 / -cos(teta)),
                 V1 = QVector2D(1 / -sin(teta), 1 / -cos(teta));

    V4 *= odRoda4;
    V3 *= odRoda3;
    V2 *= odRoda2;
    V1 *= odRoda1;

    V = (V1 + V2 + V3 + V4) / 4;

    // V = vt2dConverteVRoboVCampo(velNaoConvertida, V, anguloRobo);

    return V;
}

QVector<QVector2D> Auxiliar::vt2dPontosDeltaQuadrado(
    QVector<QVector2D> _vt2dPontos, int _iNRobos, QVector2D _vt2dPosicaoGoleiro,
    int _iLadoCampo, int _iLarguraGol, int _iProfundidadeGol, QSize _szCampo,
    int _iContador)
{
    QVector<QVector2D> ptosDelta;
    ptosDelta.clear();

    if (_vt2dPontos.size() == 6 && _iNRobos > 0)
    {

        Eigen::MatrixXd mAr(2, 2), mAs(2, 2), mAt(2, 2), // Arst
            mBr(2, 1), mBs(2, 1), mBt(2, 1),             // Brst
            mCr(2, 1), mCs(2, 1), mCt(2, 1);             // Crst

        QVector2D _vt2dP1 = _vt2dPontos.at(0), _vt2dP2 = _vt2dPontos.at(1),
                  _vt2dP3 = _vt2dPontos.at(2), _vt2dP4 = _vt2dPontos.at(3),
                  _vt2dPbk = _vt2dPontos.at(4), _vt2dPbk_1 = _vt2dPontos.at(5);

        _vt2dPbk.setX(_vt2dPbk.x());
        _vt2dPbk.setY(_vt2dPbk.y());

        _vt2dPbk_1.setX(_vt2dPbk_1.x());
        _vt2dPbk_1.setY(_vt2dPbk_1.y());

        QVector2D vt2dU, vt2dV, vt2dW, vt2dB;
        vt2dU = _vt2dP1 - _vt2dP2;
        vt2dV = _vt2dP3 - _vt2dP2;
        vt2dW = _vt2dP4 - _vt2dP3;
        vt2dB = _vt2dPbk - _vt2dPbk_1;

        mAr << static_cast<double>(vt2dU.x()),
            static_cast<double>(-vt2dB.x()), // |Ux -Bx| RETA R
            static_cast<double>(vt2dU.y()),
            static_cast<double>(-vt2dB.y()); // |Uy -By|

        mAs << static_cast<double>(vt2dV.x()),
            static_cast<double>(-vt2dB.x()), // |Vx -Bx| RETA S
            static_cast<double>(vt2dV.y()),
            static_cast<double>(-vt2dB.y()); // |Vy -By|

        mAt << static_cast<double>(vt2dW.x()),
            static_cast<double>(-vt2dB.x()), // |Wx -Bx| RETA T
            static_cast<double>(vt2dW.y()),
            static_cast<double>(-vt2dB.y()); // |Wy -By|

        mCr << static_cast<double>(_vt2dPbk.x() -
                                   _vt2dP1.x()), // |Pbkx-P1x| RETA R
            static_cast<double>(_vt2dPbk.y() - _vt2dP1.y()); // |Pbky-P1y|

        mCs << static_cast<double>(_vt2dPbk.x() -
                                   _vt2dP3.x()), // |Pbkx-P1x| RETA S
            static_cast<double>(_vt2dPbk.y() - _vt2dP3.y()); // |Pbky-P1y|

        mCt << static_cast<double>(_vt2dPbk.x() -
                                   _vt2dP4.x()), // |Pbkx-P1x| RETA T
            static_cast<double>(_vt2dPbk.y() - _vt2dP4.y()); // |Pbky-P1y|

        // Arst*Brst = Crst -> Brst = (Arst)-1 * Crst
        mBr = mAr.inverse() * mCr;
        mBs = mAs.inverse() * mCs;
        mBt = mAt.inverse() * mCt;

        QVector2D PdeltaR(9999999999.999, 9999999999.999),
            PdeltaS(9999999999.999, 9999999999.999),
            PdeltaT(9999999999.999, 9999999999.999), Pdelta;

        // Só calcula o ponto se existir solução para o sistema
        if (qIsFinite(mBr(0, 0)))
            PdeltaR = _vt2dP1 + static_cast<float>(mBr(0, 0)) *
                                    vt2dU; // Pdelta = P1 + Alfa*U

        if (qIsFinite(mBs(0, 0)))
            PdeltaS = _vt2dP3 + static_cast<float>(mBs(0, 0)) *
                                    vt2dV; // Pdelta = P3 + Beta*V

        if (qIsFinite(mBt(0, 0)))
            PdeltaT = _vt2dP4 + static_cast<float>(mBt(0, 0)) *
                                    vt2dW; // Pdelta = P4 + Gama*W

        float fDistR, fDistP1PR, fDistP2PS, fDistP2PR, fDistS, fDistP3PS,
            fDistP3PT, fDistT, fDistP4PT;

        char cRetaDelta = 'N';

        fDistR = PdeltaR.distanceToPoint(_vt2dPbk);
        fDistS = PdeltaS.distanceToPoint(_vt2dPbk);
        fDistT = PdeltaT.distanceToPoint(_vt2dPbk);

        // Escolhe qual o ponto certo para o delta ficar

        // Define os pontos válidos para o delta estar (pontos que estão no
        // retângulo da área de defesa)
        bool bPontoR = false, bPontoS = false, bPontoT = false;

        fDistP1PR = PdeltaR.distanceToPoint(_vt2dP1);
        fDistP2PR = PdeltaR.distanceToPoint(_vt2dP2);
        fDistP2PS = PdeltaS.distanceToPoint(_vt2dP2);
        fDistP3PS = PdeltaS.distanceToPoint(_vt2dP3);
        fDistP3PT = PdeltaT.distanceToPoint(_vt2dP3);
        fDistP4PT = PdeltaT.distanceToPoint(_vt2dP4);

        // Checa se os pontos encontrados estao na regiao do delta
        if (fDistP1PR <= vt2dU.length() && fDistP2PR <= vt2dU.length())
            bPontoR = true;
        if (fDistP3PS <= vt2dV.length() && fDistP2PS <= vt2dV.length())
            bPontoS = true;
        if (fDistP4PT <= vt2dW.length() && fDistP3PT <= vt2dW.length())
            bPontoT = true;

        if ((!bPontoS | (fDistR < fDistS)) && (!bPontoT | (fDistR < fDistT)) &&
            qIsFinite(mBr(0, 0))) // O ponto do delta na reta R está mais perto
                                  // da bola e existe solução para o sistema
        {
            if (bPontoR) // O ponto do delta está no segmento definido por P1 e
                         // P2
            {
                Pdelta = PdeltaR;
                cRetaDelta = 'R';
            }
        }

        if (((!bPontoR | (fDistS < fDistR)) && (!bPontoT | (fDistS < fDistT)) &&
             qIsFinite(mBs(0, 0))) ||
            (qIsFinite(mBs(0, 0)) && cRetaDelta == 'N' &&
             (!bPontoT |
              (fDistS <
               fDistT)))) // O ponto do delta na reta S está mais perto da bola
                          // e existe solução para o sistema e ainda nao foi
                          // encontrado um ponto aceitavel
        {
            if (bPontoS) // O ponto do delta está no segmento definido por P2 e
                         // P3
            {
                Pdelta = PdeltaS;
                cRetaDelta = 'S';
            }
        }

        if (((!bPontoR | (fDistT < fDistR)) && (!bPontoS | (fDistT < fDistS)) &&
             qIsFinite(mBt(0, 0))) ||
            (qIsFinite(mBs(0, 0)) &&
             cRetaDelta == 'N')) // O ponto do delta na reta T está mais perto
                                 // da bola e existe solução para o sistema e
                                 // ainda nao foi encontrado um ponto aceitavel
        {
            if (bPontoT) // O ponto do delta está no segmento definido por P3 e
                         // P4
            {
                Pdelta = PdeltaT;
                cRetaDelta = 'T';
            }
        }

        // Faz a distribuição dos robôs entorno do ponto do delta

        if (Pdelta.length() > 80) // A bolinha esta andando, portanto, existe
                                  // solucao para o sistema e ela e valida
        {
            // qDebug() << "Ponto Delta: " << Pdelta << "Pbk: " << _vt2dPbk <<
            // "Pbk-1: " << _vt2dPbk_1 << "Reta: " << cRetaDelta;

            QVector2D verA; // versor A e -A sera utilizado para gerar os pontos
                            // à direita e à esquerda do ponto do delta

            switch (cRetaDelta)
            {
            case 'R': {
                verA = vt2dU;
                verA.normalize();
            }
            break;

            case 'S': {
                verA = vt2dV;
                verA.normalize();
            }
            break;

            case 'T': {
                verA = vt2dW;
                verA.normalize();
            }
            break;
            }

            int iEspacos = 0;
            verA *= globalConfig.delta .spacing; // Um pouco mais que o
                                                 // diâmetro de um robô
                                                 // para o outro (180mm*1.3 = 234mm)

            if (_iNRobos % 2 != 0) // Robos impares para o delta
            {
                iEspacos = (_iNRobos - 1) / 2;

                if (iEspacos > 0) // Se houver mais de 1 robô para formar o
                                  // delta
                {
                    ptosDelta.append(
                        (-verA * iEspacos +
                         Pdelta)); // Adiciona o ultimo ponto à esquerda
                    ptosDelta.append(
                        (verA * iEspacos +
                         Pdelta)); // Adiciona o ultimo ponto à direita
                }
                else // Se houver somente 1 robô
                {
                    ptosDelta.append(Pdelta);
                    return ptosDelta;
                }
            }
            else // Robos pares
            {
                iEspacos = (_iNRobos) / 2;

                ptosDelta.append(
                    (-verA * iEspacos +
                     Pdelta)); // Adiciona o ultimo ponto à esquerda
                ptosDelta.append((verA * iEspacos +
                                  Pdelta)); // Adiciona o ultimo ponto à direita
            }

            ptosDelta = vt2dPosicionaRobosVetorPontos(
                ptosDelta, globalConfig.delta.spacing, _iNRobos);

            return ptosDelta;
        }
        else // A bolinha esta parada, o delta deve companha-la
        {
            QVector2D vt2dPontoDelta = (_vt2dP1 + _vt2dP4) / 2;

            _vt2dPontos.replace(5, vt2dPontoDelta);
            _iContador++; // Contador para impedir que a função se chame
                          // infinitamente

            if (_iContador > 20) // Força a retornar o ponto do meio da area
            {
                return ptosDelta;
            }

            ptosDelta = vt2dPontosDeltaQuadrado(
                _vt2dPontos, _iNRobos, _vt2dPosicaoGoleiro, _iLadoCampo,
                _iLarguraGol, _iProfundidadeGol, _szCampo, _iContador);
        }
    }

    return ptosDelta;
}

QVector<QVector2D> Auxiliar::vt2dPontosKickOff(
    int iRaioDelta, QVector<QVector2D> _vt2dCentrosCirculoDefesa,
    QVector<QVector2D> _vt2dPontos, int _iNRobos, QVector2D _vt2dPosicaoGoleiro,
    int _iLadoCampo, int _iLarguraGol, int _iProfundidadeGol, QSize _szCampo,
    int _iContador)
{
    /* _vt2dCentrosCirculoDefesa
    [0] = Circulo positivo
    [1] = Circulo negativo
    */
    QVector<QVector2D> vt2dPontosDeltaQuad = vt2dPontosDeltaQuadrado(
        _vt2dPontos, _iNRobos, _vt2dPosicaoGoleiro, _iLadoCampo, _iLarguraGol,
        _iProfundidadeGol, _szCampo, _iContador);
    QVector2D centroPositivo = _vt2dCentrosCirculoDefesa.at(0),
              centroNegativo = _vt2dCentrosCirculoDefesa.at(1),
              centroAproximacao, // Centro utilizado para reajustar os pontos
        pontoCorrigido;

    if (vt2dPontosDeltaQuad.size() > 0)
    {
        for (int i = 0; i < vt2dPontosDeltaQuad.size(); ++i)
        {
            if (vt2dPontosDeltaQuad.at(i).y() > 0)
                centroAproximacao = centroPositivo;
            else
                centroAproximacao = centroNegativo;
            if (qAbs(centroAproximacao.y()) <
                qAbs(vt2dPontosDeltaQuad.at(i).y()))
            {
                pontoCorrigido = vt2dReajustaPonto(
                    centroAproximacao, vt2dPontosDeltaQuad.at(i), iRaioDelta);
                vt2dPontosDeltaQuad.replace(i, pontoCorrigido);
            }
        }
    }

    return vt2dPontosDeltaQuad;
}

QVector2D Auxiliar::vt2dCalculaPontoCobranca(QVector2D _vt2dPontoMira,
                                             QVector2D _vt2dPontoBola,
                                             float _fDistanciaRoboBola)
{
    return _vt2dPontoBola +
           _fDistanciaRoboBola * (_vt2dPontoBola - _vt2dPontoMira).normalized();
}

int Auxiliar::iCalculaRegiaoPonto(QVector2D _vt2dPonto, int _iLarguraCampo,
                                  int _iLarguraRegiao)
{
    double dNumeroRegioes = _iLarguraCampo / _iLarguraRegiao;
    double dRegiao = 0;
    dRegiao = ((dNumeroRegioes) / _iLarguraCampo) *
                  static_cast<double>(_vt2dPonto.x()) +
              ((dNumeroRegioes) / 2);
    return qFloor(dRegiao);
}

QVector2D Auxiliar::vt2dReajustaPonto(QVector2D _vt2dPonto1,
                                      QVector2D _vt2dPonto2,
                                      float fDistanciaPonto1)
{
    QVector2D vt2dP, vt2dU, vt2dPonto3;

    vt2dP = _vt2dPonto2 - _vt2dPonto1;
    vt2dP.normalize();

    vt2dU = vt2dP * fDistanciaPonto1;

    vt2dPonto3 = vt2dU + _vt2dPonto1;

    return vt2dPonto3;
}

QVector<QVector2D> Auxiliar::vt2dPosicionaRobosVetorPontos(
    QVector<QVector2D> _vt2dPontosPosicionamento, int _iEspacamentoRobos,
    int _iQuantidadeEspacos)
{
    QVector<QVector2D> vt2dPosicoesFinais;
    vt2dPosicoesFinais.clear();

    QVector2D vt2dP, vt2dA, vt2dPrimeiroPonto, PMed;
    //    QVector2D vt2dP1;

    if (!_vt2dPontosPosicionamento.isEmpty())
    {
        vt2dP = _vt2dPontosPosicionamento.last() -
                _vt2dPontosPosicionamento.first();
        vt2dP.normalize();
        vt2dP *= _iEspacamentoRobos;

        PMed = (_vt2dPontosPosicionamento.last() +
                _vt2dPontosPosicionamento.first()) /
               2; //

        // vt2dPrimeiroPonto = _vt2dPontosPosicionamento.first();
        vt2dPrimeiroPonto = PMed;

        vt2dPosicoesFinais.append(vt2dPrimeiroPonto);

        for (int n = 0; n < qFloor(_iQuantidadeEspacos / 2.0); ++n)
        {
            vt2dA = vt2dP + vt2dPrimeiroPonto;
            vt2dPosicoesFinais.append(vt2dA);
            vt2dPrimeiroPonto = vt2dA;
        }
        vt2dPrimeiroPonto = PMed;
        for (int n = 0; n < qCeil(_iQuantidadeEspacos / 2.0); ++n)
        {
            vt2dA = -vt2dP + vt2dPrimeiroPonto;
            vt2dPosicoesFinais.append(vt2dA);
            vt2dPrimeiroPonto = vt2dA;
        }

        //        while(_iQuantidadeEspacos > 1)
        //        {
        //            vt2dA = vt2dP + vt2dPrimeiroPonto;

        //            vt2dP1 = (vt2dA + vt2dPrimeiroPonto)/2;

        //            vt2dPosicoesFinais.append(vt2dA);//Antes era vt2dP1

        //            vt2dPrimeiroPonto = vt2dA;

        //            _iQuantidadeEspacos--;
        //        }
        //        vt2dPosicoesFinais.append(_vt2dPontosPosicionamento.last());
    }

    return vt2dPosicoesFinais;
}

QVector2D Auxiliar::vt2dRotaciona(QVector2D _vt2dP1, QVector2D _vt2dP2,
                                  double _dAngulo_graus)
{
    QVector2D vt2dAux = _vt2dP2 - _vt2dP1;
    QVector2D vt2dPontoRot;

    _dAngulo_graus = qDegreesToRadians(_dAngulo_graus);

    vt2dPontoRot.setX(vt2dAux.x() * static_cast<float>(qCos(_dAngulo_graus)) -
                      vt2dAux.y() * static_cast<float>(qSin(_dAngulo_graus)));
    vt2dPontoRot.setY(vt2dAux.x() * static_cast<float>(qSin(_dAngulo_graus)) +
                      vt2dAux.y() * static_cast<float>(qCos(_dAngulo_graus)));

    vt2dPontoRot = vt2dPontoRot + _vt2dP1;

    return vt2dPontoRot;
}

QLineF Auxiliar::lineCalculaRetaMedia(QVector<QVector2D> _pontos) //**
{
    float fMediaX = 0, fMediaY = 0,
          /*fVarianciaX = 0, fVarianciaY = 0,*/ numb = 0, denb = 0;
    float a, b;
    QVector2D Pi, Pf;

    for (int i = 0; i < _pontos.size(); i++)
    {

        fMediaX += _pontos.at(i).x();
        fMediaY += _pontos.at(i).y();
    }

    fMediaX = fMediaX / (_pontos.size());
    fMediaY = fMediaY / (_pontos.size());

    for (int j = 0; j < _pontos.size(); j++)
    {
        numb += _pontos.at(j).x() * (_pontos.at(j).y() - fMediaY);
        denb += _pontos.at(j).x() * (_pontos.at(j).x() - fMediaX);
    }

    b = numb / denb;

    a = fMediaY - b * fMediaX;

    if (qIsFinite(a) && qIsFinite(b))
    {
        Pi = QVector2D(_pontos.at(0).x(), b * _pontos.at(0).x() + a);
        Pf = QVector2D(_pontos.last().x(), b * _pontos.last().x() + a);
    }
    else
    {
        Pi = Pf = _pontos.last();
    }

    return {Pi.toPointF(), Pf.toPointF()};
}

bool Auxiliar::bPontoDentroArea(QSize tamanhoCampo, QSize areaDefesa,
                                int ladoCampo, QVector2D ponto)
{
    QVector2D P1Def = QVector2D(tamanhoCampo.width() / 2 * ladoCampo,
                                areaDefesa.height() / 2),
              P2Def = QVector2D(
                  (tamanhoCampo.width() / 2 - areaDefesa.width()) * ladoCampo,
                  -areaDefesa.height() / 2);

    return bPontoContidoRetangulo(ponto, P1Def, P2Def);
}

QVector<QPair<int, int>> Auxiliar::nIDnDestOtimizaDestinosRobos(
    QVector<int> iIDsRobos, QVector<QVector2D> vt2dPosAtualRobos,
    QVector<QVector2D> vt2dDestinos, const char* str)
{
    //    QElapsedTimer timer;
    //    timer.start();
    int nRobos = iIDsRobos.size(), nDestinos = vt2dDestinos.size();
    QVector<QPair<int, int>> nIDnDestOtimizados;
    nIDnDestOtimizados.clear();
    QVector<int> robosAtribuidos;
    robosAtribuidos.clear();
    typedef std::pair<float, bool> ID_Dest;

    // O numero de robos e de destinos DEVE ser igual
    if (nRobos == nDestinos && nRobos == vt2dPosAtualRobos.size())
    {
        QVector<int> IndicePosicoesSorteadas;
        IndicePosicoesSorteadas
            .clear(); // Armazena os indices das posicoes que ja foram sorteadas
                      // para nao atribuir o mesmo destino para mais de um robo
        std::vector<std::vector<ID_Dest>>
            matrizDistancias; // Armazena a distancia de cada robo para cada
                              // ponto
        matrizDistancias = std::vector<std::vector<ID_Dest>>(
            nRobos, std::vector<ID_Dest>(nDestinos));

        // Preenche a matriz com as distancias de cada robo para cada destino
        for (int Destino = 0; Destino < nDestinos; ++Destino)
        {
            for (int Robo = 0; Robo < nRobos; ++Robo)
            {
                matrizDistancias[Robo][Destino].first =
                    vt2dPosAtualRobos.at(Robo).distanceToPoint(
                        vt2dDestinos.at(Destino));
                matrizDistancias[Robo][Destino].second = false;
                //                std::cout <<
                //                matrizDistancias[Robo][Destino].first << " ";
            }
            //            std::cout << "\n";
        }
        //        for(int n=0; n < nRobos; n++)
        //        {
        //            std::cout << "R" << n << "Pos = (" <<
        //            vt2dPosAtualRobos.at(n).x() << " , " <<
        //            vt2dPosAtualRobos.at(n).y() << ")";
        //        }
        //        std::cout << "\n";
        //        for(int n=0; n < nDestinos; n++)
        //        {
        //            std::cout << "D" << n << "Pos = (" <<
        //            vt2dDestinos.at(n).x() << " , " << vt2dDestinos.at(n).y()
        //            << ")";
        //        }
        //        std::cout << "\n";
        float distAtualRobo1, distAtualRobo2; // Guarda a distancia para o
                                              // destino atual sendo atribuido
        int iMenorDistRobo1, iMenorDistRobo2; // Guarda o indice do destino de
                                              // menor distancia do robo
        float segundaMenorDist1, segundaMenorDist2; // Guarda a segunda menor
                                                    // distancia dos robos 1 e 2
        int iRoboEscolhido = 0; // Robo escolhido para o destino atual
        for (int iDestino = 0; iDestino < nDestinos; ++iDestino)
        {
            for (int iRobo_1 = 0; iRobo_1 < nRobos - 1;
                 ++iRobo_1) // Compara o robo 1 com todos os outros para o mesmo
                            // destino
            {
                if (/*matrizDistancias[iRobo_1][iDestino].second == false && */
                    robosAtribuidos.contains(iRobo_1) ==
                    false) // Se nenhum destino nao foi atribuido para o robo 1
                {
                    distAtualRobo1 = matrizDistancias[iRobo_1][iDestino].first;
                    segundaMenorDist1 = 10 * distAtualRobo1;
                    iMenorDistRobo1 = 0;

                    for (int n = 0; n < nDestinos;
                         ++n) // Encontra o indice do destino de menor distancia
                              // pro robo 1
                    {
                        if (matrizDistancias[iRobo_1][n].first <
                            matrizDistancias[iRobo_1][iMenorDistRobo1].first)
                            iMenorDistRobo1 = n;
                    }

                    for (int iRobo_2 = iRobo_1 + 1; iRobo_2 < nRobos;
                         ++iRobo_2) // Compara o robo 1 com todos os outros
                    {
                        if (/*matrizDistancias[iRobo_2][iDestino].second ==
                               false && */
                            robosAtribuidos.contains(iRobo_2) ==
                            false) // Se o outro robo ainda nao tem nenhum
                                   // destino atribuido
                        {
                            distAtualRobo2 =
                                matrizDistancias[iRobo_2][iDestino].first;
                            segundaMenorDist2 = 10 * distAtualRobo2;
                            iMenorDistRobo2 = 0;

                            for (int n = 0; n < nDestinos;
                                 ++n) // Encontra o indice do destino de menor
                                      // distancia pro robo 2
                            {
                                if (matrizDistancias[iRobo_2][n].first <
                                    matrizDistancias[iRobo_2][iMenorDistRobo2]
                                        .first)
                                    iMenorDistRobo2 = n;
                            }
                            // Se o robo 1 e o 2 estao mais proximos do mesmo
                            // destino
                            if (iMenorDistRobo1 == iDestino &&
                                iMenorDistRobo2 == iDestino)
                            {
                                for (int n = 0; n < nDestinos; ++n)
                                {
                                    if (matrizDistancias[iRobo_1][n].first <
                                            segundaMenorDist1 &&
                                        n != iMenorDistRobo1)
                                        segundaMenorDist1 =
                                            matrizDistancias[iRobo_1][n].first;
                                    if (matrizDistancias[iRobo_2][n].first <
                                            segundaMenorDist2 &&
                                        n != iMenorDistRobo2)
                                        segundaMenorDist2 =
                                            matrizDistancias[iRobo_2][n].first;
                                }
                                // esse destino sera atribuido ao robo que
                                // possuir a segunda menor distancia
                                /// \todo O iRobo_1 nunca é utilizado
                                if (segundaMenorDist1 > segundaMenorDist2)
                                {
                                    iRoboEscolhido = iRobo_1;
                                }
                                iRoboEscolhido = iRobo_2;
                            }
                            else if (distAtualRobo1 <
                                     distAtualRobo2) // Senao, sera atribuido ao
                                                     // robo mais proximo
                            {
                                iRoboEscolhido = iRobo_1;
                            }
                            else if (distAtualRobo2 <
                                     distAtualRobo1) // Senao, sera atribuido ao
                                                     // robo mais proximo
                            {
                                iRoboEscolhido = iRobo_2;
                            }
                            if (iRoboEscolhido != iRobo_1)
                                break;
                        }
                    }
                    if (iRoboEscolhido == iRobo_1)
                        break;
                }
            }
            matrizDistancias[iRoboEscolhido][iDestino].second = true;
            robosAtribuidos.append(iRoboEscolhido);
            iRoboEscolhido = 0;
            while (robosAtribuidos.contains(iRoboEscolhido))
            {
                iRoboEscolhido++;
            }
        }
        QPair<int, int> aux;
        for (int iRobo = 0; iRobo < nRobos; ++iRobo)
        {
            for (int iDest = 0; iDest < nDestinos; ++iDest)
            {
                if (matrizDistancias[iRobo][iDest].second == true)
                {
                    aux.first = iIDsRobos.at(iRobo);
                    aux.second = iDest;
                    nIDnDestOtimizados.append(aux);
                }
            }
        }
    }
    else
    {
        qDebug() << "Funcao: "
                 << "nIDnDestOtimizaDestinosRobos; "
                 << "Valor dos parametros errados! "
                 << "Chamado por: " << str;
    }

    //    qDebug() << "Tempo otimizacao = " << timer.nsecsElapsed()/1e6 << "
    //    ms";
    return nIDnDestOtimizados;
}

void Auxiliar::vPosicaoNoDelta(const QVector2D& vt2dPosicaoAtual,
                               const QVector2D& vt2dDestino,
                               const QSize& szCampo, const int& iLarguraDefesa,
                               const int& iProfundidadeDefesa,
                               const int& iLadoCampo,
                               QVector<QVector2D>& vt2dCaminhoDelta)
{

    const int i_xlimite =
        (iLadoCampo < 0)
            ? -szCampo.width() / 2 + iProfundidadeDefesa
            : szCampo.width() / 2 -
                  iProfundidadeDefesa; // posição x na linha da área

    const int i_ylimite = iLarguraDefesa / 2; // posição y na linha da área

    const int iX_atual = vt2dPosicaoAtual.x();
    const int iY_atual = vt2dPosicaoAtual.y();

    const bool bNafrente =
        (qAbs(i_xlimite - iX_atual) <= globalConfig.robotDiameter);
    const bool bNalateral =
        (qAbs(i_ylimite - iY_atual) <= 2 * globalConfig.robotDiameter ||
         qAbs(i_ylimite + iY_atual) <= 2 * globalConfig.robotDiameter);

    if (!bNalateral &&
        !bNafrente) // o robô está um pouco distante da linha da área
    {
        if (qAbs(vt2dDestino.y() - iY_atual) <= globalConfig.robotDiameter)
        {
            vMovimentoYConstante(vt2dCaminhoDelta, vt2dPosicaoAtual,
                                 vt2dDestino);
        }
        else
        {
            vMovimentoXConstante(vt2dCaminhoDelta, vt2dPosicaoAtual,
                                 vt2dDestino);
        }
    }
    else if (bNalateral && bNafrente) // o robô está na ponta
    {

        if (abs(vt2dDestino.y() - iY_atual) <= globalConfig.robotDiameter)
        {
            vMovimentoYConstante(vt2dCaminhoDelta, vt2dPosicaoAtual,
                                 vt2dDestino);
        }
        else
        {
            vMovimentoXConstante(vt2dCaminhoDelta, vt2dPosicaoAtual,
                                 vt2dDestino);
        }
    }

    else if (bNafrente) // o robô está na frente
    {
        if (qAbs(iY_atual - vt2dDestino.y()) > globalConfig.robotDiameter)
            vMovimentoXConstante(vt2dCaminhoDelta, vt2dPosicaoAtual,
                                 vt2dDestino);
        else
            vMovimentoYConstante(vt2dCaminhoDelta, vt2dPosicaoAtual,
                                 vt2dDestino);
    }

    else if (bNalateral) // o robô está na lateral
    {
        if (qAbs(iY_atual - vt2dDestino.y()) > globalConfig.robotDiameter)
            vMovimentoYConstante(
                vt2dCaminhoDelta, vt2dPosicaoAtual,
                QVector2D(i_xlimite, iY_atual)); // vai até a ponta da área
        else
            vMovimentoYConstante(vt2dCaminhoDelta, vt2dPosicaoAtual,
                                 vt2dDestino);
    }
}

void Auxiliar::vMovimentoXConstante(QVector<QVector2D>& vt2dCaminho,
                                    const QVector2D& vt2dPosicaoAtual,
                                    const QVector2D& vt2dDestino,
                                    const quint16 passo)
{

    //    qDebug()<<"chamou vt2dMovimento_X_constante\n";

    int iYatual = vt2dPosicaoAtual.y();
    const int iYdestino = vt2dDestino.y();

    while (qAbs(iYatual - iYdestino) > passo)
    {
        if (iYatual < iYdestino)
        {
            iYatual += passo;
        }
        else
        {
            iYatual -= passo;
        }

        vt2dCaminho.append(QVector2D(vt2dPosicaoAtual.x(), iYatual));
    }

    vt2dCaminho.append(QVector2D(vt2dPosicaoAtual.x(), iYdestino));
}

void Auxiliar::vMovimentoYConstante(QVector<QVector2D>& vt2dCaminho,
                                    const QVector2D& vt2dPosicaoAtual,
                                    const QVector2D& vt2dDestino,
                                    const quint16 passo)
{
    //    qDebug()<<"chamou vt2dMovimento_Y_constante\n";

    int iXatual = vt2dPosicaoAtual.x();
    const int iXdestino = vt2dDestino.x();

    while (abs(iXatual - iXdestino) > passo)
    {
        if (iXatual < iXdestino)
            iXatual += passo;
        else
            iXatual -= passo;

        vt2dCaminho.append(QVector2D(iXatual, vt2dPosicaoAtual.y()));
    }

    vt2dCaminho.append(QVector2D(iXdestino, vt2dPosicaoAtual.y()));
}

QVector<QVector2D> Auxiliar::vt_vt2dMovimentacaoDelta(
    const QVector2D vt2dPosicaoInicial, const QVector2D vt2dDestino,
    const QSize szCampo, const int iLarguraDefesa,
    const int iProfundidadeDefesa, const int iLadoCampo)
{
    //    qDebug()<<"chamou vt_vt2dMovimentacaoDelta\n";

    QVector<QVector2D> vt2dCaminhoDelta = {};

    vt2dCaminhoDelta.append(vt2dPosicaoInicial);

    for (quint8 safetyCounter = 0;
         ((vt2dCaminhoDelta.last().distanceToPoint(vt2dDestino) >=
           globalConfig.robotDiameter) &&
          (safetyCounter < 5));
         safetyCounter++)
    {

        vPosicaoNoDelta(vt2dCaminhoDelta.last(), vt2dDestino, szCampo,
                        iLarguraDefesa, iProfundidadeDefesa, iLadoCampo,
                        vt2dCaminhoDelta);
    }

    if (vt2dCaminhoDelta.last() != vt2dDestino)
        vt2dCaminhoDelta.append(vt2dDestino);

    //    for (int i=0; i<vt2dCaminhoDelta.size();i++)
    //        qDebug()<<"vt2dCaminhoDelta ["<<i<<"]:  (
    //        "<<vt2dCaminhoDelta.at(i).x()<<" ;
    //        "<<vt2dCaminhoDelta.at(i).y()<<" )\n";

    return vt2dCaminhoDelta;
}

int Auxiliar::iSinal(float val)
{
    return val < 0 ? -1 : 1;
}

bool Auxiliar::bPontoTesteMuitoProximo(QVector2D pontoTeste,
                                       QVector<QVector2D> pontos,
                                       int distanciaMinima)
{
    foreach (QVector2D ponto, pontos)
    {
        if (ponto.distanceToPoint(pontoTeste) < distanciaMinima)
            return true;
    }
    return false;
}

QVector2D Auxiliar::vt2dCorrigePontoDentroCampo(QVector2D tamanhoCampo,
                                                QVector2D vt2dPosicaoAjuste_1,
                                                QVector2D posInicial,
                                                int iDistanciaInicial,
                                                int iReducao)
{
    QRect campo(-tamanhoCampo.x() / 2, tamanhoCampo.y() / 2, tamanhoCampo.x(),
                -tamanhoCampo.y());

    int iDistancia = iDistanciaInicial;

    // Enquanto o ponto estiver fora do campo, diminui a distancia
    while (!campo.contains(posInicial.toPoint()))
    {
        posInicial = Auxiliar::vt2dReajustaPonto(vt2dPosicaoAjuste_1,
                                                 posInicial, iDistancia);
        iDistancia -= iReducao;

        if (iDistancia <= 0)
            break;
    }

    return posInicial;
}

bool Auxiliar::bPontoForaCampo(QSize tamanhoCampo, QVector2D pontoTeste,
                               int offSet)
{
    QRect campo(
        -tamanhoCampo.width() / 2 - offSet, tamanhoCampo.height() / 2 + offSet,
        tamanhoCampo.width() + 2 * offSet, -tamanhoCampo.height() - 2 * offSet);
    return !campo.contains(pontoTeste.toPoint());
}

void Auxiliar::vConverteAnguloRobo(float& angulo)
{
    // Converte o angulo atual do robo para ser positivo e entre 0 e 2*pi graus
    if (qAbs(angulo) > 2 * M_PI)
    {
        float fatorConversao = angulo / (2 * M_PI) - trunc(angulo / (2 * M_PI));
        angulo = fatorConversao * 2 * M_PI;
    }
    if (angulo < 0)
        angulo += 2 * M_PI;
    if (angulo > 2 * M_PI)
        angulo = angulo - 2 * M_PI;
    //
}

QVector2D Auxiliar::vt2dPontoMiraReceptorRedirect(
    const QVector2D& posBola, const QVector2D& posicaoReceptor,
    const QVector2D& pontoBolaDestino, const double& dVelocidadeChegada,
    const double& dVelocidadeSaida)
{
    return pontoBolaDestino;
}

QVector2D Auxiliar::vt2dPontoMiraPassadorRedirect(
    const QVector2D& posicaoReceptor, const QVector2D& pontoMiraReceptor)
{
    return posicaoReceptor +
           (pontoMiraReceptor - posicaoReceptor).normalized() *
               globalConfig.robotDistanceToBall;
}

QVector2D Auxiliar::vt2dPosicaoReceptorRedirect(
    const QVector2D& posicaoReceptor, const QVector2D& pontoMiraReceptor,
    const QVector2D& posBola, const QVector2D& vt2dVelocidadeBola)
{
    const QVector2D posicaoRoller =
        posBola + Auxiliar::vt2dProjecao_de_A_em_B((posicaoReceptor - posBola),
                                                   vt2dVelocidadeBola);

    return posicaoRoller + (posicaoRoller - pontoMiraReceptor).normalized() *
                               globalConfig.robotDistanceToBall;
}

QVector<QVector2D> Auxiliar::vt_vt2dRetornaVetorEmOrdemCrescente(
    QVector<QVector2D> vt_vt2dXIdYParametro)
{
    // X = ID do robô
    // Y = parâmetro
    //
    // a função retorna o vetor de entrada colocando os parâmetros em ordem
    // crescente

    if (vt_vt2dXIdYParametro.size() <= 1)
        return vt_vt2dXIdYParametro;

    QVector2D auxiliar_0;
    QVector2D auxiliar_1;
    int n = vt_vt2dXIdYParametro.size();

    for (int i = 0; i < n; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (vt_vt2dXIdYParametro.at(j).y() < vt_vt2dXIdYParametro.at(i).y())
            {
                auxiliar_0 = vt_vt2dXIdYParametro.at(i);
                auxiliar_1 = vt_vt2dXIdYParametro.at(j);

                vt_vt2dXIdYParametro.replace(i, auxiliar_1);
                vt_vt2dXIdYParametro.replace(j, auxiliar_0);
            }
        }
    }

    return vt_vt2dXIdYParametro;
}

bool Auxiliar::bInterseccaoObjetosTrianguloAngulo(
    const QVector2D A, const QVector2D B, const bool _bIgnorarBola,
    const QVector<QVector3D>& _vt3dObjetosEmCampo, const float graus_2_alfa)
{
    //
    //             * C
    //          *--*
    //       *-----*
    //  A *--------* B
    //       *-----*
    //          *--*
    //             * D
    //
    //
    // a função verifica a intersecção de objetos dentro do triângulo ACBD
    // graus_2_alfa é o ângulo CÂD

    const QVector2D unitario_l = QVector2D(-(B - A).y(), (B - A).x())
                                     .normalized(); /// versor perpendicular
    const float d =
        B.distanceToPoint(A) * tan((.5) * (M_PI / 180) * graus_2_alfa);
    const QVector2D C = B - unitario_l * d;
    const QVector2D D = B + unitario_l * d;

    const float Ax = A.x(), Ay = A.y(), Cx = C.x(), Cy = C.y(), Dx = D.x(),
                Dy = D.y();

    for (const QVector3D n : _vt3dObjetosEmCampo)
    {
        if (static_cast<TipoObjeto>(n.z()) == otBola &&
            _bIgnorarBola == bNaoConsideraBola)
            continue;

        for (qint8 i = 0; i <= 8; i++)
        {
            QVector2D P = n.toVector2D();

            switch (i)
            { // considera 9 pontos do robô
            case 0:
                break;
            case 1:
                P.setX(P.x() - globalConfig.robotDiameter / 2);
                break;
            case 2:
                P.setX(P.x() + globalConfig.robotDiameter / 2);
                break;
            case 3:
                P.setY(P.y() - globalConfig.robotDiameter / 2);
                break;
            case 4:
                P.setY(P.y() + globalConfig.robotDiameter / 2);
                break;
            case 5:
                P.setY(P.y() + globalConfig.robotDiameter / sqrtf(8));
                P.setX(P.x() + globalConfig.robotDiameter / sqrtf(8));
                break;
            case 6:
                P.setY(P.y() + globalConfig.robotDiameter / sqrtf(8));
                P.setX(P.x() - globalConfig.robotDiameter / sqrtf(8));
                break;
            case 7:
                P.setY(P.y() - globalConfig.robotDiameter / sqrtf(8));
                P.setX(P.x() - globalConfig.robotDiameter / sqrtf(8));
                break;
            case 8:
                P.setY(P.y() - globalConfig.robotDiameter / sqrtf(8));
                P.setX(P.x() + globalConfig.robotDiameter / sqrtf(8));
                break;
            }
            const float Px = P.x();
            const float Py = P.y();

            const float w_2 = ((Cx - Ax) * (Py - Ay) - (Cy - Ay) * (Px - Ax)) /
                              ((Dy - Ay) * (Cx - Ax) - (Dx - Ax) * (Cy - Ay));

            const float w_1 = ((Px - Ax) - w_2 * (Dx - Ax)) / ((Cx - Ax));

            // verifica se o ponto P está dentro do triângulo ACD
            if (w_1 >= 0 && w_2 >= 0 && (w_1 + w_2) <= 1)
                return true;
        }
    }
    return false;
}

bool Auxiliar::bInterseccaoObjetosTrianguloDistancia(
    const QVector2D A, const QVector2D B, const bool _bIgnorarBola,
    const QVector<QVector3D>& _vt3dObjetosEmCampo, const float distancia_CD,
    const bool bLinhaY)
{

    //
    //             * C
    //          *--*
    //       *-----*
    //  A *--------* B
    //       *-----*
    //          *--*
    //             * D
    //
    //
    // a função verifica a intersecção de objetos dentro do triângulo ACBD
    //
    // se bLinhaY for declarada true a reta CBD será paralela ao eixo Y
    // caso contrário a reta será perpendicular à reta AB

    const QVector2D unitario_l =
        (bLinhaY) ? QVector2D(0, 1)
                  : QVector2D((A - B).y(), (B - A).x())
                        .normalized(); // versor perpendicular

    const QVector2D C = B - unitario_l * distancia_CD / 2;
    const QVector2D D = B + unitario_l * distancia_CD / 2;

    const float Ax = A.x(), Ay = A.y(), Cx = C.x(), Cy = C.y(), Dx = D.x(),
                Dy = D.y();

    for (const QVector3D& n : _vt3dObjetosEmCampo)
    {
        if (static_cast<TipoObjeto>(n.z()) == otBola &&
            _bIgnorarBola == bNaoConsideraBola)
            ;
        else
        {
            for (int i = 0; i <= 9; i++)
            {
                QVector2D P = n.toVector2D();

                switch (i)
                { // considera 9 pontos do robô
                case 0:
                    break;
                case 1:
                    P.setX(P.x() - globalConfig.robotDiameter / 2);
                    break;
                case 2:
                    P.setX(P.x() + globalConfig.robotDiameter / 2);
                    break;
                case 3:
                    P.setY(P.y() - globalConfig.robotDiameter / 2);
                    break;
                case 4:
                    P.setY(P.y() + globalConfig.robotDiameter / 2);
                    break;
                case 5:
                    P.setY(P.y() + globalConfig.robotDiameter / sqrtf(8));
                    P.setX(P.x() + globalConfig.robotDiameter / sqrtf(8));
                    break;
                case 6:
                    P.setY(P.y() + globalConfig.robotDiameter / sqrtf(8));
                    P.setX(P.x() - globalConfig.robotDiameter / sqrtf(8));
                    break;
                case 7:
                    P.setY(P.y() - globalConfig.robotDiameter / sqrtf(8));
                    P.setX(P.x() - globalConfig.robotDiameter / sqrtf(8));
                    break;
                case 8:
                    P.setY(P.y() - globalConfig.robotDiameter / sqrtf(8));
                    P.setX(P.x() + globalConfig.robotDiameter / sqrtf(8));
                    break;
                }
                const float Px = P.x();
                const float Py = P.y();

                const float w_2 =
                    ((Cx - Ax) * (Py - Ay) - (Cy - Ay) * (Px - Ax)) /
                    ((Dy - Ay) * (Cx - Ax) - (Dx - Ax) * (Cy - Ay));

                const float w_1 = ((Px - Ax) - w_2 * (Dx - Ax)) / ((Cx - Ax));

                // verifica se o ponto P está dentro do triângulo ACD
                if (w_1 >= 0 && w_2 >= 0 && (w_1 + w_2) <= 1)
                    return true;
            }
        }
    }
    return false;
}

float Auxiliar::fForcaPasse(const QVector2D& posBola,
                            const QVector2D& posReceptor, const QSize& szCampo)
{
    // Força
    //  ^     distância máxima
    //  |           |
    //  |           ********** - Força máxima
    //  |          *
    //  |         *
    //  |        *
    //  |******** - Força mínima
    //  |       |
    //  |   distância mínima
    //  |---------------------> Distância
    //
    //
    //  calcula a força do passe em função da distância

    const float minForce = 2, maxForce = globalConfig.calibratedKickForce,
                dist = posBola.distanceToPoint(posReceptor),
                maxDist = szCampo.width() / 2.0;

    float force = (dist / maxDist) * maxForce;

    if (force < minForce)
        force = minForce;

    if (force > maxForce)
        force = maxForce;

    return force;
}

bool Auxiliar::bRoboNaRetaDaBola(const QVector2D posRobo,
                                 const QVector2D posBola,
                                 const QVector2D vt2dVelocidadeBola,
                                 const float erroAdmissivel)
{
    // verifica se o robô está na reta da bola com erro de `erroAdmissivel`

    if (abs(Auxiliar::dAnguloVetor1Vetor2((posRobo - posBola),
                                          vt2dVelocidadeBola)) < erroAdmissivel)
        return true;

    else
        return false;
}

/// \todo usar camel case
QVector2D Auxiliar::vt2dProjecao_de_A_em_B(const QVector2D& A,
                                           const QVector2D& B)
{

    return (QVector2D::dotProduct(A, B) / (B.lengthSquared())) * B;
}

bool Auxiliar::bPontoDentroDaRegiao(const QVector2D& A, const QVector2D& B,
                                    const QVector2D& X, const float& raio)
{

    const QVector2D aux = (B - A).normalized();

    const QVector2D aux_perpendicular =
        QVector2D(-aux.y(), aux.x()).normalized();

    const QVector2D P0 = A - (aux + aux_perpendicular) * raio * sqrtf(2),
                    P1 = A - (aux - aux_perpendicular) * raio * sqrtf(2),
                    P2 = B + (aux + aux_perpendicular) * raio * sqrtf(2),
                    P3 = B + (aux - aux_perpendicular) * raio * sqrtf(2);

    const float a = P0.distanceToPoint(P1);
    const float b = P0.distanceToPoint(P2);

    return (X.distanceToLine(P0, aux) < a &&
            X.distanceToLine(P0, aux_perpendicular) < b &&
            X.distanceToLine(P3, aux) < a &&
            X.distanceToLine(P3, aux_perpendicular) < b);
}

QVector4D Auxiliar::vt4dPosicaoLadraoDeBola(const QVector2D& posAliado,
                                            const QVector2D& posAdversario,
                                            const QVector2D& posBola,
                                            const QVector2D& posGolAliado)
{
    const QVector2D retaBolaGol = (posGolAliado - posBola).normalized();

    QVector2D posDestino = posBola + globalConfig.robotDistanceToBall *
                                         (posAliado - posBola).normalized(),
              pontoAnguloDestino = posBola;

    // Se o adversário estiver mais próximo da bola
    if (!(posAdversario.distanceToPoint(posBola) > 300 &&
          posAdversario.distanceToPoint(posBola) >
              posAliado.distanceToPoint(posBola)))
    {
        // o robô está distante da reta posGolAliado<->posBola
        if (fabs(Auxiliar::dAnguloVetor1Vetor2((posAliado - posBola),
                                               retaBolaGol)) > 7 &&
            fabs(posGolAliado.x() - posBola.x()) >
                fabs(posGolAliado.x() - posAliado.x()))
        {
            // Posiciona o robô nessa reta
            posDestino = posBola + Auxiliar::vt2dProjecao_de_A_em_B(
                                       posAliado - posBola, retaBolaGol);
        }
        else if (posBola.distanceToPoint(posAliado) > 350)
        {
            posDestino = posBola + retaBolaGol * 250;
        }
        else
        {
            posDestino = posBola + globalConfig.robotDistanceToFront *
                                       (posAliado - posBola).normalized();
        }
    }
    // Se o aliado estiver muito próximo da bola
    else if (posAliado.distanceToPoint(posBola) < 200)
    {
        posDestino = posBola + globalConfig.robotDistanceToFront *
                                   (posAliado - posBola).normalized();
    }

    return QVector4D(posDestino.x(), posDestino.y(), pontoAnguloDestino.x(),
                     pontoAnguloDestino.y());
}

double Auxiliar::dAnguloEmGrausLivreParaChute(
    const QVector2D posBola, const int iLadoCampo, const int iLarguraGol,
    const QSize szCampo, const QVector<QVector3D>& _vt3dPosicoesOponentes)
{
    const QVector<QVector2D> vt_vt2PontosLivresGolAdversario =
        Auxiliar::vt2dIntervaloLivreGol(posBola, iLadoCampo, iLarguraGol, 0,
                                        szCampo, _vt3dPosicoesOponentes,
                                        globalConfig.robotDiameter / 2);
    if (vt_vt2PontosLivresGolAdversario.isEmpty())
        return 0;
    else
        return (dAnguloVetor1Vetor2(
            vt_vt2PontosLivresGolAdversario.first() - posBola,
            vt_vt2PontosLivresGolAdversario.last() - posBola));
}

QVector2D Auxiliar::vt2dCentroLivreGolAdversario(
    const QVector2D posBola, const int iLadoCampo, const int iLarguraGol,
    const QSize szCampo, const QVector<QVector3D>& _vt3dPosicoesOponentes)
{

    QVector<QVector2D> vt_vt2dAux = Auxiliar::vt2dIntervaloLivreGol(
        posBola, iLadoCampo, iLarguraGol, 0, szCampo, _vt3dPosicoesOponentes,
        globalConfig.robotDiameter / 2);

    if (!vt_vt2dAux.isEmpty())
        return vt_vt2dAux.at(vt_vt2dAux.size() / 2);

    return QVector2D(-iLadoCampo * szCampo.width() / 2.0, 0);
}

QVector<QVector2D> Auxiliar::vt2dCalculaCurvaBezier(
    const QVector<QVector2D>& _pontos)
{
    QVector<QVector2D> curva;
    curva.clear();
    int n = _pontos.size() - 1;

    for (int t = 0; t <= 10; t += 1)
    {
        QVector2D pontoCurva(0, 0);
        for (int i = 0; i <= n; ++i)
        {
            unsigned long int binCoef = iCalculaBinomioNewton(n, i);
            pontoCurva += binCoef * pow(1.0 - t / 10.0, n - i) *
                          pow(t / 10.0, i) * _pontos.at(i);
        }
        curva.append(pontoCurva);
    }

    return curva;
}

int Auxiliar::iCalculaBinomioNewton(int n, int k)
{
    double res = 1;
    for (int i = 1; i <= k; ++i)
        res = res * (n - k + i) / i;
    return qRound(res + 0.01);
}

double Auxiliar::dLiberdadeDeMarcacao(
    const QVector2D& posicaoReceptor,
    const QVector<QVector3D>& vt3dPosicoesOponentes)
{

    float menorDistancia = 1e10;

    for (const QVector3D& n : vt3dPosicoesOponentes)
    {
        const float distancia =
            (n.toVector2D()).distanceToPoint(posicaoReceptor);

        if (distancia < menorDistancia)
            menorDistancia = distancia;
    }

    return menorDistancia;
}

double Auxiliar::dAnguloDeRotacao(const QVector2D posicaoDoRobo,
                                  const QVector2D pontoMiraAtual,
                                  const QVector2D pontoMiraDestino)
{

    return Auxiliar::dAnguloVetor1Vetor2(pontoMiraAtual - posicaoDoRobo,
                                         pontoMiraDestino - posicaoDoRobo);
}

double Auxiliar::dAnguloEmGrausLivreParaPasse(
    const QVector2D posBola, const QVector2D posicaoReceptor,
    const QVector<QVector3D>& _vt3dPosicoesOponentes, const qint8 passo)
{

    for (quint8 i = 1; i <= 45; i += passo)
    {

        if (bInterseccaoObjetosTrianguloAngulo(posBola, posicaoReceptor, true,
                                               _vt3dPosicoesOponentes, i))
            return (i - 1);
    }

    return 45;
}

QVector<qint16> Auxiliar::vtiMaiorValorESeuIndice(
    const QVector<qint16>& vtiEntrada)
{
    if (vtiEntrada.isEmpty())
        return QVector<qint16>();

    QVector<qint16> vti = QVector<qint16>(2);

    vti.replace(0, vtiEntrada.first());
    vti.replace(1, 0);

    for (quint16 i = 0; i < vtiEntrada.size(); i++)
    {

        if (vtiEntrada.at(i) > vti.first())
        {
            vti.replace(0, vtiEntrada.at(i));
            vti.replace(1, i);
        }
    }
    return vti;
}

double Auxiliar::dDeltaXis(const QVector2D posicaoReceptor,
                           const QVector2D posicaoPassador,
                           const qint8 iLadoCampoDefesa)
{
    const double _dDeltaXis = posicaoReceptor.x() - posicaoPassador.x();

    if (iLadoCampoDefesa == XNegativo)
        return _dDeltaXis;

    return -_dDeltaXis;
}

int Auxiliar::iCalculaFPS(QVector<int>& fpsCounter)
{
    int FPS = 0;
    if (fpsCounter.size() > 50)
        fpsCounter.pop_back();
    for (auto n : fpsCounter)
        FPS += n;
    FPS = FPS / fpsCounter.size();
    return FPS;
}

void Auxiliar::vConfiguraDarkModePlot(QCustomPlot* qcp)
{
    qcp->setBackground(Qt::black);

    qcp->xAxis->setTickPen(QPen(Qt::white));
    qcp->xAxis->setBasePen(QPen(Qt::white));
    qcp->xAxis->setSubTickPen(QPen(Qt::white));
    qcp->xAxis->setTickLabelColor(Qt::white);

    qcp->yAxis->setTickPen(QPen(Qt::white));
    qcp->yAxis->setBasePen(QPen(Qt::white));
    qcp->yAxis->setSubTickPen(QPen(Qt::white));
    qcp->yAxis->setTickLabelColor(Qt::white);

    qcp->xAxis->grid()->setVisible(false);

    qcp->yAxis->grid()->setVisible(true);
    qcp->yAxis->grid()->setPen(QPen(QColor(0x44, 0x44, 0x44)));
    qcp->yAxis->grid()->setSubGridVisible(true);
    qcp->yAxis->grid()->setSubGridPen(QPen(QColor(0x11, 0x11, 0x11)));

    for (quint8 i = 0; i < qcp->graphCount(); i++)
    {
        qcp->graph(i)->setPen(QPen(Qt::yellow));

        qcp->graph(i)->setBrush(Qt::blue);
    }
}

// QVector2D Auxiliar::vt2dposicaoReceptorIndirect2R(const QVector2D posBola,
//                                                   const qint8 iLadoCampo,
//                                                   const float xDestino,
//                                                   const float yDestino)
//{
//     if(posBola.y() > 0 && iLadoCampo == XNegativo)
//     {

//    }
//    else if()
//    {

//    }
//}

QString Auxiliar::strFromID_Jogadas(const ID_Jogadas idJogada)
{
    switch (idJogada)
    {
    case Jogada_Invalida:
        return "Jogada_Invalida";
    case Nenhuma:
        return "Nenhuma";
    case Robo_Goleiro:
        return "Robo_Goleiro";
    case TimeOut:
        return "TimeOut";

    case KickOff_Cobrador:
        return "KickOff_Cobrador";
    case KickOff_Receptor:
        return "KickOff_Receptor";
    case KickOff_Defesa:
        return "KickOff_Defesa";

    case Penalty_Defesa:
        return "Penalty_Defesa";
    case Penalty_Cobrador:
        return "Penalty_Cobrador";

    case FreeKick_Cobrador:
        return "FreeKick_Cobrador";
    case FreeKick_Receptor:
        return "FreeKick_Receptor";
    case FreeKick_Defesa_Marcador:
        return "FreeKick_Defesa_Marcador";
    case FreeKick_Defesa_Barreira:
        return "FreeKick_Defesa_Barreia";
    case FreeKick_Defesa_Delta:
        return "FreeKick_Defesa_Delta";
    case FreeKick_Meia_Mines:
        return "FreeKick_Meia_Mines";
    case FreeKick_Meia_Marcacao:
        return "FreeKick_Meia_Marcacao";
    case FreeKick_Atacante_Mines:
        return "FreeKick_Atacante_Mines";

    case Normal_Ladrao_Bola:
        return "Normal_Ladrao_Bola";
    case Normal_Chutador:
        return "Normal_Chutador";
    case Normal_Passador:
        return "Normal_Passador";
    case Normal_Receptor:
        return "Normal_Receptor";

    case Normal_Defensor_Delta:
        return "Normal_Defensor_Delta";
    case Normal_Defensor_Marcacao:
        return "Normal_Defensor_Marcacao";

    case Normal_Meia_Marcacao:
        return "Normal_Meia_Marcacao";
    case Normal_Meia_Mines:
        return "Normal_Meia_Mines";

    case Normal_Atacante_Mines:
        return "Normal_Atacante_Mines";

    case BallPlacement_Principal:
        return "BallPlacement_Principal";
    case BallPlacement_Auxiliar:
        return "BallPlacement_Auxiliar";
    case BallPlacement_Cone:
        return "BallPlacement_Cone";

    case Jogada_Ensaiada_Cobrador:
        return "Escanteio_Cobrador";
    case Jogada_Ensaiada_Receptor_Real:
        return "Escanteio_Receptor_Real";
    case Jogada_Ensaiada_Receptor_Aparente_0:
        return "Escanteio_Receptor_Aparente_0";
    case Jogada_Ensaiada_Receptor_Aparente_1:
        return "Escanteio_Receptor_Aparente_1";

    case Stop_Perseguidor_Bola:
        return "Stop_Perseguidor_Bola";
    case Stop_Atacante_Mines:
        return "Stop_Atacante_Mines";
    case Stop_Meia_Mines:
        return "Stop_Meia_Mines";
    case Stop_Defensor_Delta:
        return "Stop_Defensor_Delta";

    case Subs_Entrando:
        return "Subs_Entrando";
    case Subs_Saindo:
        return "Subs_Saindo";
    }

    qCritical() << Q_FUNC_INFO << ": ID da jogada não identificado";

    return "ID_Impossivel";
}

QVector2D Auxiliar::vt2dCoordenadasGenericasParaCoordenadasCampo(
    const QVector2D coordenadasGenericas, const int iLadoCampo,
    const QSize szCampo, const QSize szGol)
{
    float x{0};
    float y{0};

    if (coordenadasGenericas.x() < -1)
    {
        x = iLadoCampo * (szCampo.width() / 2 -
                          (coordenadasGenericas.x() + 2) * szGol.width());
    }
    else if (coordenadasGenericas.x() < 1)
    {
        x = -iLadoCampo * (szCampo.width() / 2 - szGol.width()) *
            coordenadasGenericas.x();
    }
    else /*if(coordenadasGenericas.x() < 2)*/
    {
        x = -iLadoCampo * (szCampo.width() / 2 +
                           (coordenadasGenericas.x() - 2) * szGol.width());
    }

    if (coordenadasGenericas.y() < -1)
    {
        y = -szGol.height() / 2 + (szCampo.height() / 2 - szGol.height() / 2) *
                                      (coordenadasGenericas.y() + 1);
    }
    else if (coordenadasGenericas.y() < 1)
    {
        y = coordenadasGenericas.y() * szGol.height() / 2;
    }
    else /*if(coordenadasGenericas.y() < 2)*/
    {
        y = szGol.height() / 2 + (szCampo.height() / 2 - szGol.height() / 2) *
                                     (coordenadasGenericas.y() - 1);
    }
    return QVector2D(x, y);
}

QVector2D Auxiliar::vt2dCoordenadasCampoParaCoordenadasGenericas(
    const QVector2D coordenadasCampo, const int iLadoCampo, const QSize szCampo,
    const QSize szGol)
{
    float x{0};
    float y{0};

    if (coordenadasCampo.x() * iLadoCampo < 0)
    {
        // ataque
        if (fabs(coordenadasCampo.x()) > (szCampo.width() / 2 - szGol.width()))
        {
            x = 1 + (fabs(coordenadasCampo.x()) -
                     (szCampo.width() / 2 - szGol.width())) /
                        szGol.width();
        }
        else
        {
            x = fabs(coordenadasCampo.x()) /
                (szCampo.width() / 2 - szGol.width());
        }
    }
    else /*if(coordenadasCampo.x()*iLadoCampo <= 0)*/
    {
        // defesa
        if (fabs(coordenadasCampo.x()) > (szCampo.width() / 2 - szGol.width()))
        {
            x = -1 - (fabs(coordenadasCampo.x()) -
                      (szCampo.width() / 2 - szGol.width())) /
                         szGol.width();
        }
        else
        {
            x = -fabs(coordenadasCampo.x()) /
                (szCampo.width() / 2 - szGol.width());
        }
    }

    if (coordenadasCampo.y() > 0)
    {
        if (coordenadasCampo.y() > szGol.height() / 2)
        {
            y = 1 + (coordenadasCampo.y() - szGol.height() / 2) /
                        (szCampo.height() / 2 - szGol.height() / 2);
        }
        else
        {
            y = coordenadasCampo.y() / (szGol.height() / 2);
        }
    }
    else /*if(coordenadasCampo.y() <= 0)*/
    {
        if (fabs(coordenadasCampo.y()) > szGol.height() / 2)
        {
            y = -1 - (fabs(coordenadasCampo.y()) - szGol.height() / 2) /
                         (szCampo.height() / 2 - szGol.height() / 2);
        }
        else
        {
            y = coordenadasCampo.y() / (szGol.height() / 2);
        }
    }

    return QVector2D(x, y);
}

QMap<qint8, qint8> Auxiliar::mapIntIntMarcaOponentesMaisProximosAoGol(
    const QMap<qint8, QVector2D> marcadores,
    const QMap<qint8, QVector2D> oponentes, const QVector2D centroGolAliado)
{
    QMap<qint8, qint8> mapaMarcacao;

    while (mapaMarcacao.size() != marcadores.size())
    {
        qint8 oponenteAMarcar = -1;
        float menorDistancia = 999999999999999.f;

        for (auto it = oponentes.constBegin(); it != oponentes.constEnd(); ++it)
        {
            const float distancia = it.value().distanceToPoint(centroGolAliado);
            // Se o robô estiver mais próximo da bola e ainda não está sendo
            // marcado
            if (distancia < menorDistancia &&
                mapaMarcacao.key(it.key(), -2) == -2)
            {
                oponenteAMarcar = it.key();
                menorDistancia = distancia;
            }
        };

        if (oponenteAMarcar == -1)
        {
            for (const qint8 idAliado : marcadores.keys())
            {
                // Indica que o aliado está sobrando e não marcará ninguém
                if (!mapaMarcacao.contains(idAliado))
                {
                    mapaMarcacao[idAliado] = -1;
                }
            }
            break;
        }

        const QVector2D posicaoOponente = oponentes.value(oponenteAMarcar);
        // Reutiliza essa variável
        menorDistancia = 999999999999999.f;

        qint8 aliadoMarcador = -1;

        for (auto it = marcadores.constBegin(); it != marcadores.constEnd();
             ++it)
        {
            const float distancia = it.value().distanceToPoint(posicaoOponente);
            // Se o robô estiver mais próximo do aliado e ainda não está
            // marcando ninguém
            if (distancia < menorDistancia && !mapaMarcacao.contains(it.key()))
            {
                aliadoMarcador = it.key();
                menorDistancia = distancia;
            }
        };

        if (aliadoMarcador == -1)
        {
            qFatal(Q_FUNC_INFO);
        }

        mapaMarcacao[aliadoMarcador] = oponenteAMarcar;
    }

    if (mapaMarcacao.size() != marcadores.size())
    {
        qFatal(Q_FUNC_INFO);
    }

    return mapaMarcacao;
}

float Auxiliar::fProjecaoBolaNoGolAliado(const QVector2D vt2dVelocidadeBola,
                                         const QVector2D posBola,
                                         const float xDestino)
{
    const QVector2D centroGolAliado = QVector2D(xDestino, 0);

    if (vt2dVelocidadeBola.x() * xDestino <= 0)
        return 0;

    float xFaltante = fabs(centroGolAliado.x() - posBola.x());

    QVector2D vetorDeslocamentoBolaAteGolAdversario{
        fabs(xFaltante / vt2dVelocidadeBola.normalized().x()) *
        vt2dVelocidadeBola.normalized()};

    QVector2D pontoProjecao{posBola + vetorDeslocamentoBolaAteGolAdversario};

    return pontoProjecao.y();
}

QVector2D Auxiliar::vt2dDestinoGoleiroBolaEmMovimento(
    const QVector2D vt2dVelocidadeBola, const QVector2D posBola,
    const QVector2D posicaoGoleiro, const float xGoleiroMaximo)
{
    if (vt2dVelocidadeBola.x() * xGoleiroMaximo <= 0)
        return {xGoleiroMaximo, 0};

    QVector2D destino =
        posBola + Auxiliar::vt2dProjecao_de_A_em_B((posicaoGoleiro - posBola),
                                                   vt2dVelocidadeBola);

    if (fabs(destino.x()) > fabs(xGoleiroMaximo))
    {
        destino.setX(xGoleiroMaximo);
        destino.setY(Auxiliar::fProjecaoBolaNoGolAliado(
            vt2dVelocidadeBola, posBola, xGoleiroMaximo));
    }

    return destino;
}

float Auxiliar::fMelhorPosicaoGoleiroParaFecharGolAliado(
    const QVector2D posBola, const QVector2D vt2dCentroGoalAliado,
    const float larguraGol, const float xGoleiro)
{
    ///
    /// \todo Atualmente o melhor posicionamento é calculado considerando o robô
    ///  como sendo um simples ponto. Considerar o diâmetro robô para no
    ///  cálculo.
    ///

    if (abs(posBola.x()) > abs(xGoleiro) && posBola.x() * xGoleiro > 0)
    {
        if (posBola.y() == 0)
        {
            return 0;
        }
        else
        {
            return posBola.y() / abs(posBola.y()) * larguraGol / 2;
        }
    }

    const QVector2D travePositivo =
        vt2dCentroGoalAliado + QVector2D(0, larguraGol / 2);
    const QVector2D traveNegativo =
        vt2dCentroGoalAliado - QVector2D(0, larguraGol / 2);

    const float anguloTriangulo = Auxiliar::dAnguloVetor1Vetor2(
        travePositivo - posBola, traveNegativo - posBola);

    float anguloCampo;

    if (xGoleiro > 0)
    {
        anguloCampo = qAtan2((traveNegativo - posBola).y(),
                             (traveNegativo - posBola).x()) *
                          180 / M_PI +
                      anguloTriangulo / 2;
    }
    else
    {
        anguloCampo = qAtan2((travePositivo - posBola).y(),
                             (travePositivo - posBola).x()) *
                          180 / M_PI +
                      anguloTriangulo / 2;
    }

    QVector2D vetorLihaDestino = QVector2D(qCos(M_PI / 180 * anguloCampo),
                                           qSin(M_PI / 180 * anguloCampo));

    const float xFaltante = fabs(xGoleiro - posBola.x());

    vetorLihaDestino *= xFaltante / fabs(vetorLihaDestino.x());

    const QVector2D pontoDestino = posBola + vetorLihaDestino;

    return pontoDestino.y();
}

QColor Auxiliar::corEscalaLinear(const QColor& cor0, const QColor& cor1,
                                 const double valor)
{
    if (valor < 0)
        return cor0;
    if (valor > 1)
        return cor1;

    return QColor::fromRgbF((1 - valor) * cor0.redF() + valor * cor1.redF(),
                            (1 - valor) * cor0.greenF() + valor * cor1.greenF(),
                            (1 - valor) * cor0.blueF() + valor * cor1.blueF(),
                            (1 - valor) * cor0.alphaF() +
                                valor * cor1.alphaF());
}

QColor Auxiliar::corEscalaLinear(const QColor& cor0, const QColor& cor1,
                                 const double valor, const double valorMinimo,
                                 const double valorMaximo)
{
    const double valorAdaptado =
        (valorMaximo == valorMinimo)
            ? valorMaximo
            : (valor - valorMinimo) / (valorMaximo - valorMinimo);

    return Auxiliar::corEscalaLinear(cor0, cor1, valorAdaptado);
}

QVector<QVector2D> Auxiliar::vt_vt2dPosicoesDestinoDeltaInteligente(
    const int nRobosDelta, QVector<QVector2D> posicaoAdversarios,
    const QVector2D posBola, const int larguraArea, const int profundidadeArea,
    QVector2D centroGolAliado, const float distanciaMaximaConsiderada)
{
    // Ordena o vetor posicaoAdversarios, colocando as posições mais próximas ao
    // gol aliado nas
    //  primeiras posições
    for (int i = 0; i < posicaoAdversarios.size(); ++i)
    {
        for (int j = i + 1; j < posicaoAdversarios.size(); ++j)
        {
            if (posicaoAdversarios.at(i).distanceToPoint(centroGolAliado) >
                posicaoAdversarios.at(j).distanceToPoint(centroGolAliado))
            {

#if QT_VERSION > QT_VERSION_CHECK(5, 14, 0)
                posicaoAdversarios.swapItemsAt(i, j);
#else
                std::swap(posicaoAdversarios[i], posicaoAdversarios[j]);
#endif
            }
        }
    }

    // Remove os adersários que estão muito distantes do gol (considerando o
    // parâmetro `distanciaMaximaConsiderada`)
    while (!posicaoAdversarios.isEmpty() &&
           posicaoAdversarios.last().distanceToPoint(centroGolAliado) >
               distanciaMaximaConsiderada)
    {
        posicaoAdversarios.removeLast();
    }

    // A bola é prioridade e ocupa a primeira posição do vetor, sempre
    if (posBola.distanceToPoint(centroGolAliado) < distanciaMaximaConsiderada)
    {
        for (int i = 0; i < posicaoAdversarios.size(); ++i)
        {
            // Remove o ponto muito próximo à bola para não contar duas vezes
            if (posicaoAdversarios[i].distanceToPoint(posBola) < 250)
            {
                posicaoAdversarios.remove(i);
                break;
            }
        }

        posicaoAdversarios.prepend(posBola);
    }

    QVector2D pontoCentro = QVector2D(
        centroGolAliado.x() > 0 ? (centroGolAliado.x() - profundidadeArea)
                                : (centroGolAliado.x() + profundidadeArea),
        0);

    QVector<QVector2D> destinoAliados(nRobosDelta, pontoCentro);

    for (int i = 0; i < destinoAliados.size(); ++i)
    {
        // Atribui destinoAliados[i] para marcar posAdversario na linha da área
        if (!posicaoAdversarios.isEmpty())
        {
            const QVector2D posAdversario = posicaoAdversarios.first();
            posicaoAdversarios.removeFirst();

            destinoAliados[i] = Auxiliar::vt2dPosicaoDeltaInteligente(
                posAdversario, larguraArea, profundidadeArea, centroGolAliado);
        }
        // Atribui destinoAliados[i] na região mais livre do delta
        else
        {
            destinoAliados[i] = pontoCentro;
        }

        // Ordena o vetor `destinoAliados`, colocando os pontos mais negativos
        // nas primeiras posições
        for (int j = 0; j < i + 1; ++j)
        {
            for (int k = j + 1; k < i + 1; ++k)
            {
                //            const int k = j+1;
                // Se as duas posições estiverem na linha do lado da área em y
                // positivo
                if (qAbs(destinoAliados.at(j).y() - destinoAliados.at(k).y()) <
                        .1 &&
                    qAbs(destinoAliados.at(j).y() - larguraArea / 2) < .1)
                {
                    if ((destinoAliados.at(j).x() > destinoAliados.at(k).x() &&
                         centroGolAliado.x() < 0) ||
                        (destinoAliados.at(j).x() < destinoAliados.at(k).x() &&
                         centroGolAliado.x() >= 0))
                    {
                        continue;
                    }
                }
                // Se as duas posições estiverem na linha do lado da área em y
                // positivo
                else if (qAbs(destinoAliados.at(j).y() -
                              destinoAliados.at(k).y()) < .1 &&
                         qAbs(destinoAliados.at(j).y() + larguraArea / 2) < .1)
                {
                    if ((destinoAliados.at(j).x() < destinoAliados.at(k).x() &&
                         centroGolAliado.x() < 0) ||
                        (destinoAliados.at(j).x() > destinoAliados.at(k).x() &&
                         centroGolAliado.x() >= 0))
                    {
                        continue;
                    }
                }
                else if (destinoAliados.at(j).y() < destinoAliados.at(k).y())
                {
                    continue;
                }

                // Se ele chegou até aqui, ele troca os itens de posição
#if QT_VERSION > QT_VERSION_CHECK(5, 14, 0)
                destinoAliados.swapItemsAt(j, k);
#else
                std::swap(destinoAliados[j], destinoAliados[k]);
#endif
            }
        }

        // Atualiza o pontoCentro se necessário
        if (posicaoAdversarios.isEmpty())
        {
            QVector<QVector2D> temp;
            temp.reserve(i + 3);
            temp.append(centroGolAliado - QVector2D(0, larguraArea / 2));
            temp.append(destinoAliados.mid(0, i + 1));
            temp.append(centroGolAliado + QVector2D(0, larguraArea / 2));

            QVector2D p0 = temp.at(0), p1 = temp.at(1);

            for (int j = 2; j < temp.size(); ++j)
            {
                //                if(temp.at(j).distanceToPoint(temp.at(j-1)) >
                //                p0.distanceToPoint(p1))
                if (Auxiliar::dAnguloVetor1Vetor2(temp.at(j) - centroGolAliado,
                                                  temp.at(j - 1) -
                                                      centroGolAliado) >
                    Auxiliar::dAnguloVetor1Vetor2(p0 - centroGolAliado,
                                                  p1 - centroGolAliado))
                {
                    p0 = temp.at(j);
                    p1 = temp.at(j - 1);
                }
            }

            pontoCentro = Auxiliar::vt2dPosicaoDeltaInteligente(
                centroGolAliado +
                    distanciaMaximaConsiderada *
                        (p0 + p1 - 2 * centroGolAliado).normalized(),
                larguraArea, profundidadeArea, centroGolAliado);
        }
    }

    // Espaça os robôs no delta se necessário

    /// \todo Espaçar os robôs no delta caso estejam muito próximos um do outro

    return destinoAliados;
}

QVector2D Auxiliar::vt2dPosicaoDeltaInteligente(const QVector2D posAdversario,
                                                const int larguraArea,
                                                const int profundidadeArea,
                                                QVector2D centroGolAliado)
{
    const float d0 = (posAdversario.y() - centroGolAliado.y()) *
                     profundidadeArea /
                     qAbs(posAdversario.x() - centroGolAliado.x());

    QVector2D dest =
        QVector2D(centroGolAliado.x() -
                      Auxiliar::iSinal(centroGolAliado.x()) * profundidadeArea,
                  d0);

    if (qAbs(dest.y()) > larguraArea / 2)
    {
        // Altera o destino para ficar na linha paralela ao eixo x
        const float d1 = centroGolAliado.y() +
                         Auxiliar::iSinal(posAdversario.y()) * larguraArea / 2;
        const float d2 = qAbs((posAdversario.x() - centroGolAliado.x()) /
                              (posAdversario.y() - centroGolAliado.y()) * d1);

        dest = QVector2D(centroGolAliado.x() -
                             Auxiliar::iSinal(centroGolAliado.x()) * d2,
                         d1);
    }

    return dest;
}

REGIAO_CAMPO Auxiliar::regiaoCampoPonto(const QVector2D posPonto,
                                        const int ladoCampo,
                                        const QSize tamanhoCampo,
                                        const QSize tamanhoArea)
{
    const QVector2D posBolaCoordGen =
        Auxiliar::vt2dCoordenadasCampoParaCoordenadasGenericas(
            posPonto, ladoCampo, tamanhoCampo, tamanhoArea);

    if (posBolaCoordGen.x() < -.5)
        return REGIAO_CAMPO_DEFESA;
    if (posBolaCoordGen.x() < .5)
        return REGIAO_CAMPO_MEIO;
    return REGIAO_CAMPO_ATAQUE;
}

int Auxiliar::pontosRegiaoCampo(const QVector<QVector2D> pontos,
                                const REGIAO_CAMPO regiaoCampo,
                                const int ladoCampo, const QSize tamanhoCampo,
                                const QSize tamanhoArea)
{
    int conta = 0;

    for (const QVector2D p : pontos)
    {
        if (Auxiliar::regiaoCampoPonto(p, ladoCampo, tamanhoCampo,
                                       tamanhoArea) == regiaoCampo)
        {
            ++conta;
        }
    }

    return conta;
}

QString Auxiliar::strColorText(const QString _text, const QString _color)
{
    return QString("<l style=\"color:%1\">%2</l>").arg(_color).arg(_text);
}

QString Auxiliar::strBoldText(const QString _text)
{
    return QString("<b>%1</b>").arg(_text);
}

QVector2D Auxiliar::vt2dBallIntersection(const QVector<QVector2D>& _ballPoints,
                                         const QVector2D _robotPos,
                                         const QVector2D _robotAim,
                                         double _robotLineLength)
{
    QLineF ballLine = Auxiliar::lineCalculaRetaMedia(_ballPoints);
    QVector2D P1 = _robotPos, P2 = _robotPos, intercept;

    P1 = Auxiliar::vt2dRotaciona(
        _robotPos, _robotPos + _robotAim * _robotLineLength / 2.0, 90);
    P2 = Auxiliar::vt2dRotaciona(
        _robotPos, _robotPos + _robotAim * _robotLineLength / 2.0, -90);

    Auxiliar::bChecaInterseccaoLinhaLinha(P1, P2, QVector2D(ballLine.p1()),
                                          QVector2D(ballLine.p2()), intercept);

    // Caso o ponto de interceptação esteja fora do intervalo, fica parado, ou
    // se a bola não se moveu o suficiente
    if (intercept.distanceToPoint(_robotPos) > _robotLineLength / 2.0)
    {
        intercept = _robotPos;
    }
    return intercept;
}
