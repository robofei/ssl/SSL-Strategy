﻿/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef AUXILIAR_H
#define AUXILIAR_H

///
/// \file auxiliar.h
/// \brief Funções auxiliares \a Auxiliar
///

#include <QVector>
#include <QVector2D>
#include <QVector4D>
#include <QDebug>
#include <QRandomGenerator>
#include <QRect>
#include <QScopedPointer>
#include <QCustomPlot/QCustomPlot>

#include <iostream>
#include <cstdlib>
#include <Eigen/LU>
#include <Eigen/Dense>
#include <cmath>

#include "Constantes_e_Funcoes_Auxiliares/hungarianalgorithm.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"


/**
 * @namespace Auxiliar
 * @brief Neste namespace estão agrupadas diversas funções auxiliares utilizados em todo o código.
 * Essas funções incluem: checagem e cálculo intersecções entre robôs e retas, retas e retas, retas e circulo, cálculo de posições de chute, recepção de passes,
 * posição do goleiro ou delta e outros.
 */
namespace Auxiliar
{
/**
     * @brief Retorna true se algum dos objetos fornecidos estiver à uma distância mínima da reta formada pelos pontos P1 e P2.
     *
     * @param _vt2dP1 - P1.
     * @param _vt2dP2 - P2.
     * @param _bIgnorarBola - Indica se a bola deve ser ignorada ou não.
     * @param _fDistanciaMinima - Distância mínima considerada.
     * @param _vt3dObjetosEmCampo - Vetor dos objetos considerados.
     * @return bool - True se algum dos objetos fornecidos estiver à uma distância mínima da reta formada pelos pontos P1 e P2, senão, false.
     */
bool bChecaInterseccaoObjetosLinha(QVector2D _vt2dP1,
                                   QVector2D _vt2dP2,
                                   bool _bIgnorarBola,
                                   float _fDistanciaMinima,
                                   const QVector<QVector3D>& _vt3dObjetosEmCampo);

/**
     * @brief Retorna true se o ponto fornecido estiver à uma distância mínima da reta formada pelos pontos P1 e P2.
     *
     * @param _vt2dP1 - P1.
     * @param _vt2dP2 - P2.
     * @param _fDistanciaMinima - Distância mínima considerada.
     * @param _vt2dObjetoEmCampo - Ponto de teste.
     * @return bool -  True se o ponto fornecido estiver à uma distância mínima da reta formada pelos pontos P1 e P2, senão, false.
     */
bool bChecaInterseccaoObjetosLinha(QVector2D _vt2dP1,
                                   QVector2D _vt2dP2,
                                   float _fDistanciaMinima,
                                   const QVector2D& _vt2dObjetoEmCampo);

/**
     * @brief Retorna true se o objeto fornecido estiver à uma distância mínima da reta formada pelos pontos P1 e P2.
     *
     * @param _vt2dP1 - P1.
     * @param _vt2dP2 - P2.
     * @param _fDistanciaMinima - Distância mínima considerada.
     * @param _vt3dObjetoEmCampo - Objeto de teste.
     * @return bool -  True se o objeto fornecido estiver à uma distância mínima da reta formada pelos pontos P1 e P2, senão, false.
     */
bool bChecaInterseccaoObjetosLinha(QVector2D _vt2dP1,
                                   QVector2D _vt2dP2,
                                   float _fDistanciaMinima,
                                   const QVector<QVector3D> &_vt3dObjetoEmCampo);

/**
     * @brief Retorna true se o objeto fornecido estiver à uma distância mínima da reta formada pelos pontos P1 e P2.
     *
     * @param _vt2dP1 - P1.
     * @param _vt2dP2 - P2.
     * @param _vt4dObjetoEmCampo - Objeto de teste.
     * @return bool -  True se o objeto fornecido estiver à uma distância mínima da reta formada pelos pontos P1 e P2, senão, false.
     */
bool bChecaInterseccaoObjetosLinha(QVector2D _vt2dP1,
                                   QVector2D _vt2dP2,
                                   const QVector<QVector4D> &_vt4dObjetoEmCampo);

/**
     * @brief Retorna true se existe intersecção entre as retas P11-P21 e
     * P12-P22, também retorna o ponto de intersecção, se houver.
     *
     * O valor booleano retornado checa se o ponto de intersecção
     * pertence à *AMBAS* as linhas.
     *
     * @param P11
     * @param P21
     * @param P12
     * @param P22
     * @param Pint - Ponto de intersecção.
     * @return bool - True se houver intersecção, senão, false.
     */
bool bChecaInterseccaoLinhaLinha(QVector2D P11, QVector2D P21, QVector2D P12, QVector2D P22, QVector2D &Pint);

//Calcula um ponto livre do gol oponente
/**
     * @brief Retorna o intervalo de pontos livre do gol oponente.
     * Obs: É possível calcula o intervalo livre do gol aliado com esta função também, basta passar o lado do campo oponente como parâmetro, ao invés do lado aliado.
     * @param _vt2dPosicaoDeChute - Posição em que o chute para o gol será feito.
     * @param _iLadoCampoAliado - Lado do campo aliado.
     * @param _iLarguraGol - Largura do gol.
     * @param _iProfundidadeGol - Profundidade do gol.
     * @param _vt2dTamanhoCampo - Tamanho do campo.
     * @param _vt3dPosicoesOponentes - Vetor de posições dos robôs oponentes.
     * @return QVector<QVector2D> - Vetor de pontos livres do gol.
     */
QVector<QVector2D> vt2dIntervaloLivreGol(QVector2D _vt2dPosicaoDeChute,
                                         int _iLadoCampoAliado,
                                         int _iLarguraGol,
                                         int _iProfundidadeGol,
                                         QSize _szCampo,
                                         const QVector<QVector3D>& _vt3dPosicoesOponentes,
                                         int larguraMinimaDaMira = globalConfig.minAimSpace);

/**
     * @brief Retorna o quadrante do ângulo fornecido.
     *
     * @param _angulo - Ângulo de 0 a 2pi [rad].
     * @return int - Quadrante (1 a 4).
     */
int iCalculaQuadrante(double _angulo);

/**
     * @brief Retorna o ponto na reta Bola-Mira que o robô deve estar para chutar a bola.
     * Este ponto ficará distante `_fDistanciaRoboBola` mm da bola.
     *
     * @param _vt2dPontoMira - Ponto de mira.
     * @param _vt2dPontoBola - Posição da bola.
     * @param _fDistanciaRoboBola - Distância que o robô deve ficar da bola [mm].
     *
     * @return QVector2D - Posição do robô para chutar a bola.
     */
QVector2D vt2dCalculaPontoCobranca(QVector2D _vt2dPontoMira,
                                   QVector2D _vt2dPontoBola,
                                   float _fDistanciaRoboBola);

/**
     * @brief Retorna qual a região que o ponto fornecido pertence. Uma região é delimitada pelas linhas tracejadas no desenho do campo.
     *
     * @param _vt2dPonto - Ponto de teste.
     * @param _iLarguraCampo - Largura do campo (X).
     * @param _iLarguraRegiao - Largura das regiões [mm].
     * @return int - Região que o ponto fornecido pertence.
     */
int iCalculaRegiaoPonto(QVector2D _vt2dPonto, int _iLarguraCampo, int _iLarguraRegiao);

/**
     * @brief Retorna um ponto P3 na direção do vetor P2-P1 à uma distancia D do ponto P1.
     *
     * @param _vt2dPonto1 - P1
     * @param _vt2dPonto2 - P2
     * @param fDistanciaPonto1 - D
     * @return QVector2D - P3
     */
QVector2D vt2dReajustaPonto(QVector2D _vt2dPonto1, QVector2D _vt2dPonto2, float fDistanciaPonto1);

/**
     * @brief Retorna um vetor de pontos igualmente espaçados de acordo com o vetor de pontos fornecido.
     *
     * @param _vt2dPontosPosicionamento - Vetor de pontos base.
     * @param _iEspacamentoRobos - Espaçamento dos pontos.
     * @param _iQuantidadeEspacos - Quantidade de espaços.
     * @return QVector<QVector2D> - Vetor de pontos igualmente espaçados.
     */
QVector<QVector2D> vt2dPosicionaRobosVetorPontos(QVector<QVector2D> _vt2dPontosPosicionamento,
                                                 int _iEspacamentoRobos,
                                                 int _iQuantidadeEspacos);

/**
     * @brief Retorna o ponto P2 rotacionado em torno de P1 em um certo ângulo.
     *
     * @param _vt2dP1 - P1.
     * @param _vt2dP2 - P2.
     * @param _dAngulo_graus - Ângulo de rotação em graus. Sentido anti-horário é o positivo.
     * @return QVector2D - Ponto rotacionado.
     */
QVector2D vt2dRotaciona(QVector2D _vt2dP1, QVector2D _vt2dP2, double _dAngulo_graus);

/**
     * @brief Retorna os pontos onde o delta deve ficar.
     *
     * @param _vt2dPontos - Esses pontos definem as três retas da área de defesa e a reta da bola da seguinte maneira:
     * - Pontos 0,1,2,3: São os quatro pontos da área de defesa preferivelmente em ordem; SuperiorFundo, SuperiorFrente, InferiorFrente e InferiorFundo.
     * @param _iNRobos - Quantidade de robôs no delta.
     * @param _vt2dPosicaoGoleiro - Posição do goleiro aliado.
     * @param _iLadoCampo - Lado de defesa aliado.
     * @param _iLarguraGol - Largura do gol.
     * @param _iProfundidadeGol - Profundidade do gol.
     * @param _szCampo - Tamanho do campo.
     * @param _iContador - Contador de quantas vezes esta função foi chamada. Como esta função é recursiva ele impede que ela se chame infinitamente.
     * @return QVector<QVector2D> - Posição do delta.
     */
QVector<QVector2D> vt2dPontosDeltaQuadrado(QVector<QVector2D> _vt2dPontos,
                                           int _iNRobos,
                                           QVector2D _vt2dPosicaoGoleiro,
                                           int _iLadoCampo,
                                           int _iLarguraGol,
                                           int _iProfundidadeGol,
                                           QSize _szCampo,
                                           int _iContador);

/**
     * @brief Retorna os pontos de posicionamento do kickoff
     *
     * @param iRaioDelta
     * @param _vt2dCentrosCirculoDefesa
     * @param _vt2dPontos
     * @param _iNRobos
     * @param _vt2dPosicaoGoleiro
     * @param _iLadoCampo
     * @param _iLarguraGol
     * @param _iProfundidadeGol
     * @param _szCampo
     * @param _iContador
     * @return QVector<QVector2D>
     */
QVector<QVector2D> vt2dPontosKickOff(int iRaioDelta,
                                     QVector<QVector2D> _vt2dCentrosCirculoDefesa,
                                     QVector<QVector2D> _vt2dPontos,
                                     int _iNRobos,
                                     QVector2D _vt2dPosicaoGoleiro,
                                     int _iLadoCampo,
                                     int _iLarguraGol,
                                     int _iProfundidadeGol,
                                     QSize _szCampo,
                                     int _iContador);

/**
     * @brief Retorna a posição que o goleiro deve estar, baseado nas posições da bola.
     *
     * @param _vtBufferBola - Vetor de posições da bola.
     * @param _iLadoCampoAliado - Lado do campo aliado.
     * @param _iLarguraGol - Largura do gol.
     * @param _szCampo - Tamanho do campo.
     * @param _avancoGoleiro - Distância que o goleiro deve ficar da linha do gol.
     * @param delta - A função do delta também utiliza esta função, esta flag serve para indicar isso para a função.
     * @return QVector2D - Posição que o goleiro deve estar.
     */
QVector2D vt2dCalculaDefesaGoleiro(QVector<QVector2D> _vtBufferBola,
                                   int _iLadoCampoAliado,
                                   int _iLarguraGol,
                                   QSize _szCampo,
                                   float _avancoGoleiro,
                                   bool delta=false,
                                   bool* _hasIntersection=nullptr);

/**
     * @brief Retorna o ponto inicial e final da reta média dos pontos fornecidos.
     * Esta função utiliza o método do mínimos quadrados para calcular a reta média.
     *
     * @param _pontos - Vetor de pontos.
     * @return QVector4D - Ponto inicial e final da reta média no formato: (x,y,z,w) = (Pix, Piy, Pfx, Pfy).
     */
QLineF lineCalculaRetaMedia(QVector<QVector2D> _pontos);

/**
     * @brief Retorna true se o ponto fornecido está contido dentro do retângulo definido pelos pontos R1, R2.
     *
     * @param ponto - Ponto de teste.
     * @param R1 - Canto superior esquerdo do retângulo.
     * @param R2 - Canto inferior direito do retângulo.
     * @return bool - True se o ponto fornecido está contido no retângulo
     */
bool bPontoContidoRetangulo(QVector2D ponto, QVector2D R1, QVector2D R2);

/**
     * @brief Retorna a velocidade X,Y do robô a partir da odometria de cada roda.
     *
     * @param odRoda1 - Odometria da roda 1.
     * @param odRoda2 - Odometria da roda 2.
     * @param odRoda3 - Odometria da roda 3.
     * @param odRoda4 - Odometria da roda 4.
     * @return QVector2D - Velocidade linear (X,Y).
     */
QVector2D vt2dConverteOdometria(double odRoda1, double odRoda2, double odRoda3, double odRoda4);

/**
     * @brief Retorna a velocidade linear convertida em rotação para as 4 rodas.
     *
     * @param vt2dVel - Velocidade linear (X,Y) [0-100].
     * @param iVw - Velocidade angular [0-100].
     * @return QVector4D - Rotação de cada uma das quatro rodas [0-100].
     */
QVector4D vt4dConverteLinearRotacao(QVector2D vt2dVel, int iVw);

/**
     * @brief Retorna o ângulo [graus] entre dois vetores V1 e V2.
     *
     * @param vetor1 - V1.
     * @param vetor2 - V2.
     * @return double - Ângulo entre V1 e V2 em graus.
     */
double dAnguloVetor1Vetor2(QVector2D vetor1, QVector2D vetor2);

/**
     * @brief Retorna um vetor perpendicular a projecao do vetor V1 em V2
     *
     * @param V1.
     * @param V2.
     * @return QVector2D - Vetor perpendicular.
     */
QVector2D vt2dVetorPerpendicular(QVector2D V1, QVector2D V2);

/**
     * @brief Retorna true se o ponto fornecido está dentro da area de defesa definida.
     *
     * @param tamanhoCampo - Tamanho do campo.
     * @param larguraDefesa - Larguda da área de defesa.
     * @param profundidadeDefesa - Profundidade da área de defesa.
     * @param ladoCampo - Lado do campo a ser testado.
     * @param ponto - Posição da bola.
     * @return bool - True se a bola está dentro da area de defesa definida.
     */
bool bPontoDentroArea(QSize tamanhoCampo,
                      QSize areaDefesa,
                      int ladoCampo, QVector2D ponto);

/**
     * @brief *REMOVER*
     *
     * @param iIDsRobos
     * @param vt2dPosAtualRobos
     * @param vt2dDestinos
     * @param str
     * @return QVector<QPair<int, int> >
     */
QVector<QPair<int,int>> nIDnDestOtimizaDestinosRobos(QVector<int> iIDsRobos,
                                                     QVector<QVector2D> vt2dPosAtualRobos,
                                                     QVector<QVector2D> vt2dDestinos,
                                                     const char *str = __builtin_FUNCTION());

/**
     * @brief Este método deve ser utilizado na movimentação dos robos no delta
     * @details A partir das coordenadas x e y limites e da posição atual e destino do robô,
     * o robô movimenta-se em uma linha reta vertical ou horizontal. Os pontos do caminho são acrescentados ao vetor
     * \p vt2dCaminhoDelta
     * @param vt2dPosicaoAtual - Posição atual
     * @param vt2dDestino - Posição destino
     * @param szCampo - Tamanho do campo
     * @param iLarguraDefesa - Largura da defesa
     * @param iProfundidadeDefesa - Profundidade da área de defesa
     * @param iLadoCampo - Lado do campo
     * @param vt2dCaminhoDelta - vetor de coordenadas (referência)
     *
     * \see \a Auxiliar::vMovimentoXConstante \a Auxiliar::vMovimentoYConstante \a Auxiliar::vt_vt2dMovimentacaoDelta
     */
void vPosicaoNoDelta(const QVector2D &vt2dPosicaoAtual,
                     const QVector2D &vt2dDestino,
                     const QSize &szCampo,
                     const int &iLarguraDefesa,
                     const int &iProfundidadeDefesa,
                     const int &iLadoCampo,
                     QVector<QVector2D> &vt2dCaminhoDelta);

/**
     * @brief Acrescenta pontos em uma linha vertical até que a coordenada y de destino equivala à coordenada y início
     * @param vt2dCaminhoDelta - vetor de coordenadas (referência)
     * @param vt2dPosicaoAtual - Posição inicial
     * @param vt2dDestino - Posição destino
     * @param passo - Passo da movimentação
     */
void vMovimentoXConstante(QVector<QVector2D> &vt2dCaminhoDelta,
                          const QVector2D &vt2dPosicaoAtual,
                          const QVector2D &vt2dDestino,
                          const quint16 passo = globalConfig.robotDiameter);

/**
     * @brief Acrescenta pontos em uma linha horizontal até que a coordenada y de destino equivala à coordenada y início
     * @param vt2dCaminhoDelta - vetor de coordenadas (referência)
     * @param vt2dPosicaoAtual - Posição inicial
     * @param vt2dDestino - Posição destino
     * @param passo - Passo da movimentação
     */
void vMovimentoYConstante(QVector<QVector2D> &vt2dCaminhoDelta,
                          const QVector2D &vt2dPosicaoAtual,
                          const QVector2D &vt2dDestino,
                          const quint16 passo = globalConfig.robotDiameter);

/**
     * @brief Retorna o vetor com as coordenadas da movimentação do robô
     * @param vt2dPosicaoAtual - Posição atual
     * @param vt2dDestino - Posição destino
     * @param szCampo - Tamanho do campo
     * @param iLarguraDefesa - Largura da área
     * @param iProfundidadeDefesa - Profundidade da área
     * @param iLadoCampo - Lado do campo
     * \see \a Auxiliar::vMovimentoXConstante \a Auxiliar::vMovimentoYConstante \a Auxiliar::vPosicaoNoDelta
     */
QVector<QVector2D> vt_vt2dMovimentacaoDelta(const QVector2D vt2dPosicaoAtual,
                                            const QVector2D vt2dDestino,
                                            const QSize szCampo,
                                            const int iLarguraDefesa,
                                            const int iProfundidadeDefesa,
                                            const int iLadoCampo);

/**
     * @brief Retorna o sinal do número fornecido.
     * @param val - Número.
     * @return int - Sinal, pode ser +1 ou -1.
     */
int iSinal(float val);

/**
     * @brief Retorna true se o ponto fornecido estiver à uma distância menor que a mínima de todos os pontos do vetor.
     * @param pontoTeste - Ponto de teste.
     * @param pontos - Vetor de pontos.
     * @param distanciaMinima - Distância mínima.
     * @return bool - True se o ponto estiver próximo dos pontos fornecidos, senão, false.
     */
bool bPontoTesteMuitoProximo(QVector2D pontoTeste, QVector<QVector2D> pontos, int distanciaMinima);

/**
     * @brief Ajusta o ponto inicial para dentro do campo caso o mesmo não esteja.
     * @param vt2dPosicaoAjuste_1 - Posição de referência em que o ponto inicial será ajustado, i.e., o ponto inicial irá em direção á este ponto para ficar dentro do campo.
     * @param posInicial - Ponto inicial.
     * @param iDistanciaInicial - Distância inicial que a posição inicial pode estar da posição de ajuste 1 [mm].
     * @param iReducao - Distância em que a distância inicial será reduzida a cada passo [mm].
     * @return QVector2D - Ponto inicial ajustado.
     */
QVector2D vt2dCorrigePontoDentroCampo(QVector2D tamanhoCampo,
                                      QVector2D vt2dPosicaoAjuste_1,
                                      QVector2D posInicial,
                                      int iDistanciaInicial,
                                      int iReducao);

/**
     * @brief Retorna true se o ponto fornecido estiver fora do campo.
     * @param tamanhoCampo - Tamanho do campo.
     * @param pontoTeste - Ponto.
     * @param offSet - Tamanho do offset do campo a ser considerado. Se nenhum valor for fornecido, não considera o offset.
     * @return bool - True se o ponto fornecido estiver fora do campo, senão, false.
     */
bool bPontoForaCampo(QSize tamanhoCampo, QVector2D pontoTeste, int offSet = 0);

/**
     * @brief vt2dCalculaCurvaBezier Calcula uma curva de bezier que se ajusta aos pontos fornecidos
     * @param _pontos
     * @return
     */
QVector<QVector2D> vt2dCalculaCurvaBezier(const QVector<QVector2D> &_pontos);

/**
     * @brief vConverteAnguloRobo Converte o angulo do robô de [-inf;inf] para [0;2*pi] [rad]
     * @param angulo - Ângulo do robô em radiano.
     */
void vConverteAnguloRobo(float &angulo);

///
/// \brief Usada nas jogadas de Redirect Goal e Redirect Pass (BETA)
/// \details Cálculo da mira do receptor
/// \todo Calcular a ponto ideal da mira do receptor a partir dos parâmetros da função,
///  isso pode ser encontrado em [TDPs antigos]() ou resolvido como um problema de mêcanica
///  dp corpo rígido ou algo assim.
///
/// \param posBola - Posição atual da bola
/// \param posicaoReceptor - Posição do receptor
/// \param pontoBolaDestino - Posição desejada da bola depois do Redirect (
///  mormalmente o gol adversário ou algum robô aliado)
/// \param dVelocidadeChegada - Módulo da velocidade de chegada
/// \param dVelocidadeSaida - Módulo da Velocidade de saída
///
QVector2D vt2dPontoMiraReceptorRedirect(const QVector2D& posBola,
                                        const QVector2D& posicaoReceptor,
                                        const QVector2D& pontoBolaDestino,
                                        const double& dVelocidadeChegada = 0,
                                        const double& dVelocidadeSaida = 0);

///
/// \brief Usada nas jogadas de Redirect Goal e Redirect Pass (BETA)
/// \details Faz o robô passador mirar no roller do receptor.
/// \param posicaoReceptor - Posição do receptor
/// \param pontoMiraReceptor - Mira atual do receptor
/// \return
///
QVector2D vt2dPontoMiraPassadorRedirect(const QVector2D& posicaoReceptor,
                                        const QVector2D& pontoMiraReceptor);

///
/// \brief Usada nas jogadas de Redirect Goal e Redirect Pass (BETA)
/// \details Calcula a posição do receptor de forma que a bola venha
///  em seu roller
/// \param posicaoReceptor - Posição atual do receptor
/// \param pontoMiraReceptor - Mira atual do receptor
/// \param posBola - Posição atual da bola
/// \param vt2dVelocidadeBola - Vetor velocidade da bola (indica a direção da mesma)
/// \return
///
QVector2D vt2dPosicaoReceptorRedirect(const QVector2D& posicaoReceptor,
                                      const QVector2D& pontoMiraReceptor,
                                      const QVector2D& posBola,
                                      const QVector2D& vt2dVelocidadeBola);
///
/// \brief A função retorna o vetor de entrada colocando os parâmetros em ordem crescente
/// \param vt_vt2d_X_ID__Y_param - X é o ID do robô e Y é algum parâmetro
///
QVector<QVector2D> vt_vt2dRetornaVetorEmOrdemCrescente(QVector<QVector2D> vt_vt2d_X_ID__Y_param);

///
/// \brief A função verifica a intersecção de objetos dentro do triângulo ACBD
/// \param A - ponta do triângulo
/// \param B - ponto médio do lado CD
/// \param _bIgnorarBola
/// \param _vt3dObjetosEmCampo
/// \param graus_2_alfa - o ângulo CÂD
///
bool bInterseccaoObjetosTrianguloAngulo(const QVector2D A,
                                        const QVector2D B,
                                        const bool _bIgnorarBola,
                                        const QVector<QVector3D> &_vt3dObjetosEmCampo,
                                        const float graus_2_alfa);

///
/// \brief A função verifica a intersecção de objetos dentro do triângulo ACBD
/// \details se bLinhaY for declarada true a reta CBD será paralela ao eixo Y
/// \details caso contrário a reta será perpendicular à reta AB
/// \param A - ponta do triângulo
/// \param B - ponto médio do lado CD
/// \param _bIgnorarBola
/// \param _vt3dObjetosEmCampo
/// \param distancia_CD - distancia entre C e D
/// \param bLinhaY - parâmetro determina se a linha CDA será paralela ao eixo Y
///
bool bInterseccaoObjetosTrianguloDistancia(const QVector2D A,
                                           const QVector2D B,
                                           const bool _bIgnorarBola,
                                           const QVector<QVector3D> &_vt3dObjetosEmCampo,
                                           const float distancia_CD,
                                           const bool bLinhaY = false);

///
/// \brief Calcula a força do passe considerando a distância do destino à origem
/// \param posBola
/// \param posReceptor
/// \param szCampo
///
float fForcaPasse(const QVector2D& posBola,
                    const QVector2D& posReceptor,
                    const QSize &szCampo);

///
/// \brief Verifica se o robô está na reta da bola.
/// \param posRobo
/// \param posBola
/// \param vt2dVelocidadeBola
/// \return
///
bool bRoboNaRetaDaBola(const QVector2D posRobo,
                       const QVector2D posBola,
                       const QVector2D vt2dVelocidadeBola,
                       const float erroAdmissivel = 30);

///
/// \brief Projeção do Vetor A no Vetor B
/// \param A - vetor projetor
/// \param B - vetor projetado
///
QVector2D vt2dProjecao_de_A_em_B(const QVector2D& A,
                                 const QVector2D& B);

///
/// \brief Verifica se o ponto X está contido na região entre os pontos A e B.
/// \details A região é um retângulo cuja largura é igual
/// a (2*raio + distanciaAB)
/// e a altura é igual
/// a (2*raio)
/// \param A
/// \param B
/// \param X
/// \param raio
///
bool bPontoDentroDaRegiao(const QVector2D& A,
                          const QVector2D& B,
                          const QVector2D& X,
                          const float& raio);

///
/// \brief Determina as coordenadas da posição (x,y) e da mira (z,w) do Robô
/// \param posAliado
/// \param posAdversario - Posição do adversário com a bola
/// \param posBola
/// \param posGolAliado - Centro do gol aliado
/// \return
///
QVector4D vt4dPosicaoLadraoDeBola(const QVector2D& posAliado,
                                  const QVector2D& posAdversario,
                                  const QVector2D& posBola,
                                  const QVector2D& posGolAliado);

///
/// \brief Retorna o ângulo livre do gol adversário
/// \param posBola
/// \param iLadoCampo
/// \param iLarguraGol
/// \param szCampo
/// \param _vt3dPosicoesOponentes
///
double dAnguloEmGrausLivreParaChute(const QVector2D posBola,
                                    const int iLadoCampo,
                                    const int iLarguraGol,
                                    const QSize szCampo,
                                    const QVector<QVector3D>& _vt3dPosicoesOponentes);
///
/// \brief Retorna o ponto mais livre do gol adversário
/// \param posBola
/// \param iLadoCampo
/// \param iLarguraGol
/// \param vt2dTamanhoCampo
/// \param _vt3dPosicoesOponentes
///
QVector2D vt2dCentroLivreGolAdversario (const QVector2D posBola,
                                        const int iLadoCampo,
                                        const int iLarguraGol,
                                        const QSize szCampo,
                                        const QVector<QVector3D>& _vt3dPosicoesOponentes);

/**
* @brief iCalculaBinomioNewton Calcula o binômio de Newton
* @param n
* @param k
* @return
*/
int iCalculaBinomioNewton(int n, int k);

///
/// \brief Retorna a distância do robô adversário mais próximo ao ponto fornecido.
/// \param posicaoReceptor - Posição do ponto analisado
/// \param vt3dPosicoesOponentes - Posição dos oponentes
/// \return Distância do robô adversário mais próximo
///
double dLiberdadeDeMarcacao(const QVector2D& posicaoReceptor,
                            const QVector<QVector3D> &vt3dPosicoesOponentes);

///
/// \brief Ângulo de rotação para o robô conseguir mirar ao ponto desejado
/// \param posicaoDoRobo - Posição do robô
/// \param pontoMiraAtual - Mira atual do robô
/// \param pontoMiraDestino - Mira Desino do robô
/// \return Ângulo de rotação
///
double dAnguloDeRotacao(const QVector2D posicaoDoRobo,
                        const QVector2D pontoMiraAtual,
                        const QVector2D pontoMiraDestino);

///
/// \brief Ângulo livre para a realização de um passe.
/// \details Retorna na verdade um valor inteiro entre 0 e 45°
/// \note Necessita melhoria.
/// \param posBola - Posição da bola atual (equivalente à posição do passador)
/// \param posicaoReceptor - Posição do receptor
/// \param _vt3dPosicoesOponentes - Posição dos adversários
/// \param passo - Passo da verificação
/// \return Ângulo livre
///
double dAnguloEmGrausLivreParaPasse(const QVector2D posBola,
                                    const QVector2D posicaoReceptor,
                                    const QVector<QVector3D> &_vt3dPosicoesOponentes,
                                    const qint8 passo = 1);

double dDeltaXis(const QVector2D posicaoReceptor,
                 const QVector2D posicaoPassador,
                 const qint8 iLadoCampoDefesa);
///
/// \brief Retorna um vetor contendo o maior valor do vetor de entrada e seu respectiva índice
/// \param vtiEntrada - Vetor de entrada
/// \note Retorna um vetor vazio se o vetor de entrada for um vetor vazio
/// \return QVector<short>{maiorValor, Indice}
///
QVector<qint16> vtiMaiorValorESeuIndice(const QVector<qint16> &vtiEntrada);


/**
 * @brief Faz as contas para calcular a média de FPS.
 * @param fpsCounter - Vetor com as medições de FPS.
 * @return int - FPS médio.
 */
int iCalculaFPS(QVector<int> &fpsCounter);

void vConfiguraDarkModePlot(QCustomPlot* qcp);

QString strFromID_Jogadas(const ID_Jogadas idJogada);

/**
 * @brief Realiza a transformação de coordenadas
 * @param coordenadasGenericas
 * @param iLadoCampo
 * @param szCampo
 * @param vt2dTamanhoGol
 * @return
 */
QVector2D vt2dCoordenadasGenericasParaCoordenadasCampo(const QVector2D coordenadasGenericas,
                                                       const int iLadoCampo,
                                                       const QSize szCampo,
                                                       const QSize szGol);

/**
 * @brief Realiza a transformação de coordenadas
 * @param coordenadasCampo
 * @param iLadoCampo
 * @param szCampo
 * @param szGol
 * @return
 */
QVector2D vt2dCoordenadasCampoParaCoordenadasGenericas(const QVector2D coordenadasCampo,
                                                       const int iLadoCampo,
                                                       const QSize szCampo,
                                                       const QSize szGol);

///
/// \brief Realiza um mapa de marcação <idAliado, idOponente>
/// \details Prioriza sempre os oponentes mais próximos ao gol aliado. A saída tem
///  tamanho igual a `marcadores`. Caso o número de marcadores seja maior que o número
///  de oponentes, os robôs livres receberão o par <idAliado, -1>.
/// \param marcadores - Mapa com os ids e as posições dos marcadores
/// \param oponentes - Mapa cos os ids e as posições dos oponentes
/// \param centroGolAliado - Posição do centro do gol aliado
///
QMap<qint8, qint8> mapIntIntMarcaOponentesMaisProximosAoGol(const QMap<qint8, QVector2D> marcadores,
                                                            const QMap<qint8, QVector2D> oponentes,
                                                            const QVector2D centroGolAliado);

///
/// \brief Retorna a coordenada y da posição de destino da bola, dados os parâmetros de entrada.
/// \param vt2dVelocidadeBola - Vetor com as velocidades x e y da bola
/// \param posBola - Coordenadas da posição da bola
/// \param xDestino - Coordenada x correspondente da posição de destino
///
float fProjecaoBolaNoGolAliado(const QVector2D vt2dVelocidadeBola,
                               const QVector2D posBola,
                               const float xDestino);

///
/// \brief Retorna a melhor posição para o goleiro defender a bola
/// \param vt2dVelocidadeBola - Vetor com as velocidades x e y da bola
/// \param posBola - Coordenadas da posição da bola
/// \param posicaoGoleiro - Posição atual do goleiro
/// \param xGoleiroMaximo - O valor máximo da coordenada x do goleiro. Usado para evitar que o
///  goleiro acabe entrando no gol na hora de defender a bola.
///
QVector2D vt2dDestinoGoleiroBolaEmMovimento(const QVector2D vt2dVelocidadeBola,
                                            const QVector2D posBola, const QVector2D posicaoGoleiro,
                                            const float xGoleiroMaximo);

///
/// \brief Retorna a coordenada y da melhor posição do goleiro quando a bola não está em direção ao gol aliado.
/// \details Considerando a posição da bola e a coordenada x de destino do goleiro, determina a
///  coordenada y de forma que o goleiro deixe o menor espaço do gol livre.
/// \param posBola - Coordenadas da posição da bola
/// \param vt2dCentroGoalAliado - Coordenadas do meio do gol aliado
/// \param larguraGol - Largura do gol aliado
/// \param xGoleiro - Coordenada x da posição do goleiro
///
float fMelhorPosicaoGoleiroParaFecharGolAliado(const QVector2D posBola,
                                               const QVector2D vt2dCentroGoalAliado,
                                               const float larguraGol,
                                               const float xGoleiro);

///
/// \brief Retorna um objeto com a cor correspondente ao valor `valor` dadas as cores da escala.
/// \param cor0 - Cor referente ao valor 0
/// \param cor1 - Cor referente ao valor 1
/// \param valor - Valor na faixa de 0 a 1.
///
QColor corEscalaLinear(const QColor& cor0, const QColor& cor1, const double valor);


///
/// \brief Retorna um objeto com a cor correspondente ao valor `valor` dadas as cores e os valores máximo
///  e mínimo da escala.
/// \param cor0 - Cor referente ao valor `valorMínimo`
/// \param cor1 - Cor referente ao valor `valorMáximo`
/// \param valor - Valor na faixa entre `valorMáximo` e `valorMínimo`.
/// \param valorMínimo - Valor mínimo da escala
/// \param valorMáximo - Valor máximo da escala
///
QColor corEscalaLinear(const QColor& cor0, const QColor& cor1, const double valor,
                       const double valorMinimo, const double valorMaximo);

///
/// \brief Retorna a posição de um único robô no delta inteligente dada a posição do adversário a ser marcado.
/// \param posAdversario
/// \param larguraArea
/// \param profundidadeArea
/// \param centroGolAliado
///
QVector2D vt2dPosicaoDeltaInteligente(const QVector2D posAdversario,
                                      const int larguraArea, const int profundidadeArea,
                                      QVector2D centroGolAliado);

///
/// \brief Cria um vetor com as posições dos robôs no delta inteligente.
/// \param nRobosDelta - Quantidade de robôs aliados no delta
/// \param posicaoAdversarios - Vetor com as posições de todos os adversário
/// \param larguraArea
/// \param profundidadeArea
/// \param centroGolAliado
/// \param distanciaMaximaConsiderada - Distância máxima de um adversário ao centro aliado
///  para que este seja considerado como relevante na criação do delta inteligente.
///
QVector<QVector2D> vt_vt2dPosicoesDestinoDeltaInteligente(const int nRobosDelta,
                                                          QVector<QVector2D> posicaoAdversarios,
                                                          const QVector2D posBola,
                                                          const int larguraArea,
                                                          const int profundidadeArea,
                                                          QVector2D centroGolAliado,
                                                          const float distanciaMaximaConsiderada);

///
/// \brief Retorna a #REGIAO_CAMPO da posição \a posPonto
/// \param posPonto - Posição do ponto a ser analisada
/// \param ladoCampo
/// \param tamanhoCampo
/// \param tamanhoArea
/// \see Auxiliar::vt2dCoordenadasCampoParaCoordenadasGenericas
/// \see Auxiliar::vt2dCoordenadasGenericasParaCoordenadasCampo
///
REGIAO_CAMPO regiaoCampoPonto(const QVector2D posPonto, const int ladoCampo,
                              const QSize tamanhoCampo, const QSize tamanhoArea);

///
/// \brief Retorna a quantidade de posições no vetor \a pontos de robôs na #REGIAO_CAMPO \a regiaoCampo
/// \param pontos - Vetor com as posições a serem analisadas
/// \param regiaoCampo
/// \param ladoCampo
/// \param tamanhoCampo
/// \param tamanhoArea
/// \return
///
int pontosRegiaoCampo(const QVector<QVector2D> pontos, const REGIAO_CAMPO regiaoCampo,
                      const int ladoCampo, const QSize tamanhoCampo, const QSize tamanhoArea);

/**
 * @brief Adiciona as tags em HTML para mudar a cor de um texto.
 *
 * @param _text - Texto desejado.
 * @param _color - Cor desajada.
 *
 * @return
 */
QString strColorText(const QString _text, const QString _color);

/**
 * @brief Deixa o texto em negrito.
 *
 * @param _text - Texto desejado.
 *
 * @return
 */
QString strBoldText(const QString _text);

/**
 * @brief Calcula a interseção entre a reta da bola e uma reta paralela à frente
 * do robô.
 *
 * @param _ballPoints - Pontos da bola para formar a reta.
 * @param _robotPos - Posição do robô.
 * @param _robotAim - Mira do robô.
 * @param _robotLineLength - Tamanho da reta utilizada, metade fica para a
 * direta do robô, outra metade para a esquerda.
 *
 * @return
 */
QVector2D vt2dBallIntersection(const QVector<QVector2D> &_ballPoints,
                               const QVector2D _robotPos,
                               const QVector2D _robotAim,
                               double _robotLineLength);

QVector2D vt_vt2dPositionsIndividualMarking(const int nRobosDelta,
                                            QVector<QVector2D> posicaoAdversarios,
                                            const QVector2D posBola,
                                            QVector2D centroGolAliado,
                                            const float distanciaMaximaConsiderada);

};



#endif // AUXILIAR_H
