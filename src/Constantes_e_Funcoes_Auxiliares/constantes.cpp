/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "constantes.h"

#include <QString>

GlobalConfig globalConfig{"Global"};

//****************************************** Movimentacao ************************
// double STOP_LINEAR_LIMIT   = 1.5;
int iTamanhoTracoRobo    = 50;
// double dVelocidadeDelta     = 1.5; // 0 - 100
// double ROBOT_LINEAR_LIMIT = 2.5;
// double ROBOT_ANGULAR_LIMIT = 5;
//****************************************** Movimentacao ************************

//****************************************** Parametros do Campo *****************
int iTamanhoBufferBola      = 18;
// int iDeslocamentoBola       = 180;
// int iOffsetCampo            = 400;
// int iLarguraDivisaoCampo    = 1500;
// int iRaioSegurancaBolaStop  = 500 + iDiametroRobo/2;
// int iFramesParaSairDaVisao  = 20;
double dFPS                 = 62.5;
bool bDesenharAreaSeguranca = false;
int iAnguloGolContra        = 60;
float fBateriaMinima        = 10.9;
bool bUsarKickSensorNaVisao = true;
//****************************************** Parametros do Campo *****************

//****************************************** Parametros Path-Planners *********************
// float fDistanciaSeguranca  = iDiametroRobo;
// int iRaioSegurancaRobo     = 240; //Distância de segurança entre os robôs
// int iRaioSegurancaBola     = 300; //Distância de segurança entre o robô e a bola
// int iRaioSegurancaOponente = 240; //Distancia de seguranca entre os robos oponentes
//****************************************** Parametros Mapa *********************

//****************************************** Jogadas *****************************
// int iLarguraMinimaMira          = 270;
// int iEspacamentoRobosDelta      = 235;
// int iDistanciaRobosAreaDefesa   = 360;
int iDistanciaReceptorCobranca  = 3000;
// int iForcaChuteCalibrado        = 8;

float fDistanciaParaChutePuxadinha = 100;

bool bDeltaInteligenteNormal = false;
//****************************************** Jogadas *****************************


float fTempoPosicionamentoFreeKick = 1.5;
float fAnguloDeErroChuteNormal = 7;
float fAnguloDeErroPasseNormal = 5;
float fDistanciaCobradorBolaParada = 400;
float fDistanciaGolAliadoEvitar = 3000;
float fDistanciaBarreiraFreeKick = 1200;
float fVelocidadeBolaChegadaPasse = 2;
bool bUtilizarJogadasEnsaiadasFreeKick = true;
float fDistanciaMinimaParaCavadinha = 4000;
TipoMarcacao tipoMarcacao = MarcacaoConservadora;
/********************************** Atributos do robô *****************************************************/
int iTempoRecargaChute = 15;
bool bGiraComABolaNoRoller = false;
/********************************** Atributos do robô *****************************************************/

QString strTempoReferee = QStringLiteral("5:00");

//int iRaioDeSegurancaBolaNotNormal = iRaioSegurancaBola;

// bool bControlarSimulacao = false;

QString strModeloForcaChute = "simulacao_grsim";
