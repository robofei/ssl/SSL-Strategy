/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CONSTANTES_H
#define CONSTANTES_H
///
/// \file constantes.h
/// \brief Constantes globais e variáveis globais utilizadas em várias pates do código além da definição de alguns enums.
///

#include <QtGlobal>
#include <QtMath>
#include "Config/globalconfig.h"
#include "math.h"
#include "macros.h"

extern GlobalConfig globalConfig;

//****************************************** Movimentacao ************************
// extern double STOP_LINEAR_LIMIT;             /**< Velocidade máxima durante o Stop [0 - 100]. */
extern int iTamanhoTracoRobo;              /**< Quantidade de pontos no vetor de posições do robô monitorado. */
// extern double dVelocidadeDelta;               /**< Velocidade máxima para o delta [0 - 100]. */
// extern double ROBOT_LINEAR_LIMIT;  /**< Velocidade máxima do robô durante o jogo [m/s]. */
// const double ROBOT_MAX_LINEAR = 2.5; /**< Velocidade máximo do robô que o robô REAL consegue chegar [m/s].  */
// extern double ROBOT_ANGULAR_LIMIT; /**< Velocidade angular máxima do robô durante o jogo [rad/s]. */
// const double ROBOT_MAX_ANGULAR = 6.5; /**< Velocidade angular máxima que o robô REAL consegue chegar [rad/s]. */
// const double GOALIE_LINEAR_LIMIT = 2.5;   /**< Velocidade máxima para o goleiro [m/s]. */

// const double dVelocidadeTestes     = 1; /**< \todo Criar uma opção para setar essa variável */
//****************************************** Movimentacao ************************


const int iNumeroTotalCameras = 4;   /**< Número de câmeras no campo. */
// const int iDiametroBola       = 43;  /**< Diâmetro da bola [mm]. */
const float fVelocidadeMaximaBola = 6; /**< Velocidade máxima da bola [m/s]. */
// const int iDiametroRobo       = 180; /**< Diâmetro de um robô [mm]. */

// Nota: na simulação podemos deixar o valor de `PLAYERS_PER_SIDE` como 11 na divisão A e
//  6 na divisão B; porém, no campo real, devemos deixar 8 para a divisão B e 16 para a
//  divisão A (ou 8 para os dois).
// #if ROBO_DIVISAO == ROBO_DIVISAO_A
// const int PLAYERS_PER_SIDE    = 16;   /**< Quantidade de jogares por time. */
// #else
// const int PLAYERS_PER_SIDE    = 8;   /**< Quantidade de jogares por time. */
// #endif
const int MAX_IDS = 16;

const int iEspessuraLinha     = 0;   /**< Espessura das linhas do campo. Deve ser removido futuramente. */
const int iTamanhoBufferRobo  = 25;  /**< Tamanho do buffer que armazena as posições anteriores do robô. */
extern int iTamanhoBufferBola;       /**< Tamanho do buffer que armazena as posições anteriores da bola. */
// extern int iDeslocamentoBola;        /**< Deslocamento mínimo para considerar que a bola foi movida [mm]. */
// extern int iOffsetCampo;             /**< Espaço extra considerado além dos limites do campo [mm]. */
// extern int iLarguraDivisaoCampo;     /**< Largura das regiões do campo, determinadas pelas linhas tracejadas no desenho [mm]. */
// extern int iRaioSegurancaBolaStop;       /**< Raio de distância que o robô deve ficar da bola durante o Stop [mm]. */
// extern int iFramesParaSairDaVisao;   /**< Quantidade de pacotes para considerar que o robô saiu da visão. */
extern double dFPS;                  /**< FPS aproximado das câmeras. */
extern bool bDesenharAreaSeguranca;  /**< Habilita/Desabilita desenhar a área de segurança em volta dos robôs. */
extern QString strTempoReferee;      /**< Tempo restante de jogo. */
extern int iAnguloGolContra;         /**< Ângulo máximo para garantir que o robô não chute em direção ao gol [Graus]. */
extern float fBateriaMinima;         /**< Bateria mínima para o robô permanecer em campo [V].
                                       @see #MAX_BAT, #MAX_VOLTAGE_DROP, IndicadorStatusRobo
                                       @note Não é atribuído como const pois o valor pode
                                                  ser modificado no decorrer da partida */

extern bool bUsarKickSensorNaVisao;

/**
 * @brief Define a cor do time.
 *
 */
enum TeamColor
{
    timeNenhum = 0,
    timeAzul = 1,
    timeAmarelo = 2,
};

/**
 * @brief Tipos de objetos que existem
 * @details Isto geralmente é utilizado em conjunto com os métodos
 * AmbienteCampo::vt3dPosicaoObjetos, AmbienteCampo::vt3dPegaPosicaoTodosObjetos e
 * outras
 *
 */
enum TipoObjeto
{
    otOponente,      //0
    otAliado,        //1
    otBola,          //2
    otLinhaPenalty,   //3
    otDestino
};

//****************************************** Parametros Path-Planners *********************
// extern float fDistanciaSeguranca;  /**< Menor distância que o trajeto do path-planner pode estar de um obstáculo. */
// extern int iRaioSegurancaRobo;     /**< Distância de segurança entre os robôs aliados [mm]. */
// extern int iRaioSegurancaBola;     /**< Distância de segurança entre os robôs aliados e a bola[mm]. */
// extern int iRaioSegurancaOponente; /**< Distância de segurança entre os robôs aliados e oponentes [mm]. */
const float fResolucaoCampo  = 50; /**< Resolução do mapa do A* [mm]. */
const int iAnguloGrafo       = 30; /**< Angulo entre os pontos criados para cada objeto no grafo [Graus]. */
//****************************************** Parametros Mapa *********************

//****************************************** Parâmetros Jogada Pênalti *********************


///
/// \brief Indica o estado da jogada de cobrar o pênalti
/// \see \a clsJogadas::vRoboExtraBolaPenalty
///
enum ESTADO_JOGADA_PENALTY : quint8
{
    POSICIONANDO_PARA_CHUTAR_NO_CANTO_ERRADO = 0,
    POSICIONANDO_PARA_CHUTAR_NO_CANTO_CORRETO = 1,
    CHUTANDO_NO_CANTO_CORRETO = 2,
};

//****************************************** Jogadas *****************************
/**
 * @brief Define o tipo de cruzamento numa cobrança, no cruzamento em Y o chute é feito predominantemente na direção +- Y,
 * no cruzamente em X o chute é feito predominantemente na direção +- X
 */
enum TipoCruzamento
{
    CruzamentoX = 0,
    CruzamentoY = 1
};
const int iJogadasAtaque = 6;           /**< Quantidade de jogadas de ataque. Isto é utilizado na matriz de restrição de jogadas.*/
// extern int iLarguraMinimaMira;          /**< Largura mínima livre para tentar mirar a bola [mm]. */
// extern int iEspacamentoRobosDelta;      /**< Espaço entre os robôs do delta [mm]. */
// extern int iDistanciaRobosAreaDefesa;   /**< Distância entre os robôs do delta e a área de defesa [mm]. */
extern int iDistanciaReceptorCobranca;  /**< Distância entre o receptor e o cobrador de uma jogada. */
// extern int iForcaChuteCalibrado;        /**< Força do chute máxima para 6.5 m/s [0-100]. */

// const int iDistanciaCentroRoboFrente = 80;

// const int iDistanciaMaximaRoboBola = 88;/**< Distância do robô à bola quando esta estiver dominada por este. */
//const int iRaioDeSegurancaBolaNormal = iDistanciaMaximaRoboBola + iDiametroBola/2 + 50;/**< Valor da variável iRaioSegurancaBola
//                                                                                         quando em Normal Start */
//extern int iRaioDeSegurancaBolaNotNormal; /**< Valor da variável iRaioSegurancaBola quando em qualquer estado, menos Normal Start */

extern float fDistanciaParaChutePuxadinha;

extern bool bDeltaInteligenteNormal; /** Define o tipo do delta utilizado (entre padrão e inteligente).*/

//****************************************** Jogadas *****************************

extern float fTempoPosicionamentoFreeKick; /**< Tempo para os robôs se posicionarem nas jogadas de Free Kick.
                                               Deve ser obsoleta em breve (ver https://gitlab.com/robofei/ssl/SSL-Strategy/-/issues/11). */
extern float fAnguloDeErroChuteNormal; /**< Ângulo de erro máximo (em graus) para um chute ao gol durante o estado
                                             ESTADOS_ESTRATEGIA::NORMAL_CHUTE */
extern float fAnguloDeErroPasseNormal; /**< Ângulo de erro máximo (em graus) passar a bola a outro robô durante o estado
                                             ESTADOS_ESTRATEGIA::NORMAL_PASSE */
extern float fDistanciaCobradorBolaParada; /**< Distância de cobrador à bola nas jogadas de bola parada. [mm]*/

extern float fDistanciaGolAliadoEvitar; /**< Parâmetro utilizado para evitar que robôs muito próximos
                                             à bola seja receptores.
                                             @see AmbienteCampo::vtiReceptoresProibidos*/

extern float fDistanciaBarreiraFreeKick; /**< Distância mínima que o robô deve manter-se à bola durante o free kick. */

extern float fVelocidadeBolaChegadaPasse; /**< Velocidade desejada da bola ser recebida pelo receptor
                                               em uma jogada de passe*/

const  bool bDistanciaVerificaRoboComBola = true;

extern bool bUtilizarJogadasEnsaiadasFreeKick; /**< Define se serão utilizadas jogadas ensaiadas no
                                                    free kick.*/

extern float fDistanciaMinimaParaCavadinha; /**< Distância mínima para que o robô realize o passe
                                                 com um Chip Kick.*/

enum TipoMarcacao
{
    MarcacaoConservadora,
    MarcacaoInterceptacao
};

extern TipoMarcacao tipoMarcacao;

/********************************** Atributos do robô *****************************************************/
extern int iTempoRecargaChute; /**< Tempo necessário para o robô chutar a bola novamente [s]. */
extern bool bGiraComABolaNoRoller; /**< Indica a forma como o robô domina a bola,
                                        se esta variável for \a false, o robô deve contornar a
                                        bola, o que é mais lento porém mais seguro. */


/**
 * @brief Determina o ID de todas as jogadas.
 *
 */
enum ID_Jogadas
{
    Jogada_Invalida = -1,
    Nenhuma = 0  , // Dessa forma a contagem inicia-se do zero.
    Robo_Goleiro , // este vale 1
    TimeOut      , // este vale 2
    // e assim por diante...
    KickOff_Cobrador             ,
    KickOff_Receptor             ,
    KickOff_Defesa               ,

    Penalty_Defesa               ,
    Penalty_Cobrador             ,

    FreeKick_Cobrador            ,
    FreeKick_Receptor            ,
    FreeKick_Defesa_Marcador     ,
    FreeKick_Defesa_Barreira     ,
    FreeKick_Defesa_Delta        ,
    FreeKick_Meia_Mines          ,
    FreeKick_Meia_Marcacao       ,
    FreeKick_Atacante_Mines      ,

    Normal_Ladrao_Bola           ,
    Normal_Chutador              ,
    Normal_Passador              ,
    Normal_Receptor              ,

    Normal_Defensor_Delta        ,
    Normal_Defensor_Marcacao     ,

    Normal_Meia_Marcacao         ,
    Normal_Meia_Mines            ,

    Normal_Atacante_Mines        ,

    BallPlacement_Principal      ,
    BallPlacement_Auxiliar       ,
    BallPlacement_Cone           ,

    Jogada_Ensaiada_Cobrador            ,
    Jogada_Ensaiada_Receptor_Real       ,
    Jogada_Ensaiada_Receptor_Aparente_0 ,
    Jogada_Ensaiada_Receptor_Aparente_1 ,

    Stop_Perseguidor_Bola ,
    Stop_Atacante_Mines   ,
    Stop_Meia_Mines       ,
    Stop_Defensor_Delta   ,

    Subs_Entrando         ,
    Subs_Saindo
};

/**
 * @brief Determina o ID de todos os testes.
 *
 */
enum ID_Testes
{
    tstNenhum          ,
    tstPathPlanner     ,
    tstMovClick        ,
    tstPasses          ,
    tstRobotController ,
    tstDecisoes
};


/**
 * @brief Estrutura de parâmetros para o teste de passes.
 *
 */
typedef struct DADOS_TESTE_PASSE
{
    int iRoboID1;          /**< ID do robô 1 */
    int iRoboID2;          /**< ID do robô 2 */
    int iDistanciaRobos;   /**< Distância entre os robôs. */
    int iForcaChute;       /**< Força do chute para o passe. */
    int iVelocidadeRoller; /**< Velocidade do roller. */

    DADOS_TESTE_PASSE()
    {
        iRoboID1 = -1;
        iRoboID2 = -1;
        iDistanciaRobos = -1;
        iForcaChute = -1;
        iVelocidadeRoller = -1;
    }
}DADOS_TESTE_PASSE;

///
/// \brief Utilizado na \a clsEstrategia para verificar o time com a posse de bola.
///
enum POSSE_DE_BOLA : qint8
{
    BOLA_ALIADO, /**< Posse de bola do time aliado*/
    BOLA_EM_DISPUTA, /**< Indica que algum aliado e algum adversário estão
                           muito próximos da bola ou que a bola está distante
                           de todos*/
    BOLA_ADVERSARIO /**< Posse de bola do adversário*/
};

///
/// \brief Separação do campo em três regiões: defesa, meio e ataque
/// \see Auxiliar::regiaoCampoPonto Auxiliar::pontosRegiaoCampo
///
enum REGIAO_CAMPO : qint8
{
    REGIAO_CAMPO_DEFESA  = -1,
    REGIAO_CAMPO_MEIO    = 0,
    REGIAO_CAMPO_ATAQUE  = 1
};

// extern bool bControlarSimulacao;

extern QString strModeloForcaChute;

///
/// \brief Um Vetor de duas dimensões de inteiros de 16 bits (cada)
///
struct ROBOVector2D_int16
{
    qint16 x; /**< Coordenada x do vetor */
    qint16 y; /**< Coordenada y do vetor */

    ///
    /// \brief vector2D_int16
    ///
    ROBOVector2D_int16()
    {
        x = 0;
        y = 0;
    }

    ///
    /// \brief vector2D_int16
    /// \param _x - coordenada x do vetor
    /// \param _y - coordenada y do vetor
    ///
    ROBOVector2D_int16(const qint16 _x, const qint16 _y)
    {
        x = _x;
        y = _y;
    }

    ///
    /// \brief Módulo do vetor
    ///
    float length() const
    {
        return sqrtf(static_cast<float>(x*x + y*y));
    }


    ///
    /// \brief Distância a outro vetor
    /// \param point - outro vetor
    ///
    float distanceToPoint(const ROBOVector2D_int16 point) const
    {
        return ROBOVector2D_int16(point.x - x, point.y - y).length();
    }

};




#endif // CONSTANTES_H
