/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "hungarianalgorithm.h"

HungarianAlgorithm::HungarianAlgorithm(quint8 nRobots)
{
    n = nRobots;
    costMatrix = dlib::zeros_matrix<long>(n, n);

    solution.clear();
    dSolutionCost = MAX_VAL;
}

double HungarianAlgorithm::dInvertValue(double _value)
{
    return 1e5 * (1.0 / _value);
}

void HungarianAlgorithm::vSetMatrixValue(quint8 row, quint8 column, double value)
{
    if(n <= 0 || row >= n || column >= n)
    {
        return;
    }

    // Inverte o valor, pois queremos minimizar
    costMatrix(row, column) = dInvertValue(value);
}

QVector<Assignment> HungarianAlgorithm::runAlgorithm(double *_solutionCost)
{
    std::vector<long> assignment = max_cost_assignment(costMatrix);

    int task = 0;
    solution.clear();
    for(quint8 row = 0; row < n; ++row)
    {
        for(quint8 column = 0; column < n; ++column)
        {
            costMatrix(row, column) = qCeil(dInvertValue(costMatrix(row, column)));
        }
        task = assignment.at(row);
        solution.append(Assignment(row, task, costMatrix(row, task)));
    }

    dSolutionCost = assignment_cost(costMatrix, assignment);

    if(_solutionCost != nullptr)
    {
        *_solutionCost = dSolutionCost;
    }

    return solution;
}
