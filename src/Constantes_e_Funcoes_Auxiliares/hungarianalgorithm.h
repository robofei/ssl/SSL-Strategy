/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HUNGARIANALGORITHM_H
#define HUNGARIANALGORITHM_H

#include <QDebug>
#include <QSharedPointer>
#include <QVector2D>

#include "qglobal.h"
#include "qmath.h"

#include <dlib/matrix/matrix.h>
#include <dlib/matrix/matrix_utilities.h>
#include <dlib/optimization/max_cost_assignment.h>

#define MAX_VAL 99999999

class Assignment
{
public:
    int robot;
    int task;
    double cost;
    bool used;

    Assignment()
    {
        robot = -1;
        task  = -1;
        used  = false;
    }

    Assignment(int _robot, int _task, double _cost)
    {
        robot = _robot;
        task  = _task;
        cost  = _cost;
        used  = _cost <= 0 ? false : true;
    }
};

class HungarianAlgorithm
{
    quint8 n;
    QVector<Assignment> solution;
    double dSolutionCost;
    dlib::matrix<long> costMatrix;

    double dInvertValue(double _value);

public:
    HungarianAlgorithm(quint8 nRobots);

    void vSetMatrixValue(quint8 row, quint8 column, double value);

    QVector<Assignment> runAlgorithm(double *_solutionCost = nullptr);
};

#endif // HUNGARIANALGORITHM_H
