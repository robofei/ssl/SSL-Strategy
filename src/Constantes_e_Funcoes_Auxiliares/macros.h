/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MACROS_H
#define MACROS_H

///
/// \file macros.h
/// \brief Macros utilizados em todo o código estão definidos aqui
/// \details A vantangem de se utilizar macros é dada pelo fato de eles serem definidos durante
///  a compilação, e não durante a execução do programa. Note porém que para alterar alguma configuração
///  definida com um macro será necessário recompilar o código novamente.
///

#define iTempoLoopInterface     20 /**< Período de atualização da interface [ms]. */
#define iTempoLoopKalman         5 /**< Período de atualização do Filtro de Kalman [ms]. */
#define iTempoLoopEstrategia    25 /**< Periodo de atualização da estratégia [ms] */
#define iTempoLoopMovimentacao  10 /**< Período de atualização da movimentação/controle [ms] */

///
/// \brief Quando este macro for definido, a aplicação carregará com os recursos mais essenciais para a
/// realização de uma partida.
/// \see \a ROBO_TODOS_OS_RECURSOS
///
#define ROBO_APENAS_O_ESSENCIAL

#define ROBO_DIVISAO_A 0
#define ROBO_DIVISAO_B 1


///
/// \brief Quando este macro for definido, a aplicação carregará com todos os recursos disponíveis.
/// \see \a ROBO_APENAS_O_ESSENCIAL
///
#define ROBO_TODOS_OS_RECURSOS


#define ROBO_REGRAS_ROBOCUP      0 /**< Jogar com as regras da RoboCup */
#define ROBO_REGRAS_LARC         1 /**< Jogar com as regras da Larc */

//---------------------------- Definição de macros --------------------------------------
#define ROBO_REGRAS         ROBO_REGRAS_ROBOCUP /**< Define qual regra de jogo será utilizada,
                                                    #ROBO_REGRAS_LARC ou #ROBO_REGRAS_ROBOCUP **/
#define ROBO_DIVISAO        ROBO_DIVISAO_B

// Definir ROBO_TODOS_OS_RECURSOS quando estiver em debug mode
#ifdef QT_DEBUG
#undef ROBO_APENAS_O_ESSENCIAL
#else
#undef ROBO_TODOS_OS_RECURSOS
#endif
//---------------------------- Definição de macros --------------------------------------


//****************************************** Parametros Path-Planners *********************
#define bNaoConsideraBola    1     /**< Indica que a bola deve ser ignorada como obstáculo nos cálculos. */
#define bConsideraBola       0     /**< Indica que a bola *NÃO* deve ser ignorada como obstáculo nos cálculos. */

//****************************************** Parametros do Campo *****************
#define XPositivo         1 /**< Indica o lado positivo do campo. */
#define XNegativo        -1 /**< Indica o lado negativo do campo. */
#define CAMPO_REAL        1 /**< Indica o ambiente de jogo como sendo o campo real. */
#define GR_SIM            0 /**< Indica o ambiente de jogo como sendo o grSim. */
#define TesteMeioCampo    1 /**< Indica se deve utilizar somente metade do campo. */
#define CampoInteiro      0 /**< Indica se deve utilizar o campo inteiro. */
#define Testes            0 /**< Indica se estão sendo realizados testes. */
#define Jogo              1 /**< Indica se um jogo de verdade está em andamento. */

//****************************************** Parâmetros Jogada Pênalti *********************
#define miraNoLadoNegativoEChutaNoLadoPositivo 0 /**< Utilizado na jogada de pênalti.
    see \a clsJogadas::vRoboExtraBolaPenalty */
#define miraNoLadoPositivoEChutaNoLadoNegativo 1  /**< Utilizado na jogada de pênalti.
    see \a clsJogadas::vRoboExtraBolaPenalty */

//****************************************** Goleiro *********************
#define GOLEIRO_ALTERNATIVO 0
#define GOLEIRO_LEGACY 1

// Definição do macro
#define TIPO_GOLEIRO GOLEIRO_ALTERNATIVO/**< Indica qual função de goleiro será utilizada.
    see \a vGoleiroNormal*/

#endif // MACROS_H
