#include "debugbestpositionattack.h"

DebugBestPositionAttack::DebugBestPositionAttack()
{
    points.clear();
    setType(DebugData::BEST_POSITION_ATTACK);
}

void DebugBestPositionAttack::addPoints(const QVector<QVector2D>& _points)
{
    for (QVector2D point : _points)
    {
        points.append(point.toPointF());
    }
}

void DebugBestPositionAttack::draw(const QVector2D& _conversion,
                                   const float& _scaleX, const float& _scaleY,
                                   QPainter& _painter)
{
    QPen prevPen = _painter.pen();
    _painter.setPen(
        QPen(Qt::red, 5, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    for (QPointF point : points)
    {
        applyConversion(point, _conversion, _scaleX, _scaleY);
        _painter.drawPoint(point);
    }
    _painter.setPen(prevPen);
}
