#ifndef DEBUGBESTPOSITIONATTACK_H
#define DEBUGBESTPOSITIONATTACK_H

#include "debugdata.h"
#include "qvector.h"

class DebugBestPositionAttack : public DebugData
{
public:
    DebugBestPositionAttack();

    void addPoints(const QVector<QVector2D>& _points);

    void draw(const QVector2D& _conversion, const float& _scaleX,
              const float& _scaleY, QPainter& _painter) override;
};

#endif // DEBUGBESTPOSITIONATTACK_H
