#include "debugdata.h"

DebugData::DebugData()
{
    lifespan = 0;

    points.clear();
    lines.clear();
}

DebugData::DebugData(const DebugData* _other)
{
    setLifespan(_other->getLifespan());

    points.clear();
    lines.clear();

    points.append(_other->getPoints());
    lines.append(_other->getLines());

    setType(_other->getType());
}

void DebugData::applyConversion(QPointF& _point, const QVector2D& _conversion,
                     const float& _scaleX, const float& _scaleY)
{
    _point.setX((_point.x() + _conversion.x()) / _scaleX);
    _point.setY((-_point.y() + _conversion.y()) / _scaleY);
}

void DebugData::setType(DebugData::DEBUG_TYPE _type)
{
    if (_type >= SHOOT_DECISION && _type < DEBUG_TYPE_SIZE)
    {
        type = _type;
    }
    else
    {
        qWarning()
            << QString("[Debug Data] Tentando configurar um tipo inválido para "
                       "debug. _type = %1, Tipos válidos: %2 <= _type < %3. "
                       "Ver DEBUG_TYPE em debugdata.h")
                   .arg(_type)
                   .arg(DebugData::SHOOT_DECISION)
                   .arg(DebugData::DEBUG_TYPE_SIZE);
    }
}

DebugData::DEBUG_TYPE DebugData::getType() const
{
    return type;
}

QVector<QPointF> DebugData::getPoints() const
{
    return points;
}

QVector<QLineF> DebugData::getLines() const
{
    return lines;
}

qint64 DebugData::getLifespan() const
{
    return lifespan;
}

void DebugData::setLifespan(int _uses)
{
    lifespan = _uses;
}

bool DebugData::discardData() const
{
    return lifespan <= 0;
}

void DebugData::draw(const QVector2D& _conversion, const float& _scaleX,
                      const float& _scaleY, QPainter& _painter)
{
    Q_UNUSED(_conversion)
    Q_UNUSED(_scaleX)
    Q_UNUSED(_scaleY)
    Q_UNUSED(_painter)
    qWarning() << "Usando draw() não implementado";
}

void DebugData::decreaseLifespan()
{
    lifespan--;
}
