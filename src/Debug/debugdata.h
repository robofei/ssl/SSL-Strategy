#ifndef DEBUGDATA_H
#define DEBUGDATA_H

#include <QDateTime>
#include <QDebug>
#include <QLineF>
#include <QPainter>
#include <QPen>
#include <QPointF>
#include <QString>
#include <QVector2D>
#include <QVector>
#include <QtGlobal>

#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

class DebugData
{
protected:
    QVector<QPointF> points;
    QVector<QLineF> lines;
    void applyConversion(QPointF& _point, const QVector2D& _conversion,
                         const float& _scaleX, const float& _scaleY);

public:
    DebugData();
    DebugData(const DebugData* _other);
    virtual ~DebugData()
    {
        lifespan = 0;
        points.clear();
        lines.clear();
    };

    enum DEBUG_TYPE
    {
        SHOOT_DECISION = 0,
        BEST_POSITION_ATTACK,
        DEFLECT_PLAY,
        DEBUG_TYPE_SIZE
    };

    qint64 getLifespan() const;
    void setType(DebugData::DEBUG_TYPE _type);
    DEBUG_TYPE getType() const;
    QVector<QPointF> getPoints() const;
    QVector<QLineF> getLines() const;

    void setLifespan(int _uses);

    bool discardData() const;

    virtual void draw(const QVector2D& _conversion, const float& _scaleX,
                      const float& _scaleY, QPainter& _painter);
    void decreaseLifespan();

private:
    qint64 lifespan;
    DEBUG_TYPE type;
};

#endif // DEBUGDATA_H
