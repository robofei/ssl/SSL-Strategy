#include "debugdeflectplay.h"

DebugDeflectPlay::DebugDeflectPlay()
{
    points.clear();
    points.fill(QPointF(0, 0), 3);
    setType(DebugData::DEFLECT_PLAY);
}

void DebugDeflectPlay::draw(const QVector2D& _conversion, const float& _scaleX,
          const float& _scaleY, QPainter& _painter)
{
    QPen prevPen = _painter.pen();
    _painter.setPen(
        QPen(Qt::red, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

    _painter.setPen(prevPen);
}
