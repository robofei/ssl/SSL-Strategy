#ifndef DEBUGDEFLECTPLAY_H
#define DEBUGDEFLECTPLAY_H

#include "debugdata.h"

class DebugDeflectPlay : public DebugData
{
public:
    DebugDeflectPlay();

    void draw(const QVector2D& _conversion, const float& _scaleX,
              const float& _scaleY, QPainter& _painter) override;
};

#endif // DEBUGDEFLECTPLAY_H
