#include "debuginfo.h"

DebugInfo::DebugInfo()
{
    debugData.clear();
}

DebugInfo::DebugInfo(const DebugInfo& _other)
{
    debugData.clear();
    debugData.append(_other.getDebugData());
}

DebugInfo::~DebugInfo()
{
    debugData.clear();
}

void DebugInfo::addDebugData(QSharedPointer<DebugData> _data, qint64 _lifespan)
{
    _data->setLifespan(_lifespan);
    debugData.append(_data);
}

QVector<QSharedPointer<DebugData>> DebugInfo::getDebugData() const
{
    return debugData;
}

void DebugInfo::merge(DebugInfo& _other)
{
    auto dataVector = _other.getDebugData();

    for (const QSharedPointer<DebugData> &_data : qAsConst(dataVector))
    {
        debugData.append(_data);
    }

    _other.clearAll();
}

void DebugInfo::clearDeadData()
{
    QSharedPointer<DebugData> data;
    int i = 0;

    while (i < debugData.size())
    {
        data = debugData.at(i);
        if (data->discardData())
        {
            debugData.remove(i);
        }

        i++;
    }
}

void DebugInfo::clearAll()
{
    debugData.clear();
}
