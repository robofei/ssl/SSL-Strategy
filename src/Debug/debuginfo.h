#ifndef DEBUGINFO_H
#define DEBUGINFO_H

#include <QObject>
#include <QVector>
#include <QSharedPointer>
#include <QtGlobal>

#include "Debug/debugdata.h"

class DebugInfo
{
    QVector<QSharedPointer<DebugData>> debugData;

public:
    DebugInfo();
    DebugInfo(const DebugInfo& _other);
    ~DebugInfo();

    void addDebugData(QSharedPointer<DebugData> _data, qint64 _lifespan);
    QVector<QSharedPointer<DebugData>> getDebugData() const;

    void merge(DebugInfo &_other);
    void clearDeadData();
    void clearAll();
};

#endif // DEBUGINFO_H
