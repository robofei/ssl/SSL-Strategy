#include "debugshootdecision.h"

DebugShootDecision::DebugShootDecision() : DebugData()
{
    points.append({0, 0});
    points.append({0, 0});
    lines.append(QLineF(0, 0, 0, 0));

    setType(DebugData::SHOOT_DECISION);
}

void DebugShootDecision::updateLine()
{
    lines.replace(0, QLineF(points.constFirst(), points.constLast()));
}

void DebugShootDecision::setRobotPosition(QVector2D _pos)
{
    points.replace(0, _pos.toPointF());
    updateLine();
}

void DebugShootDecision::setRobotTarget(QVector2D _pos)
{
    points.replace(1, _pos.toPointF());
    updateLine();
}

void DebugShootDecision::draw(const QVector2D& _conversion,
                              const float& _scaleX, const float& _scaleY,
                              QPainter& _painter)
{
    QPen prevPen = _painter.pen();

    QPointF A = points.at(0), B = points.at(1);

    applyConversion(A, _conversion, _scaleX, _scaleY);
    applyConversion(B, _conversion, _scaleX, _scaleY);

    _painter.setPen(
        QPen(Qt::blue, 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    _painter.drawEllipse(A, globalConfig.robotDiameter / _scaleX,
                         globalConfig.robotDiameter / _scaleY);
    _painter.drawEllipse(B, globalConfig.robotDiameter / _scaleX,
                         globalConfig.robotDiameter / _scaleY);
    _painter.drawLine(A, B);

    _painter.setPen(prevPen);
}
