#ifndef DEBUGSHOOTDECISION_H
#define DEBUGSHOOTDECISION_H

#include "debugdata.h"
#include <QColor>

class DebugShootDecision : public DebugData
{
    void updateLine();

public:
    DebugShootDecision();

    void setRobotPosition(QVector2D _pos);
    void setRobotTarget(QVector2D _pos);

    void draw(const QVector2D& _conversion, const float& _scaleX,
              const float& _scaleY, QPainter& _painter) override;
};

#endif // DEBUGSHOOTDECISION_H
