/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "decisoes.h"

Decisoes* Decisoes::decisoesInstance = nullptr;

double Decisoes::dFatorControleAvaliacaoPasse = 1;
double Decisoes::dAnguloMinimoFatorRotacaoPasse = 60;
double Decisoes::dAnguloMaximoFatorRotacaoPasse = 400;


double Decisoes::dFatorControleAvaliacaoChute = 1;
double Decisoes::dAnguloMinimoFatorRotacaoChute = 60;
double Decisoes::dAnguloMaximoFatorRotacaoChute = 400;

bool Decisoes::bFatorRotacao = false;
QString Decisoes::strModeloChute = "AdaBoost";
QString Decisoes::strModeloPasse = "AdaBoost";


void Decisoes::resetInstance()
{
    delete Decisoes::decisoesInstance;
    decisoesInstance = new Decisoes();
}

void Decisoes::releaseInstance()
{
    delete Decisoes::decisoesInstance;
    Decisoes::decisoesInstance = nullptr;
}


bool Decisoes::good()
{
    return (Decisoes::decisoesInstance != nullptr);
}

void Decisoes::vRealizaPrevisoes(const QVector2D &vt2dPontoMiraRoboComBola,
                                 const QVector<QVector3D> &vt_vt3dPosicoesReceptores,
                                 const QVector2D &vt2dPosicaoBola,
                                 const QVector<QVector3D> &vt_vt3dPosicaoDeTodosadversarios,
                                 const QSize &szCampo,
                                 const qint8 &iLadoCampoDefesa,
                                 const qint16 &iLarguraGol,
                                 const QVector2D& vt2dCentroGolAdversario,
                                 const QVector<int>& vtiIDsIgnorados)
{
    QElapsedTimer elapsedTempoPrevisoes = QElapsedTimer();
    elapsedTempoPrevisoes.start();

    /// \todo Ver porque esse erro ocorre e o que podemos fazer para evitá-lo.
    /// Esse erro ocorre para pybind11 >= 2.7.0
    ///
    ///  Links relevantes:
    /// * https://pybind11.readthedocs.io/en/stable/advanced/exceptions.html
    /// * https://github.com/pybind/pybind11/commit/ad6bf5cd39ca64b4a9bf846b84b11c4c8df1c8e1
    /// * https://docs.python.org/3.8/c-api/init.html#c.PyGILState_Check
    /// * https://github.com/pybind/pybind11/pull/2919

    try
    {
        Decisoes::instance()->vAvaliaReceptoresPasse(vt2dPontoMiraRoboComBola,
                                                     vt_vt3dPosicoesReceptores,
                                                     vt2dPosicaoBola,
                                                     vt_vt3dPosicaoDeTodosadversarios,
                                                     szCampo,
                                                     iLadoCampoDefesa,
                                                     iLarguraGol,
                                                     vtiIDsIgnorados);

        Decisoes::instance()->vAvaliaChuteAoGol(vt2dPontoMiraRoboComBola,
                                                vt2dPosicaoBola,
                                                vt_vt3dPosicaoDeTodosadversarios,
                                                szCampo,
                                                iLadoCampoDefesa,
                                                iLarguraGol,
                                                vt2dCentroGolAdversario);

    }
    catch (std::runtime_error &error)
    {
        qCritical() << "Exception achada em `" << Q_FUNC_INFO << "`.\nwhat(): \'" << error.what() <<"\'.";
    }


    const double tempoTotal = elapsedTempoPrevisoes.nsecsElapsed()*1e-6;

    Decisoes::instance()->elapsedUltimaPrevisao.restart();

    if(tempoTotal > 30)
        qWarning() << "Tempo Previsões " << QString::number(tempoTotal, 'f', 2) << " ms";
}

QVector<qint16> Decisoes::vtiAvalicoesReceptores()
{
    return Decisoes::instance()->vtiAvaliacoesPasse;
}

qint16 Decisoes::iAvaliacaoChuteAoGol()
{
    return Decisoes::instance()->iAvalicaoChuteAoGol;
}

qint64 Decisoes::iTempoUltimaPrevisao()
{
    return Decisoes::instance()->elapsedUltimaPrevisao.nsecsElapsed();
}

void Decisoes::vPrintAvaliacoes()
{
    QString printMsg = QString();

    printMsg += "Avaliações\n";
    printMsg += ("-1: " + QString::number(Decisoes::instance()->iAvalicaoChuteAoGol) + '\n');
    for(qint8 i=0; i<Decisoes::instance()->vtiAvaliacoesPasse.size(); i++)
    {
        printMsg+= (QString::number(i) + ": " + QString::number(Decisoes::instance()->vtiAvaliacoesPasse[i]) + '\n');
    }

    qDebug() << qPrintable(printMsg);
}


Decisoes::Decisoes() :
    vtiAvaliacoesPasse( QVector<qint16>(PLAYERS_PER_SIDE, 0) ),
    iAvalicaoChuteAoGol( 0 ),
    elapsedUltimaPrevisao()
{
    {
        if(Py_IsInitialized() == 0)
        {
            qInfo() << "Inicializando o interpretador python na thread \""
                    << QThread::currentThread()->objectName() << "\".";
            py::initialize_interpreter(); // Inicia Python Interpreter
        }

        py::module pymoduleDecisoesJogadaPasse =
                py::module::import("robofeissl.decisoes_jogadas.decisoes_jogadas_passe");


        py::object avaliacaoPasse = pymoduleDecisoesJogadaPasse.attr("AvaliacaoJogadaPasse");


        // Ele está crashando depois dessa linha aqui depois de enviar um:
//        terminate called after throwing an instance of 'std::runtime_error'
//          what():  pybind11::object_api<>::operator() PyGILState_Check() failure.
        // Esse erro é jogado aqui:
// https://github.com/pybind/pybind11/blob/6de30d317277c26a72ed1929e09f225b74f23b7a/include/pybind11/cast.h#L1401


        // cria um novo objeto "AvaliacaoJogadaPasse" com o argumento selecionado
        if(Decisoes::strModeloPasse == "Linear Model")
            pyobjAvaliacaoPasse = avaliacaoPasse("linear_regression");
        else if(Decisoes::strModeloPasse == "AdaBoost")
            pyobjAvaliacaoPasse = avaliacaoPasse("ada");
        else if(Decisoes::strModeloPasse == "Gradient Boosting")
            pyobjAvaliacaoPasse = avaliacaoPasse("gradient");
        else if(Decisoes::strModeloPasse == "Random Forest")
            pyobjAvaliacaoPasse = avaliacaoPasse("random_forest");
        else if(Decisoes::strModeloPasse == "KNearestNeighbor")
            pyobjAvaliacaoPasse = avaliacaoPasse("knn");
        else
            qFatal("Modelo selecionado para a avaliação de passe é inválido");
    }

    {
        py::module pymoduleDecisoesJogadaChute =
                py::module::import("robofeissl.decisoes_jogadas.decisoes_jogadas_chute");

        py::object avaliacaoChute = pymoduleDecisoesJogadaChute.attr("AvaliacaoJogadaChute");

        // cria um novo objeto "AvaliacaoJogadaChute" com o argumento selecionado
        if(Decisoes::strModeloChute == "Linear Model")
            pyobjAvaliacaoChute = avaliacaoChute("linear_regression");
        else if(Decisoes::strModeloChute == "AdaBoost")
            pyobjAvaliacaoChute = avaliacaoChute("ada");
        else if(Decisoes::strModeloChute == "Gradient Boosting")
            pyobjAvaliacaoChute = avaliacaoChute("gradient");
        else if(Decisoes::strModeloChute == "Random Forest")
            pyobjAvaliacaoChute = avaliacaoChute("random_forest");
        else if(Decisoes::strModeloChute == "KNearestNeighbor")
            pyobjAvaliacaoChute = avaliacaoChute("knn");
        else
            qFatal("Modelo selecionado para a avaliação de chute é inválido");
    }

    elapsedUltimaPrevisao.start();
}

Decisoes::~Decisoes()
{
//    pyobjAvaliacaoPasse.release();
//    pyobjAvaliacaoChute.release();
}

Decisoes *Decisoes::instance()
{
    if(!Decisoes::good()){
        qWarning("Decisoes::instance() is nullptr. Call Decisoes::resetInstance()\n"
                 "before use any method of the class Decisoes");

        Decisoes::resetInstance();
    }
    return Decisoes::decisoesInstance;
}

void Decisoes::vAvaliaReceptoresPasse(const QVector2D &vt2dPontoMiraPassador,
                                      const QVector<QVector3D> &vt_vt3dPosicoesReceptores,
                                      const QVector2D &vt2dPosicaoBola,
                                      const QVector<QVector3D> &vt_vt3dPosicaoDeTodosadversarios,
                                      const QSize &szCampo,
                                      const qint8 &iLadoCampo,
                                      const qint16 &iLarguraGol,
                                      const QVector<int> &vtiIDsIgnorados)
{
    // atribui a avaliação de todas as jogadas com valor 0

    vtiAvaliacoesPasse = QVector<qint16>(vt_vt3dPosicoesReceptores.size(), 0);

    for (int id=0; id<vt_vt3dPosicoesReceptores.size(); id++)
    {
        if(vtiIDsIgnorados.contains(id)) // ignora
            continue;

        const QVector2D posicaoReceptor = vt_vt3dPosicoesReceptores.at(id).toVector2D();

        // realiza a avaliação
        py::object result =
                pyobjAvaliacaoPasse.attr("realiza_avaliacao")
                (
                    Decisoes::iEscalaVariavel(Auxiliar::dAnguloEmGrausLivreParaPasse(vt2dPosicaoBola,
                                                                                     posicaoReceptor,
                                                                                     vt_vt3dPosicaoDeTodosadversarios),
                                              0,
                                              45),

                    Decisoes::iEscalaVariavel(vt2dPosicaoBola.distanceToPoint(posicaoReceptor), 0,
                                              szCampo.width()/2),

                    Decisoes::iEscalaVariavel(Auxiliar::dLiberdadeDeMarcacao(posicaoReceptor,
                                                                             vt_vt3dPosicaoDeTodosadversarios),
                                              -szCampo.width()/2, 0),

                    Decisoes::iEscalaVariavel(Auxiliar::dAnguloDeRotacao(posicaoReceptor,
                                                                         vt2dPosicaoBola,
                                                                         QVector2D(-iLadoCampo*szCampo.width()/2, 0)),
                                              0, 180),

                    Decisoes::iEscalaVariavel(Auxiliar::dAnguloEmGrausLivreParaChute(posicaoReceptor,
                                                                                     iLadoCampo,
                                                                                     iLarguraGol,
                                                                                     szCampo,
                                                                                     vt_vt3dPosicaoDeTodosadversarios),
                                              0, 45),

                    Decisoes::iEscalaVariavel(posicaoReceptor.distanceToPoint(QVector2D(-iLadoCampo*szCampo.width()/2, 0)),
                                              0, szCampo.width()/2),

                    Decisoes::iEscalaVariavel(Auxiliar::dLiberdadeDeMarcacao(vt2dPosicaoBola,
                                                                             vt_vt3dPosicaoDeTodosadversarios),
                                              -szCampo.width()/2, 0),

                    Decisoes::iEscalaVariavel(Auxiliar::dDeltaXis(posicaoReceptor,
                                                                  vt2dPosicaoBola,
                                                                  iLadoCampo),
                                              -szCampo.width()/2, szCampo.width()/2));

        const double dFatorAnguloDeRotacao =
                Decisoes::bFatorRotacao ? Decisoes::dFatorRotacao(
                                              Auxiliar::dAnguloDeRotacao(vt2dPosicaoBola,
                                                                         vt2dPontoMiraPassador,
                                                                         posicaoReceptor),
                                              Decisoes::dAnguloMinimoFatorRotacaoPasse,
                                              Decisoes::dAnguloMaximoFatorRotacaoPasse
                                              )* Decisoes::dFatorControleAvaliacaoPasse
                                        :
                                          Decisoes::dFatorControleAvaliacaoPasse;
        /// \todo mais para frente arrumo isso aqui


        qint16 avaliacao = static_cast<qint16>(dFatorAnguloDeRotacao * result.cast<int>());


        Decisoes::vReEscala(avaliacao);
        vtiAvaliacoesPasse.replace(id, avaliacao);
    }
}

void Decisoes::vAvaliaChuteAoGol(const QVector2D &vt2dPontoMiraChutador,
                                 const QVector2D &vt2dPosicaoBola,
                                 const QVector<QVector3D> &vt_vt3dPosicaoDeTodosadversarios,
                                 const QSize &szCampo,
                                 const qint8 &iLadoCampo,
                                 const qint16 &iLarguraGol,
                                 const QVector2D &vt2dCentroGolAdversario)
{
    py::object result =
            pyobjAvaliacaoChute.attr("realiza_avaliacao")
            (
                Decisoes::iEscalaVariavel(Auxiliar::dAnguloEmGrausLivreParaChute(vt2dPosicaoBola,
                                                                                 iLadoCampo,
                                                                                 iLarguraGol,
                                                                                 szCampo,
                                                                                 vt_vt3dPosicaoDeTodosadversarios),
                                          0, 45),
                Decisoes::iEscalaVariavel(vt2dPosicaoBola.distanceToPoint(vt2dCentroGolAdversario),
                                          0,
                                          szCampo.width()/2),
                Decisoes::iEscalaVariavel(
                    Auxiliar::dLiberdadeDeMarcacao(vt2dPosicaoBola, vt_vt3dPosicaoDeTodosadversarios),
                    -szCampo.width()/2, 0));

    const double dFatorAnguloDeRotacao =
            Decisoes::bFatorRotacao ? Decisoes::dFatorRotacao(
                                          Auxiliar::dAnguloDeRotacao(vt2dPosicaoBola,
                                                                     vt2dPontoMiraChutador,
                                                                     vt2dCentroGolAdversario),
                                          Decisoes::dAnguloMinimoFatorRotacaoChute,
                                          Decisoes::dAnguloMaximoFatorRotacaoChute
                                          )* Decisoes::dFatorControleAvaliacaoChute
                                    :
                                      Decisoes::dFatorControleAvaliacaoChute;


//    qint16 avaliacao = dFatorAnguloDeRotacao * result.cast<qint16>();
    qint16 avaliacao = static_cast<qint16>(dFatorAnguloDeRotacao * result.cast<int>());


    Decisoes::vReEscala(avaliacao);

    iAvalicaoChuteAoGol = avaliacao;
}

quint16 Decisoes::iEscalaVariavel(const double& valorEntrada,
                                  const double& valorMinimoEntrada,
                                  const double& valorMaximoEntrada,
                                  const qint16& valorMinimoSaida,
                                  const qint16& valorMaximoSaida){


    if(valorEntrada < valorMinimoEntrada)
        return valorMinimoSaida;

    if(valorEntrada > valorMaximoEntrada)
        return valorMaximoSaida;

    return static_cast<qint16>( 250*(
                                    (valorEntrada - valorMinimoEntrada)
                                    /
                                    (valorMaximoEntrada - valorMinimoEntrada))
                                + valorMinimoSaida );
}

void Decisoes::vReEscala(qint16& valorEntrada,
                         const qint16& valorMinimo,
                         const qint16& valorMaximo)
{
    if(valorEntrada < valorMinimo)
        valorEntrada = valorMinimo;
    else if(valorEntrada > valorMaximo)
        valorEntrada = valorMaximo;
    // else "mantém o valor"
}

double Decisoes::dFatorRotacao(const double& angulo,
                               const double& anguloMinimo,
                               const double& anguloMaximo)
{
    if(angulo > anguloMaximo)
        return 0;

    if(angulo < angulo)
        return 1;

    return ( (anguloMaximo - angulo) / (anguloMaximo - anguloMinimo) );
}
