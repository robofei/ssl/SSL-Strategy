/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DECISOES_H
#define DECISOES_H

///
/// \file decisoes.h
/// \brief \a Decisoes
///

#include <QVector>
#include <QVector2D>
#include <QtGlobal>

#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

// O Qt define automaticamente todos os signals/slots em um arquivo _moc.cpp gerado na compilação,
// como aqui os slots e signals já estão definidos, adiciona-se esse comando para evitar que o Qt reinterprete
// os signals e slots já definidos.
#pragma push_macro("slots")
#undef slots
#include "pybind11/pybind11.h"
#include "pybind11/embed.h"
#pragma pop_macro("slots")


namespace py = pybind11;

///
/// \brief Esta classe contém metodos que permitem avaliar a decisão de jogada do robô com a bola.
/// \details A partir dos parâmetros fornecidos, cada opção de jogada é avaliada em uma escala de
/// 0 a 250. Essa classe é uma classe Singleton, o que significa exsite apenas uma única instância da classe.
/// Não é possível criar um objeto dessa classe, chame os métodos dessa classe da seguinte
/// forma: \a Decisoes::metodo() .
///
class Decisoes
{
public:

    ///
    /// \brief Deleta a instância atual e cria uma nova.
    /// \see \a Decisoes::releaseInstance , Decisoes::instance e Decisoes::decisoesInstance
    ///
    static void resetInstance();

    ///
    /// \brief Deleta a instância atual e atribui seu valor para nullptr.
    /// \see \a Decisoes::resetInstance , Decisoes::instance e Decisoes::decisoesInstance
    ///
    static void releaseInstance();

    ///
    /// \brief Utilizada para verificar a existência da classe.
    /// \return true se a instância da classe for diferente de nullptr.
    ///
    static bool good();

    ///
    /// \brief Realiza as previsões de passe e chute a partir dos valores de entrada.
    /// \param idRoboComBola - Id do robô com a bola
    /// \param vt2dPontoMiraRoboComBola - Ponto onde o robô está mirando (equivalente ao ângulo de rotação atual)
    /// \param vt_vt3dPosicoesReceptores - Vetor com a posição de todos os robôs do time aliado. \note Este vetor
    /// deve ter sua dimensão equivalente à variável #PLAYERS_PER_SIDE
    /// \param vt2dPosicaoBola - Posição da bola
    /// \param vt_vt3dPosicaoDeTodosadversarios - Vetor com a posição de todos os robôs do time adversário. \note Este vetor
    /// deve sua dimensão equivalente à variável #PLAYERS_PER_SIDE
    /// \param szCampo - Dimensões do campo
    /// \param iLadoCampoDefesa - Lado do campo
    /// \param iLarguraGol - Largura do gol
    /// \param vt2dCentroGolAdversario - Centro do gol adversário
    /// \param vtiIDsIgnorados - IDs îgnorados no cálculo, a nota desses robôs é sempre nula.
    ///
    static void vRealizaPrevisoes(const QVector2D& vt2dPontoMiraRoboComBola,
                                  const QVector<QVector3D>& vt_vt3dPosicoesReceptores,
                                  const QVector2D& vt2dPosicaoBola,
                                  const QVector<QVector3D>& vt_vt3dPosicaoDeTodosadversarios,
                                  const QSize &szCampo,
                                  const qint8& iLadoCampoDefesa,
                                  const qint16& iLarguraGol,
                                  const QVector2D &vt2dCentroGolAdversario,
                                  const QVector<int> &vtiIDsIgnorados = QVector<int>());

    ///
    /// \return Retorna o vetor Decisoes::vtiAvaliacoesPasse da instância da classe.
    /// \see \a Decisoes::iTempoUltimaPrevisao e Decisoes::vRealizaPrevisoes
    ///
    static QVector<qint16> vtiAvalicoesReceptores();

    ///
    /// \return Retorna o inteiro Decisoes::iAvalicaoChuteAoGol da instância da classe.
    /// \see \a Decisoes::iTempoUltimaPrevisao e Decisoes::vRealizaPrevisoes
    ///
    static qint16 iAvaliacaoChuteAoGol();

    ///
    /// \return Retorna o tempo em nanossegundos desde a última previsão da jogada de passe.
    /// \see \a Decisoes::elapsedUltimaPrevisao e Decisoes::vRealizaPrevisoes
    ///
    static qint64 iTempoUltimaPrevisao();

    static void vPrintAvaliacoes();
    ///
    /// \brief A nota final de avaliação da jogada de passe é sempre multiplicada por esse fator
    /// Útil para manter um bom equilíbrio entre a avaliação da jogada de passe e
    /// de chute
    ///
    static double dFatorControleAvaliacaoPasse;

    ///
    /// \brief Parâmetro ângulo mínimo no cálculo do fator de rotação na avaliação da jogada de passe
    /// \see \a Decisoes::dFatorRotacao
    ///
    static double dAnguloMinimoFatorRotacaoPasse;

    ///
    /// \brief Parâmetro ângulo máximo no cálculo do fator de rotação na avaliação da jogada de passe
    /// \see \a Decisoes::dFatorRotacao
    ///
    static double dAnguloMaximoFatorRotacaoPasse;

    ///
    /// \brief A nota final de avaliação da jogada de passe é sempre multiplicada por esse fator
    /// \see \a Decisoes::dFatorControleAvaliacaoPasse
    ///
    static double dFatorControleAvaliacaoChute;

    ///
    /// \brief Parâmetro ângulo mínimo no cálculo do fator de rotação na avaliação da jogada de chute
    /// \see \a Decisoes::dFatorRotacao
    ///
    static double dAnguloMinimoFatorRotacaoChute;

    ///
    /// \brief Parâmetro ângulo máximo no cálculo do fator de rotação na avaliação da jogada de chute
    /// \see \a Decisoes::dFatorRotacao
    ///
    static double dAnguloMaximoFatorRotacaoChute;

    ///
    /// \brief Indica se o fator de rotação deve ser utilizado no cálculo
    /// \see \a Decisoes::vRealizaAvaliacaoReceptoresPasse e \a Decisoes::vRealizaAvaliacaoChuteAoGol
    ///
    static bool bFatorRotacao;

    static QString strModeloChute;
    static QString strModeloPasse;

private:

    ///
    /// \brief Construtor privado da classe.
    ///
    Decisoes();

    ~Decisoes();

    ///
    /// \brief Método privado.
    /// \details É realizada uma verificação se a instância é nullptr; se for, a função \a Decisoes::resetInstance é
    /// chamda e então a instância é retornada. Esta função nunca retorna nullptr, prefira chamar os métodos privados da seguinte
    /// forma: \a Decisoes::instance()->privateMetodo , ao invés de \a Decisoes::decisoesInstance->privateMetodo .
    /// \return A instância da classe
    ///
    static Decisoes *instance();

    ///
    /// \brief Realiza as previsões da jogada de passe
    /// \details Realiza as previsões e altera o valor do vetor \a Decisoes::vtiAvaliacoesPasse ,
    /// no fim Decisoes::elapsedCalculoPasse é reiniciado
    /// \param idRoboPassador - ID do robô com a bola
    /// \param vt2dPontoMiraPassador - Ponto onde o robô está mirando (equivalente ao ângulo de rotação atual)
    /// \param vt_vt3dPosicoesReceptores Vetor com a posição de todos os robôs do time aliado. \note Este vetor
    /// deve sua dimensão equivalente à variável #PLAYERS_PER_SIDE
    /// \param vt2dPosicaoBola - Posição da bola
    /// \param vt_vt3dPosicaoDeTodosadversarios - Vetor com a posição de todos os robôs do time adversário. \note Este vetor
    /// deve sua dimensão equivalente à variável #PLAYERS_PER_SIDE
    /// \param szCampo - Vetor com as dimensões do campo
    /// \param vtiIDsIgnorados - IDs ignorados no cálculo, a nota desses robôs é sempre nula.
    ///
    void vAvaliaReceptoresPasse(const QVector2D& vt2dPontoMiraPassador,
                                          const QVector<QVector3D>& vt_vt3dPosicoesReceptores,
                                          const QVector2D& vt2dPosicaoBola,
                                          const QVector<QVector3D>& vt_vt3dPosicaoDeTodosadversarios,
                                          const QSize &szCampo,
                                          const qint8& iLadoCampo,
                                          const qint16& iLarguraGol,
                                          const QVector<int>& vtiIDsIgnorados = QVector<int>());

    ///
    /// \brief Realiza a previsão da jogada de chute ao gol
    /// \param vt2dPontoMiraChutador - Ponto onde o robô está mirando (equivalente ao ângulo de rotação atual)
    /// \param vt2dPosicaoBola - Posição da bola
    /// \param vt_vt3dPosicaoDeTodosadversarios - Vetor com a posição de todos os robôs do time adversário. \note Este vetor
    /// deve sua dimensão equivalente à variável #PLAYERS_PER_SIDE
    /// \param szCampo - Vetor com as dimensões do campo
    /// \param iLadoCampo - Lado de campo
    /// \param iLarguraGol - Dimensão da largura do gol
    /// \param vt2dCentroGolAdversario - Centro do gol adversário
    ///
    void vAvaliaChuteAoGol(const QVector2D& vt2dPontoMiraChutador,
                                     const QVector2D& vt2dPosicaoBola,
                                     const QVector<QVector3D>& vt_vt3dPosicaoDeTodosadversarios,
                                     const QSize &szCampo,
                                     const qint8& iLadoCampo,
                                     const qint16& iLarguraGol,
                                     const QVector2D& vt2dCentroGolAdversario);

    ///
    /// \brief Realiza a transformação de escala
    /// \param valorEntrada - double
    /// \param valorMinimoEntrada - double
    /// \param valorMaximoEntrada - double
    /// \param valorMinimoSaida - short
    /// \param valorMaximoSaida - short
    ///
    static quint16 iEscalaVariavel(const double& valorEntrada,
                                   const double& valorMinimoEntrada,
                                   const double& valorMaximoEntrada,
                                   const qint16& valorMinimoSaida = 0,
                                   const qint16& valorMaximoSaida = 250);

    ///
    /// \brief Ajusta o valor da variável de entrada
    /// \param valorEntrada - Passar como referência
    /// \param valorMinimo
    /// \param valorMaximo
    ///
    static void vReEscala(qint16& valorEntrada,
                          const qint16& valorMinimo = 0,
                          const qint16& valorMaximo = 250);

    ///
    /// \brief Realiza o cálcula do fator de rotação.
    /// \details Esse fator é utilizado para reduzir a nota da avaliação final.
    /// \param angulo - ângulo de rotação para o robo atingir seu alvo
    /// \param anguloMinimo
    /// \param anguloMaximo
    /// \return Fator de rotação
    ///
    static double dFatorRotacao(const double& angulo,
                                const double& anguloMinimo = 0,
                                const double& anguloMaximo = 180);

    ///
    /// \brief Única instância da classe.
    ///
    static Decisoes* decisoesInstance;

    ///
    /// \brief Vetor com a avaliação da jogada de passe
    /// \note Este vetor deve ter dimensão igual a #PLAYERS_PER_SIDE
    ///
    QVector<qint16> vtiAvaliacoesPasse;

    ///
    /// \brief Avaliação da jogada de chute a gol
    ///
    qint16 iAvalicaoChuteAoGol;

    ///
    /// \brief Tempo utilizado para saber o valor da última previsão
    ///
    QElapsedTimer elapsedUltimaPrevisao;

    ///
    /// \brief Objeto da classe AvaliacaoJogadaPasse (python) utilizado para avaliar a jogada de passe.
    ///
    py::object pyobjAvaliacaoPasse;

    ///
    /// \brief Objeto da classe AvaliacaoJogadaChute (python) utilizado para avaliar a jogada de chute ao gol.
    ///
    py::object pyobjAvaliacaoChute;

};

#endif // DECISOES_H
