#include "coach.h"

Coach::Coach(AmbienteCampo* _fieldEnv, QObject* parent)
    : QObject(parent)
{
    iID = 0;
    bInitialized = false;
    mode = Coach::MANUAL_COACH;

    fieldEnvironment = _fieldEnv;

    assignedPlays.clear();
    currentPlay = nullptr;
}

void Coach::vRun()
{
    // A princípio, não é para implementar nada aqui!!!
    return;
}

QVector<int> Coach::iAvailablePlays(QVector<double>& _scores)
{
    QVector<int> ids;
    _scores.clear();

    QHashIterator<int, Play*> playIterator(
        assignedPlays); // Cuidado!!! Iterator java style
    while (playIterator.hasNext())
    {
        playIterator.next();

        if (playIterator.value()->bCheckPreConditions() == true)
        {
            ids.append(playIterator.value()->id());
            _scores.append(playIterator.value()->dGetScore());
        }
    }
    return ids;
}

int Coach::iRoulleteSelection(const QVector<int>& _playIDs,
                              const QVector<double>& _scores)
{
    // Por enquanto deixa a decisão completamente aleatória
    return _playIDs.at(QRandomGenerator::global()->bounded(_playIDs.size()));

    int selectedPlay = _playIDs.at(0);
    QVector<double> accumulatedScores = _scores;

    for (int n = 1; n < accumulatedScores.size(); ++n)
    {
        accumulatedScores.replace(n, accumulatedScores.at(n - 1) +
                                         accumulatedScores.at(n));
    }
    double scoreSum = *accumulatedScores.constEnd();
    double probability =
        QRandomGenerator::global()->bounded(scoreSum); // [0, sum[

    // Seleciona a primeira Play cujo score acumulado for maior ou igual ao
    // valor aleatório gerado
    for (int n = 0; n < _playIDs.size(); ++n)
    {
        if (accumulatedScores.at(n) >= probability)
        {
            selectedPlay = _playIDs.at(n);
            break;
        }
    }

    return selectedPlay;
}

void Coach::vRunCoach()
{
    if (currentPlay != nullptr)
    {
        if (currentPlay->psGetPlayStatus() == PLAY_STATUS::FINISHED ||
            currentPlay->psGetPlayStatus() == PLAY_STATUS::ABORTED)
        {
            emit _vFinishedPlays(id());
        }
    }

    if (!bInitialized)
    {
        return;
    }

    if (mode == Coach::MANUAL_COACH)
    {
        vRun();
    }
    else if (currentPlay == nullptr ||
             currentPlay->psGetPlayStatus() != PLAY_STATUS::RUNNING)
    {
        //         qInfo() << QString("[Coach %1] Nenhuma Play sendo executada,
        //         selecionando uma nova...").arg(strName());

        QVector<double> playScores;
        QVector<int> availablePlays = iAvailablePlays(playScores);

        int selectedPlay = -1;
        if (availablePlays.size() > 1)
        {
            selectedPlay = iRoulleteSelection(availablePlays, playScores);
        }
        else if (availablePlays.size() == 1)
        {
            selectedPlay = availablePlays.at(0);
        }
        else
        {
            //             qCritical() << QString("[Coach %1] Nenhuma play tem
            //             condições de ser executada.").arg(strName());
        }

        if (selectedPlay != -1)
        {
            bSetPlay(selectedPlay);
        }
    }

    if (currentPlay != nullptr)
    {
        currentPlay->vRunPlay();
        fieldEnvironment->vSetRemainingPlayTime(currentPlay->iRemainingTime());
    }
}

int Coach::id() const
{
    return iID;
}

bool Coach::bIsInitialized() const
{
    return bInitialized;
}

void Coach::vSetMode(COACH_MODE _mode)
{
    qInfo() << QString("[Coach %1] Modo mudou %2 -> %3")
                   .arg(strName())
                   .arg(mode)
                   .arg(_mode);
    mode = _mode;
}

bool Coach::bAddPlay(Play* _play)
{
    bool ok = false;
    if (_play != nullptr)
    {
        bool contains = assignedPlays.contains(_play->id());
        if (!contains && _play->id() != -1)
        {
            assignedPlays.insert(_play->id(), _play);

            qInfo() << QString("[Coach %1] Play(%2 = %3) adicionada!")
                           .arg(strName())
                           .arg(_play->id())
                           .arg(_play->strName());
            ok = true;
        }
        else
        {
            qWarning() << QString("[Coach %1] Tentando adicionar Play inválida "
                                  "| Play(%2), Contains = %3")
                              .arg(strName())
                              .arg(_play->id())
                              .arg(contains);
        }
    }
    else
    {
        qWarning() << QString("[Coach %1] Tentando adicionar Play nula!")
                          .arg(strName());
    }
    return ok;
}

bool Coach::bSetPlay(int _playID)
{
    bool ok = false;
    if (assignedPlays.contains(_playID))
    {
        if (currentPlay == nullptr || currentPlay->id() != _playID ||
            currentPlay->psGetPlayStatus() == PLAY_STATUS::ABORTED ||
            currentPlay->psGetPlayStatus() == PLAY_STATUS::FINISHED)
        {
            if (mode == Coach::MANUAL_COACH && currentPlay != nullptr)
            {
                if (assignedPlays.value(_playID)->bCheckPreConditions() ==
                    false)
                {
                    qWarning() << QString("[Coach %1] Pré condições da Play "
                                          "(%2) não satisfeitas!")
                                      .arg(strName())
                                      .arg(currentPlay->strName());
                    return false;
                }
            }

            if (currentPlay != nullptr)
            {
                currentPlay->vReset();
            }
            currentPlay = assignedPlays.value(_playID);
            qInfo() << QString("[Coach %1] Mudou para a Play(%2)")
                           .arg(strName())
                           .arg(currentPlay->strName());
            ok = true;
        }

        if (!currentPlay->bIsInitialized())
        {
            currentPlay->vInitialize();
            ok = true;
        }
    }
    else
    {
        qWarning() << QString("[Coach %1] trying to set invalid Play ID(%1)")
                          .arg(_playID);
    }
    return ok;
}

void Coach::vReset()
{
    if (bInitialized)
    {
        qInfo() << QString("[Coach %1] Reset!").arg(strName());
        if (currentPlay != nullptr)
        {
            currentPlay->vReset();
            currentPlay = nullptr;
        }
    }

    bInitialized = false;
}
