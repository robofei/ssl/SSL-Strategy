#ifndef COACH_H
#define COACH_H

#include <QObject>
#include <QHash>
#include <QSharedPointer>
#include <QRandomGenerator>

#include "Ambiente/futbolenvironment.h"

#include "Estrategia/Skills/skill.h"
#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/tactic.h"
#include "Estrategia/Plays/play.h"

#include <numeric>
/**
 * @brief Classe genérica para a implementação de um coach
 *
 * O Coach é quem vai decidir qual/quais jogadas devem ser executadas dado o
 * estado atual de jogo
 *
 * OBS: Ao fazer herança múltiplca com QObject, o QObject deve ser o primeiro
 * Fonte: shorturl.at/rxR29
 */
class Coach : public QObject
{
    Q_OBJECT
    /**
     * @brief Play atribuídas ao coach.
     *
     * São todas as plays que o coach pode escolher para executar. @see
     * bAddPlay
     */
public:
    /**
     * @brief Define o modo de operação do Coach.
     */
    QHash<int, Play*> assignedPlays;
    enum COACH_MODE
    {
        MANUAL_COACH = 0, /**< As plays devem ser atribuídas (Coach::bSetPlay)
                            manualmente através do método Coach::vRun. */
        AUTOMATIC_COACH   /**< O Coach atribui automaticamente uma Play para
                            ser executada baseado nas pré-condições
                            (Play::bCheckPreConditions) e no score
                            (Play::dGetScore).*/
    };
    Q_ENUM(COACH_MODE);

protected:
    int iID; /**< Número do ID do coach. @note Deve ser único. */
    bool bInitialized; /**< Indica se o coach foi inicializado. */

    AmbienteCampo *fieldEnvironment; /**< Referência para o objeto com as
                                       informações do campo. */
    Play *currentPlay; /**< Play atual sendo executada. @note Somente uma play
                         pode ser executada por vez.*/
    /**
     * @brief Pode ser implementado para adicionar alguma lógica na hora de rodar
     * o coach.
     *
     * No modo COACH_MODE::MANUAL_COACH é aqui que vai a lógica para atribuir
     * as Plays.
     */
    virtual void vRun();

private:
    COACH_MODE mode;

    /**
     * @brief Retorna os IDs das Plays que, no momento atual, possuem suas
     * pré-condições satisfeitas. Também gera um vetor com os scores das Plays.
     *
     * @param _scores
     *
     * @return
     */
    QVector<int> iAvailablePlays(QVector<double> &_scores);

    /**
     * @brief Seleciona o ID de uma Play com base no método da roleta.
     *
     * @see https://en.wikipedia.org/wiki/Fitness_proportionate_selection
     *
     * @param _playIDs
     * @param _scores
     *
     * @return
     */
    int iRoulleteSelection(const QVector<int> &_playIDs,
                           const QVector<double> &_scores);

public:
    /**
     * @brief Construtor do coach.
     *
     * @param _fieldEnv - Ponteiro para o ambiente do campo.
     * @param parent
     */
    explicit Coach(AmbienteCampo *_fieldEnv, QObject *parent = nullptr);
    /**
     * @brief Destrutor do coach.
     */
    virtual ~Coach() { };

    /**
     * @brief Deve retornar o nome do coach.
     *
     * Este método é de implementação obrigatória.
     *
     * @return Nome do coach.
     */
    virtual QString strName() const = 0;
    /**
     * @brief Faz as inicializações necessárias para o coach funcionar.
     *
     * Este método é de implementação obrigatória.
     */
    virtual void vInitialize() = 0;
    /**
     * @brief Executa a lógica do coach. Este método é chamado na estratégia.
     */
    void vRunCoach();
    /**
     * @brief Retorna o iID do coach.
     *
     * @return
     */
    int id() const;
    /**
     * @brief Retorna se o coach já foi inicializado.
     *
     * @return
     */
    bool bIsInitialized() const;
    /**
     * @brief Define o modo de operação do coach.
     *
     * @see Coach::COACH_MODE.
     *
     * @param _mode
     */
    void vSetMode(COACH_MODE _mode);
    /**
     * @brief Adiciona uma Play para o coach poder usar. @see assignedPlays.
     *
     * Não podem ser adicionadas play repetidas (mesmo id) e nem nulas.
     *
     * @param _play - Play para ser adicionada.
     *
     * @return true se deu tudo certo, false caso contrário.
     */
    bool bAddPlay(Play *_play);
    /**
     * @brief Define uma Play para ser executada. @see currentPlay.
     *
     * @param _playID - ID da Play.
     *
     * @return true se deu tudo certo, false caso contrário.
     */
    bool bSetPlay(int _playID);
    /**
     * @brief Faz os resets necessários para que este coach deixe de estar
     * ativo.
     */
    void vReset();

signals:
    /**
     * @brief Sinal emitido caso a play atual tenha sido finalizada.
     *
     * @param _coachID - Emite o ID do coach.
     */
    void _vFinishedPlays(const int _coachID);
};

#endif // COACH_H
