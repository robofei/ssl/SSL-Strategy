#include "coachreferee.h"

CoachReferee::CoachReferee(AmbienteCampo* _fieldEnv) : Coach(_fieldEnv)
{
    iID = COACH_REFEREE;

    plays[PLAY_HALT].reset(new PlayHalt(_fieldEnv));
    plays[PLAY_STOPDEFENSIVE].reset(new PlayStopDefensivo(_fieldEnv));
    plays[PLAY_STOP].reset(new PlayStop(_fieldEnv));
    //    plays[PLAY_TIMEOUT].reset(new PlayTimeout(_fieldEnv));
    plays[PLAY_TIMEOUT].reset(new PlayHalt(_fieldEnv));
    plays[PLAY_KICKOFFALLY].reset(new PlayKickOffAlly(_fieldEnv));
    plays[PLAY_KICKOFFOPP].reset(new PlayKickOffOpp(_fieldEnv));
    plays[PLAY_KICKOFFALLYTAKE].reset(new PlayKickOffAllyTake(_fieldEnv));
    plays[PLAY_FREEKICKOPP].reset(new PlayFreeKickOpp(_fieldEnv));
    plays[PLAY_BASIC_FREEKICK].reset(new PlayBasicFreeKick(_fieldEnv));
    plays[PLAY_DEFLECT_KICK].reset(new PlayDeflectKick(_fieldEnv));
    plays[PLAY_BALLPLACEMENTOPP].reset(new PlayBallPlacementOpp(_fieldEnv));
    plays[PLAY_BALLPLACEMENT].reset(new PlayBallPlacement(_fieldEnv));

    // Necessário para usar métodos específicos da play
    playPenaltyDefense.reset(new PlayPenaltyDefense(_fieldEnv));
    plays[PLAY_PENALTY_DEFENSE] = playPenaltyDefense;

    playPenaltyAttack.reset(new PlayPenaltyDirectAttack(_fieldEnv));
    plays[PLAY_PENALTY_DIRECT_ATTACK] = playPenaltyAttack;

    for (const int k : plays.keys())
    {
        bAddPlay(plays[k].get());
    }

    currentState = STATES::HALT;
    bInPlayWarning = false;
    referee = nullptr;
}

CoachReferee::~CoachReferee()
{
}

void CoachReferee::vRun()
{
    if (referee == nullptr)
    {
        qCritical() << QString("[Coach %1] Referência para o Referee não "
                               "definida! (this->referee == nullptr)")
                           .arg(strName());
        return;
    }

    vUpdateState();

    // Setar as plays
    switch (currentState)
    {
    case STATES::HALT: {
        bSetPlay(plays[PLAY_HALT]->id());
    }
    break;

    case STATES::STOP: {
        if (fieldEnvironment->vt2dPosicaoBola().x() > 0)
        {
            bSetPlay(plays[PLAY_STOPDEFENSIVE]->id());
        }
        else
        {
            bSetPlay(plays[PLAY_STOPDEFENSIVE]->id());
        }
    }
    break;

    case STATES::IN_PLAY: {
        if (currentPlay != nullptr &&
            (currentPlay->psGetPlayStatus() == PLAY_STATUS::FINISHED ||
             currentPlay->psGetPlayStatus() == PLAY_STATUS::ABORTED) &&
            bInPlayWarning == false)
        {
            bInPlayWarning = true;
            qWarning() << QString("[Coach %1] Outro coach deveria estar sendo "
                                  "usado neste caso!")
                              .arg(strName());
        }
    }
    break;

    case STATES::ALLY_KICKOFF_PREPARE: {
        bSetPlay(plays[PLAY_KICKOFFALLY]->id());
    }
    break;

    case STATES::ALLY_KICKOFF_TAKE: {
        bSetPlay(plays[PLAY_KICKOFFALLYTAKE]->id());
    }
    break;

    case STATES::OPP_KICKOFF: {
        bSetPlay(plays[PLAY_KICKOFFOPP]->id());
    }
    break;

    case STATES::ALLY_FREE_KICK: {
        bSetPlay(plays[PLAY_BASIC_FREEKICK]->id());
        // bSetPlay(plays[PLAY_DEFLECT_KICK]->id());
    }
    break;

    case STATES::OPP_FREE_KICK: {
        bSetPlay(plays[PLAY_FREEKICKOPP]->id());
    }
    break;

    case STATES::ALLY_PENALTY: {
        bSetPlay(plays[PLAY_PENALTY_DIRECT_ATTACK]->id());
    }
    break;

    case STATES::OPP_PENALTY: {
        bSetPlay(plays[PLAY_PENALTY_DEFENSE]->id());
    }
    break;

    case STATES::ALLY_SHOOTOUT: {
        bSetPlay(plays[PLAY_PENALTY_DIRECT_ATTACK]->id());
    }
    break;

    case STATES::OPP_SHOOTOUT: {
        bSetPlay(plays[PLAY_PENALTY_DEFENSE]->id());
    }
    break;

    case STATES::ALLY_BALLPLACEMENT: {
        bSetPlay(plays[PLAY_BALLPLACEMENT]->id());
    }
    break;

    case STATES::OPP_BALLPLACEMENT: {
        bSetPlay(plays[PLAY_BALLPLACEMENTOPP]->id());
    }
    break;

    case STATES::TIMEOUT: {
        bSetPlay(plays[PLAY_TIMEOUT]->id());
    }
    break;
    }
}

QString CoachReferee::strStateName(STATES _state)
{
    return QVariant::fromValue(_state).toString();
}

QString CoachReferee::strName() const
{
    return QString("Referee");
}

void CoachReferee::vInitialize()
{
    QVector<qint8> ids;

    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        // A play checa automaticamente se o robô está em campo ou não
        // Ver: Play::iGetValidIDs()
        ids.append(id);
    }

    for (const auto k : plays.keys())
    {
        plays[k]->vSetAvailableIDs(ids);
    }

    bInitialized = true;
}

void CoachReferee::vSetReferee(Referee* _referee)
{
    referee = _referee;

    for (const auto k : plays.keys())
    {
        plays[k]->vSetReferee(referee);
    }
}

void CoachReferee::vUpdateState()
{
    const STATES prevState = currentState;

    if (referee->bInPlay())
    {
        currentState = STATES::IN_PLAY;
    }
    else
    {
        switch (referee->sslrefGetCommand())
        {
        case SSL_Referee_Command_HALT: {
            if (currentState != STATES::HALT)
            {
                currentState = STATES::HALT;
            }
        }
        break;

        case SSL_Referee_Command_STOP:
        case SSL_Referee_Command_GOAL_YELLOW:
        case SSL_Referee_Command_GOAL_BLUE: {
            if (currentState != STATES::STOP)
            {
                currentState = STATES::STOP;
            }
        }
        break;

        case SSL_Referee_Command_NORMAL_START: {
            // Quando vier o normal start nas cobranças de penalty e
            // kickoff oponente, nós devemos esperar o oponente mover a
            // bola
            if (currentState != STATES::IN_PLAY &&
                currentState != STATES::OPP_PENALTY &&
                currentState != STATES::ALLY_PENALTY &&
                currentState != STATES::OPP_KICKOFF &&
                currentState != STATES::ALLY_KICKOFF_PREPARE &&
                currentState != STATES::ALLY_KICKOFF_TAKE)
            {
                currentState = STATES::IN_PLAY;
            }
            else if (currentState == STATES::ALLY_KICKOFF_PREPARE)
            {
                currentState = STATES::ALLY_KICKOFF_TAKE;
            }

            if (currentState == STATES::OPP_PENALTY)
            {
                playPenaltyDefense->vSetNormalStart();
            }
            else if (currentState == STATES::ALLY_PENALTY)
            {
                playPenaltyAttack->vSetNormalStart();
            }
        }
        break;

        case SSL_Referee_Command_FORCE_START: {
            if (currentState != STATES::IN_PLAY)
            {
                currentState = STATES::IN_PLAY;
            }
        }
        break;

        case SSL_Referee_Command_PREPARE_KICKOFF_YELLOW: {
            vEvalTeamCommand(timeAmarelo, ALLY_KICKOFF_PREPARE, OPP_KICKOFF);
        }
        break;

        case SSL_Referee_Command_PREPARE_KICKOFF_BLUE: {
            vEvalTeamCommand(timeAzul, ALLY_KICKOFF_PREPARE, OPP_KICKOFF);
        }
        break;

        case SSL_Referee_Command_PREPARE_PENALTY_YELLOW: {
            vEvalTeamCommand(timeAmarelo, ALLY_PENALTY, OPP_PENALTY);
        }
        break;

        case SSL_Referee_Command_PREPARE_PENALTY_BLUE: {
            vEvalTeamCommand(timeAzul, ALLY_PENALTY, OPP_PENALTY);
        }
        break;

        case SSL_Referee_Command_DIRECT_FREE_YELLOW: {
            vEvalTeamCommand(timeAmarelo, ALLY_FREE_KICK, OPP_FREE_KICK);
        }
        break;

        case SSL_Referee_Command_DIRECT_FREE_BLUE: {
            vEvalTeamCommand(timeAzul, ALLY_FREE_KICK, OPP_FREE_KICK);
        }
        break;

        case SSL_Referee_Command_INDIRECT_FREE_YELLOW: {
            vEvalTeamCommand(timeAmarelo, ALLY_FREE_KICK, OPP_FREE_KICK);
        }
        break;

        case SSL_Referee_Command_INDIRECT_FREE_BLUE: {
            vEvalTeamCommand(timeAzul, ALLY_FREE_KICK, OPP_FREE_KICK);
        }
        break;

        case SSL_Referee_Command_TIMEOUT_YELLOW:
        case SSL_Referee_Command_TIMEOUT_BLUE: {
            if (currentState != STATES::TIMEOUT)
            {
                currentState = STATES::TIMEOUT;
            }
        }
        break;

        case SSL_Referee_Command_BALL_PLACEMENT_YELLOW: {
            vEvalTeamCommand(timeAmarelo, ALLY_BALLPLACEMENT,
                             OPP_BALLPLACEMENT);
        }
        break;

        case SSL_Referee_Command_BALL_PLACEMENT_BLUE: {
            vEvalTeamCommand(timeAzul, ALLY_BALLPLACEMENT, OPP_BALLPLACEMENT);
        }
        break;
        }
    }

    if (prevState != currentState)
    {
        bInPlayWarning = false;
        qInfo() << QString("[Coach %1] Estado mudou: %2 -> %3!")
                       .arg(strName(), strStateName(prevState),
                            strStateName(currentState));
    }
}

void CoachReferee::vEvalTeamCommand(TeamColor _cmdColor, STATES _allyState,
                                    STATES _oppState)
{
    if (fieldEnvironment->allies->teamColor() == _cmdColor &&
        currentState != _allyState)
    {
        currentState = _allyState;
    }
    else if (fieldEnvironment->opponents->teamColor() == _cmdColor &&
             currentState != _oppState)
    {
        currentState = _oppState;
    }
}

void CoachReferee::vReceiveBallInPlay_(const bool _inPlay)
{
    if (_inPlay)
    {
        qInfo() << QString("[Coach %1] Estado mudou: %2 -> %3!")
                       .arg(strName(), strStateName(currentState),
                            strStateName(STATES::IN_PLAY));
        currentState = STATES::IN_PLAY;
    }
}
