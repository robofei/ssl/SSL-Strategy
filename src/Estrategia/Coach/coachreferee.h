#ifndef COACHREFEREE_H
#define COACHREFEREE_H

#include "coach.h"
#include "Estrategia/Plays/availableplays.h"

#include "Referee/referee.h"

/**
 * @brief Enum com o ID do coach.
 *
 * Deve aumentar de 20 em 20 de coach para coach.
 */
enum
{
    COACH_REFEREE = 20
};

/**
 * @brief Implementação do coach para executar os comandos do referee.
 */
class CoachReferee : public Coach
{
    // Necessário para poder enviar sinais e ter slots
    Q_OBJECT


public:
    /**
     * @brief Estados internos do referee.
     *
     * @note Precisa ser público para usar o Q_ENUM.
     */
    enum STATES
    {
        HALT,
        STOP,
        IN_PLAY,
        ALLY_KICKOFF_PREPARE,
        ALLY_KICKOFF_TAKE,
        OPP_KICKOFF,
        ALLY_FREE_KICK,
        OPP_FREE_KICK,
        ALLY_PENALTY,
        OPP_PENALTY,
        ALLY_SHOOTOUT,
        OPP_SHOOTOUT,
        ALLY_BALLPLACEMENT,
        OPP_BALLPLACEMENT,
        TIMEOUT,
    };
    Q_ENUM(STATES) // Para conseguir mostrar os estados em formato de texto

private:
    QSharedPointer<PlayPenaltyDefense> playPenaltyDefense;
    QSharedPointer<PlayPenaltyDirectAttack> playPenaltyAttack;

    QMap<int, QSharedPointer<Play>> plays;

    /**
     * @brief Implementa a lógica para executar as ações do coach.
     */
    void vRun() override;
    Referee *referee; /**< Objeto do referee que processa os comandos
                        recebidos.*/

    /**
     * @brief Retorna o nome do estado fornecido.
     *
     * @param _state
     *
     * @return
     */
    QString strStateName(STATES _state);

    /**
     * @brief Armazena qual o estado atual.
     */
    STATES currentState;
    bool bInPlayWarning;

public:
    /**
     * @brief Construtor.
     *
     * @param _fieldEnv - Ponteiro para o ambiente do campo.
     */
    CoachReferee(AmbienteCampo *_fieldEnv);
    /**
     * @brief Destrutor.
     */
    ~CoachReferee();

    /**
     * @brief Retorna o nome do coach.
     *
     * @return
     */
    QString strName() const override;
    /**
     * @brief Faz a inicialização necessária para o coach.
     */
    void vInitialize() override;
    /**
     * @brief Define o objeto do referee.
     *
     * @param _referee
     */
    void vSetReferee(Referee *_referee);
    /**
     * @brief Atualiza o estado atual do coach.
     */
    void vUpdateState();
    /**
     * @brief Dado um comando, define se vamos para um estado de ataque ou
     * defesa.
     *
     * Exemplo: Ao receber o comando PREPARE_KICKOFF_YELLOW, se nosso time for
     * o amarelo, estamos no ataque e vamos para o estado ALLY_KICKOFF, caso
     * contrário vamos para o estado OPP_KICKOFF.
     *
     * @param _cmdColor - Comando.
     * @param _allyState - Estado caso o comando seja para o nosso time.
     * @param _oppState - Estado caso o comando seja para o time oponente.
     */
    void vEvalTeamCommand(TeamColor _cmdColor, STATES _allyState,
                          STATES _oppState);

public slots:
    /**
     * @brief Recebe do Referee o sinal de que a bola está em jogo.
     *
     * @param _inPlay - Indica se a bola está em jogo.
     */
    void vReceiveBallInPlay_(const bool _inPlay);
};

#endif // COACHREFEREE_H
