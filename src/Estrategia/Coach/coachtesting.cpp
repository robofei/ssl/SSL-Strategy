#include "coachtesting.h"
#include "qvector.h"

CoachTesting::CoachTesting(AmbienteCampo* _fieldEnv) : Coach(_fieldEnv)
{
    iID = COACH_TESTING;

    //     playTesting.reset(new PlayTesting2(_fieldEnv));
    playTesting.reset(new PlayTesting(_fieldEnv));
    playDefense.reset(new PlayUsingPassAttack(_fieldEnv));
    playPressureDefense.reset(new PlayPressureDefense(_fieldEnv));

    playGetBallAway.reset(new PlayGetBallAway(_fieldEnv));
    playAdvanceBall.reset(new PlayAdvanceBall(_fieldEnv));
    playShootToGoal.reset(new PlayShootToGoal(_fieldEnv));

    // bAddPlay(playTesting.get());
    // bAddPlay(playDefense.get());
    // bAddPlay(playPressureDefense.get());
    bAddPlay(playGetBallAway.get());
    bAddPlay(playAdvanceBall.get());
    bAddPlay(playShootToGoal.get());
}

CoachTesting::~CoachTesting()
{
}

void CoachTesting::vRun()
{
    if (currentPlay == nullptr)
    {
        // bSetPlay(playDefense->id());
        bSetPlay(playGetBallAway->id());
    }
    else if ((playGetBallAway->psGetPlayStatus() == PLAY_STATUS::FINISHED || playGetBallAway->psGetPlayStatus() == PLAY_STATUS::STOPPED) && playAdvanceBall->bCheckPreConditions())
    {
        bSetPlay(playAdvanceBall->id());
    }
    else if ((playGetBallAway->psGetPlayStatus() == PLAY_STATUS::FINISHED || playGetBallAway->psGetPlayStatus() == PLAY_STATUS::STOPPED) &&
        (playAdvanceBall->psGetPlayStatus() == PLAY_STATUS::FINISHED || playAdvanceBall->psGetPlayStatus() == PLAY_STATUS::STOPPED) && playShootToGoal->bCheckPreConditions())
    {
        bSetPlay(playShootToGoal->id());
    }

    // else
    // {
    //     if (playDefense->bCheckEndConditions())
    //     {
    //         // playDefense->vInitialize();
    //         // playDefense.reset(new PlayUsingPassAttack(fieldEnvironment));
    //         // assignedPlays.clear();
    //         // bAddPlay(playDefense.get());
    //         if (playPressureDefense->bCheckPreConditions())
    //         {
    //             bSetPlay(playPressureDefense->id());
    //         }
    //         else
    //         {
    //             bSetPlay(playDefense->id());
    //         }
    //     }
    //
    //     if (playPressureDefense->bCheckEndConditions())
    //     {
    //         bSetPlay(playDefense->id());
    //     }
    // }
}

QString CoachTesting::strName() const
{
    return QString("Testing");
}

void CoachTesting::vInitialize()
{
    QVector<qint8> ids;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        // A play checa automaticamente se o robô está em campo ou não
        // Ver: Play::iGetValidIDs()
        ids.append(id);
    }

    playDefense->vSetAvailableIDs(ids);
    playPressureDefense->vSetAvailableIDs(ids);
    playGetBallAway->vSetAvailableIDs(ids);
    playAdvanceBall->vSetAvailableIDs(ids);
    playShootToGoal->vSetAvailableIDs(ids);

    bInitialized = true;
}
