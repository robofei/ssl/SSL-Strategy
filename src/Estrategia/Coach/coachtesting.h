#ifndef COACHTESTING_H
#define COACHTESTING_H

#include "coach.h"
#include "Estrategia/Plays/availableplays.h"

/**
 * @brief Enum com o ID do coach.
 *
 * Deve aumentar de 20 em 20 de coach para coach.
 */
enum
{
    COACH_TESTING = 0
};

/**
 * @brief Implementação de um coach de teste
 */
class CoachTesting : public Coach
{
    // Necessário para poder enviar sinais e ter slots
    Q_OBJECT

private:
//     QSharedPointer<PlayTesting2> playTesting;
    QSharedPointer<PlayTesting> playTesting; /**< Definição de uma Play que o coach pode executar. */
    QSharedPointer<PlayUsingPassAttack> playDefense;
    QSharedPointer<PlayPressureDefense> playPressureDefense;

    QSharedPointer<PlayGetBallAway> playGetBallAway;
    QSharedPointer<PlayAdvanceBall> playAdvanceBall;
    QSharedPointer<PlayShootToGoal> playShootToGoal;
    /**
     * @brief Implementa a lógica para executar as ações do coach.
     */
    void vRun() override;

public:
    /**
     * @brief Construtor.
     *
     * @param _fieldEnv - Ponteiro para o ambiente do campo.
     */
    CoachTesting(AmbienteCampo *_fieldEnv);
    /**
     * @brief Destrutor.
     */
    ~CoachTesting();

    /**
     * @brief Retorna o nome do coach.
     *
     * @return
     */
    QString strName() const override;
    /**
     * @brief Faz a inicialização necessária para o coach.
     */
    void vInitialize() override;
};

#endif // COACHTESTING_H
