#include "playadvanceball.h"
double PlayAdvanceBall::dScore = 0.0;

PlayAdvanceBall::PlayAdvanceBall(AmbienteCampo* _fieldEnv) : Play{_fieldEnv}
{
    iID = PLAY_ADVANCE_BALL;

    // Inicializa as roles
    roleGoalie.reset(new RoleGoalie(_fieldEnv));
    roleAttacker.reset(new RoleAttacker(_fieldEnv));
    roleReceiver.reset(new RoleReceiver(_fieldEnv));

    roleAttacker->vSetPriority(Role::MAX_PRIORITY);
    roleReceiver->vSetPriority(Role::MEDIUM_PRIORITY);

    bAddRole(roleGoalie.get());
    bAddRole(roleAttacker.get());
    bAddRole(roleReceiver.get());

    // ==================== Comunicação entre os robôs ==================
    // Fala para o suporte quem vai receber o passe
    connect(roleReceiver.get(), &RoleReceiver::_vRoleAssigned,
            roleAttacker.get(), &RoleAttacker::vUpdateReceiver_);
    // Fala para o atacante onde ele vai receber o passe
    connect(roleAttacker.get(), &RoleAttacker::_vSendPassPoint,
            roleReceiver.get(), &RoleReceiver::vSetPassPoint_);
}

PlayAdvanceBall::~PlayAdvanceBall()
{
}

void PlayAdvanceBall::vRun()
{
}

QString PlayAdvanceBall::strName() const
{
    return QString("Advance Ball");
}

void PlayAdvanceBall::vInitialize()
{
    roleAttacker->zone.vConfigZone(
        fieldEnvironment->vt2dPosicaoBola().toPoint(), 500);

    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const int allySide = fieldEnvironment->allies->iGetSide();
    QSize fieldSz = fieldEnvironment->geoCampo->szField();
    QSize zoneSize(safeX + attackX, -fieldSz.height());
    QPoint topLeft(safeX * allySide, fieldSz.height() / 2);

    roleReceiver->zone.vConfigZone(QRect(topLeft, zoneSize));
    roleGoalie->zone.vConfigZone(
        fieldEnvironment->geoCampo->retRestriction(opponentSide));
    bInitialized = true;
}

bool PlayAdvanceBall::bCheckPreConditions()
{
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const int allySide = fieldEnvironment->allies->iGetSide();
    float ballX = fabs(fieldEnvironment->vt2dPosicaoBola().x());
    int ballSide = Auxiliar::iSinal(ballX);

    if (iGetValidIDs().size() >= 1)
    {
        if ((ballX > safeX && ballSide == allySide) || (ballSide == opponentSide && ballX < attackX))
        {
            return true;
        }
    }
    else
    {
        qInfo() << QString("[Play %1] No mínimo 2 robôs são necessários!")
                       .arg(strName());
    }

    return false;
}

bool PlayAdvanceBall::bCheckEndConditions()
{
    float ballX = fabs(fieldEnvironment->vt2dPosicaoBola().x());
    int ballSide = Auxiliar::iSinal(ballX);
    if (ballX > attackX && ballSide == fieldEnvironment->opponents->iGetSide())
    {
        qInfo() << QString("[Play %1] Bola chegou na area de finalização!")
                       .arg(strName());
        playStatus = FINISHED;
    }

    if (playStatus == FINISHED || playStatus == ABORTED)
    {
        return true;
    }
    return false;
}

double PlayAdvanceBall::dGetScore() const
{
    return PlayAdvanceBall::dScore;
}

void PlayAdvanceBall::vUpdateScore()
{
    PlayAdvanceBall::dScore += 1.0;
}

void PlayAdvanceBall::vSaveScore()
{
}
