#ifndef PLAYADVANCEBALL_H
#define PLAYADVANCEBALL_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"

enum
{
    PLAY_ADVANCE_BALL =
        500 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayAdvanceBall : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleGoalie> roleGoalie;
    QSharedPointer<RoleAttacker> roleAttacker;
    QSharedPointer<RoleReceiver> roleReceiver;

    static double dScore;
    static constexpr int attackX = 2000;
    static constexpr int safeX = 1500;

    void vRun() override;

public:
    PlayAdvanceBall(AmbienteCampo* _fieldEnv);
    ~PlayAdvanceBall();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYADVANCEBALL_H
