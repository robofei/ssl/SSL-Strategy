#include "playdeflectkick.h"

double PlayDeflectKick::dScore = 0.0;

PlayDeflectKick::PlayDeflectKick(AmbienteCampo* _fieldEnv)
    : Play(_fieldEnv), playConfig("PlayDeflect")
{
    iID = PLAY_DEFLECT_KICK;

    playConfig.read("play_deflect_config.json");

    // Inicializa as roles
    roleGoalie.reset(new RoleGoalie(_fieldEnv));
    roleSupport.reset(new RoleSupportDeflecter(_fieldEnv));
    roleDeflecter.reset(new RoleDeflecter(_fieldEnv));

    // Comunicação entre as roles
    connect(roleSupport.get(), &RoleSupportDeflecter::_vRoleAssigned,
            roleDeflecter.get(), &RoleDeflecter::vReceiveSupportID_);
    connect(roleDeflecter.get(), &RoleSupportDeflecter::_vRoleAssigned,
            roleSupport.get(), &RoleSupportDeflecter::vReceiveDeflecterID_);
    connect(roleSupport.get(), &RoleSupportDeflecter::_vSendProceedDeflect,
            roleDeflecter.get(), &RoleDeflecter::vReceiveProceedDeflect_);

    bAddRole(roleGoalie.get());
    bAddRole(roleSupport.get());
    bAddRole(roleDeflecter.get());

    roleSupport->vSetPriority(Role::MAX_PRIORITY);
    roleDeflecter->vSetPriority(Role::MEDIUM_PRIORITY);

    tmrPlayTimeout->setInterval(15 * 1000);
}

PlayDeflectKick::~PlayDeflectKick()
{
}

QVector2D PlayDeflectKick::findDeflectPosition()
{
    int oppSide = fieldEnvironment->opponents->iGetSide(),
        allySide = fieldEnvironment->allies->iGetSide();
    float alpha, bestAlpha = 90, radius = 2.7e3;

    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();

    int fieldHeight = fieldEnvironment->geoCampo->szField().height() / 2.f,
        goalHeight = fieldEnvironment->geoCampo->szGoal().height() / 2.f,
        sign = Auxiliar::iSinal(ballPos.y());

    QVector2D goalAim = fieldEnvironment->geoCampo->vt2dGoalCenter(oppSide) +
                        QVector2D(0, goalHeight * sign),
              testPos = ballPos + QVector2D(radius, 0), bestPos,
              goalLineA = fieldEnvironment->geoCampo->vt2dGoalCenter(allySide) +
                          QVector2D(0, fieldHeight),
              goalLineB = fieldEnvironment->geoCampo->vt2dGoalCenter(allySide) +
                          QVector2D(0, -fieldHeight),
              dummy;

    bool safePass = false, possiblePass = false, insideField = false,
         notRestricted = false;

    for (int angleStep = 0; angleStep < 360; angleStep += 5)
    {
        testPos = Auxiliar::vt2dRotaciona(ballPos, testPos, angleStep);
        insideField = fieldEnvironment->geoCampo->bIsInsideField(testPos);

        if (insideField)
        {
            possiblePass = qAbs(qAbs(testPos.y()) - qAbs(ballPos.y())) > 200;
            if (possiblePass)
            {
                notRestricted =
                    !fieldEnvironment->geoCampo->bPontoDentroAreaRestricao(
                        testPos.toPoint(), XNegativo) ||
                    !fieldEnvironment->geoCampo->bPontoDentroAreaRestricao(
                        testPos.toPoint(), XPositivo);
                if (notRestricted)
                {
                    safePass = !Auxiliar::bChecaInterseccaoLinhaLinha(
                        ballPos, testPos, goalLineA, goalLineB, dummy);
                }
            }
        }

        if (safePass && possiblePass && insideField && notRestricted)
        {
            alpha = Auxiliar::dAnguloVetor1Vetor2(ballPos - testPos,
                                                  goalAim - testPos);

            if (alpha <= bestAlpha && alpha > 30)
            {
                bestPos = testPos;
                bestAlpha = alpha;
            }
        }
    }
    return bestPos;
}

QString PlayDeflectKick::strName() const
{
    return QString("Deflect Kick");
}

void PlayDeflectKick::vInitialize()
{
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const int allySide = fieldEnvironment->allies->iGetSide();
    const QVector2D ball = fieldEnvironment->vt2dPosicaoBola();
    const int sign = -opponentSide * Auxiliar::iSinal(ball.y());

    QVector2D deflectPoint;
    // QVector2D deflectPoint =
    //     fieldEnvironment->geoCampo->vt2dGoalCenter(opponentSide);
    // deflectPoint = (deflectPoint - ball).normalized() * 2.5e3 + ball;
    //
    // float deflectPosAngle = playConfig.deflectPositionAngle;
    // deflectPoint = Auxiliar::vt2dRotaciona(
    // ball, deflectPoint, sign * (180 - deflectPosAngle) / 2.0);

    roleGoalie->zone.vConfigZone(
        fieldEnvironment->geoCampo->retRestriction(allySide));

    roleSupport->zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(),
                                  500);

    deflectPoint = findDeflectPosition();
    roleDeflecter->zone.vConfigZone(deflectPoint.toPoint(), 500);

    bInitialized = true;
}

bool PlayDeflectKick::bCheckPreConditions()
{
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const QVector2D goalCenter =
        fieldEnvironment->geoCampo->vt2dGoalCenter(opponentSide);

    const bool robotQuantity = iGetValidIDs().size() >= 2;

    const bool ballInOpponentSide =
        Auxiliar::iSinal(fieldEnvironment->vt2dPosicaoBola().x()) ==
        opponentSide;
    const bool ourBall =
        true; // fieldEnvironment->pdbAchaTimeComPosseDeBola() == BOLA_ALIADO;
    const bool closeToGoal =
        goalCenter.distanceToPoint(fieldEnvironment->vt2dPosicaoBola()) <= 15e3;

    const bool canDeflect = ballInOpponentSide & ourBall & closeToGoal;

    return robotQuantity & canDeflect;
}

bool PlayDeflectKick::bCheckEndConditions()
{
    if (playStatus == FINISHED)
    {
        return true;
    }
    return false;
}

double PlayDeflectKick::dGetScore() const
{
    return PlayDeflectKick::dScore;
}

void PlayDeflectKick::vUpdateScore()
{
    PlayDeflectKick::dScore += 1.0;
}

void PlayDeflectKick::vSaveScore()
{
}
