#ifndef PLAYDEFLECTKICK_H
#define PLAYDEFLECTKICK_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"

#include "Config/playdeflectconfig.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

enum
{
    PLAY_DEFLECT_KICK = 400 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayDeflectKick : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleGoalie> roleGoalie;
    QSharedPointer<RoleSupportDeflecter> roleSupport;
    QSharedPointer<RoleDeflecter> roleDeflecter;

    PlayDeflectConfig playConfig;

    static double dScore;

    QVector2D findDeflectPosition();

public:
    PlayDeflectKick(AmbienteCampo *_fieldEnv);
    ~PlayDeflectKick();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYDEFLECTKICK_H
