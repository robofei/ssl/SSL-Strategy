#include "playgetballaway.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
double PlayGetBallAway::dScore = 0.0;

PlayGetBallAway::PlayGetBallAway(AmbienteCampo* _fieldEnv) : Play{_fieldEnv}
{
    iID = PLAY_GET_BALL_AWAY;

    // Inicializa as roles
    roleGoalie.reset(new RoleGoalie(_fieldEnv));
    roleAttacker.reset(new RoleAttacker(_fieldEnv));
    roleReceiver.reset(new RoleReceiver(_fieldEnv));
    
    roleAttacker->vSetPriority(Role::MAX_PRIORITY);
    roleReceiver->vSetPriority(Role::MEDIUM_PRIORITY);

    bAddRole(roleGoalie.get());
    bAddRole(roleAttacker.get());
    bAddRole(roleReceiver.get());

    // ==================== Comunicação entre os robôs ==================
    // Fala para o suporte quem vai receber o passe
    connect(roleReceiver.get(), &RoleReceiver::_vRoleAssigned,
            roleAttacker.get(), &RoleAttacker::vUpdateReceiver_);
    // Fala para o atacante onde ele vai receber o passe
    connect(roleAttacker.get(), &RoleAttacker::_vSendPassPoint,
            roleReceiver.get(), &RoleReceiver::vSetPassPoint_);
}

PlayGetBallAway::~PlayGetBallAway()
{
}

void PlayGetBallAway::vRun()
{

}

QString PlayGetBallAway::strName() const
{
    return QString("Get Ball Away");
}

void PlayGetBallAway::vInitialize()
{
    roleAttacker->zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(),
                                   500);

    const int allySide = fieldEnvironment->allies->iGetSide();
    QSize fieldSz = fieldEnvironment->geoCampo->szField();
    QSize zoneSize(1e3, -fieldSz.height());
    QPoint topLeft(safeX * allySide - 1e3 * allySide, fieldSz.height()/2);

    roleReceiver->zone.vConfigZone(QRect(topLeft, zoneSize));
    roleGoalie->zone.vConfigZone(
        fieldEnvironment->geoCampo->retRestriction(allySide));
    bInitialized = true;
}

bool PlayGetBallAway::bCheckPreConditions()
{
    const int allySide = fieldEnvironment->allies->iGetSide();
    float ballX = fabs(fieldEnvironment->vt2dPosicaoBola().x());

    if(iGetValidIDs().size() >= 1)
    {
        if (ballX > safeX && Auxiliar::iSinal(ballX) == allySide)
        {
            return true;
        }
    }
    else
    {
        qInfo() << QString("[Play %1] No mínimo 2 robôs são necessários!")
            .arg(strName());
    }

    return false;
}

bool PlayGetBallAway::bCheckEndConditions()
{
    if(playStatus == FINISHED || playStatus == ABORTED)
    {
        return true;
    }
    return false;
}

double PlayGetBallAway::dGetScore() const
{
    return PlayGetBallAway::dScore;
}

void PlayGetBallAway::vUpdateScore()
{
    PlayGetBallAway::dScore += 1.0;
}

void PlayGetBallAway::vSaveScore()
{

}

