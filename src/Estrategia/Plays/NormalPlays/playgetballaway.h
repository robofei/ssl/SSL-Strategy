#ifndef PLAYGETBALLAWAY_H
#define PLAYGETBALLAWAY_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"

enum
{
    PLAY_GET_BALL_AWAY =
        520 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayGetBallAway : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleGoalie> roleGoalie;
    QSharedPointer<RoleAttacker> roleAttacker;
    QSharedPointer<RoleReceiver> roleReceiver;

    static double dScore;
    static constexpr int safeX = 2500;

    void vRun() override;

public:
    PlayGetBallAway(AmbienteCampo* _fieldEnv);
    ~PlayGetBallAway();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYGETBALLAWAY_H
