#include "playindividualdefense.h"

double PlayIndividualDefense::dScore = 0.0;

PlayIndividualDefense::PlayIndividualDefense(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_INDIVIDUALDEFENSE;

//    const int robosDelta = 1;
////    const int differenceRobots = fieldEnvironment->allies->getRobotsDetected() - fieldEnvironment->opponents->getRobotsDetected();
//    const int robosIndividual = fieldEnvironment->allies->getRobotsDetected() - 1 - robosDelta;
////    const int robosSobrando = fieldEnvironment->allies->getRobotsDetected() - 1 - robosDelta - robosIndividual;

//    roleGoalie.reset(new RoleGoalie(_fieldEnv));
////    roleThief.reset(new RoleNormalBallThief(_fieldEnv));

//    rolesDelta.reserve(robosDelta);
//    for(qint8 i=0; i<robosDelta; ++i)
//    {
//        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(_fieldEnv));
//    }

//    rolesIndividual.reserve(robosIndividual);
//    for(qint8 i=0; i<robosIndividual; ++i)
//    {
//        rolesIndividual.append(QSharedPointer<RoleIndividualDefender>::create(_fieldEnv));
//    }

//    for(int i=1; i<rolesDelta.size(); ++i)
//    {
//        rolesDelta[i]->bMakeDuplicate(i);
//    }

//    for(int i=1; i<rolesIndividual.size(); ++i)
//    {
//        rolesIndividual[i]->bMakeDuplicate(i);
//    }

//    deltaPos.reset(new DeltaDefense(_fieldEnv));
//    individualPos.reset(new IndividualDefense(_fieldEnv));

//    bAddRole(roleGoalie.get());

//    for(auto& role : rolesDelta)
//    {
//        bAddRole(role.get());
//    }

//    for(auto& role : rolesIndividual)
//    {
//        bAddRole(role.get());
//    }

//    for(auto& role : rolesDelta)
//    {
//        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
//                deltaPos.get(), &DeltaDefense::vAddRobot);
//        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
//                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
//    }

//    for(auto& role : rolesIndividual)
//    {
//        connect(role.get(), &RoleIndividualDefender::_vRoleAssigned,
//                individualPos.get(), &IndividualDefense::vAddRobot);
//        connect(role.get(), &RoleIndividualDefender::_vRequestDefensePosition,
//                individualPos.get(), &IndividualDefense::vReceivePositionRequest_);
//    }

}

PlayIndividualDefense::~PlayIndividualDefense()
{

}

QString PlayIndividualDefense::strName() const
{
    return QString("IndividualDefense");
}

void PlayIndividualDefense::vInitialize()
{
    const int robosDelta = 0;
//    const int differenceRobots = fieldEnvironment->allies->getRobotsDetected() - fieldEnvironment->opponents->getRobotsDetected();
    const int robosIndividual = fieldEnvironment->allies->getRobotsDetected() - 1 - robosDelta;
//    const int robosSobrando = fieldEnvironment->allies->getRobotsDetected() - 1 - robosDelta - robosIndividual;

    roleGoalie.reset(new RoleGoalie(fieldEnvironment));
//    roleThief.reset(new RoleNormalBallThief(_fieldEnv));

//    rolesDelta.clear();
    rolesIndividual.clear();

//    rolesDelta.reserve(robosDelta);
//    for(qint8 i=0; i<robosDelta; ++i)
//    {
//        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(fieldEnvironment));
//    }

    rolesIndividual.reserve(robosIndividual);
    for(qint8 i=0; i<robosIndividual; ++i)
    {
        rolesIndividual.append(QSharedPointer<RoleIndividualDefender>::create(fieldEnvironment));
    }

//    for(int i=1; i<rolesDelta.size(); ++i)
//    {
//        rolesDelta[i]->bMakeDuplicate(i);
//    }

    for(int i=1; i<rolesIndividual.size(); ++i)
    {
        rolesIndividual[i]->bMakeDuplicate(i);
    }

//    deltaPos.reset(new DeltaDefense(fieldEnvironment));
    individualPos.reset(new IndividualDefense(fieldEnvironment));

    bAddRole(roleGoalie.get());

//    for(auto& role : rolesDelta)
//    {
//        bAddRole(role.get());
//    }

    for(auto& role : rolesIndividual)
    {
        bAddRole(role.get());
    }

//    for(auto& role : rolesDelta)
//    {
//        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
//                deltaPos.get(), &DeltaDefense::vAddRobot);
//        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
//                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
//    }

    for(auto& role : rolesIndividual)
    {
        connect(role.get(), &RoleIndividualDefender::_vRoleAssigned,
                individualPos.get(), &IndividualDefense::vAddRobot);
        connect(role.get(), &RoleIndividualDefender::_vRequestDefensePosition,
                individualPos.get(), &IndividualDefense::vReceivePositionRequest_);
    }
    const AmbienteCampo& env = *fieldEnvironment;

    const int allySide = env.allies->iGetSide();

//    for(auto& role : rolesDelta)
//    {
//        role->zone = deltaPos->getZone();
//    }

    for(auto& role : rolesIndividual)
    {
        role->zone = individualPos->getZone();
    }

    roleGoalie->zone.vConfigZone(env.geoCampo->vt2dGoalCenter(allySide).toPoint(),
                                 2e3);

//    roleThief->zone.vConfigZone(env.vt2dPosicaoBola().toPoint(), 300);

    vSetPlayTimeout(500 * 1000);
//     vSetPlayTimeout(30e3);

    bInitialized = true;
}

bool PlayIndividualDefense::bCheckPreConditions()
{
    /// \todo A bola deve estar em disputa
    return true;
}

bool PlayIndividualDefense::bCheckEndConditions()
{
    const AmbienteCampo& env = *fieldEnvironment;

//    if(!roleThief->zone.bInsideZone(env.vt2dPosicaoBola()) ||
//       roleThief->player()->iID() != env.iAchaRoboProximoBola({roleGoalie->player()->iID()}) ||
//       env.bBolaDentroAreaDefesaAliado())
//    {
//        playStatus = ABORTED;
//    }

//    return playStatus == FINISHED || playStatus == ABORTED;
    return false;
}

double PlayIndividualDefense::dGetScore() const
{
    return PlayIndividualDefense::dScore;
}

void PlayIndividualDefense::vUpdateScore()
{
    PlayIndividualDefense::dScore += 1.0;
}

void PlayIndividualDefense::vSaveScore()
{

}
