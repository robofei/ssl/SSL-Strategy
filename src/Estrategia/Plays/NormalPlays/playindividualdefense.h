#ifndef PLAYINDIVIDUALDEFENSE_H
#define PLAYINDIVIDUALDEFENSE_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/individualdefense.h"
#include "Posicionamento/Delta/deltadefense.h"


enum
{
    PLAY_INDIVIDUALDEFENSE = 440
};

class PlayIndividualDefense : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleGoalie> roleGoalie;
    QSharedPointer<RoleNormalBallThief> roleThief;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;
    QVector<QSharedPointer<RoleIndividualDefender>> rolesIndividual;

    static double dScore;

    QScopedPointer<IndividualDefense> individualPos;
    QScopedPointer<DeltaDefense> deltaPos;
public:
    PlayIndividualDefense(AmbienteCampo *_fieldEnv);
    ~PlayIndividualDefense();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYINDIVIDUALDEFENSE_H
