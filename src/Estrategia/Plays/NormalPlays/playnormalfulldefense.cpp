#include "playnormalfulldefense.h"

double PlayNormalFullDefense::dScore = 0.0;

PlayNormalFullDefense::PlayNormalFullDefense(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_NORMALFULLDEFENSE;


    const int robosDelta = 2;

    // Inicializa os roles
    roleGoalie.reset(new RoleGoalie(_fieldEnv));
    roleThief.reset( new RoleNormalBallThief(_fieldEnv) );

    rolesDelta.reserve(robosDelta);
    for(qint8 i=0; i<robosDelta; ++i)
    {
        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(_fieldEnv));
    }

    for(int i=1; i<rolesDelta.size(); ++i)
    {
        rolesDelta[i]->bMakeDuplicate(i);
    }

    deltaPos.reset( new DeltaDefense(_fieldEnv) );

    roleThief->vSetPriority(Role::MAX_PRIORITY);

    bAddRole(roleGoalie.get());
    bAddRole(roleThief.get());
    for(auto& role : rolesDelta)
    {
        bAddRole(role.get());
    }

    for(auto& role : rolesDelta)
    {
        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
                deltaPos.get(), &DeltaDefense::vAddRobot);
        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
    }


}

PlayNormalFullDefense::~PlayNormalFullDefense()
{

}

QString PlayNormalFullDefense::strName() const
{
    return QString("NormalFullDefense");
}

void PlayNormalFullDefense::vInitialize()
{
    const AmbienteCampo& env = *fieldEnvironment;

    const int allySide = env.allies->iGetSide();

    for(auto& role : rolesDelta)
    {
        role->zone = deltaPos->getZone();
    }

    roleGoalie->zone.vConfigZone(env.geoCampo->vt2dGoalCenter(allySide).toPoint(),
                                 2e3);

    roleThief->zone.vConfigZone(env.vt2dPosicaoBola().toPoint(), 300);

    vSetPlayTimeout(30 * 1000);
//     vSetPlayTimeout(30e3);

    bInitialized = true;
}

bool PlayNormalFullDefense::bCheckPreConditions()
{
    /// \todo A bola deve estar em disputa
    return true;
}

bool PlayNormalFullDefense::bCheckEndConditions()
{
    const AmbienteCampo& env = *fieldEnvironment;

    if(!roleThief->zone.bInsideZone(env.vt2dPosicaoBola()) ||
       roleThief->player()->iID() != env.iAchaRoboProximoBola({roleGoalie->player()->iID()}) ||
       env.bBolaDentroAreaDefesaAliado())
    {
        playStatus = ABORTED;
    }

    return playStatus == FINISHED || playStatus == ABORTED;
}

double PlayNormalFullDefense::dGetScore() const
{
    return PlayNormalFullDefense::dScore;
}

void PlayNormalFullDefense::vUpdateScore()
{
    PlayNormalFullDefense::dScore += 1.0;
}

void PlayNormalFullDefense::vSaveScore()
{

}
