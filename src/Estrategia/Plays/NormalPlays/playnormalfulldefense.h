#ifndef PLAYNORMALFULLDEFENSE_H
#define PLAYNORMALFULLDEFENSE_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_NORMALFULLDEFENSE = 360 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayNormalFullDefense : public Play
{
    Q_OBJECT
private:
    // Define os roles da play


    QSharedPointer<RoleGoalie> roleGoalie;
    QSharedPointer<RoleNormalBallThief> roleThief;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;

    static double dScore;

    QScopedPointer<DeltaDefense> deltaPos;
public:
    PlayNormalFullDefense(AmbienteCampo *_fieldEnv);
    ~PlayNormalFullDefense();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYNORMALFULLDEFENSE_H
