#include "playpressuredefense.h"

double PlayPressureDefense::dScore = 0.0;

PlayPressureDefense::PlayPressureDefense(AmbienteCampo* _fieldEnv)
    : Play(_fieldEnv)
{
    iID = PLAY_PRESSUREDEFENSE;
}

PlayPressureDefense::~PlayPressureDefense()
{
}

void PlayPressureDefense::createDeltaRoles(int deltaRobots)
{
    deltaPos.reset(new DeltaDefense(fieldEnvironment));

    rolesDelta.clear();
    rolesDelta.reserve(deltaRobots);
    for (qint8 i = 0; i < deltaRobots; ++i)
    {
        rolesDelta.append(
            QSharedPointer<RoleDeltaDefender>::create(fieldEnvironment));

        if (i > 0)
        {
            rolesDelta[i]->bMakeDuplicate(i);
        }
        rolesDelta[i]->zone = deltaPos->getZone();

        bAddRole(rolesDelta[i].get());
        connect(rolesDelta[i].get(), &RoleDeltaDefender::_vRoleAssigned,
                deltaPos.get(), &DeltaDefense::vAddRobot);
        connect(rolesDelta[i].get(),
                &RoleDeltaDefender::_vRequestDefensePosition, deltaPos.get(),
                &DeltaDefense::vReceivePositionRequest_);
        rolesDelta[i]->vSetPriority(Role::LOW_PRIORITY);
    }
}

void PlayPressureDefense::createPressureRoles(int pressureRobots)
{
    pressurePos.reset(new PressureDefense(fieldEnvironment));

    rolesPressure.clear();
    rolesPressure.reserve(pressureRobots);
    for (qint8 i = 0; i < pressureRobots; ++i)
    {
        rolesPressure.append(
            QSharedPointer<RolePressureDefender>::create(fieldEnvironment));

        if (i > 0)
        {
            rolesPressure[i]->bMakeDuplicate(i);
        }
        rolesPressure[i]->zone = pressurePos->getZone();

        bAddRole(rolesPressure[i].get());
        connect(rolesPressure[i].get(), &RolePressureDefender::_vRoleAssigned,
                pressurePos.get(), &PressureDefense::vAddRobot);
        connect(rolesPressure[i].get(),
                &RolePressureDefender::_vRequestDefensePosition,
                pressurePos.get(), &PressureDefense::vReceivePositionRequest_);
    }
}

QString PlayPressureDefense::strName() const
{
    return QString("PressureDefense");
}

void PlayPressureDefense::vInitialize()
{
    int robosDelta = 2;
    int robosPressure =
        fieldEnvironment->allies->getRobotsDetected() - 1 - robosDelta;
    bool hasGoalie = fieldEnvironment->allies
                         ->getCPlayer(fieldEnvironment->allies->iGetGoalieID())
                         ->bRoboDetectado();
    if (robosPressure < 1)
    {
        robosDelta = fieldEnvironment->allies->getRobotsDetected() - hasGoalie - 1;
        robosDelta = qMax(robosDelta, 0);
        robosPressure = fieldEnvironment->allies->getRobotsDetected() -
                        hasGoalie - robosDelta;
    }

    vClearRoles();

    roleGoalie.reset(new RoleNightyGoalie(fieldEnvironment));
    int allySide = fieldEnvironment->allies->iGetSide();
    QVector2D goalCenter = fieldEnvironment->geoCampo->vt2dGoalCenter(allySide);
    roleGoalie->zone.vConfigZone(goalCenter.toPoint(), 2e3);
    
    bAddRole(roleGoalie.get());

    createPressureRoles(robosPressure);
    createDeltaRoles(robosDelta);

    vSetPlayTimeout(10 * 1000);
    bInitialized = true;
}

bool PlayPressureDefense::bCheckPreConditions()
{
    if (fieldEnvironment->allies->iGetSide() ==
        Auxiliar::iSinal(fieldEnvironment->vt2dPosicaoBola().x()))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool PlayPressureDefense::bCheckEndConditions()
{
    if (fieldEnvironment->opponents->iGetSide() ==
        Auxiliar::iSinal(fieldEnvironment->vt2dPosicaoBola().x()))
    {
        return true;
    }
    else
    {
        return false;
    }
    // if(bRolesFinished())
    // {
    //     return true;
    // }
    // else
    // {
    //     return false;
    // }
}

double PlayPressureDefense::dGetScore() const
{
    return PlayPressureDefense::dScore;
}

void PlayPressureDefense::vUpdateScore()
{
    PlayPressureDefense::dScore += 1.0;
}

void PlayPressureDefense::vSaveScore()
{
}
