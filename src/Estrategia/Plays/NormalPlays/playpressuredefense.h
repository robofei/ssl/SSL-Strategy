#ifndef PLAYPRESSUREDEFENSE_H
#define PLAYPRESSUREDEFENSE_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/pressuredefense.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_PRESSUREDEFENSE = 460
};

class PlayPressureDefense : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QSharedPointer<RoleNormalBallThief> roleThief;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;
    QVector<QSharedPointer<RolePressureDefender>> rolesPressure;

    static double dScore;

    QScopedPointer<PressureDefense> pressurePos;
    QScopedPointer<DeltaDefense> deltaPos;

    void createDeltaRoles(int deltaRobots);
    void createPressureRoles(int pressureRobots);
public:
    PlayPressureDefense(AmbienteCampo *_fieldEnv);
    ~PlayPressureDefense();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYPRESSUREDEFENSE_H
