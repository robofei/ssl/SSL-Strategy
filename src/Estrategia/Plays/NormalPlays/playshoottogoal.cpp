#include "playshoottogoal.h"
double PlayShootToGoal::dScore = 0.0;

PlayShootToGoal::PlayShootToGoal(AmbienteCampo* _fieldEnv) : Play{_fieldEnv}
{
    iID = PLAY_SHOOT_TO_GOAL;

    // Inicializa as roles
    roleGoalie.reset(new RoleGoalie(_fieldEnv));
    bAddRole(roleGoalie.get());

    roleAttacker.reset(new RoleAttacker(_fieldEnv));
    roleFinisher.reset(new RoleFinisher(_fieldEnv));

    roleAttacker->vSetPriority(Role::MAX_PRIORITY);
    roleFinisher->vSetPriority(Role::MEDIUM_PRIORITY);

    bAddRole(roleAttacker.get());
    bAddRole(roleFinisher.get());

    // ==================== Comunicação entre os robôs ==================
    // Fala para o suporte quem vai receber o passe
    connect(roleFinisher.get(), &RoleFinisher::_vRoleAssigned,
            roleAttacker.get(), &RoleAttacker::vUpdateReceiver_);
    // Fala para o atacante onde ele vai receber o passe
    connect(roleAttacker.get(), &RoleAttacker::_vSendPassPoint,
            roleFinisher.get(), &RoleFinisher::vSetPassPoint_);
}

PlayShootToGoal::~PlayShootToGoal()
{
}

void PlayShootToGoal::vRun()
{
}

QString PlayShootToGoal::strName() const
{
    return QString("Shoot to Goal");
}

void PlayShootToGoal::vInitialize()
{
    roleAttacker->zone.vConfigZone(
        fieldEnvironment->vt2dPosicaoBola().toPoint(), 500);
    const int opponentSide = fieldEnvironment->opponents->iGetSide();

    roleGoalie->zone.vConfigZone(
        fieldEnvironment->geoCampo->retRestriction(opponentSide));

    QSize fieldSz = fieldEnvironment->geoCampo->szField();
    QSize zoneSize(fieldSz.width() / 2 - attackX, -fieldSz.height());
    QPoint topLeft(attackX * opponentSide, fieldSz.height() / 2);

    roleFinisher->zone.vConfigZone(QRect(topLeft, zoneSize));

    bInitialized = true;
}

bool PlayShootToGoal::bCheckPreConditions()
{
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const int allySide = fieldEnvironment->allies->iGetSide();
    float ballX = fabs(fieldEnvironment->vt2dPosicaoBola().x());
    int ballSide = Auxiliar::iSinal(ballX);

    if (iGetValidIDs().size() >= 1)
    {
        if (ballX > attackX && ballSide == opponentSide)
        {
            return true;
        }
    }
    else
    {
        qInfo() << QString("[Play %1] No mínimo 2 robôs são necessários!")
                       .arg(strName());
    }

    return false;
}

bool PlayShootToGoal::bCheckEndConditions()
{
    float ballX = fabs(fieldEnvironment->vt2dPosicaoBola().x());
    int ballSide = Auxiliar::iSinal(ballX);
    if (ballX < attackX || ballSide != fieldEnvironment->opponents->iGetSide())
    {
        qInfo() << QString("[Play %1] Bola saiu da area de finalização!")
                       .arg(strName());
        playStatus = FINISHED;
    }

    if (playStatus == FINISHED || playStatus == ABORTED)
    {
        return true;
    }
    return false;
}

double PlayShootToGoal::dGetScore() const
{
    return PlayShootToGoal::dScore;
}

void PlayShootToGoal::vUpdateScore()
{
    PlayShootToGoal::dScore += 1.0;
}

void PlayShootToGoal::vSaveScore()
{
}
