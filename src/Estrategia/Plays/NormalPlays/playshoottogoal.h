#ifndef PLAYSHOOTTOGOAL_H
#define PLAYSHOOTTOGOAL_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"

enum
{
    PLAY_SHOOT_TO_GOAL =
        540 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayShootToGoal : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleGoalie> roleGoalie;
    QSharedPointer<RoleAttacker> roleAttacker;
    QSharedPointer<RoleFinisher> roleFinisher;

    static double dScore;
    static constexpr int attackX = 2000;

    void vRun() override;

public:
    PlayShootToGoal(AmbienteCampo* _fieldEnv);
    ~PlayShootToGoal();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYSHOOTTOGOAL_H
