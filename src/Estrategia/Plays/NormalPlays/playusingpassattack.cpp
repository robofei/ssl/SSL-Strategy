#include "playusingpassattack.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "qvector.h"

double PlayUsingPassAttack::dScore = 0.0;

PlayUsingPassAttack::PlayUsingPassAttack(AmbienteCampo* _fieldEnv)
    : Play(_fieldEnv)
{
    iID = PLAY_USINGPASS_ATTACK;

    roleGoalie.reset(new RoleNightyGoalie(fieldEnvironment));
    attackerPos.reset(new NormalAttack(fieldEnvironment));
}

PlayUsingPassAttack::~PlayUsingPassAttack()
{
}

// void PlayUsingPassAttack::vRun()
//{
//     if(roleBallHolder->bIsFinished() && tmrPlayTimeout->isActive())
//     {
//         tmrPlayTimeout->stop();
//     }

//    roleAttacker->vUpdateRemainingTime(tmrPlayTimeout->remainingTime());
//}

void PlayUsingPassAttack::vRun()
{
    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
    const int allySide = fieldEnvironment->allies->iGetSide();

    QVector<qint8> ignoredIDs = {fieldEnvironment->allies->iGetGoalieID()};

    for (auto& role : rolesDelta)
    {
        if(role->bIsAssigned())
        {

        ignoredIDs.append(role->player()->iID());
        }
    }
    qint8 ballNearestRobot = fieldEnvironment->iAchaRoboProximoBola(ignoredIDs);

    QVector2D nearPos = fieldEnvironment->allies->getCPlayer(ballNearestRobot)
                            ->vt2dPosicaoAtual();
    qint8 bestOption = freestRobot(nearPos);

    QVector2D bestPos =
        fieldEnvironment->allies->getCPlayer(bestOption)->vt2dPosicaoAtual();
    double bestAngle = Auxiliar::dAnguloEmGrausLivreParaChute(
        bestPos, allySide, fieldEnvironment->geoCampo->szGoal().height(),
        fieldEnvironment->geoCampo->szField(),
        fieldEnvironment->vt3dPegaPosicaoTodosObjetos(otOponente));
    double nearestAngle = Auxiliar::dAnguloEmGrausLivreParaChute(
        nearPos, allySide, fieldEnvironment->geoCampo->szGoal().height(),
        fieldEnvironment->geoCampo->szField(),
        fieldEnvironment->vt3dPegaPosicaoTodosObjetos(otOponente));

    if (ballPos.distanceToPoint(fieldEnvironment->geoCampo->vt2dGoalCenter(
            fieldEnvironment->opponents->iGetSide())) < 3e3)
    {
        id_shooter = ballNearestRobot;
        id_passer = id_receiver = id_recovering = -1;
    }
    else if ((bestOption != -1 && ballNearestRobot != bestOption &&
              bestAngle - 10 > nearestAngle) &&
             (id_passer == -1 && id_receiver == -1))
    {
        if (id_receiver != bestOption && id_receiver != -1)
        {
            QVector2D recPos = fieldEnvironment->allies->getCPlayer(id_receiver)
                                   ->vt2dPosicaoAtual(),
                      newRecPos =
                          fieldEnvironment->allies->getCPlayer(bestOption)
                              ->vt2dPosicaoAtual();
            double recAngle = Auxiliar::dAnguloEmGrausLivreParaChute(
                       recPos, allySide,
                       fieldEnvironment->geoCampo->szGoal().height(),
                       fieldEnvironment->geoCampo->szField(),
                       fieldEnvironment->vt3dPegaPosicaoTodosObjetos(
                           otOponente)),
                   newRecAngle = Auxiliar::dAnguloEmGrausLivreParaChute(
                       newRecPos, allySide,
                       fieldEnvironment->geoCampo->szGoal().height(),
                       fieldEnvironment->geoCampo->szField(),
                       fieldEnvironment->vt3dPegaPosicaoTodosObjetos(
                           otOponente));

            if (newRecAngle < recAngle + 10)
            {
                bestOption = id_receiver;
            }
        }

        id_passer = ballNearestRobot;
        id_receiver = bestOption;
        id_shooter = id_recovering = -1;
    }
    else if (bestOption != -1 && ballNearestRobot == bestOption &&
             id_shooter == -1)
    {
        id_shooter = bestOption;
        id_passer = id_receiver = id_recovering = -1;
    }

    if (id_shooter != -1 && id_passer != -1)
    {
        id_shooter = id_recovering = id_passer = id_receiver = -1;
        id_shooter = ballNearestRobot;
    }

    if (ballNearestRobot != id_shooter && ballNearestRobot != id_passer)
    {
        id_shooter = id_recovering = id_passer = id_receiver = -1;
        id_shooter = ballNearestRobot;
    }
    // else
    // {
    //     id_shooter = ballNearestRobot;
    //     id_passer = id_receiver = id_recovering = -1;
    // }
    //
    // if ((id_passer == -1 && id_receiver != -1) ||
    //     (id_passer != -1 && id_receiver == -1))
    // {
    //     id_passer = id_receiver = -1;
    //     id_shooter = ballNearestRobot;
    // }

    if (id_shooter + id_recovering + id_passer + id_receiver == -4)
    {
        id_shooter = ballNearestRobot;
    }

    emit _vSendUpdate(id_shooter, id_receiver, id_passer, id_recovering);
}

qint8 PlayUsingPassAttack::iVerifyBestOption(qint8 _ignoredID)
{
    QVector<QVector3D> positions_robots =
        fieldEnvironment->vt3dPegaPosicaoTodosObjetos(otAliado);
    //    QVector<int> bestGoalViews;
    // qDebug() << "[iVerifyBestOption()] Verifying best option!";
    QVector<QVector3D> opponents =
        fieldEnvironment->vt3dPegaPosicaoTodosObjetos(otOponente);
    int melhor_id = -1;
    int maior_intervalo = -1;

    for (qint8 i = 0; i < positions_robots.size(); ++i)
    {
        if (fieldEnvironment->allies->getPlayer(i)->bRoboDetectado() &&
            i != fieldEnvironment->allies->iGetGoalieID() && _ignoredID != i)
        {
            QVector<QVector2D> interval_to_goal =
                Auxiliar::vt2dIntervaloLivreGol(
                    positions_robots.at(i).toVector2D(),
                    fieldEnvironment->allies->iGetSide(),
                    fieldEnvironment->geoCampo->szGoal().height(),
                    fieldEnvironment->geoCampo->szGoal().width(),
                    fieldEnvironment->geoCampo->szField(), opponents);
            if (interval_to_goal.size() >= maior_intervalo)
            {
                maior_intervalo = interval_to_goal.size();
                melhor_id = fieldEnvironment->allies->getPlayer(i)->iID();
            }
        }
    }
    // qDebug() << "[iVerifyBestOption()] Best id option = " << melhor_id;
    return melhor_id;
}

qint8 PlayUsingPassAttack::freestRobot(QVector2D _kickerPos)
{
    QVector<qint8> robots;
    qint8 bestRobot = -1;
    double bestAngle = 0;

    robots = iPlayIDs;
    const int allySide = fieldEnvironment->allies->iGetSide();
    QVector2D pos;
    QVector<QVector3D> opponents =
        fieldEnvironment->vt3dPegaPosicaoTodosObjetos(otOponente);

    for (qint8 n = 0; n < robots.size(); ++n)
    {
        pos = fieldEnvironment->allies->getCPlayer(robots.at(n))
                  ->vt2dPosicaoAtual();
        if (Auxiliar::bChecaInterseccaoObjetosLinha(_kickerPos, pos, 250,
                                                    opponents) == false)
        {
            double angle = Auxiliar::dAnguloEmGrausLivreParaChute(
                pos, allySide, fieldEnvironment->geoCampo->szGoal().height(),
                fieldEnvironment->geoCampo->szField(), opponents);

            if (bestAngle < angle)
            {
                bestAngle = angle;
                bestRobot = robots.at(n);
            }
        }
    }

    return bestRobot;
}

QString PlayUsingPassAttack::strName() const
{
    return QString("Using Pass Attack");
}

void PlayUsingPassAttack::vInitialize()
{
    vClearRoles();
    qDebug() << strName() << " Initialized!";
    bool hasGoalie = fieldEnvironment->allies
                         ->getCPlayer(fieldEnvironment->allies->iGetGoalieID())
                         ->bRoboDetectado();
    int robosDelta = 2;
    int robosAtacantes =
        fieldEnvironment->allies->getRobotsDetected() - hasGoalie - robosDelta;

    while (robosAtacantes < 1 && robosDelta > 0)
    {
        robosDelta--;
        robosAtacantes = fieldEnvironment->allies->getRobotsDetected() -
                         hasGoalie - robosDelta;
    }

    if (robosDelta != rolesDelta.size())
    {
        rolesDelta.reserve(robosDelta);
        for (qint8 i = 0; i < robosDelta; ++i)
        {
            rolesDelta.append(
                QSharedPointer<RoleDeltaDefender>::create(fieldEnvironment));
        }

        for (int i = 1; i < rolesDelta.size(); ++i)
        {
            rolesDelta[i]->bMakeDuplicate(i);
        }

        deltaPos.reset(new DeltaDefense(fieldEnvironment));

        for (auto& role : rolesDelta)
        {
            role->zone = deltaPos->getZone();
        }

        for (auto& role : rolesDelta)
        {
            bAddRole(role.get());
            connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
                    deltaPos.get(), &DeltaDefense::vAddRobot);
            connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
                    deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
            role->vSetPriority(Role::LOW_PRIORITY);
        }
    }

    for (auto& role : rolesDelta)
    {
        bAddRole(role.get());
    }

    if (robosAtacantes != rolesAttackers.size())
    {
        rolesAttackers.clear();

        for (qint8 i = 0; i < robosAtacantes; ++i)
        {
            rolesAttackers.append(
                QSharedPointer<RoleAttackers>::create(fieldEnvironment));
        }

        Role::ROLE_PRIORITY priority = Role::MAX_PRIORITY;
        for (auto& role : rolesAttackers)
        {
            connect(role.get(), &RoleAttackers::_vRoleAssigned,
                    attackerPos.get(), &NormalAttack::vAddRobot);
            connect(role.get(), &RoleAttackers::_vRoleRemoved,
                    attackerPos.get(), &NormalAttack::vRemoveRobot);
            connect(role.get(), &RoleAttackers::_vRequestAttackPosition,
                    attackerPos.get(), &NormalAttack::vReceivePositionRequest_);
            connect(this, &PlayUsingPassAttack::_vSendUpdate, role.get(),
                    &RoleAttackers::_parseUpdate);
            role->vSetPriority(priority);
            priority = Role::MEDIUM_PRIORITY;
        }

        for (qint8 i = 0; i < robosAtacantes; ++i)
        {
            rolesAttackers[i]->bMakeDuplicate(i);
        }
    }
    for (auto& role : rolesAttackers)
    {
        bAddRole(role.get());
    }
    const AmbienteCampo& env = *fieldEnvironment;

    id_shooter = -1;
    id_receiver = -1;
    id_passer = -1;
    id_recovering = -1;
    const int allySide = env.allies->iGetSide();
    for (auto& role : rolesAttackers)
    {
        role->zone.vConfigZone(attackerPos->getZone());
    }
    bAddRole(roleGoalie.get());
    roleGoalie->zone.vConfigZone(
        env.geoCampo->vt2dGoalCenter(allySide).toPoint(), 2e3);
    vSetPlayTimeout(10 * 1000);

    bInitialized = true;
}

bool PlayUsingPassAttack::bCheckPreConditions()
{
    if (fieldEnvironment->opponents->iGetSide() ==
        Auxiliar::iSinal(fieldEnvironment->vt2dPosicaoBola().x()))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool PlayUsingPassAttack::bCheckEndConditions()
{
    qint8 ballNearestRobot = fieldEnvironment->iAchaRoboProximoBola();
    QVector2D nearPos = fieldEnvironment->allies->getCPlayer(ballNearestRobot)
                            ->vt2dPosicaoAtual();

    bool shooting = false;
    for (int i = 0; i < rolesAttackers.size(); ++i)
    {
        shooting = shooting | rolesAttackers.at(i)->shooting();
    }
    // return false;
    if (fieldEnvironment->allies->iGetSide() ==
        Auxiliar::iSinal(fieldEnvironment->vt2dPosicaoBola().x()))
    {
        playStatus = FINISHED;
        return true;
    }
    else if (fieldEnvironment->ballOutOfField())
    {
        playStatus = FINISHED;
        return true;
    }
    if (!shooting)
    {
        playStatus = FINISHED;
        return true;
    }
    // else if (nearPos.distanceToPoint(fieldEnvironment->vt2dPosicaoBola()) >
    //              1000 ||
    //          finished)
    // {
    //     return true;
    // }
    else
    {
        return false;
    }
}

double PlayUsingPassAttack::dGetScore() const
{
    return PlayUsingPassAttack::dScore;
}

void PlayUsingPassAttack::vUpdateScore()
{
    PlayUsingPassAttack::dScore += 1.0;
}

void PlayUsingPassAttack::vSaveScore()
{
}
