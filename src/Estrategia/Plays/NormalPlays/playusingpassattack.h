#ifndef PLAYUSINGPASSATTACK_H
#define PLAYUSINGPASSATTACK_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"
#include "Posicionamento/Marking/normalattack.h"

enum
{
    PLAY_USINGPASS_ATTACK = 480
};

class PlayUsingPassAttack : public Play
{
    Q_OBJECT
private:
    QVector<QSharedPointer<RoleAttackers>> rolesAttackers;
    QSharedPointer<RoleBasicFreeKickSupport> roleBallHolder;
    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;
    QScopedPointer<DeltaDefense> deltaPos;

    static double dScore;
//    void vRun() override;
    QScopedPointer<NormalAttack> attackerPos;
    void vRun() override;

    qint8 id_shooter;
    qint8 id_receiver;
    qint8 id_passer;
    qint8 id_recovering;

    enum STATES
    {
        SHOOT_MODE,
        SEND_PASS,
        RECOVER_BALL
    };
    STATES currentState;
        
    qint8 iVerifyBestOption(qint8 _ignoredID = -1);
    qint8 freestRobot(QVector2D _kickerPos);
public:
    PlayUsingPassAttack(AmbienteCampo *_fieldEnv);
    ~PlayUsingPassAttack();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;

signals:
    void _vSendUpdate(qint8 _shooter, qint8 _receiver, qint8 _passer, qint8 _recovering);
};

#endif // PLAYUSINGPASSATTACK_H
