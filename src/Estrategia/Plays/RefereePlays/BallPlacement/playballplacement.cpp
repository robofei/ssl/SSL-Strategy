#include "playballplacement.h"

double PlayBallPlacement::dScore = 0.0;

PlayBallPlacement::PlayBallPlacement(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_BALLPLACEMENT;

    // Inicializa os roles
    const int robosDelta = fieldEnvironment->iPegaRobosEmCampo() - 3;

    roleGoalie.reset(new RoleNightyGoalie(_fieldEnv));
    roleBallPlacer.reset(new RoleBallPlacer(_fieldEnv));
    roleBallPlacerSupport.reset(new RoleBallPlacerSupport(_fieldEnv));

    rolesDelta.reserve(robosDelta);
    for(qint8 i=0; i<robosDelta; ++i)
    {
        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(_fieldEnv));
    }

    for(int i=1; i<rolesDelta.size(); ++i)
    {
        rolesDelta[i]->bMakeDuplicate(i);
    }

    bAddRole(roleGoalie.get());
    bAddRole(roleBallPlacer.get());
    bAddRole(roleBallPlacerSupport.get());

    connect(roleBallPlacer.get(), &RoleBallPlacer::_vRoleAssigned,
            roleBallPlacerSupport.get(), &RoleBallPlacerSupport::vUpdateReceiver_);
    // connect(roleBallPlacer.get(), &RoleBallPlacer::_requestPass,
    //         roleBallPlacerSupport.get(), &RoleBallPlacerSupport::vReceivePass);

    for(auto& role : rolesDelta)
    {
        bAddRole(role.get());
    }

    deltaPos.reset( new DeltaDefense(_fieldEnv) );

    for(auto& role : rolesDelta)
    {
        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
                deltaPos.get(), &DeltaDefense::vAddRobot);
        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
    }
    tmrPlayTimeout->setInterval(30 * 1000);
}

PlayBallPlacement::~PlayBallPlacement()
{

}

QString PlayBallPlacement::strName() const
{
    return QString("Ball Placement");
}

void PlayBallPlacement::vInitialize()
{
    const int allySide = fieldEnvironment->allies->iGetSide();

    roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
            2e3);
    roleBallPlacer->zone.vConfigZone(referee->ballPlacementPosition().toPoint(),
            2e3);
    roleBallPlacerSupport->zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(),
            2e3);
    roleBallPlacer->vSetPriority(Role::MAX_PRIORITY);

    roleBallPlacerSupport->setPlacementPosition(referee->ballPlacementPosition());
    roleBallPlacer->setPlacementPosition(referee->ballPlacementPosition());

    for(auto& role : rolesDelta)
    {
        role->zone = deltaPos->getZone();
    }

//    roleGoalie->vSetGoalieMode(RoleGoalie::STOP_MODE);
    bInitialized = true;
}

bool PlayBallPlacement::bCheckPreConditions()
{
    if(referee->ballPlacementPosition().distanceToPoint(fieldEnvironment->vt2dPosicaoBola()) > 150)
    {
        return true;
    }
    return false;
}

bool PlayBallPlacement::bCheckEndConditions()
{
    const bool stateOk = bHasReferee() &&
        (referee->sslrefGetCommand() == SSL_Referee_Command_BALL_PLACEMENT_BLUE ||
         referee->sslrefGetCommand() == SSL_Referee_Command_BALL_PLACEMENT_YELLOW);

    if(!stateOk)
    {
        playStatus = ABORTED;
    }

    return playStatus == FINISHED || playStatus == ABORTED;
}

double PlayBallPlacement::dGetScore() const
{
    return PlayBallPlacement::dScore;
}

void PlayBallPlacement::vUpdateScore()
{
    PlayBallPlacement::dScore += 1.0;
}

void PlayBallPlacement::vSaveScore()
{

}
