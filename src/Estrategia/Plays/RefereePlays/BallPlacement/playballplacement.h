#ifndef PLAYBALLPLACEMENT_H
#define PLAYBALLPLACEMENT_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_BALLPLACEMENT = 420
};

class PlayBallPlacement : public Play
{
    Q_OBJECT
private:
    // Define os roles da play
    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;
    QSharedPointer<RoleBallPlacer> roleBallPlacer;
    QSharedPointer<RoleBallPlacerSupport> roleBallPlacerSupport;

    static double dScore;

    QScopedPointer<DeltaDefense> deltaPos;

public:
    PlayBallPlacement(AmbienteCampo *_fieldEnv);
    ~PlayBallPlacement();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYBALLPLACEMENT_H
