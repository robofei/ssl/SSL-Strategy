#include "playballplacementopp.h"

double PlayBallPlacementOpp::dScore = 0.0;

PlayBallPlacementOpp::PlayBallPlacementOpp(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_BALLPLACEMENTOPP;

    // Inicializa os roles
    const int robosDelta = 5;

    roleGoalie.reset(new RoleNightyGoalie(_fieldEnv));

    rolesDelta.reserve(robosDelta);
    for(qint8 i=0; i<robosDelta; ++i)
    {
        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(_fieldEnv));
    }

    for(int i=1; i<rolesDelta.size(); ++i)
    {
        rolesDelta[i]->bMakeDuplicate(i);
    }

    bAddRole(roleGoalie.get());
    for(auto& role : rolesDelta)
    {
        bAddRole(role.get());
    }

    deltaPos.reset( new DeltaDefense(_fieldEnv) );

    for(auto& role : rolesDelta)
    {
        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
                deltaPos.get(), &DeltaDefense::vAddRobot);
        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
    }
}

PlayBallPlacementOpp::~PlayBallPlacementOpp()
{

}

QString PlayBallPlacementOpp::strName() const
{
    return QString("BallPlacementOpp");
}

void PlayBallPlacementOpp::vInitialize()
{
    const int allySide = fieldEnvironment->allies->iGetSide();

    roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
            2e3);

    for(auto& role : rolesDelta)
    {
        role->zone = deltaPos->getZone();
    }

    roleGoalie->vSetGoalieMode(RoleNightyGoalie::STOP_MODE);

    bInitialized = true;
}

bool PlayBallPlacementOpp::bCheckPreConditions()
{
    return true;
}

bool PlayBallPlacementOpp::bCheckEndConditions()
{
    const bool stateOk = bHasReferee() &&
        (referee->sslrefGetCommand() == SSL_Referee_Command_BALL_PLACEMENT_BLUE ||
         referee->sslrefGetCommand() == SSL_Referee_Command_BALL_PLACEMENT_YELLOW);

    if(!stateOk)
    {
        playStatus = ABORTED;
    }

    return playStatus == FINISHED || playStatus == ABORTED;
}

double PlayBallPlacementOpp::dGetScore() const
{
    return PlayBallPlacementOpp::dScore;
}

void PlayBallPlacementOpp::vUpdateScore()
{
    PlayBallPlacementOpp::dScore += 1.0;
}

void PlayBallPlacementOpp::vSaveScore()
{

}
