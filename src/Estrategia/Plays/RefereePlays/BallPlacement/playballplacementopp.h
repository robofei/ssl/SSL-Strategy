#ifndef PLAYBALLPLACEMENTOPP_H
#define PLAYBALLPLACEMENTOPP_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_BALLPLACEMENTOPP = 380 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayBallPlacementOpp : public Play
{
    Q_OBJECT
private:
    // Define os roles da play
    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;

    static double dScore;

    QScopedPointer<DeltaDefense> deltaPos;

public:
    PlayBallPlacementOpp(AmbienteCampo *_fieldEnv);
    ~PlayBallPlacementOpp();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYBALLPLACEMENTOPP_H
