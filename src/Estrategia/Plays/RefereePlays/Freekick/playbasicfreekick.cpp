#include "playbasicfreekick.h"

double PlayBasicFreeKick::dScore = 0.0;

PlayBasicFreeKick::PlayBasicFreeKick(AmbienteCampo *_fieldEnv) : Play(_fieldEnv)
{
    iID = PLAY_BASIC_FREEKICK;

    roleAttacker.reset(new RoleBasicFreeKickAttacker(_fieldEnv));
    roleSupport.reset(new RoleBasicFreeKickSupport(_fieldEnv));
    roleGoalie.reset(new RoleNightyGoalie(_fieldEnv));

    roleSupport->vSetPriority(Role::MAX_PRIORITY);
    roleAttacker->vSetPriority(Role::MEDIUM_PRIORITY);

    bAddRole(roleGoalie.get());
    bAddRole(roleSupport.get());
    bAddRole(roleAttacker.get());

    // ==================== Comunicação entre os robôs ==================
    // Fala para o suporte quem vai receber o passe
    connect(roleAttacker.get(), &RoleBasicFreeKickAttacker::_vRoleAssigned,
            roleSupport.get(), &RoleBasicFreeKickSupport::vUpdateReceiver_);
    // Fala para o atacante onde ele vai receber o passe
    connect(roleSupport.get(), &RoleBasicFreeKickSupport::_vSendPassPoint,
            roleAttacker.get(), &RoleBasicFreeKickAttacker::vSetPassPoint_);

    // =================== Configura o Timeout da Play ==================
    tmrPlayTimeout->setInterval(13 * 1000); // 8 segundos
}

PlayBasicFreeKick::~PlayBasicFreeKick()
{

}

void PlayBasicFreeKick::vRun()
{
    if(roleSupport->bIsFinished() && tmrPlayTimeout->isActive())
    {
        tmrPlayTimeout->stop();
    }

    roleAttacker->vUpdateRemainingTime(tmrPlayTimeout->remainingTime());
}


QString PlayBasicFreeKick::strName() const
{
    return QString("Basic FreeKick");
}

void PlayBasicFreeKick::vInitialize()
{
    const int allySide = fieldEnvironment->allies->iGetSide();
    roleSupport->zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(),
                                    globalConfig.safety.ball.stop);

    roleAttacker->zone.vConfigZone(roleAttacker->vt2dFindBestPosition().toPoint(),
                                   500);

    if(allySide == XPositivo)
    {
        roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->retPositiveRestriction());
    }
    else
    {
        roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->retNegativeRestriction());
    }

    bInitialized = true;
}

bool PlayBasicFreeKick::bCheckPreConditions()
{
    if(iGetValidIDs().size() >= 1)
    {
        return true;
    }
    qInfo() << QString("[Play %1] No mínimo 2 robôs são necessários!")
        .arg(strName());
    return false;
}

bool PlayBasicFreeKick::bCheckEndConditions()
{
    if(playStatus == FINISHED || playStatus == ABORTED)
    {
        return true;
    }
    return false;
}

double PlayBasicFreeKick::dGetScore() const
{
    return PlayBasicFreeKick::dScore;
}

void PlayBasicFreeKick::vUpdateScore()
{
    PlayBasicFreeKick::dScore += 1.0;
}

void PlayBasicFreeKick::vSaveScore()
{

}
