#ifndef PLAYBASICFREEKICK_H
#define PLAYBASICFREEKICK_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"

enum
{
    PLAY_BASIC_FREEKICK = 200
};

class PlayBasicFreeKick : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleBasicFreeKickAttacker> roleAttacker;
    QSharedPointer<RoleBasicFreeKickSupport> roleSupport;
    QSharedPointer<RoleNightyGoalie> roleGoalie;

    static double dScore;

    void vRun() override;

public:
    PlayBasicFreeKick(AmbienteCampo *_fieldEnv);
    ~PlayBasicFreeKick();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYBASICFREEKICK_H
