#ifndef PLAYFREEKICKOPP_H
#define PLAYFREEKICKOPP_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_FREEKICKOPP = 180 // Consultar arquivo availableplays.h e pular 20 do último ID
};


///
/// \brief Play de freekick defensivo mais básica que coloca um robô na barreira e 4 no delta
///
class PlayFreeKickOpp : public Play
{
    Q_OBJECT
private:
    // Define os roles da play
    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QSharedPointer<RoleBallChaserFreeKick> roleChaser;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;

    static double dScore;

    QScopedPointer<DeltaDefense> deltaPos;

public:
    PlayFreeKickOpp(AmbienteCampo *_fieldEnv);
    ~PlayFreeKickOpp();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYFREEKICKOPP_H
