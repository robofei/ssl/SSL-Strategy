#include "playhalt.h"

double PlayHalt::dScore = 0.0;

PlayHalt::PlayHalt(AmbienteCampo* _fieldEnv) : Play(_fieldEnv)
{
    iID = PLAY_HALT;

    rolesHalt.reserve(globalConfig.robotsPerTeam);
    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        rolesHalt.append(QSharedPointer<RoleHalt>::create(_fieldEnv));
        rolesHalt.at(i)->bMakeDuplicate(i);

        bAddRole(rolesHalt.at(i).get());
    }
    tmrPlayTimeout->setInterval(500 * 1000); // 8 segundos
}

PlayHalt::~PlayHalt()
{
}

QString PlayHalt::strName() const
{
    return QString("Halt");
}

void PlayHalt::vInitialize()
{
    for (int i = 0; i < rolesHalt.size(); ++i)
    {
        rolesHalt[i]->zone.vConfigZone(QPoint(0, 0), 1e3);
    }

    bInitialized = true;
}

bool PlayHalt::bCheckPreConditions()
{
    return true;
}

bool PlayHalt::bCheckEndConditions()
{
    const bool inHalt =
        bHasReferee() &&
        (referee->sslrefGetCommand() == SSL_Referee_Command_HALT ||
         referee->sslrefGetCommand() == SSL_Referee_Command_TIMEOUT_BLUE ||
         referee->sslrefGetCommand() == SSL_Referee_Command_TIMEOUT_YELLOW);
    const bool inPlay = bHasReferee() && referee->bInPlay();
    if (playStatus == FINISHED || !inHalt || inPlay)
    {
        playStatus = FINISHED;
        return true;
    }
    return false;
}

double PlayHalt::dGetScore() const
{
    return PlayHalt::dScore;
}

void PlayHalt::vUpdateScore()
{
    PlayHalt::dScore += 1.0;
}

void PlayHalt::vSaveScore()
{
}
