#ifndef PLAYHALT_H
#define PLAYHALT_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_HALT = 40
};

class PlayHalt : public Play
{
    Q_OBJECT
private:
    QVector<QSharedPointer<RoleHalt>> rolesHalt;
    static double dScore;

public:
    PlayHalt(AmbienteCampo *_fieldEnv);
    ~PlayHalt();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYHALT_H
