#ifndef PLAYKICKOFFALLY_H
#define PLAYKICKOFFALLY_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_KICKOFFALLY = 100 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayKickOffAlly : public Play
{
    Q_OBJECT
private:
    // Define os roles da play


    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QSharedPointer<RoleBallChaserStop> roleAttacker;
    QVector<QSharedPointer<RoleKickOffReceiver>> rolesReceiver;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;

    static double dScore;
    QScopedPointer<DeltaDefense> deltaPos;

public:
    PlayKickOffAlly(AmbienteCampo *_fieldEnv);
    ~PlayKickOffAlly();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYKICKOFFALLY_H
