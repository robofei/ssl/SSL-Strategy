#include "playkickoffallytake.h"

double PlayKickOffAllyTake::dScore = 0.0;

void PlayKickOffAllyTake::vRun()
{
    // for(const auto& role : rolesReceiver)
    // {
        // if(role->bIsAssigned())
        // {
        //     roleAttacker->SLOT_addReceiver(role->player()->iID());
        // }
    // }

}

PlayKickOffAllyTake::PlayKickOffAllyTake(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_KICKOFFALLYTAKE;

    // Inicializa os roles

    const int receptores = 2;
    const int robosDelta = 2;


    roleGoalie.reset(new RoleNightyGoalie(_fieldEnv));

    roleAttacker.reset(new RoleFreeKickShooter(_fieldEnv));
    roleAttacker->vSetPriority(Role::MAX_PRIORITY);

    rolesReceiver.reserve(receptores);
    for(qint8 i=0; i<receptores; ++i)
    {
        rolesReceiver.append(QSharedPointer<RoleKickOffReceiver>::create(_fieldEnv));
    }
    for(int i=1; i<rolesReceiver.size(); ++i)
    {
        rolesReceiver[i]->bMakeDuplicate(i);
    }

    rolesDelta.reserve(robosDelta);
    for(qint8 i=0; i<robosDelta; ++i)
    {
        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(_fieldEnv));
    }
    for(int i=1; i<rolesDelta.size(); ++i)
    {
        rolesDelta[i]->bMakeDuplicate(i);
    }

    deltaPos.reset( new DeltaDefense(_fieldEnv) );

    bAddRole(roleGoalie.get());
    bAddRole(roleAttacker.get());
    for(auto& role : rolesReceiver)
    {
        roleAttacker->vSetPriority(Role::MEDIUM_PRIORITY);
        bAddRole(role.get());
    }
    for(auto& role : rolesDelta)
    {
        bAddRole(role.get());
    }

    for(auto& role : rolesDelta)
    {
        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
                deltaPos.get(), &DeltaDefense::vAddRobot);
        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
    }

}

PlayKickOffAllyTake::~PlayKickOffAllyTake()
{

}

QString PlayKickOffAllyTake::strName() const
{
    return QString("KickOffAllyTake");
}

void PlayKickOffAllyTake::vInitialize()
{
    const AmbienteCampo& env = *fieldEnvironment;

    const int allySide = env.allies->iGetSide();

    for(auto& role : rolesDelta)
    {
        role->zone = deltaPos->getZone();
    }

    rolesReceiver[0]->vSetYSide(-1);
    rolesReceiver[1]->vSetYSide(+1);

    for(auto& role : rolesReceiver)
    {
        role->zone.vConfigZone(role->vt2dGetDestination().toPoint(), 1e3);
    }

    roleGoalie->zone.vConfigZone(env.geoCampo->vt2dGoalCenter(allySide).toPoint(), 2e3);

    roleAttacker->zone.vConfigZone(env.vt2dPosicaoBola().toPoint(), 1e3);

    bInitialized = true;
}

bool PlayKickOffAllyTake::bCheckPreConditions()
{
    return true;
}

bool PlayKickOffAllyTake::bCheckEndConditions()
{
    if(roleAttacker->bIsFinished() ||
        fieldEnvironment->dVelocidadeBola() > 1)
    {
        playStatus = FINISHED;
    }

    if(playStatus == FINISHED)
    {
        return true;
    }
    return false;
}

double PlayKickOffAllyTake::dGetScore() const
{
    return PlayKickOffAllyTake::dScore;
}

void PlayKickOffAllyTake::vUpdateScore()
{
    PlayKickOffAllyTake::dScore += 1.0;
}

void PlayKickOffAllyTake::vSaveScore()
{

}
