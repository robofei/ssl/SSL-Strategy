#ifndef PLAYKICKOFFALLYTAKE_H
#define PLAYKICKOFFALLYTAKE_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_KICKOFFALLYTAKE = 160 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayKickOffAllyTake : public Play
{
    Q_OBJECT
private:
    void vRun() override;
    // Define os roles da play


    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QSharedPointer<RoleFreeKickShooter> roleAttacker;
    QVector<QSharedPointer<RoleKickOffReceiver>> rolesReceiver;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;

    static double dScore;
    QScopedPointer<DeltaDefense> deltaPos;

public:
    PlayKickOffAllyTake(AmbienteCampo *_fieldEnv);
    ~PlayKickOffAllyTake();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYKICKOFFALLYTAKE_H
