#include "playkickoffopp.h"

double PlayKickOffOpp::dScore = 0.0;

PlayKickOffOpp::PlayKickOffOpp(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_KICKOFFOPP;

    // Inicializa as roles
    const int robosDelta = 4;

    roleGoalie.reset(new RoleNightyGoalie(_fieldEnv));

    roleChaser.reset( new RoleBallChaserFreeKick(_fieldEnv, globalConfig.stopLinearVelocity) );
    roleChaser->vSetPriority(Role::MAX_PRIORITY);

    rolesDelta.reserve(robosDelta);
    for(qint8 i=0; i<robosDelta; ++i)
    {
        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(_fieldEnv));
    }

    for(int i=1; i<rolesDelta.size(); ++i)
    {
        rolesDelta[i]->bMakeDuplicate(i);
    }

    deltaPos.reset( new DeltaDefense(_fieldEnv) );

    bAddRole(roleGoalie.get());
    bAddRole(roleChaser.get());
    for(auto& role : rolesDelta)
    {
        bAddRole(role.get());
    }

    for(auto& role : rolesDelta)
    {
        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
                deltaPos.get(), &DeltaDefense::vAddRobot);
        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
    }

}

PlayKickOffOpp::~PlayKickOffOpp()
{

}

QString PlayKickOffOpp::strName() const
{
    return QString("KickOffOpp");
}

void PlayKickOffOpp::vInitialize()
{
    const int allySide = fieldEnvironment->allies->iGetSide();

    for(auto& role : rolesDelta)
    {
        role->zone = deltaPos->getZone();
    }

    roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
                                 2e3);

    // Ocupa o campo inteiro
    roleChaser->zone.vConfigZone(QPoint(0, 0), 10e3);
    roleChaser->setDistanceToBall(globalConfig.safety.ball.stop);

    roleChaser->vSetKickOffMode(true);

    bInitialized = true;
}

bool PlayKickOffOpp::bCheckPreConditions()
{
    return true;
}

bool PlayKickOffOpp::bCheckEndConditions()
{
    const bool inPlay = bHasReferee() && referee->bInPlay();
    if(playStatus == FINISHED || inPlay)
    {
        playStatus = FINISHED;
        return true;
    }
    return false;
}

double PlayKickOffOpp::dGetScore() const
{
    return PlayKickOffOpp::dScore;
}

void PlayKickOffOpp::vUpdateScore()
{
    PlayKickOffOpp::dScore += 1.0;
}

void PlayKickOffOpp::vSaveScore()
{

}
