#ifndef PLAYKICKOFFOPP_H
#define PLAYKICKOFFOPP_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_KICKOFFOPP = 120 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayKickOffOpp : public Play
{
    Q_OBJECT
private:
    // Define os roles da play


    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QSharedPointer<RoleBallChaserFreeKick> roleChaser;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;

    static double dScore;

    QScopedPointer<DeltaDefense> deltaPos;


public:
    PlayKickOffOpp(AmbienteCampo *_fieldEnv);
    ~PlayKickOffOpp();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYKICKOFFOPP_H
