#include "playpenaltydefense.h"

double PlayPenaltyDefense::dScore = 0.0;

PlayPenaltyDefense::PlayPenaltyDefense(AmbienteCampo* _fieldEnv)
    : Play(_fieldEnv)
{
    iID = PLAY_PENALTY_DEFENSE;

    // Inicializa as roles
    roleGoalie.reset(new RoleNightyGoalie(_fieldEnv));
    bAddRole(roleGoalie.get());

    rolesOthers.reserve(globalConfig.robotsPerTeam - 1);
    for (qint8 n = 0; n < globalConfig.robotsPerTeam - 1; ++n)
    {
        rolesOthers.append(QSharedPointer<RolePenaltyCone>::create(_fieldEnv));
        rolesOthers.at(n)->bMakeDuplicate(n);

        bAddRole(rolesOthers.at(n).get());
    }

    bNormalStartIssued = false;

    // O atacante oponente tem 10 segundos para cobrar após o comando de normal
    // start, vamos dar 1 segundo de folga
    tmrPlayTimeout->setInterval(11 * 1000);
}

PlayPenaltyDefense::~PlayPenaltyDefense()
{
}

void PlayPenaltyDefense::vRun()
{
    // Só inicia a contagem quando vier o comando de normal start
    if (bNormalStartIssued == false)
    {
        tmrPlayTimeout->stop();
    }
}

QString PlayPenaltyDefense::strName() const
{
    return QString("Penalty Defense");
}

void PlayPenaltyDefense::vInitialize()
{
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const int allySide = fieldEnvironment->allies->iGetSide();
    const int ruleDistance = 1e3;
    QVector2D allyGoal = fieldEnvironment->geoCampo->vt2dGoalCenter(allySide);

#if ROBO_DIVISAO_A
    int ballDistanceFromGoal = 8e3;
#elif ROBO_DIVISAO_B
    int ballDistanceFromGoal = 6e3;
#endif

    // A altura é negativa por causa da forma que as coordenadas do Qt
    // funcionam
    QSize zoneSize(300, -globalConfig.robotsPerTeam * 300);
    // Pela regra, os robôs devem ficar a uma distância de 1m atrás da bola
    QPoint zoneTopLeft(allyGoal.x() +
                           opponentSide * (ballDistanceFromGoal + ruleDistance),
                       fieldEnvironment->geoCampo->szField().height() / 2);
    if (opponentSide == XNegativo)
    {
        zoneTopLeft.setX(zoneTopLeft.x() + allySide * zoneSize.width());
    }
    QRect zoneRect(zoneTopLeft, zoneSize);

    for (qint8 n = 0; n < rolesOthers.size(); ++n)
    {
        rolesOthers.at(n).get()->zone.vConfigZone(zoneRect);
    }

    roleGoalie->zone.vConfigZone(
        fieldEnvironment->geoCampo->retRestriction(allySide));
    roleGoalie->vSetGoalieMode(RoleNightyGoalie::PENALTY_MODE);

    bNormalStartIssued = false;
    bInitialized = true;
}

bool PlayPenaltyDefense::bCheckPreConditions()
{
    return true;
}

bool PlayPenaltyDefense::bCheckEndConditions()
{
    if (playStatus == FINISHED)
    {
        return true;
    }
    return false;
}

double PlayPenaltyDefense::dGetScore() const
{
    return PlayPenaltyDefense::dScore;
}

void PlayPenaltyDefense::vUpdateScore()
{
    PlayPenaltyDefense::dScore += 1.0;
}

void PlayPenaltyDefense::vSaveScore()
{
}

void PlayPenaltyDefense::vCheckPlayFinished()
{
    // Esta play só finaliza quando troca o comando, se houver ou não houver
    // gol vai vir um stop -> kickoff ou stop -> free kick
    if (playStatus != FINISHED && bRolesFinished() && roleGoalie->bIsFinished())
    {
        playStatus = FINISHED;
        tmrPlayTimeout->stop();
        qInfo() << QString("[Play %1] Finished!").arg(strName());
        vUpdateScore();
        vSaveScore();
    }
}

void PlayPenaltyDefense::vSetNormalStart()
{
    if (!bNormalStartIssued)
    {
        qInfo() << QString("[Play %1] Normal Start recebido!").arg(strName());
        bNormalStartIssued = true;
        tmrPlayTimeout->start();
    }
}
