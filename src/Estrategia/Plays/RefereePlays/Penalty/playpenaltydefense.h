#ifndef PLAYPENALTYDEFENSE_H
#define PLAYPENALTYDEFENSE_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"

enum
{
    PLAY_PENALTY_DEFENSE = 240 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayPenaltyDefense : public Play
{
    Q_OBJECT
private:
    // Define os roles da play
    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QVector<QSharedPointer<RolePenaltyCone>> rolesOthers;

    bool bNormalStartIssued;

    static double dScore;

    void vRun() override;

public:
    PlayPenaltyDefense(AmbienteCampo *_fieldEnv);
    ~PlayPenaltyDefense();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
    void vCheckPlayFinished() override;

    void vSetNormalStart();
};

#endif // PLAYPENALTYDEFENSE_H
