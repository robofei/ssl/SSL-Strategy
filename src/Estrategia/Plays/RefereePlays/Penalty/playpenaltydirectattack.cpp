#include "playpenaltydirectattack.h"

double PlayPenaltyDirectAttack::dScore = 0.0;

PlayPenaltyDirectAttack::PlayPenaltyDirectAttack(AmbienteCampo* _fieldEnv)
    : Play(_fieldEnv)
{
    iID = PLAY_PENALTY_DIRECT_ATTACK;

    // Inicializa os roles

    roleGoalie.reset(new RoleNightyGoalie(_fieldEnv));
    bAddRole(roleGoalie.get());

    roleAttacker.reset(new RolePenaltyAttacker(_fieldEnv));
    bAddRole(roleAttacker.get());
    roleAttacker->vSetPriority(Role::MAX_PRIORITY);

    rolesOthers.reserve(globalConfig.robotsPerTeam - 2);
    for (qint8 n = 0; n < globalConfig.robotsPerTeam - 2; ++n)
    {
        rolesOthers.append(QSharedPointer<RolePenaltyCone>::create(_fieldEnv));
        rolesOthers.at(n)->bMakeDuplicate(n);

        bAddRole(rolesOthers.at(n).get());
    }

    bNormalStartIssued = false;

    // O atacante tem 10 segundos para cobrar após o comando de normal start
    tmrPlayTimeout->setInterval(10 * 1000);
}

PlayPenaltyDirectAttack::~PlayPenaltyDirectAttack()
{
}

void PlayPenaltyDirectAttack::vRun()
{
    // Só inicia a contagem quando vier o comando de normal start
    if (bNormalStartIssued == false)
    {
        tmrPlayTimeout->stop();
    }
}

QString PlayPenaltyDirectAttack::strName() const
{
    return QString("Penalty Dir. Attack");
}

void PlayPenaltyDirectAttack::vInitialize()
{
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const int allySide = fieldEnvironment->allies->iGetSide();
    const int ruleDistance = 1e3;
    QVector2D opponentGoal =
        fieldEnvironment->geoCampo->vt2dGoalCenter(opponentSide);

#if ROBO_DIVISAO_A
    int ballDistanceFromGoal = 8e3;
#elif ROBO_DIVISAO_B
    int ballDistanceFromGoal = 6e3;
#endif

    // A altura é negativa por causa da forma que as coordenadas do Qt
    // funcionam
    QSize zoneSize(300, -globalConfig.robotsPerTeam * 300);
    // Pela regra, os robôs devem ficar a uma distância de 1m atrás da bola
    const int penaltyMark = opponentGoal.x() + allySide * ballDistanceFromGoal;
    QPoint zoneTopLeft(penaltyMark + allySide * ruleDistance,
                       fieldEnvironment->geoCampo->szField().height() / 2);
    if (allySide == XNegativo)
    {
        zoneTopLeft.setX(zoneTopLeft.x() + allySide * zoneSize.width());
    }
    QRect zoneRect(zoneTopLeft, zoneSize);

    for (qint8 n = 0; n < rolesOthers.size(); ++n)
    {
        rolesOthers.at(n).get()->zone.vConfigZone(zoneRect);
    }

    if (allySide == XPositivo)
    {
        roleGoalie->zone.vConfigZone(
            fieldEnvironment->geoCampo->retPositiveRestriction());
    }
    else
    {
        roleGoalie->zone.vConfigZone(
            fieldEnvironment->geoCampo->retNegativeRestriction());
    }

    roleAttacker->zone.vConfigZone(
        QPoint(penaltyMark + allySide * ruleDistance / 2.0, 0), 1e3);

    bNormalStartIssued = false;
    bInitialized = true;
}

bool PlayPenaltyDirectAttack::bCheckPreConditions()
{
    return true;
}

bool PlayPenaltyDirectAttack::bCheckEndConditions()
{
    if (playStatus == FINISHED)
    {
        return true;
    }
    return false;
}

double PlayPenaltyDirectAttack::dGetScore() const
{
    return PlayPenaltyDirectAttack::dScore;
}

void PlayPenaltyDirectAttack::vUpdateScore()
{
    PlayPenaltyDirectAttack::dScore += 1.0;
}

void PlayPenaltyDirectAttack::vSaveScore()
{
}

void PlayPenaltyDirectAttack::vSetNormalStart()
{
    if (!bNormalStartIssued)
    {
        qInfo() << QString("[Play %1] Normal Start recebido!").arg(strName());
        bNormalStartIssued = true;
        roleAttacker->vAttack();
        tmrPlayTimeout->start();
    }
}
