#ifndef PLAYPENALTYDIRECTATTACK_H
#define PLAYPENALTYDIRECTATTACK_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"

enum
{
    PLAY_PENALTY_DIRECT_ATTACK = 220 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayPenaltyDirectAttack : public Play
{
    Q_OBJECT
private:
    // Define os roles da play
    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QSharedPointer<RolePenaltyAttacker> roleAttacker;
    QVector<QSharedPointer<RolePenaltyCone>> rolesOthers;

    bool bNormalStartIssued;

    static double dScore;

    void vRun() override;

public:
    PlayPenaltyDirectAttack(AmbienteCampo *_fieldEnv);
    ~PlayPenaltyDirectAttack();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;

    void vSetNormalStart();
};

#endif // PLAYPENALTYDIRECTATTACK_H
