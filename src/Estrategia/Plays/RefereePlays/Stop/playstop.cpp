#include "playstop.h"

double PlayStop::dScore = 0.0;

PlayStop::PlayStop(AmbienteCampo *_fieldEnv) : Play(_fieldEnv)
{
    iID = PLAY_STOP;

    // Inicializa os roles
    const int robosDelta = 4;

    roleGoalie.reset(new RoleNightyGoalie(_fieldEnv));
    roleChaser.reset( new RoleBallChaserStop(_fieldEnv) );
    roleChaser->vSetPriority(Role::MAX_PRIORITY);

    rolesDelta.reserve(robosDelta);
    for(qint8 i=0; i<robosDelta; ++i)
    {
        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(_fieldEnv));
    }

    for(int i=1; i<rolesDelta.size(); ++i)
    {
        rolesDelta[i]->bMakeDuplicate(i);
    }

    deltaPos.reset( new DeltaDefense(_fieldEnv) );

    bAddRole(roleGoalie.get());
    bAddRole(roleChaser.get());
    for(auto& role : rolesDelta)
    {
        bAddRole(role.get());
        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
                deltaPos.get(), &DeltaDefense::vAddRobot);
        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
    }

    tmrPlayTimeout->setInterval(500 * 1000); // 8 segundos
}

PlayStop::~PlayStop()
{

}

QString PlayStop::strName() const
{
    return QString("Stop");
}

void PlayStop::vInitialize()
{
    const int allySide = fieldEnvironment->allies->iGetSide();

    for(auto& role : rolesDelta)
    {
        role->zone.vConfigZone(deltaPos->getZone());
    }

    roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
                                 2e3);

    // Ocupa o campo inteiro
    roleChaser->zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(), 2e3);
    roleChaser->setDistToBall(globalConfig.safety.ball.stop);

    // roleGoalie->vSetGoalieMode(RoleGoalie::STOP_MODE);

    bInitialized = true;
}

bool PlayStop::bCheckPreConditions()
{
    return true;
}

bool PlayStop::bCheckEndConditions()
{
    const bool inStop = bHasReferee() && referee->sslrefGetCommand() == SSL_Referee_Command_STOP;
    const bool inPlay = bHasReferee() && referee->bInPlay();
    if(playStatus == FINISHED || !inStop || inPlay)
    {
        playStatus = FINISHED;
        return true;
    }
    return false;
}

double PlayStop::dGetScore() const
{
    return PlayStop::dScore;
}

void PlayStop::vUpdateScore()
{
    PlayStop::dScore += 1.0;
}

void PlayStop::vSaveScore()
{

}
