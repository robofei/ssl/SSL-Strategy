#ifndef PLAYSTOP_H
#define PLAYSTOP_H

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_STOP = 80 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayStop : public Play
{
    Q_OBJECT

private:
    QSharedPointer<RoleNightyGoalie> roleGoalie;
    QSharedPointer<RoleBallChaserStop> roleChaser;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;

    static double dScore;

    QScopedPointer<DeltaDefense> deltaPos;

public:
    PlayStop(AmbienteCampo *_fieldEnv);
    ~PlayStop();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYSTOP_H
