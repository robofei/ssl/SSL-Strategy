#include "playstopdefensivo.h"

double PlayStopDefensivo::dScore = 0.0;

PlayStopDefensivo::PlayStopDefensivo(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_STOPDEFENSIVE;

    // Inicializa os roles
    const int robosDelta = 4;

    roleGoalie.reset(new RoleGoalie(_fieldEnv));
    roleChaser.reset( new RoleBallChaserStop(_fieldEnv) );
    roleChaser->vSetPriority(Role::MAX_PRIORITY);

    rolesDelta.reserve(robosDelta);
    for(qint8 i=0; i<robosDelta; ++i)
    {
        rolesDelta.append(QSharedPointer<RoleDeltaDefender>::create(_fieldEnv));
    }

    for(int i=1; i<rolesDelta.size(); ++i)
    {
        rolesDelta[i]->bMakeDuplicate(i);
    }

    deltaPos.reset( new DeltaDefense(_fieldEnv) );

    bAddRole(roleGoalie.get());
    bAddRole(roleChaser.get());
    for(auto& role : rolesDelta)
    {
        bAddRole(role.get());
    }

    for(auto& role : rolesDelta)
    {
        connect(role.get(), &RoleDeltaDefender::_vRoleAssigned,
                deltaPos.get(), &DeltaDefense::vAddRobot);
        connect(role.get(), &RoleDeltaDefender::_vRequestDefensePosition,
                deltaPos.get(), &DeltaDefense::vReceivePositionRequest_);
    }

}

PlayStopDefensivo::~PlayStopDefensivo()
{

}

QString PlayStopDefensivo::strName() const
{
    return QString("Stop Defensive");
}

void PlayStopDefensivo::vInitialize()
{
    const int allySide = fieldEnvironment->allies->iGetSide();

    for(auto& role : rolesDelta)
    {
        role->zone = deltaPos->getZone();
    }

    roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
                                 2e3);

    // Ocupa o campo inteiro
    roleChaser->zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(), 2e3);
    roleChaser->setDistToBall(globalConfig.safety.ball.stop);

    // roleGoalie->vSetGoalieMode(RoleGoalie::STOP_MODE);

    bInitialized = true;
}

bool PlayStopDefensivo::bCheckPreConditions()
{
    return true;
}

bool PlayStopDefensivo::bCheckEndConditions()
{
    const bool inStop = bHasReferee() && referee->sslrefGetCommand() == SSL_Referee_Command_STOP;
    const bool inPlay = bHasReferee() && referee->bInPlay();
    if(playStatus == FINISHED || !inStop || inPlay)
    {
        playStatus = FINISHED;
        return true;
    }
    return false;
}

double PlayStopDefensivo::dGetScore() const
{
    return PlayStopDefensivo::dScore;
}

void PlayStopDefensivo::vUpdateScore()
{
    PlayStopDefensivo::dScore += 1.0;
}

void PlayStopDefensivo::vSaveScore()
{

}

void PlayStopDefensivo::vCheckPlayFinished()
{
    if (playStatus != RUNNING)
    {
        playStatus = FINISHED;
        int timeLeft = tmrPlayTimeout->remainingTime();
        tmrPlayTimeout->stop();
        qInfo() << QString("[Play %1] Finished! Tempo restante = %2 s")
                       .arg(strName())
                       .arg(timeLeft / 1e3);
        vUpdateScore();
        vSaveScore();
    }
}
