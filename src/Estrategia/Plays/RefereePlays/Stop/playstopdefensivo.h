#ifndef PLAYSTOPDEFENSIVO_H
#define PLAYSTOPDEFENSIVO_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"

enum
{
    PLAY_STOPDEFENSIVE = 60 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayStopDefensivo : public Play
{
    Q_OBJECT
private:
    // Define os roles da play


    QSharedPointer<RoleGoalie> roleGoalie;
    QSharedPointer<RoleBallChaserStop> roleChaser;
    QVector<QSharedPointer<RoleDeltaDefender>> rolesDelta;

    static double dScore;

    QScopedPointer<DeltaDefense> deltaPos;

public:
    PlayStopDefensivo(AmbienteCampo *_fieldEnv);
    ~PlayStopDefensivo();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;

    void vCheckPlayFinished() override;
};

#endif // PLAYSTOPDEFENSIVO_H
