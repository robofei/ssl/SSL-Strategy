#include "playtimeout.h"

double PlayTimeout::dScore = 0.0;

PlayTimeout::PlayTimeout(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_TIMEOUT;

    // Inicializa os roles

    roleGoalie.reset(new RoleGoalie(_fieldEnv));

}

PlayTimeout::~PlayTimeout()
{

}

QString PlayTimeout::strName() const
{
    return QString("Timeout");
}

void PlayTimeout::vInitialize()
{

}

bool PlayTimeout::bCheckPreConditions()
{
    return true;
}

bool PlayTimeout::bCheckEndConditions()
{
    if(playStatus == FINISHED)
    {
        return true;
    }
    return false;
}

double PlayTimeout::dGetScore() const
{
    return PlayTimeout::dScore;
}

void PlayTimeout::vUpdateScore()
{
    PlayTimeout::dScore += 1.0;
}

void PlayTimeout::vSaveScore()
{

}
