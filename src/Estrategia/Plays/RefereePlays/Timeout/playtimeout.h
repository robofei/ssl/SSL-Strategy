#ifndef PLAYTIMEOUT_H
#define PLAYTIMEOUT_H
// é importante modificar o header guard para o nome do arquivo correspondente

#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"

enum
{
    PLAY_TIMEOUT = 140 // Consultar arquivo availableplays.h e pular 20 do último ID
};

class PlayTimeout : public Play
{
    Q_OBJECT
private:
    // Define os roles da play


    QSharedPointer<RoleGoalie> roleGoalie;

    static double dScore;

public:
    PlayTimeout(AmbienteCampo *_fieldEnv);
    ~PlayTimeout();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYTIMEOUT_H
