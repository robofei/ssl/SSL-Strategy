#include "playtesting.h"

double PlayTesting::dScore = 0.0;

PlayTesting::PlayTesting(AmbienteCampo *_fieldEnv) : Play(_fieldEnv)
{
    iID = PLAY_TESTING;

    roleAttacker.reset(new RoleAttacker(_fieldEnv));

    //     rolesMinesMidfielder.reserve(2);
    //     rolesMinesMidfielder.append(QSharedPointer<RoleMinesMidfielder>::create(_fieldEnv));
    //     rolesMinesMidfielder.append(QSharedPointer<RoleMinesMidfielder>::create(_fieldEnv));
    //
    //     rolesMinesAtacker.reserve(2);
    //     rolesMinesAtacker.append(QSharedPointer<RoleMinesAtacker>::create(_fieldEnv));
    //     rolesMinesAtacker.append(QSharedPointer<RoleMinesAtacker>::create(_fieldEnv));

    roleSupportA.reset(new RoleSupportAttacker(_fieldEnv));
    roleSupportB.reset(new RoleSupportAttacker(_fieldEnv));
    roleDeltaA.reset(new RoleDeltaDefender(_fieldEnv));
    roleDeltaB.reset(new RoleDeltaDefender(_fieldEnv));

    roleGoalie.reset(new RoleGoalie(_fieldEnv));

    // Primeira cópia
    roleSupportB->bMakeDuplicate(1);
    roleDeltaB->bMakeDuplicate(1);
    // Se tivessem 3 roles a próxima (role***C) seria: role***C->vMakeDuplicate(2)

    // Duplica os roles conforme necessário
//     for(int i=1; i<rolesMinesMidfielder.size(); ++i)
//     {
//         rolesMinesMidfielder[i]->vMakeDuplicate(i);
//     }
//     for(int i=1; i<rolesMinesAtacker.size(); ++i)
//     {
//         rolesMinesAtacker[i]->vMakeDuplicate(i);
//     }

    deltaDefense.reset(new DeltaDefense(_fieldEnv));

//     minesPosMidfielder.reset( new Mines(_fieldEnv) );
//     minesPosAtacker.reset( new Mines(_fieldEnv) );

    bAddRole(roleAttacker.get());
    bAddRole(roleSupportA.get());
    bAddRole(roleSupportB.get());
    bAddRole(roleDeltaA.get());
    bAddRole(roleDeltaB.get());

//     for(auto& role : rolesMinesMidfielder)
//     {
//         bAddRole(role.get());
//     }
//     for(auto& role : rolesMinesAtacker)
//     {
//         bAddRole(role.get());
//     }

    bAddRole(roleGoalie.get());

    // ==================== Comunicação entre os robôs ==================
    // Support A fala para o Atacante que ele vai receber um passe
    connect(roleSupportA.get(), &RoleSupportAttacker::_vRoleAssigned,
           roleAttacker.get(), &RoleAttacker::vUpdateReceiver_);
    // Support B fala pra o Support A que ele vai receber um passe
    connect(roleSupportB.get(), &RoleSupportAttacker::_vRoleAssigned,
           roleSupportA.get(), &RoleSupportAttacker::vUpdateReceiver_);
    // Atacante avisa o Support A onde vai ser o passe
    connect(roleAttacker.get(), &RoleAttacker::_vSendPassPoint,
           roleSupportA.get(), &RoleSupportAttacker::vSetPassPoint_);
    // Support A avisa para o Support B onde vai ser o passe
    connect(roleSupportA.get(), &RoleSupportAttacker::_vSendPassPoint,
           roleSupportB.get(), &RoleSupportAttacker::vSetPassPoint_);

    // Posicionamento do Delta
    connect(roleDeltaA.get(), &RoleDeltaDefender::_vRoleAssigned,
           deltaDefense.get(), &DeltaDefense::vAddRobot);
    connect(roleDeltaB.get(), &RoleDeltaDefender::_vRoleAssigned,
           deltaDefense.get(), &DeltaDefense::vAddRobot);

    connect(roleDeltaA.get(), &RoleDeltaDefender::_vRequestDefensePosition,
           deltaDefense.get(), &DeltaDefense::vReceivePositionRequest_);
    connect(roleDeltaB.get(), &RoleDeltaDefender::_vRequestDefensePosition,
           deltaDefense.get(), &DeltaDefense::vReceivePositionRequest_);

//     for(auto& role : rolesMinesMidfielder)
//     {
//         connect(role.get(), &RoleMinesMidfielder::_vRoleAssigned,
//                 minesPosMidfielder.get(), &Mines::vAddRobot);
//         connect(role.get(), &RoleMinesMidfielder::_vRequestDefensePosition,
//                 minesPosMidfielder.get(), &Mines::vReceivePositionRequest_);
//     }
//
//     for(auto& role : rolesMinesAtacker)
//     {
//         connect(role.get(), &RoleMinesAtacker::_vRoleAssigned,
//                 minesPosAtacker.get(), &Mines::vAddRobot);
//         connect(role.get(), &RoleMinesAtacker::_vRequestDefensePosition,
//                 minesPosAtacker.get(), &Mines::vReceivePositionRequest_);
//     }
}

PlayTesting::~PlayTesting()
{

}

QString PlayTesting::strName() const
{
    return QString("Testing");
}

void PlayTesting::vInitialize()
{
    //     minesPosMidfielder->vSetaParametrosNormalMeio(fieldEnvironment);
    //     minesPosAtacker->vSetaParametrosNormalAtaque(fieldEnvironment);

    const int allySide = fieldEnvironment->allies->iGetSide();
    roleAttacker->zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(),
            500);
    roleSupportA->zone.vConfigZone(QPoint(0,
                fieldEnvironment->geoCampo->szField().height()/2 - 2*globalConfig.robotDiameter),
            500);
    roleSupportB->zone.vConfigZone(QPoint(0.5e3,
                -fieldEnvironment->geoCampo->szField().height()/2 + 2*globalConfig.robotDiameter),
            500);
    roleDeltaA->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
            2e3);
    roleDeltaB->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
            2e3);
//     for(auto& role : rolesMinesMidfielder)
//     {
//         role->zone = minesPosMidfielder->getZone();
//     }
//
//     for(auto& role : rolesMinesAtacker)
//     {
//         role->zone = minesPosAtacker->getZone();
//     }

    roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
            2e3);

    bInitialized = true;
}

bool PlayTesting::bCheckPreConditions()
{
    return true;
}

bool PlayTesting::bCheckEndConditions()
{
    if(playStatus == FINISHED)
    {
        return true;
    }
    return false;
}

double PlayTesting::dGetScore() const
{
    return PlayTesting::dScore;
}

void PlayTesting::vUpdateScore()
{
    PlayTesting::dScore += 1.0;
}

void PlayTesting::vSaveScore()
{

}
