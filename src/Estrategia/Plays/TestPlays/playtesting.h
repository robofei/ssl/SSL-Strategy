#ifndef PLAYTESTING_H
#define PLAYTESTING_H


#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Delta/deltadefense.h"
#include "Posicionamento/Mines/mines.h"

enum
{
    PLAY_TESTING = 0
};

class PlayTesting : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleAttacker> roleAttacker;

    QSharedPointer<RoleSupportAttacker> roleSupportA;
    QSharedPointer<RoleSupportAttacker> roleSupportB;
    QSharedPointer<RoleDeltaDefender> roleDeltaA;
    QSharedPointer<RoleDeltaDefender> roleDeltaB;
    //     QVector<QSharedPointer<RoleMinesMidfielder>> rolesMinesMidfielder;
    //     QVector<QSharedPointer<RoleMinesAtacker>> rolesMinesAtacker;

    QSharedPointer<RoleGoalie> roleGoalie;

    static double dScore;

    QScopedPointer<DeltaDefense> deltaDefense;
//     QScopedPointer<Mines> minesPosAtacker;
//     QScopedPointer<Mines> minesPosMidfielder;

public:
    PlayTesting(AmbienteCampo *_fieldEnv);
    ~PlayTesting();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYTESTING_H
