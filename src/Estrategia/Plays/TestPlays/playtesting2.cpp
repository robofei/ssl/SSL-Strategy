#include "playtesting2.h"

double PlayTesting2::dScore = 0.0;

PlayTesting2::PlayTesting2(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = PLAY_TESTING2;

    roleAttacker.reset(new RoleAttacker(_fieldEnv));

    roleGoalie.reset(new RoleGoalie(_fieldEnv));

    markingPosMidfielder.reset( new Marking(_fieldEnv) );

    markingPosDefender.reset( new Marking(_fieldEnv) );

    bAddRole(roleAttacker.get());
//    bAddRole(roleSupportA.get());
//    bAddRole(roleSupportB.get());
//    bAddRole(roleDeltaA.get());
//    bAddRole(roleDeltaB.get());

    bAddRole(roleGoalie.get());
}

PlayTesting2::~PlayTesting2()
{

}

QString PlayTesting2::strName() const
{
    return QString("Testing");
}

void PlayTesting2::vInitialize()
{
    markingPosMidfielder->vSetMarkingRegion(Marking::MARKING_MIDFIELD);
    markingPosDefender->vSetMarkingRegion(Marking::MARKING_DEFENSE);

    const int allySide = fieldEnvironment->allies->iGetSide();

    roleAttacker->zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(),
                                   500);

    roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
                                 2e3);

    bInitialized = true;
}

bool PlayTesting2::bCheckPreConditions()
{
    return true;
}

bool PlayTesting2::bCheckEndConditions()
{
    if(playStatus == FINISHED)
    {
        return true;
    }
    return false;
}

double PlayTesting2::dGetScore() const
{
    return PlayTesting2::dScore;
}

void PlayTesting2::vUpdateScore()
{
    PlayTesting2::dScore += 1.0;
}

void PlayTesting2::vSaveScore()
{

}
