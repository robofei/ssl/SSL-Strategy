#ifndef PLAYTESTING2_H
#define PLAYTESTING2_H


#include "Estrategia/Plays/play.h"
#include "Estrategia/Roles/availableroles.h"
#include "Posicionamento/Marking/marking.h"

enum
{
    PLAY_TESTING2 = 20
};

class PlayTesting2 : public Play
{
    Q_OBJECT
private:
    QSharedPointer<RoleAttacker> roleAttacker;
    QSharedPointer<RoleGoalie> roleGoalie;

    static double dScore;

//    QScopedPointer<DeltaDefense> deltaDefense;
    QScopedPointer<Marking> markingPosDefender;
    QScopedPointer<Marking> markingPosMidfielder;

public:
    PlayTesting2(AmbienteCampo *_fieldEnv);
    ~PlayTesting2();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // PLAYTESTING2_H
