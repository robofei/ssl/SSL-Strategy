#ifndef AVAILABLEPLAYS_H
#define AVAILABLEPLAYS_H

#include "Estrategia/Plays/TestPlays/playtesting.h"                           // ID = 0
#include "Estrategia/Plays/TestPlays/playtesting2.h"                          // ID = 20
#include "Estrategia/Plays/RefereePlays/Halt/playhalt.h"                      // ID = 40
#include "Estrategia/Plays/RefereePlays/Stop/playstopdefensivo.h"             // ID = 60
#include "Estrategia/Plays/RefereePlays/Stop/playstop.h"                      // ID = 80
#include "Estrategia/Plays/RefereePlays/Kickoff/playkickoffally.h"            // ID = 100
#include "Estrategia/Plays/RefereePlays/Kickoff/playkickoffopp.h"             // ID = 120
#include "Estrategia/Plays/RefereePlays/Timeout/playtimeout.h"                // ID = 140
#include "Estrategia/Plays/RefereePlays/Kickoff/playkickoffallytake.h"        // ID = 160
#include "Estrategia/Plays/RefereePlays/Freekick/playfreekickopp.h"           // ID = 180
#include "Estrategia/Plays/RefereePlays/Freekick/playbasicfreekick.h"         // ID = 200
#include "Estrategia/Plays/RefereePlays/Penalty/playpenaltydirectattack.h"    // ID = 220
#include "Estrategia/Plays/RefereePlays/Penalty/playpenaltydefense.h"         // ID = 240
#include "Estrategia/Plays/RefereePlays/BallPlacement/playballplacementopp.h" // ID = 380
#include "Estrategia/Plays/NormalPlays/playdeflectkick.h"                     // ID = 400
#include "Estrategia/Plays/RefereePlays/BallPlacement/playballplacement.h"    // ID = 420
#include "Estrategia/Plays/NormalPlays/playindividualdefense.h"               // ID = 440
#include "Estrategia/Plays/NormalPlays/playpressuredefense.h"                 // ID = 460
#include "Estrategia/Plays/NormalPlays/playusingpassattack.h"                 // ID = 480
#include "Estrategia/Plays/NormalPlays/playadvanceball.h"                     // ID = 500
#include "Estrategia/Plays/NormalPlays/playgetballaway.h"                     // ID = 520
#include "Estrategia/Plays/NormalPlays/playshoottogoal.h"                     // ID = 540

#endif // AVAILABLEPLAYS_H
