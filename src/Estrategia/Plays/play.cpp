#include "play.h"
#include "qglobal.h"

Play::Play(AmbienteCampo* _fieldEnv, QObject* parent)
    : QObject(parent)
{
    iID = 8000;
    bInitialized = false;
    playStatus = STOPPED;
    bHasGoalie = false;
    goalie = nullptr;
    dLastAssignmentCost = 99999999.9;

    bRobotsRoleWarning = false;

    fieldEnvironment = _fieldEnv;
    referee = nullptr;

    tmrPlayTimeout.reset(new QTimer());
    connect(tmrPlayTimeout.get(), &QTimer::timeout, this, &Play::vPlayAbort_);

    playRoles.clear();
    iPlayIDs.clear();
}

void Play::vInitialize()
{
    vt2dInitialBallPositioning = fieldEnvironment->vt2dPosicaoBola();
    bInitialized = true;
}

void Play::vRun()
{
    // A princípio, não é para implementar nada aqui!!!
    return;
}

void Play::vResetAllRoles(bool _init)
{
    //     qInfo() << QString("[Play %1] Resetando roles...").arg(strName());
    bRobotsRoleWarning = false;
    QHashIterator<int, Role*> roleIterator(
        playRoles); // Cuidado!!! Iterator java style
    while (roleIterator.hasNext())
    {
        roleIterator.next();
        roleIterator.value()->vReset();
        if (_init)
        {
            roleIterator.value()->vInitialize();
        }
    }
    dLastAssignmentCost = 99999999.9;
}

void Play::vCheckPlayFinished()
{
    if (playStatus == RUNNING && bRolesFinished())
    {
        playStatus = FINISHED;
        int timeLeft = tmrPlayTimeout->remainingTime();
        tmrPlayTimeout->stop();
        qInfo() << QString("[Play %1] Finished Default! Tempo restante = %2 s")
                       .arg(strName())
                       .arg(timeLeft / 1e3);
        vUpdateScore();
        vSaveScore();
    }
}

int Play::id() const
{
    return iID;
}

bool Play::bMakeDuplicate(int duplicateNumber)
{
    //     Atualmente estamos usando mais de 20 ids diferentes para a PlayNormal
    if (duplicateNumber > 20)
    {
        qWarning()
            << QString(
                   "[Play %1] tentando fazer duplicada fora do intervalo entre"
                   " 0 e 20")
                   .arg(strName());
        return false;
    }

    iID += duplicateNumber;
    return true;
}

void Play::vSetReferee(Referee* _ref)
{
    referee = _ref;
}

bool Play::bHasReferee() const
{
    return referee != nullptr;
}

bool Play::bIsInitialized() const
{
    return bInitialized;
}

void Play::vReset()
{
    bInitialized = false;
    playStatus = STOPPED;

    bRobotsRoleWarning = false;
    dLastAssignmentCost = 99999999.9;

    vResetAllRoles(false);
    if (tmrPlayTimeout->isActive())
    {
        tmrPlayTimeout->stop();
    }
}

bool Play::bAddRole(Role* _role)
{
    bool ok = false, contains;
    if (_role != nullptr)
    {
        contains = playRoles.contains(_role->id());

        if (!contains && _role->id() != -1)
        {
            playRoles.insert(_role->id(), _role);
            qInfo() << QString("[Play %1] Role(%2 = %3) adicionada!")
                           .arg(strName())
                           .arg(_role->id())
                           .arg(_role->strName());

            if (ROLE_GOALIE == _role->iRoleType() ||
                ROLE_NIGHTY_GOALIE == _role->iRoleType())
            {
                bHasGoalie = true;
                goalie = _role;
            }
            ok = true;
            bRobotsRoleWarning = false;
        }
        else
        {
            Role* repeated = playRoles.value(_role->id());
            qWarning() << QString("[Play %1] Tentando adicionar role inválida, "
                                  "Role(%2 = %3) | Reason: Contains(%4)")
                              .arg(strName())
                              .arg(_role->id())
                              .arg(_role->strName())
                              .arg(contains);
            qWarning() << QString("\tRepeated Role(%2 = %3)")
                              .arg(repeated->id())
                              .arg(_role->strName());
        }
    }
    else
    {
        qWarning()
            << QString("[Play %1] Tentando adicionar role NULA").arg(strName());
    }
    return ok;
}

void Play::vClearRoles()
{
    playRoles.clear();
}

bool Play::bRolesFinished() const
{
    bool finished = true;
    QHashIterator<int, Role*> roleIterator(
        playRoles); // Cuidado!!! Iterator java style

    while (roleIterator.hasNext())
    {
        roleIterator.next();
        if (roleIterator.value()->bIsAssigned() &&
            roleIterator.value()->iRoleType() != ROLE_GOALIE &&
            roleIterator.value()->iRoleType() != ROLE_NIGHTY_GOALIE)
        {
            finished &= roleIterator.value()->bIsFinished();
        }
    }

    roleIterator.toFront();
    if (roleIterator.hasNext())
        roleIterator.next();
    if (playRoles.size() == 1 &&
        (roleIterator.value()->iRoleType() == ROLE_GOALIE ||
         roleIterator.value()->iRoleType() == ROLE_NIGHTY_GOALIE))
    {
        finished = false;
    }
    return finished;
}

QVector<qint8> Play::iCurrentlyUsedIDs()
{
    QVector<qint8> IDs;
    IDs.clear();

    QHashIterator<int, Role*> roleIterator(
        playRoles); // Cuidado!!! Iterator java style

    while (roleIterator.hasNext())
    {
        roleIterator.next();
        if (roleIterator.value()->bIsAssigned())
        {
            IDs.append(roleIterator.value()->player()->iID());
        }
    }

    return IDs;
}

void Play::vSetAvailableIDs(QVector<qint8> _ids)
{
    iPlayIDs.clear();
    iPlayIDs.append(_ids);
}

bool Play::bCheckPlayOK()
{
    if (!bInitialized)
    {
        qCritical()
            << QString("[Play %1] Não foi inicializada!").arg(strName());
        return false;
    }

    if (playRoles.isEmpty())
    {
        qWarning()
            << QString("[Play %1] Nenhuma role foi definida!").arg(strName());
        return false;
    }

    if (playStatus == STOPPED)
    {
        qInfo() << QString("[Play %1] Iniciada!").arg(strName());
        playStatus = RUNNING;
        if (tmrPlayTimeout->interval() == 0)
        {
            tmrPlayTimeout->start(DEFAULT_PLAY_TIMEOUT);
        }
        else
        {
            tmrPlayTimeout->start();
        }
    }
    return true;
}

void Play::vRunPlay()
{
    if (!bCheckPlayOK())
    {
        return;
    }

    vInitializeRoles();
    vOptimizeRoleAssignment();

    vRun();

    QHashIterator<int, Role*> roleIterator(
        playRoles); // Cuidado!!! Iterator java style
    while (roleIterator.hasNext())
    {
        roleIterator.next();
        if (roleIterator.value()->bIsAssigned())
        {
            roleIterator.value()->vRunRole();
        }
        else
        {
            roleIterator.value()->vShowAssignedWarning();
        }
    }

    if (bCheckEndConditions())
    {
        qInfo() << QString("[Play %1] Condição de fim atingida, Status = %2")
                       .arg(strName())
                       .arg(psGetPlayStatus());
    }
    vCheckPlayFinished();
}

void Play::vSetPlayTimeout(int _timeoutMs)
{
    tmrPlayTimeout->setInterval(_timeoutMs);
}

PLAY_STATUS Play::psGetPlayStatus() const
{
    return playStatus;
}

const Role* Play::getRoleToAssign(QVector<int>& _roleIDs)
{
    QHashIterator<int, Role*> roleIterator(
        playRoles); // Cuidado!!! Iterator java style
    // Não pode retornar a role de MAX_PRIORITY
    Role::ROLE_PRIORITY currentPriority = Role::MEDIUM_PRIORITY;

    while (roleIterator.hasNext())
    {
        roleIterator.next();

        if (roleIterator.value()->iRoleType() != ROLE_GOALIE &&
            roleIterator.value()->iRoleType() != ROLE_NIGHTY_GOALIE)
        {
            if (roleIterator.value()->priority() == currentPriority &&
                _roleIDs.contains(roleIterator.key()) == false)
            {
                _roleIDs.append(roleIterator.key());
                return roleIterator.value();
            }
        }

        if (roleIterator.hasNext() == false &&
            currentPriority != Role::OPTIONAL_PRIORITY)
        {
            roleIterator.toFront();
            currentPriority = static_cast<Role::ROLE_PRIORITY>(
                static_cast<int>(currentPriority) - 1);
        }
    }
    return nullptr;
}

QVector<int> Play::iSortedRoleIDs(const QVector<qint8>& robotIDs)
{
    QVector<int> roleIDs;

    const Role* role = getRoleToAssign(roleIDs);
    // Não coloca mais roles que robôs disponíveis
    // OBS: Podem ser utilizados mais robôs que roles disponíveis
    while (role != nullptr && roleIDs.size() < robotIDs.size())
    {
        role = getRoleToAssign(roleIDs);
    }

    // Se sobrou alguma role e ela não é opcional
    if (role != nullptr && roleIDs.size() != robotIDs.size())
    {
        if (role->priority() != Role::OPTIONAL_PRIORITY)
        {
            qWarning() << QString("[Play %1] Role(%2) com prioridade %3 não "
                                  "foi atribuída!")
                              .arg(strName())
                              .arg(role->strName())
                              .arg(role->strPriority());
        }
    }

    return roleIDs;
}

void Play::vOptimizeRoleAssignment()
{
    QVector<qint8> validIDs = iGetValidIDs();
    if (validIDs.isEmpty())
        return;

    Role* maxPriorRole = maxPriorityRole();
    const qint8 maxPriorRoleCount = maxPriorRole == nullptr ? 0 : 1;
    qint8 maxPriorRoleRobotId = -1;
    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();

    if (maxPriorRoleCount == 1)
    {
        if (!maxPriorRole->bIsAssigned())
        {
            maxPriorRoleRobotId = validIDs[0];
            for (int i = 1; i < validIDs.size(); ++i)
            {
                const qint8 id = validIDs[i];

                //                if (maxPriorRole->zone.fDistanceToZone(
                //                        fieldEnvironment->allies->getCPlayer(id)
                //                            ->vt2dPosicaoAtual()) <
                //                    maxPriorRole->zone.fDistanceToZone(
                //                        fieldEnvironment->allies
                //                            ->getCPlayer(maxPriorRoleRobotId)
                //                            ->vt2dPosicaoAtual()))
                if (ballPos.distanceToPoint(
                        fieldEnvironment->allies->getCPlayer(id)
                            ->vt2dPosicaoAtual()) <
                    ballPos.distanceToPoint(
                        fieldEnvironment->allies
                            ->getCPlayer(maxPriorRoleRobotId)
                            ->vt2dPosicaoAtual()))
                {
                    maxPriorRoleRobotId = id;
                }
            }
        }
        else
        {
            maxPriorRoleRobotId = maxPriorRole->player()->iID();
        }

        validIDs.removeOne(maxPriorRoleRobotId);
    }

    HungarianAlgorithm hungSolver(validIDs.size());

    if (playRoles.size() - bHasGoalie - maxPriorRoleCount > validIDs.size() &&
        !bRobotsRoleWarning)
    {
        qWarning()
            << QString(
                   "[Play %1] Quantidade de roles maior que o número de robôs")
                   .arg(strName());
        bRobotsRoleWarning = true;
    }

    QVector<int> roleIDs = iSortedRoleIDs(validIDs);

    for (int n = 0; n < validIDs.size(); ++n)
    {
        for (int i = 0; i < roleIDs.size(); ++i)
        {
            const Role* role = playRoles.value(roleIDs.at(i));
            const QVector2D robotPos =
                fieldEnvironment->allies->getPlayer(validIDs.at(n))
                    ->vt2dPosicaoAtual();
            hungSolver.vSetMatrixValue(n, i,
                                       role->zone.fDistanceToZone(robotPos));
        }
    }

    double assignmentCost = 99999999.9;
    QVector<Assignment> assignments = hungSolver.runAlgorithm(&assignmentCost);

    if (assignmentCost < dLastAssignmentCost * ASSIGNMENT_HYSTERESIS)
    {
        if (maxPriorRoleCount == 1)
        {
            maxPriorRole->vSetRobot(
                fieldEnvironment->allies->getPlayer(maxPriorRoleRobotId));
        }

        dLastAssignmentCost = assignmentCost;
        for (Assignment n : qAsConst(assignments))
        {
            if (n.used)
            {
                int id = validIDs.at(n.robot);
                if (!playRoles.value(roleIDs.at(n.task))
                         ->bIsAssigned()) // &&
                                          // (playRoles.value(roleIDs.at(n.task))->player()
                                          // != nullptr &&
                                          //  playRoles.value(roleIDs.at(n.task))->player()->iID()
                                          //  != id))
                {
                    playRoles.value(roleIDs.at(n.task))
                        ->vSetRobot(fieldEnvironment->allies->getPlayer(id));
                }
            }
        }
    }
}

void Play::vInitializeRoles()
{
    QHashIterator<int, Role*> roleIterator(
        playRoles); // Cuidado!!! Iterator java style
    const qint8 goalieID = fieldEnvironment->allies->iGetGoalieID();

    while (roleIterator.hasNext())
    {
        roleIterator.next();
        if (!roleIterator.value()->bIsInitialized())
        {
            roleIterator.value()->vInitialize();
            roleIterator.value()->vClearAssignedWarning();
        }
    }
    bool goalieChanged = bHasGoalie && (!goalie->bIsAssigned() ||
                                        (goalie->player()->iID() != goalieID));
    if (goalieChanged)
    {
        qInfo() << QString("[Play %1] Atribuindo o goleiro!").arg(strName());
        vResetAllRoles();
        goalie->vSetRobot(fieldEnvironment->allies->getPlayer(goalieID));
    }
}

QVector<qint8> Play::iGetValidIDs()
{
    QVector<qint8> validIDs;

    if (iPlayIDs.isEmpty())
    {
        qCritical()
            << QString("[Play %1] Nenhum robô atribuído").arg(strName());
        return validIDs;
    }

    for (qint8 id : qAsConst(iPlayIDs))
    {
        if (fieldEnvironment->allies->getPlayer(id)->bRoboDetectado() &&
            (!bHasGoalie || id != fieldEnvironment->allies->iGetGoalieID()))
        {
            validIDs.append(id);
        }
    }

    if (validIDs.isEmpty())
    {
        qCritical() << QString("[Play %1] Nenhum robô em campo").arg(strName());
    }
    return validIDs;
}

Role* Play::maxPriorityRole()
{
    Role* maxPriorRole = nullptr;
    for (Role* role : playRoles)
    {
        if (role->priority() == Role::MAX_PRIORITY)
        {
            if (maxPriorRole == nullptr)
            {
                maxPriorRole = role;
            }
            else
            {
                qCritical()
                    << QString(
                           "[Play %1] Mais de um robô com prioridade máxima")
                           .arg(strName());
                role->vSetPriority(Role::MEDIUM_PRIORITY);
            }
        }
    }

    return maxPriorRole;
}

int Play::iRemainingTime() const
{
    return tmrPlayTimeout->remainingTime();
}

const QVector2D& Play::vt2dGetInitialBallPositioning() const
{
    return vt2dInitialBallPositioning;
}

void Play::vPlayAbort_()
{
    qInfo() << QString("[Play %1] Abortada devido ao timeout T = %2 s!")
                   .arg(strName())
                   .arg(tmrPlayTimeout->interval() / 1e3);
    playStatus = ABORTED;
    tmrPlayTimeout->stop();
}
