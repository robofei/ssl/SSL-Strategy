#ifndef PLAY_H
#define PLAY_H

#include <QObject>
#include <QString>
#include <QHash>
#include <QHashIterator>
#include <QTimer>
#include <QSharedPointer>
#include <QVector>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

#include "Estrategia/Skills/skill.h"
#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/tactic.h"

#include "Estrategia/Roles/availableroles.h"
#include "qvector.h"

#define ASSIGNMENT_HYSTERESIS 0.7

/**
 * @brief Status possíveis de uma Play
 *
 * STOPPED -> Play ainda não foi iniciada.
 * RUNNING -> Play em andamento.
 * FINISHED -> Play executada e finalizada.
 * ABORTED -> Play foi iniciada e foi abortada (tempo ou condição de saída).
 */
typedef enum PLAY_STATUS
{
    STOPPED  = -1,
    RUNNING  =  0,
    FINISHED =  1,
    ABORTED  =  2
}PLAY_STATUS;

/**
 * @brief Tempo padrão para finalizar uma jogada em milissegundos.
 *
 * Valor atual = 120 segundos.
 */
#define DEFAULT_PLAY_TIMEOUT (int)(120 * 1000)

/**
 * @brief Classe genérica para implementação de uma jogada.
 *
 * Em uma Play é definido o quê cada robô irá fazer, ou seja, qual
 * Role que cada robô irá utilizar.
 */
class Play : public QObject
{
    Q_OBJECT
    /**
     * @brief Roles atribuídas à play.
     *
     * São todas as roles que a play tem que executar.
     */
    QHash<int, Role*> playRoles;

    /**
     * @brief Flag que controla o print de warning para a quantidade de robôs e
     * roles.
     */
    bool bRobotsRoleWarning;

    QVector2D vt2dInitialBallPositioning;

protected:
    int iID; /**< Número do ID da play. @note Deve ser único. */
    bool bInitialized; /**< Indica se a play foi inicializada. */
    PLAY_STATUS playStatus; /**< Status da play. @see PLAY_STATUS. */
    bool bHasGoalie; /**< Indica se a play possui a Role de Goalie. @see
                       RoleGoalie. */
    Role *goalie; /**< Referência para o goleiro, caso ele exista. */
    double dLastAssignmentCost; /**< Custo da atribuição das roles. Serve para
                                  otimizar a atribuição de qual robô executará
                                  cada role. */

    AmbienteCampo *fieldEnvironment; /**< Referência para o objeto com as
                                       informações do campo. */
    Referee *referee; /**< Referência para o objeto do referee. Opcional. */
    QScopedPointer<QTimer> tmrPlayTimeout; /**< Timer para abortar a Play caso
                                             passe muito tempo que ela está
                                             executando. */

    QVector<qint8> iPlayIDs; /**< IDs dos robôs que a Play pode manipular. */

    /**
     * @brief Pode ser implementado para adicionar alguma lógica na hora de
     * rodar a play.
     *
     * Este método é executado após inicializar as roles e atribuí-las.
     */
    virtual void vRun();

public:
    /**
     * @brief Construtor da play.
     *
     * @param _fieldEnv - Ponteiro para o ambiente do campo.
     * @param parent
     */
    explicit Play(AmbienteCampo *_fieldEnv, QObject *parent = nullptr);
    /**
     * @brief Destrutor da play.
     */
    virtual ~Play() { tmrPlayTimeout->stop(); };

    // virtual xxxx xxxxx() = 0; -> Método virtual puro, DEVE ser definido pela
    // sub classe
    // virtual xxxx xxxxx(); -> Método virtual, pode ser definido pela sub
    // classe para modificar o comportamento

    /**
     * @brief Deve retornar o nome da play.
     *
     * Este método é de implementação obrigatória.
     *
     * @return Nome da play.
     */
    virtual QString strName() const = 0;
    /**
     * @brief Faz as inicializações necessárias para a play funcionar.
     *
     * Este método é de implementação obrigatória.
     */
    virtual void vInitialize();
    /**
     * @brief Checa as pré-condições para começar a executar a play.
     *
     * Este método é de implementação obrigatória.
     *
     * @return true se todas as condições forem válidas.
     */
    virtual bool bCheckPreConditions() = 0;
    /**
     * @brief Checa as condições de fim da play.
     *
     * Caso as condições sejam verdadeiras a play é abortada. @see PLAY_STATUS.
     *
     * Este método é de implementação obrigatória.
     *
     * @return true se todas as condições forem válidas.
     */
    virtual bool bCheckEndConditions() = 0;
    /**
     * @brief Retorna o score da play.
     *
     * Este método é de implementação obrigatória.
     *
     * @return
     */
    virtual double dGetScore() const = 0;
    /**
     * @brief Checa se a play deve ser finalizada.
     *
     * Por padrão isso acontece se todas as roles estiverem finalizadas. Mas é
     * possível reescrever esse método para mudar este comportamento.
     */
    virtual void vCheckPlayFinished();
    /**
     * @brief Atualiza o score da Play.
     *
     * Este método é de implementação obrigatória.
     */
    virtual void vUpdateScore() = 0;
    /**
     * @brief Salva o score atual da Play.
     *
     * Este método é de implementação obrigatória.
     */
    virtual void vSaveScore() = 0;

    /**
     * @brief Retorna o iID da play.
     *
     * @return
     */
    int id() const;

    /**
     * @brief Incrementa o ID da Play para possibilitar que ela seja usada de
     * forma duplicada (a mesma Play ser usada mais de uma vez pelo mesmo
     * Coach).
     *
     * @param duplicateNumber - Quantidade para incrementar o ID.
     *
     * @return
     */
    bool bMakeDuplicate(int duplicateNumber);

    /**
     * @brief Atribui uma referência do Referee para a Play poder usar.
     *
     * @param _ref
     */
    void vSetReferee(Referee *_ref);

    /**
     * @brief Retorna se a Play possui um Referee para utilizar.
     *
     * @return
     */
    bool bHasReferee() const;

    /**
     * @brief Retorna se a play já foi inicializada.
     *
     * @return
     */
    bool bIsInitialized() const;
    /**
     * @brief Reseta a play.
     */
    void vReset();
    /**
     * @brief Adiciona uma Role para a Play executar.
     *
     * Não podem ser adicionadas roles repetidas (mesmo id).
     *
     * @param _role - Role para ser adicionada.
     *
     * @return true se deu tudo certo, false caso contrário.
     */
    bool bAddRole(Role *_role);
    /**
     * @brief Remove todas as roles adicionadas.
     */
    void vClearRoles();
    /**
     * @brief Checa se todas as roles estão finalizadas.
     *
     * @return true se as roles estiverem finalizadas.
     */
    bool bRolesFinished() const;

    /**
     * @brief Retorna a lista dos IDs dos robôs utilizados.
     *
     * @return
     */
    QVector<qint8> iCurrentlyUsedIDs();
    /**
     * @brief Seta a lista de IDs dos robôs que podem ser utilizados pela Play.
     *
     * @param _ids
     */
    void vSetAvailableIDs(QVector<qint8> _ids);

    /**
     * @brief Checa se todas as condições para executar a Play são válidas.
     *
     * @return
     */
    bool bCheckPlayOK();
    /**
     * @brief Executa a Play.
     *
     * Este método é executado internamente, ele que irá chamar o método vRun,
     * que é de implementação do programador.
     */
    void vRunPlay();

    /**
     * @brief Reseta as roles da play.
     *
     * @param _init - Define se é para inicializar as roles resetadas ou não.
     *
     * Usado quando uma play deixa de ser a play ativa, para garantir que
     * caso ela se torne ativa de novo ela incialize corretamente todas as
     * roles.
     */
    void vResetAllRoles(bool _init = true);

    /**
     * @brief Configura o timeout da play, isto é, o tempo máximo para ela ser
     * concluída.
     *
     * @param _timeoutMs
     */
    void vSetPlayTimeout(int _timeoutMs);
    /**
     * @brief Retorna o status da play. @see PLAY_STATUS.
     *
     * @return
     */
    PLAY_STATUS psGetPlayStatus() const;
    /**
     * @brief Retorna a próxima role de maior prioridade que ainda não foi
     * considerada para ser atribuída.
     *
     * @param _roleIDs - IDs das roles já consideradas para atribuição.
     *
     * @return
     */
    const Role* getRoleToAssign(QVector<int> &_roleIDs);
    /**
     * @brief Organiza os IDs das roles para atribuir com base na prioridade de
     * cada uma.
     *
     * @return
     */
    QVector<int> iSortedRoleIDs(const QVector<qint8>& robotIDs);
    /**
     * @brief Otimiza a atribuição dos robôs para as roles disponíveis.
     *
     * É importante que a quantidade de robôs seja maior ou igual a quantidade
     * de roles que a play tem que executar.
     */
    void vOptimizeRoleAssignment();
    /**
     * @brief Seta a role do goleiro, caso a play tenha uma.
     */
    void vInitializeRoles();
    /**
     * @brief Retorna os ids válidos dentro do iPlayIDs.
     *
     * Um ID é válido quando o robô está em campo e não é o goleiro.
     *
     * @return
     */
    QVector<qint8> iGetValidIDs();

    Role* maxPriorityRole();

    const QVector2D& vt2dGetInitialBallPositioning() const;

    int iRemainingTime() const;

private slots:
    /**
     * @brief Slot que recebe um sinal para abortar a Play. Geralmente o sinal
     * do tmrPlayTimeout.
     */
    void vPlayAbort_();
};

#endif // PLAY_H
