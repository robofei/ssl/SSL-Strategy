# STP

## Sobre a finalização das Skills, Tactics, Roles, Plays

- Ao implementar uma Skill, o programador deve implementar a forma como é
  detectado se a skill finalizou, por exemplo, no caso da Skill Go To, foi
  necessário implementar um `if` para checar se o robô já chegou na posição
  determinada, caso positivo, a flag `bFinished` é mudada para `true`.

- A não ser que isso mude no futuro, a Tactic determina automaticamente se já
  foi finalizada ou não (todas as Skills foram finalizadas).

- A não ser que isso mude no futuro, a Role determina automaticamente se já
  foi finalizada ou não (todas as Tactics foram finalizadas).

## Convenção para atribuir o ID de um Coach, Play, Role, Skill ou Tactic

O intervalo entre cada entidade (Coach, Play, etc) é de 8000 e o intervalo
entre sub-entidades é de 20, dessa forma, é possível existir até 20 entidades
repetidas mantendo um ID diferente para cada uma e podem existir 400
sub-entidades diferentes para cada entidade.

|       | Range Total (R) | Sub-classe 1 | Sub-classe 2 | Sub-classe n        |
|-------|-----------------|--------------|--------------|---------------------|
|Coach  | 0     a 7999    | R + 0 a 19   | R + 20 a 39  | R + 20(n-1) a 20n-1 |
|Play   | 8000  a 15999   | R + 0 a 19   | R + 20 a 39  | R + 20(n-1) a 20n-1 |
|Role   | 16000 a 23999   | R + 0 a 19   | R + 20 a 39  | R + 20(n-1) a 20n-1 |
|Tactic | 24000 a 31999   | R + 0 a 19   | R + 20 a 39  | R + 20(n-1) a 20n-1 |
|Skill  | 32000 a 39999   | R + 0 a 19   | R + 20 a 39  | R + 20(n-1) a 20n-1 |

Para que tudo funcione corretamente os seguintes passos devem ser seguidos:

- Incluir um atributo desse ao criar uma sub-entidade:

```cpp
public:
    static int iInstanceCount;
```

- Criar um enum (fora da classe) com o valor inicial do range:

```cpp
enum // Sub-classe 1
{
    COACH_TESTING = 0
}
// ou
enum // Sub-classe 2
{
    COACH_REFEREE = 20
}
```

- Inicializar o atributo no inicio do arquivo .cpp:

```cpp
int CoachTesting::iInstanceCount = 0;
```

- Incrementar o atributo no construtor da classe, por exemplo:

```cpp
iID = COACH_TESTING + CoachTesting::iInstanceCount;
CoachTesting::iInstanceCount++;
```

Para saber quantas sub-classes existem de uma determinada entidade consulte os
arquivos:

```shell
./Coach/availablecoaches.h
./Plays/availableplays.h
./Roles/availableroles.h
./Tactics/availabletactics.h
./Skills/availableskills.h
```
