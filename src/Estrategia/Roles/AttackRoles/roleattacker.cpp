#include "roleattacker.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

RoleAttacker::RoleAttacker(AmbienteCampo *_fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_ATTACKER;

    tacticShootTarget.reset(new TacticShootToTarget(_fieldEnv));
    bAddTactic(tacticShootTarget.get());

    tacticShootTarget->vSetApproachVelocity(2);
    tacticShootTarget->vSetEngageVelocity(0.5);

    iReceiverID = -1;
}

RoleAttacker::~RoleAttacker()
{

}

void RoleAttacker::vRun()
{
    if(bSetTactic(tacticShootTarget->id()) || currentTactic != nullptr)
    {
        if(iReceiverID != -1)
        {
            const Robo* receiver = fieldEnvironment->allies->getCPlayer(iReceiverID);
            QVector2D passPoint = receiver->vt2dPosicaoAtual() +
                globalConfig.robotDiameter/2.0 * receiver->vt2dOrientation().normalized();
            passPoint.setX(passPoint.x());

            tacticShootTarget->vShootToPoint(passPoint);

            const float passForce = Auxiliar::fForcaPasse(
                                        robot->vt2dPosicaoAtual(),
                                        passPoint,
                                        fieldEnvironment->geoCampo->szField());
            tacticShootTarget->vSetKickType(KICK_CUSTOM, passForce);

            if(tacticShootTarget->bAboutToShoot() &&
                    !tacticShootTarget->bIsFinished())
            {
                emit _vSendPassPoint(passPoint);
            }

            currentTactic->vRunTactic();
        }

        static bool printed = false;
        if(currentTactic->bIsFinished() && !printed)
        {
            printed = true;
            qInfo() << QString("[Role %1] Tactic %2(%3) finished! Robot[%4]").arg(strName())
                                                                             .arg(currentTactic->strName())
                                                                             .arg(currentTactic->id())
                                                                             .arg(robot->iID());
        }
    }
}

QString RoleAttacker::strName() const
{
    return QString("Attacker");
}

void RoleAttacker::vInitialize()
{
    bInitialized = true;
}

int RoleAttacker::iRoleType()
{
    return ROLE_ATTACKER;
}

void RoleAttacker::vUpdateReceiver_(qint8 _receiverID)
{
    if(_receiverID >= 0 && _receiverID < globalConfig.robotsPerTeam)
        iReceiverID = _receiverID;
}
