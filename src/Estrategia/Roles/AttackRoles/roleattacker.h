#ifndef ROLEATTACKER_H
#define ROLEATTACKER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_ATTACKER = 20
};

class RoleAttacker : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticShootToTarget> tacticShootTarget;

    qint8 iReceiverID;

public:
    RoleAttacker(AmbienteCampo *_fieldEnv);
    ~RoleAttacker();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;

signals:
    void _vSendPassPoint(const QVector2D _point);

public slots:
    void vUpdateReceiver_(const qint8 _receiverID);
};

#endif // ROLEATTACKER_H
