#include "roleattackers.h"

#define SET_TACTIC(tactic)                                                     \
    if (currentTactic == nullptr || currentTactic->id() != tactic->id())       \
    {                                                                          \
        bSetTactic(tactic->id());                                              \
    }

RoleAttackers::RoleAttackers(AmbienteCampo* _fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_ATTACKERS;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));
    tacticReceivePass.reset(new TacticReceivePass(_fieldEnv));
    // tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));

    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);

    bAddTactic(tacticMoveTo.get());
    bAddTactic(tacticShootToTarget.get());
    bAddTactic(tacticReceivePass.get());
    //    bAddTactic(tacticMoveTo.get());

    tacticShootToTarget->vSetKickType(KICK_CUSTOM, globalConfig.calibratedKickForce);
    tacticShootToTarget->vSetApproachVelocity(globalConfig.robotVelocities.linear.limit * 0.3);
    tacticShootToTarget->vSetEngageVelocity(0.2);

    // tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.angular.limit);
    // tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);

    id_shooter = -1;
    id_receiver = -1;
    id_passer = -1;
    id_recovering = -1;
    change_states = true;

    currentState = MOVING;
}

RoleAttackers::~RoleAttackers()
{
}

void RoleAttackers::vRun()
{
    // vUpdateStates();

    switch (currentState)
    {
    case SHOOT_MODE: {
        vShootToGoal();
    }
    break;

    case RECEIVE_PASS: {
        vReceivePass();
    }
    break;

    case SEND_PASS: {
        vSendPass();
    }
    break;

    case MOVING: {
        vMoveToBestPosition();
    }
    break;
    case RECOVER_BALL: {
        vRecoverBall();
    }
    break;
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

void RoleAttackers::vReceivePass()
{
    QVector2D receivePosition = robot->vt2dPosicaoAtual();
    SET_TACTIC(tacticReceivePass)
    tacticReceivePass->vSetPassPoint(receivePosition);
    tacticReceivePass->vSetAutomaticMode();

    if (tacticReceivePass->bIsFinished())
    {
        currentState = SHOOT_MODE;
    }
}

void RoleAttackers::vSendPass()
{
    // qDebug() << "[vSendPass()] Robot " << robot->iID() << "sending pass to
    // Robot " << receiver_pass;
    bSetTactic(tacticShootToTarget->id());
    tacticShootToTarget->vShootToRobot(id_receiver);
}

void RoleAttackers::vShootToGoal()
{
    if (currentTactic == nullptr ||
        (currentTactic != tacticShootToTarget.get() &&
         currentTactic->bIsFinished()))
    {
        SET_TACTIC(tacticShootToTarget)
        tacticShootToTarget->vShootToGoal();
    }
}

void RoleAttackers::vMoveToBestPosition()
{
    // qDebug() << "[vMoveToBestPosition()] Moving to a better position!";
    QVector2D pos = robot->vt2dPosicaoAtual();
    emit _vRequestAttackPosition(robot->iID(), pos);
    SET_TACTIC(tacticMoveTo)

    if ((id_shooter != -1 && id_shooter != robot->iID()) ||
        (id_passer != -1 && id_passer != robot->iID() && id_receiver != -1 &&
         id_receiver != robot->iID()))
    {
        pos.setX(0);
    }
    else
    {
        tacticMoveTo->vSetGotoPosition(pos);
    }
    tacticMoveTo->vIgnoreBall();
    tacticMoveTo->vSetLooktoPosition(fieldEnvironment->vt2dPosicaoBola());
}

void RoleAttackers::vRecoverBall()
{
    qint8 id_robot_near_ball = fieldEnvironment->iAchaRoboProximoBola();

    SET_TACTIC(tacticMoveTo)
    tacticMoveTo->vSetGotoPosition(
        robot->vt2dPlaceRollerAt(fieldEnvironment->vt2dPosicaoBola()));
    tacticMoveTo->vSetLooktoPosition(fieldEnvironment->vt2dPosicaoBola());
    tacticMoveTo->vIgnoreBall();
    // qDebug() << "[vRecoverBall()] Robot " << robot->iID() << " going to
    // recover the ball";
}

void RoleAttackers::vUpdateStates()
{
}

void RoleAttackers::_vReceiveShootSignal(qint8 _id)
{
    id_shooter = _id;
    id_receiver = -1;
    id_passer = -1;
    currentState = MOVING;
}

void RoleAttackers::_vReceivePassSignal(qint8 _id, qint8 _idReceiver)
{
    id_shooter = _id;

    if (_idReceiver == robot->iID())
    {
        currentState = RECEIVE_PASS;
    }
}

void RoleAttackers::_parseUpdate(qint8 _shooter, qint8 _receiver, qint8 _passer,
                                 qint8 _recovering)
{
    if (robot == nullptr)
        return;
    if (currentTactic != tacticReceivePass.get())
    {
        if (_shooter == robot->iID())
        {
            currentState = SHOOT_MODE;
        }
        else if (_receiver == robot->iID())
        {
            id_receiver = _receiver;
            id_passer = _passer;
            if (currentState != SHOOT_MODE)
            {
                currentState = RECEIVE_PASS;
            }
        }
        else if (_passer == robot->iID())
        {
            id_receiver = _receiver;
            id_passer = _passer;
            currentState = SEND_PASS;
        }
        else if (_recovering == robot->iID())
        {
            currentState = RECOVER_BALL;
        }
        else
        {
            currentState = MOVING;
        }
    }
}

void RoleAttackers::_vReceivePassRequest(qint8 _id, qint8 _idSupport)
{
    if (_idSupport == robot->iID())
    {
        qDebug() << "[vReceivePassRequest] Robot" << _idSupport
                 << "receiving pass request from " << _id;
        id_receiver = _id;
        currentState = SEND_PASS;
    }
}

QString RoleAttackers::strName() const
{

    switch (currentState)
    {
    case SHOOT_MODE: {
        return QString("NormalAttacker ShootMode");
    }
    break;

    case RECEIVE_PASS: {
        return QString("NormalAttacker ReceivePass");
    }
    break;
    case SEND_PASS: {
        return QString("NormalAttacker SendPass");
    }
    break;
    case MOVING: {
        return QString("NormalAttacker Moving");
    }
    break;
    case RECOVER_BALL: {
        return QString("NormalAttacker RecoverBall");
    }
    break;
    }
    return QString("NormalAttacker");
}

void RoleAttackers::vInitialize()
{
    bInitialized = true;
}

int RoleAttackers::iRoleType()
{
    return ROLE_ATTACKERS;
}

bool RoleAttackers::shooting() const
{
    return (currentState == SHOOT_MODE || currentState == SEND_PASS ||
            currentState == RECEIVE_PASS || tacticShootToTarget->bIsFinished());
}

// void RoleAttackers::vCheckRoleFinished()
//{
//     bFinished = false;
// }
