#ifndef ROLEATTACKERS_H
#define ROLEATTACKERS_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_ATTACKERS = 600
};

class RoleAttackers : public Role
{
    Q_OBJECT
public:
    RoleAttackers(AmbienteCampo* _fieldEnv);
    ~RoleAttackers();

    QString strName() const override;
    void vInitialize() override;
//    void vCheckRoleFinished() override;
    int iRoleType() override;
    bool shooting() const;
    qint8 id_shooter;
    qint8 id_receiver;
    qint8 id_passer;
    qint8 id_recovering;
    bool change_states;

private:
    void vRun() override;
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;
    QSharedPointer<TacticReceivePass> tacticReceivePass;
    QSharedPointer<TacticMoveToPose> tacticRecoverBall;

    enum STATES
    {
        SHOOT_MODE,
        SEND_PASS,
        RECEIVE_PASS,
        MOVING,
        RECOVER_BALL
    };
    STATES currentState;
    void vUpdateStates();

    void vReceivePass();
    void vSendPass();
    void vShootToGoal();
    void vMoveToBestPosition();
    void vRecoverBall();

    void vt2dFindBestPosition();
signals:
    void _vRequestAttackPosition(const qint8 _id, QVector2D& _pos);
    void _vRequestPass(qint8 _id, qint8 _idSupport);
    void _vSendShootSignal(qint8 _id);
    void _vSendPassSignal(qint8 _id, qint8 _idReceiver);

public slots:
    void _vReceivePassRequest(qint8 _id, qint8 _idSupport);
    void _vReceiveShootSignal(qint8 _id);
    void _vReceivePassSignal(qint8 _id, qint8 _idReceiver);
    void _parseUpdate(qint8 _shooter, qint8 _receiver, qint8 _passer, qint8 _recovering);
};

#endif // ROLEATTACKERS_H
