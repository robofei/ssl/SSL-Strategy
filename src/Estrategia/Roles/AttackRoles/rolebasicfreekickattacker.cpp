#include "rolebasicfreekickattacker.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "qglobal.h"

RoleBasicFreeKickAttacker::RoleBasicFreeKickAttacker(AmbienteCampo* _fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_BASIC_FREEKICK_ATTACKER;

    tacticReceivePass.reset(new TacticReceivePass(_fieldEnv));
    tacticShootTarget.reset(new TacticShootToTarget(_fieldEnv));

    bAddTactic(tacticReceivePass.get());
    bAddTactic(tacticShootTarget.get());

    currentState = RECEIVE_PASS;
    completedStates.RECEIVE_PASS = false;
    completedStates.SHOOT = false;
    iRemainingTime = 0;
}

RoleBasicFreeKickAttacker::~RoleBasicFreeKickAttacker()
{
}

void RoleBasicFreeKickAttacker::vRun()
{
    switch (currentState)
    {
    case RECEIVE_PASS: {
        // Enquanto possuir mais de 3s para fazer a Play tenta se
        // posicionar, senão, fica onde está.
        if (iRemainingTime > 3 * 1000)
        {
            tacticReceivePass->vSetStartingPosition(vt2dFindBestPosition());
        }
        else
        {
            tacticReceivePass->vSetStartingPosition(robot->vt2dPosicaoAtual());
        }

        if (currentTactic->id() != tacticReceivePass->id())
        {
            bSetTactic(tacticReceivePass->id());
        }

        if (tacticReceivePass->bIsFinished())
        {
            completedStates.RECEIVE_PASS = true;
            currentState = SHOOT;
            qInfo()
                << QString("[Role %1] State changed to SHOOT").arg(strName());
        }
    }
    break;

    case SHOOT: {
        //             tacticShootTarget->vSetApproachVelocity(1.5);
        tacticShootTarget->vSetEngageVelocity(0.3);
        tacticShootTarget->vShootToGoal();

        if (currentTactic->id() != tacticShootTarget->id())
        {
            bSetTactic(tacticShootTarget->id());
        }

        if (tacticShootTarget->bIsFinished())
        {
            completedStates.SHOOT = true;
        }
    }
    break;
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RoleBasicFreeKickAttacker::strName() const
{
    return QString("B.FreeK. Attacker");
}

void RoleBasicFreeKickAttacker::vInitialize()
{
    currentState = RECEIVE_PASS;
    tacticReceivePass->vSetStartingPosition(vt2dFindBestPosition());
    bSetTactic(tacticReceivePass->id());

    completedStates.RECEIVE_PASS = false;
    completedStates.SHOOT = false;

    bInitialized = true;
}

int RoleBasicFreeKickAttacker::iRoleType()
{
    return ROLE_BASIC_FREEKICK_ATTACKER;
}

QVector2D RoleBasicFreeKickAttacker::vt2dFindBestPosition()
{
    const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
    const float angleResolution = 5; // graus
    QVector2D bestPoint(0, 0);
    int bestInterval = 0;
    float minDistance = 3e3;
    QVector<QVector2D> freeInterval;
    QVector<QVector3D> opponents =
        fieldEnvironment->vt3dPegaPosicaoTodosObjetos(otOponente);
    double posRadiius = iDistanciaReceptorCobranca;
    QVector2D goalCenter = fieldEnvironment->geoCampo->vt2dGoalCenter(
        fieldEnvironment->opponents->iGetSide());

    for (int n = 0;
         bestPoint.distanceToPoint(QVector2D(0, 0)) <= 0 && posRadiius > 500 && n < 100;
         ++n)
    {
        // Reduz em 5% a cada interação que nao encontrar um ponto
        QVector2D testPoint = ballPos + QVector2D(posRadiius * pow(0.95, n), 0);
        for (int angle = 0; angle < 360; angle += angleResolution)
        {
            if (qAbs(testPoint.x()) < 3500)
            {
                if (Auxiliar::bChecaInterseccaoObjetosLinha(ballPos, testPoint,
                                                            globalConfig.robotDiameter * 2,
                                                            opponents) == false)
                {
                    if (fieldEnvironment->geoCampo->bIsInsideField(testPoint))
                    {
                        if (!fieldEnvironment->geoCampo
                                 ->bPontoDentroAreaRestricao(
                                     testPoint.toPoint(), XNegativo) &&
                            !fieldEnvironment->geoCampo
                                 ->bPontoDentroAreaRestricao(
                                     testPoint.toPoint(), XPositivo))
                        {
                            freeInterval = Auxiliar::vt2dIntervaloLivreGol(
                                testPoint, fieldEnvironment->allies->iGetSide(),
                                fieldEnvironment->geoCampo->szGoal().height(),
                                fieldEnvironment->geoCampo->szGoal().width(),
                                fieldEnvironment->geoCampo->szField(),
                                opponents);

                            if (freeInterval.size() > bestInterval &&
                                testPoint.distanceToPoint(goalCenter) <
                                    minDistance)
                            {
                                bestInterval = freeInterval.size();
                                bestPoint = testPoint;
                                minDistance =
                                    testPoint.distanceToPoint(goalCenter);
                            }
                        }
                    }
                }
            }

            testPoint =
                Auxiliar::vt2dRotaciona(ballPos, testPoint, angleResolution);
        }
        testPoint = ballPos + QVector2D(iDistanciaReceptorCobranca / 2.0, 0);
    }

    return bestPoint;
}

void RoleBasicFreeKickAttacker::vUpdateRemainingTime(int _time)
{
    iRemainingTime = _time;
}

void RoleBasicFreeKickAttacker::vSetPassPoint_(const QVector2D _point)
{
    tacticReceivePass->vSetPassPoint(_point);
}
