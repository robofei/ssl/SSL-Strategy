#ifndef ROLEBASICFREEKICKATTACKER_H
#define ROLEBASICFREEKICKATTACKER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_BASIC_FREEKICK_ATTACKER = 300
};

class RoleBasicFreeKickAttacker : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticReceivePass> tacticReceivePass;
    QSharedPointer<TacticShootToTarget> tacticShootTarget;

    enum STATES
    {
        RECEIVE_PASS,
        SHOOT
    };

    struct completedStates
    {
        bool RECEIVE_PASS;
        bool SHOOT;
    }completedStates;

    STATES currentState;
    int iRemainingTime; /**< Tempo restante para fazer a Play [ms]. */

public:
    RoleBasicFreeKickAttacker(AmbienteCampo *_fieldEnv);
    ~RoleBasicFreeKickAttacker();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;

    QVector2D vt2dFindBestPosition();
    void vUpdateRemainingTime(int _time);

public slots:
    void vSetPassPoint_(const QVector2D _point);
};

#endif // ROLEBASICFREEKICKATTACKER_H
