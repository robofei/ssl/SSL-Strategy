#include "rolebasicfreekicksupport.h"

RoleBasicFreeKickSupport::RoleBasicFreeKickSupport(AmbienteCampo *_fieldEnv) :
    Role(_fieldEnv)
{
    iID = ROLE_BASIC_FREEKICK_SUPPORT;

    tacticShootTarget.reset(new TacticShootToTarget(_fieldEnv));

    bAddTactic(tacticShootTarget.get());

    iReceiverID = -1;
}

RoleBasicFreeKickSupport::~RoleBasicFreeKickSupport()
{

}

void RoleBasicFreeKickSupport::vRun()
{
    if(bSetTactic(tacticShootTarget->id()) || currentTactic != nullptr)
    {
        if(iReceiverID != -1)
        {
            const Robo* receiver = fieldEnvironment->allies->getCPlayer(iReceiverID);
            tacticShootTarget->vShootToRobot(iReceiverID);

            const float passForce = Auxiliar::fForcaPasse(
                                        robot->vt2dPosicaoAtual(),
                                        receiver->vt2dDestino(),
                                        fieldEnvironment->geoCampo->szField());
            tacticShootTarget->vSetKickType(KICK_CUSTOM, 10);

            if(tacticShootTarget->bAboutToShoot())
            {
                emit _vSendPassPoint(receiver->vt2dDestino());
            }
        }
        else
        {
            tacticShootTarget->vShootToGoal();
        }

        if(currentTactic != nullptr)
        {
            currentTactic->vRunTactic();
        }
    }
}

QString RoleBasicFreeKickSupport::strName() const
{
    return QString("B.FreeK. Support");
}

void RoleBasicFreeKickSupport::vInitialize()
{
    tacticShootTarget->vSetEngageVelocity(0.3);
    bInitialized = true;
    iReceiverID  = -1;
}

int RoleBasicFreeKickSupport::iRoleType()
{
    return ROLE_BASIC_FREEKICK_SUPPORT;
}

void RoleBasicFreeKickSupport::vUpdateReceiver_(const qint8 _receiverID)
{
    if(_receiverID >= 0 && _receiverID < globalConfig.robotsPerTeam)
        iReceiverID = _receiverID;
}
