#ifndef ROLEBASICFREEKICKSUPPORT_H
#define ROLEBASICFREEKICKSUPPORT_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_BASIC_FREEKICK_SUPPORT = 320
};

class RoleBasicFreeKickSupport : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticShootToTarget> tacticShootTarget;

    qint8 iReceiverID;

public:
    RoleBasicFreeKickSupport(AmbienteCampo *_fieldEnv);
    ~RoleBasicFreeKickSupport();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;

signals:
    void _vSendPassPoint(const QVector2D _point);

public slots:
    void vUpdateReceiver_(const qint8 _receiverID);
};

#endif // ROLEBASICFREEKICKSUPPORT_H
