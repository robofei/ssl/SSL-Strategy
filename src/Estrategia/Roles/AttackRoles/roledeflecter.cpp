#include "roledeflecter.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

RoleDeflecter::RoleDeflecter(AmbienteCampo* _fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_DEFLECTER;
    iSupportID = -1;

    deflectKick.reset(new TacticDeflectKick(_fieldEnv));
    moveTo.reset(new TacticGoToLookTo(_fieldEnv));

    bAddTactic(deflectKick.get());
    bAddTactic(moveTo.get());
}

RoleDeflecter::~RoleDeflecter()
{
}

void RoleDeflecter::vRun()
{
    if (iSupportID != -1)
    {
        robot->vSetaIgnorarBola(bNaoConsideraBola);
        const int opponentSide = fieldEnvironment->opponents->iGetSide();
        const QVector2D zoneCenter = QVector2D(zone.ptZoneCenter()),
                        ballPos = fieldEnvironment->vt2dPosicaoBola(),
                        supportPos =
                            fieldEnvironment->allies->getCPlayer(iSupportID)
                                ->vt2dPosicaoAtual();
        QVector2D aim =
            fieldEnvironment->geoCampo->vt2dGoalCenter(opponentSide);
        aim.setY(robot->vt2dPosicaoAtual().y());
        if (currentTactic == nullptr)
        {
            bSetTactic(moveTo->id());
            // robot->vSetaIgnorarBola(bConsideraBola);
            moveTo->vSetGotoPosition(zoneCenter);
            moveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
            moveTo->vSetLooktoPosition(robot->vt2dPontoAnguloDestino());
            moveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
        }
        else if (moveTo->bIsFinished() &&
                 currentTactic->id() != deflectKick->id() &&
                 supportPos.distanceToPoint(ballPos) > 500)
        {
            bSetTactic(deflectKick->id());
        }
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RoleDeflecter::strName() const
{
    return QString("Deflecter");
}

void RoleDeflecter::vInitialize()
{
    iSupportID = -1;
    bInitialized = true;
}

int RoleDeflecter::iRoleType()
{
    return ROLE_DEFLECTER;
}

void RoleDeflecter::vReceiveSupportID_(const int _id)
{
    if (_id >= 0 && _id < globalConfig.robotsPerTeam)
    {
        iSupportID = _id;
        deflectKick->vSetKickerID(iSupportID);
    }
    else
    {
        qWarning() << QString("[Role %1] ID do robô suporte inválido! _id = %2")
                          .arg(strName())
                          .arg(_id);
    }
}

void RoleDeflecter::vReceiveProceedDeflect_()
{
    deflectKick->vProceedToDeflect();
}
