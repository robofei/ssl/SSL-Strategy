#ifndef ROLEDEFLECTER_H
#define ROLEDEFLECTER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_DEFLECTER = 480
};

class RoleDeflecter : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticGoToLookTo> moveTo;
    QSharedPointer<TacticDeflectKick> deflectKick;

    int iSupportID;

public:
    RoleDeflecter(AmbienteCampo *_fieldEnv);
    ~RoleDeflecter();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;

public slots:
    void vReceiveSupportID_(const int _id);
    void vReceiveProceedDeflect_();
};

#endif // ROLEDEFLECTER_H
