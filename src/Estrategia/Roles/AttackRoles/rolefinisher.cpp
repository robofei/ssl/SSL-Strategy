#include "rolefinisher.h"

RoleFinisher::RoleFinisher(AmbienteCampo* _fieldEnv) : Role{_fieldEnv}
{
    iID = ROLE_FINISHER;

    tacticReceivePass.reset(new TacticReceivePass(_fieldEnv));
    bAddTactic(tacticReceivePass.get());

    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));
    bAddTactic(tacticShootToTarget.get());
}

RoleFinisher::~RoleFinisher()
{
}

void RoleFinisher::vRun()
{
    if (!tacticReceivePass->bIsFinished())
    {
        if (bSetTactic(tacticReceivePass->id()) || currentTactic != nullptr)
        {
            tacticReceivePass->vSetStartingPosition(
                QVector2D(zone.ptZoneCenter()));

            if (tacticReceivePass->bIsFinished() && bFinished == false)
            {
                qInfo() << QString("[Role %1] Pass received").arg(strName());
            }
        }
    }
    else 
    {
        bSetTactic(tacticShootToTarget->id());
        tacticShootToTarget->vSetKickType(KICK_CUSTOM, globalConfig.calibratedKickForce);
        tacticShootToTarget->vShootToGoal();
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RoleFinisher::strName() const
{
    return QString("Finisher");
}

void RoleFinisher::vInitialize()
{
    tacticShootToTarget->vSetApproachVelocity(1.5);
    tacticShootToTarget->vSetEngageVelocity(0.5);

    bInitialized = true;
}

int RoleFinisher::iRoleType()
{
    return ROLE_FINISHER;
}

void RoleFinisher::vSetPassPoint_(const QVector2D _point)
{
    //     qInfo() << QString("[Role %1]:%2) Tenho que pegar um passe em (%3;
    //     %4)")
    //                .arg(strName())
    //                .arg(robot->iID())
    //                .arg(_point.x())
    //                .arg(_point.y());

    tacticReceivePass->vSetPassPoint(_point);
}
