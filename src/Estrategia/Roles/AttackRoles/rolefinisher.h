#ifndef ROLEFINISHER_H
#define ROLEFINISHER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_FINISHER = 660
};

class RoleFinisher : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticReceivePass> tacticReceivePass;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;

public:
    RoleFinisher(AmbienteCampo *_fieldEnv);
    ~RoleFinisher();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;

public slots:
    void vSetPassPoint_(const QVector2D _point);
};

#endif // ROLEFINISHER_H
