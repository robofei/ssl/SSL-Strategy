#include "rolefreekickshooter.h"

RoleFreeKickShooter::RoleFreeKickShooter(AmbienteCampo *_fieldEnv, const float velMax)
    : Role(_fieldEnv)
{
    iID = ROLE_FREEKICKSHOOTER;

    tacticShootTarget.reset(new TacticShootToTarget(_fieldEnv));
    bAddTactic(tacticShootTarget.get());

    tacticShootTarget->vSetApproachVelocity(velMax);
    tacticShootTarget->vSetEngageVelocity(0.5);

    receiverId = INVALID_ID;
}

RoleFreeKickShooter::~RoleFreeKickShooter()
{

}

void RoleFreeKickShooter::vRun()
{
    if(currentTactic == nullptr ||
        currentTactic->id() != tacticShootTarget->id())
    {
        bSetTactic(tacticShootTarget->id());
    }

    vChooseReceiver();

    if(receiverId == OPP_GOAL)
    {
        tacticShootTarget->vShootToGoal();
    }
    else
    {
        tacticShootTarget->vShootToRobot(receiverId);
    }

    if(currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

void RoleFreeKickShooter::vChooseReceiver()
{
    // Receptor já foi decidido
    if(receiverId != INVALID_ID)
    {
        return;
    }

    if(availableReceivers.isEmpty())
    {
        receiverId = OPP_GOAL;
    }
    else
    {
        const QList<qint8> idsList = availableReceivers.values();

        const int randIndex = QRandomGenerator::global()->bounded(idsList.size());
        receiverId = idsList[randIndex];
    }
}


void RoleFreeKickShooter::SLOT_addReceiver(qint8 id)
{
    availableReceivers.insert(id);
}

QString RoleFreeKickShooter::strName() const
{
    return QString("&FreeKickShooter");
}

void RoleFreeKickShooter::vInitialize()
{
    bInitialized = true;
}

int RoleFreeKickShooter::iRoleType()
{
    return ROLE_FREEKICKSHOOTER;
}

void RoleFreeKickShooter::vCheckRoleFinished()
{
    bFinished = false;
}
