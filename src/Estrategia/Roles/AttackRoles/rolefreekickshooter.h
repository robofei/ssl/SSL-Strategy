#ifndef ROLEFREEKICKSHOOTER_H
#define ROLEFREEKICKSHOOTER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_FREEKICKSHOOTER = 280
};

class RoleFreeKickShooter : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    void vChooseReceiver();

    // Tactics
    QSharedPointer<TacticShootToTarget> tacticShootTarget;

    // Variables
    qint8 receiverId; /**< -1 para chute ao gol oponente. */
    QSet<qint8> availableReceivers;

    enum RECEIVERS : qint8
    {
        INVALID_ID = -2,
        OPP_GOAL =   -1
    };

public:
    RoleFreeKickShooter(AmbienteCampo *_fieldEnv, const float velMax = globalConfig.robotVelocities.linear.limit);
    ~RoleFreeKickShooter();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;

public slots:
    void SLOT_addReceiver(qint8 id);
};

#endif // ROLEFREEKICKSHOOTER_H
