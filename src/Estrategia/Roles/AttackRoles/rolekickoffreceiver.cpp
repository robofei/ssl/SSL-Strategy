#include "rolekickoffreceiver.h"

RoleKickOffReceiver::RoleKickOffReceiver(AmbienteCampo *_fieldEnv) :
    Role(_fieldEnv),
    ySide( -1 )

{
    iID = ROLE_KICKOFFRECEIVER;

    tacticGoTo.reset(new TacticGoToLookTo(_fieldEnv));
    tacticGoTo->vSetVelocity(globalConfig.stopLinearVelocity);
    tacticGoTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
    bAddTactic(tacticGoTo.get());
}

RoleKickOffReceiver::~RoleKickOffReceiver()
{

}

void RoleKickOffReceiver::vRun()
{
    AmbienteCampo* env = fieldEnvironment;

    if(bSetTactic(tacticGoTo->id()) || currentTactic != nullptr)
    {
        tacticGoTo->vSetGotoPosition(vt2dGetDestination());
        tacticGoTo->vSetLooktoPosition(env->vt2dPosicaoBola());

        currentTactic->vRunTactic();
    }
}

void RoleKickOffReceiver::vSetYSide(int newYSide)
{
    ySide = newYSide;
}

QVector2D RoleKickOffReceiver::vt2dGetDestination() const
{
    const AmbienteCampo& env = *fieldEnvironment;

    return env.vt2dCoordenadaGenericaParaCampo(QVector2D(-0.2, 1.5*ySide));
}

QString RoleKickOffReceiver::strName() const
{
    return QString("&KickOffReceiver");
}

void RoleKickOffReceiver::vInitialize()
{
    bInitialized = true;
}

int RoleKickOffReceiver::iRoleType()
{
    return ROLE_KICKOFFRECEIVER;
}

void RoleKickOffReceiver::vCheckRoleFinished()
{
    bFinished = false;
}
