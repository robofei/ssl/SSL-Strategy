#ifndef ROLEKICKOFFRECEIVER_H
#define ROLEKICKOFFRECEIVER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_KICKOFFRECEIVER = 260
};

class RoleKickOffReceiver : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    // Tactics
    QSharedPointer<TacticGoToLookTo> tacticGoTo;

    // Variables
    int ySide;

public:
    RoleKickOffReceiver(AmbienteCampo *_fieldEnv);
    ~RoleKickOffReceiver();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;
    void vSetYSide(int newYSide);

    QVector2D vt2dGetDestination()const;
};

#endif // ROLEKICKOFFRECEIVER_H
