#include "rolenormalattacker.h"
// #include "Decisoes/decisoes.h"

#define SET_TACTIC(tactic)                        \
if(currentTactic == nullptr ||                    \
   currentTactic->id() != tactic->id())           \
{                                                 \
    bSetTactic(tactic->id());                     \
}                                                 \

int   RoleNormalAttacker::redecidePeriod            = 3000;
float RoleNormalAttacker::minimumPassDist           = 1500;
float RoleNormalAttacker::maximumPassDist           = 4500;
int   RoleNormalAttacker::minimumShootScoreRequired = 180;
int   RoleNormalAttacker::minimumPassScoreRequired  = 90;

RoleNormalAttacker::RoleNormalAttacker(AmbienteCampo *_fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_NORMALATTACKER;

    etmLastDecision.invalidate();

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    tacticShoot.reset(new TacticNormalShootToTarget(_fieldEnv));
    tacticIntercept.reset(new TacticReceivePassAlt(_fieldEnv));

    bAddTactic(tacticMoveTo.get());
    bAddTactic(tacticShoot.get());
    bAddTactic(tacticIntercept.get());

    currentState = MOVE_TO_BALL;
}

RoleNormalAttacker::~RoleNormalAttacker()
{

}

void RoleNormalAttacker::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;

    if(env.bBolaDentroAreaDefesaAliado())
    {
        currentState = MOVE_TO_BALL;
    }
    else if(env.bReceptorRecebendoPasse(player()->iID())){
        currentState = INTERCEPT_BALL;
    }

    switch(currentState)
    {
        case RoleNormalAttacker::MOVE_TO_BALL:
            moveToBall();
            break;
        case RoleNormalAttacker::INTERCEPT_BALL:
            interceptBall();
            break;
        case RoleNormalAttacker::SHOOT:
            shoot();
            break;
    }

    if(currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

void RoleNormalAttacker::moveToBall()
{
    SET_TACTIC(tacticMoveTo)

    const AmbienteCampo& env = *fieldEnvironment;
    const QVector2D ballPos = env.vt2dPosicaoBola();
    QVector2D destiny = ballPos + 200*(player()->vt2dPosicaoAtual() - ballPos).normalized();

    if(env.bBolaDentroAreaDefesaAliado())
    {
        destiny = ballPos;
        const QVector2D centerAlly = env.geoCampo->vt2dGoalCenter(env.allies->iGetSide());

        while(env.bPontoDentroAreaDefesaAliado(destiny, 2))
        {
            destiny += globalConfig.robotDiameter*(ballPos - centerAlly).normalized();
        }
    }
    else if(env.bPontoDentroAreaDefesaAliado(destiny))
    {
        // Se o destino for dentro da área, manda ele para cima da área e dane-se.
        //  Se ele não fizer isso, há muito risco da bola ficar muito próxima da área
        //  e nenhum aliado tirar ela de lá. Aí os oponentes conseguem facilmnte
        //  chutá-la ao gol.
        destiny = ballPos;
    }

    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vSetGotoPosition(destiny);
    tacticMoveTo->vSetLooktoPosition(ballPos);

    if(tacticMoveTo->bIsFinished())
    {
        currentState = SHOOT;
    }
}

void RoleNormalAttacker::interceptBall()
{
    SET_TACTIC(tacticIntercept)

    const AmbienteCampo& env = *fieldEnvironment;

    if(tacticIntercept->bIsFinished())
    {
        currentState = SHOOT;
    }
    else if(!env.bReceptorRecebendoPasse(player()->iID()))
    {
        currentState = MOVE_TO_BALL;
    }
}

void RoleNormalAttacker::shoot()
{
    SET_TACTIC(tacticShoot)

    const AmbienteCampo& env = *fieldEnvironment;

    if(!etmLastDecision.isValid() ||
       etmLastDecision.hasExpired(RoleNormalAttacker::redecidePeriod))
    {
        decide();
    }

    if( env.vt2dPosicaoBola().distanceToPoint(player()->vt2dPosicaoAtual()) > 500 ||
        env.bPontoDentroAreaDefesaAliado(player()->vt2dDestino()) )
    {
        currentState = MOVE_TO_BALL;
    }
}

void RoleNormalAttacker::decide()
{
    const AmbienteCampo& env = *fieldEnvironment;

    // Receptores proibidos são todos menos os receptores disponíveis
    QVector<int> receptoresProibidos;
    for(int id=0; id<globalConfig.robotsPerTeam; ++id)
    {
        const float distBall = env.allies->getCPlayer(id)->vt2dPosicaoAtual().distanceToPoint
                           (env.vt2dPosicaoBola());

        const float distAllyGoal = env.allies->getCPlayer(id)->vt2dPosicaoAtual().distanceToPoint
                               (env.geoCampo->vt2dGoalCenter(env.allies->iGetSide()));

        if(availableReceivers.contains(id) &&
           distBall > RoleNormalAttacker::minimumPassDist &&
           distBall < RoleNormalAttacker::maximumPassDist &&
           distAllyGoal > 4e3 &&
           env.regiaoCampoPonto(env.allies->getCPlayer(id)->vt2dPosicaoAtual()) != REGIAO_CAMPO_DEFESA)
        {
            continue;
        }

        receptoresProibidos.append(id);
    }

    // Avalia os receptores e a jogada de chute ao gol
//     Decisoes::vRealizaPrevisoes(
//         player()->vt2dPontoDirecaoMira(),
//         env.vt_vt3dPegaPosicaoDeTodosOsObjetosSemExcecao(otAliado),
//         env.vt2dPosicaoBola(),
//         env.vt_vt3dPegaPosicaoDeTodosOsObjetosSemExcecao(otOponente),
//         env.geoCampo->szField(),
//         env.allies->iGetSide(),
//         env.geoCampo->szGoal().height(),
//         env.geoCampo->vt2dGoalCenter(env.opponents->iGetSide()),
//         receptoresProibidos
//         );

    //            Decisoes::vPrintAvaliacoes();

//     const QVector<qint16> vtiMelhorReceptor =
//         Auxiliar::vtiMaiorValorESeuIndice(Decisoes::vtiAvalicoesReceptores());
//
//     const qint16 melhorNotaPasse = vtiMelhorReceptor[0];
//     const qint8 iIDReceptor = vtiMelhorReceptor[1];
//     const qint16 melhorNotaChute = Decisoes::iAvaliacaoChuteAoGol();
    const qint16 melhorNotaPasse = 0;
    const qint8 iIDReceptor = 0;
    const qint16 melhorNotaChute = 0;

    REGIAO_CAMPO regiaoBola = env.regiaoCampoPonto(env.vt2dPosicaoBola());

    // a bola está em nosso campo de ataque
    if(regiaoBola == REGIAO_CAMPO_ATAQUE)
    {
        // Se chutar for melhor do que passar a bola ou
        // se passar a bola é uma decisão muito ruim ou
        // se chutar a bola ao gol é uma ideia boa
        if(melhorNotaChute > melhorNotaPasse ||
            melhorNotaPasse < RoleNormalAttacker::minimumPassScoreRequired ||
            melhorNotaChute > RoleNormalAttacker::minimumShootScoreRequired)
        {
            tacticShoot->setGoal();
            emit SIGNAL_setReceiver(-1);
        }
        else
        {
            tacticShoot->setReceiver(iIDReceptor);
            emit SIGNAL_setReceiver(iIDReceptor);
        }
    }
    // a bola está em nosso campo de defesa ou meio
    else
    {
        // Se passar a bola for uma péssima opção, ele chuta para frente
        if(melhorNotaPasse < RoleNormalAttacker::minimumPassScoreRequired)
        {
            tacticShoot->setGoal();
            emit SIGNAL_setReceiver(-1);
        }
        else
        {
            tacticShoot->setReceiver(iIDReceptor);
            emit SIGNAL_setReceiver(iIDReceptor);
        }
    }

    etmLastDecision.restart();
}

QString RoleNormalAttacker::strName() const
{
    return QString("&NormalAttacker");
}

void RoleNormalAttacker::vInitialize()
{
    bInitialized = true;
}

int RoleNormalAttacker::iRoleType()
{
    return ROLE_NORMALATTACKER;
}

void RoleNormalAttacker::vCheckRoleFinished()
{
    bFinished = false;
}

void RoleNormalAttacker::vClearReceivers()
{
    availableReceivers.clear();
}

void RoleNormalAttacker::SLOT_updateAvailableReceivers(qint8 receiverId)
{
    availableReceivers.insert(receiverId);
}
