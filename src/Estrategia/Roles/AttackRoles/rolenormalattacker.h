#ifndef ROLENORMALATTACKER_H
#define ROLENORMALATTACKER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_NORMALATTACKER = 460
};

class RoleNormalAttacker : public Role
{
    Q_OBJECT

private:
    static int redecidePeriod; /**< Período em que o atacante "redecide" sua jogada caso
                                           ainda tenha a tenha finalizado [ms]. */
    static float minimumPassDist;
    static float maximumPassDist;
    static int minimumShootScoreRequired;
    static int minimumPassScoreRequired;
private:
    void vRun() override;

    void moveToBall();
    void interceptBall();
    void shoot();
    void decide();


    // Tactics
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    QSharedPointer<TacticNormalShootToTarget> tacticShoot;
    QSharedPointer<TacticReceivePassAlt> tacticIntercept;

    QSet<qint8> availableReceivers;
    QElapsedTimer etmLastDecision;

    enum STATES
    {
        MOVE_TO_BALL,
        INTERCEPT_BALL,
        SHOOT
    } currentState;

public:
    RoleNormalAttacker(AmbienteCampo *_fieldEnv);
    ~RoleNormalAttacker();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;

    void vClearReceivers();

public slots:
    void SLOT_updateAvailableReceivers(qint8 receiverId);

signals:
    void SIGNAL_setReceiver(qint8 receiverId);
};

#endif // ROLENORMALATTACKER_H
