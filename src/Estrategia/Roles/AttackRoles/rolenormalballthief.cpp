#include "rolenormalballthief.h"

#define SET_TACTIC(tactic)                        \
if(currentTactic == nullptr ||                    \
   currentTactic->id() != tactic->id())           \
{                                                 \
    bSetTactic(tactic->id());                     \
}                                                 \


RoleNormalBallThief::RoleNormalBallThief(AmbienteCampo *_fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_NORMALBALLTHIEF;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    tacticIntercept.reset( new TacticReceivePassAlt(_fieldEnv) );
    tacticSteal.reset(new TacticStealBall(_fieldEnv));
    tacticShoot.reset(new TacticNormalShootToTarget(_fieldEnv));

    bAddTactic(tacticMoveTo.get());
    bAddTactic(tacticIntercept.get());
    bAddTactic(tacticSteal.get());
    bAddTactic(tacticShoot.get());

    currentState = MOVE_TO_BLOCK;
}

RoleNormalBallThief::~RoleNormalBallThief()
{

}

void RoleNormalBallThief::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;
    const POSSE_DE_BOLA pdb =  env.pdbAchaTimeComPosseDeBola();

    if(env.bBolaDentroAreaDefesaOponente())
    {
        currentState = MOVE_TO_BLOCK;
    }

    else if(env.bReceptorRecebendoPasse(player()->iID()))
    {
        currentState = INTERCEPT_BALL;
    }
    else if(env.vt2dPosicaoBola().distanceToPoint(player()->vt2dPosicaoAtual()) > 400)
    {
        if(pdb == BOLA_ADVERSARIO)
            currentState = MOVE_TO_BLOCK;
        else
            currentState = STEAL_BALL;
    }

    switch(currentState) {
        case RoleNormalBallThief::MOVE_TO_BLOCK:
            moveToBlock();
            break;
        case RoleNormalBallThief::INTERCEPT_BALL:
            interceptBall();
            break;
        case RoleNormalBallThief::STEAL_BALL:
            stealBall();
            break;
        case RoleNormalBallThief::SHOOT_TO_FRONT:
            shootToFront();
            break;
    }

    if(currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

void RoleNormalBallThief::moveToBlock()
{
    SET_TACTIC(tacticMoveTo)

    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);

    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D allyGoal = env.geoCampo->vt2dGoalCenter(env.allies->iGetSide());
    const QVector2D ballPos = env.vt2dPosicaoBola();

    if(env.bBolaDentroAreaDefesaOponente())
    {
        qint8 idOpp = env.opponents->iGetGoalieID();
        QVector2D blockPos = allyGoal;
        QVector2D opponentPos = env.opponents->getPlayer(idOpp)->vt2dPosicaoAtual();
        QVector2D opponentAim = env.opponents->getPlayer(idOpp)->vt2dPontoDirecaoMira();

        // Se o robô oponente estiver próximo da bola e mirando nela
        if(opponentPos.distanceToPoint(ballPos) < 300 &&
            Auxiliar::dAnguloVetor1Vetor2(opponentAim - opponentPos,
                                          ballPos - opponentPos) < 20)
        {
            blockPos = (opponentAim - opponentPos) + ballPos;
        }

        QVector2D destiny = ballPos;

        do
        {
            destiny +=  (blockPos - ballPos).normalized()*100;
        }
        while(env.bPontoAreaDefesa(destiny, 1.5) && env.allies->iGetSide() * destiny.x() < 0);

        tacticMoveTo->vSetGotoPosition(destiny);
        tacticMoveTo->vSetLooktoPosition(ballPos);
    }
    else
    {
        const qint8 closestOppId = env.iAchaInimigoProximoBola();
        const QVector2D closestOpp = (closestOppId == -1) ? QVector2D(9e5, 0) :
                                         env.opponents->getCPlayer(closestOppId)->vt2dPosicaoAtual();

        const QVector4D vt4dDestino = Auxiliar::vt4dPosicaoLadraoDeBola(
            player()->vt2dPosicaoAtual(), closestOpp, ballPos, allyGoal
            );

        tacticMoveTo->vSetGotoPosition(QVector2D(vt4dDestino.x(), vt4dDestino.y()));
        tacticMoveTo->vSetLooktoPosition(QVector2D(vt4dDestino.z(), vt4dDestino.w()));

        if(tacticMoveTo->bIsFinished())
        {
            currentState = STEAL_BALL;
        }
    }

}

void RoleNormalBallThief::interceptBall()
{
    SET_TACTIC(tacticIntercept)

    const AmbienteCampo& env = *fieldEnvironment;

    if(tacticIntercept->bIsFinished())
    {
        currentState = SHOOT_TO_FRONT;
    }
    else if(!env.bReceptorRecebendoPasse(player()->iID()))
    {
        currentState = MOVE_TO_BLOCK;
    }
}

void RoleNormalBallThief::stealBall()
{
    SET_TACTIC(tacticSteal)

    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();
    const QVector2D currentPos = robot->vt2dPosicaoAtual();

    const QVector2D destiny = currentPos + 300*(currentPos - ballPos).normalized();

    tacticSteal->getSkillPullBall().setVelocity(globalConfig.robotVelocities.linear.limit*.3);
    tacticSteal->getSkillPullBall().setMinimumDistToAllyGoal(2e3);
    tacticSteal->getSkillPullBall().setPullingDist(fDistanciaParaChutePuxadinha);
    tacticSteal->getSkillPullBall().setTarget(destiny);

    if(tacticSteal->bIsFinished())
    {
        currentState = SHOOT_TO_FRONT;
    }
}

void RoleNormalBallThief::shootToFront()
{
    SET_TACTIC(tacticShoot)

    tacticShoot->setGoal();


    if(tacticShoot->bIsFinished())
    {
        currentState = MOVE_TO_BLOCK;
    }
}

QString RoleNormalBallThief::strName() const
{
    return QString("&NormalBallThief");
}

void RoleNormalBallThief::vInitialize()
{
    bInitialized = true;
}

int RoleNormalBallThief::iRoleType()
{
    return ROLE_NORMALBALLTHIEF;
}

void RoleNormalBallThief::vCheckRoleFinished()
{
//    bFinished = currentState == FINISHED;
}
