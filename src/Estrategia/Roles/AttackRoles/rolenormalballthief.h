#ifndef ROLENORMALBALLTHIEF_H
#define ROLENORMALBALLTHIEF_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_NORMALBALLTHIEF = 380
};

class RoleNormalBallThief : public Role
{
    Q_OBJECT
private:
    void vRun() override;

    void moveToBlock();
    void interceptBall();
    void stealBall();
    void shootToFront();

    // Tactics
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    QSharedPointer<TacticReceivePassAlt> tacticIntercept;
    QSharedPointer<TacticStealBall> tacticSteal;
    QSharedPointer<TacticNormalShootToTarget> tacticShoot;

    enum STATES
    {
        MOVE_TO_BLOCK,
        INTERCEPT_BALL,
        STEAL_BALL,
        SHOOT_TO_FRONT
    } currentState;

public:
    RoleNormalBallThief(AmbienteCampo *_fieldEnv);
    ~RoleNormalBallThief();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;
};

#endif // ROLENORMALBALLTHIEF_H
