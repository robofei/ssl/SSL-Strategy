#include "rolepenaltyattacker.h"

RolePenaltyAttacker::RolePenaltyAttacker(AmbienteCampo* _fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_PENALTY_ATTACKER;
    tacticGoTo.reset(new TacticGoToLookTo(_fieldEnv));
    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));

    tacticGoTo->vSetVelocity(globalConfig.robotVelocities.linear.limit * 0.8);
    tacticGoTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);

    bAddTactic(tacticGoTo.get());
    bAddTactic(tacticShootToTarget.get());
}

RolePenaltyAttacker::~RolePenaltyAttacker()
{
}

void RolePenaltyAttacker::vRun()
{
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    QVector2D opponentGoal =
                  fieldEnvironment->geoCampo->vt2dGoalCenter(opponentSide),
              ballPos = fieldEnvironment->vt2dPosicaoBola();

    switch (currentState)
    {
    case WAIT_FOR_NORMAL: {
        if (currentTactic->id() != tacticGoTo->id())
        {
            bSetTactic(tacticGoTo->id());
        }
        tacticGoTo->vSetGotoPosition(QVector2D(zone.ptZoneCenter()));
        tacticGoTo->vSetLooktoPosition(
            fieldEnvironment->geoCampo->vt2dGoalCenter(opponentSide));
    }
    break;

    case MOVE_BALL_TO_GOAL: {
        vMoveBallToGoal();
    }
    break;

    case SHOOT: {
        if (currentTactic->id() != tacticShootToTarget->id())
        {
            bSetTactic(tacticShootToTarget->id());
        }
        tacticShootToTarget->vShootToGoal();

        if (ballPos.distanceToPoint(opponentGoal) > 4.5e3)
        {
            tacticShootToTarget->vSetKickType(KICK_CUSTOM, 6);
        }
        else
        {
            tacticShootToTarget->vSetKickType(KICK_CUSTOM,
                                              globalConfig.calibratedKickForce);
        }
    }
    break;
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

void RolePenaltyAttacker::vMoveBallToGoal()
{
    if (currentTactic->id() != tacticShootToTarget->id())
    {
        bSetTactic(tacticShootToTarget->id());
    }
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const QSize defenseArea = fieldEnvironment->geoCampo->szDefenseArea();
    const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola(),
                    opponentGoal = fieldEnvironment->geoCampo->vt2dGoalCenter(
                        opponentSide);
    const bool ballCloseToGoal =
        ballPos.distanceToPoint(opponentGoal) < (defenseArea.width() + 1e3);

    if (ballCloseToGoal)
    {
        tacticShootToTarget->vResetTactic();
        tacticShootToTarget->vInitialize();
        currentState = SHOOT;
    }
    else
    {
        if (tacticShootToTarget->bIsFinished())
        {
            tacticShootToTarget->vResetTactic();
            tacticShootToTarget->vInitialize();
        }

        QVector2D direction = (QVector2D(0, 0) - opponentGoal).normalized();
        tacticShootToTarget->vShootToPoint(
            opponentGoal + direction * (defenseArea.width() + 500));
        tacticShootToTarget->vSetKickType(KICK_CUSTOM,
                                          globalConfig.calibratedKickForce / 2.0);
    }
}

QString RolePenaltyAttacker::strName() const
{
    return QString("Penalty Attacker");
}

void RolePenaltyAttacker::vInitialize()
{
    currentState = WAIT_FOR_NORMAL;
    bSetTactic(tacticGoTo->id());

    bInitialized = true;
}

int RolePenaltyAttacker::iRoleType()
{
    return ROLE_PENALTY_ATTACKER;
}

void RolePenaltyAttacker::vCheckRoleFinished()
{
    if (bFinished == false && bTacticsFinished() && currentState == SHOOT)
    {
        bFinished = true;
    }
}

void RolePenaltyAttacker::vAttack()
{
    if (currentState == WAIT_FOR_NORMAL)
    {
        qInfo() << QString("[Role %1] Normal Start recebido!").arg(strName());
        currentState = MOVE_BALL_TO_GOAL;
    }
}
