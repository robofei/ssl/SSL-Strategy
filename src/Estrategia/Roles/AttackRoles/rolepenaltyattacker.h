#ifndef ROLEPENALTYATTACKER_H
#define ROLEPENALTYATTACKER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_PENALTY_ATTACKER = 360
};

class RolePenaltyAttacker : public Role
{
    Q_OBJECT

private:
    void vRun() override;
    // Tactics
    QSharedPointer<TacticGoToLookTo> tacticGoTo;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;

    enum STATES
    {
        WAIT_FOR_NORMAL,
        MOVE_BALL_TO_GOAL,
        SHOOT
    };
    STATES currentState;

    void vMoveBallToGoal();

public:
    RolePenaltyAttacker(AmbienteCampo *_fieldEnv);
    ~RolePenaltyAttacker();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;

    void vAttack();
};

#endif // ROLEPENALTYATTACKER_H
