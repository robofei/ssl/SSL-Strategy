#include "rolereceiver.h"

RoleReceiver::RoleReceiver(AmbienteCampo *_fieldEnv) : Role{_fieldEnv}
{
    iID = ROLE_RECEIVER;

    tacticReceivePass.reset(new TacticReceivePass(_fieldEnv));
    bAddTactic(tacticReceivePass.get());
}

RoleReceiver::~RoleReceiver()
{

}


void RoleReceiver::vRun() 
{
    if(bSetTactic(tacticReceivePass->id()) || currentTactic != nullptr)
    {
        tacticReceivePass->vSetStartingPosition(QVector2D(zone.ptZoneCenter()));

        if (tacticReceivePass->bIsFinished() && bFinished == false)
        {
            qInfo() << QString("[Role %1] Pass received")
                           .arg(strName());
        }

        currentTactic->vRunTactic();
    }
}

QString RoleReceiver::strName() const 
{
    return QString("Receiver");
}

void RoleReceiver::vInitialize() 
{
    bInitialized = true;
}

int RoleReceiver::iRoleType() 
{
    return ROLE_RECEIVER;
}

void RoleReceiver::vSetPassPoint_(const QVector2D _point)
{
    //     qInfo() << QString("[Role %1]:%2) Tenho que pegar um passe em (%3;
    //     %4)")
    //                .arg(strName())
    //                .arg(robot->iID())
    //                .arg(_point.x())
    //                .arg(_point.y());

    tacticReceivePass->vSetPassPoint(_point);
}
