#ifndef ROLERECEIVER_H
#define ROLERECEIVER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_RECEIVER = 640
};

class RoleReceiver : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticReceivePass> tacticReceivePass;

public:
    RoleReceiver(AmbienteCampo *_fieldEnv);
    ~RoleReceiver();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;

public slots:
    void vSetPassPoint_(const QVector2D _point);
};

#endif // ROLERECEIVER_H
