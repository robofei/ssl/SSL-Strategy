#include "rolesupportattacker.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

RoleSupportAttacker::RoleSupportAttacker(AmbienteCampo* _fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_SUPPORT_ATTACKER;

    iReceiverID = -1;

    tacticReceivePass.reset(new TacticReceivePass(_fieldEnv));
    tacticShootTarget.reset(new TacticShootToTarget(_fieldEnv));

    bAddTactic(tacticReceivePass.get());
    bAddTactic(tacticShootTarget.get());

    currentState = RECEIVE_PASS;
    completedStates.RECEIVE_PASS = false;
    completedStates.SHOOT = false;
}

RoleSupportAttacker::~RoleSupportAttacker()
{
}

void RoleSupportAttacker::vRun()
{
    switch (currentState)
    {
    case RECEIVE_PASS: {
        if (currentTactic->id() != tacticReceivePass->id())
        {
            bSetTactic(tacticReceivePass->id());
        }

        if (tacticReceivePass->bIsFinished())
        {
            completedStates.RECEIVE_PASS = true;
            currentState = SHOOT;
            qInfo()
                << QString("[Role %1] State changed to SHOOT").arg(strName());
        }
    }
    break;

    case SHOOT: {
        vShoot();
        if (currentTactic->id() != tacticShootTarget->id())
        {
            bSetTactic(tacticShootTarget->id());
        }

        if (tacticShootTarget->bIsFinished())
        {
            completedStates.SHOOT = true;
        }
    }
    break;
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

void RoleSupportAttacker::vShoot()
{
    tacticShootTarget->vSetApproachVelocity(1.5);
    tacticShootTarget->vSetEngageVelocity(1.0);
    if (iReceiverID == -1)
    {
        tacticShootTarget->vShootToGoal();
    }
    else
    {
        const Robo* receiver =
            fieldEnvironment->allies->getCPlayer(iReceiverID);
        vt2dMakePassPoint =
            receiver->vt2dPosicaoAtual() +
            globalConfig.robotDiameter / 2.0 * receiver->vt2dOrientation().normalized();
        vt2dMakePassPoint.setX(vt2dMakePassPoint.x() + 200);

        tacticShootTarget->vShootToPoint(vt2dMakePassPoint);

        if (tacticShootTarget->bAboutToShoot() &&
            !tacticShootTarget->bIsFinished())
        {
            emit _vSendPassPoint(vt2dMakePassPoint);
        }
    }
}

QString RoleSupportAttacker::strName() const
{
    switch (currentState)
    {
    case RECEIVE_PASS: {
        return QString("[RECEIVE] Support Attacker");
    }
    break;

    case SHOOT: {
        return QString("[SHOOT] Support Attacker");
    }
    break;
    }
    return QString("Support Attacker");
}

void RoleSupportAttacker::vInitialize()
{
    currentState = RECEIVE_PASS;
    tacticReceivePass->vSetStartingPosition(QVector2D(zone.ptZoneCenter()));
    bSetTactic(tacticReceivePass->id());

    completedStates.RECEIVE_PASS = false;
    completedStates.SHOOT = false;

    bInitialized = true;
}

int RoleSupportAttacker::iRoleType()
{
    return ROLE_SUPPORT_ATTACKER;
}

void RoleSupportAttacker::vSetPassPoint_(const QVector2D _point)
{
    //     qInfo() << QString("[Role %1]:%2) Tenho que pegar um passe em (%3;
    //     %4)")
    //                .arg(strName())
    //                .arg(robot->iID())
    //                .arg(_point.x())
    //                .arg(_point.y());

    tacticReceivePass->vSetPassPoint(_point);
}

void RoleSupportAttacker::vUpdateReceiver_(const qint8 _receiverID)
{
    iReceiverID = _receiverID;
}
