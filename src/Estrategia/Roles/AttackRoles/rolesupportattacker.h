#ifndef ROLESUPPORTATTACKER_H
#define ROLESUPPORTATTACKER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_SUPPORT_ATTACKER = 80
};

class RoleSupportAttacker : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticReceivePass> tacticReceivePass;
    QSharedPointer<TacticShootToTarget> tacticShootTarget;

    enum STATES
    {
        RECEIVE_PASS,
        SHOOT
    };

    struct completedStates
    {
        bool RECEIVE_PASS;
        bool SHOOT;
    }completedStates;

    STATES currentState;

    // Para fazer um passe após receber a bola
    qint8 iReceiverID;
    QVector2D vt2dMakePassPoint;

    void vShoot();

public:
    RoleSupportAttacker(AmbienteCampo *_fieldEnv);
    ~RoleSupportAttacker();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;

signals:
    void _vSendPassPoint(const QVector2D _point);

public slots:
    void vSetPassPoint_(const QVector2D _point);
    void vUpdateReceiver_(const qint8 _receiverID);
};

#endif // ROLESUPPORTATTACKER_H
