#include "rolesupportdeflecter.h"

RoleSupportDeflecter::RoleSupportDeflecter(AmbienteCampo *_fieldEnv) :
    Role(_fieldEnv), playConfig("PlayDeflect")
{
    iID = ROLE_SUPPORT_DEFLECTER;
    
    playConfig.read("play_deflect_config.json");
    iDeflecterID = -1;

    shootToTarget.reset(new TacticShootToTarget(_fieldEnv));

    bAddTactic(shootToTarget.get());
}

RoleSupportDeflecter::~RoleSupportDeflecter()
{

}

void RoleSupportDeflecter::vRun()
{
    if(iDeflecterID != -1)
    {
        if(currentTactic == nullptr)
        {
            bSetTactic(shootToTarget->id());
        }

        const Robo* receiver = fieldEnvironment->allies->getCPlayer(iDeflecterID);
        QVector2D shootPoint = receiver->vt2dRollerCenter();

        shootToTarget->vShootToRobot(iDeflecterID);
        shootToTarget->vSetKickType(KICK_CUSTOM, playConfig.passForce);

        if(shootToTarget->bAboutToShoot() && !shootToTarget->bIsFinished())
        {
            emit _vSendProceedDeflect();
        }
    }

    if(currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RoleSupportDeflecter::strName() const
{
    return QString("Support Deflecter");
}

void RoleSupportDeflecter::vInitialize()
{
    iDeflecterID = -1;
    bInitialized = true;
}

int RoleSupportDeflecter::iRoleType()
{
    return ROLE_SUPPORT_DEFLECTER;
}

void RoleSupportDeflecter::vReceiveDeflecterID_(const int _id)
{
    if(_id >= 0 && _id < globalConfig.robotsPerTeam)
    {
        iDeflecterID = _id;
    }
    else
    {
        qWarning() << QString("[Role %1] ID do robô defletor inválido! _id = %2")
            .arg(strName()).arg(_id);
    }
}
