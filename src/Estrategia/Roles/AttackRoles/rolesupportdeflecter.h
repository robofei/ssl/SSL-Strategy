#ifndef ROLESUPPORTDEFLECTER_H
#define ROLESUPPORTDEFLECTER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"
#include "Config/playdeflectconfig.h"

enum
{
    ROLE_SUPPORT_DEFLECTER = 500
};

class RoleSupportDeflecter : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticShootToTarget> shootToTarget;

    PlayDeflectConfig playConfig;
    int iDeflecterID;

public:
    RoleSupportDeflecter(AmbienteCampo *_fieldEnv);
    ~RoleSupportDeflecter();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;

public slots:
    void vReceiveDeflecterID_(const int _id);

signals:
    void _vSendProceedDeflect();
};

#endif // ROLESUPPORTDEFLECTER_H
