#include "roleballchaserfreekick.h"

RoleBallChaserFreeKick::RoleBallChaserFreeKick(AmbienteCampo* _fieldEnv,
                                               const float velMax)
    : Role(_fieldEnv)
{
    iID = ROLE_BALLCHASERFREEKICK;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    bAddTactic(tacticMoveTo.get());

    tacticMoveTo->vSetVelocity(velMax);
    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);

    kickOffMode = false;
}

RoleBallChaserFreeKick::~RoleBallChaserFreeKick()
{
}

void RoleBallChaserFreeKick::vRun()
{
    AmbienteCampo* env = fieldEnvironment;

    if (bSetTactic(tacticMoveTo->id()) || currentTactic != nullptr)
    {

        const qint8 idAdversario = env->iAchaInimigoProximoBola();

        const QVector2D posBola = env->vt2dPosicaoBola();

        QVector2D miraChutador =
            env->geoCampo->vt2dGoalCenter(env->allies->iGetSide());

        // Se o adversário estiver próximo da bola e mirando nela
        if (idAdversario != -1 &&
            env->vt2dOppPosition(idAdversario).distanceToPoint(posBola) < 300 &&
            Auxiliar::dAnguloVetor1Vetor2(
                env->opponents->getPlayer(idAdversario)
                        ->vt2dPontoDirecaoMira() -
                    env->opponents->getPlayer(idAdversario)->vt2dPosicaoAtual(),
                posBola - env->opponents->getPlayer(idAdversario)
                              ->vt2dPosicaoAtual()) < 30)
        {
            miraChutador =
                (env->opponents->getPlayer(idAdversario)
                     ->vt2dPontoDirecaoMira() -
                 env->opponents->getPlayer(idAdversario)->vt2dPosicaoAtual()) +
                posBola;
        }

        QVector2D destino = posBola + fDistanciaBarreiraFreeKick *
                                          (miraChutador - posBola).normalized();

        if (kickOffMode)
        {
            const int allySide = env->allies->iGetSide();

            if (allySide == -1)
            {
                if (destino.x() > -globalConfig.robotDiameter / 2.f)
                {
                    destino.setX(-globalConfig.robotDiameter / 2.f);
                }
            }
            else
            {
                if (destino.x() < globalConfig.robotDiameter / 2.f)
                {
                    destino.setX(globalConfig.robotDiameter / 2.f);
                }
            }

            while (destino.distanceToPoint(posBola) <
                   fDistanciaBarreiraFreeKick)
            {
                destino += globalConfig.robotDiameter / 2.f *
                           (destino - posBola).normalized();
            }
        }

        tacticMoveTo->vSetGotoPosition(destino);
        tacticMoveTo->vSetLooktoPosition(posBola);

        currentTactic->vRunTactic();
    }
}

void RoleBallChaserFreeKick::vSetKickOffMode(bool newKickOffMode)
{
    kickOffMode = newKickOffMode;
}

void RoleBallChaserFreeKick::setDistanceToBall(float newDistanceToBall)
{
    distanceToBall = newDistanceToBall;
}

QString RoleBallChaserFreeKick::strName() const
{
    return QString("&BallChaserFreeKick");
}

void RoleBallChaserFreeKick::vInitialize()
{
    bInitialized = true;
}

int RoleBallChaserFreeKick::iRoleType()
{
    return ROLE_BALLCHASERFREEKICK;
}

void RoleBallChaserFreeKick::vCheckRoleFinished()
{
    bFinished = false;
}
