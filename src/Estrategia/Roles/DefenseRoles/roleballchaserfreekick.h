#ifndef ROLEBALLCHASERFREEKICK_H
#define ROLEBALLCHASERFREEKICK_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_BALLCHASERFREEKICK = 240
};

class RoleBallChaserFreeKick : public Role
{
    Q_OBJECT
private:
    void vRun() override;

    // Tactics
    QSharedPointer<TacticMoveToPose> tacticMoveTo;

    // parâmetros da Role
    float distanceToBall;
    bool kickOffMode;

public:
    RoleBallChaserFreeKick(
        AmbienteCampo* _fieldEnv,
        const float velMax = globalConfig.robotVelocities.linear.limit);
    ~RoleBallChaserFreeKick();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;
    void setDistanceToBall(float newDistanceToBall);
    void vSetKickOffMode(bool newKickOffMode);
};

#endif // ROLEBALLCHASERFREEKICK_H
