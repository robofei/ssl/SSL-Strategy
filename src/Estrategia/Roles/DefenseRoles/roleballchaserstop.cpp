#include "roleballchaserstop.h"

RoleBallChaserStop::RoleBallChaserStop(AmbienteCampo *_fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_BALLCHASERSTOP;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    bAddTactic(tacticMoveTo.get());

    tacticMoveTo->vSetVelocity(globalConfig.stopLinearVelocity);
    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
}

RoleBallChaserStop::~RoleBallChaserStop()
{

}

void RoleBallChaserStop::vRun()
{
    if(bSetTactic(tacticMoveTo->id()) || currentTactic != nullptr)
    {
        const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
        const QVector2D centerAllyGoal =
            fieldEnvironment->geoCampo->vt2dGoalCenter(fieldEnvironment->allies->iGetSide());

        robot->vSetaIgnorarBola(bConsideraBola);
        tacticMoveTo->vSetGotoPosition(ballPos + distanceToBall*(centerAllyGoal - ballPos).normalized());
        tacticMoveTo->vSetLooktoPosition(ballPos);

        currentTactic->vRunTactic();
    }
}

float RoleBallChaserStop::getDistToBall() const
{
    return distanceToBall;
}

void RoleBallChaserStop::setDistToBall(float newDistanciaDaBola)
{
    distanceToBall = newDistanciaDaBola;
}

QString RoleBallChaserStop::strName() const
{
    return QString("&BallChaserStop");
}

void RoleBallChaserStop::vInitialize()
{
    bInitialized = true;
}

int RoleBallChaserStop::iRoleType()
{
    return ROLE_BALLCHASERSTOP;
}

void RoleBallChaserStop::vCheckRoleFinished()
{
    bFinished = false;
}
