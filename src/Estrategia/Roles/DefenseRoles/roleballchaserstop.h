#ifndef ROLEBALLCHASERSTOP_H
#define ROLEBALLCHASERSTOP_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_BALLCHASERSTOP = 220
};

///
/// \brief Role que persegue a bola, mas fica a uma distância dela
///
class RoleBallChaserStop : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    // Tactics
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    float distanceToBall;

public:
    RoleBallChaserStop(AmbienteCampo *_fieldEnv);
    ~RoleBallChaserStop();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;
    float getDistToBall() const;
    void setDistToBall(float newDistanciaDaBola);
};

#endif // ROLEBALLCHASERSTOP_H
