#include "roledeltadefender.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

RoleDeltaDefender::RoleDeltaDefender(AmbienteCampo* _fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_DELTA_DEFENDER;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));
    bAddTactic(tacticMoveTo.get());
    bAddTactic(tacticShootToTarget.get());
    tacticShootToTarget->vSetKickType(KICK_CUSTOM, globalConfig.calibratedKickForce);
    tacticShootToTarget->vSetApproachVelocity(globalConfig.robotVelocities.linear.limit * 0.3);
    tacticShootToTarget->vSetEngageVelocity(0.2);
}

RoleDeltaDefender::~RoleDeltaDefender()
{
}

void RoleDeltaDefender::vRun()
{
    QVector2D pos = robot->vt2dPosicaoAtual(),
              ballPos = fieldEnvironment->vt2dPosicaoBola();
    emit _vRequestDefensePosition(robot->iID(), pos);

    const QVector2D centerAllyGoal = fieldEnvironment->geoCampo->vt2dGoalCenter(
        fieldEnvironment->allies->iGetSide());

    bool ballInside = fieldEnvironment->bBolaDentroAreaDefesa(1.3);

    if (pos.distanceToPoint(ballPos) > 700 || ballInside)
    {
        bSetTactic(tacticMoveTo->id());
        tacticMoveTo->vSetGotoPosition(pos);
        tacticMoveTo->vSetLooktoPosition(centerAllyGoal +
                                         100 * (pos - centerAllyGoal));
    }
    else
    {
        bSetTactic(tacticShootToTarget->id());
        tacticShootToTarget->vShootToPoint({0, 0});
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RoleDeltaDefender::strName() const
{
    return QString("Delta Defender");
}

void RoleDeltaDefender::vInitialize()
{
    bInitialized = true;
}

int RoleDeltaDefender::iRoleType()
{
    return ROLE_DELTA_DEFENDER;
}

void RoleDeltaDefender::vCheckRoleFinished()
{
    bFinished = false;
}
