#ifndef ROLEDELTADEFENDER_H
#define ROLEDELTADEFENDER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_DELTA_DEFENDER = 60
};

class RoleDeltaDefender : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;

public:
    RoleDeltaDefender(AmbienteCampo *_fieldEnv);
    ~RoleDeltaDefender();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;

signals:
    void _vRequestDefensePosition(const qint8 _id, QVector2D &_pos);
};

#endif // ROLEDELTADEFENDER_H
