#include "rolegoalie.h"

#define SET_TACTIC(tactic)                                                     \
    if (currentTactic == nullptr || currentTactic->id() != tactic->id())       \
    {                                                                          \
        bSetTactic(tactic->id());                                              \
    }

RoleGoalie::RoleGoalie(AmbienteCampo* _fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_GOALIE;
    mode = GOALIE_MODE::DEFAULT_MODE;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));

    bAddTactic(tacticMoveTo.get());
    bAddTactic(tacticShootToTarget.get());

    fGoalieOffset = globalConfig.robotDiameter;
    currentState = DEFEND_DIRECT_SHOT;

    tacticShootToTarget->vSetKickType(KICK_CUSTOM, globalConfig.calibratedKickForce);
    tacticShootToTarget->vSetApproachVelocity(globalConfig.robotVelocities.linear.limit * 0.3);
    tacticShootToTarget->vSetEngageVelocity(0.2);

    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
}

RoleGoalie::~RoleGoalie()
{
}

void RoleGoalie::vRun()
{
    vUpdateStates();
    vUpdateGoalieOffset();

    switch (currentState)
    {
    case DEFEND_DIRECT_SHOT: {
        vDefendDirectShot();
    }
    break;

    case BLOCK_OPPONENT: {
        vBlockOpponent();
    }
    break;

    case BLOCK_STANDARD: {
        vBlockStandard();
    }
    break;

    case REMOVE_BALL: {
        vRemoveBall();
    }
    break;
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RoleGoalie::strName() const
{
    return QString("Goalie");
}

void RoleGoalie::vInitialize()
{
    currentState = DEFEND_DIRECT_SHOT;
    bSetTactic(tacticMoveTo->id());
    fGoalieOffset = globalConfig.robotDiameter;
    bInitialized = true;
}

void RoleGoalie::vCheckRoleFinished()
{
    bFinished = false;
}

int RoleGoalie::iRoleType()
{
    return ROLE_GOALIE;
}

void RoleGoalie::vSetGoalieMode(GOALIE_MODE _mode)
{
    qInfo() << QString("[Role %1] Mudando tipo do goleiro de %2 para %3!")
                   .arg(strName())
                   .arg(QVariant::fromValue(mode).toString())
                   .arg(QVariant::fromValue(_mode).toString());
    mode = _mode;
}

void RoleGoalie::vUpdateStates()
{
    //    if(mode == STOP_MODE)
    //    {
    //        currentState = BLOCK_STANDARD;
    //        return;
    //    }

    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();
    const QVector2D ballVel = env.vt2dVelocidadeBola();
    const int allySide = env.allies->iGetSide();

    int oppID = env.iAchaInimigoProximoPonto(ballPos);
    if (oppID != -1)
    {
        if (env.opponents->getPlayer(oppID)->vt2dPosicaoAtual().distanceToPoint(
                ballPos) > 250 ||
            env.opponents->getPlayer(oppID)->vt2dPosicaoAtual().distanceToPoint(
                env.geoCampo->vt2dGoalCenter(allySide)) > 3000)
        {
            oppID = -1;
        }
    }

    // const float goalHeight = env.geoCampo->szGoal().height(),
    //             xGoalie = allySide*fabs(env.geoCampo->szField().width()/2.0 -
    //             fGoalieOffset), yProjection =
    //             Auxiliar::fProjecaoBolaNoGolAliado(ballVel, ballPos,
    //             xGoalie);

    const bool ballRemoved = ((currentState == REMOVE_BALL) &&
                              (tacticShootToTarget->bIsFinished())) ||
                             (currentState != REMOVE_BALL) ||
                             !env.bBolaDentroAreaDefesaAliado();

    // else if(ballVel.length() > .5 && ballVel.x()*allySide >= 0 &&
    //            fabs(yProjection) < 1.5f*goalHeight/2)
    // {
    //     currentState = DEFEND_DIRECT_SHOT;
    // }
    if (oppID != -1)
    {
        currentState = BLOCK_OPPONENT;
    }
    else if (!ballRemoved && env.bBolaDentroAreaDefesaAliado() &&
             ballVel.length() < 0.5)
    {
        currentState = REMOVE_BALL;
    }
    else
    {
        currentState = DEFEND_DIRECT_SHOT;
    }

    //    if(opponentPos.distanceToPoint(ballPos) < 2*globalConfig.robotDiameter &&
    //    ballRemoved)
    //    {
    //        if(currentState != BLOCK_OPPONENT)
    //        {
    //            qInfo() << QString("[Role %1] State changed to DEFEND_OPPONENT
    //            %2
    //            %3").arg(strName()).arg(ballRemoved).arg(tacticShootToTarget->bIsFinished());
    //            tacticMoveTo->vInitialize();
    //        }
    //        currentState = BLOCK_OPPONENT;
    //    }
    //    else if(fieldEnvironment->bBolaDentroAreaDefesaAliado() &&
    //            ballVel.length() < 0.5)
    //    {
    //        if(currentState != REMOVE_BALL)
    //        {
    //            qInfo() << QString("[Role %1] State changed to
    //            REMOVE_BALL").arg(strName()); tacticMoveTo->vInitialize();
    //        }
    //        currentState = REMOVE_BALL;
    //    }
    //    else if(ballRemoved)
    //    {
    //        if(currentState != DEFEND_DIRECT_SHOT)
    //        {
    //            qInfo() << QString("[Role %1] State changed to
    //            DEFEND_DIRECT_SHOT %2
    //            %3").arg(strName()).arg(ballRemoved).arg(tacticShootToTarget->bIsFinished());
    //            tacticShootToTarget->vInitialize();
    //        }
    //        currentState = DEFEND_DIRECT_SHOT;
    //    }
}

void RoleGoalie::vUpdateGoalieOffset()
{
    if (mode == STOP_MODE)
    {
        fGoalieOffset = globalConfig.robotDiameter / 3.0;
        return;
    }

    const AmbienteCampo& env = *fieldEnvironment;
    const int allySide = env.allies->iGetSide();
    const QVector2D ballPos = env.vt2dPosicaoBola();

    if (mode == PENALTY_MODE)
    {
        if (ballPos.x() * allySide > 0)
        {
            // Se a bola está do nosso lado, o robô atacante já moveu a bola
            // e nós não precisamos mais ficar em cima da linha
            vSetGoalieMode(GOALIE_MODE::DEFAULT_MODE);
        }
        else
        {
            // Deixa o goleiro encostando de leve na linha do gol
            fGoalieOffset = globalConfig.robotDiameter / 3.0;
        }
    }

    if (mode == DEFAULT_MODE)
    {
        // O offset é menor nas laterais
        const QVector2D allyGoal =
            env.geoCampo->vt2dGoalCenter(env.allies->iGetSide());
        const QVector2D ballToGoal = allyGoal - ballPos;

        const float coef =
            ballToGoal.y() == 0 ? 9e9 : fabs(ballToGoal.x() / ballToGoal.y());
        const float maxCoef = 5, minOffset = globalConfig.robotDiameter / 2.0,
                    maxOffset = env.geoCampo->szDefenseArea().width() / 1.5;

        if (coef > maxCoef)
        {
            fGoalieOffset = maxOffset;
        }
        else
        {
            fGoalieOffset =
                minOffset + coef * (maxOffset - minOffset) / maxCoef;
        }
    }
}

void RoleGoalie::vDefendDirectShot()
{
    SET_TACTIC(tacticMoveTo)
    const AmbienteCampo& env = *fieldEnvironment;
    const int allySide = env.allies->iGetSide();

    if (mode == PENALTY_MODE)
    {
        const float xGoalie =
            allySide *
            fabs(env.geoCampo->szField().width() / 2.0 - fGoalieOffset);
        const float yProjection = Auxiliar::fProjecaoBolaNoGolAliado(
            env.vt2dVelocidadeBola(), env.vt2dPosicaoBola(), xGoalie);

        tacticMoveTo->vSetGotoPosition(QVector2D(xGoalie, yProjection));
    }
    else
    {
        // const QVector2D destiny =
        // Auxiliar::vt2dDestinoGoleiroBolaEmMovimento(
        //     env.vt2dVelocidadeBola(),
        //     env.vt2dPosicaoBola(),
        //     player()->vt2dPosicaoAtual(),
        //     (env.geoCampo->szField().width()/2.0 - globalConfig.robotDiameter/2.0) *
        //     allySide);
        QVector2D target = Auxiliar::vt2dCalculaDefesaGoleiro(
            fieldEnvironment->blBola->vvt2dGetFilteredPositions(),
            fieldEnvironment->allies->iGetSide(),
            fieldEnvironment->geoCampo->szGoal().height(),
            fieldEnvironment->geoCampo->szField(), 100);

        tacticMoveTo->vSetGotoPosition(target);
    }

    tacticMoveTo->vSetLooktoPosition(env.vt2dPosicaoBola());
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vIgnoreBall();
}

void RoleGoalie::vBlockOpponent()
{
    SET_TACTIC(tacticMoveTo)

    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();
    const int allySide = env.allies->iGetSide();

    int oppID = env.iAchaInimigoProximoPonto(ballPos);
    const Robo* adv = env.opponents->getCPlayer(oppID);
    const float xGoalie =
                    allySide *
                    fabs(env.geoCampo->szField().width() / 2.0 - fGoalieOffset),
                goalHeight = env.geoCampo->szGoal().height();

    float yDestiny = Auxiliar::fProjecaoBolaNoGolAliado(
        adv->vt2dPontoDirecaoMira() - adv->vt2dPosicaoAtual(), ballPos,
        xGoalie);

    // Para evitar que o goleiro vá em alguma ponta do gol e deixe algum canto
    // muito aberto.
    if (yDestiny > goalHeight / 2.53)
    {
        yDestiny = goalHeight / 2.3;
    }
    else if (yDestiny < -goalHeight / 2.3)
    {
        yDestiny = -goalHeight / 2.3;
    }

    tacticMoveTo->vSetGotoPosition(QVector2D(xGoalie, yDestiny));
    tacticMoveTo->vSetLooktoPosition(env.vt2dPosicaoBola());
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vIgnoreBall();
}

void RoleGoalie::vBlockStandard()
{
    SET_TACTIC(tacticMoveTo)

    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();
    const int allySide = env.allies->iGetSide();
    const float xGoalie =
                    allySide *
                    fabs(env.geoCampo->szField().width() / 2.0 - fGoalieOffset),
                goalHeight = env.geoCampo->szGoal().height();

    float yDestiny = Auxiliar::fMelhorPosicaoGoleiroParaFecharGolAliado(
        ballPos, env.geoCampo->vt2dGoalCenter(allySide), goalHeight, xGoalie);

    if (yDestiny > goalHeight / 2.3)
        yDestiny = goalHeight / 2.3;
    else if (yDestiny < -goalHeight / 2.3)
        yDestiny = -goalHeight / 2.3;

    tacticMoveTo->vSetGotoPosition(QVector2D(xGoalie, yDestiny));
    tacticMoveTo->vSetLooktoPosition(env.vt2dPosicaoBola());
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vIgnoreBall();
}

void RoleGoalie::vRemoveBall()
{
    SET_TACTIC(tacticShootToTarget)

    const AmbienteCampo& env = *fieldEnvironment;

    const int bolaY = env.vt2dPosicaoBola().y() > 0 ? 1 : -1;
    const QVector2D target =
        env.vt2dCoordenadaGenericaParaCampo(QVector2D(0, 1.5 * bolaY));

    tacticShootToTarget->vShootToPoint(target);
    tacticShootToTarget->vSetKickType(CHIP_KICK_STRONG);
}

// QVector2D RoleGoalie::vt2dCalculateDirectShotDefense()
//{
//     QVector<QVector2D> ballPath =
//     fieldEnvironment->blBola->vvt2dGetFilteredPositions(); QVector2D
//     pos(goalX, 0);

//    if(ballPath.size() > 0)
//    {
//        if(ballSide != allySide)
//        {
//            ballPath.clear();
//            ballPath.append(ballPos);
//            ballPath.append(ballPos);
//        }

//        pos = Auxiliar::vt2dCalculaDefesaGoleiro(ballPath, allySide, goalSize,
//                                                 fieldEnvironment->geoCampo->szField(),
//                                                 fGoalieOffset);
//    }

//    if(mode == GOALIE_MODE::PENALTY_MODE)
//    {
//        if(ballSide == allySide)
//        {
//            // Se a bola está do nosso lado, o robô atacante já moveu a bola
//            // e nós não precisamos mais ficar em cima da linha
//            vSetGoalieMode(GOALIE_MODE::DEFAULT_MODE);
//        }
//        pos.setX(goalX);
//    }

//    return QVector2D();
//}

// QVector2D RoleGoalie::vt2dCalculateOpponentBlock()
//{

//}

// QVector2D RoleGoalie::vt2dCalculateStandardBlock()
//{

//}
