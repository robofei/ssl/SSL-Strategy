#ifndef ROLEGOALIE_H
#define ROLEGOALIE_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_GOALIE = 40
};

class RoleGoalie : public Role
{
    Q_OBJECT
public:
    /**
     * @brief Define o modo de operação do goleiro, no modo PENALTY_MODE ele
     * não pode sair da linha do gol enquanto não tiver o comando de normal
     * start.
     */
    enum GOALIE_MODE
    {
        DEFAULT_MODE = 0,
        PENALTY_MODE,
        STOP_MODE
    };
    Q_ENUM(GOALIE_MODE)

private:
    void vRun() override;
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;
    float fGoalieOffset;
    GOALIE_MODE mode;

    enum STATES
    {
        DEFEND_DIRECT_SHOT,
        BLOCK_OPPONENT,
        BLOCK_STANDARD,
        REMOVE_BALL
    };

    STATES currentState;
    void vUpdateStates();

    void vUpdateGoalieOffset();

    void vDefendDirectShot();
    void vBlockOpponent();
    void vBlockStandard();
    void vRemoveBall();

public:
    RoleGoalie(AmbienteCampo *_fieldEnv);
    ~RoleGoalie();

    QString strName() const override;
    void vInitialize() override;
    void vCheckRoleFinished() override;
    int iRoleType() override;

    void vSetGoalieMode(GOALIE_MODE _mode);
};

#endif // ROLEGOALIE_H
