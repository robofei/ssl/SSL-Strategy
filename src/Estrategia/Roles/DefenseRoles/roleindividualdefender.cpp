#include "roleindividualdefender.h"

RoleIndividualDefender::RoleIndividualDefender(AmbienteCampo* _fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_INDIVIDUAL_DEFENDER;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));
    bAddTactic(tacticMoveTo.get());
    bAddTactic(tacticShootToTarget.get());
    tacticShootToTarget->vSetKickType(KICK_CUSTOM, globalConfig.calibratedKickForce);
    tacticShootToTarget->vSetApproachVelocity(globalConfig.robotVelocities.linear.limit * 0.3);
    tacticShootToTarget->vSetEngageVelocity(0.2);
}

RoleIndividualDefender::~RoleIndividualDefender()
{
}

void RoleIndividualDefender ::vRun()
{
    QVector2D pos = robot->vt2dPosicaoAtual();
    emit _vRequestDefensePosition(robot->iID(), pos);

    if (bSetTactic(tacticMoveTo->id()) || currentTactic != nullptr)
    {
        if (pos.distanceToPoint(fieldEnvironment->vt2dPosicaoBola()) < 700)
        {
            // const qint8 closestOppId =
            // fieldEnvironment->iAchaInimigoProximoBola(); const QVector2D
            // closestOpp = (closestOppId == -1) ? QVector2D(9e5, 0) :
            //                                  fieldEnvironment->opponents->getCPlayer(closestOppId)->vt2dPosicaoAtual();
            //
            // const QVector4D vt4dDestino = Auxiliar::vt4dPosicaoLadraoDeBola(
            //             player()->vt2dPosicaoAtual(),
            //             closestOpp,
            //             fieldEnvironment->vt2dPosicaoBola(),
            //             fieldEnvironment->geoCampo->vt2dGoalCenter(fieldEnvironment->allies->iGetSide())
            //             );
            //
            // tacticMoveTo->vSetGotoPosition(QVector2D(vt4dDestino.x(),
            // vt4dDestino.y()));
            // tacticMoveTo->vSetLooktoPosition(QVector2D(vt4dDestino.z(),
            // vt4dDestino.w()));
            bSetTactic(tacticShootToTarget->id());
            QVector2D oppGoal = fieldEnvironment->geoCampo->vt2dGoalCenter(
                fieldEnvironment->opponents->iGetSide());
            tacticShootToTarget->vShootToPoint(oppGoal);
        }
        else
        {
            tacticMoveTo->vSetGotoPosition(pos);
            tacticMoveTo->vSetLooktoPosition(
                fieldEnvironment->vt2dPosicaoBola());
        }
        currentTactic->vRunTactic();
    }
}

QString RoleIndividualDefender::strName() const
{
    return QString("&Individual; Defender");
}

void RoleIndividualDefender::vInitialize()
{
    bInitialized = true;
}

int RoleIndividualDefender::iRoleType()
{
    return ROLE_INDIVIDUAL_DEFENDER;
}

void RoleIndividualDefender::vCheckRoleFinished()
{
    bFinished = false;
}
