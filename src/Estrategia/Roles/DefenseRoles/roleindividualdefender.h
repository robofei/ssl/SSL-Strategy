#ifndef ROLEINDIVIDUALDEFENDER_H
#define ROLEINDIVIDUALDEFENDER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_INDIVIDUAL_DEFENDER = 560
};

class RoleIndividualDefender : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;

public:
    RoleIndividualDefender(AmbienteCampo *_fieldEnv);
    ~RoleIndividualDefender();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;

signals:
    void _vRequestDefensePosition(const qint8 _id, QVector2D &_pos);
};

#endif // ROLEINDIVIDUALDEFENDER_H
