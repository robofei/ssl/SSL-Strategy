#include "rolenightygoalie.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

RoleNightyGoalie::RoleNightyGoalie(AmbienteCampo* _fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_NIGHTY_GOALIE;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));

    bAddTactic(tacticMoveTo.get());
    bAddTactic(tacticShootToTarget.get());

    currentState = BALL_WITH_OPPONENT;

    tacticShootToTarget->vSetKickType(KICK_CUSTOM, globalConfig.calibratedKickForce);
    tacticShootToTarget->vSetApproachVelocity(globalConfig.robotVelocities.linear.limit * 0.3);
    tacticShootToTarget->vSetEngageVelocity(0.2);

    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
}

RoleNightyGoalie::~RoleNightyGoalie()
{
}

QString RoleNightyGoalie::strName() const
{
    switch (currentState)
    {
    case BALL_INSIDE: {
        return QString("Nighty Goalie | INSIDE");
    }
    break;

    case BALL_WITH_OPPONENT: {
        return QString("Nighty Goalie | OPP");
    }
    break;

    case BALL_ALONE: {
        return QString("Nighty Goalie | ALONE");
    }
    break;
    }
    return QString("!!! Nighty Goalie");
}

void RoleNightyGoalie::vInitialize()
{
    currentState = BALL_WITH_OPPONENT;

    bInitialized = true;
}

void RoleNightyGoalie::vCheckRoleFinished()
{
    bFinished = false;
}

int RoleNightyGoalie::iRoleType()
{
    return ROLE_NIGHTY_GOALIE;
}

void RoleNightyGoalie::vSetGoalieMode(GOALIE_MODE _mode)
{
    qInfo() << QString("[Role %1] Mudando tipo do goleiro de %2 para %3!")
                   .arg(strName())
                   .arg(QVariant::fromValue(mode).toString())
                   .arg(QVariant::fromValue(_mode).toString());
    mode = _mode;
}

void RoleNightyGoalie::vRun()
{
    vUpdateStates();
    if (mode == STOP_MODE)
    {
        goalieX = fieldEnvironment->allies->iGetSide() *
                  (fieldEnvironment->geoCampo->szField().width() / 2.f - 200);
    }
    else if (mode == PENALTY_MODE)
    {
        goalieX = fieldEnvironment->allies->iGetSide() *
                  (fieldEnvironment->geoCampo->szField().width() / 2.f);
    }
    else if (mode == DEFAULT_MODE)
    {
        goalieX = fieldEnvironment->allies->iGetSide() *
                  (fieldEnvironment->geoCampo->szField().width() / 2.f - 100);
    }

    switch (currentState)
    {
    case BALL_INSIDE: {
        vBallInside();
    }
    break;

    case BALL_WITH_OPPONENT: {
        vBallOpponent();
    }
    break;

    case BALL_ALONE: {
        vBallAlone();
    }
    break;
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
        robot->vChutar(KICK_CUSTOM, globalConfig.calibratedKickForce);
    }
}

void RoleNightyGoalie::vUpdateStates()
{
    qint8 opponentId = fieldEnvironment->iAchaInimigoProximoBola();
    goalieX = fieldEnvironment->allies->iGetSide() *
              (fieldEnvironment->geoCampo->szField().width() / 2.f - 100);

    if (opponentId != -1 &&
        fieldEnvironment->opponents->getCPlayer(opponentId)
                ->vt2dPosicaoAtual()
                .distanceToPoint(fieldEnvironment->vt2dPosicaoBola()) >
            globalConfig.robotDistanceToBall * 1.5)
    {
        opponentId = -1;
    }

    if (fieldEnvironment->bBolaDentroAreaDefesaAliado(1.3) &&
        fieldEnvironment->dVelocidadeBola() < 0.2)
    {
        currentState = BALL_INSIDE;
    }
    else if (opponentId != -1)
    {
        currentState = BALL_WITH_OPPONENT;
    }
    else
    {
        currentState = BALL_ALONE;
    }
}

void RoleNightyGoalie::vBallInside()
{
    bSetTactic(tacticShootToTarget->id());

    QVector2D target(0, 0);

    tacticShootToTarget->vShootToPoint(target);
    tacticShootToTarget->vSetKickType(CHIP_KICK_CUSTOM, globalConfig.calibratedKickForce);
}

void RoleNightyGoalie::vBallAlone()
{
    bSetTactic(tacticMoveTo->id());
    QVector2D target;

    if (fieldEnvironment->dVelocidadeBola() > 0.2)
    {
        int goalHeight = fieldEnvironment->geoCampo->szGoal().height();
        target = Auxiliar::vt2dCalculaDefesaGoleiro(
            fieldEnvironment->blBola->vvt2dGetFilteredPositions(),
            fieldEnvironment->allies->iGetSide(), goalHeight,
            fieldEnvironment->geoCampo->szField(), 50);
    }
    else
    {
        target = vt2dBallBisectrix();
    }

    if (qAbs(target.x()) >= 4500)
    {
        target.setX(Auxiliar::iSinal(target.x()) * 4490);
    }

    tacticMoveTo->vSetGotoPosition(target);
    tacticMoveTo->vSetLooktoPosition(fieldEnvironment->vt2dPosicaoBola());
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vIgnoreBall();
}

void RoleNightyGoalie::vBallOpponent()
{
    bSetTactic(tacticMoveTo->id());

    qint8 opponentId = fieldEnvironment->iAchaInimigoProximoBola();
    int goalHeight = fieldEnvironment->geoCampo->szGoal().height();
    QVector2D opponentPos =
        fieldEnvironment->opponents->getCPlayer(opponentId)->vt2dPosicaoAtual();

    QVector2D opponentAim = fieldEnvironment->opponents->getCPlayer(opponentId)
                                ->vt2dAimDirectionNormalized();
    opponentAim = opponentAim * 10e3 + opponentPos;

    QVector2D target;
    bool hasIntersection = false;

    target = Auxiliar::vt2dCalculaDefesaGoleiro(
        {opponentAim, opponentPos}, fieldEnvironment->allies->iGetSide(),
        goalHeight, fieldEnvironment->geoCampo->szField(), 100, false,
        &hasIntersection);
    target.setX(goalieX);

    if (!hasIntersection)
    {
        target = vt2dBallBisectrix();
    }

    if (qAbs(target.x()) >= 4500)
    {
        target.setX(Auxiliar::iSinal(target.x()) * 4490);
    }

    tacticMoveTo->vSetGotoPosition(target);
    tacticMoveTo->vSetLooktoPosition(fieldEnvironment->vt2dPosicaoBola());
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vIgnoreBall();
}

QVector2D RoleNightyGoalie::vt2dBallBisectrix()
{
    QSize fieldSz = fieldEnvironment->geoCampo->szField();
    int fieldSide = fieldEnvironment->allies->iGetSide();
    int goalHeight = fieldEnvironment->geoCampo->szGoal().height();
    QVector2D leftPole = QVector2D(goalieX, -goalHeight / 2.f);
    QVector2D rightPole = QVector2D(goalieX, goalHeight / 2.f);
    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola(), result(goalieX, 0);

    if (qAbs(ballPos.x()) < fieldSz.width() / 2.f - 200 &&
        Auxiliar::iSinal(ballPos.x()) == fieldSide)
    {
        QVector2D ballLeftPole = QVector2D(leftPole - ballPos),
                  ballRightPole = QVector2D(rightPole - ballPos);

        float ballGoalAngle =
            Auxiliar::dAnguloVetor1Vetor2(ballLeftPole, ballRightPole) / 2.f;

        QVector2D bisectrix = Auxiliar::vt2dRotaciona(
            ballPos, rightPole, -1 * ballGoalAngle * fieldSide);

        Auxiliar::bChecaInterseccaoLinhaLinha(bisectrix, ballPos, rightPole,
                                              leftPole, bisectrix);
        result = bisectrix;

        // float a = ballRightPole.x() * qCos((ballGoalAngle) / 2) +
        //           ballRightPole.y() * qSin(ballGoalAngle / 2),
        //       b = -ballRightPole.x() * qSin(ballGoalAngle / 2) +
        //           ballRightPole.y() * qCos(ballGoalAngle / 2);
        //
        // QVector2D bisectrix(a, b);
        //
        // result.setY(ballPos.y() +
        //             bisectrix.y() * ((goalieX - ballPos.x()) /
        //             bisectrix.x()));
    }
    else
    {
        result.setY(ballPos.y());
    }
    result.setY(qMax(qMin(result.y(), rightPole.y()), leftPole.y()));

    return result;
}
