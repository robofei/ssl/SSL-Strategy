#ifndef ROLENIGHTYGOALIE_H
#define ROLENIGHTYGOALIE_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"
#include <QObject>

enum
{
    ROLE_NIGHTY_GOALIE = 620
};

class RoleNightyGoalie : public Role
{
    Q_OBJECT
public:
    RoleNightyGoalie(AmbienteCampo* _fieldEnv);
    ~RoleNightyGoalie();

    enum GOALIE_MODE
    {
        DEFAULT_MODE = 0,
        PENALTY_MODE,
        STOP_MODE
    };
    Q_ENUM(GOALIE_MODE)

    QString strName() const override;
    void vInitialize() override;
    void vCheckRoleFinished() override;
    int iRoleType() override;
    void vSetGoalieMode(GOALIE_MODE _mode);

private:
    void vRun() override;
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;
    float goalieX;
    GOALIE_MODE mode;

    enum STATES
    {
        BALL_INSIDE,
        BALL_WITH_OPPONENT,
        BALL_ALONE,
    };
    STATES currentState;

    void vUpdateStates();
    void vBallInside();
    void vBallAlone();
    void vBallOpponent();

    QVector2D vt2dBallBisectrix();
};

#endif // ROLENIGHTYGOALIE_H
