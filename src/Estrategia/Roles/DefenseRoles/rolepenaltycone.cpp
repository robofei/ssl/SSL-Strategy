#include "rolepenaltycone.h"

RolePenaltyCone::RolePenaltyCone(AmbienteCampo *_fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_PENALTY_CONE;

    tacticGoTo.reset(new TacticGoToLookTo(_fieldEnv));

    tacticGoTo->vSetVelocity(globalConfig.robotVelocities.linear.limit * 0.8);
    tacticGoTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);

    bAddTactic(tacticGoTo.get());
}

RolePenaltyCone::~RolePenaltyCone()
{

}

void RolePenaltyCone::vRun()
{
    if(currentTactic == nullptr)
    {
        bSetTactic(tacticGoTo->id());
    }

    const int fieldHeight = fieldEnvironment->geoCampo->szField().height()/2;
    const QVector2D pos = QVector2D(zone.ptZoneCenter().x(),
                                    fieldHeight - robot->iID() * 300);

    robot->vSetaIgnorarBola(bConsideraBola);
    tacticGoTo->vSetGotoPosition(pos);
    tacticGoTo->vSetLooktoPosition(fieldEnvironment->vt2dPosicaoBola());

    if(currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RolePenaltyCone::strName() const
{
    return QString("PenaltyCone");
}

void RolePenaltyCone::vInitialize()
{
    bInitialized = true;
}

int RolePenaltyCone::iRoleType()
{
    return ROLE_PENALTY_CONE;
}
