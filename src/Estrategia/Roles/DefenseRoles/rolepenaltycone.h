#ifndef ROLEPENALTYCONE_H
#define ROLEPENALTYCONE_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_PENALTY_CONE = 340
};

class RolePenaltyCone : public Role
{
    Q_OBJECT

private:
    void vRun() override;
    // Tactics
    QSharedPointer<TacticGoToLookTo> tacticGoTo;

public:
    RolePenaltyCone(AmbienteCampo *_fieldEnv);
    ~RolePenaltyCone();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
};

#endif // ROLEPENALTYCONE_H
