#include "rolepressuredefender.h"

RolePressureDefender::RolePressureDefender(AmbienteCampo* _fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_PRESSURE_DEFENDER;

    tacticMoveTo.reset(new TacticMoveToPose(_fieldEnv));
    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));
    tacticMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    tacticMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
    bAddTactic(tacticMoveTo.get());
    bAddTactic(tacticShootToTarget.get());
    tacticShootToTarget->vSetKickType(KICK_CUSTOM, globalConfig.calibratedKickForce);
    tacticShootToTarget->vSetApproachVelocity(globalConfig.robotVelocities.linear.limit * 0.3);
    tacticShootToTarget->vSetEngageVelocity(0.8);
}

RolePressureDefender::~RolePressureDefender()
{
}

void RolePressureDefender ::vRun()
{
    QVector2D pos = robot->vt2dPosicaoAtual();
    emit _vRequestDefensePosition(robot->iID(), pos);
    qint8 ballNearestRobot = fieldEnvironment->iAchaRoboProximoBola();

    bool ballInside = fieldEnvironment->bBolaDentroAreaDefesa(1.3);

    if (ballNearestRobot == robot->iID() && !ballInside)
    {
        tacticShootToTarget->vShootToGoal();
        bSetTactic(tacticShootToTarget->id());
    }
    else // (bSetTactic(tacticMoveTo->id()) || currentTactic != nullptr)
    {
        bSetTactic(tacticMoveTo->id());
        // if (pos.distanceToPoint(fieldEnvironment->vt2dPosicaoBola()) < 700)
        // const qint8 closestOppId =
        //     fieldEnvironment->iAchaInimigoProximoBola();
        // const QVector2D closestOpp =
        //     (closestOppId == -1)
        //         ? QVector2D(9e5, 0)
        //         : fieldEnvironment->opponents->getCPlayer(closestOppId)
        //               ->vt2dPosicaoAtual();
        //
        // const QVector4D vt4dDestino = Auxiliar::vt4dPosicaoLadraoDeBola(
        //     player()->vt2dPosicaoAtual(), closestOpp,
        //     fieldEnvironment->vt2dPosicaoBola(),
        //     fieldEnvironment->geoCampo->vt2dGoalCenter(
        //         fieldEnvironment->allies->iGetSide()));
        //
        // tacticMoveTo->vSetGotoPosition(
        //     QVector2D(vt4dDestino.x(), vt4dDestino.y()));
        // tacticMoveTo->vSetLooktoPosition(
        //     QVector2D(vt4dDestino.z(), vt4dDestino.w()));
        // }
        // else
        // {
        tacticMoveTo->vSetGotoPosition(pos);
        tacticMoveTo->vSetLooktoPosition(fieldEnvironment->vt2dPosicaoBola());
        // }
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RolePressureDefender::strName() const
{
    return QString("&Pressure; Defender");
}

void RolePressureDefender::vInitialize()
{
    bInitialized = true;
}

int RolePressureDefender::iRoleType()
{
    return ROLE_PRESSURE_DEFENDER;
}

void RolePressureDefender::vCheckRoleFinished()
{
    bFinished = false;
}
