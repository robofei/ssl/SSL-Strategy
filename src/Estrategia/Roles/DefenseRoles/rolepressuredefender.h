#ifndef ROLEPRESSUREDEFENDER_H
#define ROLEPRESSUREDEFENDER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_PRESSURE_DEFENDER = 580
};

class RolePressureDefender : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticMoveToPose> tacticMoveTo;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;

public:
    RolePressureDefender(AmbienteCampo *_fieldEnv);
    ~RolePressureDefender();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;

signals:
    void _vRequestDefensePosition(const qint8 _id, QVector2D &_pos);
};

#endif // ROLEPRESSUREDEFENDER_H
