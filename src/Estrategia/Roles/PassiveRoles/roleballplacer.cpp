#include "roleballplacer.h"

RoleBallPlacer::RoleBallPlacer(AmbienteCampo* _fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_BALL_PLACER;

    tacticReceivePass.reset(new TacticReceivePass(_fieldEnv));
    tacticGetBall.reset(new TacticGetBall(_fieldEnv));

    tacticReceivePass->vSetAutomaticMode();

    // bAddTactic(tacticReceivePass.get());
    bAddTactic(tacticGetBall.get());
}

RoleBallPlacer::~RoleBallPlacer()
{
}

void RoleBallPlacer::setPlacementPosition(QVector2D position)
{
    this->supportPosition = position;
}

void RoleBallPlacer::vRun()
{
    float distance = 100;

    // distance = supportPosition.distanceToPoint(fieldEnvironment->vt2dPosicaoBola());

    if (currentTactic == nullptr || tacticReceivePass->bIsFinished())
    {
        if (distance < 1000 || (distance < 1000 && currentTactic != nullptr &&
                                currentTactic->id() != tacticGetBall->id()))
        {
            if (tacticGetBall->bIsFinished())
            {
                tacticGetBall->vInitialize();
            }
            tacticGetBall->setTargetPosition(supportPosition);
            bSetTactic(tacticGetBall->id());
        }
        // else if (distance >= 1000)
        // {
        //     tacticReceivePass->vSetStartingPosition(supportPosition);
        //     emit _requestPass();
        //     qInfo() << QString("[Role %1] Requesting pass!").arg(strName());
        //     if (tacticReceivePass->bIsFinished())
        //     {
        //         tacticReceivePass->vInitialize();
        //     }
        //     bSetTactic(tacticReceivePass->id());
        // }
    }

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RoleBallPlacer::strName() const
{
    return QString("Ball Placer");
}

void RoleBallPlacer::vInitialize()
{
    bInitialized = true;
}

int RoleBallPlacer::iRoleType()
{
    return ROLE_BALL_PLACER;
}
