#ifndef ROLEBALLPLACER_H
#define ROLEBALLPLACER_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_BALL_PLACER = 520
};

class RoleBallPlacer: public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticReceivePass> tacticReceivePass;
    QSharedPointer<TacticGetBall> tacticGetBall;
    QVector2D supportPosition;

public:
    RoleBallPlacer(AmbienteCampo *_fieldEnv);
    ~RoleBallPlacer();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void setPlacementPosition(QVector2D position);

signals:
    void _requestPass();
};

#endif // ROLEBALLPLACER_H
