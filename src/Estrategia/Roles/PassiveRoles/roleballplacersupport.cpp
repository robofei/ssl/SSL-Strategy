#include "roleballplacersupport.h"

RoleBallPlacerSupport::RoleBallPlacerSupport(AmbienteCampo* _fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_BALL_PLACER_SUPPORT;

    tacticShootToTarget.reset(new TacticShootToTarget(_fieldEnv));

    bAddTactic(tacticShootToTarget.get());
}

RoleBallPlacerSupport::~RoleBallPlacerSupport()
{
}

void RoleBallPlacerSupport::setPlacementPosition(QVector2D position)
{
    this->supportPosition = position;
}

void RoleBallPlacerSupport::vRun()
{

    if (currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString RoleBallPlacerSupport::strName() const
{
    return QString("Ball Placer Support");
}

void RoleBallPlacerSupport::vInitialize()
{
    bInitialized = true;
}

int RoleBallPlacerSupport::iRoleType()
{
    return ROLE_BALL_PLACER_SUPPORT;
}

void RoleBallPlacerSupport::vUpdateReceiver_(const qint8 _receiverID)
{
    if (_receiverID >= 0 && _receiverID < globalConfig.robotsPerTeam)
        iReceiverID = _receiverID;
}

void RoleBallPlacerSupport::vReceivePass()
{
    qInfo() << QString("[Role %1] Sending pass...").arg(strName());
    if (bFinished)
    {
        vResetAllTactics();
        tacticShootToTarget->vInitialize();
        bFinished = false;
    }
    tacticShootToTarget->vShootToPoint(supportPosition);
    bSetTactic(tacticShootToTarget->id());
}
