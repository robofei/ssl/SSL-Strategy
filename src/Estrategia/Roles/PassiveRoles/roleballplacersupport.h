#ifndef ROLEBALLPLACERSUPPORT_H
#define ROLEBALLPLACERSUPPORT_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_BALL_PLACER_SUPPORT = 540
};

class RoleBallPlacerSupport: public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticShootToTarget> tacticShootToTarget;
    QVector2D supportPosition;

    qint8 iReceiverID;

public:
    RoleBallPlacerSupport(AmbienteCampo *_fieldEnv);
    ~RoleBallPlacerSupport();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void setPlacementPosition(QVector2D position);

public slots:
    void vUpdateReceiver_(const qint8 _receiverID);
    void vReceivePass();
};

#endif // ROLEBALLPLACERSUPPORT_H
