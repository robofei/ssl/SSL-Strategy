#include "rolehalt.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

RoleHalt::RoleHalt(AmbienteCampo* _fieldEnv) : Role(_fieldEnv)
{
    iID = ROLE_HALT;

    tacticGoTo.reset(new TacticGoToLookTo(_fieldEnv));
    bAddTactic(tacticGoTo.get());
}

RoleHalt::~RoleHalt()
{
}

void RoleHalt::vRun()
{
    if (bSetTactic(tacticGoTo->id()) || currentTactic != nullptr)
    {
        if (fieldEnvironment->blPegaBola().prediction.hasPrediction)
        {
            QVector2D ballPrediction =
                fieldEnvironment->blPegaBola().prediction.position;

            if (ballPrediction.distanceToPoint(
                    fieldEnvironment->geoCampo->vt2dGoalCenter(
                        fieldEnvironment->opponents->iGetSide())) > 500 &&
                // Auxiliar::iSinal(ballPrediction.x()) ==
                //     fieldEnvironment->allies->iGetSide() &&
                fieldEnvironment->vt2dPosicaoBola().distanceToPoint(
                    robot->vt2dPosicaoAtual()) > 200)
            {
                while (
                    !fieldEnvironment->geoCampo->bIsInsideField(ballPrediction))
                {
                    ballPrediction = Auxiliar::vt2dReajustaPonto(
                        ballPrediction, fieldEnvironment->vt2dPosicaoBola(),
                        100);
                    if (ballPrediction.distanceToPoint(
                            fieldEnvironment->vt2dPosicaoBola()) <= 100)
                    {
                        break;
                    }
                }

                // if (robot->vt2dPosicaoAtual().distanceToPoint(
                //         fieldEnvironment->vt2dPosicaoBola()) > 300)
                // {
                //     robot->vSetaIgnorarBola(bConsideraBola);
                // }
                tacticGoTo->vSetGotoPosition(ballPrediction);
                tacticGoTo->vSetLooktoPosition(
                    fieldEnvironment->vt2dPosicaoBola());
                tacticGoTo->vSetVelocity(2);
                tacticGoTo->vSetAngularVelocity(6);
            }
            else
            {
                tacticGoTo->vSetGotoPosition(robot->vt2dPosicaoAtual());
                tacticGoTo->vSetLooktoPosition(robot->vt2dPontoDirecaoMira());
            }
        }
        else
        {
            tacticGoTo->vSetGotoPosition(robot->vt2dPosicaoAtual());
            tacticGoTo->vSetLooktoPosition(robot->vt2dPontoDirecaoMira());
        }
        currentTactic->vRunTactic();
    }
}

QString RoleHalt::strName() const
{
    return QString("Halt");
}

void RoleHalt::vInitialize()
{
    tacticGoTo->vSetVelocity(0);
    tacticGoTo->vSetAngularVelocity(0);
    bInitialized = true;
}

int RoleHalt::iRoleType()
{
    return ROLE_HALT;
}

void RoleHalt::vCheckRoleFinished()
{
    bFinished = false;
}
