#ifndef ROLEHALT_H
#define ROLEHALT_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_HALT = 200
};

class RoleHalt: public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticGoToLookTo> tacticGoTo;

public:
    RoleHalt(AmbienteCampo *_fieldEnv);
    ~RoleHalt();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;
};

#endif // ROLEHALT_H
