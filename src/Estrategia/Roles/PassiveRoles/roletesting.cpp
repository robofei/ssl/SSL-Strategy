#include "roletesting.h"

RoleTesting::RoleTesting(AmbienteCampo *_fieldEnv)
    : Role(_fieldEnv)
{
    iID = ROLE_TESTING;

    tacticTesting.reset(new TacticGoToLookTo(_fieldEnv));
    bAddTactic(tacticTesting.get());
}

RoleTesting::~RoleTesting()
{

}

void RoleTesting::vRun()
{
    if(bSetTactic(tacticTesting->id()) || currentTactic != nullptr)
    {
        int oppSide = fieldEnvironment->opponents->iGetSide();
        tacticTesting->vSetGotoPosition(QVector2D(zone.ptZoneCenter()) - oppSide * QVector2D(globalConfig.robotDiameter/2.0, 0));
        tacticTesting->vSetLooktoPosition(fieldEnvironment->geoCampo->vt2dGoalCenter(oppSide));
        currentTactic->vRunTactic();

        static bool printed = false;
        if(currentTactic->bIsFinished() && !printed)
        {
            printed = true;
            qInfo() << QString("[Role %1] Tactic %2(%3) finished! Robot[%4]").arg(strName())
                                                                             .arg(currentTactic->strName())
                                                                             .arg(currentTactic->id())
                                                                             .arg(robot->iID());
        }
    }
}

QString RoleTesting::strName() const
{
    return QString("Testing");
}

void RoleTesting::vInitialize()
{
    tacticTesting->vSetVelocity(1.0);
    bInitialized = true;
}

int RoleTesting::iRoleType()
{
    return ROLE_TESTING;
}
