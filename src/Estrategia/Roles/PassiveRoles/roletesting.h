#ifndef ROLETESTING_H
#define ROLETESTING_H

#include "Estrategia/Roles/role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    ROLE_TESTING = 0
};

class RoleTesting : public Role
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<TacticGoToLookTo> tacticTesting;

public:
    RoleTesting(AmbienteCampo *_fieldEnv);
    ~RoleTesting();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
};

#endif // ROLETESTING_H
