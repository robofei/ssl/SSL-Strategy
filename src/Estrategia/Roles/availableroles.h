#ifndef AVAILABLEROLES_H
#define AVAILABLEROLES_H

#include "Estrategia/Roles/PassiveRoles/roletesting.h"              // ID = 0
#include "Estrategia/Roles/AttackRoles/roleattacker.h"              // ID = 20
#include "Estrategia/Roles/DefenseRoles/rolegoalie.h"               // ID = 40
#include "Estrategia/Roles/DefenseRoles/roledeltadefender.h"        // ID = 60
#include "Estrategia/Roles/AttackRoles/rolesupportattacker.h"       // ID = 80
#include "Estrategia/Roles/PassiveRoles/rolehalt.h"                 // ID = 200
#include "Estrategia/Roles/DefenseRoles/roleballchaserstop.h"       // ID = 220
#include "Estrategia/Roles/DefenseRoles/roleballchaserfreekick.h"   // ID = 240
#include "Estrategia/Roles/AttackRoles/rolekickoffreceiver.h"       // ID = 260
#include "Estrategia/Roles/AttackRoles/rolefreekickshooter.h"       // ID = 280
#include "Estrategia/Roles/AttackRoles/rolebasicfreekickattacker.h" // ID = 300
#include "Estrategia/Roles/AttackRoles/rolebasicfreekicksupport.h"  // ID = 320
#include "Estrategia/Roles/DefenseRoles/rolepenaltycone.h"          // ID = 340
#include "Estrategia/Roles/AttackRoles/rolepenaltyattacker.h"       // ID = 360
#include "Estrategia/Roles/AttackRoles/rolenormalballthief.h"       // ID = 380
#include "Estrategia/Roles/AttackRoles/rolenormalattacker.h"        // ID = 460
#include "Estrategia/Roles/AttackRoles/roledeflecter.h"             // ID = 480
#include "Estrategia/Roles/AttackRoles/rolesupportdeflecter.h"      // ID = 500
#include "Estrategia/Roles/PassiveRoles/roleballplacer.h"           // ID = 520
#include "Estrategia/Roles/PassiveRoles/roleballplacersupport.h"    // ID = 540
#include "Estrategia/Roles/DefenseRoles/roleindividualdefender.h"   // ID = 560
#include "Estrategia/Roles/DefenseRoles/rolepressuredefender.h"     // ID = 580
#include "Estrategia/Roles/AttackRoles/roleattackers.h"             // ID = 600
#include "Estrategia/Roles/DefenseRoles/rolenightygoalie.h"         // ID = 620
#include "Estrategia/Roles/AttackRoles/rolereceiver.h"              // ID = 640
#include "Estrategia/Roles/AttackRoles/rolefinisher.h"              // ID = 660
#endif // AVAILABLEROLES_H
