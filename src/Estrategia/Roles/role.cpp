#include "role.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

Role::Role(AmbienteCampo* _fieldEnv, QObject* parent)
    : QObject(parent)
{
    iID = 16000;
    bInitialized = false;
    bFinished = false;
    bAssigned = false;
    bAssignedWarning = false;

    fieldEnvironment = _fieldEnv;
    robot = nullptr;
    rpPriority = ROLE_PRIORITY::OPTIONAL_PRIORITY;

    robotTactics.clear();
    currentTactic = nullptr;
}

void Role::vCheckRoleFinished()
{
    if (bFinished == false && bTacticsFinished())
    {
        bFinished = true;
    }
}

int Role::id() const
{
    return iID;
}

Robo* Role::player() const
{
    return robot;
}

Role::ROLE_PRIORITY Role::priority() const
{
    return rpPriority;
}

QString Role::strPriority() const
{
    return QVariant::fromValue(rpPriority).toString();
}

bool Role::bIsInitialized() const
{
    return bInitialized;
}

bool Role::bIsFinished() const
{
    return bFinished;
}

bool Role::bTacticsFinished() const
{
    bool finished = true;
    QHashIterator<int, Tactic*> tacticIterator(
        robotTactics); // Cuidado!!! Iterator java style

    while (tacticIterator.hasNext())
    {
        tacticIterator.next();
        finished &= tacticIterator.value()->bIsFinished();
    }
    return finished;
}

bool Role::bIsAssigned() const
{
    return bAssigned;
}

void Role::vShowAssignedWarning()
{
    if (!bAssignedWarning)
    {
        bAssignedWarning = true;
        qWarning() << QString("[Role %1] Not assigned!!!").arg(strName());
    }
}

void Role::vClearAssignedWarning()
{
    bAssignedWarning = false;
}

void Role::vSetPriority(ROLE_PRIORITY _priority)
{
    qInfo() << QString("[Role %1] Prioridade mudou para: %2")
                   .arg(strName())
                   .arg(QVariant::fromValue(_priority).toString());
    ;
    rpPriority = _priority;
}

bool Role::bMakeDuplicate(int _duplicateN)
{
    if (_duplicateN > 20)
    {
        qWarning() << qPrintable(
            QString("[Role %1] tentando fazer duplicada fora do intervalo entre"
                    " 0 e 20")
                .arg(strName()));
        return false;
    }
    iID += _duplicateN;
    return true;
}

bool Role::bAddTactic(Tactic* _tactic)
{
    bool ok = false, contains;
    if (_tactic != nullptr)
    {
        contains = robotTactics.contains(_tactic->id());
        if (!contains && _tactic->id() != -1)
        {
            robotTactics.insert(_tactic->id(), _tactic);

            ok = true;
        }
        else
        {
            qWarning() << QString(
                              "[Role %1] Tentando adicionar tática inválida, "
                              "Tactic(%2 = %3) | Reason: Contains(%4)")
                              .arg(strName())
                              .arg(_tactic->id())
                              .arg(_tactic->strName())
                              .arg(contains);
        }
    }
    else
    {
        qWarning() << QString("[Role %1] Tentando adiciona tática nula!")
                          .arg(strName());
    }
    return ok;
}

bool Role::bSetTactic(int _tacticID)
{
    bool ok = false;
    if (robotTactics.contains(_tacticID))
    {
        if (currentTactic == nullptr || currentTactic->id() != _tacticID)
        {
            if (currentTactic != nullptr)
            {
                currentTactic->vForceFinished();
            }

            currentTactic = robotTactics.value(_tacticID);
            if (currentTactic->bIsFinished())
            {
                currentTactic->vResetTactic();
            }
            if (!currentTactic->bIsInitialized() ||
                currentTactic->bIsFinished())
            {
                currentTactic->vInitialize();
            }
        }

        if (currentTactic->player() == nullptr ||
            currentTactic->player()->iID() != robot->iID())
        {
            currentTactic->vSetRobot(robot);
        }

        ok = true;
    }
    else
    {
        qWarning() << QString("[Role %1] Tatica nao adicionada, use "
                              "bAddTactic() | Tactic ID(%1)")
                          .arg(_tacticID);
    }
    return ok;
}

bool Role::bCheckRoleOK()
{
    if (!bInitialized)
    {
        qCritical() << QString("[Role %1] Trying to run without initializing")
                           .arg(strName());
        return false;
    }
    if (robotTactics.isEmpty())
    {
        qWarning() << QString("[Role %1] Trying to run without tactics")
                          .arg(strName());
        return false;
    }
    if (robot == nullptr)
    {
        qCritical() << QString("[Role %1] Trying to run without a robot")
                           .arg(strName());
        return false;
    }
    //     if(currentTactic == nullptr)
    //     {
    //         qWarning() << QString("[Role(%1)] Trying to run with null
    //         tactic").arg(strName()); return false;
    //     }
    return true;
}

void Role::vRunRole()
{
    if (!bCheckRoleOK())
    {
        const QString color = "yellow";
        robot->vSetRole(Auxiliar::strColorText(strName(), color));
        return;
    }

    vRun();
    vCheckRoleFinished();

    if (robot != nullptr)
    {
        const QString color = bIsFinished() ? "green" : "red";
        robot->vSetRole(Auxiliar::strColorText(strName(), color));
        robot->vSetZone(zone);
    }
}

void Role::vSetRobot(Robo* _robot)
{

    if (robot == nullptr)
    {
        qInfo() << QString("[Role %1] Setando Robô[%2]!")
                       .arg(strName())
                       .arg(_robot->iID());
    }
    else if (robot->iID() != _robot->iID())
    {
        qInfo() << QString("[Role %1] Mudando Robô[%2] -> Robô[%3]!")
                       .arg(strName())
                       .arg(robot->iID())
                       .arg(_robot->iID());
    }
    robot = _robot;
    if (!bAssigned)
    {
        qInfo() << QString("[Role %1] Removendo do Robô[%2]")
                       .arg(strName())
                       .arg(robot->iID());
        robot->vStay();
        robot->vDesligaChute();
        robot->vDesligaRoller();
        robot->vSetRole("");
        robot->vSetTactic("");
        robot->vSetSkill("");
        emit _vRoleRemoved(robot->iID());
    }
    bAssigned = true;
    robot->vSetRole(strName());
    emit _vRoleAssigned(robot->iID());

    if (currentTactic != nullptr)
        currentTactic->vSetRobot(_robot);
}

void Role::vReset()
{
    bAssigned = false;
    bInitialized = false;
    bFinished = false;
    currentTactic = nullptr;
    if (robot != nullptr)
    {
        robot->vStay();
        robot->vDesligaChute();
        robot->vDesligaRoller();
        robot->vSetRole("");
        robot->vSetTactic("");
        robot->vSetSkill("");
    }
    vClearAssignedWarning();
    vResetAllTactics();
}

void Role::vResetAllTactics()
{
    QHashIterator<int, Tactic*> tacticIterator(
        robotTactics); // Cuidado!!! Iterator java style
    if (robotTactics.size() > 0)
    {
        while (tacticIterator.hasNext())
        {
            try
            {
                tacticIterator.next();
                tacticIterator.value()->vResetTactic();
            }
            catch (...)
            {
                qWarning() << QString("[Role %1] Exception in vResetAllTactics")
                                  .arg(strName());
            }
        }
    }
    qInfo() << QString("[Role %1] Reset all tactics!").arg(strName());
}
