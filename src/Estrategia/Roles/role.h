#ifndef ROLE_H
#define ROLE_H

#include <QObject>
#include <QString>
#include <QHash>
#include <QHashIterator>
#include <QSharedPointer>

#include "Ambiente/futbolenvironment.h"

#include "Estrategia/Skills/skill.h"
#include "Estrategia/Tactics/tactic.h"

/**
 * @brief Classe genérica para implementação de uma role.
 *
 * Em uma Role ficam definidas as táticas (Tactic) que um robô deve
 * executar para realizar uma Play. Para isso, é necessário atribuir as
 * táticas que serão utilizadas (Role::robotTactics).
 */
class Role : public QObject
{
    Q_OBJECT
    QHash<int, Tactic*> robotTactics;

public:
    /**
     * @brief Prioridade das roles. No caso em que existem mais roles do que robôs
     * disponíveis, as roles com maior prioridade serão atribuídas primeiro.
     *
     * Por padrão a prioridade é opcional.
     */
    enum ROLE_PRIORITY
    {
        MAX_PRIORITY      = 3, /**< Role que deve ser atribuída OBRIGATORIAMENTE. */
        MEDIUM_PRIORITY   = 2, /**< Caso não seja atribuída irá gerar um aviso. */
        LOW_PRIORITY      = 1, /**< Caso não seja atribuída irá gerar um aviso. */
        OPTIONAL_PRIORITY = 0  /**< Role que, caso não seja atribuída não irá
                                 prejudicar a jogada. */
    };
    Q_ENUM(ROLE_PRIORITY)

protected:
    int iID;
    bool bInitialized;
    bool bFinished;
    bool bAssigned;
    bool bAssignedWarning;
    int iType;
    ROLE_PRIORITY rpPriority;

    AmbienteCampo *fieldEnvironment;
    Robo *robot;

    Tactic *currentTactic;

    virtual void vRun() = 0;

public:
    FieldZone zone;

    explicit Role(AmbienteCampo *_fieldEnv, QObject *parent = nullptr);
    virtual ~Role() { };

    virtual QString strName() const = 0;
    virtual void vInitialize() = 0;
    virtual int iRoleType() = 0;
    virtual void vCheckRoleFinished();

    int id() const;
    Robo* player() const;
    ROLE_PRIORITY priority() const;
    QString strPriority() const;
    bool bIsInitialized() const;
    bool bIsFinished() const;
    bool bTacticsFinished() const;
    bool bIsAssigned() const;
    void vShowAssignedWarning();
    void vClearAssignedWarning();
    void vSetPriority(ROLE_PRIORITY _priority);
    bool bMakeDuplicate(int _duplicateN);
    bool bAddTactic(Tactic *_tactic);
    bool bSetTactic(int _tacticID);

    bool bCheckRoleOK();
    void vRunRole();

    void vSetRobot(Robo *_robot);
    void vReset();
    void vResetAllTactics();

signals:
    void _vRoleAssigned(const qint8 _robotID);
    void _vRoleRemoved(const qint8 _robotID);
};

#endif // ROLE_H
