#ifndef AVAILABLESKILLS_H
#define AVAILABLESKILLS_H

#include "Debug/debugtypes.h"

#include "Estrategia/Skills/skillgoto.h"          // ID = 0
#include "Estrategia/Skills/skilllookto.h"        // ID = 20
#include "Estrategia/Skills/skillshoot.h"         // ID = 40
#include "Estrategia/Skills/skillgotozone.h"      // ID = 60
#include "Estrategia/Skills/skillmovetopose.h"    // ID = 80
#include "Estrategia/Skills/skillgetstaticball.h" // ID = 100
#include "Estrategia/Skills/skillgetoppball.h"    // ID = 120
#include "Estrategia/Skills/skillpullball.h"      // ID = 140
#include "Estrategia/Skills/skillwaitball.h"      // ID = 160
#include "Estrategia/Skills/skillaim.h"           // ID = 180

#endif // AVAILABLESKILLS_H
