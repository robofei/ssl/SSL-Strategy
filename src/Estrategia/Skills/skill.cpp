#include "skill.h"

void Skill::vCheckRobotDestination()
{
    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D centerAlly = env.geoCampo->vt2dGoalCenter(env.allies->iGetSide());
    int safetyCounter = 0;

    QVector2D currentDest = robot->vt2dDestino();
    while(env.bPontoDentroAreaDefesaAliado(currentDest))
    {
        currentDest = robot->vt2dDestino();
        robot->vSetaDestino(currentDest + globalConfig.robotDiameter * (currentDest - centerAlly).normalized());

        if(safetyCounter++ > 15)
            break;
    }

    const QVector2D centerOpp = env.geoCampo->vt2dGoalCenter(env.opponents->iGetSide());
    safetyCounter = 0;

    while(env.bPontoDentroAreaDefesaOponente(robot->vt2dDestino()))
    {
        currentDest = robot->vt2dDestino();
        robot->vSetaDestino(currentDest + 100 * (currentDest - centerOpp).normalized());

        if(safetyCounter++ > 15)
            break;
    }
}

Skill::Skill(AmbienteCampo *_fieldEnv)
{
    iID = 32000;
    bInitialized = false;
    bFinished    = false;

    fieldEnvironment = _fieldEnv;
    robot = nullptr;
}


int Skill::id() const
{
    return iID;
}

Robo* Skill::player() const
{
    return robot;
}

bool Skill::bIsInitialized() const
{
    return bInitialized;
}

bool Skill::bIsFinished() const
{
    return bFinished;
}

void Skill::vResetSkill()
{
    bFinished = false;
    bInitialized = false;
}

void Skill::vSetRobot(Robo *_robot)
{
    if(robot != nullptr && _robot->iID() != robot->iID())
    {
//         qInfo() << QString("[Skill (%1)] Trocando robôs %2 -> %3").arg(strName())
//                                                                   .arg(robot->iID())
//                                                                   .arg(_robot->iID());
        robot->vStay();
        robot->vAndarXY(0, 0, 0, 0);
        robot->vDesligaChute();
        robot->vDesligaRoller();
        robot->path->vSetPath({}, 0);
        robot->vSetaIgnorarBola(false);
    }
    robot = _robot;
}

bool Skill::bCheckSkillOK()
{
    if(!bInitialized)
    {
        qCritical() << QString("[Skill (%1)] Trying to run without initializing").arg(strName());
        return false;
    }
    if(robot == nullptr)
    {
        qWarning() << QString("[Skill (%1)] Trying to run with null robot").arg(strName());
        return false;
    }
    return true;
}

void Skill::vRunSkill()
{
    if(!bCheckSkillOK())
    {
        return;
    }

    vRun();

    if(robot != nullptr)
    {
        const QString color = bIsFinished() ? "green" : "red";
        robot->vSetSkill(Auxiliar::strColorText(strName(), color));

        if(robot->iID() != fieldEnvironment->allies->iGetGoalieID())
            vCheckRobotDestination();
    }
}
