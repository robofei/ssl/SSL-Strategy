#ifndef SKILL_H
#define SKILL_H

#include <QString>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

/**
 * @brief Classe genérica para a implementação de habilidades do robô.
 *
 * Cada Skill é uma micro habilidade do robô, por exemplo:
 * - Andar até um ponto
 * - Olhar para um ponto
 * - Contornar um ponto
 * - Chutar para o gol
 */
class Skill
{
private:
    /**
     * @brief Corrige o destino do robô caso este seja dentro de alguama área
     * de defesa.
     */
    void vCheckRobotDestination();

protected:
    int iID;
    bool bInitialized;
    bool bFinished;

    AmbienteCampo *fieldEnvironment;
    Robo *robot;

    virtual void vRun() = 0;

public:
    Skill(AmbienteCampo *_fieldEnv);
    virtual ~Skill() { };

    virtual QString strName() const = 0;
    virtual void vInitialize() = 0;

    int id() const;
    Robo* player() const;
    bool bIsInitialized() const;
    bool bIsFinished() const;
    virtual void vResetSkill();

    bool bCheckSkillOK();
    void vRunSkill();

    void vSetRobot(Robo *_robot);
};

#endif // SKILL_H
