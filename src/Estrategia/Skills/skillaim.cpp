#include "skillaim.h"

SkillAim::SkillAim(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_AIM;

    setGoal();
}

SkillAim::~SkillAim()
{

}

void SkillAim::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();
    const QVector2D currentPos = robot->vt2dPosicaoAtual();

    if(env.bRoboEstaComABola(robot->iID()))
    {
        robot->vSetaPontoAnguloDestino(aimPoint);
        robot->vSetaDestino(currentPos);
    }
    else
    {
        robot->vSetaPontoAnguloDestino(ballPos);
        robot->vSetaDestino(ballPos + 80*(currentPos - ballPos).normalized());
    }

    robot->vLigaRoller(100);
    robot->vDesligaChute();
    robot->vSetMaxAngularVelocity(globalConfig.robotVelocities.linear.limit);
    robot->vSetMaxAngularVelocity(globalConfig.robotVelocities.angular.limit*.15);

    switch(targetType)
    {
        case CUSTOM:
            bFinished = robot->bPodeChutar(aimPoint - ballPos, qMax(fAnguloDeErroChuteNormal,
                                                                    fAnguloDeErroPasseNormal));
            break;
        case ROBOT:
            bFinished = robot->bPodeChutar(aimPoint - ballPos, fAnguloDeErroPasseNormal);
            break;
        case GOAL:
            bFinished = robot->bPodeChutar(aimPoint - ballPos, fAnguloDeErroChuteNormal);
            break;
    }
}

void SkillAim::setAimPoint(const QVector2D& newAimPoint)
{
    aimPoint = newAimPoint;

    targetType = CUSTOM;
}

void SkillAim::setReceiver(qint8 recId)
{
    const AmbienteCampo& env = *fieldEnvironment;

    aimPoint = env.allies->getCPlayer(recId)->vt2dPosicaoAtual();

    targetType = ROBOT;
}

void SkillAim::setGoal()
{
    const AmbienteCampo& env = *fieldEnvironment;

    aimPoint = Auxiliar::vt2dCentroLivreGolAdversario(
        env.vt2dPosicaoBola(),
        env.allies->iGetSide(),
        env.geoCampo->szGoal().height(),
        env.geoCampo->szField(),
        env.vt3dPegaPosicaoTodosObjetos(otOponente, true));

    targetType = GOAL;
}

QString SkillAim::strName() const
{
    return QString("Aim");
}

void SkillAim::vInitialize()
{
    bInitialized = true;
    bFinished = false;
}
