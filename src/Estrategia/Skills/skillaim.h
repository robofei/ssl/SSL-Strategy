#ifndef SKILLAIM_H
#define SKILLAIM_H

#include "skill.h"

/**
 * @brief Define o ID inicial das instâncias dessa skill.
 *
 * @note Esse enum é de implementação obrigatória!
 */
enum
{
    SKILL_AIM = 180
};

class SkillAim : public Skill
{
private:
    /**
     * @brief Roda a skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vRun() override;

    // Variáveis
    QVector2D aimPoint;

    enum TARGET_TYPE
    {
        CUSTOM,
        ROBOT,
        GOAL
    } targetType;

public:
    SkillAim(AmbienteCampo *_fieldEnv);
    ~SkillAim();

    /**
     * @brief Retorna o nome da skill.
     *
     * @note Esse método é de implementação obrigatória!
     *
     * @return
     */
    QString strName() const override;

    /**
     * @brief Inicializa parâmetros internos da skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vInitialize() override;

    void setAimPoint(const QVector2D& newAimPoint);
    void setReceiver(qint8 recId);
    void setGoal();
};

#endif // SKILLAIM_H
