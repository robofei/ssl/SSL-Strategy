#include "skillgetoppball.h"

SkillGetOppBall::SkillGetOppBall(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_GETOPPBALL;
}

SkillGetOppBall::~SkillGetOppBall()
{

}

void SkillGetOppBall::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();
    const QVector2D currentPos = robot->vt2dPosicaoAtual();
    const QVector2D destiny = ballPos +
                             .8*globalConfig.robotDistanceToBall*(currentPos - ballPos).normalized();

    robot->vSetaIgnorarBola(true);

    robot->vSetaDestino(destiny);
    robot->vSetaPontoAnguloDestino(ballPos);
    robot->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
    robot->vSetMaxAngularVelocity(globalConfig.robotVelocities.angular.limit);
    robot->vLigaRoller(100);
    robot->vDesligaChute();

    bFinished = env.bRoboEstaComABola(robot->iID());
}

QString SkillGetOppBall::strName() const
{
    return QString("GetOppBall");
}

void SkillGetOppBall::vInitialize()
{
    bInitialized = true;
    bFinished = false;
}
