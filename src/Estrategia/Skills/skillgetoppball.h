#ifndef SKILLGETOPPBALL_H
#define SKILLGETOPPBALL_H

#include "skill.h"

/**
 * @brief Define o ID inicial das instâncias dessa skill.
 *
 * @note Esse enum é de implementação obrigatória!
 */
enum
{
    SKILL_GETOPPBALL = 120
};

class SkillGetOppBall : public Skill
{
private:
    /**
     * @brief Roda a skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vRun() override;

    // Variáveis

public:
    SkillGetOppBall(AmbienteCampo *_fieldEnv);
    ~SkillGetOppBall();

    /**
     * @brief Retorna o nome da skill.
     *
     * @note Esse método é de implementação obrigatória!
     *
     * @return
     */
    QString strName() const override;

    /**
     * @brief Inicializa parâmetros internos da skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vInitialize() override;
};

#endif // SKILLGETOPPBALL_H
