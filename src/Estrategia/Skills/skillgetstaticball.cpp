#include "skillgetstaticball.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

SkillGetStaticBall::SkillGetStaticBall(AmbienteCampo *_fieldEnv) :
    Skill(_fieldEnv)
{
    iID = SKILL_GET_STATIC_BALL;
    fileRobotBallDistance.reset(new QFile);
}

SkillGetStaticBall::~SkillGetStaticBall()
{

}

void SkillGetStaticBall::vRun()
{
    const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
//     const QVector2D pos = Auxiliar::vt2dCalculaPontoCobranca(vt2dTarget,
//                                                        ballPos,
//                                                        fMinRobotBallDistance);
    robot->vSetaIgnorarBola(bNaoConsideraBola);
    robot->vLigaRoller(60);

    vUpdateMinDistance();
    vUpdateOptimumVelocity();

    robot->vSetaDestino(robot->vt2dPlaceRollerAt(ballPos));
    robot->vSetaPontoAnguloDestino(ballPos);
    robot->vSetMaxLinearVelocity(fOptimumVelocity);

    if(ballPos.distanceToPoint(robot->vt2dPosicaoAtual()) > 1e3)
    {
        robot->vSetMaxLinearVelocity(fOptimumVelocity * 3);
    }
    robot->vSetMaxAngularVelocity(globalConfig.robotVelocities.angular.limit);

    const float ballDist = vt2dBallStart.distanceToPoint(ballPos);
    if((robot->bKickSensor() || ballDist > 10) && !bFinished)
    {
        bFinished = true;
    }
}

void SkillGetStaticBall::vUpdateMinDistance()
{
    fMinRobotBallDistance = 100;
    if(robot->bKickSensor())
    {
        const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
        float currentDistance = robot->vt2dPosicaoAtual().distanceToPoint(ballPos);

        // new = old + K*(target - old)
        fMinRobotBallDistance = fMinRobotBallDistance +
            0.2*(currentDistance - fMinRobotBallDistance);
        qInfo() << QString("[Skill %1] Robot distance = %2").arg(strName())
                                                            .arg(currentDistance);
    }
    else if(robot->bArrived())
    {
        const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
        float currentDistance = robot->vt2dPosicaoAtual().distanceToPoint(ballPos) - 20;

        // new = old + K*(target - old)
        fMinRobotBallDistance = fMinRobotBallDistance +
            0.2*(currentDistance - fMinRobotBallDistance);
    }

    if(fMinRobotBallDistance > 120)
    {
        fMinRobotBallDistance = 120;
    }

}

void SkillGetStaticBall::vUpdateOptimumVelocity()
{
    const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
    const float ballDist = vt2dBallStart.distanceToPoint(ballPos);

    // Double touch se moveu a bola mais de 50mm
    if(ballDist > 50)
    {
        qInfo() << QString("[Skill %1] Robo[%2] double touch!").arg(strName())
                                                               .arg(robot->iID());
        fOptimumVelocity *= 0.90;
        vt2dBallStart     = ballPos;
    }
//     if(robot->bArrived() && ballDist > 1.1*fMinRobotBallDistance)
//     {
//         fOptimumVelocity *= 1.05;
//     }

    if(fOptimumVelocity < 0.5)
        fOptimumVelocity = 0.5;
}

void SkillGetStaticBall::vReadSaveFile()
{
    QDir fileDir = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

    if(!fileDir.cd("RobotBallDist"))
    {
        fileDir.mkdir("RobotBallDist");
        fileDir.cd("RobotBallDist");
    }

    const QString filename = QString("Robot_%1_Ball.cfg").arg(robot->iID());
    fileRobotBallDistance->setFileName(fileDir.absoluteFilePath(filename));

    if(fileRobotBallDistance->open(QIODevice::ReadOnly))
    {
        float distance = fileRobotBallDistance->readLine().toFloat();
        float velocity = fileRobotBallDistance->readLine().toFloat();

        if(distance > 0 && distance < globalConfig.robotDiameter)
        {
            fMinRobotBallDistance = distance;
        }
        else
        {
            fMinRobotBallDistance = globalConfig.robotDistanceToBall;
        }

        if(velocity > 0 && velocity < globalConfig.robotVelocities.linear.limit)
        {
            fOptimumVelocity = velocity;
        }
        else
        {
            fOptimumVelocity = 1.0;
        }
        fileRobotBallDistance->close();
    }
}

void SkillGetStaticBall::vSaveDistanceFile()
{
    if(!fileRobotBallDistance->isOpen())
    {
        if(fileRobotBallDistance->open(QIODevice::WriteOnly | QIODevice::Truncate))
        {
            QByteArray data;
            data.setNum(fMinRobotBallDistance);
            data.append('\n');
            fileRobotBallDistance->write(data);
            data.setNum(fOptimumVelocity);
            fileRobotBallDistance->write(data);

            fileRobotBallDistance->close();
        }
        else
        {
            qDebug() << fileRobotBallDistance->errorString();
        }
    }

}

QString SkillGetStaticBall::strName() const
{
    return QString("Get S. Ball");
}

void SkillGetStaticBall::vInitialize()
{
    bInitialized  = true;
    bFinished     = false;
    vt2dBallStart = vt2dTarget = fieldEnvironment->vt2dPosicaoBola();
    vReadSaveFile();
}

void SkillGetStaticBall::vResetSkill()
{
    if(bInitialized)
    {
        vSaveDistanceFile();
    }
    bInitialized = false;
    bFinished    = false;
}

void SkillGetStaticBall::vSetTarget(QVector2D _target)
{
    vt2dTarget = _target;
}
