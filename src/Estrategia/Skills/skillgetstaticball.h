#ifndef SKILLGETSTATICBALL_H
#define SKILLGETSTATICBALL_H

#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include "skill.h"

enum
{
    SKILL_GET_STATIC_BALL = 100
};

class SkillGetStaticBall : public Skill
{
private:
    void vRun() override;

    float fMinRobotBallDistance;
    void vUpdateMinDistance();

    float fOptimumVelocity;
    void vUpdateOptimumVelocity();

    QScopedPointer<QFile> fileRobotBallDistance;
    void vReadSaveFile();
    void vSaveDistanceFile();

    QVector2D vt2dTarget;
    QVector2D vt2dBallStart;

public:
    SkillGetStaticBall(AmbienteCampo *_fieldEnv);
    ~SkillGetStaticBall();

    QString strName() const override;
    void vInitialize() override;
    void vResetSkill() override;

    void vSetTarget(QVector2D _target);
};

#endif // SKILLGETSTATICBALL_H
