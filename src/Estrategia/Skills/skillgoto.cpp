#include "skillgoto.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

SkillGoTo::SkillGoTo(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_GO_TO;

    fVelocity = 0;
    vt2dGotoPosition = QVector2D(0,
                                 fieldEnvironment->geoCampo->szField().height()/2.0);
}

SkillGoTo::~SkillGoTo()
{

}

void SkillGoTo::vRun()
{
    robot->vSetaDestino(vt2dGotoPosition);
    robot->vSetMaxLinearVelocity(fVelocity);

    if(robot->vt2dPosicaoAtual().distanceToPoint(vt2dGotoPosition) < globalConfig.robotDiameter/8.0 &&
        robot->dModuloVelocidade() < 0.5 &&
            bFinished == false)
    {
        bFinished = true;
//         qInfo() << QString("[Skill (%1)] Robô [%2] chegou!").arg(strName())
//                                                             .arg(robot->iID());
    }
}

QString SkillGoTo::strName() const
{
    return QString("Go To");
}

void SkillGoTo::vInitialize()
{
    bInitialized = true;
    bFinished = false;
}

bool SkillGoTo::vSetGotoPosition(QVector2D _position)
{
    // if(!fieldEnvironment->geoCampo->bIsInsideField(_position))
    // {
    //     return false;
    // }

    if(_position.distanceToPoint(vt2dGotoPosition) > globalConfig.robotDiameter/8.0)
    {
        bFinished = false;
    }

    vt2dGotoPosition = _position;
    return true;
}

void SkillGoTo::vSetVelocity(float _velocity)
{
    fVelocity = _velocity;
}
