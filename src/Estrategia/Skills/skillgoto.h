#ifndef SKILLGOTO_H
#define SKILLGOTO_H

#include "skill.h"

/**
 * @brief Define o ID inicial das instâncias dessa skill.
 *
 * @note Esse enum é de implementação obrigatória!
 */
enum
{
    SKILL_GO_TO = 0
};

/**
 * @brief Skill que movimenta o robô até uma determinada posição (x, y) com
 * orientação constante à um ponto.
 */
class SkillGoTo : public Skill
{
private:
    /**
     * @brief Roda a skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vRun() override;

    QVector2D vt2dGotoPosition;
    float fVelocity;

public:
    SkillGoTo(AmbienteCampo *_fieldEnv);
    ~SkillGoTo();

    /**
     * @brief Retorna o nome da skill.
     *
     * @note Esse método é de implementação obrigatória!
     *
     * @return
     */
    QString strName() const override;

    /**
     * @brief Inicializa parâmetros internos da skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vInitialize() override;

    bool vSetGotoPosition(QVector2D _position);
    void vSetVelocity(float _velocity);
};

#endif // SKILLGOTO_H
