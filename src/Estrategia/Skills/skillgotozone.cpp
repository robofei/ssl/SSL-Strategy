#include "skillgotozone.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Estrategia/Skills/skill.h"
#include <cstring>

SkillGoToZone::SkillGoToZone(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_GO_TO_ZONE;
}

SkillGoToZone::~SkillGoToZone()
{

}

void SkillGoToZone::vRun()
{
    const QVector2D robotPos = robot->vt2dPosicaoAtual();
    const QVector2D zoneCenter = QVector2D(zone.ptZoneCenter());
    const QVector2D zoneEdge = Auxiliar::vt2dReajustaPonto(zoneCenter,
                                                           robotPos,
                                                           0.9*zone.iSize());
    robot->vSetaDestino(zoneEdge);
    robot->vSetMaxLinearVelocity(fVelocity);

    if(zoneCenter.distanceToPoint(fieldEnvironment->vt2dPosicaoBola()) < 100 &&
            zoneEdge.distanceToPoint(robotPos) < 1e3 && fVelocity > 1)
    {
        robot->vSetMaxLinearVelocity(fVelocity / 2);
    }

    if(vt2dLookTo)
    {
        robot->vSetaPontoAnguloDestino(*vt2dLookTo);
    }

    if(zone.bInsideZone(robot->vt2dPosicaoAtual()) && bFinished == false)
    {
        bFinished = true;
        robot->vStay();
//         qInfo() << QString("[Skill (%1)] Robô [%2] chegou!").arg(strName())
//                                                             .arg(robot->iID());
    }
}

QString SkillGoToZone::strName() const
{
    return QString("Go To Zone");
}

void SkillGoToZone::vInitialize()
{
    bInitialized = true;
}

void SkillGoToZone::vSetZone(const FieldZone &_zone)
{
    if(!fieldEnvironment->geoCampo->bIsInsideField(QVector2D(_zone.ptZoneCenter())))
    {
        return;
    }

    if(zone.bIsConfigured() &&
            QVector2D(zone.ptZoneCenter()).distanceToPoint(QVector2D(_zone.ptZoneCenter())) > 100)
    {
        bFinished = false;
    }

    if(_zone.ztZoneType() == CIRCLE)
    {
        zone.vConfigZone(_zone.circleGetZone().getCenter(),
                         _zone.circleGetZone().getRadius());
    }
    else if(_zone.ztZoneType() == RECTANGLE)
    {
        zone.vConfigZone(_zone.rctGetZone());
    }
    else
    {
        qWarning() << QString("[Skill %1] Zona inválida!").arg(strName());
    }
}

void SkillGoToZone::vSetLookToPoint(QVector2D _lookPoint)
{
    vt2dLookTo.reset( new QVector2D(_lookPoint) );
}

void SkillGoToZone::vSetVelocity(float _velocity)
{
    fVelocity = _velocity;
}
