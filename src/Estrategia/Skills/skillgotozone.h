#ifndef SKILLGOTOZONE_H
#define SKILLGOTOZONE_H

#include "skill.h"

#include "Ambiente/fieldzone.h"

enum
{
    SKILL_GO_TO_ZONE = 60
};

class SkillGoToZone : public Skill
{
private:
    void vRun() override;

    FieldZone zone;
    float fVelocity;
    QScopedPointer<QVector2D> vt2dLookTo;

public:
    SkillGoToZone(AmbienteCampo *_fieldEnv);
    ~SkillGoToZone();
    static int iInstanceCount;

    QString strName() const override;
    void vInitialize() override;

    void vSetZone(const FieldZone &_zone);
    void vSetLookToPoint(QVector2D _lookPoint);

    void vSetVelocity(float _velocity);
};

#endif // SKILLGOTOZONE_H
