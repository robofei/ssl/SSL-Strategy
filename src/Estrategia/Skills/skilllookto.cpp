#include "skilllookto.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

SkillLookTo::SkillLookTo(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_LOOK_TO;

    vt2dLookPoint = QVector2D(0,0);
    fVelocity = 0;
}

SkillLookTo::~SkillLookTo()
{

}

void SkillLookTo::vRun()
{
    robot->vSetaPontoAnguloDestino(vt2dLookPoint);
    robot->vSetMaxAngularVelocity(fVelocity);

    QVector2D currentOrientation = robot->vt2dOrientation(),
              targetOrientation  = vt2dLookPoint - robot->vt2dPosicaoAtual();
    const double error = Auxiliar::dAnguloVetor1Vetor2(currentOrientation,
                                                       targetOrientation);

    if(error < 5 && bFinished == false)
    {
        bFinished = true;
//         qInfo() << QString("[Skill (%1)] Robô [%2] está alinhado!").arg(strName())
//                                                                    .arg(robot->iID());
    }
}

QString SkillLookTo::strName() const
{
    return QString("Look To");
}

void SkillLookTo::vInitialize()
{
    bInitialized = true;
    bFinished = false;
}

void SkillLookTo::vSetLookPoint(QVector2D _lookPoint)
{
    if(_lookPoint.distanceToPoint(vt2dLookPoint) > 2)
    {
        bFinished = false;
    }
    vt2dLookPoint = _lookPoint;
}

void SkillLookTo::vSetVelocity(float _velocity)
{
    fVelocity = _velocity;
}
