#ifndef SKILLLOOKTO_H
#define SKILLLOOKTO_H

#include "skill.h"

enum
{
    SKILL_LOOK_TO = 20
};

class SkillLookTo : public Skill
{
private:
    void vRun() override;

    QVector2D vt2dLookPoint;
    float fVelocity;

public:
    SkillLookTo(AmbienteCampo *_fieldEnv);
    ~SkillLookTo();

    QString strName() const override;
    void vInitialize() override;

    void vSetLookPoint(QVector2D _lookPoint);
    void vSetVelocity(float _velocity);
};

#endif // SKILLLOOKTO_H
