#include "skillmovetopose.h"

SkillMoveToPose::SkillMoveToPose(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_MOVE_TO_POSE;

    fVelocity = fAngularVelocity = 0;
    fMaxLinearError = globalConfig.robotDiameter/4.0;
    fMaxAngularError = 5; // graus

    vt2dGotoPosition = QVector2D(0,
                                 fieldEnvironment->geoCampo->szField().height()/2.0);
    vt2dLookPoint = QVector2D(0, 0);
}

SkillMoveToPose::~SkillMoveToPose()
{

}

void SkillMoveToPose::vRun()
{
    robot->vSetaDestino(vt2dGotoPosition);
    robot->vSetaPontoAnguloDestino(vt2dLookPoint);

    robot->vSetMaxLinearVelocity(fVelocity);
    robot->vSetMaxAngularVelocity(fAngularVelocity);


    QVector2D currentOrientation = robot->vt2dOrientation(),
              targetOrientation  = vt2dLookPoint - robot->vt2dPosicaoAtual();
    const float angularError = Auxiliar::dAnguloVetor1Vetor2(currentOrientation,
                                                             targetOrientation);

    if(robot->bArrived(fMaxLinearError) && angularError < fMaxAngularError &&
       bFinished == false)
    {
        bFinished = true;
    }
}

QString SkillMoveToPose::strName() const
{
    return QString("Move To Pose");
}

void SkillMoveToPose::vInitialize()
{
    bInitialized = true;
    bFinished = false;
}

void SkillMoveToPose::vSetGotoPosition(QVector2D _position)
{
    if(!fieldEnvironment->geoCampo->bIsInsideField(_position))
    {
        qWarning() << "[Skill Move To Pose] Destino fora do campo!";
        return;
    }

    if(_position.distanceToPoint(vt2dGotoPosition) > fMaxLinearError/2.0)
    {
        bFinished = false;
    }
    vt2dGotoPosition = _position;
}

void SkillMoveToPose::vSetLookPoint(QVector2D _lookPoint)
{
    if(_lookPoint.distanceToPoint(vt2dLookPoint) > fMaxAngularError/2.0)
    {
        bFinished = false;
    }
    vt2dLookPoint = _lookPoint;
}

void SkillMoveToPose::vSetVelocity(float _velocity)
{
    fVelocity = _velocity;
}

void SkillMoveToPose::vSetAngularVelocity(float _velocity)
{
    fAngularVelocity = _velocity;
}

void SkillMoveToPose::vIgnoreBall()
{
    robot->vSetaIgnorarBola(true);
}

void SkillMoveToPose::vConsiderBall()
{
    robot->vSetaIgnorarBola(false);
}
