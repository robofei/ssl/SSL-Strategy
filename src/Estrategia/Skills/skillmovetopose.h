#ifndef SKILLMOVETOPOSE_H
#define SKILLMOVETOPOSE_H

#include "skill.h"

enum
{
    SKILL_MOVE_TO_POSE = 80
};

/**
 * @brief Movimenta o robô até a pose (posição + ângulo) desejados de forma
 * simultânea.
 *
 * Para movimentar sem rotacionar ver: SkillGoTo.
 * Para rotacionar sem movimentar ver: SkillLookTo.
 */
class SkillMoveToPose : public Skill
{
private:
    /**
     * @brief Roda a skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vRun() override;

    QVector2D vt2dGotoPosition;
    float fVelocity;
    QVector2D vt2dLookPoint;
    float fAngularVelocity;

    float fMaxLinearError;
    float fMaxAngularError;

public:
    SkillMoveToPose(AmbienteCampo *_fieldEnv);
    ~SkillMoveToPose();

    /**
     * @brief Retorna o nome da skill.
     *
     * @note Esse método é de implementação obrigatória!
     *
     * @return
     */
    QString strName() const override;

    /**
     * @brief Inicializa parâmetros internos da skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vInitialize() override;

    void vSetGotoPosition(QVector2D _position);
    void vSetLookPoint(QVector2D _lookPoint);
    void vSetVelocity(float _velocity);
    void vSetAngularVelocity(float _velocity);
    void vIgnoreBall();
    void vConsiderBall();

};

#endif // SKILLMOVETOPOSE_H
