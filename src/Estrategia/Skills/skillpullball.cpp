#include "skillpullball.h"

SkillPullBall::SkillPullBall(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_PULLBALL;
    limitPullingDistance = 700;
    // A regra define a distância máxima de drible de 1m.
    // https://robocup-ssl.github.io/ssl-rules/sslrules.html#_excessive_dribbling
    // Adotamos 30% de margem para evitar maiores problemas.
}

SkillPullBall::~SkillPullBall()
{

}

void SkillPullBall::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D allyGoal = env.geoCampo->vt2dGoalCenter(env.allies->iGetSide());

    const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();

    const QVector2D goToPos   = ballPos + 300*(target - ballPos).normalized();
    const QVector2D lookToPos = ballPos - 300*(target - ballPos).normalized();

    const qint8 closestOppId = env.iAchaInimigoProximoBola();
    const QVector2D closestOpp = (closestOppId == -1) ? QVector2D(9e5, 0) :
                                     env.opponents->getCPlayer(closestOppId)->vt2dPosicaoAtual();

    robot->vSetaIgnorarBola(true);
    robot->vLigaRoller(100);

    robot->vSetaDestino(goToPos);
    robot->vSetaPontoAnguloDestino(lookToPos);
    robot->vSetMaxLinearVelocity(velocity);
    robot->vSetMaxAngularVelocity(0.3 * globalConfig.robotVelocities.angular.limit);

    // Se o robô perdeu a bola, não tem sentido continuar utilizando essa skill
    // Se a bola estiver muito próxima ao gol ou se o destino do robô for fora do campo,
    //  a skill é finalizada.
    // Se o robô puxou a bola o suficiente, a skill é finalizada.
    if(!fieldEnvironment->bRoboEstaComABola(robot->iID()) ||
        ballPos.distanceToPoint(allyGoal) < minimumDistToAllyGoal ||
        !env.geoCampo->bIsInsideField(goToPos) ||
        ballPos.distanceToPoint(closestOpp) > pullingDist ||
        ballBeginningPos.distanceToPoint(ballPos) > limitPullingDistance)
    {
        bFinished = true;
    }
}

void SkillPullBall::setMinimumDistToAllyGoal(float newMinimumDistToAllyGoal)
{
    minimumDistToAllyGoal = newMinimumDistToAllyGoal;
}

void SkillPullBall::setPullingDist(float newPullingDist)
{
    pullingDist = newPullingDist;
}

void SkillPullBall::setVelocity(float newVelocity)
{
    velocity = newVelocity;
}

void SkillPullBall::setTarget(const QVector2D& newTarget)
{
    target = newTarget;
}

QString SkillPullBall::strName() const
{
    return QString("PullBall");
}

void SkillPullBall::vInitialize()
{
    const AmbienteCampo& env = *fieldEnvironment;

    ballBeginningPos = env.vt2dPosicaoBola();

    bInitialized = true;
    bFinished = false;
}
