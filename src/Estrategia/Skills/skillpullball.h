#ifndef SKILLPULLBALL_H
#define SKILLPULLBALL_H

#include "skill.h"

/**
 * @brief Define o ID inicial das instâncias dessa skill.
 *
 * @note Esse enum é de implementação obrigatória!
 */
enum
{
    SKILL_PULLBALL = 140
};

class SkillPullBall : public Skill
{
private:
    /**
     * @brief Roda a skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vRun() override;

    // Variáveis
    QVector2D ballBeginningPos;
    QVector2D target;
    float velocity;
    float pullingDist;
    float minimumDistToAllyGoal;

    float limitPullingDistance;

public:
    SkillPullBall(AmbienteCampo *_fieldEnv);
    ~SkillPullBall();

    /**
     * @brief Retorna o nome da skill.
     *
     * @note Esse método é de implementação obrigatória!
     *
     * @return
     */
    QString strName() const override;

    /**
     * @brief Inicializa parâmetros internos da skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vInitialize() override;
    void setTarget(const QVector2D& newTarget);
    void setVelocity(float newVelocity);
    void setPullingDist(float newPullingDist);
    void setMinimumDistToAllyGoal(float newMinimumDistToAllyGoal);
};

#endif // SKILLPULLBALL_H
