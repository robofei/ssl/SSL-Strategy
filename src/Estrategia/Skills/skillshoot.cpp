#include "skillshoot.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

SkillShoot::SkillShoot(AmbienteCampo* _fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_SHOOT;

    bTouched = false;

    kickType = KICK_CUSTOM;
    kickStrength = globalConfig.calibratedKickForce;
}

SkillShoot::~SkillShoot()
{
}

void SkillShoot::vRun()
{
    if (!bFinished)
        robot->vChutar(kickType, kickStrength, 0);
    else
        robot->vDesligaChute();

    robot->vSetaPontoAnguloDestino(vt2dAim);
    robot->vSetMaxAngularVelocity(globalConfig.robotVelocities.angular.limit);
    robot->vSetaDestino(
        robot->vt2dPlaceRollerAt(fieldEnvironment->vt2dPosicaoBola()));
    // robot->vSetaDestino(fieldEnvironment->vt2dPosicaoBola());
    robot->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
    // if(robot->bKickSensor())
    // {
    //     robot->vSetMaxLinearVelocity(0);
    // }

    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
    const float botDist = robot->vt2dPosicaoAtual().distanceToPoint(ballPos);

    if (!bAboutToShoot())
    {
        vt2dStartingBall = fieldEnvironment->vt2dPosicaoBola();
    }
    else
    {
        bTouched = true;
        //         qInfo() << QString("Touched the ball %1").arg(bFinished);
    }

    if (fieldEnvironment->blPegaBola().dVelocidade > 0.2)
    {
        QVector2D ballDirection = ballPos - vt2dStartingBall;
        QVector2D desiredDirection =
            robot->vt2dPontoAnguloDestino() - vt2dStartingBall;
        double error =
            Auxiliar::dAnguloVetor1Vetor2(ballDirection, desiredDirection);

        bFinished = true;
        qInfo() << QString("[Skill %1] Robô [%2] chutou! Erro = %3 graus")
                       .arg(strName())
                       .arg(robot->iID())
                       .arg(error);
    }

    // if(vt2dStartingBall.distanceToPoint(ballPos) > globalConfig.robotDiameter
    // && bTouched &&
    //         bFinished == false)
    // {
    //     QVector2D ballDirection = ballPos - vt2dStartingBall;
    //     QVector2D desiredDirection = robot->vt2dPontoAnguloDestino() -
    //     vt2dStartingBall; double error =
    //     Auxiliar::dAnguloVetor1Vetor2(ballDirection, desiredDirection);
    //
    //     bFinished = true;
    //     qInfo() << QString("[Skill %1] Robô [%2] chutou! Erro = %3
    //     graus").arg(strName())
    //                                                                       .arg(robot->iID())
    //                                                                       .arg(error);
    // }
    // else if(botDist > 1e3 && /*!bTouched &&*/ !bFinished)
    // {
    //     bFinished = true;
    //     qInfo() << QString("[Skill %1] Acho que o robô chutou muito rápido,
    //     não foi possível calcular o erro. [%2 -> %3]")
    //         .arg(strName())
    //         .arg(QString("(%1,
    //         %2)").arg(vt2dStartingBall.x()).arg(vt2dStartingBall.y()))
    //         .arg(QString("(%1, %2)").arg(ballPos.x()).arg(ballPos.y()));
    // }

    if (bFinished)
    {
        robot->vDesligaChute();
        robot->vStay();
    }
}

QString SkillShoot::strName() const
{
    return QString("Shoot");
}

void SkillShoot::vInitialize()
{
    bInitialized = true;
    bFinished = false;
    bTouched = false;
    vt2dStartingBall = fieldEnvironment->vt2dPosicaoBola();
}

void SkillShoot::vResetSkill()
{
    bFinished = false;
    bInitialized = false;
    bTouched = false;
    if (robot != nullptr)
    {
        robot->vDesligaChute();
    }
}

void SkillShoot::vSetKick(KICKTYPE _type, float _strength)
{
    kickType = _type;
    kickStrength = _strength;
}

void SkillShoot::vSetAim(const QVector2D& _aim)
{
    vt2dAim = _aim;
}

void SkillShoot::vSetVelocity(float _velocity)
{
    fVelocity = _velocity;
}

bool SkillShoot::bAboutToShoot() const
{
    if (fieldEnvironment->vt2dPosicaoBola().distanceToPoint(
            robot->vt2dRollerCenter()) < 100)
    {
        return true;
    }
    return false;
}
