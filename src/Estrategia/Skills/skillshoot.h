#ifndef SKILLSHOOT_H
#define SKILLSHOOT_H

#include "skill.h"

enum
{
    SKILL_SHOOT = 40
};

class SkillShoot : public Skill
{
private:
    void vRun() override;
    QVector2D vt2dStartingBall;
    QVector2D vt2dAim;
    KICKTYPE kickType;
    float kickStrength;
    float fVelocity;
    bool bTouched;

public:
    SkillShoot(AmbienteCampo *_fieldEnv);
    ~SkillShoot();

    QString strName() const override;
    void vInitialize() override;
    void vResetSkill() override;

    void vSetKick(KICKTYPE _type, float _strength);
    void vSetAim(const QVector2D &_aim);
    void vSetVelocity(float _velocity);

    bool bAboutToShoot() const;
};

#endif // SKILLSHOOT_H
