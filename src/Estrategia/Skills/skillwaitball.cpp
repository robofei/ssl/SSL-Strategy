#include "skillwaitball.h"

SkillWaitBall::SkillWaitBall(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = SKILL_WAITBALL;
}

SkillWaitBall::~SkillWaitBall()
{

}

void SkillWaitBall::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;

    if(env.bReceptorRecebendoPasse(player()->iID()))
    {
        const double distAprox = 150;
        const QVector2D ballPos = env.vt2dPosicaoBola();

        const QVector2D receiverPos = player()->vt2dPosicaoAtual();

        QVector2D destiny = ballPos + Auxiliar::vt2dProjecao_de_A_em_B(receiverPos - ballPos,
                                                                       env.vt2dVelocidadeBola());

        // Aproxima o robô da bola se seu destino estiver fora do campo com offset de um raio do robô
        if(Auxiliar::bPontoForaCampo(env.geoCampo->szField(), destiny, -globalConfig.robotDiameter/2))
        {
            destiny += (ballPos - destiny).normalized()*distAprox;
        }
        // Aproxima o robô da bola se ele já estiver posicionado para recebê-la
        else if(destiny.distanceToPoint(receiverPos) < 100 &&
                 ballPos.distanceToPoint(destiny) > 500 &&
                 env.dVelocidadeBola() < 2)
        {
            destiny += (ballPos - destiny).normalized()*distAprox;
        }

        player()->vSetaDestino(destiny);

    }
    else
    {
        player()->vSetaDestino(player()->vt2dPosicaoAtual());
    }

    player()->vSetaPontoAnguloDestino(env.vt2dPosicaoBola());
    player()->vSetMaxAngularVelocity(globalConfig.robotVelocities.angular.limit);
    player()->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
    player()->vLigaRoller(100);
    player()->vSetaIgnorarBola(true);

    if(env.bRoboEstaComABola(player()->iID()))
    {
        bFinished = true;
    }
}

QString SkillWaitBall::strName() const
{
    return QString("WaitBall");
}

void SkillWaitBall::vInitialize()
{
    bInitialized = true;
    bFinished = false;
}
