#ifndef AVAILABLETACTICS_H
#define AVAILABLETACTICS_H

#include "Estrategia/Tactics/tacticgotolookto.h"           // ID = 0
#include "Estrategia/Tactics/tacticshoottotarget.h"        // ID = 20
#include "Estrategia/Tactics/tacticmovetopose.h"           // ID = 40
#include "Estrategia/Tactics/tacticreceivepass.h"          // ID = 60
#include "Estrategia/Tactics/tacticstealball.h"            // ID = 80
#include "Estrategia/Tactics/tacticreceivepassalt.h"       // ID = 100
#include "Estrategia/Tactics/tacticnormalshoottotarget.h"  // ID = 120
#include "Estrategia/Tactics/tacticdeflectkick.h"          // ID = 140
#include "Estrategia/Tactics/tacticgetball.h"              // ID = 160

#endif // AVAILABLETACTICS_H
