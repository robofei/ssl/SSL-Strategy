#include "tactic.h"

Tactic::Tactic(AmbienteCampo* _fieldEnv, QObject* parent)
    : QObject(parent)
{
    iID = 24000;
    bInitialized = false;
    bFinished = false;

    fieldEnvironment = _fieldEnv;
    robot = nullptr;

    assignedSkills.clear();
    currentSkill = nullptr;
}

void Tactic::vCheckTacticFinished()
{
    if (bFinished == false && bSkillsFinished())
    {
        //         qInfo() << QString("[Tactic %1] Finished!").arg(strName());
        bFinished = true;
    }
}

int Tactic::id() const
{
    return iID;
}

Robo* Tactic::player() const
{
    return robot;
}

bool Tactic::bIsInitialized() const
{
    return bInitialized;
}

bool Tactic::bIsFinished() const
{
    return bFinished;
}

bool Tactic::bSkillsFinished() const
{
    bool finished = true;
    QHashIterator<int, Skill*> skillIterator(
        assignedSkills); // Cuidado!!! Iterator java style

    while (skillIterator.hasNext())
    {
        skillIterator.next();
        finished &= skillIterator.value()->bIsFinished();
    }
    return finished;
}

bool Tactic::bAddSkill(Skill* _skill)
{
    bool ok = false, contains;
    if (_skill != nullptr)
    {
        contains = assignedSkills.contains(_skill->id());
        if (!contains && _skill->id() != -1)
        {
            assignedSkills.insert(_skill->id(), _skill);
            ok = true;
        }
        else
        {
            qWarning() << QString("[Tactic %1] trying to add invalid Skill(%2 "
                                  "- %3) | Reason: Contains(%4)")
                              .arg(strName())
                              .arg(_skill->strName())
                              .arg(_skill->id())
                              .arg(contains);
        }
    }
    else
    {
        qWarning()
            << QString("[Tactic %1] trying to add null skill").arg(strName());
    }
    return ok;
}

bool Tactic::bSetSkill(int _skillID)
{
    bool ok = false;
    if (assignedSkills.contains(_skillID))
    {
        if (currentSkill == nullptr || currentSkill->id() != _skillID)
        {
            currentSkill = assignedSkills.value(_skillID);
        }

        if (currentSkill->player() == nullptr ||
            currentSkill->player()->iID() != robot->iID())
        {
            currentSkill->vSetRobot(robot);
        }

        if (!currentSkill->bIsInitialized() || currentSkill->bIsFinished())
        {
            currentSkill->vResetSkill();
            currentSkill->vInitialize();
        }

        ok = true;
    }
    else
    {
        qWarning() << QString("[Tactic %1] Skill ID(%1) inválido!")
                          .arg(strName())
                          .arg(_skillID);
    }
    return ok;
}

bool Tactic::bSetSkill(Skill* _skill)
{
    if (currentSkill == nullptr || currentSkill->id() != _skill->id())
    {
        return bSetSkill(_skill->id());
    }
    return false;
}

void Tactic::vForceFinished()
{
    bFinished = true;
}

bool Tactic::bCheckTacticOK()
{
    if (!bInitialized)
    {
        qCritical() << QString("[Tactic %1] Trying to run without initializing")
                           .arg(strName());
        return false;
    }
    if (assignedSkills.isEmpty())
    {
        qWarning() << QString("[Tactic %1] Trying to run without skills")
                          .arg(strName());
        return false;
    }
    return true;
}

void Tactic::vRunTactic()
{
    if (!bCheckTacticOK())
    {
        return;
    }

    vRun();
    vCheckTacticFinished();

    if (robot != nullptr)
    {
        const QString color = bIsFinished() ? "green" : "red";
        robot->vSetTactic(Auxiliar::strColorText(strName(), color));
    }
}

void Tactic::vResetTactic()
{
    currentSkill = nullptr;
    bInitialized = false;
    bFinished = false;

    QHashIterator<int, Skill*> skillIterator(
        assignedSkills); // Cuidado!!! Iterator java style
    while (skillIterator.hasNext())
    {
        skillIterator.next();
        skillIterator.value()->vResetSkill();
    }

    // qInfo() << QString("[Tactic %1] Reset!").arg(strName());
}

void Tactic::vSetRobot(Robo* _robot)
{
    robot = _robot;
    if (currentSkill != nullptr)
        currentSkill->vSetRobot(_robot);
}
