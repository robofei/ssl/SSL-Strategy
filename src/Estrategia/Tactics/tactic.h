#ifndef TACTIC_H
#define TACTIC_H

#include <QString>
#include <QHash>
#include <QSharedPointer>
#include <QObject>

#include "Ambiente/futbolenvironment.h"
#include "Ambiente/fieldzone.h"

#include "Estrategia/Skills/skill.h"

/**
 * @brief Classe genérica apra a implementação de uma tática.
 *
 * A Tactic é um conjunto de Skill que o robô tem que executar, por exemplo:
 * - Ir para um ponto E chutar para o gol
 * - Ir para um ponto E esperar um passe
 * - Ir para um ponto/região E marcar um oponente
 */
class Tactic : public QObject
{
    Q_OBJECT
    QHash<int, Skill*> assignedSkills;

protected:
    int iID;
    bool bInitialized;
    bool bFinished;

    AmbienteCampo *fieldEnvironment;
    Robo *robot;

    Skill *currentSkill;

    virtual void vRun() = 0;

public:
    FieldZone tacticZone;

    explicit Tactic(AmbienteCampo *_fieldEnv, QObject *parent = nullptr);
    virtual ~Tactic() { };

    virtual QString strName() const = 0;
    virtual void vInitialize() = 0;
    virtual void vCheckTacticFinished();

    int id() const;
    Robo* player() const;
    bool bIsInitialized() const;
    bool bIsFinished() const;
    bool bSkillsFinished() const;
    bool bAddSkill(Skill *_skill);
    bool bSetSkill(int _skillID);
    bool bSetSkill(Skill *_skill);
    void vForceFinished();

    bool bCheckTacticOK();
    void vRunTactic();
    void vResetTactic();

    void vSetRobot(Robo *_robot);
};

#endif // TACTIC_H
