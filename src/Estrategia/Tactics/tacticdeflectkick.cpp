#include "tacticdeflectkick.h"
#include "Config/playdeflectconfig.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "qglobal.h"

TacticDeflectKick::TacticDeflectKick(AmbienteCampo* _fieldEnv)
    : Tactic(_fieldEnv), playConfig("PlayDeflect")
{
    iID = TACTIC_DEFLECT_KICK;

    playConfig.read("play_deflect_config.json");

    move.reset(new SkillGoTo(_fieldEnv));
    look.reset(new SkillLookTo(_fieldEnv));
    shoot.reset(new SkillShoot(_fieldEnv));

    bAddSkill(move.get());

    vt2dAim = vt2dReceiveBall = QVector2D(0, 0);
}

TacticDeflectKick::~TacticDeflectKick()
{
}

void TacticDeflectKick::vRun()
{
    robot->vChutar(KICK_CUSTOM, playConfig.kickForce);

    vUpdateDeflectPoint();

    if (currentSkill != nullptr)
    {
        currentSkill->vRunSkill();
    }
}

void TacticDeflectKick::vUpdateDeflectPoint()
{
    const int opponentSide = fieldEnvironment->opponents->iGetSide();
    const QVector2D
        rollerCenter = robot->vt2dRollerCenter(),
        //                     ball = fieldEnvironment->vt2dPosicaoBola(),
        goalCenter = fieldEnvironment->geoCampo->vt2dGoalCenter(opponentSide);

    // vt2dReceiveBall =
    // Auxiliar::vt2dBallIntersection(fieldEnvironment->blBola->vvt2dGetFilteredPositions(),
    //                                                  rollerCenter,
    //                                                  robot->vt2dAimDirectionNormalized(),
    //                                                  2e3);

    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola(),
              ballVector = fieldEnvironment->vt2dVelocidadeBola();

    vt2dReceiveBall = ballPos + Auxiliar::vt2dProjecao_de_A_em_B(
                                    (rollerCenter - ballPos), ballVector);
    if (qIsNaN(vt2dReceiveBall.length()))
    {
        vt2dReceiveBall = Auxiliar::vt2dBallIntersection(
            fieldEnvironment->blBola->vvt2dGetFilteredPositions(), rollerCenter,
            robot->vt2dAimDirectionNormalized(), 1.0e3);
    }

    // QVector2D deceleration(0.2, 0.2);
    // QVector2D ballVel = fieldEnvironment->vt2dVelocidadeBola();
    // float t = ballVel.length() / deceleration.length();
    //
    // QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola() + ballVel * t -
    // 1/2.f * deceleration * t*t;

    const QVector2D aimVec = goalCenter - rollerCenter;
    double aimAngle = Auxiliar::dAnguloVetor1Vetor2(aimVec, goalCenter);
    if (aimVec.y() < 0)
    {
        aimAngle *= -1;
    }
    if (aimVec.x() < 0)
    {
        aimAngle *= -1;
    }

    vt2dAim = Auxiliar::vt2dRotaciona(rollerCenter, goalCenter + rollerCenter,
                                      aimAngle);

    int oppSide = fieldEnvironment->opponents->iGetSide(),
        goalHeight = fieldEnvironment->geoCampo->szGoal().height() / 2.f,
        sign = Auxiliar::iSinal(ballPos.y());
    QVector2D goalAim = fieldEnvironment->geoCampo->vt2dGoalCenter(oppSide) +
                        QVector2D(0, goalHeight * sign);

    QSharedPointer<DebugShootDecision> debugShoot;
    debugShoot.reset(new DebugShootDecision);
    debugShoot->setRobotPosition(vt2dReceiveBall);
    debugShoot->setRobotTarget(goalAim);
    fieldEnvironment->debugInfo->addDebugData(debugShoot, 2);


    move->vSetGotoPosition(robot->vt2dPlaceRollerAt(vt2dReceiveBall));
    move->vSetVelocity(globalConfig.robotVelocities.linear.limit);
    robot->vSetaPontoAnguloDestino(goalAim);
    robot->vSetMaxAngularVelocity(globalConfig.robotVelocities.angular.limit);
}

QString TacticDeflectKick::strName() const
{
    return QString("Deflect Kick");
}

void TacticDeflectKick::vInitialize()
{
    iKickerID = -1;
    bInitialized = true;
}

void TacticDeflectKick::vCheckTacticFinished()
{
    // A tática só é finalizada quando o robô chutar
    if (shoot->bIsFinished() && !bFinished)
    {
        qInfo() << QString("[Tactic %1] Chutou!").arg(strName());
        bFinished = true;
    }
}

void TacticDeflectKick::vSetKickerID(int _id)
{
    if (_id >= 0 && _id < globalConfig.robotsPerTeam)
    {
        iKickerID = _id;
    }
}

void TacticDeflectKick::vSetAim(QVector2D _aim)
{
    vt2dAim = _aim;
}

void TacticDeflectKick::vProceedToDeflect()
{
    bSetSkill(move->id());
}
