#ifndef TACTICDEFLECTKICK_H
#define TACTICDEFLECTKICK_H

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"
#include "Config/playdeflectconfig.h"

enum
{
    TACTIC_DEFLECT_KICK = 140
};

class TacticDeflectKick : public Tactic
{
    Q_OBJECT

private:
    void vRun() override;

    QSharedPointer<SkillGoTo> move;
    QSharedPointer<SkillLookTo> look;
    QSharedPointer<SkillShoot> shoot;

    PlayDeflectConfig playConfig;
    QVector2D vt2dReceiveBall;
    QVector2D vt2dAim;
    int iKickerID; /**< ID do robô que vai chutar para o robô atual
                     fazer a deflexão. */
    void vUpdateDeflectPoint();

public:
    TacticDeflectKick(AmbienteCampo *_fieldEnv);
    ~TacticDeflectKick();

    QString strName() const override;
    void vInitialize() override;
    void vCheckTacticFinished() override;

    void vSetKickerID(int _id);
    void vSetAim(QVector2D _aim);

    void vProceedToDeflect();
};

#endif // TACTICDEFLECTKICK_H
