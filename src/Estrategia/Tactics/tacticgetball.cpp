#include "tacticgetball.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Constantes_e_Funcoes_Auxiliares/macros.h"
#include "qvector2d.h"

TacticGetBall::TacticGetBall(AmbienteCampo* _fieldEnv) : Tactic(_fieldEnv)
{
    iID = TACTIC_GET_BALL;

    skillGoTo.reset(new SkillGoTo(_fieldEnv));
    skillGoToZone.reset(new SkillGoToZone(_fieldEnv));
    skillGetBall.reset(new SkillGetStaticBall(_fieldEnv));

    bAddSkill(skillGoTo.get());
    bAddSkill(skillGoToZone.get());
    bAddSkill(skillGetBall.get());

    currentState = TacticGetBall::POSITION;

    targetPos = QVector2D(0, 0);
}

TacticGetBall::~TacticGetBall()
{
}

void TacticGetBall::vRun()
{
    switch (currentState)
    {
    case TacticGetBall::POSITION: {
        position();
    }
    break;

    case TacticGetBall::ADJUST_BALL: {
        position();
    }
    break;

    case TacticGetBall::GET_BALL: {
        getBall();
    }
    break;

    case TacticGetBall::PUSH_BALL: {
        pushBall();
    }
    break;

    case TacticGetBall::RETREAT: {
        retreat();
    }
    break;
    }

    if (currentSkill != nullptr)
    {
        currentSkill->vRunSkill();
    }
}

void TacticGetBall::position()
{
    robot->vSetaIgnorarBola(bConsideraBola);
    skillGoTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);

    QVector2D aux = Auxiliar::vt2dReajustaPonto(
        fieldEnvironment->vt2dPosicaoBola(), targetPos, -300);

    bSetSkill(skillGoTo.get());
    if(!skillGoTo->vSetGotoPosition(aux))
    {
        aux = Auxiliar::vt2dReajustaPonto(fieldEnvironment->vt2dPosicaoBola(), targetPos, 300);
        skillGoTo->vSetGotoPosition(aux);
    }

    if (skillGoTo->bIsFinished())
    {
        currentState = GET_BALL;
    }
}

void TacticGetBall::adjust()
{
    robot->vSetaIgnorarBola(bNaoConsideraBola);
}

void TacticGetBall::getBall()
{
    robot->vSetaIgnorarBola(bNaoConsideraBola);
    bSetSkill(skillGetBall.get());

    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();
    if (fieldEnvironment->geoCampo->bIsInsideField(ballPos) == false)
    {
        robot->vChutar(KICK_CUSTOM, 200);
    }
    else{
        robot->vChutar(KICK_NONE, 0);
    }

    robot->vDesligaRoller();
    if (skillGetBall->bIsFinished())
    {
        currentState = PUSH_BALL;
    }
}

void TacticGetBall::pushBall()
{
    robot->vSetaIgnorarBola(bNaoConsideraBola);
    robot->vDesligaRoller();
    // robot->vLigaRoller(60);
    bSetSkill(skillGoToZone.get());
    skillGoToZone->vSetVelocity(0.4);
    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola(),
              robotPos = robot->vt2dPosicaoAtual(), testPos = ballPos;
    float distance = robot->vt2dPosicaoAtual().distanceToPoint(ballPos);

    testPos = Auxiliar::vt2dReajustaPonto(testPos, targetPos, -500);

    if (ballPos.distanceToPoint(targetPos) < 120)
    {
        robot->vDesligaRoller();
        currentState = RETREAT;
    }
    else if (distance > globalConfig.robotDiameter)
    {
        bSetSkill(skillGetBall.get());
        currentState = GET_BALL;
    }
    else if (Auxiliar::bChecaInterseccaoObjetosLinha(testPos, targetPos, 100,
                                                     robotPos) == false)
    {
        currentState = POSITION;
    }

    if(distance > 1500)
    {
        robot->vChutar(KICK_CUSTOM, 200);
    }
}

void TacticGetBall::retreat()
{
    robot->vSetaIgnorarBola(bConsideraBola);

    QVector2D aux = Auxiliar::vt2dReajustaPonto(
        fieldEnvironment->vt2dPosicaoBola(), robot->vt2dPosicaoAtual(),
        500 + globalConfig.robotDiameter);

    skillGoToZone->vSetZone(
        FieldZone(Circle(aux.toPoint(), globalConfig.robotDiameter / 2.0)));
    bSetSkill(skillGoToZone.get());

    if (skillGoToZone->bIsFinished())
    {
        bFinished = true;
    }
}

QString TacticGetBall::strName() const
{
    return QString("Get Ball");
}

void TacticGetBall::vInitialize()
{
    //     const AmbienteCampo& env = *fieldEnvironment;

    skillGoToZone->vSetVelocity(0.8);
    currentState = POSITION;

    bInitialized = true;
}

void TacticGetBall::setTargetPosition(QVector2D _pos)
{
    targetPos = _pos;
    skillGoToZone->vSetZone(
        FieldZone(Circle(targetPos.toPoint(), globalConfig.robotDiameter / 2.0)));
    skillGoToZone->vSetLookToPoint(targetPos);
}
