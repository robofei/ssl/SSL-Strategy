#ifndef TACTICGETBALL_H
#define TACTICGETBALL_H

#include "Estrategia/Skills/skillgetstaticball.h"
#include "Estrategia/Skills/skillgotozone.h"
#include "qvector2d.h"
#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    TACTIC_GET_BALL = 160
};

class TacticGetBall: public Tactic
{
    Q_OBJECT
private:
    void vRun() override;

    void position();
    void adjust();
    void getBall();
    void pushBall();
    void retreat();

    QSharedPointer<SkillGoToZone> skillGoToZone;
    QSharedPointer<SkillGoTo> skillGoTo;
    QSharedPointer<SkillGetStaticBall> skillGetBall;

    QVector2D targetPos;

public:
    TacticGetBall(AmbienteCampo *_fieldEnv);
    ~TacticGetBall();

    enum  STATES
    {
        POSITION,
        ADJUST_BALL,
        GET_BALL,
        PUSH_BALL,
        RETREAT
    } currentState;
    Q_ENUM(STATES);

    QString strName() const override;
    void vInitialize() override;

    void setTargetPosition(QVector2D _pos);
};

#endif // TACTICGETBALL_H
