#include "tacticgotolookto.h"

TacticGoToLookTo::TacticGoToLookTo(AmbienteCampo *_fieldEnv) :
    Tactic(_fieldEnv),
    fAngularVelocity(3) // Velocidade máxima de rotação padrão
{
    iID = TACTIC_GOTO_LOOKTO;

    skillGoto.reset(new SkillGoTo(_fieldEnv));
    skillLookTo.reset(new SkillLookTo(_fieldEnv));

    bAddSkill(skillGoto.get());
    bAddSkill(skillLookTo.get());
}

TacticGoToLookTo::~TacticGoToLookTo()
{

}

void TacticGoToLookTo::vRun()
{
    bool skillSet = true;
    if(currentSkill != nullptr)
    {
        if(currentSkill->id() == skillGoto->id())
        {
            skillGoto->vSetGotoPosition(vt2dGotoPosition);
            skillGoto->vSetVelocity(fVelocity);

            if(skillGoto->bIsFinished())
            {
                skillSet = bSetSkill(skillLookTo->id());
            }
        }
        else if(currentSkill->id() == skillLookTo->id())
        {
            skillLookTo->vSetLookPoint(vt2dLooktoPosition);
            skillLookTo->vSetVelocity(fAngularVelocity);
        }
    }
    else
    {
        skillSet = bSetSkill(skillGoto->id());
    }

    if(skillSet)
    {
        currentSkill->vRunSkill();
    }
}

QString TacticGoToLookTo::strName() const
{
    return QString("GoTo LookTo");
}

void TacticGoToLookTo::vInitialize()
{
    skillGoto->vSetGotoPosition(vt2dGotoPosition);
    skillGoto->vSetVelocity(fVelocity);

    skillLookTo->vSetLookPoint(vt2dLooktoPosition);
    skillLookTo->vSetVelocity(fAngularVelocity);
    bSetSkill(skillGoto->id());

    bInitialized = true;
}

void TacticGoToLookTo::vSetGotoPosition(QVector2D _position)
{
    if(_position.distanceToPoint(vt2dGotoPosition) > globalConfig.robotDiameter/8.0)
    {
        bFinished = false;
        bSetSkill(skillGoto->id());
    }
    vt2dGotoPosition = _position;
}

void TacticGoToLookTo::vSetLooktoPosition(QVector2D _position)
{
    if(_position.distanceToPoint(vt2dLooktoPosition) > 2)
    {
        bFinished = false;
    }
    vt2dLooktoPosition = _position;
}

void TacticGoToLookTo::vSetVelocity(float _velocity)
{
    fVelocity = _velocity;
}

void TacticGoToLookTo::vSetAngularVelocity(float _velocity)
{
    fAngularVelocity = _velocity;
}
