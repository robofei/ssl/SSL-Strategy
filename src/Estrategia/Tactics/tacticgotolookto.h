#ifndef TACTICGOTOLOOKTO_H
#define TACTICGOTOLOOKTO_H

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    TACTIC_GOTO_LOOKTO = 0
};

class TacticGoToLookTo : public Tactic
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<SkillGoTo> skillGoto;
    QSharedPointer<SkillLookTo> skillLookTo;

    QVector2D vt2dGotoPosition;
    QVector2D vt2dLooktoPosition;
    float fVelocity;
    float fAngularVelocity;

public:
    TacticGoToLookTo(AmbienteCampo *_fieldEnv);
    ~TacticGoToLookTo();

    QString strName() const override;
    void vInitialize() override;

    void vSetGotoPosition(QVector2D _position);
    void vSetLooktoPosition(QVector2D _position);
    void vSetVelocity(float _velocity);
    void vSetAngularVelocity(float _velocity);
};

#endif // TACTICGOTOLOOKTO_H
