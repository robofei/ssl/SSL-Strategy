#include "tacticmovetopose.h"

TacticMoveToPose::TacticMoveToPose(AmbienteCampo *_fieldEnv) :
    Tactic(_fieldEnv)
{
    iID = TACTIC_MOVETOPOSE;

    skillMove.reset( new SkillMoveToPose(_fieldEnv) );

    bAddSkill(skillMove.get());

}

TacticMoveToPose::~TacticMoveToPose()
{

}

void TacticMoveToPose::vRun()
{
    bool skillSet = true;
    if(currentSkill != nullptr)
    {
        if(currentSkill->id() == skillMove->id())
        {
            skillMove->vSetGotoPosition(vt2dDestiny);
            skillMove->vSetVelocity(fVelocity);
            skillMove->vSetLookPoint(vt2dAimPoint);
            skillMove->vSetAngularVelocity(fAngularVelocity);
        }
    }
    else
    {
        skillSet = bSetSkill(skillMove->id());
    }

    if(skillSet)
    {
        currentSkill->vRunSkill();
    }
}

void TacticMoveToPose::vSetVelocity(float newFVelocity)
{
    fVelocity = newFVelocity;
}

void TacticMoveToPose::vIgnoreBall()
{
    skillMove->vIgnoreBall();
}

void TacticMoveToPose::vConsiderBall()
{
    skillMove->vConsiderBall();
}

void TacticMoveToPose::vSetAngularVelocity(float newFAngularVelocity)
{
    fAngularVelocity = newFAngularVelocity;
}

void TacticMoveToPose::vSetLooktoPosition(const QVector2D& newVt2dAimPoint)
{
    vt2dAimPoint = newVt2dAimPoint;
}

void TacticMoveToPose::vSetGotoPosition(const QVector2D& newVt2dDestiny)
{
    vt2dDestiny = newVt2dDestiny;
}

QString TacticMoveToPose::strName() const
{
    return QString("MoveToPose");
}

void TacticMoveToPose::vInitialize()
{
    skillMove->vSetGotoPosition(vt2dDestiny);
    skillMove->vSetVelocity(fVelocity);

    skillMove->vSetLookPoint(vt2dAimPoint);
    skillMove->vSetVelocity(fAngularVelocity);

    bSetSkill(skillMove->id());

    bInitialized = true;
}
