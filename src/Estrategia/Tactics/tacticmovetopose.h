#ifndef TACTICMOVETOPOSE_H
#define TACTICMOVETOPOSE_H

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    TACTIC_MOVETOPOSE = 40
};

class TacticMoveToPose : public Tactic
{
    Q_OBJECT
private:
    void vRun() override;

    QSharedPointer<SkillMoveToPose> skillMove;

    // Skills
    QVector2D vt2dDestiny;
    QVector2D vt2dAimPoint;

    // Variables
    float fVelocity;
    float fAngularVelocity;


public:
    TacticMoveToPose(AmbienteCampo *_fieldEnv);
    ~TacticMoveToPose();

    QString strName() const override;
    void vInitialize() override;
    void vSetGotoPosition(const QVector2D& newVt2dDestiny);
    void vSetLooktoPosition(const QVector2D& newVt2dAimPoint);
    void vSetAngularVelocity(float newFAngularVelocity);
    void vSetVelocity(float newFVelocity);
    void vIgnoreBall();
    void vConsiderBall();
};

#endif // TACTICMOVETOPOSE_H
