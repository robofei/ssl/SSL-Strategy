#include "tacticnormalshoottotarget.h"

#define SET_SKILL(skill)                        \
if(currentSkill == nullptr ||                   \
   currentSkill->id() != skill->id())           \
{                                               \
    bSetSkill(skill->id());                     \
}                                               \

TacticNormalShootToTarget::TacticNormalShootToTarget(AmbienteCampo *_fieldEnv) :
    Tactic(_fieldEnv)
{
    iID = TACTIC_NORMALSHOOTTOTARGET;

    skillGotoZone.reset(new SkillGoToZone(_fieldEnv));
    skillGetBall.reset(new SkillGetOppBall(_fieldEnv));
    skillAim.reset(new SkillAim(_fieldEnv));
    skillShoot.reset(new SkillShoot(_fieldEnv));

    bAddSkill(skillGotoZone.get());
    bAddSkill(skillGetBall.get());
    bAddSkill(skillAim.get());
    bAddSkill(skillShoot.get());

    currentState = APPROACH_ZONE;
}

TacticNormalShootToTarget::~TacticNormalShootToTarget()
{

}

void TacticNormalShootToTarget::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;

    if(env.vt2dPosicaoBola().distanceToPoint(player()->vt2dPosicaoAtual()) > 500)
    {
        currentState = APPROACH_ZONE;
    }

    switch(currentState)
    {
        case APPROACH_ZONE:
            approachZone();
            break;
        case GET_BALL:
            getBall();
            break;
        case AIM:
            aim();
            break;
        case SHOOT:
            shoot();
            break;
    }

    if(currentSkill != nullptr)
    {
        currentSkill->vRunSkill();
    }
}

void TacticNormalShootToTarget::approachZone()
{
    SET_SKILL(skillGotoZone);

    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();

    skillGotoZone->vSetZone(FieldZone(Circle(ballPos.toPoint(), 500)));
    skillGotoZone->vSetLookToPoint(ballPos);
    skillGotoZone->vSetVelocity(globalConfig.robotVelocities.linear.limit);


    if(skillGotoZone->bIsFinished())
    {
        currentState = GET_BALL;
    }
}

void TacticNormalShootToTarget::getBall()
{
    SET_SKILL(skillGetBall);

    if(skillGetBall->bIsFinished())
    {
        currentState = AIM;
    }
}

void TacticNormalShootToTarget::aim()
{
    SET_SKILL(skillAim)

    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();
    const QVector2D currentPos = player()->vt2dPosicaoAtual();

    if(targetId == CUSTOM)
    {
        skillAim->setAimPoint(customTargetPos);
    }
    else if(targetId == GOAL)
    {
        skillAim->setGoal();
    }
    else
    {
        skillAim->setReceiver(targetId);
    }

    if(currentPos.distanceToPoint(ballPos) > 200)
    {
        currentState = GET_BALL;
    }
    else if(skillAim->bIsFinished())
    {
        currentState = SHOOT;
    }
}

void TacticNormalShootToTarget::shoot()
{
    SET_SKILL(skillShoot)

    const AmbienteCampo& env = *fieldEnvironment;

    if(targetId == GOAL || targetId == CUSTOM)
    {
        skillShoot->vSetKick(KICK_CUSTOM, globalConfig.calibratedKickForce);
    }
    else
    {
        const float dist = env.vt2dAllyPosition(targetId).distanceToPoint(robot->vt2dPosicaoAtual());
        const float strength = player()->fForcaChuteDistanciaVelocidade(
            dist, fVelocidadeBolaChegadaPasse);

        skillShoot->vSetKick(KICK_CUSTOM, strength);
    }

    if(!env.bRoboEstaComABola(player()->iID()))
    {
        currentState = GET_BALL;
    }
}

QString TacticNormalShootToTarget::strName() const
{
    return QString("NormalShootToTarget");
}

void TacticNormalShootToTarget::vInitialize()
{
    bInitialized = true;
}

void TacticNormalShootToTarget::setCustomTarget(const QVector2D& newAimPoint)
{
    customTargetPos = newAimPoint;
    targetId = CUSTOM;
}

void TacticNormalShootToTarget::setReceiver(qint8 recId)
{
    targetId = recId;
}

void TacticNormalShootToTarget::setGoal()
{
    targetId = GOAL;
}
