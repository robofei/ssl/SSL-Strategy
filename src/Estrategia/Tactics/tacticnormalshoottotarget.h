#ifndef TACTICNORMALSHOOTTOTARGET_H
#define TACTICNORMALSHOOTTOTARGET_H

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    TACTIC_NORMALSHOOTTOTARGET = 120
};

class TacticNormalShootToTarget : public Tactic
{
    Q_OBJECT
private:
    void vRun() override;

    void approachZone();
    void getBall();
    void aim();
    void shoot();

    // Skills
    QSharedPointer<SkillGoToZone> skillGotoZone;
    QSharedPointer<SkillGetOppBall> skillGetBall;
    QSharedPointer<SkillAim> skillAim;
    QSharedPointer<SkillShoot> skillShoot;

    enum STATES
    {
        APPROACH_ZONE,
        GET_BALL,
        AIM,
        SHOOT
    } currentState;

    enum TARGET_TYPE : qint8
    {
        CUSTOM = -2,
        GOAL = -1
    };

    QVector2D customTargetPos;
    qint8 targetId;

public:
    TacticNormalShootToTarget(AmbienteCampo *_fieldEnv);
    ~TacticNormalShootToTarget();

    QString strName() const override;
    void vInitialize() override;

    void setCustomTarget(const QVector2D& newAimPoint);
    void setReceiver(qint8 recId);
    void setGoal();

};

#endif // TACTICNORMALSHOOTTOTARGET_H
