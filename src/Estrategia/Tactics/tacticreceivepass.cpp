#include "tacticreceivepass.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

TacticReceivePass::TacticReceivePass(AmbienteCampo* _fieldEnv)
    : Tactic(_fieldEnv)
{
    iID = TACTIC_RECEIVE_PASS;

    tmrPass.reset(new QTimer);
    connect(tmrPass.get(), &QTimer::timeout, this,
            &TacticReceivePass::vPassTimeout_);
    tmrPass->setInterval(2000); // 2s para receber o passe
    tmrPass->setSingleShot(true);

    skillMoveTo.reset(new SkillMoveToPose(_fieldEnv));
    skillGoToZone.reset(new SkillGoToZone(_fieldEnv));

    bAddSkill(skillMoveTo.get());
    bAddSkill(skillGoToZone.get());

    currentState = POSITION_FOR_PASS;
    completedStates.POSITION_FOR_PASS = false;
    completedStates.WAIT_FOR_BALL = false;
    completedStates.GET_BALL = false;

    vt2dBallStartingPosition = fieldEnvironment->vt2dPosicaoBola();
    bPassPointSet = false;
    mode = MANUAL;
}

TacticReceivePass::~TacticReceivePass()
{
}

void TacticReceivePass::vRun()
{
    if (bFinished)
    {
        return;
    }

    switch (currentState)
    {
    case POSITION_FOR_PASS: {
        skillMoveTo->vSetGotoPosition(vt2dStartingPosition);
        skillMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);

        skillMoveTo->vSetLookPoint(fieldEnvironment->vt2dPosicaoBola());
        skillMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);

        if (currentSkill->id() != skillMoveTo->id())
        {
            bSetSkill(skillMoveTo->id());
        }

        if (skillMoveTo->bIsFinished())
        {
            completedStates.POSITION_FOR_PASS = true;

            if (mode == AUTOMATIC)
            {
                vSetPassPoint(vt2dStartingPosition);
            }
        }
    }
    break;

    case WAIT_FOR_BALL: {
        skillMoveTo->vSetGotoPosition(vt2dCalculateInterceptPoint());
        skillMoveTo->vSetVelocity(globalConfig.robotVelocities.linear.limit);

        skillMoveTo->vSetLookPoint(fieldEnvironment->vt2dPosicaoBola());
        skillMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
        skillMoveTo->vIgnoreBall();

        robot->vLigaRoller(100);
        if (currentSkill->id() != skillMoveTo->id())
        {
            bSetSkill(skillMoveTo->id());
        }

        if (skillMoveTo->bIsFinished())
        {
            completedStates.WAIT_FOR_BALL = true;
        }
    }
    break;

    case GET_BALL: {
        FieldZone zone;
        zone.vConfigZone(fieldEnvironment->vt2dPosicaoBola().toPoint(), 150);
        skillGoToZone->vSetZone(zone);
        skillGoToZone->vSetVelocity(globalConfig.robotVelocities.linear.limit);

        if (currentSkill->id() != skillGoToZone->id())
        {
            bSetSkill(skillGoToZone->id());
        }

        if (skillGoToZone->bIsFinished())
        {
            completedStates.GET_BALL = true;
        }
    }
    break;
    }
    if (currentSkill != nullptr)
    {
        currentSkill->vRunSkill();
    }
}

QVector2D TacticReceivePass::vt2dCalculateInterceptPoint()
{
    QVector2D interceptPt;
    // QVector2D interceptPt = Auxiliar::vt2dBallIntersection(
    //     fieldEnvironment->blBola->vvt2dGetFilteredPositions(),
    //     vt2dReceivePassPoint, robot->vt2dAimDirectionNormalized(), 2.0e3);

    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola(),
              ballVector = fieldEnvironment->vt2dVelocidadeBola();

    interceptPt =
        ballPos + Auxiliar::vt2dProjecao_de_A_em_B(
                      (robot->vt2dRollerCenter() - ballPos), ballVector);
    if(qIsNaN(interceptPt.length()))
    {
     interceptPt = Auxiliar::vt2dBallIntersection(
         fieldEnvironment->blBola->vvt2dGetFilteredPositions(),
         vt2dReceivePassPoint, robot->vt2dAimDirectionNormalized(), 1.0e3);
    }

    // Ainda nao chutou
    if (vt2dBallStartingPosition.distanceToPoint(
            fieldEnvironment->vt2dPosicaoBola()) < 300)
    {
        if (tmrPass->isActive())
        {
            tmrPass->stop();
        }
        tmrPass->start();
    }

    bool pointInside = fieldEnvironment->geoCampo->bIsInsideField(interceptPt),
         ballInside = fieldEnvironment->geoCampo->bIsInsideField(
             fieldEnvironment->vt2dPosicaoBola());
    while (pointInside == false && ballInside == true)
    {
        pointInside = fieldEnvironment->geoCampo->bIsInsideField(interceptPt);
        ballInside = fieldEnvironment->geoCampo->bIsInsideField(
            fieldEnvironment->vt2dPosicaoBola());

        interceptPt = Auxiliar::vt2dReajustaPonto(
            interceptPt, fieldEnvironment->vt2dPosicaoBola(), globalConfig.robotDiameter);
    }
    //     qInfo() << QString("%1;%2;%3;%4;%5;%6;%7;%8;%9;%10").arg(P1.x())
    //                                                         .arg(P1.y())
    //                                                         .arg(P2.x())
    //                                                         .arg(P2.y())
    //                                                         .arg(ballLine.p1().x())
    //                                                         .arg(ballLine.p1().y())
    //                                                         .arg(ballLine.p2().x())
    //                                                         .arg(ballLine.p2().y())
    //                                                         .arg(interceptPt.x())
    //                                                         .arg(interceptPt.y());
    //     vt2dReceivePassPoint = interceptPt;
    return interceptPt;
}

QString TacticReceivePass::strName() const
{
    return QString("Receive Pass [%1]")
        .arg(QVariant::fromValue(currentState).toString());
}

void TacticReceivePass::vInitialize()
{
    currentState = POSITION_FOR_PASS;
    bSetSkill(skillMoveTo->id());

    completedStates.POSITION_FOR_PASS = false;
    completedStates.WAIT_FOR_BALL = false;
    completedStates.GET_BALL = false;

    if (tmrPass->isActive())
    {
        tmrPass->stop();
    }

    bInitialized = true;
    bFinished = false;
}

void TacticReceivePass::vCheckTacticFinished()
{
    // A tática só é finalizada quando a bola chega no robô
    const QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola();

    if (ballPos.distanceToPoint(robot->vt2dPosicaoAtual()) < 200 &&
        (currentState == WAIT_FOR_BALL || currentState == GET_BALL))
    {
        if (!bFinished && bPassPointSet)
        {
            tmrPass->stop();
            qInfo() << QString("[Tactic %1] Passe recebido!").arg(strName());
            bFinished = true;
            robot->vDesligaRoller();
        }
    }
}

void TacticReceivePass::vSetStartingPosition(QVector2D _pos)
{
    vt2dStartingPosition = _pos;
}

void TacticReceivePass::vSetPassPoint(const QVector2D _point)
{
    //     qInfo() << QString("[Role %1]:%2) Tenho que pegar um passe em (%3;
    //     %4)")
    //                .arg(strName())
    //                .arg(robot->iID())
    //                .arg(_point.x())
    //                .arg(_point.y());

    if (currentState != WAIT_FOR_BALL)
    {
        vt2dReceivePassPoint = _point;
        vt2dBallStartingPosition = fieldEnvironment->vt2dPosicaoBola();
        bPassPointSet = true;
        qInfo() << QString("[Tactic %1] Estado mudou para WAIT_FOR_BALL")
                       .arg(strName());
    }

    if (tmrPass->isActive())
    {
        tmrPass->stop();
    }
    tmrPass->start();

    currentState = WAIT_FOR_BALL;
}

void TacticReceivePass::vSetAutomaticMode()
{
    mode = AUTOMATIC;
}

void TacticReceivePass::vSetManualMode()
{
    mode = MANUAL;
}

void TacticReceivePass::vPassTimeout_()
{
    float distance = fieldEnvironment->vt2dPosicaoBola().distanceToPoint(
        robot->vt2dPosicaoAtual());
    if (distance > globalConfig.robotDiameter && distance < 1500)
    {
        if (currentState != GET_BALL)
        {
            qInfo() << QString(
                           "[Tactic %1] Estado mudou para GET_BALL | Timeout")
                           .arg(strName());
        }
        currentState = GET_BALL;
    }
    else
    {
        bFinished = true;
    }
}
