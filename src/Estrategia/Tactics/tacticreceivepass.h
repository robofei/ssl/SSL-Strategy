#ifndef TACTICRECEIVEPASS_H
#define TACTICRECEIVEPASS_H

#include <QTimer>

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    TACTIC_RECEIVE_PASS = 60
};

class TacticReceivePass : public Tactic
{
    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<SkillMoveToPose> skillMoveTo;
    QSharedPointer<SkillGoToZone> skillGoToZone;

    struct completedStates
    {
        bool POSITION_FOR_PASS;
        bool WAIT_FOR_BALL;
        bool GET_BALL;
    }completedStates;

    enum PASS_MODE
    {
        AUTOMATIC, // Assim que o robô se posicionar para o passe
                   // (POSITION_FOR_PASS completo) ele irá para o estado
                   // WAIT_FOR_BALL
        MANUAL  // O método vSetPassPoint deverá ser chamado para que o robô mude
                // de estado para WAIT_FOR_BALL
    }mode;

    QScopedPointer<QTimer> tmrPass;
    QVector2D vt2dStartingPosition; /**< Posição que o robô vai antes de saber
                                      onde vai ocorrer o passe. */
    QVector2D vt2dReceivePassPoint; /**< Posição em que o passe vai ser feito. */
    bool bPassPointSet;
    QVector2D vt2dBallStartingPosition;

    QVector2D vt2dCalculateInterceptPoint();

public:
    TacticReceivePass(AmbienteCampo *_fieldEnv);
    ~TacticReceivePass();

    enum STATES
    {
        POSITION_FOR_PASS,
        WAIT_FOR_BALL,
        GET_BALL
    }currentState;
    Q_ENUM(STATES);


    QString strName() const override;
    void vInitialize() override;
    void vCheckTacticFinished() override;

    void vSetStartingPosition(QVector2D _pos);
    void vSetPassPoint(const QVector2D _point);
    void vSetAutomaticMode();
    void vSetManualMode();

private slots:
    void vPassTimeout_();
};

#endif // TACTICRECEIVEPASS_H
