#include "tacticreceivepassalt.h"

#define SET_SKILL(skill)                        \
if(currentSkill == nullptr ||                   \
   currentSkill->id() != skill->id())           \
{                                               \
    bSetSkill(skill->id());                     \
}                                               \


TacticReceivePassAlt::TacticReceivePassAlt(AmbienteCampo *_fieldEnv) :
    Tactic(_fieldEnv)
{
    iID = TACTIC_RECEIVEPASSALT;

    skillMoveTo.reset(new SkillMoveToPose(_fieldEnv));
    skillWaitBall.reset(new SkillWaitBall(_fieldEnv));

    bAddSkill(skillMoveTo.get());
    bAddSkill(skillWaitBall.get());

    currentState = POSITION_FOR_PASS;
}

TacticReceivePassAlt::~TacticReceivePassAlt()
{

}

void TacticReceivePassAlt::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;

    if(env.bReceptorRecebendoPasse(player()->iID()))
    {
        currentState = WAIT_BALL;
    }
    else
    {
        currentState = POSITION_FOR_PASS;
    }

    switch(currentState)
    {
        case TacticReceivePassAlt::POSITION_FOR_PASS:
            positionForPass();
            break;
        case TacticReceivePassAlt::WAIT_BALL:
            waitBall();
            break;
    }

    if(currentSkill != nullptr)
    {
        currentSkill->vRunSkill();
    }
}

void TacticReceivePassAlt::positionForPass()
{
    SET_SKILL(skillMoveTo);

    const AmbienteCampo& env = *fieldEnvironment;

    const QVector2D ballPos = env.vt2dPosicaoBola();

    const qint8 attacker = env.iAchaRoboProximoBola({env.allies->iGetGoalieID(), player()->iID()});

    const QVector2D attackerPos = (attacker == -1) ? QVector2D(9e7, 0) :
                                      env.allies->getCPlayer(attacker)->vt2dPosicaoAtual();

    const QVector2D recPos = player()->vt2dPosicaoAtual();
    const QVector2D vt2d_Bola_Passador = ballPos - attackerPos;
    const QVector2D vt2d_Receptor_Bola= recPos - ballPos;

    QVector2D destiny = recPos;

    // Se o ponto estiver fora do campo, faz o robô se aproximar do centro
    if(Auxiliar::bPontoForaCampo(env.geoCampo->szField(), recPos))
    {
        destiny = recPos + 300*(QVector2D(0, 0) - recPos).normalized();
    }
    // Se a distância da bola ao receptor for grande o suficiente
    else if(vt2d_Receptor_Bola.length() > 2000)
    {
        const double fAngulo_graus = Auxiliar::dAnguloVetor1Vetor2(vt2d_Bola_Passador,
                                                                   vt2d_Receptor_Bola);

        if(fAngulo_graus < 15 && fAngulo_graus > fAnguloDeErroPasseNormal*2)
        {
            destiny = ballPos + Auxiliar::vt2dProjecao_de_A_em_B(vt2d_Receptor_Bola,
                                                                 vt2d_Bola_Passador);
        }
    }

    const float velocityRec = qMin(1.f, globalConfig.robotVelocities.linear.limit);

    skillMoveTo->vSetGotoPosition(destiny);
    skillMoveTo->vSetLookPoint(ballPos);
    skillMoveTo->vSetVelocity(velocityRec);
    skillMoveTo->vSetAngularVelocity(globalConfig.robotVelocities.angular.limit);
}

void TacticReceivePassAlt::waitBall()
{
    SET_SKILL(skillWaitBall);

    bFinished = skillWaitBall->bIsFinished();
}

QString TacticReceivePassAlt::strName() const
{
    return QString("ReceivePassAlt");
}

void TacticReceivePassAlt::vInitialize()
{
    bInitialized = true;
}
