#ifndef TACTICRECEIVEPASSALT_H
#define TACTICRECEIVEPASSALT_H

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    TACTIC_RECEIVEPASSALT = 100
};

class TacticReceivePassAlt : public Tactic
{
    Q_OBJECT
private:
    void vRun() override;

    void positionForPass();
    void waitBall();

    // Skills
    QSharedPointer<SkillMoveToPose> skillMoveTo;
    QSharedPointer<SkillWaitBall> skillWaitBall;

    enum STATES
    {
        POSITION_FOR_PASS,
        WAIT_BALL,
    } currentState;


public:
    TacticReceivePassAlt(AmbienteCampo *_fieldEnv);
    ~TacticReceivePassAlt();

    QString strName() const override;
    void vInitialize() override;
};

#endif // TACTICRECEIVEPASSALT_H
