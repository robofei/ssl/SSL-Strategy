#include "tacticshoottotarget.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

TacticShootToTarget::TacticShootToTarget(AmbienteCampo* _fieldEnv)
    : Tactic(_fieldEnv)
{
    iID = TACTIC_SHOOT_TO_TARGET;

    skillGotoZone.reset(new SkillGoTo(_fieldEnv));
    skillLookTo.reset(new SkillLookTo(_fieldEnv));
    skillGetBall.reset(new SkillGetStaticBall(_fieldEnv));
    skillShoot.reset(new SkillShoot(_fieldEnv));

    bAddSkill(skillGotoZone.get());
    bAddSkill(skillLookTo.get());
    bAddSkill(skillGetBall.get());
    bAddSkill(skillShoot.get());

    currentState = APPROACH;
    targetType = UNDEFINED;

    tmrKickTimeout.reset(new QTimer);
    connect(tmrKickTimeout.get(), &QTimer::timeout, this,
            &TacticShootToTarget::vKickTimeout_);
    tmrKickTimeout->setInterval(1000); // 1s para chutar após o AIM
    tmrKickTimeout->setSingleShot(true);

    kickType = KICK_CUSTOM;
    kickStrength = globalConfig.calibratedKickForce;
    fApproachVelocity = globalConfig.robotVelocities.linear.limit * 0.7;
    fEngageVelocity = 1;

    vt2dStartingBall = fieldEnvironment->vt2dPosicaoBola();
}

TacticShootToTarget::~TacticShootToTarget()
{
}

void TacticShootToTarget::vRun()
{
    if (targetType == TARGET_TYPE::UNDEFINED)
    {
        qCritical()
            << QString("[Tactic %1] Tipo de alvo não definido!").arg(strName());
        return;
    }

    vUpdateTarget();

    switch (currentState)
    {
    case APPROACH: {
        vApproachBall();
    }
    break;

    case ENGAGE: {
        vEngageBall();
    }
    break;

    case AIM: {
        vAim();
    }
    break;

    case SHOOT: {
        vShoot();
    }
    break;
    }

    if (currentSkill != nullptr)
    {
        currentSkill->vRunSkill();
    }
}

void TacticShootToTarget::vApproachBall()
{
    if (currentSkill == nullptr)
    {
        bSetSkill(skillGotoZone->id());
        vt2dStartingBall = fieldEnvironment->vt2dPosicaoBola();
    }

    // if
    // (robot->vt2dPosicaoAtual().distanceToPoint(fieldEnvironment->vt2dPosicaoBola())
    // > 300)
    robot->vSetaPontoAnguloDestino(vt2dTarget);
    robot->vSetMaxAngularVelocity(globalConfig.robotVelocities.angular.limit);
    robot->vDesligaRoller();

    int approachDist = 200;
    QVector2D approachPoint = fieldEnvironment->vt2dPosicaoBola();

    approachPoint = Auxiliar::vt2dCalculaPontoCobranca(
        vt2dTarget, approachPoint, approachDist);

    // Folga de 20% para possíveis erros de movimentação
    const float radiusRobotBall =
        2.0 * (globalConfig.ballDiameter / 2.0 + globalConfig.robotDiameter / 2.0);
    const bool ballBetweenRobot = Auxiliar::bChecaInterseccaoObjetosLinha(
        approachPoint, robot->vt2dPosicaoAtual(), radiusRobotBall,
        fieldEnvironment->vt2dPosicaoBola());

    if (ballBetweenRobot)
    {
        approachPoint = Auxiliar::vt2dCalculaPontoCobranca(
            vt2dTarget, approachPoint, approachDist);
        robot->vSetaIgnorarBola(bConsideraBola);
    }
    else
    {
        robot->vSetaIgnorarBola(bNaoConsideraBola);
    }


    skillGotoZone->vSetGotoPosition(approachPoint);
    skillGotoZone->vSetVelocity(fApproachVelocity);
    if (fieldEnvironment->vt2dPosicaoBola().distanceToPoint(
            robot->vt2dPosicaoAtual()) <= approachDist)
    {
        skillGotoZone->vSetVelocity(0.8);
    }

    bool robotClose = false;
    if (targetType == ROBOT)
    {
        const Robo* receiver = fieldEnvironment->allies->getCPlayer(iTargetID);
        robotClose = receiver->bArrived();
    }
    else
    {
        robotClose = true;
    }

    if (skillGotoZone->bIsFinished() && robotClose)
    {
        currentState = ENGAGE;
        // qInfo() << "Engage!";
        bSetSkill(skillGetBall->id());
        robot->vLigaRoller(100);

        // currentState = SHOOT;
        // bSetSkill(skillShoot->id());
        // skillShoot->vSetAim(vt2dTarget);
    }
}

void TacticShootToTarget::vEngageBall()
{
    skillGetBall->vSetTarget(vt2dTarget);
    if (vt2dStartingBall.distanceToPoint(fieldEnvironment->vt2dPosicaoBola()) >
        globalConfig.robotDiameter)
    {
        currentState = APPROACH;
        currentSkill = nullptr;
    }

    if (skillGetBall->bIsFinished())
    {
        currentState = AIM;
        bSetSkill(skillLookTo->id());

        // currentState = SHOOT;
        // bSetSkill(skillShoot->id());
        // skillShoot->vSetAim(vt2dTarget);
    }
}

void TacticShootToTarget::vAim()
{
    skillLookTo->vSetLookPoint(vt2dTarget);
    skillLookTo->vSetVelocity(0.3 * globalConfig.robotVelocities.angular.limit);

    bool robotClose = false;
    if (targetType == ROBOT)
    {
        const Robo* receiver = fieldEnvironment->allies->getCPlayer(iTargetID);
        robotClose = receiver->bArrived();
    }
    else
    {
        robotClose = true;
    }

    if (skillLookTo->bIsFinished() && robotClose)
    {
        currentState = SHOOT;
        //         qInfo() << "Shoot!";
        robot->vDesligaRoller();
        bSetSkill(skillShoot->id());

        tmrKickTimeout->start();
    }
}

void TacticShootToTarget::vShoot()
{
    robot->vSetaIgnorarBola(bNaoConsideraBola);
    if (skillShoot->bAboutToShoot())
    {
        tmrKickTimeout->start();
    }

    QVector2D ballPos = fieldEnvironment->vt2dPosicaoBola(),
              robotPos = robot->vt2dPosicaoAtual(), testPos = ballPos;
    testPos = Auxiliar::vt2dReajustaPonto(testPos, vt2dTarget, -500);

    if (robot->vt2dPosicaoAtual().distanceToPoint(
            fieldEnvironment->vt2dPosicaoBola()) < 300)
    {
        // if (Auxiliar::bChecaInterseccaoObjetosLinha(testPos, vt2dTarget, 60,
        //                                             robotPos) == false) // ||
        // // robot->bPodeChutar(vt2dTarget, 5) == false)
        // {
        //     currentState = APPROACH;
        //     currentSkill = nullptr;
        // }
    }

    if (skillShoot->bIsFinished() && tmrKickTimeout->isActive())
    {
        tmrKickTimeout->stop();
        qInfo() << QString("[Tactic %1] Parando o timer (%2)")
                       .arg(strName())
                       .arg(robot->iID());
        bFinished = true;
    }
}

QVector2D TacticShootToTarget::vt2dGetTarget() const
{
    return vt2dTarget;
}

TacticShootToTarget::TARGET_TYPE TacticShootToTarget::getTargetType() const
{
    return targetType;
}

void TacticShootToTarget::vUpdateTarget()
{
    switch (targetType)
    {
    case TARGET_TYPE::UNDEFINED:
        break;
    case TARGET_TYPE::ROBOT: {
        vt2dTarget =
            fieldEnvironment->allies->getCPlayer(iTargetID)->vt2dPosicaoAtual();
        // const float targetDistance =
        //     vt2dTarget.distanceToPoint(fieldEnvironment->vt2dPosicaoBola());
        // const float strength =
        // robot->fForcaChuteDistanciaVelocidade(targetDistance,
        //                                                              fVelocidadeBolaChegadaPasse);
        vSetKickType(KICK_CUSTOM, kickStrength);
    }
    break;

    case TARGET_TYPE::POINT: {
        vt2dTarget = vt2dPoint;
        const float targetDistance =
            vt2dTarget.distanceToPoint(fieldEnvironment->vt2dPosicaoBola());
        vSetKickType(KICK_CUSTOM, kickStrength);
    }
    break;

    case TARGET_TYPE::GOAL: {
        vSetKickType(KICK_CUSTOM, kickStrength);
        vt2dTarget = Auxiliar::vt2dCentroLivreGolAdversario(
            fieldEnvironment->vt2dPosicaoBola(),
            fieldEnvironment->allies->iGetSide(),
            fieldEnvironment->geoCampo->szGoal().height(),
            fieldEnvironment->geoCampo->szField(),
            fieldEnvironment->vt3dPegaPosicaoTodosObjetos(otOponente, true));
    }
    break;
    }

    QVector2D robotPos = robot->vt2dPosicaoAtual();
    QSharedPointer<DebugShootDecision> debugShoot;
    debugShoot.reset(new DebugShootDecision);
    debugShoot->setRobotPosition(robotPos);
    debugShoot->setRobotTarget(vt2dTarget);
    fieldEnvironment->debugInfo->addDebugData(debugShoot, 2);
}

QString TacticShootToTarget::strName() const
{
    QString argVal = "";
    if (targetType == ROBOT)
    {
        argVal = QString::number(iTargetID);
    }
    else if (targetType == POINT)
    {
        argVal = QString("(%1, %2)").arg(vt2dPoint.x()).arg(vt2dPoint.y());
    }
    return QString("Shoot to [%1] %2")
        .arg(QVariant::fromValue(targetType).toString())
        .arg(argVal);
}

void TacticShootToTarget::vInitialize()
{
    bInitialized = true;
    currentState = APPROACH;
    currentSkill = nullptr;
    skillGotoZone->vResetSkill();
    skillLookTo->vResetSkill();
    skillShoot->vResetSkill();
    skillGetBall->vResetSkill();
}

void TacticShootToTarget::vShootToGoal()
{
    targetType = TARGET_TYPE::GOAL;
}

void TacticShootToTarget::vShootToRobot(qint8 _robotID)
{
    if ((iTargetID != -1 && iTargetID != _robotID) ||
        targetType != TARGET_TYPE::ROBOT)
    {
        vInitialize(); // Reseta
    }
    targetType = TARGET_TYPE::ROBOT;
    iTargetID = _robotID;
}

void TacticShootToTarget::vShootToPoint(QVector2D _position)
{
    targetType = TARGET_TYPE::POINT;
    vt2dPoint = _position;
}

void TacticShootToTarget::vSetKickType(KICKTYPE _kick, float _strength)
{
    kickType = _kick;
    kickStrength = _strength;
    
    if (kickType != KICK_NONE && kickStrength <= 0)
    {
        kickStrength = globalConfig.calibratedKickForce;
    }
    skillShoot->vSetKick(kickType, kickStrength);
}

void TacticShootToTarget::vSetApproachVelocity(float _vel)
{
    fApproachVelocity = _vel;
}

void TacticShootToTarget::vSetEngageVelocity(float _vel)
{
    fEngageVelocity = _vel;
    skillShoot->vSetVelocity(fEngageVelocity);
}

bool TacticShootToTarget::bAboutToShoot() const
{
    if (skillShoot->bIsInitialized())
    {
        return skillShoot->bAboutToShoot();
    }
    return false;
}

void TacticShootToTarget::vKickTimeout_()
{
    if (!skillShoot->bIsFinished())
    {
        const float ballDist = vt2dStartingBall.distanceToPoint(
            fieldEnvironment->vt2dPosicaoBola());
        const float robotBallDist = robot->vt2dPosicaoAtual().distanceToPoint(
            fieldEnvironment->vt2dPosicaoBola());
        const float ballVel = fieldEnvironment->blBola->dVelocidade;
        qInfo() << "Approach denovo! BallDist | RobotBallDist | BallVel"
                << ballDist << robotBallDist << ballVel
                << skillShoot->bIsFinished();

        currentState = APPROACH;
        skillGotoZone->vResetSkill();
        skillLookTo->vResetSkill();
        skillShoot->vResetSkill();
        skillGetBall->vResetSkill();
        currentSkill = nullptr;
        bFinished = false;
    }
}
