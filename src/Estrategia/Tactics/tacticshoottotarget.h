#ifndef TACTICSHOOTTOTARGET_H
#define TACTICSHOOTTOTARGET_H

#include <QTimer>

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    TACTIC_SHOOT_TO_TARGET = 20
};

class TacticShootToTarget : public Tactic
{
public:
    /**
    * @brief Tipo de alvo da tática
    */
    enum TARGET_TYPE
    {
        UNDEFINED = 0,
        ROBOT, // Somente aliados
        POINT,
        GOAL
    };
    Q_ENUM(TARGET_TYPE);

    Q_OBJECT
private:
    void vRun() override;
    QSharedPointer<SkillGoTo> skillGotoZone;
    QSharedPointer<SkillLookTo> skillLookTo;
    QSharedPointer<SkillGetStaticBall> skillGetBall;
    QSharedPointer<SkillShoot> skillShoot;

    enum STATES
    {
        APPROACH,
        ENGAGE,
        AIM,
        SHOOT
    };

    void vApproachBall();
    void vEngageBall();
    void vAim();
    void vShoot();

    STATES currentState;
    QScopedPointer<QTimer> tmrKickTimeout;

    TARGET_TYPE targetType;
    qint8 iTargetID;
    QVector2D vt2dPoint;
    KICKTYPE kickType;
    float kickStrength;
    float fApproachVelocity;
    float fEngageVelocity;

    QVector2D vt2dTarget;
    QVector2D vt2dStartingBall;

    void vUpdateTarget();

public:
    TacticShootToTarget(AmbienteCampo *_fieldEnv);
    ~TacticShootToTarget();

    QString strName() const override;
    void vInitialize() override;

    void vShootToGoal();
    void vShootToRobot(qint8 _robotID);
    void vShootToPoint(QVector2D _position);

    void vSetKickType(KICKTYPE _kick, float _strength = 0);
    void vSetApproachVelocity(float _vel);
    void vSetEngageVelocity(float _vel);

    bool bAboutToShoot() const;

    TARGET_TYPE getTargetType() const;
    QVector2D vt2dGetTarget() const;

private slots:
    void vKickTimeout_();
};

#endif // TACTICSHOOTTOTARGET_H
