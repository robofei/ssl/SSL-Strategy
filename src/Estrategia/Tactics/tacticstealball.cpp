#include "tacticstealball.h"

TacticStealBall::TacticStealBall(AmbienteCampo *_fieldEnv) : Tactic(_fieldEnv)
{
    iID = TACTIC_STEALBALL;

    skillGoToZone.reset(new SkillGoToZone(_fieldEnv));
    skillGetBall.reset(new SkillGetOppBall(_fieldEnv));
    skillPullBall.reset(new SkillPullBall(_fieldEnv));

    bAddSkill(skillGoToZone.get());
    bAddSkill(skillGetBall.get());
    bAddSkill(skillPullBall.get());

    currentState = TacticStealBall::APPROACH_ZONE;
}

TacticStealBall::~TacticStealBall()
{

}

void TacticStealBall::vRun()
{
    const AmbienteCampo& env = *fieldEnvironment;

    if(env.vt2dPosicaoBola().distanceToPoint(player()->vt2dPosicaoAtual()) > 500)
    {
        currentState = APPROACH_ZONE;
    }

    switch(currentState)
    {
        case TacticStealBall::APPROACH_ZONE:
            approachZone();
            break;
        case TacticStealBall::GET_BALL:
            getBall();
            break;
        case TacticStealBall::PULL_BALL:
            pullBall();
            break;
    }

    if(currentSkill != nullptr)
    {
        currentSkill->vRunSkill();
    }
}

void TacticStealBall::approachZone()
{
    bSetSkill(skillGoToZone.get());

    const AmbienteCampo& env = *fieldEnvironment;

//    const QVector2D allyGoal = env.geoCampo->vt2dGoalCenter(env.allies->iGetSide());
    const QVector2D ballPos = env.vt2dPosicaoBola();

//    const qint8 closestOppId = env.iAchaInimigoProximoBola();
//    const QVector2D closestOpp = (closestOppId == -1) ? QVector2D(9e5, 0) :
//                                     env.opponents->getCPlayer(closestOppId)->vt2dPosicaoAtual();

//    const QVector4D vt4dDestino = Auxiliar::vt4dPosicaoLadraoDeBola(
//        player()->vt2dPosicaoAtual(), closestOpp, ballPos, allyGoal
//        );

//    skillGoToZone->vSetZone(FieldZone(Circle(QPoint(vt4dDestino.x(), vt4dDestino.y()), 500)));
    skillGoToZone->vSetZone(FieldZone(Circle(ballPos.toPoint(), 500)));
    skillGoToZone->vSetLookToPoint(ballPos);
    skillGoToZone->vSetVelocity(globalConfig.robotVelocities.linear.limit);


    if(skillGoToZone->bIsFinished())
    {
        currentState = GET_BALL;
    }
}

void TacticStealBall::getBall()
{
    bSetSkill(skillGetBall.get());

    if(skillGoToZone->bIsFinished())
    {
        skillGetBall->vInitialize();
        currentState = PULL_BALL;
    }
}

void TacticStealBall::pullBall()
{
    bSetSkill(skillPullBall.get());

    bFinished = skillPullBall->bIsFinished();
}

QString TacticStealBall::strName() const
{
    return QString("StealBall");
}

void TacticStealBall::vInitialize()
{
    const AmbienteCampo& env = *fieldEnvironment;

    skillPullBall->setVelocity(globalConfig.robotVelocities.linear.limit*.4);
    skillPullBall->setPullingDist(fDistanciaParaChutePuxadinha);
    skillPullBall->setMinimumDistToAllyGoal(4'000);
    skillPullBall->setTarget(env.geoCampo->vt2dGoalCenter(env.allies->iGetSide()));

    bInitialized = true;
}

SkillPullBall& TacticStealBall::getSkillPullBall()
{
    return *skillPullBall;
}
