#ifndef TACTICSTEALBALL_H
#define TACTICSTEALBALL_H

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    TACTIC_STEALBALL = 80
};

class TacticStealBall : public Tactic
{
    Q_OBJECT
private:
    void vRun() override;
    
    void approachZone();
    void getBall();
    void pullBall();

    // Skills
    QSharedPointer<SkillGoToZone> skillGoToZone;
    QSharedPointer<SkillGetOppBall> skillGetBall;
    QSharedPointer<SkillPullBall> skillPullBall;


    enum STATES
    {
        APPROACH_ZONE,
        GET_BALL,
        PULL_BALL
    } currentState;
    
public:
    TacticStealBall(AmbienteCampo *_fieldEnv);
    ~TacticStealBall();

    QString strName() const override;
    void vInitialize() override;

    SkillPullBall& getSkillPullBall();
};

#endif // TACTICSTEALBALL_H
