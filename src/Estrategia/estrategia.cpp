/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "estrategia.h"

// #define USE_MRA_STAR
#define USE_DVG_A_STAR
#define RUN_COACH_TEST

Estrategia::Estrategia()
{
    acEstrategia.reset(new AmbienteCampo());
    //    mMines.reset(new Mines());

    coachTest.reset();
    coachReferee.reset();
    coachNormal.reset();
    activeCoach = nullptr;

    // rrtPathPlanner.reset();
    starvgPathPlanner.reset();
    mraPathPlanner.reset();

    tmrTimerEstrategia.reset();
    fFPSEstrategia = 0;
    iFPSCounter.clear();

    bThreadAtivada = false;
    bJogoRolando = false;
    bRecebeuPrimeiroPacote = false;

    // #ifdef ROBO_TODOS_OS_RECURSOS
    //  Inicio do arquivo de log
    fileLogEstrategia.setFileName(QString("LogEstrategia_%1_.estlog")
                                      .arg(QDateTime::currentDateTime()
                                               .toString(Qt::ISODateWithMs)
                                               .replace(':', '_')));
    fileLogEstrategia.open(QIODevice::WriteOnly);
    QTextStream stream(&fileLogEstrategia);

    stream << "Tempo;Estado Atual;Comando do Referee;(PosX@PosY)Bola"
              "(PosX@PosY@Ang@id)Oponentes;&;"
              "[PosX@PosY@Ang@Jogada@id]{DestX@DestY@AngX@AngY}Aliados;";
    fileLogEstrategia.close();
    // #endif
}

Estrategia::~Estrategia()
{
}

bool Estrategia::bAtivada()
{
    return bThreadAtivada;
}

void Estrategia::vRun_()
{
    QElapsedTimer elapsedTempoDeExecucao;
    elapsedTempoDeExecucao.start();

    fFPSEstrategia = 1.f / (etmTimerFPSEstrategia.nsecsElapsed() / 1e9);
    iFPSCounter.prepend(qRound(fFPSEstrategia));

    fFPSEstrategia = Auxiliar::iCalculaFPS(iFPSCounter);
    etmTimerFPSEstrategia.restart();
    //------------------------------------------------------------------------

    activeCoach->vRunCoach();

#ifdef USE_DVG_A_STAR
#ifdef USE_MRA_STAR
    qFatal() << QString("Utilize somente UM path planner! USE_DVG_A_STAR e "
                        "USE_MRA_STAR foram definidos ao mesmo tempo!");
#endif // USE_MRA_STAR
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        if (acEstrategia->allies->getPlayer(id)->bRoboDetectado())
        {
            starvgPathPlanner->vt2dCalculaTrajeto(acEstrategia.get(), id,
                                                  false);
        }
    }
#endif // USE_DVG_A_STAR

#ifdef USE_MRA_STAR
#ifdef USE_DVG_A_STAR
    qFatal() << QString("Utilize somente UM path planner! USE_DVG_A_STAR e "
                        "USE_MRA_STAR foram definidos ao mesmo tempo!");
#endif // USE_DVG_A_STAR
    mraPathPlanner->findCoordinatedPaths(acEstrategia.get());
#endif // USE_MRA_STAR

    //------------------------------------------------------------------------
    if (bThreadAtivada)
    {
        // QSharedPointer<DebugShootDecision> debugShoot;
        // debugShoot.reset(new DebugShootDecision);
        //
        // debugShoot->setRobotPosition({1500, 550});
        // debugShoot->setRobotTarget({0, 50});
        //
        // acEstrategia->debugInfo->addDebugData(debugShoot, 1);

        // Envia os dados para movimentar os robos
        emit vEnviaInformacoesMovimentacao(*acEstrategia.get());
        acEstrategia->debugInfo->clearAll();

        if (referee->sslrefGetCommand() != SSL_Referee_Command_HALT)
            vAdicionaLog();
    }

    double tempoTotal = elapsedTempoDeExecucao.nsecsElapsed() * 1e-6;

    if (tempoTotal < iTempoLoopEstrategia)
        return;

    if (tempoTotal < 1.2 * iTempoLoopEstrategia)
        qWarning() << "Tempo Estratégia " << QString::number(tempoTotal, 'f', 2)
                   << " ms";
    else
        qCritical() << "Tempo Estratégia "
                    << QString::number(tempoTotal, 'f', 2) << " ms";
}

void Estrategia::vCoachFinishedPlay_(const int _coachID)
{
    if (activeCoach->id() == _coachID)
    {
        if (!coachQueue.isEmpty())
        {
            vMakeCoachActive(coachQueue.takeFirst());
        }
        else
        {
            //             qWarning() << QString("[Estrategia] Não há próximo
            //             coach!");
        }
    }
    else
    {
        qWarning() << QString("[Estrategia] Um coach diferente do atual acabou "
                              "de finalizar, isso não deveria ocorrer! ID "
                              "esperado != ID recebido (%1 != %2)")
                          .arg(activeCoach->id())
                          .arg(_coachID);
    }
}

void Estrategia::vAtivaThread(const bool _bThreadAtivada)
{
    bThreadAtivada = _bThreadAtivada;

    if (!bThreadAtivada)
    {
        bRecebeuPrimeiroPacote = false;
        if (tmrTimerEstrategia)
        {
            tmrTimerEstrategia->stop();
            tmrTimerEstrategia.reset();
        }
        coachTest.reset();
    }

    if (bThreadAtivada)
    {
        referee.reset(new Referee());
        connect(referee.get(), &Referee::_vBallInPlay, this,
                &Estrategia::vIsBallInPlay_);
    }
}

void Estrategia::vReceiveVisionData_(const VisionPacket _packet)
{
    if (!bThreadAtivada)
        return;
    // Atualiza os dados dos robôs e da bola
    acEstrategia.get()->vAtualizaDadosVisao(_packet);
    referee->vUpdateBallPosition(acEstrategia->blBola->vt2dPosicaoAtual);

    if (bRecebeuPrimeiroPacote == false)
        bRecebeuPrimeiroPacote = true;

    // Se a estrategia ja recebeu o primeiro pacote de visao, ja recebeu o sinal
    // para iniciar e o timer ainda nao foi iniciado -> Inicia o timer
    if (bRecebeuPrimeiroPacote == true && bThreadAtivada == true &&
        !tmrTimerEstrategia)
    {
        tmrTimerEstrategia.reset(new QTimer(this));
        tmrTimerEstrategia->setTimerType(Qt::PreciseTimer);
        connect(tmrTimerEstrategia.get(), &QTimer::timeout, this,
                &Estrategia::vRun_);
        tmrTimerEstrategia->start(iTempoLoopEstrategia);

        // Inicializa path-planners
        QSize tamanhoCampo = acEstrategia.get()->geoCampo->szField();

        // rrtPathPlanner.reset(new RRT(tamanhoCampo));
        starvgPathPlanner.reset(new A_StarVG(iAnguloGrafo));
        mraPathPlanner.reset(
            new MRAStar(globalConfig.robotsPerTeam, tamanhoCampo, 250));

        coachTest.reset(new CoachTesting(acEstrategia.get()));
        coachReferee.reset(new CoachReferee(acEstrategia.get()));
        coachNormal.reset(new CoachTesting(acEstrategia.get()));

        coachReferee->vSetReferee(referee.get());
        connect(referee.get(), &Referee::_vBallInPlay, coachReferee.get(),
                &CoachReferee::vReceiveBallInPlay_);

        connect(coachNormal.get(), &CoachTesting::_vFinishedPlays, this,
                &Estrategia::vCoachFinishedPlay_);
        connect(coachTest.get(), &CoachTesting::_vFinishedPlays, this,
                &Estrategia::vCoachFinishedPlay_);
        connect(coachReferee.get(), &CoachReferee::_vFinishedPlays, this,
                &Estrategia::vCoachFinishedPlay_);

// Idealmente acho que ele começaria pelo CoachReferee
#ifdef RUN_COACH_TEST
        vMakeCoachActive(coachTest.get());
        coachTest->vSetMode(Coach::MANUAL_COACH);
#else
        vMakeCoachActive(coachReferee.get());
       // vMakeCoachActive(coachNormal.get());
#endif // RUN_COACH_TEST
    }
}

void Estrategia::vReceiveRefereeData_(const RefereePacket _packet)
{
    if (!referee)
        return;

    // Atualiza comando atual do referee
    if (referee->sslrefGetCommand() != _packet.sslrefCommand)
    {
        // Todos os comandos do referee começam com um Stop ou Halt
        if (_packet.sslrefCommand == SSL_Referee_Command_STOP ||
            _packet.sslrefCommand == SSL_Referee_Command_HALT ||
            _packet.sslrefCommand == SSL_Referee_Command_BALL_PLACEMENT_BLUE ||
            _packet.sslrefCommand == SSL_Referee_Command_BALL_PLACEMENT_YELLOW)
        {
#ifndef RUN_COACH_TEST
            // O coach Referee pode ser atribuído instantaneamente, não podemos
            // esperar terminar a play de algum coach anterior
            vMakeCoachActive(coachReferee.get());
            // Adiciona o CoachNormal para ser executado depois que o
            // CoachReferee terminar de executar a play atual
            vAddCoachToQueue(coachNormal.get());
#endif // !RUN_COACH_TEST
        }
    }

    referee->vUpdateDataFromPacket(_packet);

    if (acEstrategia->allies->teamColor() == timeAzul)
    {
        // Retira a jogada de goleiro do robo anterior
        if (acEstrategia->allies->iGetGoalieID() != _packet.blueTeam->iGoalieID)
        {
            acEstrategia->allies
                ->getPlayer(acEstrategia->allies->iGetGoalieID())
                ->vSetaJogada(Nenhuma);
        }
        acEstrategia->allies->vSetGoalieID(_packet.blueTeam->iGoalieID);
        acEstrategia->opponents->vSetGoalieID(_packet.yellowTeam->iGoalieID);
    }
    else
    {
        // Retira a jogada de goleiro do robo anterior
        if (acEstrategia->allies->iGetGoalieID() !=
            _packet.yellowTeam->iGoalieID)
        {
            acEstrategia->allies
                ->getPlayer(acEstrategia->allies->iGetGoalieID())
                ->vSetaJogada(Nenhuma);
        }
        acEstrategia->allies->vSetGoalieID(_packet.yellowTeam->iGoalieID);
        acEstrategia->opponents->vSetGoalieID(_packet.blueTeam->iGoalieID);
    }

    acEstrategia.get()->vt2dPosicaoBallPlacement = _packet.vt2dBallPlacement;
}

void Estrategia::vIsBallInPlay_(const bool _inPlay)
{
    qInfo() << QString("[Estrategia] Bola em jogo (%1)").arg(_inPlay);
}

void Estrategia::vRecebeDadosInterface(QVector2D posTimeout, int idGoleiro,
                                       int corTime, int ladoCampo,
                                       bool tipoAmbiente)
{
    // TODO: Fazer os ajustes necessários nesse slot na thread TestesRobos
    // para remover esse parâmetro
    Q_UNUSED(idGoleiro);
    acEstrategia.get()->vt2dPosicaoTimeout = posTimeout;

    acEstrategia->allies->vSetTeamColor(static_cast<TeamColor>(corTime));

    if (acEstrategia->allies->teamColor() == timeAzul)
    {
        acEstrategia->opponents->vSetTeamColor(timeAmarelo);
    }
    else
    {
        acEstrategia->opponents->vSetTeamColor(timeAzul);
    }

    acEstrategia->allies->vSetSide(ladoCampo);
    acEstrategia->opponents->vSetSide(-ladoCampo);
    acEstrategia.get()->bTipoAmbiente = tipoAmbiente;
}

void Estrategia::vRecebeCaminhosAlterados(const MotionPathFeedback& _paths)
{
    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        acEstrategia->allies->getPlayer(i)->path->vUpdatePath(
            _paths.robotsPaths[i].getPath());
        //        acEstrategia.get()->vt2dCaminhosPathPlanner[i].clear();
        //        acEstrategia.get()->vt2dCaminhosPathPlanner[i].append(acCaminhos.vt2dCaminhosPathPlanner[i]);
        //        acEstrategia.get()->bCaminhoAlterado[i] = false; //Reseta a
        //        flag quando o caminho é enviado de volta

        //        acEstrategia.get()->vt2dCaminhosOponentes[i].clear();
        //        acEstrategia.get()->vt2dCaminhosOponentes[i].append(acCaminhos.vt2dCaminhosOponentes[i]);
        //        acEstrategia.get()->bCaminhoOponenteAlterado[i] = false;
        //        //Reseta a flag quando o caminho é enviado de volta
    }
}

void Estrategia::vReceiveRadioData_(const RadioFeedback& _feedback)
{
    //     vtiRobosComBateriaOk.clear();
    //     vtiRobosComBateriaBaixa.clear();

    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        Robo* roboEstrategia = acEstrategia->allies->getPlayer(i);

        roboEstrategia->vAtualizaDadosRadio(_feedback);

        if (roboEstrategia->bRoboDetectado() == true)
        {
            //             if(roboEstrategia->fMinhaBateria() < fBateriaMinima)
            //                 vtiRobosComBateriaBaixa.append(i); // adiciona o
            //                 robô à lista
        }
        else // atualiza a bateria dos robôs que não estão em campo para a
             // possibilidade de substituição
        {
            //             if(roboEstrategia->fMinhaBateria() >= fBateriaMinima)
            //                 vtiRobosComBateriaOk.append(i); // adiciona o id
            //                 do robô à lista
        }
    }
}

float Estrategia::fGetFPSEstrategia()
{
    return fFPSEstrategia;
}

void Estrategia::vAdicionaLog()
{
    //    fileLogEstrategia.open(QIODevice::Append);
    //    QTextStream stream(&fileLogEstrategia);
    //    const QString tempo =
    //    QDateTime::currentDateTime().toString("hhLmm:ss.zzz");
    //    //     const QVector2D pBola = acEstrategia->vt2dPosicaoBola();

    //    //     stream << QString("%1;%2;%3;(%4@%5);")
    //    //                                      .arg(tempo)
    //    // .arg(estadoToQString(stateEstadoAtual))
    //    // .arg(Logger::getCommand(referee->sslrefGetCommand()))
    //    //                                      .arg(pBola.x())
    //    //                                      .arg(pBola.y());
    //    Robo* r;
    //    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    //    {
    //        r = acEstrategia->opponents->getPlayer(id);
    //        if (r->bRoboDetectado())
    //            stream << QString("(%1@%2@%3@%4)")
    //                          .arg(r->vt2dPosicaoAtual().x())
    //                          .arg(r->vt2dPosicaoAtual().y())
    //                          .arg(r->fAnguloAtual())
    //                          .arg(r->iID());
    //    }
    //    stream << ";&;";

    //    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    //    {
    //        r = acEstrategia->allies->getPlayer(id);
    //        if (r->bRoboDetectado())
    //            stream << QString("[%1@%2@%3@%4@%5]{%6@%7@%8@%9}")
    //                          .arg(r->vt2dPosicaoAtual().x())
    //                          .arg(r->vt2dPosicaoAtual().y())
    //                          .arg(r->fAnguloAtual())
    //                          .arg(Auxiliar::strFromID_Jogadas(r->jJogadaAtual()))
    //                          .arg(r->iID())
    //                          .arg(r->vt2dDestino().x())
    //                          .arg(r->vt2dDestino().y())
    //                          .arg(r->vt2dPontoAnguloDestino().x())
    //                          .arg(r->vt2dPontoAnguloDestino().y());
    //    }
    //    stream << "\n";
    //    fileLogEstrategia.close();
}

void Estrategia::vAddCoachToQueue(Coach* _coach)
{
    coachQueue.append(_coach);
}

void Estrategia::vMakeCoachActive(Coach* _coach)
{
    if (_coach != nullptr)
    {
        if (activeCoach != nullptr)
        {
            activeCoach->vReset();
        }
        activeCoach = _coach;
        activeCoach->vInitialize();

        qInfo() << QString("[Estrategia] Coach ativo = %1")
                       .arg(activeCoach->strName());
    }
    else
    {
        qWarning() << QString("[Estrategia] Tentando atribuir um coach nulo, "
                              "coach anterior (%1) será mantido!")
                          .arg(activeCoach->strName());
    }
}
