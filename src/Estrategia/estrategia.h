/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ESTATEGIA_H
#define ESTATEGIA_H

///
/// \file estrategia.h
/// \brief \a clsEstrategia
///

#include <QThread>
#include <QtCore>
#include <QMutex>
#include <QTimer>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QPointer>

#include "Ambiente/futbolenvironment.h"
#include "Goleiro/goleiro.h"
#include "Movimentacao/robotmovement.h"
#include "Movimentacao/motionpathfeedback.h"
#include "Path_Planning/path_p_rrt_2.h"
#include "Path_Planning/a_star_vg.h"
#include "Path_Planning/mrastar.h"
#include "Radio/radiofeedback.h"
#include "Posicionamento/Mines/mines.h"
// #include "Decisoes/decisoes.h"
#include "jogadasensaiadas.h"
#include "Rede/logger.h"
#include "Referee/referee.h"

#include "Debug/debugtypes.h"

#include "Estrategia/Coach/availablecoaches.h"

/**
 * @brief Define o comportamento do time durante os estados de Normal Start
 * \see Estrategia::vEscolheJogadaNormal
 */
enum ComportamentoTatico
{
    Comportamento_Retranca,
    Comportamento_Ultradefensivo,
    Comportamento_Defensivo,
    Comportamento_Equilibrado,
    Comportamento_Ofensivo,
    Comportamento_Ultraofensivo
};


/**
 * @brief Classe da estratégia
 * @details Esta é a classe que irá interpretar os comandos do referee e escolher
 * a jogada correta a ser executada no momento
 *
 */
class Estrategia : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Construtor da classe
     */
    Estrategia();

    /**
     * @brief Destrutor da classe
     */
    ~Estrategia();

    /**
     * @brief Retorna true se a thread está ativada e a estratégia está rodando
     * @return bool
     */
    bool bAtivada();

    /**
     * @brief Retorna o FPS médio da thread da estratégia
     * @return float - FPS médio
     */
    float fGetFPSEstrategia();

private:
    /**
     * @brief Adiciona uma linha de logs.
     * @attention Isso aqui é temporário, acho que seria mais útil possuir uma classe
     * genérica de logs, onde, as outras classes do código herdam desssa classe e
     * implementam seu próprio método de adicionar dados às logs, dessa forma fica
     * tudo mais uniforme e padronizado
     */
    void vAdicionaLog();

    /**
     * @brief Adiciona um Coach na fila de coachs a serem tornados ativos.
     *
     * Um coach só é tornado ativo quando o coach atual termina sua Play.
     * @see Coach::_vFinishedPlays.
     *
     * @param _coach
     */
    void vAddCoachToQueue(Coach* _coach);

    /**
     * @brief Força um Coach a se tornar ativo.
     *
     * IMPORTANTE: Isso fará com que a Play sendo executada no momento seja
     * abortada.
     *
     * @param _coach
     */
    void vMakeCoachActive(Coach* _coach);

    QFile fileLogEstrategia; /**< Arquivo de logs. */

    QScopedPointer<AmbienteCampo> acEstrategia; /**< Objeto do ambiente de campo da estratégia. */
    QScopedPointer<A_StarVG> starvgPathPlanner; /**< Objeto do path-planner #A_StarVG. */
    QScopedPointer<MRAStar> mraPathPlanner; /**< Objeto do path-planner #MRAStar. */
    // QScopedPointer<RRT> rrtPathPlanner; /**< Objeto do path-planner #RRT. */
    QScopedPointer<Referee> referee;   /**< Objeto da classe do referee. @see Referee */

    QScopedPointer<QTimer> tmrTimerEstrategia; /**< Gera a base de tempo para rodar a
                                                 estratégia. */

    // FPS
    QElapsedTimer etmTimerFPSEstrategia; /**< Timer para medição do FPS da thread da
                                              estratégia.*/
    float fFPSEstrategia;     /**< FPS médio da thread da estratégia. */
    QVector<int> iFPSCounter; /**< Vetor para cálculo do FPS médio da thread da
                                   estratégia.*/

    bool bThreadAtivada; /**< Indica se a thread está ativada. */
    bool bJogoRolando;   /**< Indica se o jogo está em normal game ou não. */
    bool bRecebeuPrimeiroPacote; /**< Indica se a estratégia já recebeu pelo menos um
                                   pacote de visão, o loop de execução só é executado
                                   após isso */

    // STP
    QSharedPointer<CoachTesting> coachTest;
    QSharedPointer<CoachReferee> coachReferee;
   QSharedPointer<CoachTesting> coachNormal;
    QVector<Coach*> coachQueue;
    Coach *activeCoach;

public slots:
    /**
     * @brief Recebe o sinal para ativar/desativar a thread
     *
     * @param _bThreadAtivada - Indica se é para ativar ou desativar (true/false)
     */
    void vAtivaThread(const bool _bThreadAtivada);

    /**
     * @brief Recebe os dados da visão filtrados pelo Filtro de Kalman
     *
     * @param _packet - Pacote com os dados
     *
     * @see Visao, KalmanFilter, VisionPacket
     */
    void vReceiveVisionData_(const VisionPacket _packet);

    /**
     * @brief Recebe os dados do referee necessários para a estratégia
     * funcionar
     *
     * @param _packet - Pacote com os dados
     */
    void vReceiveRefereeData_(const RefereePacket _packet);

    /**
     * @brief Recebe o evento indicando que a bola está em jogo (free kick foi
     * cobrado)
     *
     * @param _inPlay
     */
    void vIsBallInPlay_(const bool _inPlay);

    /**
     * @brief Recebe os dados da interface necessários para a estratégia funcionar
     * @param posTimeout - Posição inicial que os robôs ficam na hora do timeout
     * @param idGoleiro - ID do nosso goleiro
     * @param corTime - Cor do nosso time
     * @param ladoCampo - Lado que defendemos
     * @param tipoAmbiente - Tipo do ambiente de jogo (GR_SIM/CAMPO_REAL)
     *
     * @see RoboFeiSSL
     */
    void vRecebeDadosInterface(QVector2D posTimeout, int idGoleiro, int corTime,
                               int ladoCampo, bool tipoAmbiente);

    /**
     * @brief Recebe os  trajetos alterados pela thread de movimentação
     * @param acCaminhos - Objeto do ambiente de campo que possui os trajetos alterados
     *
     * @see MovimentacaoRobo
     */
    void vRecebeCaminhosAlterados(const MotionPathFeedback& _paths);

    /**
     * @brief Recebe os dados recebidos pelo rádio
     * @param _feedback - Objeto com os dados de cada robô..
     *
     * @see RadioFeedback
     */
    void vReceiveRadioData_(const RadioFeedback &_feedback);

private slots:
    /**
     * @brief Slot que se conecta com o timer para executar a estratégia
     * @see #tmrTimerEstrategia
     */
    void vRun_();

    void vCoachFinishedPlay_(const int _coachID);

signals:
   /**
    * @brief Envia informações recebidas sobre a movimentação do robô
    *
    * @param _acAmbiente - Objeto do ambiente de campo que possui as informações
    * @see vRecebeDadosMovimentacao
    */
   void vEnviaInformacoesMovimentacao(const AmbienteCampo &_acAmbiente);//
};


#endif // ESTATEGIA_H
