/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "jogadasensaiadas.h"

#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

#include <cmath>

JogadasEnsaiadas::JogadasEnsaiadas(const QSize tamanhoCampo,
                                   const QSize tamanhoGol,
                                   const int ladoCampo):
    p(),
    estadoAtual(0),
    distanciaMinimaPosicionamento(0),
    distanciaMinimaPosicionamentoUltimoEstado(0),
    pronto(false),
    szCampo(tamanhoCampo),
    szGol(tamanhoGol),
    iLadoCampo(ladoCampo)
{

}

JogadasEnsaiadas::Posicionamento::Posicionamento(const std::initializer_list<QVector2D> &thatDestinoReceptores):
    destinoReceptores(thatDestinoReceptores)
{
    //                                 COORDENADAS GENÉRICAS:
    //
    //     x = -2   x = -1                        x=0                      x = +1   x = +2
    //          |   #                             |                             #   |
    //   y = +2 |---#-----------------------------|-----------------------------#---|
    //          |   #                             |                             #   |
    //   y = +1 |____  _   _   _   _   _   _   _  |_   _   _   _   _   _   _    ____|
    //          |   |                             |                             |   |
    //   y = 0  |   |                D E F        |          A T A              |   |
    //          |   |                             |                             |   |
    //   y = -1 |----  -   -   -   -   -   -   -  |-   -   -   -   -   -   -    ----|
    //          |   #                             |                             #   |
    //   y = -2 |___#_____________________________|_____________________________#___|
    //
}

bool JogadasEnsaiadas::bReceptoresPosicionados(const QVector<QVector2D>& vt_vt2dPosicaoAtualReceptores)
{
    if(!pronto)
        qFatal(Q_FUNC_INFO);

    const Posicionamento& pAtual = p[estadoAtual];

    if(pAtual.destinoReceptores.size() != vt_vt2dPosicaoAtualReceptores.size())
        qFatal(Q_FUNC_INFO);

    if(estadoAtual == iNumeroDeEstados() -1)
    {
        return vt_vt2dPosicaoAtualReceptores.at(0).distanceToPoint(
                    vt2dCoordenadasGenericasParaCoordenadasCampo(pAtual.destinoReceptores.at(0))
                    ) < distanciaMinimaPosicionamentoUltimoEstado;
    }

    for(int i=0; i<vt_vt2dPosicaoAtualReceptores.size(); ++i)
    {
        if(vt_vt2dPosicaoAtualReceptores.at(i).distanceToPoint(
               vt2dCoordenadasGenericasParaCoordenadasCampo(pAtual.destinoReceptores.at(i))
               ) > distanciaMinimaPosicionamento)
        {
            return false;
        }
    }

    return true;
}

bool JogadasEnsaiadas::bAvancaEstado()
{
    ++estadoAtual;

    if(estadoAtual == p.size())
        return false;

    return true;
}

void JogadasEnsaiadas::vAdicionaPosicionamento(const JogadasEnsaiadas::Posicionamento &posicionamento)
{
    if(pronto)
        qFatal(Q_FUNC_INFO);

    p.append(posicionamento);
}

void JogadasEnsaiadas::vSetaDistanciaMinimaPosicionamento(const float distancia, float distanciaUltimoEstado)
{
    if(pronto)
        qFatal(Q_FUNC_INFO);

    distanciaMinimaPosicionamento = distancia;

    if(distanciaUltimoEstado < 0)
        distanciaUltimoEstado = distancia;

    distanciaMinimaPosicionamentoUltimoEstado = distanciaUltimoEstado;
}

const JogadasEnsaiadas::Posicionamento &JogadasEnsaiadas::posicionamentoAtual() const
{
    return p[estadoAtual];
}

int JogadasEnsaiadas::iEstadoAtual() const
{
    return estadoAtual;
}

int JogadasEnsaiadas::iNumeroDeEstados() const
{
    return p.size();
}

void JogadasEnsaiadas::vFinalizaPreparacao()
{
    pronto = true;
}

QVector2D JogadasEnsaiadas::vt2dCoordenadasGenericasParaCoordenadasCampo(const QVector2D coordenadasGenericas)
{
    return Auxiliar::vt2dCoordenadasGenericasParaCoordenadasCampo(coordenadasGenericas,
                                                                  iLadoCampo, szCampo, szGol);
}

QVector2D JogadasEnsaiadas::vt2dCoordenadasCampoParaCoordenadasGenericas(const QVector2D coordenadasCampo)
{
    return Auxiliar::vt2dCoordenadasCampoParaCoordenadasGenericas(coordenadasCampo,
                                                                  iLadoCampo, szCampo, szGol );
}

