/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef JOGADASENSAIADAS_H
#define JOGADASENSAIADAS_H

#include <QList>
#include <QVector2D>
#include <QSize>

///
/// \brief Classe utilizada para criar jogadas ensaiadas em lances de bola parada
///
/// \details todo
///
/// \example
/// ```
/// JogadasEnsaiadas jogs(tamanhoCampo, tamanhoGol, ladoCampoDefesa);
/// jogs.vAdicionaPosicionamento({ {1, .5}, {1, -.5} });
/// jogs.vAdicionaPosicionamento({ {1.5, 1.5}, {1.5, -1.5} });
/// jogs.vSetaDistanciaMinimaPosicionamento(300);
/// jogs.vFinalizaPreparacao();
/// ```
///
class JogadasEnsaiadas
{
public:

    ///
    /// \brief Único construtor da classe
    /// \details Tem como parâmetros as dimensões do campo e o campo de defesa aliado
    /// \param tamanhoCampo - Vetor com as dimensões do campo
    /// \param tamanhoGol - Vetor com as dimensões do gol {largura (x) x profundidade (y)}
    /// \param ladoCampo - Lado do campo de defesa
    ///
    JogadasEnsaiadas(const QSize tamanhoCampo, const QSize tamanhoGol, const int ladoCampo);
    JogadasEnsaiadas()=delete;
    JogadasEnsaiadas(const JogadasEnsaiadas& that)=delete;
    JogadasEnsaiadas& operator=(const JogadasEnsaiadas& that)=delete;

    ///
    /// \brief Conjunto de posicionamentos genéricos representando os posicionamentos
    /// \note Passe o receptor real (se houver) à primeiro posicição do vetor
    ///
    struct Posicionamento
    {
        ///
        /// \brief Cria Posicionamento com as coordenadas genéricas passadas.
        /// \example
        /// ```
        /// Posicionamento pos({ {1, 1.5}, {1, -1.5} });
        /// ```
        ///
        Posicionamento(const std::initializer_list<QVector2D>& thatDestinoReceptores);

        QVector<QVector2D> destinoReceptores;
    };

    ///
    /// \brief Verifica se os receptores já estão posicionados
    /// \details Caso o estado seja o último, apenas será considerada a posição
    ///  do primeiro receptor.
    /// \param vt_vt2dPosicaoAtualReceptores - Posições dos receptores (coordenadas campo)
    /// \return true - se os receptores estiverem corretamente posicionados
    ///
    bool bReceptoresPosicionados(const QVector<QVector2D>& vt_vt2dPosicaoAtualReceptores);

    ///
    /// \brief Tenta avançar o estado atual
    /// \details retorna true se a tarefa foi bem-sucedida
    ///
    bool bAvancaEstado();

    ///
    /// \brief Adiciona novo posicionamento
    ///
    void vAdicionaPosicionamento(const Posicionamento& posicionamento);

    ///
    /// \brief Seta a distância mínima para que os robôs estejam posicionados
    /// \param distancia - distância mínima
    /// \param distanciaUltimoEstado - opcional, se este parâmetro for passado,
    ///  será considerado no último estado apenas
    ///
    void vSetaDistanciaMinimaPosicionamento(const float distancia, float distanciaUltimoEstado = -1);

    ///
    /// \brief Retorna o posicionamento do estado atual
    ///
    const Posicionamento& posicionamentoAtual()const;

    ///
    /// \brief Retorna o estado atual
    /// \details A contagem começa a partir do zero
    ///
    int iEstadoAtual()const;

    ///
    /// \brief Retorna o número de estados
    ///
    int iNumeroDeEstados()const;

    ///
    /// \brief Função a ser chamada
    ///
    void vFinalizaPreparacao();

    ///
    /// \brief Realiza a transformação de coordenadas
    ///
    QVector2D vt2dCoordenadasGenericasParaCoordenadasCampo(const QVector2D coordenadasGenericas);

    ///
    /// \brief Realiza a transformação de coordenadas
    ///
    QVector2D vt2dCoordenadasCampoParaCoordenadasGenericas(const QVector2D coordenadasCampo);

private:
    QList<Posicionamento> p;
    int estadoAtual;

    float distanciaMinimaPosicionamento;
    float distanciaMinimaPosicionamentoUltimoEstado;

    bool pronto;
    const QSize szCampo;
    const QSize szGol;
    const int iLadoCampo;
};

#endif // JOGADASENSAIADAS_H
