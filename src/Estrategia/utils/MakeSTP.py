#!/usr/bin/python3

import argparse
import os

def get_args():
    parser = argparse.ArgumentParser(description="Create a template of a STP class")
    parser.add_argument(
        "type", type=str, help="The type of the STP class",
        choices=["Coach", "Play", "Role", "Tactic", "Skill"]
    )
    parser.add_argument("name", type=str, help="Name of the instance")
    parser.add_argument("--id", type=int, help="ID of the class")
    parser.add_argument("--overwrite", action="store_true", help="Allow overwrite")
    args = parser.parse_args()
    return args

def make_stp_template(stp_type, name, id, overwrite):
    if id is None:
        id = -1

    file_name = "{}{}".format(stp_type.lower(), name.lower())
    cpp_file_name = "{}.cpp".format(file_name)
    h_file_name = "{}.h".format(file_name)
    id_key = "{}_{}".format(stp_type.upper(), name.upper())

    header_guard = "{}_H".format(file_name.upper())


    cpp_file = open(f'{stp_type.lower()}template.cpp', 'r').read()

    cpp_file = cpp_file.replace("$$idkey$$", id_key)
    cpp_file = cpp_file.replace("$$name$$", name)
    cpp_file = cpp_file.replace("$$header$$", h_file_name)

    h_file = open(f'{stp_type.lower()}template.h', 'r').read()

    h_file = h_file.replace("$$idkey$$", id_key)
    h_file = h_file.replace("$$name$$", name)
    h_file = h_file.replace("$$id$$", str(id))
    h_file = h_file.replace("$$headerguard$$", header_guard)
    
    print("Informações: ")

    print(f"""
Nome do arquivo source: {cpp_file_name}
Nome do arquivo header: {h_file_name}
Nome da classe: {stp_type}{name}
""")

    confirm = input('Confirma? (s/n): ')
    
    if(confirm != 's'):
        print('Operação cancelada.')
        return

    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    os.chdir('..')

    if stp_type == 'Coach':
        os.chdir('Coach')
    elif stp_type == 'Play':
        os.chdir('Plays')
    elif stp_type == 'Role':
        os.chdir('Roles')
    elif stp_type == 'Skill':
        os.chdir('Skills')
    elif stp_type == 'Tactic':
        os.chdir('Tactics')
    
    if (not overwrite) and (os.path.exists(cpp_file_name) or os.path.exists(h_file_name)) :
        print('Impossível sobrescrever! Arquivo já existente.')
        exit(1)

    with open(cpp_file_name, 'w') as src:
        src.write(cpp_file)

    with open(h_file_name, 'w') as h:
        h.write(h_file)

    print('Feito!')


if __name__ == "__main__":
    args = get_args()
    
    make_stp_template(args.type, args.name, args.id, args.overwrite)