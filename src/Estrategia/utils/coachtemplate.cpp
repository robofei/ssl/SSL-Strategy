#include "$$header$$"

Coach$$name$$::Coach$$name$$(AmbienteCampo *_fieldEnv)
    : Coach(_fieldEnv)
{
    iID = $$idkey$$;

    // playA.reset(new PlayA(_fieldEnv));
    // playB.reset(new PlayB(_fieldEnv));
    // ...

    // bAddPlay(playA.get());
    // bAddPlay(playB.get());
    // ...
}

Coach$$name$$::~Coach$$name$$()
{

}

void Coach$$name$$::vRun()
{
    // Implementar esse método caso o coach seja manual
}

QString Coach$$name$$::strName() const
{
    return QString("$$name$$");
}

void Coach$$name$$::vInitialize()
{
    QVector<qint8> ids;

    for(qint8 id=0; id<PLAYERS_PER_SIDE; ++id)
    {
        // A play checa automaticamente se o robô está em campo ou não
        // Ver: Play::iGetValidIDs()
        ids.append(id);
    }

    for(const auto k : plays.keys())
    {
        plays[k].get()->vSetAvailableIDs(ids);
    }
    bInitialized = true;
}
