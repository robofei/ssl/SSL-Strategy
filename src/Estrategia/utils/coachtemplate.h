#ifndef $$headerguard$$
#define $$headerguard$$

#include "coach.h"
#include "Estrategia/Plays/availableplays.h"

/**
 * @brief Enum com o ID do coach.
 *
 * Deve aumentar de 20 em 20 de coach para coach.
 */
enum
{
    $$idkey$$ = $$id$$
};

/**
 * @brief Implementação de um coach de teste
 */
class Coach$$name$$ : public Coach
{
    // Necessário para poder enviar sinais e ter slots
    Q_OBJECT

private:
    // Definição das plays
    // QSharedPointer<PlayTesting2> playTesting;

    /**
     * @brief Implementa a lógica para executar as ações do coach.
     */
    void vRun() override;

public:
    /**
     * @brief Construtor.
     *
     * @param _fieldEnv - Ponteiro para o ambiente do campo.
     */
    Coach$$name$$(AmbienteCampo *_fieldEnv);
    /**
     * @brief Destrutor.
     */
    ~Coach$$name$$();

    /**
     * @brief Retorna o nome do coach.
     *
     * @return
     */
    QString strName() const override;
    /**
     * @brief Faz a inicialização necessária para o coach.
     */
    void vInitialize() override;
};

#endif // $$headerguard$$
