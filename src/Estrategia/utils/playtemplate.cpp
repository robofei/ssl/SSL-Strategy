#include "$$header$$"

double Play$$name$$::dScore = 0.0;

Play$$name$$::Play$$name$$(AmbienteCampo *_fieldEnv) :
    Play(_fieldEnv)
{
    iID = $$idkey$$;

    // Inicializa os roles
    // const int robotsB = 4;

    roleGoalie.reset(new RoleGoalie(_fieldEnv));
    // roleA.reset( new RoleA(_fieldEnv) );

    // rolesB.reserve(robotsB);
    // for(qint8 i=0; i<robotsB; ++i)
    // {
    //     rolesB.append(QSharedPointer<RoleB>::create(_fieldEnv));
    // }

    // for(int i=1; i<rolesB.size(); ++i)
    // {
    //     rolesB[i]->bMakeDuplicate(i);
    // }

    bAddRole(roleGoalie.get());
    // bAddRole(roleA.get());
    // for(auto& role : rolesB)
    // {
    //     bAddRole(role.get());
    // }
}

Play$$name$$::~Play$$name$$()
{

}

QString Play$$name$$::strName() const
{
    return QString("$$name$$");
}

void Play$$name$$::vInitialize()
{
    const int allySide = fieldEnvironment->allies->iGetSide();

    roleGoalie->zone.vConfigZone(fieldEnvironment->geoCampo->vt2dGoalCenter(allySide).toPoint(),
                                 2e3);

    // ...

    bInitialized = true;
}

bool Play$$name$$::bCheckPreConditions()
{
    return true;
}

bool Play$$name$$::bCheckEndConditions()
{
    // ...

    return playStatus == FINISHED || playStatus == ABORTED;
}

double Play$$name$$::dGetScore() const
{
    return Play$$name$$::dScore;
}

void Play$$name$$::vUpdateScore()
{
    Play$$name$$::dScore += 1.0;
}

void Play$$name$$::vSaveScore()
{

}
