#ifndef $$headerguard$$
#define $$headerguard$$
// é importante modificar o header guard para o nome do arquivo correspondente

#include "play.h"
#include "Estrategia/Roles/availableroles.h"

enum
{
    $$idkey$$ = $$id$$ // Consultar arquivo availableplays.h e pular 20 do último ID
};

class Play$$name$$ : public Play
{
    Q_OBJECT
private:
    // Define os roles da play


    QSharedPointer<RoleGoalie> roleGoalie;
    // QSharedPointer<RoleA> rolesA;
    // QVector<QSharedPointer<RoleB>> rolesB;


    static double dScore;

public:
    Play$$name$$(AmbienteCampo *_fieldEnv);
    ~Play$$name$$();

    QString strName() const override;
    void vInitialize() override;
    bool bCheckPreConditions() override;
    bool bCheckEndConditions() override;
    double dGetScore() const override;
    void vUpdateScore() override;
    void vSaveScore() override;
};

#endif // $$headerguard$$
