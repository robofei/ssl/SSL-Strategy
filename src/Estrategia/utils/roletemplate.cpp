#include "$$header$$"

#define SET_TACTIC(tactic)                        \
if(currentTactic == nullptr ||                    \
   currentTactic->id() != tactic->id())           \
{                                                 \
    bSetTactic(tactic->id());                     \
}                                                 \

Role$$name$$::Role$$name$$(AmbienteCampo *_fieldEnv)
    : Role(_fieldEnv)
{
    iID = $$idkey$$;

    // tacticA.reset(new TacticA(_fieldEnv));
    // tacticB.reset(new TacticB(_fieldEnv));
    // ...
    
    // bAddTactic(tacticA.get());
    // bAddTactic(tacticB.get());
    // ...
}

Role$$name$$::~Role$$name$$()
{

}

void Role$$name$$::vRun()
{
    // SET_TACTIC(tacticA)

    // ...

    if(currentTactic != nullptr)
    {
        currentTactic->vRunTactic();
    }
}

QString Role$$name$$::strName() const
{
    return QString("&$$name$$");
}

void Role$$name$$::vInitialize()
{
    bInitialized = true;
}

int Role$$name$$::iRoleType()
{
    return $$idkey$$;
}

void Role$$name$$::vCheckRoleFinished()
{
    bFinished = false;
}
