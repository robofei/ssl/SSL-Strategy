#ifndef $$headerguard$$
#define $$headerguard$$

#include "role.h"
#include "Estrategia/Tactics/availabletactics.h"

enum
{
    $$idkey$$ = $$id$$
};

class Role$$name$$ : public Role
{
    Q_OBJECT
private:
    void vRun() override;

    // Tactics
    // QSharedPointer<TacticA> tacticA;
    // QSharedPointer<TacticB> tacticB;
    // ...

public:
    Role$$name$$(AmbienteCampo *_fieldEnv);
    ~Role$$name$$();

    QString strName() const override;
    void vInitialize() override;
    int iRoleType() override;
    void vCheckRoleFinished() override;
};

#endif // $$headerguard$$
