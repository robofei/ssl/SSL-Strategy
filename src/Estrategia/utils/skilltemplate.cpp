#include "$$header$$"

Skill$$name$$::Skill$$name$$(AmbienteCampo *_fieldEnv) : Skill(_fieldEnv)
{
    iID = $$idkey$$;
}

Skill$$name$$::~Skill$$name$$()
{

}

void Skill$$name$$::vRun()
{

}

QString Skill$$name$$::strName() const
{
    return QString("$$name$$");
}

void Skill$$name$$::vInitialize()
{
    bInitialized = true;
    bFinished = false;
}
