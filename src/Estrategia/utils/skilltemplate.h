#ifndef $$headerguard$$
#define $$headerguard$$

#include "skill.h"

/**
 * @brief Define o ID inicial das instâncias dessa skill.
 *
 * @note Esse enum é de implementação obrigatória!
 */
enum
{
    $$idkey$$ = $$id$$
};

class Skill$$name$$ : public Skill
{
private:
    /**
     * @brief Roda a skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vRun() override;

    // Variáveis

public:
    Skill$$name$$(AmbienteCampo *_fieldEnv);
    ~Skill$$name$$();

    /**
     * @brief Retorna o nome da skill.
     *
     * @note Esse método é de implementação obrigatória!
     *
     * @return
     */
    QString strName() const override;

    /**
     * @brief Inicializa parâmetros internos da skill.
     *
     * @note Esse método é de implementação obrigatória!
     */
    void vInitialize() override;
};

#endif // $$headerguard$$
