#include "$$header$$"

#define SET_SKILL(skill)                        \
if(currentSkill == nullptr ||                   \
   currentSkill->id() != skill->id())           \
{                                               \
    bSetSkill(skill->id());                     \
}                                               \

Tactic$$name$$::Tactic$$name$$(AmbienteCampo *_fieldEnv) :
    Tactic(_fieldEnv)
{
    iID = $$idkey$$;

    // skillA.reset(new SkillA(_fieldEnv));
    // skillB.reset(new SkillB(_fieldEnv));
    // ...

    // bAddSkill(skillA.get());
    // bAddSkill(skillB.get());
    // ...
}

Tactic$$name$$::~Tactic$$name$$()
{

}

void Tactic$$name$$::vRun()
{
    // SET_SKILL(skillA)

    // ...
    
    if(currentSkill != nullptr)
    {
        currentSkill->vRunSkill();
    }
}

QString Tactic$$name$$::strName() const
{
    return QString("$$name$$");
}

void Tactic$$name$$::vInitialize()
{
    bInitialized = true;
}
