#ifndef $$headerguard$$
#define $$headerguard$$

#include "tactic.h"
#include "Estrategia/Skills/availableskills.h"

enum
{
    $$idkey$$ = $$id$$
};

class Tactic$$name$$ : public Tactic
{
    Q_OBJECT
private:
    void vRun() override;

    // Skills
    // QSharedPointer<SkillA> skillA;
    // QSharedPointer<SkillB> skillB;

public:
    Tactic$$name$$(AmbienteCampo *_fieldEnv);
    ~Tactic$$name$$();

    QString strName() const override;
    void vInitialize() override;
};

#endif // $$headerguard$$
