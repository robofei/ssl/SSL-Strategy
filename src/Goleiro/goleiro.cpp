﻿/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "goleiro.h"
#include "qdebug.h"

Goleiro::Goleiro()
{
//    goleiroNeural.initialize();
    fAvancoGoleiro = 0.5*globalConfig.robotDiameter;
}

// void Goleiro::vGoleiroNormal(AmbienteCampo *ac, Jogadas* jogadas, const bool penalty)
// {
// #if TIPO_GOLEIRO == GOLEIRO_ALTERNATIVO
//     vGoleiroNormalAlternativo(ac, jogadas, penalty);
// #elif TIPO_GOLEIRO == GOLEIRO_LEGACY
//     vGoleiroNormalLegacy(ac);
// #endif
// }


void Goleiro::vGoleiroNormalLegacy(AmbienteCampo *ac)
{
    Robo* roboGoleiro = ac->allies->getPlayer(ac->allies->iGetGoalieID());
    QVector<QVector2D> retaBola = ac->blBola->vvt2dGetFilteredPositions();
    QVector2D posGoleiro(0,0);

    int iIdAdversario =  ac->iAchaInimigoProximoPonto(ac->vt2dPosicaoBola());
    int iLadoDoCampo = ac->allies->iGetSide();
    int iLarguraDoGol = ac->geoCampo->szGoal().height();

    //Se existe um oponente proximo da bola, marca a frente dele.
    //A principio, o goleiro ira marcar a reta da bola formada pelos pontos calculados pelo kalman
    if(iIdAdversario != -1 &&
       ac->opponents->getPlayer(iIdAdversario)->vt2dPosicaoAtual().distanceToPoint(retaBola.constFirst()) < 2*globalConfig.robotDiameter)
    {
        const Atributos& atbOponente = ac->opponents->getPlayer(iIdAdversario)->atbRetornaRobo();
        QVector2D miraOponente = atbOponente.vt2dPosicaoAtual + QVector2D(100*qCos(atbOponente.rotation), 100*qSin(atbOponente.rotation));
        retaBola.clear();
        retaBola.append(atbOponente.vt2dPosicaoAtual);
        retaBola.append(miraOponente);
    }
    roboGoleiro->vSetaPontoAnguloDestino( Auxiliar::vt2dReajustaPonto(roboGoleiro->vt2dPosicaoAtual(),
                                                                         ac->vt2dPosicaoBola(),
                                                                         2*globalConfig.robotDiameter) );   // angulo do goleiro

    int ladoBolinha = (ac->vt2dPosicaoBola().x()/qAbs(ac->vt2dPosicaoBola().x()));

    if(retaBola.size() > 0)
    {
        if(retaBola.constFirst().distanceToPoint(retaBola.constLast()) > globalConfig.ballDiameter )
        {

            if(ladoBolinha != ac->allies->iGetSide() && iIdAdversario == -1)//Se esta do lado oposto do campo, so acompanha a bolinha
            {
                retaBola.clear();
                retaBola.append(ac->vt2dPosicaoBola());
                retaBola.append(ac->vt2dPosicaoBola());
            }

            posGoleiro = Auxiliar::vt2dCalculaDefesaGoleiro(retaBola, iLadoDoCampo, iLarguraDoGol,
                                                            ac->geoCampo->szField(), fAvancoGoleiro);
            roboGoleiro->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
            roboGoleiro->vSetaIgnorarBola(bNaoConsideraBola);
        }
        else
        {
            if(ac->bBolaDentroAreaDefesa() == true && ladoBolinha == ac->allies->iGetSide())
            {
                vGoleiroTiraBola(.4, roboGoleiro->iID(),
                                 QVector2D(ac->opponents->iGetSide() * ac->geoCampo->szField().width()/4,
                                           ac->geoCampo->szField().height()/2), ac);

                posGoleiro = roboGoleiro->vt2dDestino();
                fAvancoGoleiro = 0.5*globalConfig.robotDiameter;
            }
        }
    }

//    ExtU_rede_simuliink_T posB;
//    posB.posBolaX[0] = acAmbienteGoleiro->vt2dPosicaoAtualBola().x();
//    posB.posBolaX[1] = acAmbienteGoleiro->vt2dPosicaoAtualBola().y();
//    goleiroNeural.rede_simuliink_U = posB;

//    goleiroNeural.step();

//    posGoleiro = QVector2D(goleiroNeural.rede_simuliink_Y.posRoboX[0],goleiroNeural.rede_simuliink_Y.posRoboX[1]);
//    qDebug() << posGoleiro;
    if(posGoleiro.length() == 0)
    {
        retaBola.clear();
        posGoleiro = Auxiliar::vt2dCalculaDefesaGoleiro(retaBola, iLadoDoCampo, iLarguraDoGol,
                                                                     ac->geoCampo->szField(), fAvancoGoleiro);
        roboGoleiro->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
        roboGoleiro->vSetaIgnorarBola(bNaoConsideraBola);
    }

    roboGoleiro->vSetaDestino(posGoleiro);
}

// void Goleiro::vGoleiroNormalAlternativo(AmbienteCampo *ac, Jogadas* jogadas, const bool penalty)
// {
//     Robo* goleiro = ac->allies->getPlayer(ac->allies->iGetGoalieID());
//     const QVector2D posBola = ac->vt2dPosicaoBola();
//     const QVector2D vt2dVelocidadeBola = ac->vt2dVelocidadeBola();
//     const int iLadoCampo = ac->allies->iGetSide();
//
//     ///
//     /// \todo Setar o avanço do goleiro de forma mais inteligente e eficiente.
//     ///
//
//     if(penalty)
//         vSetaAvancoGoleiro(0);
//     else if(fabs(posBola.x() - ac->geoCampo->szField().width()/2*iLadoCampo) > 2000)
//         vSetaAvancoGoleiro(globalConfig.robotDiameter*2.f);
//     else
//         vSetaAvancoGoleiro(globalConfig.robotDiameter/2.f);
// //    vSetaAvancoGoleiro(0);
//
//     // Se o adversário estiver distante da bola ou distante do gol, o ignoramos
//     int iIdAdversario =  ac->iAchaInimigoProximoPonto(posBola);
//     if(ac->opponents->getPlayer(iIdAdversario)->vt2dPosicaoAtual().distanceToPoint(posBola) > 250 ||
//        ac->opponents->getPlayer(iIdAdversario)->vt2dPosicaoAtual().distanceToPoint(ac->geoCampo->vt2dGoalCenter(iLadoCampo)) > 3000)
//     {
//         iIdAdversario = -1;
//     }
//
//     const float larguraGol = ac->geoCampo->szGoal().height();
//     const float xGoleiro = iLadoCampo*fabs(ac->geoCampo->szField().width()/2 - fAvancoGoleiro);
//     const float yProjecao = Auxiliar::fProjecaoBolaNoGolAliado(vt2dVelocidadeBola, posBola, xGoleiro);
//
//     // Se a bola estiver parada dentro da área, o goleiro tirará a bola de lá.
//     if(ac->bBolaDentroAreaDefesa() && ac->dVelocidadeBola() < .5 && iLadoCampo*posBola.x() > 0)
//     {
//         // vGoleirioTiraBolaAlternativo(.5, goleiro->iID(), ac, jogadas);
//     }
//     // Se a bola estiver se movendo em direção ao gol, o goleiro ficará posicionado para defendê-la
//     else if(ac->dVelocidadeBola() > .5 && vt2dVelocidadeBola.x()*iLadoCampo >= 0 &&
//             fabs(yProjecao) < 1.5f*larguraGol/2)
//     {
//         QVector2D destino = Auxiliar::vt2dDestinoGoleiroBolaEmMovimento(
//                                 vt2dVelocidadeBola,
//                                 posBola,
//                                 goleiro->vt2dPosicaoAtual(),
//                                 (ac->geoCampo->szField().width()/2 - globalConfig.robotDiameter/2)*iLadoCampo);
//
// //        if(penalty)
// //        {
// //            destino = QVector2D(
// //                          ac->geoCampo->szField().width()/2*iLadoCampo,
// //                          Auxiliar::fProjecaoBolaNoGolAliado(vt2dVelocidadeBola, posBola,
// //                                                             ac->geoCampo->szField().width()/2*iLadoCampo));
// //        }
//
//         goleiro->vSetaDestino(penalty ? QVector2D(xGoleiro, yProjecao) : destino);
//         goleiro->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
//         goleiro->vSetaIgnorarBola(true);
//
//         // Liga o roller se o goleiro está próximo da bola
//         if(posBola.distanceToPoint(goleiro->vt2dPosicaoAtual()) < 300)
//         {
//             goleiro->vLigaRoller(100);
//
//             // Se estivermos em pênalti, o robô estará pronto para defender a bola
//             if(penalty)
//                 goleiro->vChutar(CHIP_KICK_STRONG);
//             else
//                 goleiro->vDesligaChute();
//         }
//         else
//             goleiro->vDesligaRoller();
//     }
//     // Se algum adversário está mirando no gol
//     else if(iIdAdversario != -1)
//     {
//         const Robo *adv = ac->opponents->getPlayer(iIdAdversario);
//
//         float yDestino = Auxiliar::fProjecaoBolaNoGolAliado(adv->vt2dPontoDirecaoMira() - adv->vt2dPosicaoAtual(),
//                                                             posBola, xGoleiro);
//
//         // Para evitar que o goleiro vá em alguma ponta do gol e deixe algum canto muito aberto.
//         if(yDestino > larguraGol/2)
//             yDestino = larguraGol/2;
//         else if(yDestino < -larguraGol/2)
//             yDestino = - larguraGol/2;
//
//         vGoleiroSetaYDestino(yDestino, ac);
//         goleiro->vDesligaRoller();
//         goleiro->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
//         goleiro->vSetaIgnorarBola(true);
//     }
//     // Posiciona o goleiro de forma a deixar o menor ângulo livre possível
//     else
//     {
//         float yDestino = Auxiliar::fMelhorPosicaoGoleiroParaFecharGolAliado(posBola,
//                                                                             ac->geoCampo->vt2dGoalCenter(iLadoCampo),
//                                                                             larguraGol,
//                                                                             xGoleiro);
//         if(yDestino > larguraGol/2)
//             yDestino = larguraGol/2;
//         else if(yDestino < -larguraGol/2)
//             yDestino = - larguraGol/2;
//
//         vGoleiroSetaYDestino(yDestino, ac);
//         goleiro->vDesligaRoller();
//         goleiro->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
//         goleiro->vSetaIgnorarBola(true);
//     }
//
//     if(fabs(posBola.x()) < fabs(goleiro->vt2dPosicaoAtual().x()) - iDistanciaMaximaRoboBola)
//     {
//         goleiro->vSetaPontoAnguloDestino(posBola);
//     }
// }

void Goleiro::vGoleiroStop(AmbienteCampo *ac)
{
    Robo* goleiro = ac->allies->getPlayer(ac->allies->iGetGoalieID());
    const QVector2D posBola = ac->vt2dPosicaoBola();
    const int iLadoCampo = ac->allies->iGetSide();

    const float larguraGol = ac->geoCampo->szGoal().height();
    const float xGoleiro = iLadoCampo*fabs(ac->geoCampo->szField().width()/2 - fAvancoGoleiro);

    float yDestino = Auxiliar::fMelhorPosicaoGoleiroParaFecharGolAliado(posBola,
                                                                        ac->geoCampo->vt2dGoalCenter(iLadoCampo),
                                                                        larguraGol,
                                                                        xGoleiro);

    if(yDestino > larguraGol/2)
        yDestino = larguraGol/2;
    else if(yDestino < -larguraGol/2)
        yDestino = - larguraGol/2;

    vGoleiroSetaYDestino(yDestino, ac);
    goleiro->vDesligaRoller();
    goleiro->vSetMaxLinearVelocity(globalConfig.stopLinearVelocity);
    goleiro->vSetaIgnorarBola(true);

}

void Goleiro::vGoleiroBallPlacementDefesa(AmbienteCampo *ac, const QVector2D posDesignada)
{
    /// \todo Fazer essa função considerar a posição designada da bola
    Robo* goleiro = ac->allies->getPlayer(ac->allies->iGetGoalieID());

    if(goleiro->jJogadaAtual() != Robo_Goleiro)
        return;

    const QVector2D posBola = ac->vt2dPosicaoBola();

    // Se a bola estiver fora da área, posiciona o goleiro como se fosse STOP
    if(!Auxiliar::bPontoDentroArea(ac->geoCampo->szField(), ac->geoCampo->szDefenseArea(),
                                   ac->allies->iGetSide(), posBola))
    {
        vGoleiroStop(ac);
    }
    else
    {
        QVector2D destinoGoleiro = posBola + 1000*(QVector2D(0, 0) - posBola).normalized();
        goleiro->vSetaDestino(destinoGoleiro);
    }

    goleiro->vSetMaxLinearVelocity(globalConfig.stopLinearVelocity);
    goleiro->vSetaIgnorarBola(false);
    goleiro->vSetaPontoAnguloDestino(QVector2D(0, 0));

}

void Goleiro::vSetaAvancoGoleiro(float _avanco)
{
    fAvancoGoleiro = _avanco;
}

void Goleiro::vGoleiroSetaYDestino(const float yDestino, AmbienteCampo* ac)
{
    ac->allies->getPlayer(ac->allies->iGetGoalieID())->vSetaDestino(
                QVector2D((ac->geoCampo->szField().width()/2 - fAvancoGoleiro) * ac->allies->iGetSide(),
                          yDestino));
}

void Goleiro::vGoleiroTiraBola(float _fVelocidade, int _iIDRobo, QVector2D _vt2dPontoMira, AmbienteCampo *acAmbiente)
{
    const Atributos& atbRobo = acAmbiente->allies->getPlayer(_iIDRobo)->atbRetornaRobo();

    QVector2D vt2dMiraRobo;
    QVector2D vt2dPontoChute = atbRobo.vt2dDestino;
    QVector2D vt2dPontoAngulo = acAmbiente->vt2dPosicaoBola();

    bool bChegouNaBola = false;
    float fDistanciaBola = globalConfig.robotDiameter*1;

    vt2dMiraRobo = _vt2dPontoMira;

    fDistanciaBola = acAmbiente->vt2dPosicaoBola().distanceToPoint(atbRobo.vt2dPosicaoAtual);

    if(fDistanciaBola > globalConfig.robotDiameter)
        fDistanciaBola = globalConfig.robotDiameter;

    if(atbRobo.vt2dPosicaoAtual.distanceToPoint(vt2dMiraRobo) < acAmbiente->vt2dPosicaoBola().distanceToPoint(vt2dMiraRobo))
    {
        fDistanciaBola = globalConfig.robotDiameter;
        acAmbiente->allies->getPlayer(_iIDRobo)->vSetaIgnorarBola(bConsideraBola);
    }

    QVector2D vt2dPontoTeste = vt2dMiraRobo;
    vt2dPontoTeste = Auxiliar::vt2dReajustaPonto(acAmbiente->vt2dPosicaoBola(), vt2dPontoTeste, -3*globalConfig.robotDiameter);

    QVector3D vt3dRobo;
    vt3dRobo.setZ(otAliado);
    vt3dRobo = atbRobo.vt2dPosicaoAtual;
    QVector<QVector3D> vt3dAux;
    vt3dAux.clear();
    vt3dAux.append(vt3dRobo);

    acAmbiente->allies->getPlayer(_iIDRobo)->vSetaIgnorarBola(bConsideraBola);


    if(Auxiliar::bChecaInterseccaoObjetosLinha(acAmbiente->vt2dPosicaoBola(), vt2dPontoTeste, bNaoConsideraBola, 90, vt3dAux) == true)
    {
        acAmbiente->allies->getPlayer(_iIDRobo)->vLigaRoller(100);
        if(atbRobo.vt2dDestino.distanceToPoint(atbRobo.vt2dPosicaoAtual) <= globalConfig.robotDiameter)//Quando chegar ao destino se aproxima mais um pouco da bola
        {
            fDistanciaBola -= globalConfig.robotDiameter;
            acAmbiente->allies->getPlayer(_iIDRobo)->vSetaIgnorarBola(bConsideraBola);
            acAmbiente->allies->getPlayer(_iIDRobo)->vSetMaxLinearVelocity(_fVelocidade);

        }
        else
        {
            fDistanciaBola -= globalConfig.robotDiameter/2;
        }

        if(acAmbiente->vt2dPosicaoBola().distanceToPoint(atbRobo.vt2dPosicaoAtual) <= globalConfig.robotDiameter)//Quando estiver bem próximo se aproxima mais devagar
        {
            fDistanciaBola =0;//-= globalConfig.robotDiameter/8;

            bChegouNaBola = true;
        }

        if(acAmbiente->vt2dPosicaoBola().distanceToPoint(atbRobo.vt2dPosicaoAtual) <= 300)//Quando estiver bem próximo se aproxima mais devagar
        {
            acAmbiente->allies->getPlayer(_iIDRobo)->vSetaIgnorarBola(bNaoConsideraBola);
            acAmbiente->allies->getPlayer(_iIDRobo)->vSetMaxLinearVelocity(_fVelocidade);


            vt2dPontoAngulo = vt2dMiraRobo;
        }
    }
    else
    {
        acAmbiente->allies->getPlayer(_iIDRobo)->vSetaIgnorarBola(bConsideraBola);
        acAmbiente->allies->getPlayer(_iIDRobo)->vSetMaxLinearVelocity(_fVelocidade);

        fDistanciaBola = globalConfig.ballDiameter;
        acAmbiente->allies->getPlayer(_iIDRobo)->vDesligaRoller();


    }

    if(fDistanciaBola < globalConfig.robotDiameter/2)
    {
        fDistanciaBola = 20;
        acAmbiente->allies->getPlayer(_iIDRobo)->vSetMaxLinearVelocity(.4);

    }

    vt2dPontoChute = Auxiliar::vt2dCalculaPontoCobranca(vt2dMiraRobo, acAmbiente->vt2dPosicaoBola(), fDistanciaBola);

    if(acAmbiente->allies->getPlayer(_iIDRobo)->bPodeChutar(vt2dMiraRobo-acAmbiente->vt2dPosicaoBola()) && bChegouNaBola == true)
    {
        acAmbiente->allies->getPlayer(_iIDRobo)->vChutar(CHIP_KICK_STRONG);
        acAmbiente->allies->getPlayer(_iIDRobo)->vDesligaRoller();
    }
    else
    {
        acAmbiente->allies->getPlayer(_iIDRobo)->vDesligaChute();
    }

    acAmbiente->allies->getPlayer(_iIDRobo)->vSetaPontoAnguloDestino(vt2dPontoAngulo);
    acAmbiente->allies->getPlayer(_iIDRobo)->vSetaDestino(vt2dPontoChute);
}

// void Goleiro::vGoleirioTiraBolaAlternativo(float velocidade, qint8 idRobo, AmbienteCampo *ac, Jogadas *jogadas)
// {
//     jogadas->vCobrarChuteBolaParada(idRobo, ac);
//
//     if(ac->allies->getPlayer(idRobo)->fMaxLinearVelocity() > velocidade)
//         ac->allies->getPlayer(idRobo)->vSetMaxLinearVelocity(velocidade);
// }
