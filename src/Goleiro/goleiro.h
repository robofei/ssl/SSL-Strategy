/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef GOLEIRO_H
#define GOLEIRO_H

///
/// \file goleiro.h
/// \brief \a Goleiro
///

#include "Ambiente/futbolenvironment.h"
// #include "Jogadas/jogadas.h"
//#include "GoleiroNeural/rede_simuliink.h"


/**
 * @brief Classe responsável por tomar as decisões do goleiro.
 *
 */
class Goleiro
{
public:
    /**
     * @brief Construtor da classe.
     *
     */
    Goleiro();

    // Não está sendo utilizado
//    rede_simuliinkModelClass goleiroNeural;

    /**
     * @brief Chama a função de controlar o goleiro.
     * @param ac - Objeto do ambiente do campo.
     * @see TIPO_GOLEIRO
     */
    // void vGoleiroNormal(AmbienteCampo* ac, Jogadas* jogadas = nullptr, const bool penalty = false);

    /**
     * @brief Faz o controle do goleiro durante o normal game.
     * @param ac - Objeto do ambiente do campo.
     */
    void vGoleiroNormalLegacy(AmbienteCampo *ac);

    /**
     * @brief Faz o controle do goleiro durante o normal game.
     * @param ac - Objeto do ambiente do campo.
     */
    // void vGoleiroNormalAlternativo(AmbienteCampo* ac,  Jogadas *jogadas = nullptr, const bool penalty = false);

    ///
    /// \brief Posiciona o goleiro de forma a deixar o menor ângulo do gol livre
    /// \param ac
    ///
    void vGoleiroStop(AmbienteCampo* ac);

    void vGoleiroBallPlacementDefesa(AmbienteCampo* ac, const QVector2D posDesignada);

    /**
     * @brief Atribue o valor da distância que o goleiro deve ficar da linha do gol.
     * @param _avanco - Distância que o goleiro deve ficar da linha do gol.
     */
    void vSetaAvancoGoleiro(float _avanco);

    ///
    /// \brief Seta o destino do goleiro, sendo a coordenada x obtida em função da variável #fAvancoGoleiro
    /// \param yDestino - Coordenada y de destino
    /// \param ac - Objeto do ambiente de campo
    ///
    void vGoleiroSetaYDestino(const float yDestino, AmbienteCampo *ac);

private:

    float fAvancoGoleiro; /**< Distancia que o goleiro tem que ficar da linha do gol [mm]. */

    /**
     * @brief Faz o controle dele quando é necessário tirar a bola da área de defesa.
     *
     * @param _iVelocidade - Velocidade máxima [0-100].
     * @param _iIDRobo - ID do goleiro.
     * @param _vt2dPontoMira - Ponto em que o goleiro deve mirar.
     * @param acAmbiente - Ambiente do campo.
     * @return QVector2D - Posição do goleiro.
     */
    void vGoleiroTiraBola(float _fVelocidade, int _iIDRobo, QVector2D _vt2dPontoMira, AmbienteCampo *acAmbiente);
    // void vGoleirioTiraBolaAlternativo(float velocidade, qint8 idRobo, AmbienteCampo* ac, Jogadas* jogadas);
};

#endif // GOLEIRO_H
