#include "decisoestesteinterface.h"
#include "ui_decisoestesteinterface.h"

#include <QVector2D>
#include <QVector>
// #include "Decisoes/decisoes.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

DecisoesTesteInterface::DecisoesTesteInterface(QWidget* parent)
    : QDialog(parent), ui(new Ui::DecisoesTesteInterface)
{
    ui->setupUi(this);

    connect(ui->pbDecisoesPasse, &QPushButton::released, this,
            &DecisoesTesteInterface::SIGNAL_solicitaTesteDecisoesPasse);
    connect(ui->pbDecisoesChute, &QPushButton::released, this,
            &DecisoesTesteInterface::SIGNAL_solicitaTesteDecisoesChute);

    //     Decisoes::resetInstance();
}

DecisoesTesteInterface::~DecisoesTesteInterface()
{
    delete ui;
}

void DecisoesTesteInterface::vDesenhaHeatMap(AmbienteCampo* ac,
                                             QVector<QVector3D>& posicoes,
                                             QVector<qint16>& avaliacoes, int X,
                                             int Y)
{
    const QSize szDesenho = ac->geoCampo->szField();
    QScopedPointer<QPixmap> pm(
        new QPixmap(szDesenho.width(), szDesenho.height()));
    QPainter painter(pm.get());

    // Cria um fundo de campo cinza
    painter.fillRect(pm->rect(), Qt::gray);

    painter.translate(0, szDesenho.height());
    painter.scale(1, -1);

    painter.setPen(QPen(Qt::gray, 5));
    // desenha as avaliações em suas respectivas posições
    for (int i = 0; i < X; ++i)
    {
        for (int j = 0; j < Y; ++j)
        {
            const QVector2D pos =
                posicoes[X * j + i].toVector2D() +
                QVector2D(szDesenho.width(), szDesenho.height()) / 2;

            const qint16 aval = avaliacoes[X * j + i];

            const QColor color =
                (aval < 125) ? Auxiliar::corEscalaLinear(Qt::red, Qt::yellow,
                                                         aval, 0, 125)
                             : Auxiliar::corEscalaLinear(Qt::yellow, Qt::green,
                                                         aval, 125, 250);

            painter.setBrush(color);

            painter.drawRect(pos.x(), pos.y(),
                             ui->sbEscalaPassoDesenho->value(),
                             ui->sbEscalaPassoDesenho->value());
        }
    }

    // desenha as linhas do campo e o gol
    painter.setPen(QPen(Qt::white, 50));
    painter.setBrush(Qt::NoBrush);

    painter.drawRect(pm->rect());
    painter.drawLine(szDesenho.width() / 2, 0, szDesenho.width() / 2,
                     szDesenho.height() - 1);

    // Área negativa
    painter.drawRect(
        QRect(QPoint(0, szDesenho.height() / 2 -
                            ac->geoCampo->szDefenseArea().height() / 2),
              ac->geoCampo->szDefenseArea()));

    // Desenha círculo
    painter.drawEllipse(QPoint(szDesenho.width() / 2, szDesenho.height() / 2),
                        ac->geoCampo->iCenterCircleRadius(),
                        ac->geoCampo->iCenterCircleRadius());

    // Área positiva
    painter.drawRect(
        QRect(QPoint(szDesenho.width() - ac->geoCampo->szDefenseArea().width(),
                     szDesenho.height() / 2 -
                         ac->geoCampo->szDefenseArea().height() / 2),
              ac->geoCampo->szDefenseArea()));

    // Aliado preto e oponente azul
    if (ac->allies->iGetSide() == XNegativo)
    {
        painter.setPen(QPen(Qt::black, 150));
    }
    else
    {
        painter.setPen(QPen(Qt::blue, 150));
    }

    // Gol negativo
    painter.drawLine(
        0, szDesenho.height() / 2 - ac->geoCampo->szDefenseArea().width() / 2,
        0, szDesenho.height() / 2 + ac->geoCampo->szDefenseArea().width() / 2);

    // Aliado branco e oponente preto
    if (ac->allies->iGetSide() == XNegativo)
    {
        painter.setPen(QPen(Qt::blue, 150));
    }
    else
    {
        painter.setPen(QPen(Qt::black, 150));
    }

    // Gol positivo
    painter.drawLine(
        szDesenho.width() - 1,
        szDesenho.height() / 2 - ac->geoCampo->szDefenseArea().width() / 2,
        szDesenho.width() - 1,
        szDesenho.height() / 2 + ac->geoCampo->szDefenseArea().width() / 2);

    // desenha os robôs adversários

    painter.setPen(QPen(Qt::blue, 50));
    painter.setBrush(Qt::NoBrush);

    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        const Robo* r = ac->opponents->getCPlayer(i);
        if (!r->bRoboDetectado())
            continue;

        const QVector2D pos =
            r->vt2dPosicaoAtual() +
            QVector2D(szDesenho.width(), szDesenho.height()) / 2;

        painter.drawEllipse(pos.toPoint(), globalConfig.robotDiameter / 2,
                            globalConfig.robotDiameter / 2);
    }

    // desenha posição da bola

    //    painter.setPen(Qt::NoPen);
    //    painter.setBrush(Qt::black);
    //    painter.drawEllipse(
    //                (ac->vt2dPosicaoBola() + QVector2D(szDesenho.width(),
    //                                                   szDesenho.height())/2).toPoint(),
    //                50, 50
    //                );

    // salva a imagem
    pm->save(QDateTime::currentDateTime()
                 .toString(Qt::ISODateWithMs)
                 .replace(QChar(':'), QChar('_')) +
             "decisoes.png");

    ui->lHeatMapPreview->setPixmap((*pm).scaledToWidth(
        ui->lHeatMapPreview->width(), Qt::SmoothTransformation));
}

void DecisoesTesteInterface::SLOT_decisoesPasse(AmbienteCampo* ac)
{
    const QSize campo = ac->geoCampo->szField();

    const int X = campo.width() / ui->sbEscalaPassoDesenho->value();
    const int Y = campo.height() / ui->sbEscalaPassoDesenho->value();

    QVector<qint16> avaliacoes(X * Y, 0);
    QVector<QVector3D> posicoes(X * Y, QVector3D());

    for (int i = 0; i < X; ++i)
    {
        for (int j = 0; j < Y; ++j)
        {
            posicoes[X * j + i] = QVector3D(
                -campo.width() / 2 + i * ui->sbEscalaPassoDesenho->value(),
                -campo.height() / 2 + j * ui->sbEscalaPassoDesenho->value(), 0);
        }
    }

    //     Decisoes::vRealizaPrevisoes(
    //                 QVector2D(), posicoes, ac->vt2dPosicaoBola(),
    //                 ac->vt3dPegaPosicaoTodosObjetos(otOponente, true),
    //                 campo, ac->allies->iGetSide(),
    //                 ac->geoCampo->szDefenseArea().width(),
    //                 ac->geoCampo->vt2dGoalCenter(ac->opponents->iGetSide())
    //     );
    //
    //     avaliacoes = Decisoes::vtiAvalicoesReceptores();

    vDesenhaHeatMap(ac, posicoes, avaliacoes, X, Y);
}

void DecisoesTesteInterface::SLOT_decisoesChute(AmbienteCampo* ac)
{
    const QSize campo = ac->geoCampo->szField();

    const int X = campo.width() / ui->sbEscalaPassoDesenho->value();
    const int Y = campo.height() / ui->sbEscalaPassoDesenho->value();

    QVector<qint16> avaliacoes(X * Y, 0);
    QVector<QVector3D> posicoes(X * Y, QVector3D());

    for (int i = 0; i < X; ++i)
    {
        for (int j = 0; j < Y; ++j)
        {
            posicoes[X * j + i] = QVector3D(
                -campo.width() / 2 + i * ui->sbEscalaPassoDesenho->value(),
                -campo.height() / 2 + j * ui->sbEscalaPassoDesenho->value(), 0);
            //             Decisoes::vRealizaPrevisoes(
            //                         QVector2D(), QVector<QVector3D>(),
            //                         posicoes[X*j + i].toVector2D(),
            //                     ac->vt3dPegaPosicaoTodosObjetos(otOponente,
            //                     true), campo, ac->allies->iGetSide(),
            //                     ac->geoCampo->szDefenseArea().width(),
            //                     ac->geoCampo->vt2dGoalCenter(ac->opponents->iGetSide())
            //                     );
            //
            //             avaliacoes[X*j + i] =
            //             Decisoes::iAvaliacaoChuteAoGol();
        }
    }

    vDesenhaHeatMap(ac, posicoes, avaliacoes, X, Y);
}
