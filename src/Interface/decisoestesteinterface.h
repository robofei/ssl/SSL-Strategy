#ifndef DECISOESTESTEINTERFACE_H
#define DECISOESTESTEINTERFACE_H

#include <QDialog>

#include "Ambiente/futbolenvironment.h"

namespace Ui {
class DecisoesTesteInterface;
}

class DecisoesTesteInterface : public QDialog
{
    Q_OBJECT

public:
    explicit DecisoesTesteInterface(QWidget *parent = nullptr);
    ~DecisoesTesteInterface();

private:
    Ui::DecisoesTesteInterface *ui;

    void vDesenhaHeatMap(AmbienteCampo* ac,
                         QVector<QVector3D>& posicoes,
                         QVector<qint16>& avaliacoes,
                         int X, int Y);
signals:
    void SIGNAL_solicitaTesteDecisoesPasse();
    void SIGNAL_solicitaTesteDecisoesChute();

public slots:
    void SLOT_decisoesPasse(AmbienteCampo* ac);
    void SLOT_decisoesChute(AmbienteCampo* ac);
};

#endif // DECISOESTESTEINTERFACE_H
