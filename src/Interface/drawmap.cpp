﻿/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Interface/drawmap.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "qfont.h"
#include "qglobal.h"
#include "qmath.h"
#include "qrgb.h"

QColor DrawMap::corFundoCampo = Qt::darkGreen;
QColor DrawMap::corRoboAmarelo = Qt::yellow;
QColor DrawMap::corBordaRoboAmarelo = Qt::darkYellow;
QColor DrawMap::corRoboAzul = Qt::blue;
QColor DrawMap::corBordaRoboAzul = Qt::cyan;
QColor DrawMap::corLinhasCampo = Qt::white;
QColor DrawMap::corIdRobo = Qt::magenta;
QColor DrawMap::corFpsBaixo = Qt::black;
QColor DrawMap::corFpsAlto = Qt::white;
QColor DrawMap::corBolaEmCampo = QColor(255, 180, 0);
QColor DrawMap::corBolaForaDeCampo = Qt::red;

DrawMap::DrawMap(AmbienteCampo* acAmbiente, QWidget* parent)
    : QOpenGLWidget(parent)

{
    elapsed = 0;
    setAutoFillBackground(false);
    QSurfaceFormat suf = this->format();
    suf.setSamples(5);
    this->setFormat(suf);
    this->setMouseTracking(true);

    acWidgetCampo = acAmbiente;
    szCampo = acWidgetCampo->geoCampo->szField();

    bBotaoDireitoPressionado = false;
    bBotaoEsquerdoPressionado = false;
    vt2dPosicaoMouse = QVector2D(0, 0);
    vt2dPosicaoInicialMouseBotaoDireito = QVector2D(0, 0);

    bAntiAliasing = false;
    bSaboia = false;
    bPosicaoTimeout = false;
    bPathPlanners = false;
    bDirecaoRobo = false;
    bPosicoesSimulacao = false;
    bMiraBola = false;
    bLinhasGoleiro = false;
    bPredicoesKalman = false;
    bGrafo = false;
    bBallPlacement = false;
    bLiberdadeRobos = false;
    bRobosMines = false;
    bDestacarReceptor = false;
    bVetorBola = false;
    bReposicaoMouse = false;
    bDestacaRegiaoRoles = false;
    iLadoCampoUtilizado = XPositivo;

    bTirarPrint = false;

    iFPSUI = 0;
    iFPSKalman = 0;
    iFPSEstrategia = 0;
    iFPSMovimentacao = 0;
    iFPSDesenho = 0;

    vtiAvaliacoesReceptores.clear();
    dTempoUltimaPrevisao = 0;
    iAvaliacaoChuteAoGol = 0;

    pen = new QPen(DrawMap::corLinhasCampo, dLarguraLinhasBrancas,
                   Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
    pntPincel = new QPainter;
}

DrawMap::~DrawMap()
{
    delete pen;
    delete pntPincel;
}

void DrawMap::vAnimar()
{
    elapsed = (elapsed + qobject_cast<QTimer*>(sender())->interval()) % 1000;
    update();
}

void DrawMap::paintEvent(QPaintEvent* e)
{
    QElapsedTimer tempoLoopDesenho;
    tempoLoopDesenho.start();

    QSize widgetSize = e->rect().size();
    vMudarDimensoes(widgetSize);

    pntPincel->begin(this);

    pntPincel->setRenderHint(QPainter::Antialiasing, bAntiAliasing);
    pntPincel->fillRect(e->rect(), DrawMap::corFundoCampo);
    pen->setColor(DrawMap::corLinhasCampo);

    vDesenhaAmbiente();
    vDesenharPosicoesSimulacao();
    vDesenharMiraBola();
    vDesenharLinhasGoleiro();
    vDesenharPredicoesKalman();
    vDesenhaPosicaoBallPlacement();
    vDesenhaLiberdadeRobos();
    vDesenhaRobosMines();
    vDesenhaReceptor();
    vDesenhaVetorVelocidadeBola();
    vDesenhaTesteMeioCampo();
    vDesenhaPontos(acWidgetCampo->blBola->vvt2dGetFilteredPositions(),
             QColor(200, 69, 0), 2.5);
    vDesenhaVelocidadeBolaMouse();
    vDesenhaGrafo();
    vDesenhaTestePathPlanner();
    vDesenhaTrajetoAtualMClick();
    drawRealTrajectory();
    drawDebugData();

    acWidgetCampo->debugInfo->clearDeadData();

    this->vMostrarFPS(iFPSUI, iFPSKalman, iFPSEstrategia, iFPSMovimentacao);

    QVector2D playTimePos =
        acWidgetCampo->vt2dPosicaoBola() + QVector2D(200, 200);
    vConvertePontoRealParaImagem(playTimePos);
    pntPincel->setPen(Qt::black);
    pntPincel->drawText(
        playTimePos.toPoint(),
        QString("%1 s").arg(acWidgetCampo->iGetRemainingPlayTime() / 1000));
    this->vEscrevePosicaoMouse();
    //-------------------------------------------------------------------------
    int FPS = qRound(1 / (etmFPSDesenhoCampo.nsecsElapsed() / 1e9));
    iFPSCounter.prepend(FPS);
    iFPSDesenho = Auxiliar::iCalculaFPS(iFPSCounter);
    etmFPSDesenhoCampo.restart();

    pntPincel->end();

    this->vTirarPrint(e->rect());

    const double tempoTotal = tempoLoopDesenho.nsecsElapsed() / 1e6;

    if (tempoTotal > iTempoLoopInterface)
        qWarning() << "Tempo Desenho " << QString::number(tempoTotal, 'f', 2)
                   << " ms";
    else if (tempoTotal > 1.2 * iTempoLoopInterface)
        qCritical() << "Tempo Desenho " << QString::number(tempoTotal, 'f', 2)
                    << " ms";
}

void DrawMap::mousePressEvent(QMouseEvent* event)
{
    szCampo = acWidgetCampo->geoCampo->szField();

    QPoint position = event->pos();
    /// \todo Essa conta é feita 3x+, melhor fazer uma função
    vt2dPosicaoMouse = QVector2D(
        position.x() / static_cast<float>(sizeTamanhoWidget.width()) *
                (szCampo.width() + 2 * globalConfig.fieldOffset) -
            (szCampo.width() / 2 + globalConfig.fieldOffset),
        -(position.y() / static_cast<float>(sizeTamanhoWidget.height()) *
              (szCampo.height() + 2 * globalConfig.fieldOffset) -
          (szCampo.height() / 2 + globalConfig.fieldOffset)));

    if (event->button() == Qt::LeftButton)
        bBotaoEsquerdoPressionado = true;
    else if (event->button() == Qt::RightButton)
    {
        bBotaoDireitoPressionado = true;
        vt2dPosicaoInicialMouseBotaoDireito = vt2dPosicaoMouse;
    }

    emit clicked(vt2dPosicaoMouse, event->button());
}

void DrawMap::mouseMoveEvent(QMouseEvent* event)
{
    szCampo = acWidgetCampo->geoCampo->szField();

    QPoint position = event->pos();
    /// \todo Essa conta é feita 3x+, melhor fazer uma função
    vt2dPosicaoMouse = QVector2D(
        position.x() / static_cast<float>(sizeTamanhoWidget.width()) *
                (szCampo.width() + 2 * globalConfig.fieldOffset) -
            (szCampo.width() / 2 + globalConfig.fieldOffset),
        -(position.y() / static_cast<float>(sizeTamanhoWidget.height()) *
              (szCampo.height() + 2 * globalConfig.fieldOffset) -
          (szCampo.height() / 2 + globalConfig.fieldOffset)));
    emit moved(vt2dPosicaoMouse);
}

void DrawMap::mouseReleaseEvent(QMouseEvent* event)
{
    szCampo = acWidgetCampo->geoCampo->szField();

    QPoint position = event->pos();
    /// \todo Essa conta é feita 3x+, melhor fazer uma função
    vt2dPosicaoMouse = QVector2D(
        position.x() / static_cast<float>(sizeTamanhoWidget.width()) *
                (szCampo.width() + 2 * globalConfig.fieldOffset) -
            (szCampo.width() / 2 + globalConfig.fieldOffset),

        -(position.y() / static_cast<float>(sizeTamanhoWidget.height()) *
              (szCampo.height() + 2 * globalConfig.fieldOffset) -
          (szCampo.height() / 2 + globalConfig.fieldOffset)));

    if (event->button() == Qt::LeftButton)
        bBotaoEsquerdoPressionado = false;
    else if (event->button() == Qt::RightButton)
        bBotaoDireitoPressionado = false;

    emit released(vt2dPosicaoMouse, event->button());
}

void DrawMap::vMudarDimensoes(QSize widgetSize)
{
    szCampo = acWidgetCampo->geoCampo->szField();
    if (widgetSize != sizeTamanhoWidget || sizeTamanhoCampo != szCampo)
    {
        sizeTamanhoWidget = widgetSize;

        fEscalaX = (2.0 * globalConfig.fieldOffset + szCampo.width()) / widgetSize.width();
        fEscalaY =
            (2.0 * globalConfig.fieldOffset + szCampo.height()) / widgetSize.height();

        vAjustaEscalaImagens();

        iLarguraGol = acWidgetCampo->geoCampo->szGoal().height();
        iProfundidadeGol = acWidgetCampo->geoCampo->szGoal().width();
        iLarguraDefesa = acWidgetCampo->geoCampo->szDefenseArea().height();
        iProfundidadeDefesa = acWidgetCampo->geoCampo->szDefenseArea().width();

        sizeTamanhoCampo = szCampo;
    }
}

void DrawMap::vSetAntiAliasing(bool ativar)
{
    bAntiAliasing = ativar;
}

void DrawMap::vSetSaboia(bool ativar)
{
    bSaboia = ativar;
}

void DrawMap::vSetTimeout(bool ativar)
{
    bPosicaoTimeout = ativar;
}

void DrawMap::vSetPathPlanners(bool ativar)
{
    bPathPlanners = ativar;
}

void DrawMap::vSetDirecaoRobo(bool ativar)
{
    bDirecaoRobo = ativar;
}

void DrawMap::vSetPosicoesSimulacao(bool ativar)
{
    bPosicoesSimulacao = ativar;
}

void DrawMap::vSetMiraBola(bool ativar)
{
    bMiraBola = ativar;
}

void DrawMap::vSetLinhasGoleiro(bool ativar)
{
    bLinhasGoleiro = ativar;
}

void DrawMap::vSetPredicoesKalman(bool ativar)
{
    bPredicoesKalman = ativar;
}

void DrawMap::vSetGrafo(bool ativar, const VisibilityGraph* grafo)
{
    bGrafo = ativar;
    if (grafo != nullptr)
        vgGrafoDesenho = *grafo;
}

void DrawMap::vSetBallPlacement(bool ativar)
{
    bBallPlacement = ativar;
}

void DrawMap::vSetLiberdadeRobos(bool ativar, const QVector<qint16>& avaliacoes,
                                 const qint64 tempo, const qint16 chuteGol)
{
    vtiAvaliacoesReceptores = avaliacoes;
    dTempoUltimaPrevisao = tempo * 1e-9;
    iAvaliacaoChuteAoGol = chuteGol;
    bLiberdadeRobos = ativar;
}

void DrawMap::vSetRobosMines(bool ativar)
{
    bRobosMines = ativar;
}

void DrawMap::vSetDestacaReceptor(bool ativar)
{
    bDestacarReceptor = ativar;
}

void DrawMap::vSetVetorBola(bool ativar)
{
    bVetorBola = ativar;
}

void DrawMap::vSetReposicaoMouse(bool ativar)
{
    bReposicaoMouse = ativar;
}

void DrawMap::vSetaFPS(int ui, int kalman, int estrategia, int movimentacao)
{
    iFPSUI = ui;
    iFPSKalman = kalman;
    iFPSEstrategia = estrategia;
    iFPSMovimentacao = movimentacao;
}

void DrawMap::vSetMeioCampo(const int& lado)
{
    iLadoCampoUtilizado = lado;
}

void DrawMap::setRobotTrajectory(bool _enable, const QVector<QVector2D> _points)
{
    drawRobotTrajectory = _enable;

    robotTrajectory.clear();
    robotTrajectory.append(_points);
}

void DrawMap::vPrint()
{
    bTirarPrint = true;
}

bool DrawMap::bGetBotaoEsquerdoPressionado()
{
    return bBotaoEsquerdoPressionado;
}

bool DrawMap::bGetBotaoDireitoPressionado()
{
    return bBotaoDireitoPressionado;
}

QVector2D DrawMap::vt2dPosicaoInicialCliqueDireito()
{
    return vt2dPosicaoInicialMouseBotaoDireito;
}

QSize DrawMap::sizeField() const
{
    return sizeTamanhoCampo;
}

void DrawMap::vRecebeTrajetosTestePathPlanner(
    const VisibilityGraph* grafo, const QVector<QVector2D>& caminhoAStar,
    const QVector<QVector2D>& caminhoAStarOriginal,
    const QVector<QVector2D>& pontosArvore,
    const QVector<QVector2D>& paisArvore, const QVector<QVector2D>& caminhoRRT,
    const QVector<QVector2D>& caminhoRRTOriginal)
{
    vgGrafoDesenho = *grafo;
    vt2dCaminhoAStar = caminhoAStar;
    vt2dCaminhoOriginalAStar = caminhoAStarOriginal;

    vt2dPontosArvore = pontosArvore;
    vt2dPaisArvore = paisArvore;
    vt2dCaminhoRRT = caminhoRRT;
    vt2dCaminhoOriginalRRT = caminhoRRTOriginal;
}

void DrawMap::vRecebeTrajetoAtualMClick(const QVector<QVector2D>& trajeto)
{
    vt2dTrajetoAtualMClick = trajeto;
}

void DrawMap::vTirarPrint(const QRect& rct)
{
    if (bTirarPrint)
    {
        bTirarPrint = false;
        QPixmap print(this->size());
        pntPincel->begin(&print);
        pntPincel->setRenderHint(QPainter::Antialiasing, true);

        pntPincel->fillRect(rct, QColor(255, 255, 255));
        pen->setColor(Qt::black);

        this->vDesenhaAmbiente();
        this->vDesenharPathPlanners();
        this->vDesenharDirecaoRobo();
        this->vDesenharPosicoesSimulacao();
        this->vDesenharMiraBola();
        this->vDesenharLinhasGoleiro();
        this->vDesenharPredicoesKalman();
        this->vDesenhaPosicaoBallPlacement();
        this->vDesenhaLiberdadeRobos();
        this->vDesenhaRobosMines();
        this->vDesenhaReceptor();
        this->vDesenhaVetorVelocidadeBola();
        this->vDesenhaTesteMeioCampo();
        this->vDesenhaPontos(acWidgetCampo->blBola->vvt2dGetFilteredPositions(),
                             QColor(200, 69, 0), 2.5);
        this->vDesenhaVelocidadeBolaMouse();
        this->vDesenhaGrafo();
        this->vDesenhaTestePathPlanner();
        this->vDesenhaTrajetoAtualMClick();
        pntPincel->setRenderHint(QPainter::Antialiasing, false);

        pntPincel->end();
        QDir dir = QDir::current();
        if (!dir.cd("Prints"))
        {
            dir.mkdir("Prints");
            dir.cd("Prints");
        }
        QString fileName = QLatin1String("");
        fileName += dir.path() + QStringLiteral("/");
        fileName +=
            QDateTime::currentDateTime().toString(QStringLiteral("h:mm:s"));
        fileName += QLatin1String(".png");

        print.save(fileName);
    }
}

void DrawMap::vCarregaImagens()
{
    imPosicaoTimeout.load(":/Imagens/PosicaoTimeout.png", "PNG");
    imPadreSaboia.load(":/Imagens/PadreSaboia.png", "PNG");
    imInimigo.load(":/Imagens/Inimigo.png", "PNG");
}

void DrawMap::vAjustaEscalaImagens()
{
    vCarregaImagens();
    imPadreSaboia = imPadreSaboia.scaled(
        QSize(sizeTamanhoWidget.width() / 2, sizeTamanhoWidget.height()),
        Qt::AspectRatioMode::IgnoreAspectRatio);
    imInimigo = imInimigo.scaled(
        QSize(sizeTamanhoWidget.width() / 2, sizeTamanhoWidget.height()),
        Qt::AspectRatioMode::IgnoreAspectRatio);
    imPosicaoTimeout = imPosicaoTimeout.scaled(
        QSize(2 * globalConfig.robotDiameter / fEscalaX, 2 * globalConfig.robotDiameter / fEscalaY),
        Qt::AspectRatioMode::IgnoreAspectRatio);
}

void DrawMap::vDesenhaAmbiente()
{
    QRect retanguloCampo(globalConfig.fieldOffset / fEscalaX, globalConfig.fieldOffset / fEscalaY,
                         qRound((szCampo.width()) / fEscalaX),
                         qRound((szCampo.height()) / fEscalaY));

    if (bSaboia == true)
    {
        pntPincel->setOpacity(0.1);

        if (acWidgetCampo->allies->iGetSide() == XNegativo)
        {
            pntPincel->drawImage(QPoint(0, 0), imPadreSaboia);
            pntPincel->drawImage(
                QPoint(retanguloCampo.width() / 2 + globalConfig.fieldOffset / fEscalaX, 0),
                imInimigo);
        }
        else
        {
            pntPincel->drawImage(
                QPoint(retanguloCampo.width() / 2 + globalConfig.fieldOffset / fEscalaX, 0),
                imPadreSaboia);
            pntPincel->drawImage(QPoint(0, 0), imInimigo);
        }
        pntPincel->setOpacity(1);
    }

    vDesenhaRegioesCampo();
    pntPincel->setPen(*pen);

    pntPincel->drawRect(retanguloCampo);

    retanguloCampo = QRect(globalConfig.fieldOffset / fEscalaX, globalConfig.fieldOffset / fEscalaY,
                           qRound(szCampo.width() / fEscalaX),
                           qRound(szCampo.height() / fEscalaY));

    pntPincel->drawLine(retanguloCampo.width() / 2 + globalConfig.fieldOffset / fEscalaX,
                        globalConfig.fieldOffset / fEscalaY,
                        retanguloCampo.width() / 2 + globalConfig.fieldOffset / fEscalaX,
                        retanguloCampo.height() + globalConfig.fieldOffset / fEscalaY);

    pntPincel->drawLine((globalConfig.fieldOffset + iProfundidadeDefesa) / fEscalaX,
                        retanguloCampo.height() / 2 + globalConfig.fieldOffset / fEscalaY,
                        retanguloCampo.width() +
                            (globalConfig.fieldOffset - iProfundidadeDefesa) / fEscalaX,
                        retanguloCampo.height() / 2 + globalConfig.fieldOffset / fEscalaY);

    pntPincel->drawEllipse(
        QPoint(retanguloCampo.width() / 2 + globalConfig.fieldOffset / fEscalaX,
               retanguloCampo.height() / 2 + globalConfig.fieldOffset / fEscalaY),
        qRound(acWidgetCampo->geoCampo->iCenterCircleRadius() / fEscalaX),
        qRound(acWidgetCampo->geoCampo->iCenterCircleRadius() / fEscalaY));

    vDesenhaAreaDefesa();
    vDesenhaAreaRestricao();
    vDesenhaGol(acWidgetCampo->allies->teamColor(),
                acWidgetCampo->allies->iGetSide());

    if (bPosicaoTimeout == true)
        vDesenhaPosicaoTimeout(acWidgetCampo->vt2dPosicaoTimeout);

    vDesenharPathPlanners();
    vDesenharDirecaoRobo();
    vDesenhaObjetos();

    if (acWidgetCampo->sslrefComandoRefereeAtual ==
        SSL_Referee_Command_STOP) // Desenho circulo de distancia minima da
                                  // bolinha no stop
    {
        vDesenhaCirculo(acWidgetCampo->vt2dPosicaoBola(),
                        globalConfig.safety.ball.stop, QColor(200, 69, 0), 1.5);
    }
}

void DrawMap::vDesenhaObjetos()
{
    QPen penAzul(DrawMap::corBordaRoboAzul, 2, Qt::SolidLine, Qt::SquareCap,
                 Qt::RoundJoin);
    QPen penAmarelo(DrawMap::corBordaRoboAmarelo, 2, Qt::SolidLine,
                    Qt::SquareCap, Qt::RoundJoin);
    QPen penLaranja(QColor(255, 184, 108), 2, Qt::SolidLine, Qt::SquareCap,
                    Qt::RoundJoin);

    Robo* r;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        r = acWidgetCampo->opponents->getPlayer(id);

        if (r->bRoboDetectado())
        {
            if (acWidgetCampo->opponents->teamColor() == timeAzul)
            {
                pntPincel->setPen(penAzul);
                pntPincel->setBrush(DrawMap::corRoboAzul);
            }
            else
            {
                pntPincel->setPen(penAmarelo);
                pntPincel->setBrush(DrawMap::corRoboAmarelo);
            }

            vDesenhaRobo(r->atbRetornaRobo(), false);
            vDesenhaCirculo(r->getPrediction(), 200, Qt::black, 2);
        }
    }

    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        r = acWidgetCampo->allies->getPlayer(id);

        if (r->bRoboDetectado())
        {
            const QColor clrCor =
                acWidgetCampo->bGolContra(r->iID()) ? Qt::red : Qt::darkGray;
            // Direcao da mira do robo
            // vDesenhaMiraRobo(r->vt2dPosicaoAtual(),
            // r->vt2dPontoDirecaoMira(),
            //                  clrCor);
            vDesenhaMiraRobo(r->vt2dPosicaoAtual(), r->vt2dPontoAnguloDestino(),
                             clrCor);

            vDrawZone(r->fzGetZone(), r->path->rgbGetColor());
            vDrawRoller(r->polGetRoller());
            vDesenhaPonto(r->vt2dRollerCenter(), Qt::red, 3);

            if (r->bKickSensor())
            {
                vDesenhaContornoRobo(r->vt2dPosicaoAtual(), r->fAnguloAtual(),
                                     Qt::red);
            }

            if (r->kickChuteAtual() > KICK_NONE &&
                r->kickChuteAtual() <= KICK_STRONG)
            {
                vDesenhaIndicacaoChute(r->vt2dPosicaoAtual(), Qt::red);
            }
            else if (r->kickChuteAtual() >= CHIP_KICK_SOFT &&
                     r->kickChuteAtual() <= CHIP_KICK_STRONG)
            {
                vDesenhaIndicacaoChute(r->vt2dPosicaoAtual(), Qt::yellow);
            }

            // if (id == 2)
            // {
            //     drawRobotConstraint(r->prsbcConstraint,
            //     r->vt2dPosicaoAtual());
            // }

            if (acWidgetCampo->allies->teamColor() == timeAzul)
            {
                penAzul.setColor(r->path->rgbGetColor());
                pntPincel->setPen(penAzul);
                pntPincel->setBrush(DrawMap::corRoboAzul);
            }
            else
            {
                penAmarelo.setColor(r->path->rgbGetColor());
                pntPincel->setPen(penAmarelo);
                pntPincel->setBrush(DrawMap::corRoboAmarelo);
            }

            if (r->bPegaIgnorarBola() == bConsideraBola)
            {
                pntPincel->setPen(penLaranja);
                pntPincel->setBrush(penLaranja.color());
            }

            vDesenhaRobo(r->atbRetornaRobo(), true);
            vDesenhaCirculo(r->getPrediction(), 200, Qt::black, 2);
        }
    }

    const Bola& blBola = acWidgetCampo->blPegaBola();

    if (blBola.bEmCampo)
    {
        pntPincel->setPen(QPen(DrawMap::corBolaEmCampo, 1.0, Qt::SolidLine,
                               Qt::SquareCap, Qt::RoundJoin));
        pntPincel->setBrush(DrawMap::corBolaEmCampo);
    }
    else
    {
        pntPincel->setPen(QPen(DrawMap::corBolaForaDeCampo, 1.0, Qt::SolidLine,
                               Qt::SquareCap, Qt::RoundJoin));
        pntPincel->setBrush(DrawMap::corBolaForaDeCampo);
    }

    // Desennha a bola
    QVector2D vt2dPontoBola = blBola.vt2dPosicaoAtual;

    vConvertePontoRealParaImagem(vt2dPontoBola);

    pntPincel->drawEllipse(vt2dPontoBola.toPoint(),
                           qRound(0.5 * globalConfig.ballDiameter / (fEscalaX)),
                           qRound(0.5 * globalConfig.ballDiameter / (fEscalaY)));

    QColor prevColor = pntPincel->pen().color();
    pntPincel->setPen(Qt::transparent);
    pntPincel->setBrush(QColor(255, 0, 0, 70));
    pntPincel->drawEllipse(vt2dPontoBola.toPoint(),
                           qRound(qSqrt(blBola.covariance.x())*3000 / (fEscalaX)),
                           qRound(qSqrt(blBola.covariance.y())*3000 / (fEscalaY)));
    pntPincel->setPen(prevColor);

    pntPincel->setBrush(Qt::transparent);
    pntPincel->drawEllipse(vt2dPontoBola.toPoint(), qRound(200 / fEscalaX),
                           qRound(200 / fEscalaY));

    // float ballV = blBola.dVelocidade, ballDeceleration = 0.2, targetV = 0.2, tTarget;
    // tTarget = qMax((targetV - ballV) / (-ballDeceleration), 0.0f);
    //
    // QVector2D ballVelocity = blBola.vt2dVelocidade,
    //           ballTarget, deceleration = -ballVelocity.normalized() * ballDeceleration;
    //
    // ballTarget = blBola.vt2dPosicaoAtual/1e3 + ballVelocity * tTarget + 0.5 * deceleration * tTarget * tTarget;
    // ballTarget *= 1e3;

    // ballTarget = blBola.vt2dPosicaoAtual / 1e3;
    //
    // float t = 0, dt = 0.05;
    // while (t < tTarget)
    // {
    //     ballTarget = ballTarget + ballVelocity * dt + 0.5 * deceleration * dt*dt;
    //     t += dt;
    // }
    // ballTarget *= 1e3;

    if (blBola.prediction.hasPrediction)
    {
        QVector2D ballTarget = blBola.prediction.position;
        pntPincel->setPen(QPen(Qt::blue, 2, Qt::SolidLine,
                               Qt::SquareCap, Qt::RoundJoin));
        vConvertePontoRealParaImagem(ballTarget);
        pntPincel->drawEllipse(ballTarget.toPoint(),
                               qRound(2.0 * globalConfig.ballDiameter / (fEscalaX)),
                               qRound(2.0 * globalConfig.ballDiameter / (fEscalaY)));
    }

    // float mass = 40 / 1000.f, kinEnergy = mass * ballV * ballV / 2.f;
    // float stopDist = kinEnergy / (0.03 * mass * 9.81);

    // pntPincel->drawText((vt2dPontoBola + QVector2D(30, 20)).toPoint(),
    //                     QString("t = %1").arg(tTarget, 0, 'g', 2));
    // pntPincel->drawText((vt2dPontoBola + QVector2D(30, 50)).toPoint(),
    //                     QString("v = %1").arg(ballV, 0, 'g', 2));
    // pntPincel->drawText((vt2dPontoBola + QVector2D(30, 80)).toPoint(),
    //                     QString("d = %1").arg(stopDist, 0, 'g', 2));
    //
    // ballTarget = blBola.vt2dPosicaoAtual + ballVelocity.normalized() * stopDist * 1000;
    // vConvertePontoRealParaImagem(ballTarget);
    // pntPincel->drawEllipse(ballTarget.toPoint(),
    //                        qRound(1.0 * globalConfig.ballDiameter / (fEscalaX)),
    //                        qRound(1.0 * globalConfig.ballDiameter / (fEscalaY)));

    if (bDesenharAreaSeguranca)
    {
        pntPincel->drawEllipse(vt2dPontoBola.toPoint(),
                               qRound(globalConfig.safety.ball.normal / (fEscalaX)),
                               qRound(globalConfig.safety.ball.normal / (fEscalaY)));
    }

    pntPincel->setPen(QPen(DrawMap::corLinhasCampo, 2, Qt::SolidLine,
                           Qt::SquareCap, Qt::RoundJoin));
}

void DrawMap::vDesenhaGol(TeamColor _time, int _iLadoCampo)
{
    QPen xPostivo = pntPincel->pen();
    QPen xNegativo = pntPincel->pen();

    if (_time == timeAzul)
    {
        if (_iLadoCampo == XPositivo)
        {
            xPostivo = QPen(DrawMap::corRoboAzul, 4, Qt::SolidLine,
                            Qt::RoundCap, Qt::RoundJoin);
            xNegativo = QPen(DrawMap::corRoboAmarelo, 4, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin);
        }
        else
        {
            xPostivo = QPen(DrawMap::corRoboAmarelo, 4, Qt::SolidLine,
                            Qt::RoundCap, Qt::RoundJoin);
            xNegativo = QPen(DrawMap::corRoboAzul, 4, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin);
        }
    }

    else if (_time == timeAmarelo)
    {
        if (_iLadoCampo == XPositivo)
        {
            xPostivo = QPen(DrawMap::corRoboAmarelo, 4, Qt::SolidLine,
                            Qt::RoundCap, Qt::RoundJoin);
            xNegativo = QPen(DrawMap::corRoboAzul, 4, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin);
        }
        else
        {
            xPostivo = QPen(DrawMap::corRoboAzul, 4, Qt::SolidLine,
                            Qt::RoundCap, Qt::RoundJoin);
            xNegativo = QPen(DrawMap::corRoboAmarelo, 4, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin);
        }
    }

    // Desenha o gol no lado direito, X Positivo
    pntPincel->setPen(xPostivo);

    pntPincel->drawLine(
        (szCampo.width() + globalConfig.fieldOffset) / fEscalaX,
        (szCampo.height() / 2 + iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY,
        (szCampo.width() + globalConfig.fieldOffset + iProfundidadeGol) / fEscalaX,
        (szCampo.height() / 2 + iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->drawLine(
        (szCampo.width() + globalConfig.fieldOffset) / fEscalaX,
        (szCampo.height() / 2 - iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY,
        (szCampo.width() + globalConfig.fieldOffset + iProfundidadeGol) / fEscalaX,
        (szCampo.height() / 2 - iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->drawLine(
        (szCampo.width() + globalConfig.fieldOffset + iProfundidadeGol) / fEscalaX,
        (szCampo.height() / 2 + iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY,
        (szCampo.width() + globalConfig.fieldOffset + iProfundidadeGol) / fEscalaX,
        (szCampo.height() / 2 - iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY);

    // Desenha o gol no lado esquerdo, X Negativo
    pntPincel->setPen(xNegativo);

    pntPincel->drawLine(
        globalConfig.fieldOffset / fEscalaX,
        (szCampo.height() / 2 + iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY,
        (globalConfig.fieldOffset - iProfundidadeGol) / fEscalaX,
        (szCampo.height() / 2 + iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->drawLine(
        globalConfig.fieldOffset / fEscalaX,
        (szCampo.height() / 2 - iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY,
        (globalConfig.fieldOffset - iProfundidadeGol) / fEscalaX,
        (szCampo.height() / 2 - iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->drawLine(
        (globalConfig.fieldOffset - iProfundidadeGol) / fEscalaX,
        (szCampo.height() / 2 + iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY,
        (globalConfig.fieldOffset - iProfundidadeGol) / fEscalaX,
        (szCampo.height() / 2 - iLarguraGol / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->setPen(QPen(DrawMap::corLinhasCampo, dLarguraLinhasBrancas,
                           Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
}

void DrawMap::vDesenhaAreaDefesa()
{
    // Desenha a area do penalty no lado direito
    pntPincel->drawLine(
        (szCampo.width() + globalConfig.fieldOffset) / fEscalaX,
        (szCampo.height() / 2 + iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY,
        (szCampo.width() + globalConfig.fieldOffset - iProfundidadeDefesa) / fEscalaX,
        (szCampo.height() / 2 + iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->drawLine(
        (szCampo.width() + globalConfig.fieldOffset) / fEscalaX,
        (szCampo.height() / 2 - iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY,
        (szCampo.width() + globalConfig.fieldOffset - iProfundidadeDefesa) / fEscalaX,
        (szCampo.height() / 2 - iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->drawLine(
        (szCampo.width() + globalConfig.fieldOffset - iProfundidadeDefesa) / fEscalaX,
        (szCampo.height() / 2 + iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY,
        (szCampo.width() + globalConfig.fieldOffset - iProfundidadeDefesa) / fEscalaX,
        (szCampo.height() / 2 - iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY);

    // Desenha a area do penalty no lado esquerdo
    pntPincel->drawLine(
        globalConfig.fieldOffset / fEscalaX,
        (szCampo.height() / 2 + iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY,
        (globalConfig.fieldOffset + iProfundidadeDefesa) / fEscalaX,
        (szCampo.height() / 2 + iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->drawLine(
        globalConfig.fieldOffset / fEscalaX,
        (szCampo.height() / 2 - iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY,
        (globalConfig.fieldOffset + iProfundidadeDefesa) / fEscalaX,
        (szCampo.height() / 2 - iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY);

    pntPincel->drawLine(
        (globalConfig.fieldOffset + iProfundidadeDefesa) / fEscalaX,
        (szCampo.height() / 2 + iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY,
        (globalConfig.fieldOffset + iProfundidadeDefesa) / fEscalaX,
        (szCampo.height() / 2 - iLarguraDefesa / 2 + globalConfig.fieldOffset) / fEscalaY);
}

void DrawMap::vDesenhaAreaRestricao()
{
    pntPincel->setOpacity(0.5);
    pntPincel->setPen(QPen(QColor(255, 0, 0), dLarguraLinhasBrancas));

    QRectF areaRestricao = acWidgetCampo->geoCampo->retNegativeRestriction();
    vConverteRetanguloRealParaImagem(areaRestricao);
    pntPincel->drawRect(areaRestricao);

    areaRestricao = acWidgetCampo->geoCampo->retPositiveRestriction();
    vConverteRetanguloRealParaImagem(areaRestricao);
    pntPincel->drawRect(areaRestricao);

    pntPincel->setOpacity(1);
    pntPincel->setPen(*pen);
}

void DrawMap::vDesenhaRegioesCampo()
{
    int y1, y2, xRegiao, nRegioes;
    QPoint P1, P2;

    pntPincel->setPen(QPen(DrawMap::corLinhasCampo, dLarguraLinhasBrancas,
                           Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setOpacity(0.3);

    nRegioes = qCeil((szCampo.width()) / globalConfig.fieldDivisionWidth);

    y1 = (szCampo.height() + globalConfig.fieldOffset) / fEscalaY;
    y2 = globalConfig.fieldOffset / fEscalaY;

    P1.setY(y1);
    P2.setY(y2);

    for (int n = 0; n < nRegioes; ++n)
    {
        xRegiao = (globalConfig.fieldOffset + globalConfig.fieldDivisionWidth * n) / fEscalaX;
        P1.setX(xRegiao);
        P2.setX(xRegiao);

        pntPincel->drawLine(P1, P2);
    }

    pntPincel->setOpacity(1);
}

void DrawMap::vDesenhaPosicaoTimeout(QVector2D _vt2dPosicaoTimeout)
{
    vConvertePontoRealParaImagem(_vt2dPosicaoTimeout);

    pntPincel->drawImage(
        QPoint(_vt2dPosicaoTimeout.x() - globalConfig.robotDiameter / (2 * fEscalaX),
               _vt2dPosicaoTimeout.y() - globalConfig.robotDiameter / (2 * fEscalaY)),
        imPosicaoTimeout);
}

void DrawMap::vDesenharPathPlanners()
{
    Robo* robot;
    QVector2D robotPos;
    if (bPathPlanners)
    {
        for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
        {
            robot = acWidgetCampo->allies->getPlayer(n);
            robotPos = robot->vt2dPosicaoAtual();
            if (robot->bRoboDetectado())
            {
                QColor colorAux = robot->path->rgbGetColor();
                vDesenhaCaminhoRobo(robot->path->getPath(), colorAux, 1);

                QVector2D followPoint = robot->path->followPoint(robotPos);
                vDesenhaPonto(followPoint, colorAux, 15);

                if (robot->vt2dDestino().distanceToPoint(robotPos) >
                    globalConfig.robotDiameter / 2.0)
                {
                    vDesenhaCaminhoRobo(robot->path->getFullPath(),
                                        colorAux, 3);
                }
                vDesenhaPonto(
                    robot->vt2dDestino(),
                    colorAux);
            }
        }
    }
}

void DrawMap::vDesenharDirecaoRobo()
{
    if (bDirecaoRobo) // Mostrar direcao de movimentacao do robo
    {
        QVector<QVector2D> direcaoRobo;
        Robo* r;
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
        {
            r = acWidgetCampo->allies->getPlayer(id);
            if (r->bRoboDetectado())
            {
                QVector2D vel;
                const Atributos& atbRobo = r->atbRetornaRobo();
                direcaoRobo.clear();
                direcaoRobo.append(atbRobo.vt2dPosicaoAtual);
                vel = direcaoRobo.first() +
                      atbRobo.vt2dVelocidadeNaoConvertida * 500;

                direcaoRobo.append(vel);
                vDesenhaLinhas(direcaoRobo, Qt::black);

                // Recompõe a velocidade do robô a partir da odometria recebida
                if (atbRobo.dOdometriaRoda1.size() > 0)
                {
                    if (atbRobo.dOdometriaRoda1.last() +
                            atbRobo.dOdometriaRoda2.last() +
                            atbRobo.dOdometriaRoda3.last() +
                            atbRobo.dOdometriaRoda4.last() !=
                        0)
                    {
                        direcaoRobo.clear();
                        direcaoRobo.append(atbRobo.vt2dPosicaoAtual);
                        vel = direcaoRobo.first() +
                              (50 / globalConfig.robotVelocities.linear.limit) *
                                  Auxiliar::vt2dConverteOdometria(
                                      atbRobo.dOdometriaRoda1.last(),
                                      atbRobo.dOdometriaRoda2.last(),
                                      atbRobo.dOdometriaRoda3.last(),
                                      atbRobo.dOdometriaRoda4.last());
                        direcaoRobo.append(vel);

                        vDesenhaLinhas(direcaoRobo, Qt::yellow, 6);
                    }
                }
            }

            r = acWidgetCampo->opponents->getPlayer(id);
            if (r->bRoboDetectado())
            {
                QVector2D vel;
                const Atributos& atbRobo = r->atbRetornaRobo();
                direcaoRobo.clear();
                direcaoRobo.append(atbRobo.vt2dPosicaoAtual);
                vel = direcaoRobo.first() + atbRobo.vt2dVelocidade * 500;

                direcaoRobo.append(vel);
                vDesenhaLinhas(direcaoRobo, Qt::red);
            }
        }
    }
}

void DrawMap::vDesenharPosicoesSimulacao()
{
    if (bPosicoesSimulacao)
    {
        if (acWidgetCampo->vt3dPosicaoSimulacaoRobo.size() >= 200)
        {
            Atributos atbAux;
            float opac = 1,
                  inc = 0.8 / acWidgetCampo->vt3dPosicaoSimulacaoRobo.size();
            QVector<QVector2D> vt2dAux;
            vt2dAux.clear();

            for (int i = 0; i < acWidgetCampo->vt3dPosicaoSimulacaoRobo.size();
                 i += 10)
            {
                if (i == 200)
                    i = 199;
                atbAux.vt2dPosicaoAtual =
                    acWidgetCampo->vt3dPosicaoSimulacaoRobo.at(i).toVector2D();
                atbAux.rotation =
                    acWidgetCampo->vt3dPosicaoSimulacaoRobo.at(i).z();
                atbAux.id = 1;

                if (i == 0)
                {
                    vt2dAux.append(atbAux.vt2dPosicaoAtual);
                    vt2dAux.append(
                        vt2dAux.first() +
                        acWidgetCampo->vt3dVelocidadeSimulacaoRobo.at(i)
                                .toVector2D() *
                            (500));
                    vDesenhaLinha(vt2dAux, Qt::white, 1);
                }

                vDesenhaRobo(atbAux, opac);

                opac -= inc * 15;
                if (opac < 0.1)
                    opac = 0.1;
            }
        }
    }
}

void DrawMap::vDesenharMiraBola()
{
    if (bMiraBola) // Mostra mira da bola para o gol inimigo
    {
        QVector2D posBola = acWidgetCampo->vt2dPosicaoBola();
        QVector<QVector3D> posicoesOponentes;
        posicoesOponentes.clear();
        posicoesOponentes.append(
            acWidgetCampo->vt3dPegaPosicaoTodosObjetos(otOponente));
        QVector<QVector2D> mira;
        mira.clear();
        mira.append(Auxiliar::vt2dIntervaloLivreGol(
            posBola, acWidgetCampo->allies->iGetSide(), iLarguraGol,
            iProfundidadeGol, acWidgetCampo->geoCampo->szField(),
            posicoesOponentes));

        if (!mira.isEmpty()) // Desenha o intervalo livre do gol inimigo
        {
            vDesenhaMiraBola(mira, posBola, Qt::red);
        }

        posicoesOponentes.clear();
        posicoesOponentes.append(
            acWidgetCampo->vt3dPegaPosicaoTodosObjetos(otAliado));
        mira.clear();
        mira.append(Auxiliar::vt2dIntervaloLivreGol(
            posBola, acWidgetCampo->opponents->iGetSide(), iLarguraGol,
            iProfundidadeGol, acWidgetCampo->geoCampo->szField(),
            posicoesOponentes));

        if (!mira.isEmpty()) // Desenha o intervalo livre do gol aliado
        {
            vDesenhaMiraBola(mira, posBola, Qt::blue);
        }
    }
}

void DrawMap::vDesenharLinhasGoleiro()
{
    if (bLinhasGoleiro)
    {
        // Ponto goleiro
        if (acWidgetCampo->blBola->vvt2dGetFilteredPositions().size() > 0)
        {
            QVector<QVector2D> linhas;
            linhas.clear();
            QVector2D pontoGoleiro = Auxiliar::vt2dCalculaDefesaGoleiro(
                acWidgetCampo->blBola->vvt2dGetFilteredPositions(),
                acWidgetCampo->allies->iGetSide(), iLarguraGol,
                acWidgetCampo->geoCampo->szField(), 0);
            linhas.append(pontoGoleiro);
            linhas.append(
                acWidgetCampo->blBola->vvt2dGetFilteredPositions().constLast());
            vDesenhaLinha(linhas, Qt::white, 5);
            linhas.clear();

            // Linha gol
            QVector4D linhaGol;
            linhaGol.setX(acWidgetCampo->allies->iGetSide() *
                          (acWidgetCampo->geoCampo->szField().width() / 2 -
                           0.75 * globalConfig.robotDiameter));
            linhaGol.setY(iLarguraGol / 2);
            linhaGol.setZ(acWidgetCampo->allies->iGetSide() *
                          (acWidgetCampo->geoCampo->szField().width() / 2 -
                           0.75 * globalConfig.robotDiameter));

            linhaGol.setW(-iLarguraGol / 2);
            linhas.append(linhaGol.toVector2D());
            linhas.append(QVector2D(linhaGol.z(), linhaGol.w()));

            vDesenhaLinha(linhas, Qt::blue, 5);
            linhas.clear();

            // Reta media
            QLineF retaMedia = Auxiliar::lineCalculaRetaMedia(
                acWidgetCampo->blBola->vvt2dGetFilteredPositions());
            linhas.append(QVector2D(retaMedia.p1()));
            linhas.append(QVector2D(retaMedia.p2()));
            vDesenhaLinha(linhas, Qt::yellow, 10);

            vDesenhaPontos(acWidgetCampo->blBola->vvt2dGetFilteredPositions(),
                           Qt::red, 3);
        }
    }
}

void DrawMap::vDesenharPredicoesKalman()
{
    if (bPredicoesKalman)
    {
        // desenha predicao da bola kalman
        if (!acWidgetCampo->blBola->vvt2dGetPredictedPositions().isEmpty())
        {
            QLineF aux = Auxiliar::lineCalculaRetaMedia(
                acWidgetCampo->blBola->vvt2dGetPredictedPositions());
            QVector<QVector2D> aux2;
            aux2.clear();
            aux2.append(QVector2D(aux.p1()));
            aux2.append(acWidgetCampo->vt2dPosicaoBola());
            aux2.append(QVector2D(aux.p2()));
            vDesenhaLinhas(aux2, Qt::black); // Pontos da bola depois do Kalman
        }
        vDesenhaPontos(acWidgetCampo->blBola->vvt2dGetPredictedPositions(),
                       Qt::white, 3); // Predição da bola

        // desenha predicao dos robos kalman
        //        for(int n=0; n < globalConfig.robotsPerTeam ;n++)
        //        {
        //            if(!acWidgetCampo->vtPredicaoAdversario[n].isEmpty() &&
        //                    acWidgetCampo->opponents->getPlayer(n)->bRoboDetectado())
        //            {
        //                QVector4D aux =
        //                Auxiliar::vt4dCalculaRetaMedia(acWidgetCampo->vtPredicaoAdversario[n]);
        //                QVector<QVector2D> aux2; aux2.clear();
        //                aux2.append(aux.toVector2D());
        //                aux2.append(acWidgetCampo->opponents->getPlayer(n)->vt2dPosicaoAtual());
        //                aux2.append(QVector2D(aux.z(), aux.w()));
        //                vDesenhaLinhas(aux2, Qt::red); // Pontos da bola
        //                depois do Kalman
        //            }
        //            if(!acWidgetCampo->vtPredicaoAliado[n].isEmpty() &&
        //                    acWidgetCampo->allies->getPlayer(n)->bRoboDetectado()
        //                    )
        //            {
        //                QVector4D aux =
        //                Auxiliar::vt4dCalculaRetaMedia(acWidgetCampo->vtPredicaoAliado[n]);
        //                QVector<QVector2D> aux2; aux2.clear();
        //                aux2.append(aux.toVector2D());
        //                aux2.append(acWidgetCampo->allies->getPlayer(n)->vt2dPosicaoAtual());
        //                aux2.append(QVector2D(aux.z(), aux.w()));
        //                vDesenhaLinhas(aux2, Qt::cyan); // Pontos da bola
        //                depois do Kalman
        //            }
        //        }
    }
}

void DrawMap::vDesenhaPosicaoBallPlacement()
{
    if (bBallPlacement && (acWidgetCampo->sslrefComandoRefereeAtual ==
                               SSL_Referee_Command_BALL_PLACEMENT_BLUE ||
                           acWidgetCampo->sslrefComandoRefereeAtual ==
                               SSL_Referee_Command_BALL_PLACEMENT_YELLOW))
    {
        QVector2D posDesignada = acWidgetCampo->vt2dPosicaoBallPlacement,
                  posBola = acWidgetCampo->vt2dPosicaoBola();

        vDesenhaBallPlacement(posDesignada, posBola);
    }
}

void DrawMap::vDesenhaLiberdadeRobos()
{
    if (bLiberdadeRobos)
    {
        if (dTempoUltimaPrevisao > 3)
            return;

        for (qint8 i = 0; i < globalConfig.robotsPerTeam; i++)
        {
            if (!acWidgetCampo->allies->getPlayer(i)->bRoboDetectado())
                continue;

            const qint16& avaliacaoReceptor = vtiAvaliacoesReceptores.at(i);

            const QColor color =
                (avaliacaoReceptor < 125)
                    ? Auxiliar::corEscalaLinear(Qt::red, Qt::yellow,
                                                avaliacaoReceptor, 0, 125)
                    : Auxiliar::corEscalaLinear(Qt::yellow, Qt::green,
                                                avaliacaoReceptor, 125, 250);

            vDesenhaLiberdadeDoRobo(
                acWidgetCampo->allies->getPlayer(i)->vt2dPosicaoAtual(), color);
        }

        // desenha liberdade do gol
        const QColor color =
            (iAvaliacaoChuteAoGol < 125)
                ? Auxiliar::corEscalaLinear(Qt::red, Qt::yellow,
                                            iAvaliacaoChuteAoGol, 0, 125)
                : Auxiliar::corEscalaLinear(Qt::yellow, Qt::green,
                                            iAvaliacaoChuteAoGol, 125, 250);

        vDesenhaLiberdadeDoRobo(acWidgetCampo->geoCampo->vt2dGoalCenter(
                                    acWidgetCampo->opponents->iGetSide()),
                                color);
    }
}

void DrawMap::vDesenhaRobosMines()
{
    if (bRobosMines)
    {
        for (qint8& n : acWidgetCampo->vtiRobosComJogada(Normal_Atacante_Mines))
            vDesenhaPontoRobosMines(
                acWidgetCampo->allies->getPlayer(n)->vt2dPosicaoAtual());
    }
}

void DrawMap::vDesenhaReceptor()
{
    if (bDestacarReceptor)
    {
        qint8 id = acWidgetCampo->iAchaRoboComJogada(Normal_Receptor);

        if (id != -1)
            vDestacaReceptor(
                acWidgetCampo->allies->getPlayer(id)->vt2dPosicaoAtual(),
                acWidgetCampo->vt2dPosicaoBola());

        if (acWidgetCampo->iAchaRoboComJogada(Normal_Chutador) != -1)
            vDestacaReceptor(Auxiliar::vt2dCentroLivreGolAdversario(
                                 acWidgetCampo->vt2dPosicaoBola(),
                                 acWidgetCampo->allies->iGetSide(), iLarguraGol,
                                 acWidgetCampo->geoCampo->szField(),
                                 acWidgetCampo->vt3dPegaPosicaoTodosObjetos(
                                     otOponente, true)),
                             acWidgetCampo->vt2dPosicaoBola());
    }
}

void DrawMap::vDesenhaVetorVelocidadeBola()
{
    if (bVetorBola)
    {
        vDesenhaVetorDaBola(acWidgetCampo->vt2dPosicaoBola(),
                            acWidgetCampo->vt2dVelocidadeBola(),
                            QPen(QColor(0, 69, 250), 1.5));
    }
}

void DrawMap::vDesenhaTesteMeioCampo()
{
    if (iLadoCampoUtilizado != CampoInteiro)
    {
        vTesteMeioCampo();
    }
}

void DrawMap::vDesenhaVelocidadeBolaMouse()
{
    if (bBotaoDireitoPressionado && bReposicaoMouse)
    {
        QVector2D velocidade =
            (vt2dPosicaoMouse - vt2dPosicaoInicialMouseBotaoDireito) / 500;
        vDesenhaVetorDaBola(vt2dPosicaoInicialMouseBotaoDireito, velocidade,
                            QPen(QColor(0x0, 0x40, 0x0), 2, Qt::DashLine,
                                 Qt::FlatCap, Qt::RoundJoin));
    }
}

void DrawMap::vDesenhaGrafo()
{
    if (bGrafo)
    {
        QVector<QVector2D> conversor;
        foreach (QVector3D pai, vgGrafoDesenho.vt3dPaisObjetos)
        {
            conversor.append(pai.toVector2D());
        }
        vDesenhaGrafo(vgGrafoDesenho.vgMatrizAdjacencia,
                      vgGrafoDesenho.vt2dPontosObjetos, conversor);
        vDesenhaAgrupamentos(vgGrafoDesenho.circAgrupamentos);
    }
}

void DrawMap::vDesenhaTestePathPlanner()
{
    if (bPathPlanners)
    {
        vDesenhaGrafo();
        vDesenhaLinhas(vt2dCaminhoOriginalAStar, QColor(0, 153, 204), 6);
        vDesenhaLinhas(vt2dCaminhoAStar, Qt::red, 2);

        vDesenhaLinhas(vt2dCaminhoRRT, QColor(0, 102, 0), 5);
        vDesenhaArvoreRRT(vt2dPontosArvore, vt2dPaisArvore, Qt::green);
        vDesenhaLinhas(vt2dCaminhoOriginalRRT, QColor(40, 70, 180), 5);
    }
}

void DrawMap::vDesenhaTrajetoAtualMClick()
{
    if (!vt2dTrajetoAtualMClick.isEmpty())
    {
        vDesenhaCaminhoRobo(vt2dTrajetoAtualMClick, QColor(0, 255, 255), 2);
        vDesenhaPontos(vt2dTrajetoAtualMClick, QColor(0, 255, 255), 4);
    }
}

void DrawMap::drawRealTrajectory()
{
    if (drawRobotTrajectory)
    {
        vDesenhaPontos(robotTrajectory, QColor(255, 0, 0, 127), 3);
    }
}

void DrawMap::vConvertePontoRealParaImagem(QVector2D& _vt2dPonto)
{
    // Torna as coordenadas dos robôs positivas
    const QVector2D vetorDeConversao(
        ((szCampo.width() - iEspessuraLinha) / 2) + globalConfig.fieldOffset,
        ((szCampo.height() - iEspessuraLinha) / 2) + globalConfig.fieldOffset);

    _vt2dPonto.setX((_vt2dPonto.x() + vetorDeConversao.x()) / fEscalaX);
    _vt2dPonto.setY((-_vt2dPonto.y() + vetorDeConversao.y()) / fEscalaY);
}

void DrawMap::vConverteCaminhoRealParaImagem(QVector<QVector2D>& _caminho)
{
    // Torna as coordenadas dos robôs positivas
    const QVector2D vetorDeConversao(
        ((szCampo.width() - iEspessuraLinha) / 2) + globalConfig.fieldOffset,
        ((szCampo.height() - iEspessuraLinha) / 2) + globalConfig.fieldOffset);
    for (QVector2D& n : _caminho)
    {
        n.setX((n.x() + vetorDeConversao.x()) / fEscalaX);
        n.setY((-n.y() + vetorDeConversao.y()) / fEscalaY);
    }
}

void DrawMap::vConvertePontoRealParaImagem(QPointF& _ponto)
{
    // Torna as coordenadas dos robôs positivas
    const QVector2D vetorDeConversao(
        ((szCampo.width() - iEspessuraLinha) / 2) + globalConfig.fieldOffset,
        ((szCampo.height() - iEspessuraLinha) / 2) + globalConfig.fieldOffset);

    _ponto.setX((_ponto.x() + vetorDeConversao.x()) / fEscalaX);
    _ponto.setY((-_ponto.y() + vetorDeConversao.y()) / fEscalaY);
}

void DrawMap::vConverteRetanguloRealParaImagem(QRectF& _retangulo)
{
    QVector2D cantoCima =
                  QVector2D(_retangulo.topLeft().x(), _retangulo.topLeft().y()),
              cantoBaixo = QVector2D(_retangulo.bottomRight().x(),
                                     _retangulo.bottomRight().y());

    vConvertePontoRealParaImagem(cantoCima);
    vConvertePontoRealParaImagem(cantoBaixo);

    _retangulo.setWidth(_retangulo.width() / fEscalaX);
    _retangulo.setHeight(_retangulo.height() / fEscalaY);
    _retangulo.setTopLeft(cantoCima.toPoint());
    _retangulo.setBottomRight(cantoBaixo.toPoint());
}

void DrawMap::vDesenhaPontos(QVector<QVector2D> _pontos, const QColor& _cor,
                             const double _dTamanhoPonto)
{

    vConverteCaminhoRealParaImagem(_pontos);
    pntPincel->setPen(
        QPen(_cor, _dTamanhoPonto, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    for (int n = 0; n < _pontos.size(); ++n)
    {
        pntPincel->drawPoint(_pontos[n].toPointF());
    }
}

void DrawMap::drawPath(QVector<QVector2D> _points, const QColor& _color,
                       const double _size)
{
    if (_points.isEmpty())
    {
        return;
    }
    vConverteCaminhoRealParaImagem(_points);
    pntPincel->setPen(
        QPen(_color, _size, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    QPainterPath path;

    path.moveTo(_points.constFirst().toPointF());
    for (int n = 1; n < _points.size(); ++n)
    {
        path.lineTo(_points.at(n).toPointF());
    }
    pntPincel->drawPath(path);
}

void DrawMap::vDesenhaLinhas(QVector<QVector2D> _pontos, const QColor& _cor)
{
    vConverteCaminhoRealParaImagem(_pontos);

    for (int n = 0; n < _pontos.size() - 1; ++n)
    {
        pntPincel->setPen(
            QPen(_cor, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

        pntPincel->drawLine(_pontos[n].toPointF(), _pontos[n + 1].toPointF());

        pntPincel->setPen(
            QPen(_cor, 7, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        pntPincel->drawPoint(_pontos[n].toPointF());

        pntPincel->setPen(
            QPen(Qt::black, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    }
}

void DrawMap::vDesenhaLinhas(QVector<QVector2D> _pontos, const QColor& _cor,
                             int _iEspessuraLinha)
{
    vConverteCaminhoRealParaImagem(_pontos);

    for (int n = 0; n < (int)_pontos.size() - 1; ++n)
    {
        pntPincel->setPen(QPen(_cor, _iEspessuraLinha, Qt::SolidLine,
                               Qt::RoundCap, Qt::RoundJoin));

        pntPincel->drawLine(_pontos[n].toPointF(), _pontos[n + 1].toPointF());
    }
}

void DrawMap::vDrawPolygon(QVector<QVector2D> _vertices, const QColor& _color,
                           int _lineWidth)
{
    vConverteCaminhoRealParaImagem(_vertices);
    pntPincel->setPen(
        QPen(_color, _lineWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    pntPincel->setBrush(_color);
    QPolygonF polygon;

    for (const QVector2D& _point : qAsConst(_vertices))
    {
        polygon << _point.toPointF();
    }

    pntPincel->drawConvexPolygon(polygon);
    pntPincel->setBrush(Qt::transparent);
}

void DrawMap::vDrawPolygon(QList<QPointF> _vertices, const QColor& _color,
                           int _lineWidth)
{
    QVector<QVector2D> vertices;
    for (const QPointF& _point : qAsConst(_vertices))
    {
        vertices.append(QVector2D(_point));
    }
    vDrawPolygon(vertices, _color, _lineWidth);
}

void DrawMap::vDesenhaCaminhoRobo(QVector<QVector2D> _caminho,
                                  const QColor& _clrCorCaminho,
                                  int _iEspessuraLinha)
{
    vConverteCaminhoRealParaImagem(_caminho);

    for (int n = 0; n < _caminho.size() - 1; ++n)
    {
        pntPincel->setPen(QPen(_clrCorCaminho, _iEspessuraLinha, Qt::SolidLine,
                               Qt::RoundCap, Qt::RoundJoin));
        pntPincel->setBrush(_clrCorCaminho);

        pntPincel->drawLine(_caminho[n].toPointF(), _caminho[n + 1].toPointF());

        // Desenhas seta indicando o sentido do trajeto a cada 5 pontos
        int anguloSeta = 15, comprimentoSeta = 10;
        if (n > 0 && n % 10 == 0)
        {
            QPainterPath path;
            QVector2D pSeta1 = Auxiliar::vt2dReajustaPonto(
                _caminho[n],
                Auxiliar::vt2dRotaciona(_caminho[n], _caminho[n - 1],
                                        anguloSeta),
                comprimentoSeta);
            QVector2D pSeta2 = Auxiliar::vt2dReajustaPonto(
                _caminho[n],
                Auxiliar::vt2dRotaciona(_caminho[n], _caminho[n - 1],
                                        -anguloSeta),
                comprimentoSeta);
            QVector2D ponta = _caminho[n];

            path.moveTo(pSeta1.toPointF());
            path.lineTo(ponta.toPointF());
            path.lineTo(pSeta2.toPointF());
            path.lineTo(pSeta1.toPointF());

            pntPincel->drawPath(path);
        }
    }
    pntPincel->setBrush(Qt::transparent);
}

void DrawMap::vDesenhaLinha(QVector<QVector2D> _pontos, const QColor& _cor,
                            int _iEspessuraLinha)
{
    vConverteCaminhoRealParaImagem(_pontos);

    pntPincel->setPen(QPen(_cor, _iEspessuraLinha, Qt::SolidLine, Qt::RoundCap,
                           Qt::RoundJoin));
    for (int n = 0; n < _pontos.size() - 1; ++n)
        pntPincel->drawLine(_pontos[n].toPointF(), _pontos[n + 1].toPointF());
}

void DrawMap::vDesenhaPonto(QVector2D _vt2dPonto, const QColor& _cor,
                            const double _size)
{
    vConvertePontoRealParaImagem(_vt2dPonto);

    pntPincel->setPen(
        QPen(_cor, _size, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    pntPincel->drawPoint(_vt2dPonto.toPointF());
}

void DrawMap::vDesenhaCirculo(QVector2D _vt2dCentro, int _iRaio,
                              const QColor& _cor, float _fEspessuraLinha)
{
    vConvertePontoRealParaImagem(_vt2dCentro);

    pntPincel->setPen(QPen(_cor, _fEspessuraLinha));
    pntPincel->drawEllipse(_vt2dCentro.toPointF(), qRound(_iRaio / fEscalaX),
                           qRound(_iRaio / fEscalaY));
}

void DrawMap::vDesenhaArvoreRRT(QVector<QVector2D> _vt2dPontos,
                                QVector<QVector2D> _vt2dParentes,
                                const QColor& _cor)
{

    vConverteCaminhoRealParaImagem(_vt2dPontos);
    vConverteCaminhoRealParaImagem(_vt2dParentes);

    pntPincel->setPen(QPen(_cor, 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    for (int n = 0; n < _vt2dPontos.size(); ++n)
    {
        pntPincel->drawLine(_vt2dPontos.at(n).toPointF(),
                            _vt2dParentes.at(n).toPointF());
        //            paint.drawPoint(_vrArvore[n].vt2dPosicao.x(),
        //            (-1)*_vrArvore[n].vt2dPosicao.y());
    }
}

void DrawMap::vDesenhaMiraRobo(QVector2D _vt2dPosicaoRobo,
                               QVector2D _vt2dPontoAnguloDestino,
                               const QColor& _cor)
{
    QVector2D vt2dP2 = Auxiliar::vt2dReajustaPonto(
        _vt2dPosicaoRobo, _vt2dPontoAnguloDestino, 150);
    QVector2D vt2dP3 =
        Auxiliar::vt2dReajustaPonto(vt2dP2, _vt2dPosicaoRobo, 50);
    QVector2D vt2dPa, vt2dPb;

    const int arrowAngle = 25;
    vt2dPa = Auxiliar::vt2dRotaciona(vt2dP2, vt2dP3, arrowAngle);
    vt2dPb = Auxiliar::vt2dRotaciona(vt2dP2, vt2dP3, -arrowAngle);

    vConvertePontoRealParaImagem(_vt2dPosicaoRobo);
    vConvertePontoRealParaImagem(vt2dP2);
    vConvertePontoRealParaImagem(vt2dP3);
    vConvertePontoRealParaImagem(vt2dPa);
    vConvertePontoRealParaImagem(vt2dPb);

    pntPincel->setPen(QPen(_cor, 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    pntPincel->drawLine(_vt2dPosicaoRobo.x(), _vt2dPosicaoRobo.y(), vt2dP2.x(),
                        vt2dP2.y());

    pntPincel->drawLine(vt2dPa.x(), vt2dPa.y(), vt2dP2.x(), vt2dP2.y());

    pntPincel->drawLine(vt2dPb.x(), vt2dPb.y(), vt2dP2.x(), vt2dP2.y());
}

void DrawMap::vTesteMeioCampo()
{
    pntPincel->setOpacity(0.7);

    double dX1 = 0, dX2 = 0;

    if (iLadoCampoUtilizado == XPositivo)
    {
        dX1 = 0;
        dX2 = qRound((szCampo.width() / 2 + globalConfig.fieldOffset) / fEscalaX);
    }
    else if (iLadoCampoUtilizado == XNegativo)
    {
        dX1 = qRound((szCampo.width() / 2 + globalConfig.fieldOffset) / fEscalaX);
        dX2 = qRound((szCampo.width() + globalConfig.fieldOffset) / fEscalaX);
    }
    QRect retanguloCampo(
        dX1, 0, dX2, qRound((szCampo.height() + 2 * globalConfig.fieldOffset) / fEscalaY));

    pntPincel->fillRect(retanguloCampo, Qt::darkGray);
}

void DrawMap::vDesenhaMiraBola(QVector<QVector2D> _vt2dMiraBola,
                               QVector2D _vt2dPosicaoBola, const QColor& _cor)
{
    pntPincel->setPen(QPen(_cor, 3, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setBrush(_cor);
    pntPincel->setOpacity(0.25);

    QVector2D P1 = _vt2dMiraBola.first(), P2 = _vt2dMiraBola.last();

    vConvertePontoRealParaImagem(_vt2dPosicaoBola);
    vConvertePontoRealParaImagem(P1);
    vConvertePontoRealParaImagem(P2);

    QPainterPath path;
    path.moveTo(P1.toPointF());
    path.lineTo(P2.toPointF());
    path.lineTo(_vt2dPosicaoBola.toPointF());
    path.lineTo(P1.toPointF());

    pntPincel->drawPath(path);

    pntPincel->setBrush(Qt::transparent);
    pntPincel->setOpacity(1);
}

void DrawMap::vDesenhaGrafo(
    std::vector<std::vector<ArestaVG>> vgMatrizAdjacencia,
    QVector<QVector2D> _pts, QVector<QVector2D> _pais)
{

    vConverteCaminhoRealParaImagem(_pts);
    vConverteCaminhoRealParaImagem(_pais);

    if (_pts.size() == static_cast<int>(vgMatrizAdjacencia.size()))
    {
        for (uint i = 0; i < vgMatrizAdjacencia.size(); ++i)
        {
            for (uint j = i + 1; j < vgMatrizAdjacencia.size(); ++j)
            {
                if (vgMatrizAdjacencia[i][j].arestaConectada == true)
                {
                    if (_pais.at(i).distanceToPoint(_pais.at(j)) > 50)
                        pntPincel->setPen(QPen(QColor(80, 0, 180), 1.5,
                                               Qt::SolidLine, Qt::RoundCap,
                                               Qt::RoundJoin));
                    else
                        pntPincel->setPen(QPen(QColor(0, 0, 0), 1.5,
                                               Qt::SolidLine, Qt::RoundCap,
                                               Qt::RoundJoin));

                    pntPincel->drawLine(_pts.at(i).toPointF(),
                                        _pts.at(j).toPointF());
                }
            }
            //        pntPincel->setPen(QPen(QColor( 180, 0, 90), 5,
            //        Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
            //        pntPincel->drawPoint(_pais.at(i).x(), _pais.at(i).y());
            //        pntPincel->setPen(QPen(QColor( 120, 50, 150), 8,
            //        Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
            //        pntPincel->drawPoint(_pts.at(i).toPointF());
        }
    }
}

void DrawMap::vDesenhaAgrupamentos(
    const QVector<QPair<QVector2D, float>>& circulos)
{
    QVector2D centro;
    pntPincel->setPen(QPen(QColor(255, 51, 0), 1.5));
    for (QPair<QVector2D, float> circ : circulos)
    {
        centro = circ.first;
        vConvertePontoRealParaImagem(centro);

        pntPincel->drawEllipse(centro.toPointF(), circ.second / fEscalaX,
                               circ.second / fEscalaY);
    }
}

void DrawMap::vMostrarFPS(float FPS_UI, float FPS_Visao, float FPS_Estrategia,
                          float FPS_Mov)
{
    float razaoUI = FPS_UI / (1.0e3 / iTempoLoopInterface),
          razaoVisao = FPS_Visao / (1.0e3 / iTempoLoopKalman),
          razaoEstrategia = FPS_Estrategia / (1.0e3 / iTempoLoopEstrategia),
          razaoMov = FPS_Mov / (1.0e3 / globalConfig.samplingTime.motion);

    const QColor corUI = Auxiliar::corEscalaLinear(
                     DrawMap::corFpsBaixo, DrawMap::corFpsAlto, razaoUI),
                 corVisao = Auxiliar::corEscalaLinear(
                     DrawMap::corFpsBaixo, DrawMap::corFpsAlto, razaoVisao),
                 corEstrategia = Auxiliar::corEscalaLinear(DrawMap::corFpsBaixo,
                                                           DrawMap::corFpsAlto,
                                                           razaoEstrategia),
                 corMov = Auxiliar::corEscalaLinear(
                     DrawMap::corFpsBaixo, DrawMap::corFpsAlto, razaoMov);

    QVector2D aux = QVector2D(-szCampo.width() / 2 - globalConfig.fieldOffset + 50,
                              szCampo.height() / 2 + globalConfig.fieldOffset / 2);
    vConvertePontoRealParaImagem(aux);

    pntPincel->setPen(
        QPen(corUI, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    pntPincel->drawText(aux.x(), aux.y(), QString::number(FPS_UI) + " Hz(UI) ");

    pntPincel->setPen(
        QPen(corVisao, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    pntPincel->drawText(aux.x() + 100, aux.y(),
                        QString::number(FPS_Visao) + " Hz(Kalman) ");

    pntPincel->setPen(
        QPen(corEstrategia, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    pntPincel->drawText(aux.x() + 250, aux.y(),
                        QString::number(FPS_Estrategia) + " Hz(Estrategia) ");

    pntPincel->setPen(
        QPen(corMov, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    pntPincel->drawText(aux.x() + 410, aux.y(),
                        QString::number(FPS_Mov) + " Hz(Controle)");
}

void DrawMap::vDesenhaRobo(const Atributos& atbRobo, bool timeRobo,
                           float opacidade)
{
    int iRaio = globalConfig.safety.opponent;
    if (timeRobo == true) // Se o time for aliado
        iRaio = globalConfig.safety.ally;

    QVector2D vt2dPontoRobo = atbRobo.vt2dPosicaoAtual;
    vConvertePontoRealParaImagem(vt2dPontoRobo);

    if (vt2dPontoRobo.length() <= 32767)
    {
        pntPincel->setOpacity(opacidade);
        // Desenha o circulo do robô
        QRectF rectRobo;
        rectRobo.setTopLeft(
            QPointF(vt2dPontoRobo.x() - globalConfig.robotDiameter * 0.5 / fEscalaX,
                    vt2dPontoRobo.y() - globalConfig.robotDiameter * 0.5 / fEscalaY));
        rectRobo.setWidth(globalConfig.robotDiameter / fEscalaX);
        rectRobo.setHeight(globalConfig.robotDiameter / fEscalaY);

        float teta = qAcos(1.0 - 336.0 / 289.0); // Angulo da frente do robô
        float angRobo = atbRobo.rotation;
        Auxiliar::vConverteAnguloRobo(angRobo);

        pntPincel->drawChord(rectRobo,
                             ((angRobo + teta / 2) * (180 / M_PI)) * 16,
                             (360 - teta * 180 / M_PI) * 16);

        pntPincel->setBrush(Qt::transparent);

        if (bDesenharAreaSeguranca)
            pntPincel->drawEllipse(vt2dPontoRobo.toPoint(),
                                   qRound(iRaio / (fEscalaX)),
                                   qRound(iRaio / (fEscalaY)));

        if (timeRobo)
        {
            QVector2D anglePoint = atbRobo.vt2dPontoAnguloDestino;
            vDrawX(anglePoint, pntPincel->pen().color(), 80);
        }

        // Desenha o ID do robô
        vt2dPontoRobo =
            vt2dPontoRobo - QVector2D(40 / fEscalaX, -70 / fEscalaY);
        pntPincel->setPen(QPen(DrawMap::corIdRobo, 5.0, Qt::SolidLine,
                               Qt::SquareCap, Qt::RoundJoin));
        QFont font("Arial", 13);
        font.setBold(true);
        pntPincel->setFont(font);
        pntPincel->drawText(vt2dPontoRobo.toPoint(),
                            QString("%1").arg(atbRobo.id));
    }
}

void DrawMap::vDesenhaRobo(Atributos atbRobo, float opacidade)
{
    if (opacidade < 1.0)
    {
        pntPincel->setPen(
            QPen(Qt::cyan, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
        pntPincel->setBrush(Qt::blue);
    }
    else
    {
        pntPincel->setPen(
            QPen(Qt::red, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
        pntPincel->setBrush(Qt::darkRed);
    }
    pntPincel->setOpacity(opacidade);

    QVector2D vt2dPontoRobo = atbRobo.vt2dPosicaoAtual;
    vConvertePontoRealParaImagem(vt2dPontoRobo);

    pntPincel->setOpacity(opacidade);
    // Desenha o circulo do robô
    pntPincel->drawEllipse(vt2dPontoRobo.toPointF(),
                           static_cast<int>(globalConfig.robotDiameter * 0.5 / fEscalaX),
                           static_cast<int>(globalConfig.robotDiameter * 0.5 / fEscalaY));

    // Desenha ângulo do robô
    QVector2D aux =
        atbRobo.vt2dPosicaoAtual +
        QVector2D(150 * qCos(atbRobo.rotation), 150 * qSin(atbRobo.rotation));
    vConvertePontoRealParaImagem(aux);
    pntPincel->setOpacity(1.0);

    QVector<QVector2D> vt2dAnguloRobo;
    vt2dAnguloRobo.clear();
    vt2dAnguloRobo.append(vt2dPontoRobo);
    vt2dAnguloRobo.append(aux);
    pntPincel->drawLine(vt2dAnguloRobo.constFirst().toPoint(),
                        vt2dAnguloRobo.constLast().toPoint());
}

void DrawMap::vDrawZone(const FieldZone& _zone, const QColor& _color)
{
    if (_zone.bIsConfigured())
    {
        pntPincel->setPen(
            QPen(_color, 3, Qt::DashLine, Qt::SquareCap, Qt::RoundJoin));

        if (_zone.ztZoneType() == ZONE_TYPE::CIRCLE)
        {
            vDesenhaCirculo(QVector2D(_zone.circleGetZone().getCenter()),
                            _zone.circleGetZone().getRadius(), _color, 3);
        }
        else if (_zone.ztZoneType() == ZONE_TYPE::RECTANGLE)
        {
            QVector2D topLeft = QVector2D(_zone.rctGetZone().topLeft());
            QSizeF size =
                QSizeF(_zone.rctGetZone().size().width() / fEscalaX,
                       -_zone.rctGetZone().size().height() / fEscalaY);

            vConvertePontoRealParaImagem(topLeft);
            pntPincel->drawRect(QRectF(topLeft.toPointF(), size));
        }
    }
}

void DrawMap::vDrawRoller(const QPolygonF& _rollerPoly)
{
    pntPincel->setOpacity(0.5);
    vDrawPolygon(_rollerPoly.toList(), Qt::green, 1);
    pntPincel->setOpacity(1);
}

void DrawMap::vDesenhaBallPlacement(QVector2D posBallPlacement,
                                    QVector2D posBolaAtual)
{
    float fRaio = 150; // 150 mm

    pntPincel->setOpacity(.9);

    if (posBolaAtual.distanceToPoint(posBallPlacement) < 150)
        pntPincel->setPen(QPen(QColor(Qt::green), 2, Qt::SolidLine, Qt::FlatCap,
                               Qt::RoundJoin));

    else
        pntPincel->setPen(QPen(QColor(Qt::red), 2, Qt::SolidLine, Qt::FlatCap,
                               Qt::RoundJoin));

    vConvertePontoRealParaImagem(posBallPlacement);
    vConvertePontoRealParaImagem(posBolaAtual);

    pntPincel->drawEllipse(posBallPlacement.toPointF(),
                           static_cast<int>(fRaio / fEscalaX),
                           static_cast<int>(fRaio / fEscalaY));

    pntPincel->setPen(
        QPen(QColor(Qt::black), 2, Qt::DashLine, Qt::FlatCap, Qt::RoundJoin));

    pntPincel->drawLine(posBallPlacement.toPointF(), posBolaAtual.toPointF());
    pntPincel->setOpacity(1);
}

void DrawMap::vDesenhaLiberdadeDoRobo(QVector2D posRobo, const QColor& color)
{

    pntPincel->setPen(
        QPen(color, 1, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setBrush(color);

    vConvertePontoRealParaImagem(posRobo);

    pntPincel->drawEllipse(posRobo.toPointF(),
                           static_cast<int>(globalConfig.robotDiameter / 4 / fEscalaX),
                           static_cast<int>(globalConfig.robotDiameter / 4 / fEscalaY));
}

void DrawMap::vDesenhaPontoRobosMines(QVector2D posRobo)
{
    vConvertePontoRealParaImagem(posRobo);

    pntPincel->setPen(
        QPen(Qt::white, 1, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setBrush(Qt::white);

    pntPincel->drawEllipse(posRobo.toPointF(), static_cast<int>(20 / fEscalaX),
                           static_cast<int>(20 / fEscalaY));
}

void DrawMap::vDrawX(QVector2D posPonto, const QColor color,
                                    const float distanciaDiagonal)
{
    vConvertePontoRealParaImagem(posPonto);

    pntPincel->setPen(
        QPen(color, 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    pntPincel->drawLine(
        QPointF(posPonto.x() - (distanciaDiagonal * sqrt(2) / 2) / fEscalaX,
                posPonto.y() - (distanciaDiagonal * sqrt(2) / 2) / fEscalaY),
        QPointF(posPonto.x() + (distanciaDiagonal * sqrt(2) / 2) / fEscalaX,
                posPonto.y() + (distanciaDiagonal * sqrt(2) / 2) / fEscalaY));

    pntPincel->drawLine(
        QPointF(posPonto.x() + (distanciaDiagonal * sqrt(2) / 2) / fEscalaX,
                posPonto.y() - (distanciaDiagonal * sqrt(2) / 2) / fEscalaY),
        QPointF(posPonto.x() - (distanciaDiagonal * sqrt(2) / 2) / fEscalaX,
                posPonto.y() + (distanciaDiagonal * sqrt(2) / 2) / fEscalaY));
}

void DrawMap::vDestacaReceptor(QVector2D posReceptor, QVector2D posBola)
{
    vDrawX(posReceptor, Qt::red, globalConfig.robotDiameter);

    vConvertePontoRealParaImagem(posBola);
    vConvertePontoRealParaImagem(posReceptor);

    pntPincel->setPen(
        QPen(QColor(Qt::red), 2, Qt::DashLine, Qt::FlatCap, Qt::RoundJoin));

    pntPincel->drawLine(posBola.toPointF(), posReceptor.toPointF());
}

void DrawMap::vDesenhaVetorDaBola(QVector2D posBola,
                                  QVector2D vt2dVelocidadeBola, QPen pen)
{
    if (vt2dVelocidadeBola.length() >
        fVelocidadeMaximaBola) // 6 é a velocidade máxima permitida
        pen.setColor(QColor(0xff, 0x0, 0x0));

    QVector2D posFinal = posBola + vt2dVelocidadeBola * 500;

    QVector2D posPontaA = Auxiliar::vt2dRotaciona(
        posFinal, posBola + vt2dVelocidadeBola * 300, 30);

    QVector2D posPontaB = Auxiliar::vt2dRotaciona(
        posFinal, posBola + vt2dVelocidadeBola * 300, -30);

    vConvertePontoRealParaImagem(posBola);
    vConvertePontoRealParaImagem(posFinal);
    vConvertePontoRealParaImagem(posPontaA);
    vConvertePontoRealParaImagem(posPontaB);

    pntPincel->setPen(pen);

    pntPincel->drawLine(posBola.toPointF(), posFinal.toPointF());
    pntPincel->drawLine(posFinal.toPointF(), posPontaA.toPointF());
    pntPincel->drawLine(posFinal.toPointF(), posPontaB.toPointF());
}

void DrawMap::drawRobotConstraint(Eigen::MatrixXd& _constraint, QVector2D _pos)
{
    if (_constraint.rows() < 2)
    {
        return;
    }
    double minVal = _constraint.minCoeff(), maxVal = 0;
    int constraintSize = 500;
    float xShift = -constraintSize / 2, yShift = -constraintSize / 2;
    pntPincel->setOpacity(0.5);
    quint8 red, green;

    for (qint8 row = _constraint.rows() - 1; row >= 0;
         row--, xShift += constraintSize / _constraint.rows())
    {
        yShift = -constraintSize / 2;
        for (qint8 col = _constraint.cols() - 1; col >= 0;
             col--, yShift += constraintSize / _constraint.cols())
        {
            red = 0;
            double value = _constraint(row, col);
            if (value < 0)
            {
                red = qMin(255.0, qAbs(value / minVal) * 255);
            }

            //            red = 255 - (value + abs(minVal)) /
            //                                   (maxVal - minVal + 1e-10) *
            //                                   255;
            //            if (minVal > 0)
            //            {
            //                red = 0;
            //            }
            //            if (maxVal - minVal < 1e-3)
            //            {
            //                red = 0;
            //            }
            green = qMin(255 - red, 255);
            QPen pen;
            pen.setColor(QColor(red, green, 0));
            pntPincel->setBrush(QColor(red, green, 0));
            pntPincel->setPen(pen);

            QVector2D constraintPos = _pos + QVector2D(xShift, yShift);
            vConvertePontoRealParaImagem(constraintPos);

            // pntPincel->drawRect(constraintPos.x(), constraintPos.y(),
            //                     constraintSize / _constraint.rows() /
            //                     fEscalaX, constraintSize / _constraint.cols()
            //                     / fEscalaY);
            pntPincel->drawEllipse(constraintPos.x(), constraintPos.y(),
                                   40 / fEscalaX, 40 / fEscalaY);
        }
    }
    pntPincel->setOpacity(1);
}

void DrawMap::vEscrevePosicaoMouse()
{
    // QVector2D coordenadasGenericas =
    //     Auxiliar::vt2dCoordenadasCampoParaCoordenadasGenericas(
    //         vt2dPosicaoMouse, acWidgetCampo->allies->iGetSide(), szCampo,
    //         QSize(iProfundidadeDefesa, iLarguraDefesa));

    QString
        texto = //"( " + QString::number(coordenadasGenericas.x(), 'f', 3) +
                //" , " + QString::number(coordenadasGenericas.y(), 'f', 3) +
                // " )" + "   |||||||||||||||||||||||||   " +
        "( " + QString::number((int)vt2dPosicaoMouse.x()) + " , " +
        QString::number((int)vt2dPosicaoMouse.y()) + " )";

    QVector2D posicaoDoPonto =
        QVector2D(0, -szCampo.height() / 2 - .5 * globalConfig.fieldOffset);

    vConvertePontoRealParaImagem(posicaoDoPonto);

    pntPincel->setPen(QPen(DrawMap::corLinhasCampo));

    QRectF rect = QRect();
    rect.setSize(QSize(szCampo.width() / fEscalaX, globalConfig.fieldOffset / fEscalaY));
    rect.moveCenter(posicaoDoPonto.toPointF());

    //    QFont fonte = qApp->font();
    //    fonte.setPointSizeF(qMin(qreal(rect.height()),
    //    qApp->font().pointSizeF()));

    //    pntPincel->setFont(fonte);

    pntPincel->drawText(rect, Qt::AlignCenter, texto);
}

void DrawMap::vDesenhaIndicacaoChute(const QVector2D& pos, const QColor& _cor)
{
    QVector2D posRobo = pos, p1, p2;
    p2 = p1 = posRobo;
    p1.setY(p1.y() + globalConfig.robotDiameter / 2.f);
    p2.setY(p1.y() + globalConfig.robotDiameter / 2.f);

    vConvertePontoRealParaImagem(posRobo);
    vConvertePontoRealParaImagem(p1);
    vConvertePontoRealParaImagem(p2);

    pntPincel->setPen(QPen(_cor, 3, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    for (int k = 0; k < 360; k += 30)
    {
        p1 = Auxiliar::vt2dRotaciona(posRobo, p1, 30);
        p2 = Auxiliar::vt2dRotaciona(posRobo, p2, 30);
        pntPincel->drawLine(p1.toPointF(), p2.toPointF());
    }
}

void DrawMap::vDesenhaContornoRobo(const QVector2D& pos, float anguloRobo,
                                   const QColor& _cor)
{
    QVector2D posRobo = pos;
    vConvertePontoRealParaImagem(posRobo);
    QRectF rectRobo;

    rectRobo.setTopLeft(QPointF(posRobo.x() - globalConfig.robotDiameter * 0.5 / fEscalaX,
                                posRobo.y() - globalConfig.robotDiameter * 0.5 / fEscalaY));
    rectRobo.setWidth(globalConfig.robotDiameter / fEscalaX);
    rectRobo.setHeight(globalConfig.robotDiameter / fEscalaY);

    pntPincel->setPen(QPen(_cor, 3, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    float teta = qAcos(1.0 - 336.0 / 289.0); // Angulo da frente do robô
    Auxiliar::vConverteAnguloRobo(anguloRobo);

    pntPincel->drawChord(rectRobo,
                         ((anguloRobo + teta / 2) * (180 / M_PI)) * 16,
                         (360 - teta * 180 / M_PI) * 16);
}

void DrawMap::drawDebugData()
{
    auto dataList = acWidgetCampo->debugInfo->getDebugData();

    const QVector2D conversion(
        ((szCampo.width() - iEspessuraLinha) / 2) + globalConfig.fieldOffset,
        ((szCampo.height() - iEspessuraLinha) / 2) + globalConfig.fieldOffset);

    for (const auto& data : qAsConst(dataList))
    {
        if (data->getType() < DebugData::DEBUG_TYPE_SIZE)
        {
            data->draw(conversion, fEscalaX, fEscalaY, *pntPincel);
            data->decreaseLifespan();
        }
    }
}

void DrawMap::vSetDestacaRegiaoRoles(bool newBDestacaRegiaoRoles)
{
    bDestacaRegiaoRoles = newBDestacaRegiaoRoles;
}
