/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DRAWMAP_H
#define DRAWMAP_H

///
/// \file drawmap.h
/// \brief Desenho da imagem do campo \a DrawMap
///

#include <QPainter>
#include <QPen>
#include <QPixmap>
#include <QtMath>
#include <QPainterPath>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QWidget>
#include <QOpenGLWidget>
#include <QScreen>
#include <QTimer>

#include <utility>

#include "Path_Planning/path_p_rrt_2.h"
#include "Mapa/visibilitygraph.h"
#include "Debug/debugtypes.h"

const double dLarguraLinhasBrancas = 2.5; /**< Largura das linhas brancas */

/**
 * @brief Classe responsável por desenhar qualquer coisa na imagem do campo.
 *
 */
class DrawMap : public QOpenGLWidget
{
    Q_OBJECT


public slots:
    /**
     * @brief Slot que recebe os sinais do QTimer de atualização da interface.
     * (RoboFeiSSL::tmrAtualizacaoDesenhos).
     */
    void vAnimar();

    /**
     * @brief Recebe os dados do teste dos path-planners para fazer o desenho.
     * @param grafo - Grafo (VisibilityGraph) utilizado pelo A_StarVG.
     * @param caminhoAStar - Caminho gerado pelo A_StarVG e devidamente tratado.
     * @param caminhoAStarOriginal - Caminho gerado pelo A_StarVG sem nenhum tratamento.
     * @param pontosArvore - Pontos da árvore gerada pelo RRT.
     * @param paisArvore - Pais dos pontos da árvore gerada pelo RRT.
     * @param caminhoRRT - Caminho gerado pelo RRT e devidamente tratado.
     * @param caminhoRRTOriginal - Caminho gerado pelo RRT sem nenhum tratamento.
     */
    void vRecebeTrajetosTestePathPlanner(const VisibilityGraph *grafo,
                                         const QVector<QVector2D> &caminhoAStar,
                                         const QVector<QVector2D> &caminhoAStarOriginal,
                                         const QVector<QVector2D> &pontosArvore,
                                         const QVector<QVector2D> &paisArvore,
                                         const QVector<QVector2D> &caminhoRRT,
                                         const QVector<QVector2D> &caminhoRRTOriginal);

    /**
     * @brief Recebe o trajeto atual da MovimentacaoClick.
     * @param trajeto
     */
    void vRecebeTrajetoAtualMClick(const QVector<QVector2D> &trajeto);

signals:
    /**
     * @brief Sinal de clique do mouse no widget de desenho.
     * @param pos - Posição do mouse já convertida para a escala do campo [mm].
     * @param button - Botão do mouse pressionado.
     */
    void clicked(const QVector2D pos, const Qt::MouseButton button);

    /**
     * @brief Sinal de movimento do mouse no widget de desenho.
     * @param pos - Posição do mouse já convertida para a escala do campo [mm].
     */
    void moved(const QVector2D pos);

    /**
     * @brief Sinal emitido ao soltar o clique do mouse no widget de desenho.
     * @param pos - Posição do mouse já convertida para a escala do campo [mm].
     * @param button - Botão do mouse pressionado.
     */
    void released(const QVector2D pos, const Qt::MouseButton button);
protected:
    /**
     * @brief Evento para pintura do widget.
     * @param e - Evento de pintura.
     */
    void paintEvent(QPaintEvent *e) override;

    /**
     * @brief Evento de clique do mouse no widget de desenho.
     * @param event - Evento do mouse.
     */
    void mousePressEvent(QMouseEvent *event) override;

    /**
     * @brief Evento de movimento do mouse no widget de desenho.
     * @param event - Evento do mouse.
     */
    void mouseMoveEvent(QMouseEvent *event) override;

    /**
     * @brief Evento emitido ao soltar o clique do mouse no widget de desenho.
     * @param event - Evento do mouse.
     */
    void mouseReleaseEvent(QMouseEvent *event) override;
public:

    /**
     * @brief Inicializa o objeto que faz os desenhos do campo.
     * @param acAmbiente - Ponteiro para o AmbienteCampo da interface (RoboFeiSSL).
     * @param parent - Ponteiro para o widget pai (RoboFeiSSL).
     */
    DrawMap(AmbienteCampo *acAmbiente, QWidget *parent);

    /**
     * @brief Destrutor da classe.
     */
    ~DrawMap();

    /**
     * @brief Ativa ou desativa o anti-aliasing nas imagens.
     * @param ativar - Ativar = true, Desativar = false.
     */
    void vSetAntiAliasing(bool ativar);

    /**
     * @brief Seta o flag #bSaboia.
     * @param ativar - Ativar = true, Desativar = false.
     */
    void vSetSaboia(bool ativar);

    /**
     * @brief Seta o flag #bPosicaoTimeout.
     * @param ativar - Ativar = true, Desativar = false.
     */
    void vSetTimeout(bool ativar);

    /**
      * @brief Seta o flag #bPathPlanners.
      * @param ativar - Ativar = true, Desativar = false.
      */
    void vSetPathPlanners(bool ativar);

    /**
      * @brief Seta o flag #bDirecaoRobo.
      * @param ativar - Ativar = true, Desativar = false.
      */
    void vSetDirecaoRobo(bool ativar);

    /**
      * @brief Seta o flag #bPosicoesSimulacao.
      * @param ativar - Ativar = true, Desativar = false.
      */
    void vSetPosicoesSimulacao(bool ativar);

    /**
      * @brief Seta o flag #bMiraBola.
      * @param ativar - Ativar = true, Desativar = false.
      */
    void vSetMiraBola(bool ativar);

    /**
      * @brief Seta o flag #bLinhasGoleiro.
      * @param ativar - Ativar = true, Desativar = false.
      */
    void vSetLinhasGoleiro(bool ativar);

    /**
      * @brief Seta o flag #bPredicoesKalman.
      * @param ativar - Ativar = true, Desativar = false.
      */
    void vSetPredicoesKalman(bool ativar);

    /**
      * @brief Seta o flag #bGrafo.
      * @param ativar - Ativar = true, Desativar = false.
      * @param grafo - Grafo para desenhar.
      */
    void vSetGrafo(bool ativar, const VisibilityGraph *grafo = nullptr);

    /**
      * @brief Seta o flag #bBallPlacement.
      * @param ativar - Ativar = true, Desativar = false.
      */
    void vSetBallPlacement(bool ativar);

    /**
     * @brief Seta o flag #bLiberdadeRobos.
     * @param ativar - Ativar = true, Desativar = false.
     * @param avaliacoes - Vetor com a avaliação de cada robô.
     * @param tempo - Tempo desde a ultima previsão.
     * @param chuteGol - Avaliação do chute ao gol.
     * @see Decisoes.
     */
    void vSetLiberdadeRobos(bool ativar, const QVector<qint16> &avaliacoes,
                            const qint64 tempo, const qint16 chuteGol);
    /**
     * @brief Seta o flag #bRobosMines.
     * @param ativar - Ativar = true, Desativar = false.
     */
    void vSetRobosMines(bool ativar);

    /**
     * @brief Seta o flag #bDestacarReceptor.
     * @param ativar - Ativar = true, Desativar = false.
     */
    void vSetDestacaReceptor(bool ativar);

    /**
      * @brief Seta o flag #bVetorBola.
      * @param ativar - Ativar = true, Desativar = false.
      */
    void vSetVetorBola(bool ativar);

    /**
     * @brief Seta o flag #bReposicaoMouse.
     * @param ativar - Ativar = true, Desativar = false.
     */
    void vSetReposicaoMouse(bool ativar);

    /**
     * @brief Seta o valor do FPS da threads para mostrar no desenho.
     * @param kalman - Valor do FPS da thread da Visao.
     * @param estrategia - Valor do FPS da thread da clsEstrategia.
     * @param movimentacao - Valor do FPS da thread da MovimentacaoRobo.
     */
    void vSetaFPS(int ui, int kalman, int estrategia, int movimentacao);

    /**
     * @brief Seta se está sendo utilizada somente metade do campo e qual o
     * lado está sendo utilizado.
     * @param lado - Indica qual o lado está sendo utilizado
     * (#CampoInteiro/#XPositivo/#XNegativo).
     */
    void vSetMeioCampo(const int &lado);

    /**
     * @brief Seta o traço do robô para ser desenhado.
     *
     * @param _enable
     * @param _points - Pontos do traço.
     */
    void setRobotTrajectory(bool _enable, const QVector<QVector2D> _points);

    /**
     * @brief vPrint
     */
    void vPrint();

    /**
     * @brief Retorna o flag #bBotaoEsquerdoPressionado.
     * @return
     */
    bool bGetBotaoEsquerdoPressionado();

    /**
     * @brief Retorna o flag #bBotaoDireitoPressionado.
     * @return
     */
    bool bGetBotaoDireitoPressionado();

    /**
     * @brief Retorna a #vvt2dPosicaoInicialMouseBotaoDireito.
     * @return
     */
    QVector2D vt2dPosicaoInicialCliqueDireito();

    QSize sizeField() const;

    static QColor corFundoCampo;
    static QColor corRoboAmarelo;
    static QColor corBordaRoboAmarelo;
    static QColor corRoboAzul;
    static QColor corBordaRoboAzul;
    static QColor corLinhasCampo;
    static QColor corIdRobo;
    static QColor corFpsBaixo;
    static QColor corFpsAlto;
    static QColor corBolaEmCampo;
    static QColor corBolaForaDeCampo;

    void vSetDestacaRegiaoRoles(bool newBDestacaRegiaoRoles);

private:

    /**
     * @brief Muda as dimensões do objeto que faz os desenhos do campo.
     * @param widgetSize - Tamannho do widget de desenho.
     */
    void vMudarDimensoes(QSize widgetSize);

    /**
     * @brief Salva um print do desenho caso o flag #bTirarPrint estiver setado.
     * @param rct Retângulo para desenhar o print.
     */
    void vTirarPrint(const QRect &rct);

    /**
     * @brief Carrega as imagens.
     *
     */
    void vCarregaImagens();

    /**
     * @brief Ajusta o tamanho das imagens para a escala do campo.
     *
     */
    void vAjustaEscalaImagens();

    /**
     * @brief Desenha o campo, os robôs e a bola.
     */
    void vDesenhaAmbiente();

    /**
     * @brief Desenha os robôs e a bola no campo
     */
    void vDesenhaObjetos();

    /**
     * @brief Desenha os gols.
     *
     * @param _time - Cor do time aliado.
     * @param _iLadoCampo - Lado do campo do time aliado.
     */
    void vDesenhaGol(TeamColor _time, int _iLadoCampo);

    /**
     * @brief Desenha as áreas de defesa.
     */
    void vDesenhaAreaDefesa();

    /**
     * @brief Desenha as áreas de restrição do path-planner.
     */
    void vDesenhaAreaRestricao();

    /**
     * @brief Desenha as regiões do campo.
     */
    void vDesenhaRegioesCampo();

    /**
     * @brief Desenha a posição do timeout.
     * @param _vt2dPosicaoTimeout - Posição do timeout.
     */
    void vDesenhaPosicaoTimeout(QVector2D _vt2dPosicaoTimeout);

    /**
     * @brief Desenha os trajetos do path-planner.
     */
    void vDesenharPathPlanners();

    /**
     * @brief Desenha a direção em que os robôs estão se movimentando.
     */
    void vDesenharDirecaoRobo();

    /**
     * @brief Desenha a posição dos robõs simulados pela classe ARXModel.
     */
    void vDesenharPosicoesSimulacao();

    /**
     * @brief Desenha a mira da bola em direção aos dois gols.
     */
    void vDesenharMiraBola();

    /**
     * @brief Desenha as linhas para verificar o comportamento do goleiro.
     */
    void vDesenharLinhasGoleiro();

    /**
     * @brief Desenha a reta média (interpolação) das previsões futuras das posições dos
     * robôs e da bola.
     */
    void vDesenharPredicoesKalman();

    /**
     * @brief Desenha a posição do ball placement quando o comando estiver ativo.
     */
    void vDesenhaPosicaoBallPlacement();

    /**
     * @brief Desenha a liberdade de cada robô.
     * @see Decisoes.
     */
    void vDesenhaLiberdadeRobos();

    /**
     * @brief Destaca os robôs com a jogada de posicionamento Mines.
     * @see Mines.
     */
    void vDesenhaRobosMines();

    /**
     * @brief Desenha o robô/ponto determinado como receptor de um passe/chute.
     */
    void vDesenhaReceptor();

    /**
     * @brief Desenha o vetor de velocidade da bola.
     */
    void vDesenhaVetorVelocidadeBola();

    /**
     * @brief Deixa o lado não utilizado do campo em cinza.
     */
    void vDesenhaTesteMeioCampo();

    /**
     * @brief Desenha o vetor da direção da velocidade da bola quando ela é controlada
     * pelo mouse no grsim.
     */
    void vDesenhaVelocidadeBolaMouse();

    /**
     * @brief Desenha o grafo #vgGrafoDesenho.
     */
    void vDesenhaGrafo();

    /**
     * @brief Desenha o grafo e os trajetos gerados no teste dos path-planners.
     * @see TestesRobos.
     */
    void vDesenhaTestePathPlanner();

    /**
     * @brief Desenha o #vt2dTrajetoAtualMClick.
     */
    void vDesenhaTrajetoAtualMClick();

    /**
     * @brief Desenha o traço do robô.
     */
    void drawRealTrajectory();

    /**
     * @brief Converte o ponto fornecido da escala do campo para a escala da imagem.
     * @param _vt2dPonto - Ponto.
     */
    void vConvertePontoRealParaImagem(QVector2D &_vt2dPonto);

    /**
     * @brief Converte o vetor de pontos fornecidos da escala do campo para a escala da
     * imagem.
     * @param _caminho - Vetor de pontos.
     */
    void vConverteCaminhoRealParaImagem(QVector<QVector2D> &_caminho);

    /**
     * @brief Converte o ponto fornecido da escala do campo para a escala da
     * imagem.
     * @param _ponto.
     */
    void vConvertePontoRealParaImagem(QPointF &_ponto);

    /**
     * @brief Converte o retângulo fornecido da escala do campo para a escala da
     * imagem.
     * @param _retangulo.
     */
    void vConverteRetanguloRealParaImagem(QRectF &_retangulo);

    /**
     * @brief Desenha os pontos fornecidos no campo.
     *
     * @param _pontos - Vetor de pontos.
     * @param _cor - Cor dos pontos.
     * @param _dTamanhoPonto - Tamanho do pontos [pixels].
     */
    void vDesenhaPontos(QVector<QVector2D> _pontos, const QColor &_cor,
                        const double _dTamanhoPonto);

    /**
     * @brief Desenha um trajeto com os pontos fornecidos
     *
     * @param _points - Vetor de pontos 
     * @param _color - Cor do traço 
     * @param _size - Tamanho do traço
     */
    void drawPath(QVector<QVector2D> _points, const QColor &_color,
                        const double _size);

    /**
     * @brief Desenha um único ponto no campo.
     * @param _vt2dPonto - Ponto.
     * @param _cor - Cor do ponto.
     * @param _size - Tamanho do ponto.
     */
    void vDesenhaPonto(QVector2D _vt2dPonto, const QColor &_cor,
                       const double _size = 5);

    /**
     * @brief Desenha as linhas definidas pelos pontos fornecidos.
     * @param _pontos - Vetor de pontos.
     * @param _cor - Cor das linhas.
     */
    void vDesenhaLinhas(QVector<QVector2D> _pontos, const QColor &_cor);

    /**
     * @brief Desenha as linhas definidas pelos pontos fornecidos.
     * @param _pontos - Vetor de pontos.
     * @param _cor - Cor das linhas.
     * @param _iEspessuraLinha - Espessura das linhas.
     */
    void vDesenhaLinhas(QVector<QVector2D> _pontos, const QColor &_cor,
                        int _iEspessuraLinha);

    /**
     * @brief Desenha um polígono.
     *
     * @param _vertices - Vértices do polígono.
     * @param _color - Cor.
     * @param _lineWidth - Espessura da linha.
     */
    void vDrawPolygon(QVector<QVector2D> _vertices, const QColor &_color,
                      int _lineWidth);
    /**
     * @brief Desenha um polígono.
     *
     * @param _vertices - Vértices do polígono.
     * @param _color - Cor.
     * @param _lineWidth - Espessura da linha.
     */
    void vDrawPolygon(QList<QPointF> _vertices, const QColor &_color,
                      int _lineWidth);

    /**
     * @brief vDesenhaCaminhoRobo - Desenha o trajeto que o robô esta executando,
     *  com indicação do sentido.
     * @param _caminho - Trajeto.
     * @param _clrCorCaminho - Cor do trajeto.
     * @param _iEspessuraLinha - Espessura da linha.
     */
    void vDesenhaCaminhoRobo(QVector<QVector2D> _caminho, const QColor &_clrCorCaminho,
                             int _iEspessuraLinha);

    /**
     * @brief Desenha uma única linha definida pelos pontos fornecidos.
     * @attention _pontos.size() deve ser igual à 2.
     * @param _pontos - Vetor de pontos.
     * @param _cor - Cor da linha.
     * @param _iEspessuraLinha - Espessura da linha.
     */
    void vDesenhaLinha(QVector<QVector2D> _pontos, const QColor &_cor,
                       int _iEspessuraLinha);

    /**
     * @brief Desenha um circulo.
     * @param _vt2dCentro - Centro do circulo [mm].
     * @param _iRaio - Raio do círculo [mm].
     * @param _cor - Cor do circulo.
     * @param _fEspessuraLinha - Espessura da linha.
     */
    void vDesenhaCirculo(QVector2D _vt2dCentro, int _iRaio, const QColor& _cor,
                         float _fEspessuraLinha);

    /**
     * @brief Desenha a árvore produzida pelo RRT.
     *
     * @param _vt2dPontos - Pontos da árvore.
     * @param _vt2dParentes - Parentes dos pontos da árvore.
     * @param _cor - Cor da árvore.
     */
    void vDesenhaArvoreRRT(QVector<QVector2D> _vt2dPontos, QVector<QVector2D> _vt2dParentes,
                           const QColor &_cor);

    /**
     * @brief Desenha a mira do robô.
     * @param _vt2dPosicaoRobo - Posição atual do robô.
     * @param _vt2dPontoAnguloDestino - Ponto em que o robô está mirando.
     * @param _cor - Cor da mira.
     */
    void vDesenhaMiraRobo(QVector2D _vt2dPosicaoRobo, QVector2D _vt2dPontoAnguloDestino,
                          const QColor& _cor);

    /**
     * @brief Pinta metade do campo para indicar que somente metade do campo
     * esta sendo utilizada.
     */
    void vTesteMeioCampo();

    /**
     * @brief Desenha o espaço livre da bola em direção ao gol.
     *
     * @param _vt2dMiraBola - Espaço livre do gol.
     * @param _vt2dPosicaoBola - Posição da bola.
     * @param _cor - Cor da mira.
     */
    void vDesenhaMiraBola(QVector<QVector2D> _vt2dMiraBola, QVector2D _vt2dPosicaoBola,
                          const QColor &_cor);

    /**
     * @brief Desenha o grafo utilizado pelo DVG+A*
     *
     * @param vgMatrizAdjacencia - Matriz de adjacência do grafo.
     * @param _pts - Pontos do grafo.
     * @param _pais - Pais dos pontos do grafo.
     */
    void vDesenhaGrafo(std::vector<std::vector<ArestaVG> > vgMatrizAdjacencia,
                       QVector<QVector2D> _pts, QVector<QVector2D> _pais );

    /**
     * @brief Desenha os agrupamentos do grafo.
     * @param circulos - Agrupamentos determinados.
     */
    void vDesenhaAgrupamentos(const QVector<QPair<QVector2D, float> > &circulos);

    /**
     * @brief Mostra o FPS de cada uma das threads do programa.
     * Caso o frame rate esteja abaixo do esperado, a cor do texto tende a ficar preta,
     * senão, tende a ficar branca.
     * @param FPS_UI - FPS da thread da interface.
     * @param FPS_Visao - FPS da thread da visão/Kalman.
     * @param FPS_Estrategia - FPS da estratégia.
     * @param FPS_Mov - FPS da movimentação/controle.
     */
    void vMostrarFPS(float FPS_UI, float FPS_Visao, float FPS_Estrategia, float FPS_Mov);

    /**
     * @brief Desenha um robô no campo.
     * @param atbRobo - Atributos do robô.
     * @param timeRobo - Time que o robô pertence. False = oponente, True = aliado.
     * @param opacidade - Opacidade em que o robô é desenhado.
     */
    void vDesenhaRobo(const Atributos &atbRobo, bool timeRobo, float opacidade = 1.0);

    /**
     * @brief Desenha um robô no campo.
     * @param atbRobo - Atributos do robô.
     * @param opacidade - Opacidade em que o robô é desenhado.
     */
    void vDesenhaRobo(Atributos atbRobo, float opacidade);

    /**
     * @brief Desenha a zona atribuída ao robô.
     *
     * @param _zone
     * @param _color
     */
    void vDrawZone(const FieldZone &_zone, const QColor &_color);

    /**
     * @brief Desenha o retângulo que representa o roller do robô.
     *
     * @param _robot
     */
    void vDrawRoller(const QPolygonF &_rollerPoly);

    /**
     * @brief Desenha o caminho do Ball Placement e destaca a posição designada
     * @param posBallPlacement - posição designada
     * @param posBolaAtual
     */
    void vDesenhaBallPlacement(QVector2D posBallPlacement,
                               QVector2D posBolaAtual);

    /**
     * @brief Desenha um ponto em cima robô para representar a liberdade deste
     * @param posRobo - posição do robô
     * @param color - cor do ponto
     */
    void vDesenhaLiberdadeDoRobo(QVector2D posRobo,
                                 const QColor &color);

    /**
     * @brief Desenha um ponto branco para destacar o robô
     * @param posRobo
     */
    void vDesenhaPontoRobosMines(QVector2D posRobo);

    /**
     * @brief Desenha um X em cima do ponto
     * @param posPonto - posição do ponto
     * @param color - cor do x
     * @param distanciaDiagonal - comprimento do traço
     */
    void vDrawX(QVector2D posPonto,
                                const QColor color,
                                const float distanciaDiagonal);

    /**
     * @brief Traça uma reta entre a bola e o receptor e destaca o receptor com um X
     * vermelho
     * @param posReceptor
     * @param posBola
     */
    void vDestacaReceptor(QVector2D posReceptor,
                          QVector2D posBola);

    /**
     * @brief Desenha um vetor que representa a direção e a velocidade da bola
     * @param vt4dDirecaoDaBola
     * @param dVelocidadeBola
     * @param pen
     */
    void vDesenhaVetorDaBola(QVector2D posBola,
                             QVector2D vt2dVelocidadeBola,
                             QPen pen);

    void drawRobotConstraint(Eigen::MatrixXd &_constraint, QVector2D _pos);

    /**
     * @brief Escreve qual a posição do mouse em relação ao campo (#vt2dPosicaoMouse).
     */
    void vEscrevePosicaoMouse();

    /**
     * @brief Desenha uma indicação que o robô está tentando chutar.
     * @param pos - Posição do robô.
     * @param _cor - Cor da indicação.
     */
    void vDesenhaIndicacaoChute(const QVector2D &pos, const QColor &_cor);

    /**
     * @brief Desenha o contorno do robô
     *
     * @param pos - Posição do robô.
     * @param anguloRobo - Ângulo do robô.
     * @param _cor - Cor do contorno.
     */
    void vDesenhaContornoRobo(const QVector2D &pos, float anguloRobo,
                              const QColor &_cor);

    void drawDebugData();

    int iLarguraGol;         /**< Largura do gol. */
    int iProfundidadeGol;    /**< Profundidade do gol. */
    int iLarguraDefesa;      /**< Largura da área de defesa. */
    int iProfundidadeDefesa; /**< Profundidade da área de defesa. */

    bool bAntiAliasing;      /**< Indica se o Anti-Aliasing deve ser utilizado. */
    bool bSaboia;            /**< Indica se o Saboia(#imPadreSaboia) deve ser desenhado.*/
    bool bPosicaoTimeout;    /**< Indica se a posicao do timeout deve ser destacada
                                                                (#imPosicaoTimeout). */
    bool bPathPlanners;      /**< Indica se os trajetos dos robôs devem ser desenhados. */
    bool bDirecaoRobo;       /**< Indica se a direção da velocidade de comando dos robôs
                           (Atributos::vt2dVelocidadeNaoConvertida) deve ser desenhada. */
    bool bPosicoesSimulacao; /**< Indica se as simulações dos modelos ARXModel devem ser
                                                                 desenhadas. */
    bool bMiraBola;          /**< Indica se a mira da bola em direção aos gols deve ser
                                                                            desenhada. */
    bool bLinhasGoleiro;     /**< Indica se as linhas do goleiro devem ser desenhadas. */
    bool bPredicoesKalman;	 /**< Indica se as linhas de predição do KalmanFilter devem
                                                                        ser desenhadas. */
    bool bGrafo;             /**< Indica se o (VisibilityGraph) deve ser desenhado. */
    bool bBallPlacement;	 /**< Indica se a posição do ball placement deve ser destacada
                                                                                      . */
    bool bLiberdadeRobos;    /**< Indica se a liberdade dos robôs deve ser destacada.
                                                                         @see Decisoes. */
    bool bRobosMines;		 /**< Indica se os robôs participantes do posicionamento Mines
                                                                  devem ser destacados. */
    bool bDestacarReceptor;  /**< Indica se o receptor dos passes/chutes deve ser
                                                                             destacado. */
    bool bVetorBola;		 /**< Indica se o vetor de velocidade da bola deve ser
                                                                             desenhado. */
    bool bReposicaoMouse;    /**< Indica se a reposição da bola com o mouse está
                                                                            habilitada. */
    bool bDestacaRegiaoRoles;
    int iLadoCampoUtilizado; /**< Indica qual o lado do campo está sendo usado. */

    bool bTirarPrint;		 /**< Indica se é para tirar um print da tela. */

    QVector<qint16> vtiAvaliacoesReceptores; /**< Utilizado no método
                                                #vDesenhaLiberdadeRobos. @see Decisoes. */
    double dTempoUltimaPrevisao;    /**< Utilizado no método #vDesenhaLiberdadeRobos.
                                                                         @see Decisoes. */
    qint16 iAvaliacaoChuteAoGol;    /**< Utilizado no método #vDesenhaLiberdadeRobos.
                                                                         @see Decisoes. */

    bool drawRobotTrajectory; /**< Indica se é para desenhar o traço do robô. */
    QVector<QVector2D> robotTrajectory; /**< Trajetória do robô para ser desenhada. */

    QVector<QVector2D> vt2dTrajetoAtualMClick; /**< Trajeto atual da MovimentacaoClick. */

    QVector<int> iFPSCounter; /**< Vetor para calcular o FPS médio do widget. */
    int iFPSUI;               /**< FPS médio da interface (RoboFeiSSL). */
    int iFPSKalman;			  /**< FPS médio da visão (Visao). */
    int iFPSEstrategia;       /**< FPS médio da estratégia (clsEstrategia). */
    int iFPSMovimentacao;     /**< FPS médio da movimentação (MovimentacaoRobo).*/
    int iFPSDesenho;          /**< FPS médio dos desenhos (DrawMap). */

    // Variáveis utilizadas para mostrar os trajetos no teste dos path-planners
    VisibilityGraph vgGrafoDesenho;              /**< Grafo (VisibilityGraph) utilizado
                                                                          pelo A_StarVG.*/
    QVector<QVector2D> vt2dCaminhoAStar;         /**< Caminho gerado pelo A_StarVG e
                                                                    devidamente tratado.*/
    QVector<QVector2D> vt2dCaminhoOriginalAStar; /**< Caminho gerado pelo A_StarVG sem
                                                                      nenhum tratamento.*/
    QVector<QVector2D> vt2dPontosArvore;         /**< Pontos da árvore gerada pelo RRT.*/
    QVector<QVector2D> vt2dPaisArvore;           /**< Pais dos pontos da árvore gerada
                                                                               pelo RRT.*/
    QVector<QVector2D> vt2dCaminhoRRT;           /**< Caminho gerado pelo RRT e
                                                                    devidamente tratado.*/
    QVector<QVector2D> vt2dCaminhoOriginalRRT;   /**< Caminho gerado pelo RRT sem
                                                                      nenhum tratamento.*/
    // -------------------------------------------------------------------------

    QSize szCampo;  /**< Tamanho do campo real [mm]. */

    QVector2D vt2dPosicaoMouse;  /**< Posicão do mouse na tela referenciada ao campo
                                                                                  [mm]. */
    QVector2D vt2dPosicaoInicialMouseBotaoDireito; /**< Posição do mouse ao clicar com o
                                              botão direito referenciada ao campo [mm]. */
    bool bBotaoEsquerdoPressionado;/**< Indica se o botão esquerdo do mouse está
                                                                           pressionado. */
    bool bBotaoDireitoPressionado; /**< Indica se o botão direito do mouse está
                                                                           pressionado. */

    QSize sizeTamanhoCampo;      /**< Tamanho do campo real [mm]. */
    QSize sizeTamanhoWidget;     /**< Tamanho do widget de desenho. */

    float fEscalaX;              /**< Valor da escala no eixo X da imagem real para o
                                                          tamanho do widget de desenho. */
    float fEscalaY;              /**< Valor da escala no eixo Y da imagem real para o
                                                          tamanho do widget de desenho. */

    QPen *pen;                   /**< Caneta utilizada para fazer os desenhos. */
    QPainter *pntPincel;         /**< Pincel utilizado nos desenhos. */

    QImage imPosicaoTimeout;     /**< Imagem da posição inicial do timeout. */
    QImage imPadreSaboia;        /**< Imagem do Padre Sabóia. */
    QImage imInimigo;            /**< Imagem do oponente. */

    QVector<QPixmap> pngRobos;   /**< Imagens das capas dos robôs. */
    QVector<QString> pngAddress; /**< Endereços das imagens dos robôs. */

    int elapsed; /**< Tempo entre cada atualização do widget (#vAnimar). */
    AmbienteCampo *acWidgetCampo; /**< Ponteiro para o ambiente de campo da interface
                                            (RoboFeiSSL::acInterface). */

    QElapsedTimer etmFPSDesenhoCampo; /**< Timer para a medição do FPS médio do widget de
                                                                               desenho. */
};

#endif // DRAWMAP_H
