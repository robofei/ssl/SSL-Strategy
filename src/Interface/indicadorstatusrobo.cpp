/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "indicadorstatusrobo.h"

IndicadorStatusRobo::IndicadorStatusRobo(QWidget *parent) :
    QPushButton(parent)
{
    this->pmIndicator = QPixmap(TAMANHO_INDICADOR);

    this->pmIndicator.load(":/Imagens/StatusRoboB.svg");
    this->pmIndicator = this->pmIndicator.scaled(TAMANHO_INDICADOR, Qt::KeepAspectRatio,
                                                 Qt::SmoothTransformation);
    this->setFixedWidth(TAMANHO_INDICADOR.width());
    this->setFixedHeight(TAMANHO_INDICADOR.height());

    connect(this, &QPushButton::clicked,
            this, &IndicadorStatusRobo::recebeuClick);

    clrRobot = QColor(255, 255, 255);
}

IndicadorStatusRobo::~IndicadorStatusRobo()
{
    if(this->pntPainter.isActive())
        this->pntPainter.end();
}

void IndicadorStatusRobo::vAtualizaStatus(const Atributos &atbRobo,
                                          const int &rodaRuim,
                                          const int &time, const QColor &_color)
{
    atbRoboIndicado = atbRobo;
    clrRobot = _color;
    if(time != corTime)
    {
        corTime = time;
        const QString status = corTime == 1? "B" : "Y";
        this->pmIndicator.load(":/Imagens/StatusRobo" + status + ".svg");
        this->pmIndicator = this->pmIndicator.scaled(TAMANHO_INDICADOR,
                                                     Qt::KeepAspectRatio,
                                                     Qt::SmoothTransformation);
    }
    iRodaRuim = rodaRuim;
    update();
}

void IndicadorStatusRobo::paintEvent(QPaintEvent *e)
{
    QPushButton::paintEvent(e);
    this->pntPainter.begin(this);

    this->pntPainter.drawPixmap(QPoint(0,0), this->pmIndicator);
    this->vMudaID(atbRoboIndicado.id);
    this->vMudaBateria(atbRoboIndicado.battery);
    this->vMudaEstadoRoller(atbRoboIndicado.roller,
                            atbRoboIndicado.etmControleKick.nsecsElapsed()/1.0e9);
    this->vMudaSensorBola(atbRoboIndicado.kickSensor);
    this->vMudaRoboEmCampo(atbRoboIndicado.bDetectadoVisao);
    this->vMudaRodaRuim(iRodaRuim);
    this->vMudaRadio(false); //TODO : Implementar a verificação se o robô está
                             // recebendo dados do rádio.
    this->pntPainter.end();
}

void IndicadorStatusRobo::recebeuClick()
{
    emit clicked(atbRoboIndicado.id);
}

void IndicadorStatusRobo::vMudaID(int id)
{
    QFont font =this->pntPainter.font();
    font.setPixelSize(30);
    this->pntPainter.setFont(font);
    this->pntPainter.setPen(clrRobot);

    const QRectF textRect(POS_ID, QSizeF(30, 30));
    this->pntPainter.drawText(textRect, QString::number(id));
}

void IndicadorStatusRobo::vMudaBateria(float bat)
{
    if(bat < MAX_BAT - MAX_VOLTAGE_DROP)
        bat = MAX_BAT - MAX_VOLTAGE_DROP;
    else if(bat > MAX_BAT)
        bat = MAX_BAT;

    float batVal = LARGURA_BATERIA * (1 - (MAX_BAT - bat)/ MAX_VOLTAGE_DROP);

    QColor cor = QColor(this->pmIndicator.toImage().pixel(
                            qRound(POS_BATERIA.x() + batVal), POS_BATERIA.y() - 1));

    QPen pen = QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    this->pntPainter.setPen(pen);
    this->pntPainter.setBrush(pen.color());

    this->pntPainter.drawRect(POS_BATERIA.x(), POS_BATERIA.y(),
                              LARGURA_BATERIA - 2, ALTURA_BATERIA);

    pen = QPen(cor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    this->pntPainter.setPen(pen);
    this->pntPainter.setBrush(pen.color());

    this->pntPainter.drawRect(POS_BATERIA.x(), POS_BATERIA.y(), batVal, ALTURA_BATERIA);
}

void IndicadorStatusRobo::vMudaEstadoRoller(bool roller, double tempoChute)
{
    this->pntPainter.setBrush(Qt::black);
    this->pntPainter.drawRect(ROLLER);

    QColor cor(0, 255, 0);
    if(!roller)
    {
        cor = QColor(255, 0, 0);
    }

    QPen pen = QPen(cor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    this->pntPainter.setPen(pen);
    this->pntPainter.setBrush(cor);

    QRect rollerRect = ROLLER;

    // Corrige o tempo atual caso esteja fora do intervalo
    tempoChute = qMax(tempoChute, 0.0);
    tempoChute = qMin(tempoChute, static_cast<double>(iTempoRecargaChute));

    int rollerWidth = iTempoRecargaChute == 0 ? ROLLER.width() :
                          qFloor(ROLLER.width()*tempoChute/iTempoRecargaChute);
    rollerRect.setWidth(rollerWidth);
    this->pntPainter.drawRect(rollerRect);
}

void IndicadorStatusRobo::vMudaSensorBola(bool bola)
{
    QColor cor(0, 255, 0);
    if(!bola)
    {
        cor = QColor(255, 0, 0);
    }

    QPen pen = QPen(cor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    this->pntPainter.setPen(pen);
    this->pntPainter.setBrush(cor);
    this->pntPainter.setRenderHint(QPainter::Antialiasing, true);
    this->pntPainter.drawEllipse(POS_SENSOR_BOLA, RAIO_SENSOR_BOLA, RAIO_SENSOR_BOLA);
    this->pntPainter.setRenderHint(QPainter::Antialiasing, false);
}

void IndicadorStatusRobo::vMudaRodaRuim(int roda)
{
    this->vSetaCorRoda(roda, 1);
    this->pntPainter.drawRect(RODA_1);

    this->vSetaCorRoda(roda, 2);
    this->pntPainter.drawRect(RODA_2);

    this->vSetaCorRoda(roda, 3);
    this->pntPainter.drawRect(RODA_3);

    this->vSetaCorRoda(roda, 4);
    this->pntPainter.drawRect(RODA_4);
}

void IndicadorStatusRobo::vMudaRoboEmCampo(bool emCampo)
{
    QColor cor(0, 255, 0);
    if(!emCampo)
    {
        cor = QColor(255, 0, 0);
    }

    QPen pen = QPen(cor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    this->pntPainter.setPen(pen);
    this->pntPainter.setBrush(cor);
    this->pntPainter.setRenderHint(QPainter::Antialiasing, true);
    this->pntPainter.drawEllipse(POS_VISAO, RAIO_VISAO, RAIO_VISAO);
    this->pntPainter.setRenderHint(QPainter::Antialiasing, false);
}

void IndicadorStatusRobo::vMudaRadio(bool radio)
{
    QColor cor(0, 255, 0);
    if(!radio)
    {
        cor = QColor(255, 0, 0);
    }
    QPen pen = QPen(cor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    this->pntPainter.setPen(pen);
    this->pntPainter.setBrush(pen.color());

    this->pntPainter.drawRect(RADIO);
    this->pntPainter.setRenderHint(QPainter::Antialiasing, true);
    this->pntPainter.drawEllipse(POS_IND_RADIO, RAIO_RADIO, RAIO_RADIO);
    this->pntPainter.setRenderHint(QPainter::Antialiasing, false);
}

void IndicadorStatusRobo::vSetaCorRoda(int roda, int rodaAtual)
{
    QColor green(0, 255, 0);
    QColor red(255, 0, 0);
    QPen pen;
    if(roda == rodaAtual)
        pen = QPen(red, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    else
        pen = QPen(green, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    this->pntPainter.setPen(pen);
    this->pntPainter.setBrush(pen.color());
}
