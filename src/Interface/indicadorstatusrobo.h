/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file indicadorstatusrobo.h
 * @brief Arquivo header da classe do indicar de status dos robôs.
 * @author Leonardo da Silva Costa
 * @version 1.0
 * @date 2020-08-27
 */
#ifndef INDICADORSTATUSROBO_H
#define INDICADORSTATUSROBO_H

#include <QObject>
#include <QPushButton>
#include <QImage>
#include <QPixmap>
#include <QPainter>
#include <QLabel>
#include <QDebug>
#include <QFont>
#include <QElapsedTimer>
#include <QPushButton>
#include <QStaticText>
#include <QtMath>

#include "Robo/atributos.h"

#define MAX_BAT 12.4 /**< Nível máximo possível da bateria do robô [V].*/
#define MAX_VOLTAGE_DROP 1.5 /**< Queda máxima de tensão da bateria do robô [V]*/

const QPoint POS_BATERIA(25,15); /**< Posição onde inicia o retângulo da bateria.*/
const QPoint POS_ID(42, 37); /**< Posição para inserção do caractere do ID do robô.*/
const QPointF POS_SENSOR_BOLA(50, 87.5); /**< Posição do indicador do sensor da bola.*/
const QPointF POS_VISAO(16.5, 86.5); /**< Posição do indicador de detecção na visão.*/
const QPointF POS_IND_RADIO(90.3, 71.5); /**< Posição do indicador de chegada de pacotes
                                              do rádio.*/
const QRect RODA_1(95, 47, 3, 18); /**< Retângulo da roda 1 no indicador.*/
const QRect RODA_2(95, 14, 3, 18); /**< Retângulo da roda 2 no indicador.*/
const QRect RODA_3(1, 14, 3, 18); /**< Retângulo da roda 3 no indicador.*/
const QRect RODA_4(1, 47, 3, 18); /**< Retângulo da roda 4 no indicador.*/
const QRect ROLLER(31, 71, 37, 6); /**< Retângulo do indicador do roller.*/
const QRectF RADIO(82, 80, 6.5, 5); /**< Retângulo do indicador do rádio.*/
const float RAIO_VISAO = 3.5; /**< Raio do indicador de detecção na visão.*/
const float RAIO_RADIO = 1.2; /**< Raio do indicador de chegada de pacotes do rádio.*/
const int ALTURA_BATERIA = 19; /**< Altura máxima do retângulo da bateria.*/
const int LARGURA_BATERIA = 48; /**< Largura máxima do retângulo da bateria.*/
const int RAIO_SENSOR_BOLA = 7; /**< Raio do indicador do sensor da bola.*/
const QSize TAMANHO_INDICADOR(100, 100); /**< Tamanho padronizado do indicador.*/

/**
 * @brief Classe do widget indicador de status do robô
 * @details Este widget apresenta diversos dados do robô, como, bateria,
 * sensor de chute, ativação do roller e etc. A imagem padrão do widget para os
 * robôs azuis e amarelos é:
 * @image html Figuras/StatusRoboB.svg width=7.5%
 * @image html Figuras/StatusRoboY.svg width=7.5%
 */
class IndicadorStatusRobo : public QPushButton
{
    Q_OBJECT
public:
    /**
     * @brief Construtor da classe
     * @param parent - Widget pai
     */
    IndicadorStatusRobo(QWidget *parent = 0);

    /**
     * @brief Destrutor da classe
     */
    ~IndicadorStatusRobo();

    /**
     * @brief Atualiza o indicador
     */
    void vAtualizaStatus(const Atributos &atbRobo, const int &rodaRuim,
                         const int &time, const QColor &_color);

signals:
    /**
     * @brief Sinal emitido quando o widget é clicado
     *
     * @param id - ID do robô que teve o widget clicado
     */
    void clicked(const int &id);

public slots:
    /**
     * @brief Evento de pintura do widget
     */
    void paintEvent(QPaintEvent *) override;

private slots:
    /**
     * @brief Recebe o sinal do QPushButton de click
     */
    void recebeuClick();

private:

    /**
     * @brief Coloca o ID do robô na imagem padrão
     * @param id - ID do robô
     */
    void vMudaID(int id);

    /**
     * @brief Muda o nível de bateria no indicador
     * @param bat - Valor atual da bateria
     */
    void vMudaBateria(float bat);

    /**
     * @brief Muda o estado atual do roller e indica o tempo de chute do robô
     * @details A cor do do retângulo indica se o roller está ativado ou não. O
     * preenchimento do retângulo indica o tempo desde a última vez que o robô chutou,
     * ou seja, o retângulo cheio indica que se passou #iTempoRecargaChute desde que o
     * robô chutou
     * @param roller - Estado do roller. `true`/`false` = ativado/desativado
     * @param tempoChute - Tempo que se passou desde que o robô chutou [s]
     */
    void vMudaEstadoRoller(bool roller, double tempoChute);

    /**
     * @brief Muda o estado atual do sensor de bola
     * @param bola - Estado atual do sensor de bola. Funciona igual o vMudaEstadoRoller
     */
    void vMudaSensorBola(bool bola);

    /**
     * @brief Muda a indicação de qual roda está ruim
     * @param roda - Número da roda com problemas
     */
    void vMudaRodaRuim(int roda);

    /**
     * @brief Muda o estado atual da detecção do robô no campo
     * @param emCampo - Flag que indica se o robô está no campo
     */
    void vMudaRoboEmCampo(bool emCampo);

    /**
     * @brief Muda o estado se o robô está recebendo dados do rádio
     * @param radio - Indica se o robô está recebendo dados do rádio
     */
    void vMudaRadio(bool radio);

    /**
     * @brief Seta a cor do pincel para os retângulos das rodas
     * @param roda - Número da roda com problemas
     * @param rodaAtual - Número da roda atual sendo pintada no indicador
     */
    void vSetaCorRoda(int roda, int rodaAtual);

    Atributos atbRoboIndicado; /**< Atributos do robô indicado. */
    QColor clrRobot; /**< Cor do robô. */
    int corTime;               /**< Time atual. */
    int iRodaRuim;             /**< Número da roda com problemas. Se não houver
                                   nenhuma, será igual à -1. */
    QPixmap pmIndicator; /**< QPixmap do indicador.*/
    QPainter pntPainter; /**< Pincel para manipular o indicador.*/
};

#endif // INDICADORSTATUSROBO_H
