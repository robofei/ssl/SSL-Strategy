/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "joystickrobotcontrollerinterface.h"
#include "ui_joystickrobotcontrollerinterface.h"

namespace Auxiliar
{
qint8 iConverteEscalaAxis_100_100(qint16 iAxis, qint16 iValorMaximo)
{
    return static_cast<qint8>( iAxis/32767.0*iValorMaximo );
}

qint8 iConverteEscalaAxis_0_100(qint16 iAxis, qint16 iValorMaximo)
{
    return static_cast<qint8>( (iAxis + axisMaximumV)/65534.0*iValorMaximo );
}
}

JoystickRobotControllerInterface::JoystickRobotControllerInterface(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VirtualJoystickInterface),
    virtualJoystick(new VirtualJoystick(this)),
    bHalt(false),
    robo()
{
    ui->setupUi(this);

    this->setWindowTitle("Robot Controller");
    this->setAttribute(Qt::WA_DeleteOnClose);

    // Connect do sinal de Axis (analógico) do VirtualJoystick
    connect(virtualJoystick.get(), &VirtualJoystick::SIGNAL_axisEvent,
            this, &JoystickRobotControllerInterface::SLOT_getAxisEventVirtual);

    // Connect do sinal de botão do virtual joystick
    connect(virtualJoystick.get(), &VirtualJoystick::SIGNAL_buttonEvent,
            this, &JoystickRobotControllerInterface::SLOT_getButtonEventVirtual);

    // Conexão utilizada para alterar a sensibilidade
    connect(ui->hs_Sensibilidade, &QSlider::valueChanged,
            virtualJoystick.get(), &VirtualJoystick::SLOT_setStepValue);

    robo.id = ui->sb_robotID->value();
    ui->l_Roller_ON_OFF->setAutoFillBackground(true);
    vSetRollerOnOffInterface(false);
}

JoystickRobotControllerInterface::~JoystickRobotControllerInterface()
{
    delete ui;
}

void JoystickRobotControllerInterface::closeEvent(QCloseEvent *event)
{
    event->accept();
    emit vCloseVirtualJoystickInterface();
}

void JoystickRobotControllerInterface::reject()
{
    emit vCloseVirtualJoystickInterface();
}

void JoystickRobotControllerInterface::SLOT_getAxisEventVirtual(const VirtualJoystickAxisEvent& event)
{
    if(bHalt)
        return;

    const qint16 velocidadeMaxima = ui->sb_velocidadeMaxima->value();

    switch(event.axis)
    {
        case AXIS_AD:
            robo.iVx = Auxiliar::iConverteEscalaAxis_100_100(event.value, velocidadeMaxima);
            ui->pB_Vx->setValue(robo.iVx);
            break;

        case AXIS_SW:
            robo.iVy = Auxiliar::iConverteEscalaAxis_100_100(event.value, velocidadeMaxima);
            ui->pB_Vy->setValue(robo.iVy);
            break;

        case AXIS_QE:
            robo.iVw = Auxiliar::iConverteEscalaAxis_100_100(event.value, velocidadeMaxima);
            ui->pB_W->setValue(robo.iVw);
            break;

        case AXIS_UO:
            robo.iForcaChute = Auxiliar::iConverteEscalaAxis_0_100(event.value, ui->sb_forcaChuteMaxima->value());
            ui->pB_ForcaChute->setValue(robo.iForcaChute);
            break;

        case AXIS_JL:
            robo.iVelocidadeRoller= Auxiliar::iConverteEscalaAxis_0_100(event.value, 100);
            ui->pB_Velocidade_Roller->setValue(robo.iVelocidadeRoller);
            break;

        default:
            break;
    }

    emit SIGNAL_enviaDadosRobo(robo);
}

void JoystickRobotControllerInterface::SLOT_getButtonEventVirtual(const VirtualJoystickButtonEvent& event)
{
    if(bHalt && event.key != Qt::Key_0)
        return;

    switch(event.key)
    {
        case Qt::Key_0:
            ui->cb_HALT->setChecked(event.pressed);
            bHalt = event.pressed;

            if(event.pressed)
            {
                virtualJoystick->vResetAxis({AXIS_AD, AXIS_SW, AXIS_QE, AXIS_UO, AXIS_JL});

                ui->cb_Liga_Roller->setChecked(false);
                ui->cb_Desliga_Roller->setChecked(false);

                ui->cb_KICK_SOFT->setChecked(false);
                ui->cb_CHIP_KICK_STRONG->setChecked(false);
                ui->cb_KICK_STRONG_KICK_CUSTOM->setChecked(false);
                ui->cb_KickStrong->setChecked(false);
                ui->cb_PararRobo->setChecked(false);

                vSetRollerOnOffInterface(false);
                vSetKickType(KICK_NONE);

                robo.iVx = 0;
                robo.iVy = 0;
                robo.iVw = 0;
                robo.iForcaChute = 0;
                robo.iVelocidadeRoller = 0;
                ui->pB_Vx->setValue(robo.iVx);
                ui->pB_Vy->setValue(robo.iVy);
                ui->pB_W->setValue(robo.iVw);
                ui->pB_ForcaChute->setValue(robo.iForcaChute);
                ui->pB_Velocidade_Roller->setValue(robo.iVelocidadeRoller);
            }
            break;

        case Qt::Key_1:
            ui->cb_Desliga_Roller->setChecked(event.pressed);

            if(event.pressed)
            {
                ui->cb_Liga_Roller->setChecked(false);
                vSetRollerOnOffInterface(false);
            }
            break;

        case Qt::Key_2:
            ui->cb_Liga_Roller->setChecked(event.pressed);

            if(event.pressed)
            {
                ui->cb_Desliga_Roller->setChecked(false);
                vSetRollerOnOffInterface(true);
            }
            break;

        case Qt::Key_3:
            ui->cb_KICK_SOFT->setChecked(event.pressed);

            if(event.pressed)
            {
                ui->cb_KICK_STRONG_KICK_CUSTOM->setChecked(false);
                ui->cb_CHIP_KICK_STRONG->setChecked(false);
                ui->cb_KickStrong->setChecked(false);
                ui->cb_ChipKickCustom->setChecked(false);
                vSetKickType(KICK_SOFT);
            }
            else
            {
                vSetKickType(KICK_NONE);
            }
            break;

        case Qt::Key_4:
            ui->cb_KICK_STRONG_KICK_CUSTOM->setChecked(event.pressed);

            if(event.pressed)
            {
                ui->cb_KICK_SOFT->setChecked(false);
                ui->cb_CHIP_KICK_STRONG->setChecked(false);
                ui->cb_KickStrong->setChecked(false);
                ui->cb_ChipKickCustom->setChecked(false);
                vSetKickType(KICK_CUSTOM);

            }
            else
            {
                vSetKickType(KICK_NONE);
            }
            break;

        case Qt::Key_5:
            ui->cb_CHIP_KICK_STRONG->setChecked(event.pressed);

            if(event.pressed)
            {
                ui->cb_KICK_SOFT->setChecked(false);
                ui->cb_KICK_STRONG_KICK_CUSTOM->setChecked(false);
                ui->cb_KickStrong->setChecked(false);
                ui->cb_ChipKickCustom->setChecked(false);
                vSetKickType(CHIP_KICK_STRONG);
            }
            else
            {
                vSetKickType(KICK_NONE);
            }
            break;

        case Qt::Key_6:
            ui->cb_KickStrong->setChecked(event.pressed);

            if(event.pressed)
            {
                ui->cb_KICK_SOFT->setChecked(false);
                ui->cb_KICK_STRONG_KICK_CUSTOM->setChecked(false);
                ui->cb_CHIP_KICK_STRONG->setChecked(false);
                ui->cb_ChipKickCustom->setChecked(false);

                vSetKickType(KICK_STRONG);
            }
            else
            {
                vSetKickType(KICK_NONE);
            }
            break;

        case Qt::Key_7:
            ui->cb_PararRobo->setChecked(event.pressed  );
            if(event.pressed)
            {
                virtualJoystick->vResetAxis({AXIS_AD, AXIS_SW, AXIS_QE});

                robo.iVx = 0;
                robo.iVy = 0;
                robo.iVw = 0;

                ui->pB_Vx->setValue(robo.iVx);
                ui->pB_Vy->setValue(robo.iVy);
                ui->pB_W->setValue(robo.iVw);
            }
            break;

        case Qt::Key_8:
            ui->cb_ChipKickCustom->setChecked(event.pressed);

            if(event.pressed)
            {
                ui->cb_KICK_SOFT->setChecked(false);
                ui->cb_KICK_STRONG_KICK_CUSTOM->setChecked(false);
                ui->cb_CHIP_KICK_STRONG->setChecked(false);
                ui->cb_KickStrong->setChecked(false);

                vSetKickType(CHIP_KICK_CUSTOM);
            }
            else
            {
                vSetKickType(KICK_NONE);
            }
            break;

        default:
            break;
    }

    emit SIGNAL_enviaDadosRobo(robo);
}


void JoystickRobotControllerInterface::vSetKickType(KICKTYPE type)
{
    switch (type)
    {
        case KICK_NONE:
            ui->l_KICK_TYPE->setText("KICK_NONE");
            break;
        case KICK_SOFT:
            ui->l_KICK_TYPE->setText("KICK_SOFT");
            break;
        case KICK_CUSTOM:
            ui->l_KICK_TYPE->setText("KICK_CUSTOM");
            break;
        case CHIP_KICK_STRONG:
            ui->l_KICK_TYPE->setText("CHIP_KICK_STRONG");
            break;
        case KICK_STRONG:
            ui->l_KICK_TYPE->setText("KICK_STRONG");
            break;
        case CHIP_KICK_CUSTOM:
            ui->l_KICK_TYPE->setText("CHIP_KICK_CUSTOM");
            break;
        default:
            qFatal("KICK_TYPE impossível");
            break;
    }

    robo.kickType = type;
}


void JoystickRobotControllerInterface::vSetRollerOnOffInterface(bool b)
{
    if(b)
        ui->l_Roller_ON_OFF->setPalette(QPalette(QColor(Qt::green)));
    else
        ui->l_Roller_ON_OFF->setPalette(QPalette(QColor(Qt::red)));

    robo.bRollerLigado = b;
}

void JoystickRobotControllerInterface::on_sb_robotID_valueChanged(int arg1)
{
    robo.id = arg1;
    emit SIGNAL_enviaDadosRobo(robo);
}

void JoystickRobotControllerInterface::on_sb_velocidadeMaxima_valueChanged(int arg1)
{
    if(robo.iVx > arg1)
        robo.iVx = arg1;
    else if(robo.iVx < -arg1)
        robo.iVx = -arg1;

    if(robo.iVy > arg1)
        robo.iVy = arg1;
    else if(robo.iVy < -arg1)
        robo.iVy = -arg1;

    if(robo.iVw > arg1)
        robo.iVw = arg1;
    else if(robo.iVw < -arg1)
        robo.iVw = -arg1;

    ui->pB_Vx->setValue(robo.iVx);
    ui->pB_Vy->setValue(robo.iVy);
    ui->pB_W->setValue(robo.iVw);

    emit SIGNAL_enviaDadosRobo(robo);
}

void JoystickRobotControllerInterface::on_sb_forcaChuteMaxima_valueChanged(int arg1)
{
    if(robo.iForcaChute > arg1)
        robo.iForcaChute = arg1;

    ui->pB_ForcaChute->setValue(robo.iForcaChute);

    emit SIGNAL_enviaDadosRobo(robo);
}
