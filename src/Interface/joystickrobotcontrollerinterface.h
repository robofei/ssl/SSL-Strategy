#ifndef VIRTUALJOYSTICKINTERFACE_H
#define VIRTUALJOYSTICKINTERFACE_H

///
/// \file joystickrobotcontrollerinterface.h
/// \brief \a VirtualJoystickRobot e \a JoystickRobotControllerInterface
///

#include <QDialog>

#include "VirtualJoystick/virtualjoystick.h"
#include "Robo/atributos.h"

namespace Ui {
class VirtualJoystickInterface;
}

///
/// \brief Estrutura com os dados utilizados no controle do robô
///
struct VirtualJoystickRobot
{
    qint8 iVx = 0; ///< Velocidade X (em relação à geometria do campo)
    qint8 iVy = 0; ///< Velocidade Y (em relação à geometria do campo)
    qint8 iVw = 0; ///< Velocidade de rotação omega
    qint8 iVelocidadeRoller = 0; ///< Velocidade do roller em uma escala de 0 a 100
    qint8 iForcaChute = 0; ///< Força do chute em uma escala de 0 a 100
    bool bRollerLigado = false; ///< Status do roller (On/Off)
    KICKTYPE kickType = KICK_NONE; ///< Tipo do chute
    qint8 id = -1; ///< ID do robô sendo testado
};
Q_DECLARE_METATYPE(VirtualJoystickRobot)

///
/// \brief Classe responsável pelo teste de controlar o robô com o Joystick.
/// \details Interface que mostra o status do joystick e serve de intermediária na comunicação com o objeto de testes.
///
///
class JoystickRobotControllerInterface : public QDialog
{
    Q_OBJECT

public:
    ///
    /// \brief Construtor padrão da classe
    /// \param Configura e conecta o \a VirtualJoystick
    ///
    explicit JoystickRobotControllerInterface(QWidget *parent = nullptr);

    ///
    /// \brief Destrutor da classe
    ///
    ~JoystickRobotControllerInterface();

protected:
    ///
    /// \brief Overload do slot da classe QDialog
    ///
    /// \details Esse slot é chamado quando o usuário finaliza
    /// a janela (por um shortcut ou por algum botão).
    ///
    /// O sinal \p vCloseVirtualJoystickInterface() é emitido indicando que o objeto
    /// da classe pode ser finalizado.
    ///
    void closeEvent(QCloseEvent *event = nullptr);

    ///
    /// \brief Overload do slot da classe QDialog
    /// \details Esse slot é chamado quando o usuário
    /// pressiona a tecla ESCAPE.
    ///
    /// O sinal \p vCloseVirtualJoystickInterface() é emitido indicando que o objeto
    /// da classe pode ser finalizado.
    ///
    void reject();


private:
    ///
    /// \brief Elemento da interface do usuário
    ///
    Ui::VirtualJoystickInterface *ui;

    ///
    /// \brief Objeto da classe \a VirtualJoystick
    ///
    QScopedPointer<VirtualJoystick> virtualJoystick;

    ///
    /// \brief Indica se o estado halt está ativo (onde todas as entradas são ignoradas)
    ///
    bool bHalt;

    ///
    /// \brief Objeto da classe \a VirtualJoystickRobot com os dados do robô testado
    ///
    VirtualJoystickRobot robo;

    ///
    /// \brief Muda o estado do tipo de chute na interface e no \a robo
    /// \param type
    ///
    void vSetKickType(KICKTYPE type);

    ///
    /// \brief Muda o estado do Roller na interface e no \a robo
    /// \param b
    ///
    void vSetRollerOnOffInterface(bool b);

private slots:

    ///
    /// \brief Trata o evento de axis vindo do \a virtualJoystick
    /// \param event
    ///
    void SLOT_getAxisEventVirtual(const VirtualJoystickAxisEvent& event);

    ///
    /// \brief Trata o evento de botão vindo do \a virtualJoystick
    /// \param event
    ///
    void SLOT_getButtonEventVirtual(const VirtualJoystickButtonEvent& event);

    void on_sb_robotID_valueChanged(int arg1);

    void on_sb_velocidadeMaxima_valueChanged(int arg1);

    void on_sb_forcaChuteMaxima_valueChanged(int arg1);

signals:

    ///
    /// \brief Envia o novo valor de step da variação do axis do \a virtualJoystick
    ///
    void SIGNAL_passoAxisMudou(double);

    ///
    /// \brief Envia os dados do robô
    /// \details Esse sinal é emitido sempre que algum sinal do \a virtualJoystick é
    /// emitido (desde o joystick não esteja em estado halt)
    ///
    void SIGNAL_enviaDadosRobo(VirtualJoystickRobot);

    ///
    /// \brief Emite um sinal para fechar a classe
    ///
    void vCloseVirtualJoystickInterface();
};


#endif // VIRTUALJOYSTICKINTERFACE_H
