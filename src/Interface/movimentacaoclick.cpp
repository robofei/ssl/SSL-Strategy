/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "movimentacaoclick.h"

MovimentacaoClick::MovimentacaoClick()
{
    bGravandoPosicoes = false;
    bTrajetosIniciados = false;
    strlistHLabelTabela.clear();
    strlistHLabelTabela << "X [mm]"
                        << "Y [mm]";

    gridLayout = new QGridLayout;

    strlistTextoBotoes << "Iniciar\nTrajeto\nEscolhido"
                       << "Gravar Trajeto"
                       << "Salvar\nTrajeto"
                       << "Carregar\nTrajeto"
                       << "Reset"
                       << "Excluir Ponto";
    strlistIconesBotoes << ":/Imagens/path.png"
                        << ":/Imagens/gravar.png"
                        << ":/Imagens/salvar.png"
                        << ":/Imagens/carregar.png"
                        << ":/Imagens/refresh.png"
                        << "";

    this->setFixedWidth(430);
    vCriaInterface();
    vConectaSlots();
    setLayout(gridLayout);
}

MovimentacaoClick::~MovimentacaoClick()
{
    for (QPushButton* bt : *vtpbBotoes)
    {
        delete bt;
        bt = nullptr;
    }
    delete lbRoboTime;
    delete sbIDRobo;
    delete cbTimeRobo;
    delete tbwTabelaTrajeto;
    delete vtpbBotoes;
    delete gridLayout;
}

void MovimentacaoClick::vRecebeMouseClick(const QVector2D& posMouseCampo,
                                          const Qt::MouseButton& botao)
{
    if (bGravandoPosicoes && botao == Qt::LeftButton)
    {
        vAdicionaPontoTabela(posMouseCampo);
    }
}

void MovimentacaoClick::closeEvent(QCloseEvent*)
{
    vt2dTrajetoAtual.clear();
    emit vEnviaTrajetoAtual(vt2dTrajetoAtual);
}

void MovimentacaoClick::showEvent(QShowEvent*)
{
    vExtraiTrajetoAtual();
}

void MovimentacaoClick::vCriaInterface()
{
    vtpbBotoes = new QVector<QPushButton*>;

    for (int i = 0; i < strlistTextoBotoes.size(); ++i)
    {
        vtpbBotoes->append(new QPushButton(QIcon(strlistIconesBotoes.at(i)),
                                           strlistTextoBotoes.at(i), this));
        vtpbBotoes->last()->setIconSize(QSize(25, 25));

        if (strlistTextoBotoes.at(i) == "Iniciar\nTrajeto\nEscolhido")
        {
            vtpbBotoes->last()->setStyleSheet(QStringLiteral("background-color:"
                                                             "darkgreen"));
            vtpbBotoes->last()->setIconSize(QSize(50, 50));
        }
        if (strlistTextoBotoes.at(i) == "Excluir Ponto")
            vtpbBotoes->last()->setStyleSheet(
                QStringLiteral("background-color:darkred"));
    }

    lbRoboTime = new QLabel(QStringLiteral("ID Robô | Time"), this);
    lbRoboTime->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    lbRoboTime->setFrameStyle(QFrame::Box | QFrame::Raised);
    sbIDRobo = new QSpinBox(this);
    sbIDRobo->setRange(0, globalConfig.robotsPerTeam - 1);
    sbIDRobo->setAlignment(Qt::AlignHCenter);
    cbTimeRobo = new QComboBox(this);
    cbTimeRobo->addItem(QIcon(":/Imagens/Leds/circle_blue.svg"), "Azul",
                        timeAzul);
    cbTimeRobo->addItem(QIcon(":/Imagens/Leds/circle_yellow.svg"), "Amarelo",
                        timeAmarelo);

    tbwTabelaTrajeto = new QTableWidget(0, 2, this);
    tbwTabelaTrajeto->setShowGrid(true);
    tbwTabelaTrajeto->setHorizontalHeaderLabels(strlistHLabelTabela);

    gridLayout->addWidget(vtpbBotoes->at(0), 0, 0, 2, 2);
    gridLayout->addWidget(lbRoboTime, 2, 0, 1, 2);
    gridLayout->addWidget(sbIDRobo, 3, 0);
    gridLayout->addWidget(cbTimeRobo, 3, 1);
    gridLayout->addWidget(vtpbBotoes->at(1), 4, 0, 1, 2);
    gridLayout->addWidget(vtpbBotoes->at(2), 5, 0);
    gridLayout->addWidget(vtpbBotoes->at(3), 5, 1);
    gridLayout->addWidget(vtpbBotoes->at(4), 6, 0, 1, 2);

    gridLayout->addWidget(tbwTabelaTrajeto, 0, 2, 6, 2);
    gridLayout->addWidget(vtpbBotoes->at(5), 6, 2, 1, 2);

    this->setWindowTitle(QStringLiteral("Teste - Movimentação Click"));
}

void MovimentacaoClick::vConectaSlots()
{
    connect(vtpbBotoes->at(0), &QPushButton::clicked, this,
            &MovimentacaoClick::vIniciarTrajeto);
    connect(vtpbBotoes->at(1), &QPushButton::clicked, this,
            &MovimentacaoClick::vComecaGravar);
    connect(vtpbBotoes->at(2), &QPushButton::clicked, this,
            &MovimentacaoClick::vSalvarTrajetos);
    connect(vtpbBotoes->at(3), &QPushButton::clicked, this,
            &MovimentacaoClick::vCarregarTrajetos);
    connect(vtpbBotoes->at(4), &QPushButton::clicked, this,
            &MovimentacaoClick::vResetaTrajetos);
    connect(vtpbBotoes->at(5), &QPushButton::clicked, this,
            &MovimentacaoClick::vRemovePontoTabela);

    connect(sbIDRobo, QOverload<int>::of(&QSpinBox::valueChanged), this,
            &MovimentacaoClick::vAtualizaTabela);
    connect(cbTimeRobo, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &MovimentacaoClick::vAtualizaTabela);
}

void MovimentacaoClick::vExtraiTrajetoTabela()
{
    QVector<int> iAux;
    iAux.clear();

    if (tsDados.iTrajetosRobosMouse.size() < globalConfig.robotsPerTeam * 2)
    {
        tsDados.iTrajetosRobosMouse.clear();
        for (int i = 0; i < globalConfig.robotsPerTeam * 2; ++i)
            tsDados.iTrajetosRobosMouse.append(iAux);
    }

    iAux.append(sbIDRobo->value());
    TeamColor CorTime = static_cast<TeamColor>(
        cbTimeRobo->itemData(cbTimeRobo->currentIndex()).toInt());

    if (CorTime == timeAzul)
        iAux.append(0);
    else if (CorTime == timeAmarelo)
        iAux.append(1);

    QTableWidgetItem* tbwiAux = new QTableWidgetItem();

    for (int j = 0; j < tbwTabelaTrajeto->rowCount(); ++j)
    {
        tbwiAux = tbwTabelaTrajeto->item(j, 0); // X
        iAux.append(tbwiAux->data(Qt::EditRole).toInt());
        tbwiAux = tbwTabelaTrajeto->item(j, 1); // Y
        iAux.append(tbwiAux->data(Qt::EditRole).toInt());
    }

    tsDados.iTrajetosRobosMouse.replace(
        iAux.at(0) + globalConfig.robotsPerTeam * iAux.at(1), iAux);
}

void MovimentacaoClick::vExtraiTrajetoAtual()
{
    QTableWidgetItem* tbwiAux = new QTableWidgetItem();
    int X, Y;
    vt2dTrajetoAtual.clear();

    for (int j = 0; j < tbwTabelaTrajeto->rowCount(); ++j)
    {
        tbwiAux = tbwTabelaTrajeto->item(j, 0); // X
        X = tbwiAux->data(Qt::EditRole).toInt();
        tbwiAux = tbwTabelaTrajeto->item(j, 1); // Y
        Y = tbwiAux->data(Qt::EditRole).toInt();
        vt2dTrajetoAtual.append(QVector2D(X, Y));
    }
    emit vEnviaTrajetoAtual(vt2dTrajetoAtual);
}

void MovimentacaoClick::vLeTrajetoArquivo()
{
    QByteArray byTrajetos;
    byTrajetos.clear();
    byTrajetos.append(fileTrajetosRobos.readAll());

    // iAux      - Indice auxiliar do vetor do trajeto do robo.
    // iFator    - Fator de multiplicação da casa decimal (1, 10, 100, 1000).
    // iSinal    - Sinal do valor (Positivo/Negativo).
    // iPosicao  - Valor da posição X/Y em mm.
    // iTrajetos - Quantidade de trajetos lidos do arquivo.
    int iAux = 0, iFator = 1, iSinal = 1, iPosicao = 0, iTrajetos = 0;

    QVector<int> iTrajetoRobo;
    iTrajetoRobo.clear();

    tsDados.iTrajetosRobosMouse.clear();

    for (int i = 0; i < globalConfig.robotsPerTeam * 2; ++i)
        tsDados.iTrajetosRobosMouse.append(iTrajetoRobo);

    for (int i = 0; i < byTrajetos.size() - 1; ++i)
    {
        iTrajetoRobo.clear();
        while (byTrajetos.at(i) != '\n')
        {
            if (byTrajetos.at(i) != ',')
            {
                if (iAux < 2)
                {
                    iAux++;
                    iTrajetoRobo.append(byTrajetos.at(i) - 48);
                }

                else if (byTrajetos.at(i) == 0x2d) // Sinal de negativo
                {
                    iSinal = -1;
                }

                else
                {
                    int iNumeroCasas = 0, iIndiceAux = i;
                    while (byTrajetos.at(iIndiceAux) != ',')
                    {
                        iIndiceAux++;
                        iNumeroCasas++;
                    }
                    iFator = qCeil(pow(10, iNumeroCasas - 1));
                    iPosicao = 0;
                    for (int j = i; j < i + iNumeroCasas; ++j)
                    {
                        iPosicao += iFator * (byTrajetos.at(j) - 48);
                        iFator /= 10;
                    }
                    iPosicao *= iSinal;

                    iTrajetoRobo.append(iPosicao);

                    i += iNumeroCasas;
                    iSinal = 1;
                }
            }
            i++;

            if (i >= byTrajetos.size() - 1)
                break;
        }

        if (iTrajetos < globalConfig.robotsPerTeam * 2)
        {
            tsDados.iTrajetosRobosMouse.replace(iTrajetos, iTrajetoRobo);
            iTrajetos++;
        }
    }
}

void MovimentacaoClick::vAtribueTrajetoTabela()
{
    int iIDRobo = sbIDRobo->value();
    int CorTime = cbTimeRobo->itemData(cbTimeRobo->currentIndex()).toInt() - 1;

    if (!tsDados.iTrajetosRobosMouse.isEmpty())
    {
        int iNumeroPontos =
            (tsDados.iTrajetosRobosMouse
                 .at(iIDRobo + CorTime * globalConfig.robotsPerTeam)
                 .size() -
             2) /
            2;
        if (iNumeroPontos < 0)
            iNumeroPontos = 0;

        int iLinhaRobo = iIDRobo + CorTime * globalConfig.robotsPerTeam;

        int X = 0;
        int Y = 0;
        int iAuxI = 0;

        tbwTabelaTrajeto->clear();
        tbwTabelaTrajeto->setRowCount(iNumeroPontos);
        tbwTabelaTrajeto->setHorizontalHeaderLabels(strlistHLabelTabela);

        for (int i = 2; i <= iNumeroPontos * 2; i += 2)
        {
            X = tsDados.iTrajetosRobosMouse.at(iLinhaRobo).at(i);
            Y = tsDados.iTrajetosRobosMouse.at(iLinhaRobo).at(i + 1);

            tbwTabelaTrajeto->setItem(iAuxI, 0,
                                      new QTableWidgetItem(QString::number(X)));
            tbwTabelaTrajeto->item(iAuxI, 0)->setData(Qt::EditRole, X);

            tbwTabelaTrajeto->setItem(iAuxI, 1,
                                      new QTableWidgetItem(QString::number(Y)));
            tbwTabelaTrajeto->item(iAuxI, 1)->setData(Qt::EditRole, Y);

            iAuxI++;
        }
    }
}

void MovimentacaoClick::vAdicionaPontoTabela(const QVector2D& ponto)
{
    tbwTabelaTrajeto->setRowCount(tbwTabelaTrajeto->rowCount() + 1);

    int j = tbwTabelaTrajeto->rowCount() - 1;
    int X = qCeil(ponto.x());
    int Y = qCeil(ponto.y());

    tbwTabelaTrajeto->setItem(j, 0, new QTableWidgetItem(QString::number(X)));
    tbwTabelaTrajeto->item(j, 0)->setData(Qt::EditRole, X);

    tbwTabelaTrajeto->setItem(j, 1, new QTableWidgetItem(QString::number(Y)));
    tbwTabelaTrajeto->item(j, 1)->setData(Qt::EditRole, Y);

    vt2dTrajetoAtual.append(QVector2D(X, Y));
    emit vEnviaTrajetoAtual(vt2dTrajetoAtual);
}

void MovimentacaoClick::vRemovePontoTabela()
{
    for (QTableWidgetItem* item : tbwTabelaTrajeto->selectedItems())
    {
        int row = tbwTabelaTrajeto->row(item);

        if (row == -1)
            continue;

        if (row < tbwTabelaTrajeto->rowCount())
        {
            tbwTabelaTrajeto->removeRow(row);
        }
        if (row < vt2dTrajetoAtual.size())
        {
            vt2dTrajetoAtual.removeAt(row);
        }

        emit vEnviaTrajetoAtual(vt2dTrajetoAtual);
    }

    //    int j = tbwTabelaTrajeto->currentRow();
    //    if(j == -1)
    //        return;

    //    tbwTabelaTrajeto->removeRow(j);

    //    if(vt2dTrajetoAtual.size() > j)
    //    {
    //        vt2dTrajetoAtual.removeAt(j);
    //        emit vEnviaTrajetoAtual(vt2dTrajetoAtual);
    //    }
}

void MovimentacaoClick::vComecaGravar()
{
    bool estado = false;

    if (bGravandoPosicoes == false)
    {
        bGravandoPosicoes = true;
        vtpbBotoes->at(1)->setIcon(
            QIcon(QStringLiteral(":/Imagens/gravando.png")));
        vtpbBotoes->at(1)->setText(QStringLiteral("Parar Gravação"));
        estado = false;
        vExtraiTrajetoAtual();
    }
    else
    {
        bGravandoPosicoes = false;
        vtpbBotoes->at(1)->setIcon(QIcon(strlistIconesBotoes.at(1)));
        vtpbBotoes->at(1)->setText(strlistTextoBotoes.at(1));
        estado = true;

        vExtraiTrajetoTabela();
    }

    sbIDRobo->setEnabled(estado);
    cbTimeRobo->setEnabled(estado);
    vtpbBotoes->at(0)->setEnabled(estado);
    vtpbBotoes->at(2)->setEnabled(estado);
    vtpbBotoes->at(3)->setEnabled(estado);
}

void MovimentacaoClick::vSalvarTrajetos()
{
    if (!fileTrajetosRobos.isOpen())
    {
        QDir dir = QDir::current();
        if (!dir.cd("TrajetosTeste"))
        {
            dir.mkdir("TrajetosTeste");
            dir.cd("TrajetosTeste");
        }

        fileTrajetosRobos.setFileName(QFileDialog::getSaveFileName(
            this, tr("Salvar Trajeto"), dir.path() + "/TrajetosRobos.trj",
            tr("TRJ files (*.trj)")));
        fileTrajetosRobos.open(QIODevice::ReadOnly | QIODevice::Text |
                               QIODevice::ReadWrite);
    }

    fileTrajetosRobos.flush();

    if (tsDados.iTrajetosRobosMouse.size() == globalConfig.robotsPerTeam * 2)
    {
        QTextStream stream(&fileTrajetosRobos);

        for (int i = 0; i < globalConfig.robotsPerTeam * 2; ++i)
        {
            for (int j = 0; j < tsDados.iTrajetosRobosMouse.at(i).size(); ++j)
                stream << tsDados.iTrajetosRobosMouse.at(i).at(j) << ",";

            if (i + 1 < globalConfig.robotsPerTeam * 2)
                stream << "\n";
        }
    }

    fileTrajetosRobos.close();
}

void MovimentacaoClick::vCarregarTrajetos()
{
    if (!fileTrajetosRobos.isOpen())
    {
        QDir dir = QDir::current();
        if (!dir.cd("TrajetosTeste"))
        {
            dir.mkdir("TrajetosTeste");
            dir.cd("TrajetosTeste");
        }
        fileTrajetosRobos.setFileName(QFileDialog::getOpenFileName(
            this, tr("Abrir Trajeto"), dir.path() + "/TrajetosRobos.trj",
            tr("TRJ files (*.trj)")));
        fileTrajetosRobos.open(QIODevice::ReadOnly | QIODevice::Text |
                               QIODevice::ReadWrite);
    }

    vLeTrajetoArquivo();
    vAtribueTrajetoTabela();
    vExtraiTrajetoAtual();

    fileTrajetosRobos.close();
}

void MovimentacaoClick::vResetaTrajetos()
{
    QVector<int> iTrajetoRobo;
    iTrajetoRobo.clear();

    tsDados.iTrajetosRobosMouse.clear();

    for (int i = 0; i < globalConfig.robotsPerTeam * 2; ++i)
        tsDados.iTrajetosRobosMouse.append(iTrajetoRobo);

    vSalvarTrajetos();
}

void MovimentacaoClick::vAtualizaTabela(int i)
{
    vAtribueTrajetoTabela();
    vExtraiTrajetoAtual();
}

void MovimentacaoClick::vIniciarTrajeto()
{
    if (bTrajetosIniciados == false)
    {
        bTrajetosIniciados = true;
        vtpbBotoes->at(0)->setStyleSheet(
            QStringLiteral("background-color: darkred"));
        vtpbBotoes->at(0)->setText(QStringLiteral("Parar\nTrajeto\nEscolhido"));

        tsDados.tstTesteAtual = tstMovClick;
        tsDados.iIDRoboTeste = sbIDRobo->value();
        tsDados.bMovimentarRobos = true;
    }
    else
    {
        bTrajetosIniciados = false;
        vtpbBotoes->at(0)->setStyleSheet(
            QStringLiteral("background-color: darkgreen"));
        vtpbBotoes->at(0)->setText(strlistTextoBotoes.at(0));

        tsDados.bMovimentarRobos = false;
        tsDados.tstTesteAtual = tstNenhum;
    }

    vt2dTrajetoAtual.clear();
    emit vEnviaTrajetoAtual(vt2dTrajetoAtual);
    emit vEnviaDadosTestes(tsDados);
}
