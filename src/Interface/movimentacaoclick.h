/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file movimentacaoclick.h
 * @brief Arquivo header da classe do diálogo de configuração do teste de
 * movimentação utilizando o mouse
 * @author Leonardo da Silva Costa
 * @version 1.0
 * @date 2020-08-27
 */
#ifndef MOVIMENTACAOCLICK_H
#define MOVIMENTACAOCLICK_H

#include <QDialog>
#include <QObject>
#include <QtWidgets>
#include <QGridLayout>
#include <QTableWidget>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

/**
 * @brief Classe que implementa o diálogo de configuração do teste de movimentação
 * utilizando o mouse
 * @details Este teste consiste em definir um trajeto clicando no campo, e então, 
 * os robôs seguem este trajeto repetidamente até que o teste seja parado
 * @see TestesRobos
 */
class MovimentacaoClick : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Construtor da classe.
     */
    MovimentacaoClick();

    /**
      *@brief Destrutor da classe.
      */
    ~MovimentacaoClick();

signals:
    /**
     * @brief Sinal que envia os dados do teste de movimentação click.
     * @param dados
     */
    void vEnviaDadosTestes(const DADOS_TESTES &dados);

    /**
     * @brief Sinal que envia o trajeto atual sendo editado na interface.
     * @param trajeto
     */
    void vEnviaTrajetoAtual(const QVector<QVector2D> &trajeto);

public slots:
    /**
     * @brief Recebe o evento de click do mouse.
     * @param posMouseCampo - Posição do mouse já referenciada ao tamanho do campo [mm].
     * @param botao - Botão do mouse que foi pressionado.
     */
    void vRecebeMouseClick(const QVector2D &posMouseCampo, const Qt::MouseButton &botao);

private:
    /**
     * @brief Override do evento que ocorre ao fechar a janela.
     * @details É utilizado para que o #vt2dTrajetoAtual não seja mais
     * desenhado ao fechar a janela do teste.
     */
    void closeEvent(QCloseEvent *) override;

    /**
     * @brief Override do evento que ocorre ao abrir a janela.
     * @details É utilizado para mostrar o #vt2dTrajetoAtual, caso ele não esteja vazio,
     * assim que a janela for aberta.
     */
    void showEvent(QShowEvent *) override;

    /**
     * @brief Cria e atribue os widgets que compoem a interface da janela.
     */
    void vCriaInterface();

    /**
     * @brief Conecta os sinais e slots de todos os widgets utilizados.
     */
    void vConectaSlots();

    /**
     * @brief Extrai o trajeto do widget de tabela (#tbwTabelaTrajeto) e o atribui para
     * o #tsDados.
     */
    void vExtraiTrajetoTabela();

    /**
     * @brief Extrai o trajeto do widget de tabela (#tbwTabelaTrajeto) e o atribui para
     * o #vt2dTrajetoAtual.
     */
    void vExtraiTrajetoAtual();

    /**
     * @brief Lê os trajetos do arquivo especificado.
     */
    void vLeTrajetoArquivo();

    /**
     * @brief Atribui o trajeto do robô selecionado para a tabela (#tbwTabelaTrajeto).
     */
    void vAtribueTrajetoTabela();

    /**
     * @brief Adiciona o ponto fornecido na tabela (#tbwTabelaTrajeto).
     * @param ponto
     */
    void vAdicionaPontoTabela(const QVector2D &ponto);

    DADOS_TESTES tsDados; /**< Armazena os dados do teste. */
    bool bGravandoPosicoes; /**< Indica se um trajeto está sendo gravado. */
    bool bTrajetosIniciados; /**< Indica se os trajetos estão sendo executados (botão
                                                            de iniciar foi pressionado. */
    QVector<QVector2D> vt2dTrajetoAtual; /**< Trajeto atual do robô selecionado. */
    QFile fileTrajetosRobos; /**< Arquivo para salvar/ler os trajetos.*/

    QVector<QPushButton *> *vtpbBotoes; /**< Botões da interface.*/
    QStringList strlistTextoBotoes; /**< Texto dos botões (#vtpbBotoes).*/
    QStringList strlistIconesBotoes; /**< Endereço dos ícones dos botões (#vtpbBotoes). */
    QStringList strlistHLabelTabela; /**< Nome das colunas do widget de tabela
                                                                    (#tbwTabelaTrajeto).*/
    QLabel *lbRoboTime; /**< Label da interface. */
    QSpinBox *sbIDRobo; /**< SpinBox que contém o ID do robô selecionado. */
    QComboBox *cbTimeRobo; /**< ComboBox com o time do robô selecionado. */
    QTableWidget *tbwTabelaTrajeto; /**< Widget de tabela que mostra os pontos do trajeto
                                                                                  atual.*/
    QGridLayout *gridLayout; /**< GridLayout para organizar os widgets na interface. */

private slots:
    /**
     * @brief Remove o ponto selecionado na tabela.
     */
    void vRemovePontoTabela();

    /**
     * @brief Começa a gravar um trajeto.
     * @details A cada clique esquerdo do mouse um ponto é adicionado ao trajeto.
     */
    void vComecaGravar();

    /**
     * @brief Salva os trajetos atuais no arquivo selecionado (#fileTrajetosRobos).
     * @details Ao clicar em salvar, uma janela irá aparecer perguntando em que local e
     * qual o nome do arquivo de trajetos. Todos os arquivos de trajetos são terminados em
     * .trj.
     */
    void vSalvarTrajetos();

    /**
     * @brief Carrega os trajetos do arquivo selecionado (#fileTrajetosRobos).
     * @details De forma similar ao vSalvarTrajetos, uma janela irá aparecer perguntando
     * em que local e qual o nome do arquivo de trajetos a ser lido. Todos os arquivos de
     * trajetos são terminados em .trj.
     */
    void vCarregarTrajetos();

    /**
     * @brief Limpa todos os trajetos e salva os trajetos vazios em um arquivo.
     * @see vSalvarTrajetos.
     */
    void vResetaTrajetos();

    /**
     * @brief Atualiza o trajeto mostrado na tabela (#tbwTabelaTrajeto) quando o ID ou o
     * time do robô mudar.
     * @param i - Serve somente para manter compatibilidade com os sinais do #cbTimeRobo
     * e o #sbIDRobo.
     */
    void vAtualizaTabela(int i);

    /**
     * @brief Envia o sinal para iniciar e parar a execução dos trajetos definidos.
     */
    void vIniciarTrajeto();
};

#endif // MOVIMENTACAOCLICK_H
