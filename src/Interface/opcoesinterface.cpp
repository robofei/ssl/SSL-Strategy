/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "opcoesinterface.h"

OpcoesInterface::OpcoesInterface()
{
    gridLayout = new QGridLayout;

    strlistOpcoes <<
        "Mostrar Direção Robô"         << "Mostrar Área de Segurança"    << "Mostrar Path Planner"           <<
        "Mostrar Posição Timeout"      << "Mostrar Mira da Bola"         << "Mostrar Posição Ball Placement" <<
        "Mostrar Posições Simulação"   << "Mostrar Linhas Goleiro"       << "Mostrar Predições Kalman"       <<
        "Mostrar Liberdade Receptores" << "Destacar Robôs Mines"         << "Destacar Receptor"              <<
        "Desenha Vetor Da Bola"        << "Mostrar Grafo"                << "Reposição com Mouse"            <<
        "Reposição Automática da Bola" << "Saboia"                       << "Criar Logs"                     <<
        "Anti-Aliasing"                << "Destacar Regiões das Roles";

    strlistIcones <<
        ""                             << ""                             << ":/Imagens/path.png"           <<
        ":/Imagens/PosicaoTimeout.png" << ":/Imagens/alvo.png"           << ""                             <<
        ""                             << ""                             << ""                             <<
        ""                             << ""                             << ""                             <<
        ""                             << ""                             << ""                             <<
        ""                             << ":/Imagens/PadreSaboia.png"    << ""                             <<
        ""                             << "";

    vCriaOpcoes();
    setLayout(gridLayout);

}

OpcoesInterface::~OpcoesInterface()
{
    for(QCheckBox *box : *vtckbOpcoes)
    {
        delete box;
        box = nullptr;
    }
    delete vtckbOpcoes;
    delete gridLayout;
}

bool OpcoesInterface::bOpcaoAtivada(const QString &&opcao)
{
    int i = strlistOpcoes.indexOf(opcao);
    if(i != -1)
        return vtckbOpcoes->at(i)->isChecked();
    return false;
}

void OpcoesInterface::vDesmarcarOpcao(const QString &&opcao)
{
    int i = strlistOpcoes.indexOf(opcao);
    if(i != -1)
        vtckbOpcoes->at(i)->setChecked(false);
}

void OpcoesInterface::vSetEnabledOpcao(const QString &&opcao, const bool &&enable)
{
    int i = strlistOpcoes.indexOf(opcao);
    if(i != -1)
        vtckbOpcoes->at(i)->setEnabled(enable);
}

QString OpcoesInterface::strOpcoesAtivas()const
{
    QString strOpcoes = "";

    for (const QCheckBox* cb : *vtckbOpcoes)
    {
        strOpcoes.append( cb->isChecked() ? '1' : '0' );
    }

    if(strOpcoes.length() == vtckbOpcoes->size())
        return strOpcoes;

    qCritical() << Q_FUNC_INFO << "Não foi possível salvar as opções ativas pois"
        "strOpcoes e OpcoesInterface::vtckbOpcoes têm comprimento diferentes";
    return "";
}

void OpcoesInterface::vSetaOpcoesFromString(const QString& opcoes)
{
    if(opcoes.length() != vtckbOpcoes->size())
    {
        qCritical() << Q_FUNC_INFO << " Não foi possível carregar as opções"
            "ativas pois opcoes e OpcoesInterface::vtckbOpcoes têm"
            "comprimento diferentes";

        return;
    }

    for (quint8 i=0; i<vtckbOpcoes->size(); i++)
    {
        vtckbOpcoes->at(i)->setChecked(opcoes.at(i) == QChar('1'));
    }
}

void OpcoesInterface::vRecebeOpcaoMudou(int estado)
{
   emit vOpcaoMudou(sender()->property("text").toString(),
                    estado == 2 ? true : false);
}

void OpcoesInterface::vCriaOpcoes()
{
    vtckbOpcoes = new QVector<QCheckBox*>;

    const int rows = 3;
    int row = 0, col = 0;

    for(int i = 0; i < strlistOpcoes.size(); ++i)
    {
        vtckbOpcoes->append(new QCheckBox);
        vtckbOpcoes->at(i)->setChecked(false);
        vtckbOpcoes->at(i)->setIcon(QIcon(strlistIcones.at(i)));
        vtckbOpcoes->at(i)->setText(strlistOpcoes.at(i));
        gridLayout->addWidget(vtckbOpcoes->at(i), row, col);

        col++;
        if((i+1) % rows == 0)
            row++;
        if(col == rows)
            col = 0;

        connect(vtckbOpcoes->at(i), &QCheckBox::stateChanged, this, &OpcoesInterface::vRecebeOpcaoMudou);
    }

}


