/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef OPCOESINTERFACE_H
#define OPCOESINTERFACE_H

/**
  *
  * @file opcoesinterface.h
  * @brief Gerencia as opções da interface \a OpcoesInterface
  *
 */

#include <QDialog>
#include <QObject>
#include <QtWidgets>
#include <QGridLayout>
#include <QCheckBox>

/**
 * @brief Classe responsável por gerenciar as opções disponíveis na interface
 * em relação à: mostrar ou não alguns desenhos no campo, habilitar/desabilitar algumas
 * funções especiais.
 */
class OpcoesInterface : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Construtor da classe.
     */
    OpcoesInterface();

    /**
     * @brief Destrutor da classe.
     */
    ~OpcoesInterface();

    /**
     * @brief Retorna se a opção fornecida está ou não habilitada.
     * @param opcao - Nome da opção. #strlistOpcoes.
     * @return bool.
     */
    bool bOpcaoAtivada(const QString &&opcao);

    /**
     * @brief vDesmarcarOpcao - Desmarca a opção fornecida.
     * @param opcao - Nome da opção. #strlistOpcoes.
     */
    void vDesmarcarOpcao(const QString &&opcao);

    /**
     * @brief Habilita/Desabilita a opção fornecida.
     * @param opcao - Nome da opção. #strlistOpcoes.
     * @param enable - Determina se a opção deve ser habilitada/desabilitada.
     */
    void vSetEnabledOpcao(const QString &&opcao, const bool &&enable);

    /**
     * @brief Retorna um string com o estado de todas as opções (checada ou não checada)
     *
     * @details O string tem o mesmo número de elementos que 
     * #vtckbOpcoes, a posição de alguma \a i no vetor #vtckbOpcoes equivale ao 
     * estado na  posição \a i desse string. '1' refere-se a checado e '0' 
     * refere-se a não checado.  
     * 
     * @see #vSetaOpcoesFromString
     *
     * @return 
     */
    QString strOpcoesAtivas() const;

    /**
     * @brilf Configura as configurações da Interface 
     *
     * @param opcoes - String com o mesmo formato de #strOpcoesAtivas
     * @see #strOpcoesAtivas
     */
    void vSetaOpcoesFromString(const QString& opcoes);

signals:
    /**
     * @brief Sinal que indica quando qualquer opção muda.
     * @param opcao - Nome da opção que mudou. #strlistOpcoes.
     * @param estadoAtual - Estado atual da opção que mudou.
     */
    void vOpcaoMudou(const QString &opcao, const bool &estadoAtual);

private slots:
    /**
     * @brief Recebe todos os sinais de mudança das opções.
     * @param estado - Estado atual da opção que mudou.
     */
    void vRecebeOpcaoMudou(int estado);

private:
    /**
     * @brief Inicializa todas as opções definidas em #strlistOpcoes.
     */
    void vCriaOpcoes();

    QVector<QCheckBox*> *vtckbOpcoes; /**< Vetor com os objetos da interface. */
    QGridLayout *gridLayout; /**< Objeto do layout. */
    QStringList strlistOpcoes; /**< Lista com os nomes de cada opção. Esta lista deve sempre possuir o mesmo
                                tamamnho da #strlistIcones. */
    QStringList strlistIcones; /**< Lista com os ícones das opções. Caso um ícone não seja desejado
                            basta deixa o respectivo índica com uma string vazia (""). */

};

#endif // OPCOESINTERFACE_H
