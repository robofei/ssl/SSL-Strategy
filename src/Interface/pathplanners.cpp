/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pathplanners.h"

TestePathPlanners::TestePathPlanners(const QSize &campo)
{
    bTesteIniciado = false;

    strlistPrefixoSpins << "X: " << "Y: "        <<
                           "X: " << "Y: "        <<
                           "ID Robô: "           <<
                           "Leaf Size: "         <<
                           "Máximo Iterações: "  <<
                           "Distância Destino: " <<
                           "Distância Avanço: ";
    vtiRangeSpins << QPair<int,int>(-campo.width()/2, campo.width()/2) <<
                     QPair<int,int>(-campo.height()/2, campo.height()/2) <<
                     QPair<int,int>(-campo.width()/2, campo.width()/2) <<
                     QPair<int,int>(-campo.height()/2, campo.height()/2) <<
                     QPair<int,int>(0, globalConfig.robotsPerTeam - 1)   <<
                     QPair<int,int>(1, 500)                    <<
                     QPair<int,int>(60, 30000)                 <<
                     QPair<int,int>(0, 1000)                   <<
                     QPair<int,int>(1, 1000);
    strlistTextoBotoesRadio << "1) Static" << "2) Dynamic"
                            << "3) Antipodal" << "4) Random"
                            << "5) Marking";
    strlistTextoLabels << "Inicio" << "Destino" << "Parâmetros" << "Teste";

    vCriaInterface();
    vConectaSlots();

    setLayout(gridLayout);
    this->setWindowTitle(QStringLiteral("Teste - Path Planners"));
}

TestePathPlanners::~TestePathPlanners()
{
    for(QSpinBox *sb : *vtsbSpinBoxes)
    {
        delete sb;
        sb = nullptr;
    }

    for(QRadioButton *rb : *vtrbBotoesRadio)
    {
        delete rb;
        rb = nullptr;
    }

    for(QLabel *lb : *vtlbLabels)
    {
        delete lb;
        lb = nullptr;
    }
    delete vtsbSpinBoxes;
    delete vtrbBotoesRadio;
    delete vtlbLabels;
    delete pbIniciar;
    delete ckbMovimentar;
    delete pgbProgressoTeste;
    delete gridLayout;
}

void TestePathPlanners::vRecebeMouseClick(const QVector2D &posMouseCampo,
                                          const Qt::MouseButton &botao)
{
    if(this->isVisible() && bTesteIniciado == false)
    {
        if(botao == Qt::RightButton)
        {
            vtsbSpinBoxes->at(2)->setValue(qRound(posMouseCampo.x()));
            vtsbSpinBoxes->at(3)->setValue(qRound(posMouseCampo.y()));
            tsDados.vt2dDestinoTesteRobo = posMouseCampo;
        }
        else if(botao == Qt::LeftButton)
        {
            vtsbSpinBoxes->at(0)->setValue(qRound(posMouseCampo.x()));
            vtsbSpinBoxes->at(1)->setValue(qRound(posMouseCampo.y()));
            tsDados.vt2dPosicaoInicialTeste = posMouseCampo;
        }
    }
}

void TestePathPlanners::vRecebeProgressoTeste(const int &max, const int &atual)
{
    if(pgbProgressoTeste->maximum() != max)
        pgbProgressoTeste->setMaximum(max);
    pgbProgressoTeste->setValue(atual);
}

void TestePathPlanners::vCriaInterface()
{
    gridLayout      = new QGridLayout;
    vtsbSpinBoxes   = new QVector<QSpinBox*>;
    vtrbBotoesRadio = new QVector<QRadioButton *>;
    vtlbLabels      = new QVector<QLabel *>;

    const int startVal[] = {0, 0, 0, 0, 0, 5, 2500, 360, 360};
    for (int n = 0; n < strlistPrefixoSpins.size(); ++n)
    {
        vtsbSpinBoxes->append(new QSpinBox(this));
        vtsbSpinBoxes->last()->setPrefix(strlistPrefixoSpins.at(n));
        vtsbSpinBoxes->last()->setRange(vtiRangeSpins.at(n).first,
                                        vtiRangeSpins.at(n).second);
        vtsbSpinBoxes->last()->setAlignment(Qt::AlignHCenter);
        vtsbSpinBoxes->last()->setValue(startVal[n]);
    }

    for (int n = 0; n < strlistTextoBotoesRadio.size(); ++n)
    {
        vtrbBotoesRadio->append(new QRadioButton(strlistTextoBotoesRadio.at(n), this));
    }
    vtrbBotoesRadio->first()->setChecked(true);

    for (int n = 0; n < strlistTextoLabels.size(); ++n)
    {
        vtlbLabels->append(new QLabel(strlistTextoLabels.at(n), this));
        if(n <= 1)
        {
            vtlbLabels->at(n)->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            vtlbLabels->at(n)->setFrameStyle(QFrame::Box | QFrame::Raised);
        }
    }

    pbIniciar = new QPushButton(QStringLiteral("Iniciar\nTeste"), this);
    pbIniciar->setStyleSheet(QStringLiteral("background-color:darkgreen"));
    ckbMovimentar = new QCheckBox(QStringLiteral("Movimentar Robô"));
    pgbProgressoTeste = new QProgressBar(this);
    pgbProgressoTeste->setFormat("%v");
    pgbProgressoTeste->setMaximum(100);
    pgbProgressoTeste->setValue(0);

    gridLayout->addWidget(pbIniciar,              0, 0, 2, 1);
    gridLayout->addWidget(vtlbLabels->at(0),      0, 1, 1, 2); //Inicio
    gridLayout->addWidget(vtsbSpinBoxes->at(0),   1, 1);
    gridLayout->addWidget(vtsbSpinBoxes->at(1),   1, 2);
    gridLayout->addWidget(vtlbLabels->at(1),      0, 3, 1, 2); //Destino
    gridLayout->addWidget(vtsbSpinBoxes->at(2),   1, 3);
    gridLayout->addWidget(vtsbSpinBoxes->at(3),   1, 4);
    gridLayout->addWidget(vtlbLabels->at(2),      2, 0, 1, 3); //Parâmetros
    gridLayout->addWidget(vtsbSpinBoxes->at(5),   3, 0, 1, 3);
    gridLayout->addWidget(vtsbSpinBoxes->at(6),   4, 0, 1, 3);
    gridLayout->addWidget(vtsbSpinBoxes->at(7),   5, 0, 1, 3);
    gridLayout->addWidget(vtsbSpinBoxes->at(8),   6, 0, 1, 3);
    gridLayout->addWidget(pgbProgressoTeste,      8, 0, 1, 5);
    gridLayout->addWidget(vtlbLabels->at(3),      2, 3, 1, 2); //Testes
    gridLayout->addWidget(vtsbSpinBoxes->at(4),   3, 3, 1, 2);
    gridLayout->addWidget(ckbMovimentar,          4, 3, 1, 2);
    gridLayout->addWidget(vtrbBotoesRadio->at(0), 5, 3, 1, 1);
    gridLayout->addWidget(vtrbBotoesRadio->at(1), 6, 3, 1, 1);
    gridLayout->addWidget(vtrbBotoesRadio->at(2), 5, 4, 1, 1);
    gridLayout->addWidget(vtrbBotoesRadio->at(3), 6, 4, 1, 1);
    gridLayout->addWidget(vtrbBotoesRadio->at(4), 7, 3, 1, 1);
}

void TestePathPlanners::vConectaSlots()
{
    connect(ckbMovimentar, &QCheckBox::toggled,
            this,          &TestePathPlanners::vMovimentarRoboMudou);

    for(int n = 0; n < 4; ++n)
    {
        connect(vtsbSpinBoxes->at(n), QOverload<int>::of(&QSpinBox::valueChanged),
                this, &TestePathPlanners::vPosicaoInicioDestinoMudou);
    }

    connect(vtsbSpinBoxes->at(4), QOverload<int>::of(&QSpinBox::valueChanged),
            this, &TestePathPlanners::vIDRoboMudou);

    for(int n = 5; n < 9; ++n)
    {
        connect(vtsbSpinBoxes->at(n), QOverload<int>::of(&QSpinBox::valueChanged),
                this, &TestePathPlanners::vParametrosRRTMudou);
    }

    for(QRadioButton *rb : *vtrbBotoesRadio)
    {
        connect(rb, &QRadioButton::clicked,
                this, &TestePathPlanners::vTipoTesteMudou);
    }

    connect(pbIniciar, &QPushButton::clicked,
            this, &TestePathPlanners::vIniciarPararTeste);
}

void TestePathPlanners::vMovimentarRoboMudou(bool estado)
{
    tsDados.bMovimentarRobos = estado;
}

void TestePathPlanners::vPosicaoInicioDestinoMudou()
{
    tsDados.vt2dPosicaoInicialTeste = QVector2D(vtsbSpinBoxes->at(0)->value(),
                                                vtsbSpinBoxes->at(1)->value());
    tsDados.vt2dDestinoTesteRobo = QVector2D(vtsbSpinBoxes->at(2)->value(),
                                             vtsbSpinBoxes->at(3)->value());
}

void TestePathPlanners::vIDRoboMudou(int id)
{
    tsDados.iIDRoboTeste = id;
}

void TestePathPlanners::vParametrosRRTMudou()
{
    tsDados.iLeafSizeRRT 		= vtsbSpinBoxes->at(5)->value();
    tsDados.iNumeroIteracoesRRT = vtsbSpinBoxes->at(6)->value();
    tsDados.iDistanciaMinimaRRT = vtsbSpinBoxes->at(7)->value();
    tsDados.fDistanciaAvancoRRT = vtsbSpinBoxes->at(8)->value();
}

void TestePathPlanners::vTipoTesteMudou()
{
    for(int n = 1; n <= vtrbBotoesRadio->size(); ++n)
    {
        if(vtrbBotoesRadio->at(n-1)->isChecked())
        {
            tsDados.iTipoTestePathPlanner = n;
            break;
        }
    }
}

void TestePathPlanners::vIniciarPararTeste()
{
    if(bTesteIniciado == false)
    {
        bTesteIniciado = true;
        pbIniciar->setStyleSheet(QStringLiteral("background-color:darkred"));
        pbIniciar->setText(QStringLiteral("Parar\nTeste"));
        tsDados.tstTesteAtual = tstPathPlanner;
    }
    else
    {
        bTesteIniciado = false;
        pgbProgressoTeste->setValue(0);
        pbIniciar->setStyleSheet(QStringLiteral("background-color:darkgreen"));
        pbIniciar->setText(QStringLiteral("Iniciar\nTeste"));
        tsDados.tstTesteAtual = tstNenhum;
    }
    emit vEnviaDadosTestes(tsDados);
}
