/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TESTEPATHPLANNERS_H
#define TESTEPATHPLANNERS_H

/**
  *
  * @file pathplanners.h
  * @brief Configuração para testes dos path-planners \a TestePathPlanners
  *
 */

#include <QDialog>
#include <QObject>
#include <QGridLayout>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>
#include <QRadioButton>
#include <QCheckBox>
#include <QProgressBar>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

/**
  * @brief Classe que implementa uma interface de configuração para os testes dos 
  * path-planners.
  *
  * Os testes são executados pela classe TestesRobos.
  */
class TestePathPlanners : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Construtor da classe.
     * @param campo Tamanho do campo.
     */
    TestePathPlanners(const QSize &campo);

    /**
      * @brief Destrutor da classe.
      */
    ~TestePathPlanners();

public slots:
    /**
     * @brief Recebe o evento de click do mouse.
     * @param posMouseCampo - Posição do mouse já referenciada ao tamanho do campo [mm].
     * @param botao - Botão do mouse que foi pressionado.
     */
    void vRecebeMouseClick(const QVector2D &posMouseCampo, const Qt::MouseButton &botao);

    /**
     * @brief Recebe a informação do progresso do teste atual sendo executado.
     * @param max Quantidade máxima de repetições do teste.
     * @param atual Quantidade atual de repetições do teste.
     */
    void vRecebeProgressoTeste(const int &max, const int &atual);

signals:
    /**
     * @brief Envia os parâmetros configurados do teste.
     * @param dados Parâmetros do teste dos path-planners.
     */
    void vEnviaDadosTestes(const DADOS_TESTES &dados);

private:
    /**
     * @brief Cria, inicializa e posiciona os widgets utilizados na interface.
     * @details Os widgets utilizados são: #pbIniciar, #vtsbSpinBoxes, #vtrbBotoesRadio,
     * #vtlbLabels, #ckbMovimentar e #pgbProgressoTeste. Eles são posicionados no
     * #gridLayout.
     */
    void vCriaInterface();

    /**
     * @brief Conecta os sinais dos widgets da interface com os slots da classe.
     */
    void vConectaSlots();

    DADOS_TESTES tsDados; /**< Dados do teste dos path-planners.
                                (Destino, ID do robô, tipo de teste e etc).*/
    bool bTesteIniciado; /**< Indica se o teste foi iniciado.*/

    QGridLayout *gridLayout; /**< Layout para posicionar os widgets.*/
    QStringList strlistPrefixoSpins; /**< Lista de nomes dos prefixos dos _spin box_.*/
    QVector<QPair<int, int>> vtiRangeSpins; /**< Intervalo de valores dos _spin box_.*/
    QStringList strlistTextoBotoesRadio; /**< Lista de nomes dos botões.*/
    QStringList strlistTextoLabels; /**< Lista de nomes dos textos dos _labels_.*/
    QPushButton *pbIniciar; /**< Botão de iniciar teste.*/
    QVector<QSpinBox *> *vtsbSpinBoxes; /**< _Spin box's_ utilizados na interface. */
    QVector<QRadioButton *> *vtrbBotoesRadio; /**< _Radio button's_ utilizados.*/
    QVector<QLabel *> *vtlbLabels; /**< _Label's_ utilizados.*/
    QCheckBox *ckbMovimentar; /**< _Check box_ que indica se os robôs devem ser
                                                                           movimentados.*/
    QProgressBar *pgbProgressoTeste; /**< Barra de progresso do teste.*/

private slots:
    /**
     * @brief Slot que recebe o sinal se o #ckbMovimentar mudou de estado.
     * @param estado
     */
    void vMovimentarRoboMudou(bool estado);

    /**
     * @brief Slot que recebe o sinal se os _spin box's_ da posição inicial ou destino
     * mudaram.
     */
    void vPosicaoInicioDestinoMudou();

    /**
     * @brief Slot que recebe o sinal se o _spin box_ de ID do robô mudou.
     * @param id
     */
    void vIDRoboMudou(int id);

    /**
     * @brief Slot que recebe os sinais se algum _spin box_ dos parâmetros do RRT mudar.
     */
    void vParametrosRRTMudou();

    /**
     * @brief Slot que recebe os sinais se os #vtrbBotoesRadio mudaram.
     */
    void vTipoTesteMudou();

    /**
     * @brief Slot que recebe o sinal se o botão #pbIniciar foi pressionado.
     */
    void vIniciarPararTeste();
};

#endif // TESTEPATHPLANNERS_H
