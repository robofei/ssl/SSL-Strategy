/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "visibilitygraph.h"

VisibilityGraph::VisibilityGraph(int _iResolucaoCirculo)
{
    iResolucaoCirculo = _iResolucaoCirculo;
    lineLinhaRestricao = QLine();
    bConsideraAreaDefesa = false;
}

VisibilityGraph::VisibilityGraph()
{
    iResolucaoCirculo = iAnguloGrafo;
    lineLinhaRestricao = QLine();
    bConsideraAreaDefesa = false;
}

void VisibilityGraph::vCriaObjeto(const QVector2D& vt2dCentro,
                                  const TipoObjetoGrafo& otgObjeto,
                                  const QSize& tamanhoCampo,
                                  const int& iRaioObj)
{
    // Variável auxiliar que será rotacionada para gerar os pontos do círculo
    QVector2D Ponto = vt2dCentro;
    QVector<QVector4D> objetosProximos;
    objetosProximos.clear();
    int iRaioOutroObj = 0;
    QVector2D centroOutroObj;

    for (quint16 n = 0; n < vt4dCentroObjetos.size(); ++n)
    {
        centroOutroObj = vt4dCentroObjetos.at(n).toVector2D();
        iRaioOutroObj = vt4dCentroObjetos.at(n).w();

        if (centroOutroObj.distanceToPoint(vt2dCentro) <
                iRaioObj + iRaioOutroObj &&
            centroOutroObj.distanceToPoint(vt2dCentro) >=
                globalConfig.robotDiameter / 2)
            objetosProximos.append(vt4dCentroObjetos.at(n));
    }

    Ponto.setX(vt2dCentro.x() + iRaioObj);

    bool centroAreaDefesa;
    bool pontoForaCampo;
    bool adicionaPonto;

    for (quint16 graus = 0; graus < 360; graus += iResolucaoCirculo)
    {
        Ponto = Auxiliar::vt2dRotaciona(vt2dCentro, Ponto, iResolucaoCirculo);
        centroAreaDefesa = bDentroAreaDefesa(Ponto);
        pontoForaCampo = Auxiliar::bPontoForaCampo(tamanhoCampo, Ponto,
                                                   globalConfig.fieldOffset);
        adicionaPonto = true;

        for (quint16 n = 0; n < objetosProximos.size(); ++n)
        {
            centroOutroObj = objetosProximos.at(n).toVector2D();
            iRaioOutroObj = objetosProximos.at(n).w();

            if (Ponto.distanceToPoint(centroOutroObj) < iRaioOutroObj &&
                centroOutroObj.distanceToPoint(vt2dCentro) >
                    globalConfig.robotDiameter / 2)
            {
                adicionaPonto = false;
                break;
            }
        }

        if (!centroAreaDefesa && !pontoForaCampo && adicionaPonto)
        {
            // Os dois vetores são sincronizados, assim sabe-se qual o centro de
            // cada vértice
            vt3dPaisObjetos.append(QVector3D(vt2dCentro, otgObjeto));
            vt2dPontosObjetos.append(Ponto);
        }
    }
}

void VisibilityGraph::vCriaAreaDefesa(const AreaDefesa& Area,
                                      const QSize& tamanhoCampo)
{
    QVector<QVector2D> pontosArea;
    pontosArea.append(Area.SuperiorFundo);
    pontosArea.append(Area.SuperiorLinha);
    pontosArea.append((Area.SuperiorLinha + Area.InferiorLinha) / 2);
    pontosArea.append(Area.InferiorLinha);
    pontosArea.append(Area.InferiorFundo);

    bool pontoForaCampo = false;
    bool pontoDentroAreaRestricao = false;

    for (quint8 n = 0; n < pontosArea.size(); ++n)
    {
        vt4dCentroObjetos.append(
            QVector4D(pontosArea.at(n), LINHA_DEFESA, iRaioAreaDefesa));

        QVector2D ponto = pontosArea.at(n);
        ponto.setX(ponto.x() + iRaioAreaDefesa);

        for (quint16 graus = 0; graus < 360; graus += 45)
        {
            ponto = Auxiliar::vt2dRotaciona(pontosArea.at(n), ponto, 45);
            pontoForaCampo = Auxiliar::bPontoForaCampo(tamanhoCampo, ponto);
            pontoDentroAreaRestricao = bDentroAreaRestricao(ponto);

            if (!pontoForaCampo && !pontoDentroAreaRestricao)
            {
                vt3dPaisObjetos.append(
                    QVector3D(pontosArea.at(n), LINHA_DEFESA));
                vt2dPontosObjetos.append(ponto);
            }
        }
    }
}

int VisibilityGraph::iRaioObjeto(const TipoObjetoGrafo& _obj)
{
    switch (_obj)
    {
    case ALIADO: {
        return globalConfig.safety.ally;
    }
    break;
    case OPONENTE: {
        return globalConfig.safety.opponent;
    }
    break;
    case BOLA: {
        return globalConfig.safety.ball.normal;
    }
    break;
    case LINHA_DEFESA: {
        return iRaioAreaDefesa;
    }
    break;
    case DESTINO: {
        return 0;
    }
    break;
    case AGRUPAMENTO: {
        return -1;
    }
    break;
    default:
        return -2;
        break;
    }
}

int VisibilityGraph::iRaioAgrupamento(const QVector2D& centro)
{
    for (int i = 0; i < circAgrupamentos.size(); ++i)
    {
        if (centro.distanceToPoint(circAgrupamentos.at(i).first) < 1)
            return circAgrupamentos.at(i).second;
    }
    return 0;
}

void VisibilityGraph::vAdicionaAresta(const bool& aresta, const int& i,
                                      const int& j)
{
    ArestaVG aux;

    aux.pos1 = vt2dPontosObjetos.at(i);
    aux.pos2 = vt2dPontosObjetos.at(j);

    aux.arestaConectada = aresta;
    aux.indice = j;
    vgMatrizAdjacencia[i][j] = aux;

    aux.pos1 = vt2dPontosObjetos.at(j);
    aux.pos2 = vt2dPontosObjetos.at(i);
    aux.indice = i;
    vgMatrizAdjacencia[j][i] = aux;
}

void VisibilityGraph::vEncontraObjetosRegiaoAtiva(
    const AmbienteCampo* acAmbiente, const int& iRoboID,
    QVector<QVector3D>& vt3dAliados, QVector<QVector3D>& vt3dOponentes)
{
    const Robo* robo = acAmbiente->allies->getPlayer(iRoboID);

    vt3dAliados.clear();
    vt3dOponentes.clear();
    vt3dPaisObjetos.clear();
    vt2dPontosObjetos.clear();

    // Posicao inicial
    vt2dPontosObjetos.append(robo->vt2dPosicaoAtual());
    vt3dPaisObjetos.append(QVector3D(robo->vt2dPosicaoAtual(), ALIADO));

    vt2dPosicaoInicial = robo->vt2dPosicaoAtual();
    vt2dPosicaoFinal = robo->vt2dDestino();

    int iRegiaoInicial = Auxiliar::iCalculaRegiaoPonto(
        vt2dPosicaoInicial, acAmbiente->geoCampo->szField().width(),
        globalConfig.fieldDivisionWidth);
    int iRegiaoFinal = Auxiliar::iCalculaRegiaoPonto(
        vt2dPosicaoFinal, acAmbiente->geoCampo->szField().width(),
        globalConfig.fieldDivisionWidth);

    // Inverte as regiões caso a inicial seja maior que a final
    if (iRegiaoInicial > iRegiaoFinal)
    {
        iRegiaoInicial = iRegiaoInicial + iRegiaoFinal;
        iRegiaoFinal = iRegiaoInicial - iRegiaoFinal;
        iRegiaoInicial = iRegiaoInicial - iRegiaoFinal;
    }

    if (robo->jJogadaAtual() == FreeKick_Defesa_Delta ||
        robo->jJogadaAtual() == Normal_Defensor_Delta ||
        robo->jJogadaAtual() == Stop_Defensor_Delta)
        vt3dAliados = acAmbiente->vt3dPegaDestinosAliados(
            iRegiaoInicial, iRegiaoFinal, iRoboID);
    else
        vt3dAliados = acAmbiente->vt3dPegaPosicaoTodosObjetos(
            otAliado, iRegiaoInicial, iRegiaoFinal);

    vt3dOponentes = acAmbiente->vt3dPegaPosicaoTodosObjetos(
        otOponente, iRegiaoInicial, iRegiaoFinal);
}

void VisibilityGraph::vOrganizaObjetosRegiaoAtiva(
    const AmbienteCampo* acAmbiente, const int& iRoboID)
{
    QVector<QVector3D> vt3dAliados, vt3dOponentes;
    vt3dAliados.clear();
    vt3dOponentes.clear();

    QVector2D posBola = acAmbiente->vt2dPosicaoBola();
    const Robo* robo = acAmbiente->allies->getPlayer(iRoboID);

    vEncontraObjetosRegiaoAtiva(acAmbiente, iRoboID, vt3dAliados,
                                vt3dOponentes);

    QVector2D pontoInicial = robo->vt2dPosicaoAtual();
    QVector2D pontoDestino = robo->vt2dDestino();
    pqObjetos pqObjetosAux = pqObjetos();

    // Adiciona os objetos numa distancia de 1.5m da reta entre o inicio e o
    // destino
    vAnalisaObstaculos(vt3dAliados, pontoInicial, pontoDestino, pqObjetosAux);
    vAnalisaObstaculos(vt3dOponentes, pontoInicial, pontoDestino, pqObjetosAux);

    if (robo->bPegaIgnorarBola() == bConsideraBola &&
        Auxiliar::bChecaInterseccaoObjetosLinha(pontoInicial, pontoDestino,
                                                1000, posBola) == true)
    {
        pqObjetosAux.push(
            ObjetoVG(posBola, posBola.distanceToPoint(pontoInicial), BOLA));
    }

    // Retira os centros dos objetos na ordem
    vt4dCentroObjetos.clear();
    int iTamanhoInicial = pqObjetosAux.size();
    //    QFile fileObj;
    //    fileObj.setFileName("vt3dCentrosObjetos.csv");
    //    fileObj.open(QIODevice::WriteOnly);
    //    QTextStream stream(&fileObj);

    for (quint16 n = 0; n < iTamanhoInicial; ++n)
    {
        vt4dCentroObjetos.append(
            QVector4D(pqObjetosAux.top().vt2dP, pqObjetosAux.top().otgTipo,
                      iRaioObjeto(pqObjetosAux.top().otgTipo)));
        //        stream << QString::number(vt3dCentroObjetos.constLast().x())
        //        << ";"
        //<< QString::number(vt3dCentroObjetos.constLast().y())
        //               << Qt::endl;

        pqObjetosAux.pop();
    }
    //    fileObj.close();
}

void VisibilityGraph::vDeterminaAgrupamentos()
{
    quint16 nObjetos = vt4dCentroObjetos.size();
    QVector<int> agrupamentos;
    agrupamentos.clear();
    //    std::vector<std::vector<float>> matrizDistancias(nObjetos,
    //                                          std::vector<float>(nObjetos));
    //    for(quint16 c = 0; c < nObjetos; ++c)
    //    {
    //        for(quint16 l = c + 1; l < nObjetos; ++l)
    //        {
    //           matrizDistancias[c][l] =
    //           vt3dCentroObjetos.at(c).distanceToPoint(
    //                                            vt3dCentroObjetos.at(l));
    //        }
    //    }
    if (nObjetos > 0)
    {
        agrupamentos.append(0);
        agrupamentos.append(-1);
        for (quint16 c = 1; c < nObjetos; ++c)
        {
            while (agrupamentos.contains(c) && c < nObjetos - 1)
                c++;
            if (!agrupamentos.contains(c))
                agrupamentos.append(c);
            vEncontraProximo(agrupamentos, c);
            agrupamentos.append(-1);
        }
        vCalculaAgrupamentos(agrupamentos);
    }
}

void VisibilityGraph::vEncontraProximo(QVector<int>& agrupamentos, int c)
{
    quint16 nObjetos = vt4dCentroObjetos.size();
    for (quint16 l = c + 1; l < nObjetos; ++l)
    {
        if (vt4dCentroObjetos.at(c).toVector2D().distanceToPoint(
                vt4dCentroObjetos.at(l).toVector2D()) < DIST_AGRUPAMENTO)
        {
            if (!agrupamentos.contains(l))
                agrupamentos.append(l);
            vEncontraProximo(agrupamentos, l);
        }
    }
}

void VisibilityGraph::vCalculaAgrupamentos(const QVector<int>& g)
{
    QVector2D pt;
    int count;
    QVector<QVector4D> pts;
    QVector<QVector4D> novosObstaculos;
    circAgrupamentos.clear();
    novosObstaculos.clear();

    for (quint16 i = 0; i < g.size(); ++i)
    {
        pt = QVector2D(0, 0);
        count = 0;
        pts.clear();

        for (; g.at(i) != -1; ++i)
        {
            pt += vt4dCentroObjetos.at(g.at(i)).toVector2D();
            pts.append(vt4dCentroObjetos.at(g.at(i)));
            count++;
        }

        if (count > 1)
        {
            pt = pt / count; // Centro
            float raio = 0;
            for (int n = 0; n < pts.size(); ++n)
            {
                raio = qMax(raio, pt.distanceToPoint(pts.at(n).toVector2D()));
            }
            raio +=
                qMax(globalConfig.safety.opponent, globalConfig.safety.ally);

            // Só cria agrupamentos que não envolvem a origem e o destino
            if (pt.distanceToPoint(vt2dPosicaoFinal) >= raio &&
                pt.distanceToPoint(vt2dPosicaoInicial) >= raio)
            {
                circAgrupamentos.append(QPair<QVector2D, float>(pt, raio));
                novosObstaculos.append(QVector4D(pt, AGRUPAMENTO, raio));
            }
            else
            {
                for (int n = 0; n < pts.size(); ++n)
                {
                    novosObstaculos.append(pts.at(n));
                }
            }
        }
        else if (count == 1)
        {
            novosObstaculos.append(pts.constLast());
        }
    }
    vt4dCentroObjetos.clear();
    vt4dCentroObjetos.append(novosObstaculos);
}

void VisibilityGraph::vCriaObjetosRegiaoAtiva(const AmbienteCampo* acAmbiente,
                                              const int& iRoboID)
{
    const Robo* robo = acAmbiente->allies->getPlayer(iRoboID);
    vOrganizaObjetosRegiaoAtiva(acAmbiente, iRoboID);
    vDeterminaAgrupamentos();

    // Nao adiciona o proprio robo caso so exista ele
    QVector2D centroObj;
    QSize tamanhoCampo = acAmbiente->geoCampo->szField();
    TipoObjetoGrafo tipoObj;
    QVector<QVector4D> removerCentros;
    removerCentros.clear();
    if (vt4dCentroObjetos.size() > 0 || bConsideraAreaDefesa == true)
    {
        // Cria os circulos entorno de cada objeto
        for (quint16 n = 0; n < vt4dCentroObjetos.size(); ++n)
        {
            // Nao cria o objeto se ele nao for o proprio robo e estiver dentro
            // da area do penalti, com excecao da bola,
            // Se a bola esta na lista de objetos considerados ela sempre sera
            // criada
            centroObj = vt4dCentroObjetos.at(n).toVector2D();
            bool proprioRobo =
                robo->vt2dPosicaoAtual().distanceToPoint(centroObj) < 1;

            if ((bDentroAreaDefesa(centroObj) == true && proprioRobo == true) ||
                bDentroAreaDefesa(centroObj) == false ||
                static_cast<TipoObjetoGrafo>(vt4dCentroObjetos.at(n).z()) ==
                    BOLA)
            {
                tipoObj =
                    static_cast<TipoObjetoGrafo>(vt4dCentroObjetos.at(n).z());
                vCriaObjeto(vt4dCentroObjetos.at(n).toVector2D(), tipoObj,
                            tamanhoCampo, vt4dCentroObjetos.at(n).w());
            }
            else
            {
                if (vt4dCentroObjetos.indexOf(vt4dCentroObjetos.at(n)) != -1)
                    removerCentros.append(vt4dCentroObjetos.at(n));
            }
        }

        foreach (QVector4D centro, removerCentros)
        {
            vt4dCentroObjetos.removeAt(vt4dCentroObjetos.indexOf(centro));
        }

        if (bConsideraAreaDefesa == true)
        {
            if (bLadoDefesaUsado == true) // Lado aliado
                vCriaAreaDefesa(AreaRestricaoAliado, tamanhoCampo);
            else
                vCriaAreaDefesa(AreaRestricaoOponente, tamanhoCampo);
        }
    }
    else
    {
        vt4dCentroObjetos.clear();
    }

    // Destino
    // Desloca-se o ponto para que mais tarde seja possível calcular um raio de
    // seguranca para evitar interseccoes O raio utilizado é o maior entre robô
    // aliado e oponente
    QVector2D vt2dPosicaoObjeto = robo->vt2dDestino();
    globalConfig.safety.ally > globalConfig.safety.opponent
        ? vt2dPosicaoObjeto.setX(vt2dPosicaoObjeto.x() +
                                 globalConfig.safety.ally)
        : vt2dPosicaoObjeto.setX(vt2dPosicaoObjeto.x() +
                                 globalConfig.safety.opponent);

    vt2dPontosObjetos.append(robo->vt2dDestino());
    vt3dPaisObjetos.append(QVector3D(vt2dPosicaoObjeto, DESTINO));
}

void VisibilityGraph::vSetaAreasDefesa(const AmbienteCampo* acAmbiente)
{
    QVector2D P1, // Canto superior esquerdo
        P2,       // Canto superior direito
        P3,       // Canto inferior esquerdo
        P4;       // Canto inferior direito
    QSize tamanhoCampo;
    tamanhoCampo = acAmbiente->geoCampo->szField();
    int ladoAliado = acAmbiente->allies->iGetSide();
    int larguraDefesa = acAmbiente->geoCampo->szDefenseArea().height(),
        profundidadeDefesa = acAmbiente->geoCampo->szDefenseArea().width();

    // Seta a area do penalty aliado
    P1.setX(tamanhoCampo.width() / 2 * ladoAliado);
    P1.setY(larguraDefesa / 2);
    P2.setX(P1.x() - (profundidadeDefesa * ladoAliado));
    P2.setY(P1.y());
    P3.setX(P1.x());
    P3.setY(-P1.y());
    P4.setX(P2.x());
    P4.setY(P3.y());

    AreaDefesaAliado.SuperiorFundo = P1;
    AreaDefesaAliado.SuperiorLinha = P2;
    AreaDefesaAliado.InferiorFundo = P3;
    AreaDefesaAliado.InferiorLinha = P4;

    // Seta a area do penalty oponente
    P1.setX(P1.x() * -1);
    P2.setX(P2.x() * -1);
    P3.setX(P3.x() * -1);
    P4.setX(P4.x() * -1);

    AreaDefesaOponente.SuperiorFundo = P1;
    AreaDefesaOponente.SuperiorLinha = P2;
    AreaDefesaOponente.InferiorFundo = P3;
    AreaDefesaOponente.InferiorLinha = P4;
}

void VisibilityGraph::vSetaAreasRestricao(const AmbienteCampo* acAmbiente)
{
    QVector2D P1, // Canto superior esquerdo
        P2,       // Canto superior direito
        P3,       // Canto inferior esquerdo
        P4;       // Canto inferior direito
    QSize tamanhoCampo;
    tamanhoCampo = acAmbiente->geoCampo->szField();
    int ladoAliado = acAmbiente->allies->iGetSide();
    int larguraDefesa = acAmbiente->geoCampo->szDefenseArea().height(),
        profundidadeDefesa = acAmbiente->geoCampo->szDefenseArea().width();

    // Seta a area do penalty aliado
    P1.setX(tamanhoCampo.width() / 2 * ladoAliado);
    P1.setY(larguraDefesa / 2 + globalConfig.distanceToDefenseArea);
    P2.setX(P1.x() -
            ((profundidadeDefesa + globalConfig.distanceToDefenseArea) * ladoAliado));
    P2.setY(P1.y());
    P3.setX(P1.x());
    P3.setY(-P1.y());
    P4.setX(P2.x());
    P4.setY(P3.y());

    AreaRestricaoAliado.SuperiorFundo = P1;
    AreaRestricaoAliado.SuperiorLinha = P2;
    AreaRestricaoAliado.InferiorFundo = P3;
    AreaRestricaoAliado.InferiorLinha = P4;

    // Seta a area do penalty oponente
    P1.setX(P1.x() * -1);
    P2.setX(P2.x() * -1);
    P3.setX(P3.x() * -1);
    P4.setX(P4.x() * -1);

    AreaRestricaoOponente.SuperiorFundo = P1;
    AreaRestricaoOponente.SuperiorLinha = P2;
    AreaRestricaoOponente.InferiorFundo = P3;
    AreaRestricaoOponente.InferiorLinha = P4;
}

void VisibilityGraph::vSetaTravesGol(const AmbienteCampo* acAmbiente)
{
    QVector2D P1, // Canto superior esquerdo
        P2,       // Canto superior direito
        P3,       // Canto inferior esquerdo
        P4;       // Canto inferior direito
    QSize tamanhoCampo;
    tamanhoCampo = acAmbiente->geoCampo->szField();
    int ladoAliado = acAmbiente->allies->iGetSide();
    float larguraGol = acAmbiente->geoCampo->szGoal().height() +
                       globalConfig.robotDiameter / 2,
          profundidadeGol = acAmbiente->geoCampo->szGoal().width() * 0.8;

    // Seta as traves do gol aliado
    P1.setX((tamanhoCampo.width() / 2 - globalConfig.robotDiameter) *
            ladoAliado);
    P1.setY(larguraGol / 2);
    P2.setX(P1.x() +
            ((profundidadeGol + globalConfig.robotDiameter) * ladoAliado));
    P2.setY(P1.y());
    P3.setX(P1.x());
    P3.setY(-larguraGol / 2);
    P4.setX(P2.x());
    P4.setY(P3.y());

    TravesGolAliado.SuperiorFundo = P1;
    TravesGolAliado.SuperiorLinha = P2;
    TravesGolAliado.InferiorFundo = P3;
    TravesGolAliado.InferiorLinha = P4;

    // Seta as traves do gol oponente
    P1.setX(P1.x() * -1);
    P2.setX(P2.x() * -1);
    P3.setX(P3.x() * -1);
    P4.setX(P4.x() * -1);

    TravesGolOponente.SuperiorFundo = P1;
    TravesGolOponente.SuperiorLinha = P2;
    TravesGolOponente.InferiorFundo = P3;
    TravesGolOponente.InferiorLinha = P4;
}

float VisibilityGraph::fDeterminaDistanciaMinima(const int& raio1,
                                                 const int& raio2,
                                                 const int& vertice1,
                                                 const int& vertice2,
                                                 bool& mesmoObjeto)
{
    float distMin = globalConfig.robotDiameter;
    float distanciaMinimaRobo =
        qFloor(raio1 * qCos(qDegreesToRadians(iResolucaoCirculo / 2.0)));
    float distObj1_2 = vt2dPontosObjetos.at(vertice1).distanceToPoint(
        vt2dPontosObjetos.at(vertice2));

    mesmoObjeto = false;
    if (vt3dPaisObjetos.at(vertice1).distanceToPoint(
            vt3dPaisObjetos.at(vertice2)) < 1)
    {
        // Quando esta conectando os vertices do proprio objeto
        distMin = distanciaMinimaRobo * 0.95;
        mesmoObjeto = true;
    }
    else if (raio1 == raio2)
    {
        // Para objetos de mesmo raio
        distMin = raio2 * 0.9;
    }
    else if (distObj1_2 < qMin(raio1, raio2))
    {
        // Para objetos diferentes, de raios diferentes e uma distancia entre
        // vertices pequena
        distMin = distObj1_2;
    }
    else
    {
        // Para o resto
        distMin = qMin(raio1, raio2);
        if (distMin < 1 && distObj1_2 < 2 * globalConfig.robotDiameter)
            distMin = qMax(raio1, raio2) / 2;
        else if (distMin < 1)
            distMin = qMax(raio1, raio2);
    }

    return distMin;
}

bool VisibilityGraph::bCruzaTravesGol(const int& vertice1, const int& vertice2)
{
    bool cruzaGol = false;
    const int v1x = vt2dPontosObjetos.at(vertice1).x(),
              v2x = vt2dPontosObjetos.at(vertice2).x();
    // Se o ponto esta alem do fim de campo, so ira conecta-lo se ele nao cruzar
    // o gol
    if (qAbs(v1x) > qAbs(AreaDefesaAliado.SuperiorFundo.x()) ||
        qAbs(v2x) > qAbs(AreaDefesaAliado.SuperiorFundo.x()))
    {
        if (bInterseccaoLinhaTravesGol(vt2dPontosObjetos.at(vertice2),
                                       vt2dPontosObjetos.at(vertice1)) == true)
        {
            cruzaGol = true;
        }
    }
    return cruzaGol;
}

void VisibilityGraph::vConectaProprioObjeto(const int& raioObj,
                                            const int& verticeA,
                                            const int& verticeB,
                                            const TipoObjetoGrafo& tipo)
{
    QVector2D posA = vt2dPontosObjetos.at(verticeA),
              posB = vt2dPontosObjetos.at(verticeB);
    float distMinima =
        qCeil(2 * raioObj * qSin(qDegreesToRadians(iResolucaoCirculo / 2.0)));

    if (tipo == LINHA_DEFESA)
        distMinima = qCeil(2 * raioObj * qSin(qDegreesToRadians(45 / 2.0)));

    float distAB = posA.distanceToPoint(posB);
    // Os IFs estão intercalados para evitar cálculos desnecessários
    // A ultima condição é a mais lenta, e por conta disso, só sera checada caso
    // todas as outras sejam verdadeiras.
    if (distAB <= distMinima)
    {
        bool cruzaGol = bCruzaTravesGol(verticeA, verticeB);
        if (cruzaGol == false)
            vAdicionaAresta(true, verticeA, verticeB);
    }
}

void VisibilityGraph::vConectaObjetoDestino(const int& verticeA,
                                            const int& verticeB)
{
    QVector2D posA = vt2dPontosObjetos.at(verticeA),
              posB = vt2dPontosObjetos.at(verticeB);
    bool cruzaGol = bCruzaTravesGol(verticeA, verticeB);
    // Os IFs estão intercalados para evitar cálculos desnecessários
    // A ultima condição é a mais lenta, e por conta disso, só sera checada caso
    // todas as outras sejam verdadeiras.
    if (cruzaGol == false)
    {
        bool cruzaLinhaRestricao = bInterseccaoLinhaLinhaRestricao(posA, posB);

        if (cruzaLinhaRestricao == false)
        {
            bool cruzaDefesa =
                bConsideraAreaDefesa && bInterseccaoLinhaAreaDefesa(posA, posB);
            if (cruzaDefesa == false)
            {
                bool colisao = Auxiliar::bChecaInterseccaoObjetosLinha(
                    posA, posB, vt4dCentroObjetos);
                if (colisao == false)
                    vAdicionaAresta(true, verticeA, verticeB);
            }
        }
    }
}

void VisibilityGraph::vConectaObjetosDiferentes(const int& RoboA,
                                                const int& RoboB)
{
    QVector2D posA = vt2dPontosObjetos.at(RoboA),
              posB = vt2dPontosObjetos.at(RoboB);
    bool cruzaGol = bCruzaTravesGol(RoboA, RoboB);
    // Os IFs estão intercalados para evitar cálculos desnecessários
    // A ultima condição é a mais lenta, e por conta disso, só sera checada
    // caso todas as outras sejam verdadeiras.
    if (cruzaGol == false)
    {
        bool cruzaLinhaRestricao = bInterseccaoLinhaLinhaRestricao(posA, posB);

        if (cruzaLinhaRestricao == false)
        {
            bool cruzaDefesa =
                bConsideraAreaDefesa && bInterseccaoLinhaAreaDefesa(posA, posB);

            if (cruzaDefesa == false)
            {
                float
                    distVertices = posA.distanceToPoint(posB),
                    distPais =
                        vt3dPaisObjetos.at(RoboA).toVector2D().distanceToPoint(
                            vt3dPaisObjetos.at(RoboB).toVector2D());
                bool verticeConectado = (distVertices >= distPais);

                if (verticeConectado)
                {
                    bool colisao = Auxiliar::bChecaInterseccaoObjetosLinha(
                        posA, posB, vt4dCentroObjetos);
                    if (colisao == false)
                    {
                        vAdicionaAresta(true, RoboA, RoboB);
                    }
                }
            }
        }
    }
}

void VisibilityGraph::vConectaObjetos(const int& RoboA, const int& RoboB,
                                      const TipoObjetoGrafo& tipoA,
                                      const TipoObjetoGrafo& tipoB)
{
    int iRaioObj = iRaioObjeto(tipoA), iRaioObj2 = iRaioObjeto(tipoB);
    if (iRaioObj == -1)
        iRaioObj = iRaioAgrupamento(vt3dPaisObjetos.at(RoboA).toVector2D());
    if (iRaioObj2 == -1)
        iRaioObj2 = iRaioAgrupamento(vt3dPaisObjetos.at(RoboB).toVector2D());

    float distMin;
    bool bMesmoObjeto = false;

    distMin = fDeterminaDistanciaMinima(iRaioObj, iRaioObj2, RoboA, RoboB,
                                        bMesmoObjeto);

    if (bMesmoObjeto == true)
        vConectaProprioObjeto(iRaioObj, RoboA, RoboB, tipoA);
    else if (tipoA == DESTINO || tipoB == DESTINO)
        vConectaObjetoDestino(RoboA, RoboB);
    else
        vConectaObjetosDiferentes(RoboA, RoboB);
}

void VisibilityGraph::vAnalisaObstaculos(const QVector<QVector3D>& obstaculos,
                                         const QVector2D& P1,
                                         const QVector2D& P2,
                                         pqObjetos& pqRegAtiva)
{
    foreach (QVector3D objeto, obstaculos)
    {
        if (Auxiliar::bChecaInterseccaoObjetosLinha(
                P1, P2, 1000, objeto.toVector2D()) == true)
        {
            pqRegAtiva.push(ObjetoVG(objeto.toVector2D(),
                                     objeto.toVector2D().distanceToPoint(P1),
                                     static_cast<TipoObjetoGrafo>(objeto.z())));
        }
    }
}

bool VisibilityGraph::bDentroAreaDefesa(const QVector2D& ponto,
                                        const bool& jaSetou)
{
    if (bGoleiro == true) // Se for o goleiro, nao precisa checar isto
        return false;
    if (bConsideraAreaDefesa == false && jaSetou == true)
        return false;
    return (Auxiliar::bPontoContidoRetangulo(ponto,
                                             AreaDefesaAliado.SuperiorFundo,
                                             AreaDefesaAliado.InferiorLinha) ||
            Auxiliar::bPontoContidoRetangulo(ponto,
                                             AreaDefesaOponente.SuperiorFundo,
                                             AreaDefesaOponente.InferiorLinha));
}

bool VisibilityGraph::bDentroAreaRestricao(const QVector2D& ponto)
{
    if (bGoleiro == true)
        return false;
    return Auxiliar::bPontoContidoRetangulo(
        ponto, AreaRestricaoOponente.SuperiorFundo,
        AreaRestricaoOponente.InferiorLinha);
}

bool VisibilityGraph::bInterseccaoLinhaAreaDefesa(const QVector2D& P1,
                                                  const QVector2D& P2)
{
    AreaDefesa area;
    if (bLadoDefesaUsado == true) // Lado aliado
    {
        area.InferiorFundo = AreaDefesaAliado.InferiorFundo;
        area.InferiorLinha = AreaDefesaAliado.InferiorLinha;
        area.SuperiorFundo = AreaDefesaAliado.SuperiorFundo;
        area.SuperiorLinha = AreaDefesaAliado.SuperiorLinha;
    }
    else // lado oponente
    {
        area.InferiorFundo = AreaDefesaOponente.InferiorFundo;
        area.InferiorLinha = AreaDefesaOponente.InferiorLinha;
        area.SuperiorFundo = AreaDefesaOponente.SuperiorFundo;
        area.SuperiorLinha = AreaDefesaOponente.SuperiorLinha;

        if (sslrefComandoAtual == SSL_Referee_Command_STOP ||
            sslrefComandoAtual == SSL_Referee_Command_DIRECT_FREE_YELLOW ||
            sslrefComandoAtual == SSL_Referee_Command_DIRECT_FREE_BLUE ||
            sslrefComandoAtual == SSL_Referee_Command_INDIRECT_FREE_YELLOW ||
            sslrefComandoAtual == SSL_Referee_Command_INDIRECT_FREE_BLUE)
        {
            area.InferiorFundo = AreaRestricaoOponente.InferiorFundo;
            area.InferiorLinha = AreaRestricaoOponente.InferiorLinha;
            area.SuperiorFundo = AreaRestricaoOponente.SuperiorFundo;
            area.SuperiorLinha = AreaRestricaoOponente.SuperiorLinha;
        }
    }

    QVector2D Pint;
    if (Auxiliar::bChecaInterseccaoLinhaLinha(
            area.SuperiorLinha, area.SuperiorFundo, P1, P2, Pint) == false)
    {
        if (Auxiliar::bChecaInterseccaoLinhaLinha(
                area.InferiorLinha, area.InferiorFundo, P1, P2, Pint) == false)
        {
            if (Auxiliar::bChecaInterseccaoLinhaLinha(area.SuperiorLinha,
                                                      area.InferiorLinha, P1,
                                                      P2, Pint) == false)
            {
                return false;
            }
        }
    }

    return true;
}

bool VisibilityGraph::bInterseccaoLinhaTravesGol(const QVector2D& P1,
                                                 const QVector2D& P2)
{
    AreaDefesa area;
    if (bLadoDefesaUsado == true) // Lado aliado
    {
        area.InferiorFundo = TravesGolAliado.InferiorFundo;
        area.InferiorLinha = TravesGolAliado.InferiorLinha;
        area.SuperiorFundo = TravesGolAliado.SuperiorFundo;
        area.SuperiorLinha = TravesGolAliado.SuperiorLinha;
    }
    else // lado oponente
    {
        area.InferiorFundo = TravesGolOponente.InferiorFundo;
        area.InferiorLinha = TravesGolOponente.InferiorLinha;
        area.SuperiorFundo = TravesGolOponente.SuperiorFundo;
        area.SuperiorLinha = TravesGolOponente.SuperiorLinha;
    }

    QVector2D Pint;
    if (Auxiliar::bChecaInterseccaoLinhaLinha(
            area.SuperiorLinha, area.SuperiorFundo, P1, P2, Pint) == false)
    {
        if (Auxiliar::bChecaInterseccaoLinhaLinha(
                area.InferiorLinha, area.InferiorFundo, P1, P2, Pint) == false)
        {
            if (Auxiliar::bChecaInterseccaoLinhaLinha(area.SuperiorLinha,
                                                      area.InferiorLinha, P1,
                                                      P2, Pint) == false)
            {
                return false;
            }
        }
    }

    return true;
}

bool VisibilityGraph::bInterseccaoLinhaLinhaRestricao(const QVector2D& P1,
                                                      const QVector2D& P2)
{
    if (!lineLinhaRestricao.isNull())
    {
        QVector2D p1l(lineLinhaRestricao.x1(), lineLinhaRestricao.y1()),
            p2l(lineLinhaRestricao.x2(), lineLinhaRestricao.y2()), Pint;

        return Auxiliar::bChecaInterseccaoLinhaLinha(p1l, p2l, P1, P2, Pint);
    }
    else
    {
        return false;
    }
}

void VisibilityGraph::vCriaGrafo(AmbienteCampo* acAmbiente, int iRoboID)
{
    QElapsedTimer tmrGrafo;
    tmrGrafo.start();
    vSetup(acAmbiente, iRoboID);
    vCriaObjetosRegiaoAtiva(acAmbiente, iRoboID);

    int nVertices = vt2dPontosObjetos.size();
    vgMatrizAdjacencia = std::vector<std::vector<ArestaVG>>(
        nVertices, std::vector<ArestaVG>(nVertices));

    QVector2D centroAtual, objetoAtual;

    for (quint16 i = 0; i < nVertices; ++i)
    {
        for (quint16 j = i + 1; j < nVertices; ++j)
        {
            centroAtual = vt3dPaisObjetos.at(i).toVector2D();
            objetoAtual = vt2dPontosObjetos.at(j);

            if (i == 0) // Origem
            {
                centroAtual = vt3dPaisObjetos.constFirst().toVector2D();
                // Para conectar somente os vertices do proprio robo
                if (objetoAtual.distanceToPoint(centroAtual) <=
                        globalConfig.safety.ally * 1.1 &&
                    bInterseccaoLinhaTravesGol(objetoAtual, centroAtual) ==
                        false)
                {
                    vAdicionaAresta(true, i, j);
                }
            }
            else
            {
                TipoObjetoGrafo tipoI = static_cast<TipoObjetoGrafo>(
                                    vt3dPaisObjetos.at(i).z()),
                                tipoJ = static_cast<TipoObjetoGrafo>(
                                    vt3dPaisObjetos.at(j).z());

                vConectaObjetos(i, j, tipoI, tipoJ);
            }
        }
    }

    // Para mostrar o resultado
    //	vCriarLogGrafo(iRoboID, tmrGrafo.nsecsElapsed()/1e6);
    //    qDebug() << tmrGrafo.nsecsElapsed()/1e6;
}

void VisibilityGraph::vSetaOpenClosed(bool open, bool closed, int indiceVertice,
                                      float custoG)
{
    for (quint16 i = 0; i <= indiceVertice; ++i)
    {
        vgMatrizAdjacencia[indiceVertice][i].open.first = open;
        vgMatrizAdjacencia[indiceVertice][i].closed.first = closed;
        vgMatrizAdjacencia[indiceVertice][i].g = custoG;

        vgMatrizAdjacencia[i][indiceVertice].open.second = open;
        vgMatrizAdjacencia[i][indiceVertice].closed.second = closed;
        vgMatrizAdjacencia[i][indiceVertice].g = custoG;
    }
}

void VisibilityGraph::vCriarLogGrafo(const int& iRoboID,
                                     const int& celulasExpandidas)
{
    QFile fileLog;
    fileLog.setFileName(QString("LogGrafo_Robo_") + QString::number(iRoboID) +
                        QString("_") + QString::number(celulasExpandidas) +
                        QDateTime::currentDateTime().toString() +
                        QString("_.matgraph"));

    fileLog.open(QIODevice::WriteOnly);

    QTextStream stream(&fileLog);

    for (quint16 i = 0; i < vgMatrizAdjacencia.size(); ++i)
    {
        for (quint16 j = 0; j < vgMatrizAdjacencia.size(); ++j)
        {
            stream << vgMatrizAdjacencia[i][j].arestaConectada << " ";
        }
        stream << "\n";
    }
    stream << "Conectou o destino = " << QString::number(bDestinoConectado())
           << "\n";
    stream << "Inicio:" << QString::number(vt2dPosicaoInicial.x()) << " "
           << QString::number(vt2dPosicaoInicial.y()) << "\n";
    stream << "Destino:" << QString::number(vt2dPosicaoFinal.x()) << " "
           << QString::number(vt2dPosicaoFinal.y()) << "\n";
    stream << "Obstaculos:\n";
    QVector2D pos;
    for (quint16 i = 0; i < vt2dPontosObjetos.size(); ++i)
    {
        pos = vt2dPontosObjetos.at(i);
        stream << QString::number(pos.x()) << ";" << QString::number(pos.y())
               << "\n";
    }
    stream << "Fim"
           << "\n";
    fileLog.close();
}

bool VisibilityGraph::bDestinoConectado()
{
    quint16 indiceDestino = vgMatrizAdjacencia.size() - 1;
    for (quint16 i = 0; i < vgMatrizAdjacencia.size(); ++i)
    {
        if (vgMatrizAdjacencia[indiceDestino][i].arestaConectada == true ||
            vgMatrizAdjacencia[i][indiceDestino].arestaConectada == true)
            return true;
    }
    return false;
}

QVector<QVector4D> VisibilityGraph::vt4dObstaculos(int& maiorRaio)
{
    maiorRaio = qMax(iRaioObjeto(ALIADO), iRaioObjeto(OPONENTE));
    maiorRaio = qMax(maiorRaio, iRaioObjeto(LINHA_DEFESA));
    maiorRaio = qMax(maiorRaio, iRaioObjeto(BOLA));
    maiorRaio = qMax(maiorRaio, static_cast<int>(globalConfig.safety.distance));

    return vt4dCentroObjetos;
}

void VisibilityGraph::vSetup(const AmbienteCampo* acAmbiente,
                             const int& iRoboID)
{
    vt2dPosicaoInicial =
        acAmbiente->allies->getPlayer(iRoboID)->vt2dPosicaoAtual();
    vt2dPosicaoFinal = acAmbiente->allies->getPlayer(iRoboID)->vt2dDestino();

    bLadoDefesaUsado = true; // Lado aliado
    if ((vt2dPosicaoInicial.x() > 0 &&
         acAmbiente->allies->iGetSide() == XNegativo) ||
        (vt2dPosicaoInicial.x() < 0 &&
         acAmbiente->allies->iGetSide() == XPositivo))
        bLadoDefesaUsado = false; // Lado oponente

    vSetaAreasDefesa(acAmbiente);
    vSetaAreasRestricao(acAmbiente);
    vSetaTravesGol(acAmbiente);

    if (iRoboID == acAmbiente->allies->iGetGoalieID())
        bGoleiro = true;
    else
        bGoleiro = false;

    sslrefComandoAtual = acAmbiente->sslrefComandoRefereeAtual;

    const bool roboDentroDefesa = bDentroAreaDefesa(vt2dPosicaoInicial, false);
    const bool jogadaRoboIgnoraAreaDefesa =
        iRoboID == acAmbiente->allies->iGetGoalieID() ||
        acAmbiente->allies->getPlayer(iRoboID)->jJogadaAtual() ==
            Penalty_Cobrador ||
        acAmbiente->allies->getPlayer(iRoboID)->jJogadaAtual() ==
            BallPlacement_Auxiliar ||
        acAmbiente->allies->getPlayer(iRoboID)->jJogadaAtual() ==
            BallPlacement_Principal;

    if (roboDentroDefesa || jogadaRoboIgnoraAreaDefesa)
    {
        bConsideraAreaDefesa = false;
    }
    else if (bDentroAreaRestricao(vt2dPosicaoInicial) == true ||
             bDentroAreaRestricao(vt2dPosicaoFinal) == true)
    {
        bConsideraAreaDefesa = true;
    }
    else if (bInterseccaoLinhaAreaDefesa(vt2dPosicaoInicial,
                                         vt2dPosicaoFinal) == true)
    {
        bConsideraAreaDefesa = true;
    }
    else
        bConsideraAreaDefesa = false;

    vAtualizaLinhaRestricao(acAmbiente, iRoboID);
}

void VisibilityGraph::vAtualizaLinhaRestricao(const AmbienteCampo* acAmbiente,
                                              const int& iRoboID)
{
    qint8 rec = acAmbiente->iAchaRoboComJogada(Normal_Receptor);

    if (rec != -1 && rec != iRoboID)
    {
        QPoint ptRobo =
            acAmbiente->allies->getPlayer(rec)->vt2dPosicaoAtual().toPoint();
        lineLinhaRestricao.setP1(ptRobo);
        lineLinhaRestricao.setP2(acAmbiente->vt2dPosicaoBola().toPoint());
    }
    else if (acAmbiente->bChuteAoGolAliadoEmAndamento())
    {
        QVector2D pontoLivreGol = Auxiliar::vt2dCentroLivreGolAdversario(
            acAmbiente->vt2dPosicaoBola(), acAmbiente->allies->iGetSide(),
            acAmbiente->geoCampo->szGoal().height(),
            acAmbiente->geoCampo->szField(),
            acAmbiente->vt3dPegaPosicaoTodosObjetos(otOponente));

        lineLinhaRestricao.setP1(pontoLivreGol.toPoint());
        lineLinhaRestricao.setP2(acAmbiente->vt2dPosicaoBola().toPoint());
    }
    else if ((acAmbiente->sslrefComandoRefereeAtual ==
                  SSL_Referee_Command_BALL_PLACEMENT_BLUE &&
              acAmbiente->allies->teamColor() == timeAmarelo) ||
             (acAmbiente->sslrefComandoRefereeAtual ==
                  SSL_Referee_Command_BALL_PLACEMENT_YELLOW &&
              acAmbiente->allies->teamColor() == timeAzul))
    {
        lineLinhaRestricao.setP1(
            acAmbiente->vt2dPosicaoBallPlacement.toPoint());
        lineLinhaRestricao.setP2(acAmbiente->vt2dPosicaoBola().toPoint());
    }
    else
    {
        lineLinhaRestricao = QLine();
    }
}

QVector<ArestaVG> VisibilityGraph::vgVerticesVisiveis(int iVerticeAtual)
{
    QVector<ArestaVG> vgAux;
    vgAux.clear();
    ArestaVG vertice;

    for (uint n = 0; n < vgMatrizAdjacencia.size(); ++n)
    {
        vertice = vgMatrizAdjacencia[iVerticeAtual][n]; // C ; L
        if (vertice.arestaConectada == true)
        {
            vgAux.append(vertice);
        }
    }

    return vgAux;
}
