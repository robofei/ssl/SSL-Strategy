/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VISIBILITYGRAPH_H
#define VISIBILITYGRAPH_H

///
/// \file visibilitygraph.h
/// \brief \a ObjetoVG, \a CompararObjeto, \a ArestaVG, \a AreaDefesa e \a VisibilityGraph
///

#include <QVector>
#include <QVector2D>
#include <QVector4D>
#include <QLine>
#include <QtMath>
#include <QDebug>
#include <QDateTime>

#include <algorithm>
#include <queue>
#include <set>
#include <stdlib.h>

#include  "Ambiente/futbolenvironment.h"

#define DIST_AGRUPAMENTO 500
const int iRaioAreaDefesa = globalConfig.safety.ally; /**< Raio dos objetos da área de restrição. */

/**
 * @brief Determina os tipos de objetos possíveis no grafo.
 */
enum TipoObjetoGrafo
{
    OPONENTE     = otOponente,
    ALIADO       = otAliado,
    BOLA         = otBola,
    LINHA_DEFESA,
    DESTINO     ,
    AGRUPAMENTO
};

/**
 * @brief Classe utilizada para ajudar a organizar os pontos do grafo em ordem crescente em relação ao ponto inicial.
 *
 */
class ObjetoVG
{
public:
    QVector2D vt2dP; /**< Ponto da região ativa. */
    float fDistancia; /**< Distância do ponto P até o ponto inicial do grafo [mm]. */
    TipoObjetoGrafo otgTipo; /**< Tipo do objeto. */

    /**
     * @brief Construtor da classe.
     *
     * @param _vt2dP - Ponto.
     * @param _fDistancia - Distância até o ponto inicial.
     * @param _otgTipo - Tipo do objeto.
     */
    ObjetoVG(QVector2D _vt2dP, float _fDistancia, TipoObjetoGrafo _otgTipo)
    {
        vt2dP = _vt2dP ;
        fDistancia = _fDistancia;
        otgTipo = _otgTipo;
    }
};


/**
 * @brief Struct com o método para compara dois objetos da classe ObjetoVG.
 *
 */
struct CompararObjeto
{
    /**
     * @brief Operador de comparação.
     *
     * @param _O1 - Objeto 1.
     * @param _O2 - Objeto 2.
     * @return bool operator - True se a distância do objeto 1 for maior que do objeto 2, senão, false.
     */
    bool operator()(const ObjetoVG& _O1, const ObjetoVG& _O2)
    {
        if(_O1.fDistancia > _O2.fDistancia)
            return true;
        else
            return false;
    }
};

/**
 * @brief Classe que representa uma aresta no grafo.
 *
 */
class ArestaVG
{
public:
    ArestaVG()
    {
        pos1 = QVector2D(0,0);
        pos2 = QVector2D(0,0);
        arestaConectada  = false;
        closed.first  = false;
        closed.second = false;
        open.first    = false;
        open.second   = false;
        indice  = 0;
        g       = 0;
    }
    QVector2D pos1; /**< Posição do primeiro vértice no campo. */
    QVector2D pos2; /**< Posição do segundo vértice no campo. */
    bool arestaConectada; /**< Indica se a aresta está conectada. */
    QPair<bool, bool> closed; /**< Indica se este vértice está na closed list. */
    QPair<bool, bool> open; /**< Indica se este vértice está na open list. */
    float g; /**< Custo G da aresta. */
    int indice; /**< Índice deste vértice. */
};

/**
 * @brief Estrutura que determina a Área de defesa.
 *
 */
typedef struct AreaDefesa
{
    /**
     * @brief Construtor.
     *
     */
    AreaDefesa()
    {
        SuperiorFundo = QVector2D(0,0);
        SuperiorLinha = QVector2D(0,0);
        InferiorFundo = QVector2D(0,0);
        InferiorLinha = QVector2D(0,0);
    }
    QVector2D SuperiorFundo; /**< Ponto superior da linha de fundo da área. */
    QVector2D SuperiorLinha; /**< Ponto superior da linha da frente da área. */
    QVector2D InferiorFundo; /**< Ponto inferior da linha de fundo da área. */
    QVector2D InferiorLinha; /**< Ponto inferior da linha da frente da área. */
}AreaDefesa;


/**
 * @brief Classe que representa o grafo de visibilidade utilizado pelo A_StarVG.
 *
 * Esta classe foi criada em conjunto com a classe A_StarVG para o projeto de iniciação
 * científica:
 * Implementação e comparação de algoritmos de _path planning_ para futebol de robôs.
 * O repositório deste trabalho pode ser visto em:
 * https://gitlab.com/leo_costa/ic_pathplanner.
 */
class VisibilityGraph
{
    /**
     * @brief Priority queue utilizada para organizar os objetos da região ativa em
     * ordem crescente de distância em relação ao ponto inicial.
     */
    typedef std::priority_queue<ObjetoVG, std::vector<ObjetoVG>, CompararObjeto> pqObjetos;

public:
    /**
     * @brief Construtor da classe.
     *
     * @param _iResolucaoCirculo - Ângulo entre cada vértice de um objeto, quanto menor,
     *  mais circular o objeto. Porém, mais demorado será para criar o grafo.
     */
    VisibilityGraph(int _iResolucaoCirculo);

    /**
     * @brief Construtor padrão da classe. Neste caso a #iResolucaoCirculo será igual ao
     * #iAnguloGrafo.
     */
    VisibilityGraph();

    /**
     * @brief Cria o grafo de visibilidade utilizado pelo A_StarVG.
     *
     * @param acAmbiente - Ambiente do campo.
     * @param iRoboID - ID do robô que o trajeto será calculado.
     */
    void vCriaGrafo(AmbienteCampo *acAmbiente, int iRoboID);

    /**
     * @brief Retorna os vértices conectados com o vértice fornecido.
     *
     * @param iVerticeAtual - Índice do vértice fornecido.
     * @return QVector<VerticeVG> - Vetor de vértices conectados.
     */
    QVector<ArestaVG> vgVerticesVisiveis(int iVerticeAtual);

    /**
     * @brief Seta se um vértice está na open ou closed list.
     *
     * @param open - Indica se o vértice deve estar na open list.
     * @param closed - Indica se o vértice deve estar na closed list.
     * @param indiceVertice - Índice do vértice a ser alterado.
     * @param custoG - Custo G do vértice.
     */
    void vSetaOpenClosed(bool open, bool closed, int indiceVertice, float custoG);

    /**
     * @brief Cria um log contendo a matriz de adjacência, os obstáculos considerados,
     * a posição final e inicial.
     * @param iRoboID - ID do robô cujo grafo foi construído.
     * @param celulasExpandidas - Quantidade de células expandidas pelo A* até não
     * encontrar o caminho.
     * @note Este método é utilizado somente pelo path-planner.
     */
    void vCriarLogGrafo(const int &iRoboID, const int &celulasExpandidas);

    /**
     * @brief Retorna true se existe pelo menos um vértice conectado ao destino.
     * @return bool.
     */
    bool bDestinoConectado();

    /**
     * @brief Retornar os obstáculos considerados na criação do grafo.
     * @param maiorRaio - Retornar o maior raio dos tipos de objetos possíveis [mm].
     * @return
     */
    QVector<QVector4D> vt4dObstaculos(int &maiorRaio);

    std::vector<std::vector<ArestaVG> > vgMatrizAdjacencia; /**< Matriz de adjacência do
                                                                 grafo de visibilidade. */
    QVector<QVector2D> vt2dPontosObjetos; /**< Vértices dos objetos contidos na região
                                                                      ativa do trajeto. */
    QVector<QVector3D> vt3dPaisObjetos; /**< Pais dos vértices dos objetos contidos na
                                                               região ativa do trajeto. */
    QVector<QPair<QVector2D, float>> circAgrupamentos; /**< Circulos que agrupam robôs
                                                                              próximos. */


private:

    /**
     * @brief Realiza algumas configurações básicas antes de criar o grafo.
     */
    void vSetup(const AmbienteCampo *acAmbiente, const int &iRoboID);

    /**
     * @brief Define as coordenadas da linha de restrição (#lineLinhaRestricao).
     * @details Caso seja necessário que o path-planner ignore outros tipos de reta,
     *  por exemplo, de outras jogadas, basta adicionar o `if-else` que seta os pontos P1
     * e P2 da #lineLinhaRestricao.
     * @param acAmbiente
     * @param iRoboID
     */
    void vAtualizaLinhaRestricao(const AmbienteCampo *acAmbiente, const int &iRoboID);

    /**
     * @brief Cria um objeto.
     * Um objeto é definido pelo seu centro (pai), e seus pontos periféricos (filhos).
     *
     * @param vt2dCentro - Centro do objeto.
     * @param otgObjeto - Tipo do objeto.
     * @param tamanhoCampo - Tamanho do campo.
     * @param iRaioOb - Raio do objeto a ser adicionado.
     */
    void vCriaObjeto(const QVector2D &vt2dCentro, const TipoObjetoGrafo &otgObjeto,
                     const QSize &tamanhoCampo, const int &iRaioObj);

    /**
     * @brief Cria o objeto da área de defesa.
     *
     * @param Area - Dimensão da área de defesa.
     * @param tamanhoCampo - Tamanho do campo.
     */
    void vCriaAreaDefesa(const AreaDefesa &Area, const QSize &tamanhoCampo);

    /**
     * @brief Retorna o raio do tipo de objeto fornecido.
     *
     * @param _obj - Tipo do objeto.
     * @return int - Raio do objeto [mm].
     */
    int iRaioObjeto(const TipoObjetoGrafo &_obj);

    /**
     * @brief Retorna o raio para objetos do tipo AGRUPAMENTO (#TipoObjetoGrafo).
     * @param centro - Centro do objeto. (#vt3dPaisObjetos).
     * @return int - Raio do objeto [mm].
     */
    int iRaioAgrupamento(const QVector2D &centro);

    /**
     * @brief Conecta uma aresta do grafo.
     *
     * @param aresta - Indica se a aresta está conectada.
     * @param i - Índice do primeiro vértice.
     * @param j - Índice do segundo vértice.
     */
    void vAdicionaAresta(const bool &aresta, const int &i, const int &j);

    /**
     * @brief Encontra os objetos que participam da região ativa do trajeto.
     * @param acAmbiente - Ambiente do campo.
     * @param iRoboID - ID do robô que o grafo será construído.
     * @param vt3dAliados - Vetor que possuirá os aliados contidos na região ativa.
     * @param vt3dOponentes - Vetor que possuirá os oponentes contidos na região ativa.
     */
    void vEncontraObjetosRegiaoAtiva(const AmbienteCampo *acAmbiente, const int &iRoboID,
                                     QVector<QVector3D> &vt3dAliados,
                                     QVector<QVector3D> &vt3dOponentes);

    /**
     * @brief Organiza os objetos detectados na região ativa em ordem crescente de acordo
     * com a distância para o ponto inicial.
     * @param acAmbiente - Ambiente do campo.
     * @param iRoboID - ID do robô que o grafo será construído.
     */
    void vOrganizaObjetosRegiaoAtiva(const AmbienteCampo *acAmbiente, const int &iRoboID);

    /**
     * @brief Determina quais robôs podem ser agrupados em retângulos para reduzir o
     * processamento necessário.
     */
    void vDeterminaAgrupamentos();

    /**
     * @brief Utilizado pelo método #vDeterminaAgrupamentos para encontrar quais robôs
     * estão próximos do robô de índice #c.
     * @param agrupamentos - Vetor com os agrupamentos já realizados.
     * @param c - Índice do robô.
     */
    void vEncontraProximo(QVector<int> &agrupamentos, int c);

    /**
     * @brief Com base nos agrupamentos determinados, calcula os retângulos que incluem os
     *  robôs de um mesmo grupo.
     * @param g - Vetor com os agrupamentos.
     */
    void vCalculaAgrupamentos(const QVector<int> &g);

    /**
     * @brief Cria os objetos da região ativa para o grafo de visibilidade.
     *
     * @param acAmbiente - Ambiente do campo.
     * @param iRoboID - ID do robô que o grafo será construído.
     */
    void vCriaObjetosRegiaoAtiva(const AmbienteCampo *acAmbiente, const int &iRoboID);

    /**
     * @brief Seta os valores das áreas de defesa.
     * @image html restricaoAreaDefesa.svg width=40%
     * @param acAmbiente - Ambiente do campo.
     */
    void vSetaAreasDefesa(const AmbienteCampo *acAmbiente);

    /**
     * @brief Seta os valores das áreas de restrição.
     * @image html restricaoAreaDefesa.svg width=40%
     * @param acAmbiente
     */
    void vSetaAreasRestricao(const AmbienteCampo *acAmbiente);

    /**
     * @brief Seta os valores das traves dos gols.
     *
     * @param acAmbiente - Ambiente do campo.
     */
    void vSetaTravesGol(const AmbienteCampo *acAmbiente);

    /**
     * @brief Determina a distância mínima para checagem de colisão entre dois objetos.
     * @param raio1 - Raio do objeto 1.
     * @param raio2 - Raio do objeto 2.
     * @param vertice1 - Vértice do objeto 1.
     * @param vertice2 - Vértice do objeto 2.
     * @param mesmoObjeto - Utilizado para retornar os parâmetros fornecido pertencem ao
     * mesmo objeto.
     * @return float - Distância mínima para checagem de colisão entre dois objetos.
     */
    float fDeterminaDistanciaMinima(const int &raio1, const int &raio2,
                                    const int &vertice1, const int &vertice2,
                                    bool &mesmoObjeto);

    /**
     * @brief Checa se a aresta que une o vértice 1 ao 2 cruza as traves do gol.
     * @param vertice1 - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @param vertice2 - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @return bool - Indica se a aresta cruza o gol.
     */
    bool bCruzaTravesGol(const int &vertice1, const int &vertice2);

    /**
     * @brief Conecta os vértices A e B que pertencem ao mesmo objeto.
     * @param raioObj - Raio do objeto considerado.
     * @param verticeA - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @param verticeB - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @param tipo - Tipo do objeto. Pode ser um robô ou a linha do penalti.
     */
    void vConectaProprioObjeto(const int &raioObj, const int &verticeA,
                               const int &verticeB, const TipoObjetoGrafo &tipo);

    /**
     * @brief Conecta um vértice do objeto ao vértice do destino.
     * @param verticeA - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @param verticeB - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @see fDeterminaDistanciaMinima.
     */
    void vConectaObjetoDestino(const int &verticeA, const int &verticeB);

    /**
     * @brief Conecta um vértice do objeto A à outro vértice do objeto B.
     * @param RoboA - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @param RoboB - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @see fDeterminaDistanciaMinima.
     */
    void vConectaObjetosDiferentes(const int &RoboA, const int &RoboB);

    /**
     * @brief Conecta quaisquer dois objetos.
     *
     * @param RoboA - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @param RoboB - Índice do vértice no vetor #vt2dPontosObjetos/#vt3dPaisObjetos.
     * @param tipoA - Tipo do objeto A.
     * @param tipoB - Tipo do objeto B.
     */
    void vConectaObjetos(const int &RoboA, const int &RoboB,
                         const TipoObjetoGrafo &tipoA, const TipoObjetoGrafo &tipoB);

    /**
     * @brief Analisa os obstáculos fornecidos e adiciona-os à priority queue caso eles
     * estejam dentro da região ativa.
     *
     * @param obstaculos - Vetor de obstáculos com seus respectivos tipos.
     * @param P1 - Ponto inicial do trajeto.
     * @param P2 - Ponto final do trajeto.
     * @param pqRegAtiva - Priority queue dos objetos da região ativa.
     */
    void vAnalisaObstaculos(const QVector<QVector3D> &obstaculos, const QVector2D &P1,
                            const QVector2D &P2, pqObjetos &pqRegAtiva);

    /**
     * @brief Retorna true se o ponto fornecido está dentro de alguma área de defesa.
     *
     * @param ponto - Ponto de teste.
     * @param jaSetou - Indica se o flag de considerar area de defesa ja foi setado,
     * @return bool - True se o ponto fornecido está dentro de alguma área de defesa,
     *  senão, false.
     */
    bool bDentroAreaDefesa(const QVector2D &ponto, const bool &jaSetou = true);

    /**
     * @brief Retorna true se o ponto fornecido está dentro de alguma área de restrição.
     * @param ponto - Ponto de teste.
     * @return
     */
    bool bDentroAreaRestricao(const QVector2D &ponto);

    /**
     * @brief Retorna true se a reta definida por P1-P2 intercepta a área de defesa
     * utilizada.
     *
     * @param P1
     * @param P2
     * @return bool - True se a reta definida por P1-P2 intercepta a área de defesa
     * utilizada, senão, false.
     */
    bool bInterseccaoLinhaAreaDefesa(const QVector2D &P1, const QVector2D &P2);

    /**
     * @brief Retorna true se a reta definida por P1-P2 intercepta a trave do gol
     * utilizada.
     *
     * @param P1
     * @param P2
     * @return bool - True se a reta definida por P1-P2 intercepta a trave do gol
     * utilizada, senão, false.
     */
    bool bInterseccaoLinhaTravesGol(const QVector2D &P1, const QVector2D &P2);

    /**
     * @brief Retorna true se a reta definida por P1-P2 intercepta a #lineLinhaRestricao.
     * @param P1
     * @param P2
     * @return bool - True se a reta P1-P2 intercepta a #lineLinhaRestricao.
     */
    bool bInterseccaoLinhaLinhaRestricao(const QVector2D &P1, const QVector2D &P2);

    QVector<QVector4D> vt4dCentroObjetos; /**< Centros dos vértices adicionados em
                                                conjunto com o tipo do objeto no campo Z
                                                e o raio do objeto no campo W.
                                                Ex: (PosX, PosY, Tipo, Raio)*/

    AreaDefesa AreaDefesaAliado;          /**< Área de defesa aliados. */
    AreaDefesa AreaDefesaOponente;        /**< Área de defesa oponente. */
    AreaDefesa AreaRestricaoAliado;       /**< Área de restrição aliada. */
    AreaDefesa AreaRestricaoOponente;     /**< Área de restrição oponente. */
    AreaDefesa TravesGolAliado;           /**< Traves do gol aliado. */
    AreaDefesa TravesGolOponente;         /**< Traves do gol oponente. */

    bool bConsideraAreaDefesa;            /**< Indica se a Área de defesa deve ser
                                               considerada. */
    bool bLadoDefesaUsado;                /**< Indica qual o lado de defesa deve ser
                                               usado. True = aliado,
                                               false = oponente. */
    bool bGoleiro;                        /**< Indica se é o goleiro. */
    SSL_Referee_Command sslrefComandoAtual; /**< Comando atual sendo executado. */

    QVector2D vt2dPosicaoInicial;         /**< Posição inicial do grafo. */
    QVector2D vt2dPosicaoFinal;           /**< Posição final do grafo. */

    QLine lineLinhaRestricao;            /**< Nenhum caminho gerado poderá cruzar
                                              esta linha. Isso é utilizado para que os
                                              robôs não atrapalhem um passe ou chute
                                              aliado. */

    int iResolucaoCirculo; /**< Define a cada quantos graus deve ser colocado um vértice
                                do objeto. Os objetos da área de restrição
                                (#AreaRestricaoAliado/#AreaRestricaoOponente)
                                utilizam sempre uma resolução de 45 graus.*/
};

#endif // VISIBILITYGRAPH_H
