/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "arxmodel.h"

ARXModel::ARXModel()
{
    A = B = nullptr;
}

void ARXModel::readModelFile(QString filename)
{
    QFile file;
    file.setFileName(filename);
    QByteArray fileData, matrixData;
    fileData.clear();
    matrixData.clear();

    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        fileData = file.read(file.size());
        char matrixName = 'A';
        QPair<int,int> matrixSize;
        QList<QByteArray> matrixAB;

        if(fileData.count(';') != 2)
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setText("Failed to load ARX Model! Not enough delimiters");
            msgBox.exec();
        }
        else
        {
            matrixAB = fileData.split(';');

            for(auto matrix : matrixAB)
            {
                if(matrix.contains('A') || matrix.contains('B'))
                {
                    int matrixSizeIndex = matrix.indexOf('x',matrix.indexOf(' '));

                    if(matrixSizeIndex != -1)
                    {
                        if(matrix.contains('B'))
                        {
                            matrixName = 'B';
                        }
                        // -'0' converts char to int
                        matrixSize.first = matrix.at(matrixSizeIndex-1)-'0';
                        matrixSize.second = matrix.at(matrixSizeIndex+1)-'0';
                        // Separates the coefficients from Matrix X=nxn\n
                        matrixData = matrix.mid(matrix.indexOf('\n',2)+1);

                        QList<QByteArray> auxData = matrixData.split(',');
                        int matrixOrder = auxData.at(0).count(' ');
                        createMatrix(matrixData, matrixSize, matrixName,matrixOrder);
                    }

                }
            }
        }
    }
    else
    {
        qDebug() << file.errorString();
    }
}

QVector3D ARXModel::calculateActualPosition(QVector<QVector3D> lastPositions,
        QVector<QVector3D> lastVelocities)
{
    QVector3D modelPosition;//(X,Y,θ)

    if(this->A == nullptr || this->B == nullptr)
    {
        return modelPosition;
    }

    //Only use the model if there is enough data
    if(lastPositions.size()+1 >= A->getOrder() && lastVelocities.size()+1 >= B->getOrder())
    {
        ARXMatrix Y(A->getRowSize(), 1, A->getOrder()),
                  U(B->getRowSize(), 1, B->getOrder());

        Eigen::MatrixXd position(3,1);
        position.setZero(3,1);

        //The order of the values must range from 1 to MaxOrder-1
        //In a second order model, e.g. a + bx + cx², we will only set the b and c values
        //                              0   1    2
        for(int n=1; n < Y.getOrder(); ++n)
        {
            Y.setParameter(0,0,n, lastPositions.at(n-1).x()/1e3);//Sets the X values
            Y.setParameter(1,0,n, lastPositions.at(n-1).y()/1e3);//Sets the Y values
            Y.setParameter(2,0,n, lastPositions.at(n-1).z());//Sets the θ values

            // Calculate the model output
            position +=  - A->getMatrix(n)*Y.getMatrix(n);
        }
        for(int n=1; n < U.getOrder(); ++n)
        {
            U.setParameter(0,0,n, lastVelocities.at(n-1).x());//Sets the VX values
            U.setParameter(1,0,n, lastVelocities.at(n-1).y());//Sets the VY values
            U.setParameter(2,0,n, lastVelocities.at(n-1).z());//Sets the Vθ values

            // Calculate the model output
            position += B->getMatrix(n)*U.getMatrix(n);
        }

        modelPosition.setX(position(0,0)*1e3);
        modelPosition.setY(position(1,0)*1e3);
        modelPosition.setZ(position(2,0));
    }
    return modelPosition;
}

void ARXModel::createMatrix(QByteArray data, QPair<int, int> matrixSize, char matrixName, int order)
{
    int endPos = 0;
    double value = 0;
    QPair<QPair<int,int>,int> matrixIndex;//Line,Column,Order
    matrixIndex.first.first = matrixIndex.first.second = matrixIndex.second = 0;
    ARXMatrix *auxMatrix = nullptr;

    if(matrixName == 'A')
    {
        if(A != nullptr)
            delete A;

        A = new ARXMatrix(matrixSize.first, matrixSize.second, order);
        auxMatrix = A;
    }
    else if(matrixName == 'B')
    {
        if(B != nullptr)
            delete B;

        B = new ARXMatrix(matrixSize.first, matrixSize.second, order);
        auxMatrix = B;
    }

    while(data.size() > 1)
    {
        endPos++;

        if(data.at(endPos) == ' ')
        {
            value = data.mid(0,endPos).toDouble();
            data.remove(0, endPos+1);
            endPos = -1;
            auxMatrix->setParameter(matrixIndex.first.first, matrixIndex.first.second,
                                    matrixIndex.second, value);
//            qDebug() << "Matrix " + QString(matrixName) + "["+QString::number(matrixIndex.first.first)  +","+
//                                                              QString::number(matrixIndex.first.second) +","+
//                                                              QString::number(matrixIndex.second)       +"] = " << value;
            matrixIndex.second +=1;//Change Order
        }
        else if(data.at(endPos) == ':')
        {
            matrixIndex.first.first +=1; //Change Line
            matrixIndex.first.second = 0; //Reset Column
            matrixIndex.second = 0;       //Reset Order
            if(matrixIndex.first.first > matrixSize.first)
            {
                qDebug() << "Matrix Line Size Error";
            }
            data.remove(0,1);
            endPos=0;
        }
        else if(data.at(endPos) == ',')
        {
            matrixIndex.first.second +=1;//Change Column
            matrixIndex.second = 0;      //Reset Order
            if(matrixIndex.first.second > matrixSize.second)
            {
                qDebug() << "Matrix Column Size Error";
            }
            data.remove(0,1);
            endPos=0;
        }
    }
    auxMatrix = nullptr;
}

