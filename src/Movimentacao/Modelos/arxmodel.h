/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file arxmodel.h
 * @brief Arquivo header com a definição das classes utilizadas para simular
 * um modelo ARX.
 * @details Essa classe é produto de uma iniciação científica, que pode ser
 * encontrada [neste repositório](https://gitlab.com/leo_costa/ic_identificacao).
 *
 * O modelo gerado nessa iniciação está contido no arquivo
 * '../../../include/Movimentacao/ARXModel.arx'.
 *
 * @author Leonardo da Silva Costa
 * @version 1.0
 * @date 2020-08-27
 */
#ifndef ARXMODEL_H
#define ARXMODEL_H

#include <QFile>
#include <QDebug>
#include <QVector3D>
#include <Eigen/LU>
#include <Eigen/Dense>
#include <QMessageBox>

/**
 * @brief Classe que implementa uma matriz para cálculo da saída de um modelo
 * ARX.
 * @details Um modelo ARX MIMO é composto por duas matrizes de polinômios. Essas
 * matrizes de polinômios foram implementadas como sendo um vetor de matrizes
 * de um único coeficiente, onde, cada índice desse vetor é o coeficiente de uma
 * ordem, por exemplo, a matriz de índice 3 no vetor, possui todos os coeficientes
 * de ordem 3 da matriz de polinômios completa.
 *
 */
class ARXMatrix
{

public:
    /**
     * @brief Construtor da classe
     * @details Inicializa uma matrix do modelo
     *
     * @param rows - Número de linhas
     * @param columns - Número de colunas
     * @param order - Ordem máxima dos polinômios da matriz
     */
    ARXMatrix(int rows, int columns, int order)
    {
        columnSize = columns;
        rowSize = rows;
        orderSize = order;
        matrix = new QVector<Eigen::MatrixXd*>;
        for (int n = 0; n < order; ++n)
        {
            matrix->append(new Eigen::MatrixXd(rows, columns));
        }
    }

    /**
     * @brief Destrutor da classe
     * @details Faz a desalocação dos ponteiros alocados
     */
    ~ARXMatrix()
    {
        for (int n = 0; n < orderSize; ++n)
        {
            delete matrix->at(n);
        }
        delete matrix;

    }

    /**
     * @brief Seta o valor de um coeficiente na matriz
     *
     * @param row - Linha do coeficiente na matriz
     * @param column - Coluna do coeficiente na matriz
     * @param order - Ordem do coeficiente na matriz
     * @param value - Valor do coeficiente
     * @note Note que nesta função, a ordem é a ordem **do coeficiente** e não
     * a ordem máxima
     */
    void setParameter(int row, int column, int order, double value)
    {
        if(row < rowSize && column < columnSize && order < orderSize)
        {
            matrix->at(order)->operator()(row, column) = value;
        }
    }

    /**
     * @brief Retorna a matriz de coeficientes de uma ordem
     * @param order - Ordem dos coeficientes a serem retornados
     * @return
     */
    Eigen::MatrixXd getMatrix(int order)
    {
        return *matrix->at(order);
    }

    /**
     * @brief Retorna a quantidade de colunas da matriz
     *
     * @return int - Quantidade de colunas
     */
    int getColumnSize() const
    {
        return columnSize;
    }

    /**
     * @brief Retorna a quantidade de linhas da matriz
     *
     * @return int - Quantidade de linhas
     */
    int getRowSize() const
    {
        return rowSize;
    }

    /**
     * @brief Retorna a ordem do modelo
     *
     * @return int - Ordem do modelo
     */
    int getOrder() const
    {
        return orderSize;
    }

private:
    int columnSize; /**< Guarda quantias colunas a matriz possui. */
    int rowSize; /**< Guarda quantias linhas a matriz possui. */
    int orderSize; /**< Guarda a ordem máxima dos polinômios da matriz. */
    QVector<Eigen::MatrixXd*> *matrix; /**< Vetor de matrizes que simulam uma
                                         matriz de polinômios. */

};

/**
 * @brief Classe responsável por manipular um modelo ARX
 *
 * @details Para mais informações sobre modelos ARX
 * [visite](https://gitlab.com/leo_costa/ic_identificacao).
 */
class ARXModel
{

public:
    /**
     * @brief Construtor da classe
     *
     */
    ARXModel();

    /**
     * @brief Lê um arquivo que contém os coeficientes das matrizes A e B de um
     * modelo ARX
     * @param filename - Nome do arquivo .arx
     */
    void readModelFile(QString filename);

    /**
     * @brief Calcula a posição atual do robô simulado baseado nas ultimas posições
     * e entradas (velocidades de comando Atributos::cmd_vx, Atributos::cmd_vy,
     * Atributos::cmd_w)
     *
     * @param lastPositions - Vetor com o histórico de posições do robô [mm]
     * @param lastVelocities - Vetor com o histórico de velocidades de comando do
     * robô [0 - 100]
     *
     * @note O vetor com as posições e velocidades deve possuir no minimo
     * \f$ \max(n_a, n_b) + n_k - 1\f$ amostras para utilizar o modelo
     *
     * @return QVector3D - A posição do robô (\f$ X,Y,\theta\f$) [mm, mm, rad]
     */
    QVector3D calculateActualPosition(QVector<QVector3D> lastPositions,
            QVector<QVector3D> lastVelocities);


private:
    /**
     * @brief A - Matriz A do modelo
     */
    ARXMatrix *A;

    /**
     * @brief B - Matriz B do modelo
     */
    ARXMatrix *B;

    /**
     * @brief Cria uma ARXMatrix a partir dos dados lidos do arquivo
     * @param data - Dados lidos do arquivo contendo os coeficientes da matriz
     * @param matrixSize - Tamanho da matriz
     * @param matrixName - Nome da matriz. Deve ser 'A' ou 'B'
     * @param order - Ordem da matriz
     */
    void createMatrix(QByteArray data, QPair<int,int> matrixSize,
                      char matrixName, int order);
};

#endif // ARXMODEL_H
