#include "motionfeedbackpacket.h"

MotionFeedbackPacket::MotionFeedbackPacket()
{
    debugInfo = nullptr;
    allies = nullptr;
    opponents = nullptr;
    remainingPlayTime = 0;
}

MotionFeedbackPacket::MotionFeedbackPacket(const MotionFeedbackPacket& _other)
{
    debugInfo = new DebugInfo(*_other.debugInfo);
    allies = new SSLTeam<Robo>(*_other.allies);
    opponents = new SSLTeam<Robo>(*_other.opponents);
    remainingPlayTime = _other.remainingPlayTime;
}

MotionFeedbackPacket::~MotionFeedbackPacket()
{
    if (debugInfo != nullptr)
        delete debugInfo;
    if (allies != nullptr)
        delete allies;
    if (opponents != nullptr)
        delete opponents;
    remainingPlayTime = 0;
}

void MotionFeedbackPacket::vUpdateInfo(const DebugInfo* _debugInfo,
                                       const SSLTeam<Robo>* _allies,
                                       const SSLTeam<Robo>* _opponents,
                                       int _remainingPlayTime)
{
    if (debugInfo != nullptr)
        delete debugInfo;
    if (allies != nullptr)
        delete allies;
    if (opponents != nullptr)
        delete opponents;

    debugInfo = new DebugInfo(*_debugInfo);
    allies = new SSLTeam<Robo>(*_allies);
    opponents = new SSLTeam<Robo>(*_opponents);
    remainingPlayTime = _remainingPlayTime;
}
