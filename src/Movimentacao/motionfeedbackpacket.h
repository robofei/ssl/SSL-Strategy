#ifndef MOTIONFEEDBACKPACKET_H
#define MOTIONFEEDBACKPACKET_H

#include <QObject>

#include "Ambiente/sslteam.h"
#include "Debug/debuginfo.h"
#include "Robo/robot.h"

class MotionFeedbackPacket
{
public:
    DebugInfo* debugInfo;
    SSLTeam<Robo>* allies;
    SSLTeam<Robo>* opponents;
    int remainingPlayTime;

    MotionFeedbackPacket();
    MotionFeedbackPacket(const MotionFeedbackPacket& _other);
    ~MotionFeedbackPacket();

    void vUpdateInfo(const DebugInfo* _debugInfo, const SSLTeam<Robo>* _allies,
                     const SSLTeam<Robo>* _opponents, int _remainingPlayTime);
};

Q_DECLARE_METATYPE(MotionFeedbackPacket)

#endif // MOTIONFEEDBACKPACKET_H
