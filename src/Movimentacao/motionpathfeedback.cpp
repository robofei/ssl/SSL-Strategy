#include "motionpathfeedback.h"

MotionPathFeedback::MotionPathFeedback()
{
}

MotionPathFeedback::MotionPathFeedback(const MotionPathFeedback& _other)
{
    for (int i = 0; i < MAX_PATHS; i++)
    {
        robotsPaths[i].setFromPath(_other.robotsPaths[i]);
    }
}

MotionPathFeedback::~MotionPathFeedback()
{
}

void MotionPathFeedback::vUpdateInfo(const Path& _path, qint8 _robotID)
{
    if(_robotID > 0 && _robotID < MAX_PATHS)
    {
        robotsPaths[_robotID].setFromPath(_path);
    }
}
