#ifndef MOTIONPATHFEEDBACK_H
#define MOTIONPATHFEEDBACK_H

#include "Path_Planning/path.h"

class MotionPathFeedback
{
public:
    MotionPathFeedback();
    MotionPathFeedback(const MotionPathFeedback& _other);
    ~MotionPathFeedback();
    static constexpr int MAX_PATHS = 16;
    Path robotsPaths[MAX_PATHS];

    void vUpdateInfo(const Path& _path, qint8 _robotID);
};

Q_DECLARE_METATYPE(MotionPathFeedback)

#endif // MOTIONPATHFEEDBACK_H
