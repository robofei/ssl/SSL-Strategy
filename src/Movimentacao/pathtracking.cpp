#include "pathtracking.h"
#include <QtMath>
#include <iostream>

PathTracking::PathTracking()
{
    timeStep = globalConfig.samplingTime.motion * 1e-3;
    R.setIdentity();
    R = R / 10;
    R(3, 3) /= 1.0e1;
    // R(2, 2) *= 4;
    Q.setIdentity();
    Q(2, 2) = 2;
    Q(3, 3) = 1.0e2;

    A.setIdentity(LQR_STATES, LQR_STATES);
    B.setIdentity(LQR_STATES, LQR_INPUTS);
    B *= timeStep;

    // std::cout << Q << "\n" << R << "\n" << A << "\n" << B << "\n";

    pathLogs.setFileName("logPath.csv");
    pathLogs.open(QIODevice::WriteOnly | QIODevice::Text |QIODevice::Append);
}

Eigen::Matrix<double, LQR_INPUTS, 1> PathTracking::lqrTracking(RobotState _state,
                                                               QVector<QVector2D> _path,
                                                               QVector<float> _angle,
                                                               QVector<float> _speedProfile,
                                                               int _id)
{

    // QTextStream outr(&pathLogs);
    // outr << "PathX;";
    // for(QVector2D point : _path)
    // {
    //     outr << point.x() << ";";
    // }
    // outr << "\nPathY;";
    // for(QVector2D point : _path)
    // {
    //     outr << point.y() << ";";
    // }
    // outr << "\nAngle;";
    // for(float ang : _angle)
    // {
    //     outr << ang << ";";
    // }
    // outr << "\nSpeed;";
    // for(float speed : _speedProfile)
    // {
    //     outr << speed << ";";
    // }
    // outr << "\n";
    if(_path.isEmpty() || _angle.isEmpty() || _speedProfile.isEmpty())
    {
        return Eigen::MatrixXd::Zero(LQR_INPUTS, 1);
    }
    if(_path.size() != _angle.size() || _path.size() != _speedProfile.size() ||
        _angle.size() != _speedProfile.size())
    {
        return Eigen::MatrixXd::Zero(LQR_INPUTS, 1);
    }

    int index = findNearestIndex(_state, _path, _angle,
                                 lateralError, frontalError);

    angularError = adjustAngle(_angle.at(index) - _state.theta);

//     qInfo() << "Beta" << angularError * 180.0/M_PI <<
//                "Traj" << _angle.at(index) * 180.0/M_PI <<
//                "Robo" << _state.theta * 180.0 / M_PI <<
//                "v" << _state.v <<
//                "e" << lateralError <<
//                "f" << frontalError <<
//                "dt" << timeStep;


    Eigen::Matrix<double, LQR_INPUTS, LQR_STATES> K = lqr();

    x << (_state.x - _path.at(index).x()) / 1e3,
         (_state.y - _path.at(index).y()) / 1e3,
         _state.theta - _angle.at(index),
         _state.v - _speedProfile.at(index);

    // if(_id == 3)
    // {
    //      // std::cout << x << "\n\n";
    //      qDebug() << index;
    // }

    return -K * x;
}

Eigen::Matrix<double, LQR_STATES, LQR_STATES> PathTracking::solveDARE()
{
    Eigen::Matrix<double, LQR_STATES, LQR_STATES> xQ = Q, xNext = Q;
    int maxIteration = 10;
    float error = 0.01;

    for(int i = 0; i < maxIteration; ++i)
    {
        xNext = A.transpose() * xQ * A - A.transpose() * xQ * B *
                (R + B.transpose() * xQ * B).inverse() *
                B.transpose() * xQ * A + Q;
        if(qAbs((xNext - xQ).maxCoeff()) < error)
        {
            break;
        }

        xQ = xNext;
    }
    return xNext;
}

Eigen::Matrix<double, LQR_INPUTS, LQR_STATES> PathTracking::lqr()
{
    Eigen::Matrix<double, LQR_STATES, LQR_STATES> dare = solveDARE();
    Eigen::Matrix<double, LQR_INPUTS, LQR_STATES> K = (B.transpose() * dare * B + R).inverse() *
                                                      (B.transpose() * dare * A);
    return K;
}

int PathTracking::findNearestIndex(RobotState _state, QVector<QVector2D> _path,
                                   QVector<float> _angle,
                                   float &_lateralError, float &_frontalError)
{
    int index = 0;

    QVector2D robotPos = QVector2D(_state.x, _state.y);
    float minDistance = 9999999, distance;

    for(int i = 0; i < _path.size(); ++i)
    {
        distance = robotPos.distanceToPoint(_path.at(i));
        if(distance < minDistance)
        {
            minDistance = distance;
            index = i;
        }
    }

    index = qMin(index + 10, _path.length()-1);
    QVector2D error = _path.at(index) - robotPos;
    _lateralError = error.length() / 1e3; // Conversion from [mm] to [m]

    if(adjustAngle(_angle.at(index) - qAtan2(error.y(), error.x())) < 0)
    {
        _lateralError *= -1;
    }

    int futureIndex = qMin(index + 5, _path.length()-1);
    _frontalError = sqrt(pow(_state.x - _path.at(futureIndex).x(), 2) +
                         pow(_state.y - _path.at(futureIndex).y(), 2)) / 1e3;

    return index;
}

double PathTracking::adjustAngle(double angle)
{
    if(angle < 0)
    {
        angle += 2 * M_PI;
    }

    int multiples = angle / (2 * M_PI);
    angle -= multiples * 2 * M_PI;

    if(angle > M_PI)
    {
        angle -= 2 * M_PI;
    }

    return angle;
}
