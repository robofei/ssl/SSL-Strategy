#ifndef PATHTRACKING_H
#define PATHTRACKING_H
#include "Constantes_e_Funcoes_Auxiliares/macros.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/Dense>
#include <QVector>
#include <QVector2D>
#include <QDebug>
#include <QFile>


#define LQR_STATES 4
#define LQR_INPUTS 4

class PathTracking
{

public:
    PathTracking();

    typedef struct RobotState
    {
        float x;
        float y;
        float theta;
        float v;
    }RobotState;

    Eigen::Matrix<double, LQR_INPUTS, 1> lqrTracking(RobotState _state,
                                                     QVector<QVector2D> _path,
                                                     QVector<float> _angle,
                                                     QVector<float> _speedProfile,
                                                     int _id = -1);

private:
    Eigen::Matrix<double, LQR_STATES, LQR_STATES> A;
    Eigen::Matrix<double, LQR_STATES, LQR_INPUTS> B;
    Eigen::Matrix<double, LQR_STATES, LQR_STATES> Q;
    Eigen::Matrix<double, LQR_INPUTS, LQR_INPUTS> R;
    Eigen::Matrix<double, LQR_STATES, 1> x;
    Eigen::Matrix<double, LQR_INPUTS, 1> u;
    float timeStep;

    QFile pathLogs;

    float lateralError;
    float prevLateralError;
    float angularError;
    float prevAngularError;
    float frontalError;
    float prevFrontalError;

    Eigen::Matrix<double, LQR_STATES, LQR_STATES> solveDARE();
    Eigen::Matrix<double, LQR_INPUTS, LQR_STATES> lqr();
    int findNearestIndex(RobotState _state, QVector<QVector2D> _path,
                         QVector<float> _angle, float &_lateralError,
                         float &_frontalError);
    double adjustAngle(double angle);

};

#endif // PATHTRACKING_H
