﻿/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "robotmovement.h"
#include "Config/pidconfig.h"
#include "Config/robofeiconfig.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Movimentacao/pathtracking.h"
#include "qelapsedtimer.h"
#include "qvector2d.h"
#include "qvector3d.h"

#include <Eigen/LU>
#include <QLineF>
#include <cstddef>
#include <math.h>

MovimentacaoRobo::MovimentacaoRobo()
{
    currentPacket.reset(new MotionFeedbackPacket());
    currentPathPacket.reset(new MotionPathFeedback());

    bAtivo = false;
    bRecebeuPrimeiroPacote = false;
    velocidadesRobo.clear();
    velocidadesRobo.append(QVector3D(0, 0, 0));
    velocidadesRobo.append(QVector3D(0, 0, 0));
    velocidadesRobo.append(QVector3D(0, 0, 0));

    posicoesRobo.clear();
    posicoesRobo.append(QVector3D(0, 0, 0));
    posicoesRobo.append(QVector3D(0, 0, 0));
    posicoesRobo.append(QVector3D(0, 0, 0));

    fileX.close();
    dadosX = "Time(s);Vx(m/s);Vy(m/s);Vw(m/"
             "s);PosX(m);PosY(m);Ang(rad);PosPred10X(m);"
             "PosPred10Y(m);AngPred10(rad);VelPred10X(m/s);VelPred10Y(m/s);"
             "VelPredW(rad/s)\n";
    // dadosControle =
    //     "Tempo[s];SetPointX[m];SetPointY[m];SetPointW[rad];PosX[m];PosY[m];"
    //     "Angulo[rad];SinalControleX;SinalControleY;SinalControleW\n";
    dadosControle = "Tempo[s];PosX[m];PosY[m];Angulo[rad];VelX[m/s];VelY[m/"
                    "s];\n";
    etmTempoDadosControle.start();

    modelARX.readModelFile("../../include/Movimentacao/ARXModel.arx");

    //    pidConfig.create("pid_config.json");
    defaultConfig.reset(new PIDConfig("PID"));
    goalieConfig.reset(new PIDConfig("PID"));

    defaultConfig->read("pid_config.json");
    goalieConfig->read("pid_config_goalie.json");

    for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
    {
        setConfig(PID[n], defaultConfig);
        // Fuzzy[n].initialize();
    }

    PIDSimulacao.initialize();

    tmrLoopControle.reset();
    fFPSControle = 0;
    iFPSCounter.clear();

    simulador.reset();
    rdRadio.reset();

    useSafeController = true;
    prsbcComputationalTime.clear();
}

float MovimentacaoRobo::fGetFPSControle()
{
    return fFPSControle;
}

bool MovimentacaoRobo::bRadioConectado()
{
    return rdRadio->spPorta->isOpen();
}

MovimentacaoRobo::~MovimentacaoRobo()
{
    fileDadosControle.setFileName("LogControle.csv");
    fileDadosControle.open(QIODevice::ReadOnly | QIODevice::Text |
                           QIODevice::ReadWrite);

    if (fileDadosControle.isOpen())
    {
        fileDadosControle.flush();
        QTextStream stream(&fileDadosControle);
        stream << dadosControle;

        fileDadosControle.close();
        dadosControle.clear();
    }
}

void MovimentacaoRobo::vCalculaAngulosControle(float& targetAngle)
{
    // Converte o angulo atual do robo para ser positivo e entre -180 e 180
    // graus
    //    clsAuxiliar::vConverteAnguloRobo(anguloRobo);
    //    clsAuxiliar::vConverteAnguloRobo(anguloBackUp);

    while (targetAngle < -M_PI)
        targetAngle += (float)2. * M_PI;
    while (targetAngle > M_PI)
        targetAngle -= (float)2. * M_PI;
}

void MovimentacaoRobo::vEnviaComandosGrSim()
{
    QList<ROBO_SIM> simRobos;
    for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
    {
        const Atributos atbRobo(
            acMovimentacao.allies->getPlayer(n)->atbRetornaRobo());

        if (atbRobo.bDetectadoVisao)
        {
            simRobos.append(ROBO_SIM(atbRobo));
        }
    }

    // Envia o pacote com os dados de todos os robôs para o simulador
    simulador->send_robot_packet(simulador->create_packet_robot(simRobos));
}

void MovimentacaoRobo::vEnviaComandosRadio()
{
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        Atributos atbRobo(
            acMovimentacao.allies->getPlayer(id)->atbRetornaRobo());

        if (atbRobo.bDetectadoVisao == false)
        {
            atbRobo.cmd_vx = 0;
            atbRobo.cmd_vy = 0;
            atbRobo.cmd_w = 0.5;
            atbRobo.roller = false;
            atbRobo.cmd_rollerspd = 0;
        }
        rdRadio->bSendData(atbRobo); // Envia dado
    }
}

void MovimentacaoRobo::vAtualizarFPS()
{
    fFPSControle = 1.f / (etmTimerFPSControle.nsecsElapsed() / 1e9);
    iFPSCounter.prepend(qRound(fFPSControle));
    fFPSControle = Auxiliar::iCalculaFPS(iFPSCounter);
    etmTimerFPSControle.restart();
}

QVector2D MovimentacaoRobo::vt2dDecompoeVelocidadeRobo(QVector2D tempVel,
                                                       Atributos& _robot)
{
    float anguloRobo = _robot.rotation;

    float anguloY = 0.0, anguloX = 0.0;
    float _vMax =
        tempVel.length(); // sqrt(pow(tempVel.x(),2) + pow(tempVel.y(),2));

    QVector2D vetorXRobo, vetorYRobo(qCos(anguloRobo), qSin(anguloRobo));

    vetorXRobo =
        Auxiliar::vt2dRotaciona(_robot.vt2dPosicaoAtual,
                                vetorYRobo + _robot.vt2dPosicaoAtual, -90) -
        _robot.vt2dPosicaoAtual;

    anguloX = Auxiliar::dAnguloVetor1Vetor2(vetorXRobo, tempVel) * M_PI / 180;
    anguloY = Auxiliar::dAnguloVetor1Vetor2(vetorYRobo, tempVel) * M_PI / 180;

    tempVel.setX(_vMax * qCos(anguloX));
    tempVel.setY(_vMax * qCos(anguloY));

    return tempVel;
}

QVector2D MovimentacaoRobo::vt2dDecompoeVelocidadeRobo(QVector2D tempVel,
                                                       const Robo* _robot)
{
    float anguloRobo = _robot->fAnguloAtual();

    float anguloY = 0.0, anguloX = 0.0;
    float _vMax =
        tempVel.length(); // sqrt(pow(tempVel.x(),2) + pow(tempVel.y(),2));

    QVector2D vetorXRobo, vetorYRobo(qCos(anguloRobo), qSin(anguloRobo));

    vetorXRobo =
        Auxiliar::vt2dRotaciona(_robot->vt2dPosicaoAtual(),
                                vetorYRobo + _robot->vt2dPosicaoAtual(), -90);
    vetorXRobo = vetorXRobo - _robot->vt2dPosicaoAtual();

    anguloX = Auxiliar::dAnguloVetor1Vetor2(vetorXRobo, tempVel) * M_PI / 180;
    anguloY = Auxiliar::dAnguloVetor1Vetor2(vetorYRobo, tempVel) * M_PI / 180;

    tempVel.setX(_vMax * qCos(anguloX));
    tempVel.setY(_vMax * qCos(anguloY));

    return tempVel;
}

void MovimentacaoRobo::vControleMatLab(Robo* _rbtRobo, bool bAmbiente)
{
    QVector2D dP;
    float Vx, Vy, Vw, anguloDestino, anguloRobo;
    int id = _rbtRobo->iID();

    // Se o caminho esta vazio a velocidade é zero, Se o robo esta fora da visao
    // a velocidade é zero
    // if (_rbtRobo->path->bIsEmpty() || !_rbtRobo->bRoboDetectado())
    // {
    //     _rbtRobo->vSetMaxLinearVelocity(0);
    //     _rbtRobo->vAndarXY(0, 0, 0, 0);
    //     return;
    // }

    // Calculo do sinal de controle
    // Set model inputs here
    ExtU_ControlePosicao_T inputs;
    QVector2D nextPoint =
        _rbtRobo->path->followPoint(_rbtRobo->vt2dPosicaoAtual());

    if (nextPoint.length() > 1e10)
    {
        nextPoint = _rbtRobo->vt2dDestino();
    }

    // if (_rbtRobo->bArrived())
    if (_rbtRobo->vt2dPosicaoAtual().distanceToPoint(_rbtRobo->vt2dDestino()) <
        300)
    {
        anguloDestino = fCalculaAnguloRoboPonto(
            _rbtRobo->vt2dPosicaoAtual(), _rbtRobo->vt2dPontoAnguloDestino());
    }
    else
    {
        anguloDestino =
            fCalculaAnguloRoboPonto(_rbtRobo->vt2dPosicaoAtual(), nextPoint);
        // nextPoint = _rbtRobo->path->getPathAt(pathHalf, ok);
    }
    anguloRobo = _rbtRobo->fAnguloAtual();
    Auxiliar::vConverteAnguloRobo(anguloRobo);
    Auxiliar::vConverteAnguloRobo(anguloDestino);

    // Faz com que o robô gire a menor quantidade possível
    if (qAbs(anguloDestino - anguloRobo) > M_PI)
    {
        if (anguloDestino < anguloRobo)
        {
            anguloDestino += 2 * M_PI;
        }
        else
        {
            anguloDestino -= 2 * M_PI;
        }
    }

    // Setpoint
    inputs.PosX = nextPoint.x() / 1e3;
    inputs.PosY = nextPoint.y() / 1e3;
    inputs.PosAngular = anguloDestino; // Angulo destino

    // Realimentação
    inputs.ReAlimX = _rbtRobo->vt2dPosicaoAtual().x() / 1e3;
    inputs.ReAlimY = _rbtRobo->vt2dPosicaoAtual().y() / 1e3;
    inputs.ReAlimAngular = anguloRobo; // Angulo atual do robô

    // Criação de log
    if (_rbtRobo->iID() == 0)
    {
        // dadosControle =
        // "Tempo[s];PosX[m];PosY[m];Angulo[rad];VelX[m/s];VelY[m/s]\n";
        dadosControle += QString("%1;%2;%3;%4;%5;%6\n")
                             .arg(etmTempoDadosControle.nsecsElapsed() / 1e9)
                             .arg(_rbtRobo->vt2dPosicaoAtual().x())
                             .arg(_rbtRobo->vt2dPosicaoAtual().y())
                             .arg(_rbtRobo->fAnguloAtual())
                             .arg(_rbtRobo->vt2dVelocidadeXY().x())
                             .arg(_rbtRobo->vt2dVelocidadeXY().y());
    }

    PID[id].setExternalInputs(&inputs);

    // Step the model
    PID[id].step();

    // Get model outputs here
    const ExtY_ControlePosicao_T& outputs = PID[id].getExternalOutputs();
    dP.setX(outputs.SinalControleX);
    dP.setY(outputs.SinalControleY);
    Vw = outputs.SinalControleW;

    float velMax = _rbtRobo->fMaxLinearVelocity();

    if (velMax >= 0 && velMax < dP.length())
        dP = dP / dP.length() * velMax;

    if (abs(Vw) > _rbtRobo->fMaxAngularVelocity())
        Vw = Vw / abs(Vw) * _rbtRobo->fMaxAngularVelocity();

    // QVector2D nextWaypoint =
    // _rbtRobo->path->vt2dGetNextPoint(_rbtRobo->vt2dPosicaoAtual());
    QVector2D dPAux = nextPoint - _rbtRobo->vt2dPosicaoAtual();
    dPAux.normalize();

    dP = dPAux * dP.length();
    QVector2D velNotDecomposed = dP;

    // if (bAmbiente == CAMPO_REAL)
    // {
    //     // dP = dP * 100/dVelocidadeMaximaReal;
    //     // Vw = Vw * 100/dRotacaoMaximaReal;
    // }
    // else
    // {
    //     // No controlador ajustado para o firmware do ITA, o esforço de
    //     // controle em W é 10x mais baixo que o usual
    //     // Vw *= 10;
    // }

    // Decompôe a velocidade gerada (em coordenadas do campo) para o frame de
    // referência do robô
    if (!useSafeController)
    {
        dP = this->vt2dDecompoeVelocidadeRobo(dP, _rbtRobo);
    }
    Vx = dP.x();
    Vy = dP.y();

    currentPathPacket->vUpdateInfo(*_rbtRobo->path.get(), id);
    _rbtRobo->vAndarXY(Vx, Vy, Vw, _rbtRobo->fMaxLinearVelocity(),
                       velNotDecomposed);
}

void MovimentacaoRobo::vControleMatLabFuzzy(Robo* _rbtRobo, bool bAmbiente)
{
    QVector2D dP;
    float Vx, Vy, Vw, anguloDestino, anguloBackUpRobo, anguloRobo;
    int id = _rbtRobo->iID();

    // Se o caminho esta vazio a velocidade é zero, Se o robo esta fora da visao
    // a velocidade é zero
    if (_rbtRobo->path->bIsEmpty() || !_rbtRobo->bRoboDetectado())
    {
        _rbtRobo->vSetMaxLinearVelocity(0);
        _rbtRobo->vAndarXY(0, 0, 0, 0);

        return;
    }

    QVector2D nextWaypoint =
        _rbtRobo->path->vt2dGetNextPoint(_rbtRobo->vt2dPosicaoAtual());
    anguloDestino = fCalculaAnguloRoboPonto(_rbtRobo->vt2dPosicaoAtual(),
                                            _rbtRobo->vt2dPontoAnguloDestino());
    anguloBackUpRobo = _rbtRobo->fAnguloBackup();
    anguloRobo = _rbtRobo->fAnguloAtual();

    vCalculaAngulosControle(anguloDestino);

    // Calculo do sinal de controle
    // Set model inputs here
    ExtU_ControlePosicao_T inputs;
    // Setpoint
    inputs.PosX = inputs.PosY = 0;
    inputs.PosAngular = (anguloDestino); // Angulo destino

    // Realimentação
    inputs.ReAlimX = inputs.ReAlimY = 0;
    inputs.ReAlimAngular =
        (anguloRobo - anguloBackUpRobo); // Angulo atual do robô

    PID[id].setExternalInputs(&inputs);

    int idPontoAnterior = _rbtRobo->path->getPath().length();
    if (idPontoAnterior >= _rbtRobo->path->getFullPath().length())
    {
        idPontoAnterior = _rbtRobo->path->getFullPath().length() - 1;
    }

    bool ok;
    QVector2D v1 = _rbtRobo->path->getFullPathAt(idPontoAnterior, ok) -
                   nextWaypoint,
              v2 = _rbtRobo->vt2dPosicaoAtual() - nextWaypoint;

    int desvio = round(Auxiliar::dAnguloVetor1Vetor2(v1, v2) * 180 / M_PI);
    int idOponente =
        acMovimentacao.iAchaInimigoProximoPonto(_rbtRobo->vt2dPosicaoAtual());
    int distanciaOponente = 1000,
        distanciaTotal = round(_rbtRobo->vt2dDestino().distanceToPoint(
            _rbtRobo->vt2dPosicaoAtual()));

    if (idOponente != -1)
    {
        distanciaOponente = round(_rbtRobo->vt2dPosicaoAtual().distanceToPoint(
            acMovimentacao.opponents->getPlayer(idOponente)
                ->vt2dPosicaoAtual()));
    }

    if (distanciaTotal > 1000)
    {
        distanciaTotal = 1000;
    }
    Fuzzy[id].ControleFuzzy_U.desvio = desvio;
    //     Fuzzy[id].ControleFuzzy_U.deslocamento =
    //     _rbtRobo->path->fGetLength();
    Fuzzy[id].ControleFuzzy_U.distancia_opo = distanciaOponente;
    Fuzzy[id].ControleFuzzy_U.distancia_tot = distanciaTotal;

    // Step the model
    PID[id].step(); // utilizado só para o controle do ângulo
    Fuzzy[id].step();

    // Get model outputs here
    const ExtY_ControlePosicao_T& outputs = PID[id].getExternalOutputs();
    Vw = outputs.SinalControleW;

    float velMax = _rbtRobo->fMaxLinearVelocity();
    float modVelocidade = Fuzzy[id].ControleFuzzy_Y.velocidade;

    if (velMax >= 0 && velMax < modVelocidade)
        modVelocidade = velMax;

    if (abs(Vw) > _rbtRobo->fMaxAngularVelocity())
        Vw = Vw / abs(Vw) * _rbtRobo->fMaxAngularVelocity();

    QVector2D dPAux = nextWaypoint - _rbtRobo->vt2dPosicaoAtual();
    dPAux.normalize();

    dP = dPAux * modVelocidade;
    QVector2D velNotDecomposed = dP;

    if (bAmbiente == CAMPO_REAL)
    {
        dP = dP * 100 / globalConfig.robotVelocities.linear.max;
        Vw = Vw * 100 / globalConfig.robotVelocities.angular.max;
    }

    // Decompôe a velocidade gerada (em coordenadas do campo) para o frame de
    // referência do robô
    dP = this->vt2dDecompoeVelocidadeRobo(dP, _rbtRobo);
    Vx = dP.x();
    Vy = dP.y();

    _rbtRobo->vAndarXY(Vx, Vy, Vw, _rbtRobo->fMaxLinearVelocity(),
                       velNotDecomposed);
}

void MovimentacaoRobo::trackPath(Robo* _robot, bool _environment)
{
    float vx = 0, vy = 0, vw = 0, acc;
    QVector2D robotCommand;

    if (!_robot->bArrived(500) && !_robot->path->bIsEmpty())
    {
        PathTracking::RobotState state;
        const qint8 id = _robot->iID();
        state.theta = _robot->fAnguloAtual();
        state.x = _robot->vt2dPosicaoAtual().x();
        state.y = _robot->vt2dPosicaoAtual().y();
        state.v = _robot->dModuloVelocidade();

        auto angles = _robot->path->getPathAngle();
        angles.clear();
        angles.fill(fCalculaAnguloRoboPonto(_robot->vt2dPosicaoAtual(),
                                            _robot->vt2dPontoAnguloDestino()),
                    _robot->path->getFullPath().size());
        _robot->path->setPathAngle(angles);
        Eigen::Matrix<double, LQR_INPUTS, 1> u = pathTracking[id].lqrTracking(
            state, _robot->path->getFullPath(), _robot->path->getPathAngle(),
            _robot->path->getSpeedProfile(), id);

        float dt = globalConfig.samplingTime.motion * 1e-3;

        const double maxLinear = _robot->fMaxLinearVelocity(),
                     maxAngular = _robot->fMaxAngularVelocity();

        vx = qMax(qMin(u(0, 0), maxLinear), -maxLinear);
        vy = qMax(qMin(u(1, 0), maxLinear), -maxLinear);
        vw = qMax(qMin(u(2, 0), maxAngular), -maxAngular);
        acc = u(3, 0);

        QVector2D localCmd(vx, vy);
        if (!_robot->bArrived(40))
        {
            localCmd = localCmd.normalized() *
                       (acc * dt + state.v); // usa o v do perfil de velocidade
        }

        if (localCmd.length() <= 0.0)
        {
            vControleMatLab(_robot, acMovimentacao.bTipoAmbiente);
        }
        else
        {
            if (useSafeController)
            {
                _robot->vAndarXY(localCmd.x(), localCmd.y(), vw,
                                 _robot->fMaxLinearVelocity(), localCmd);
            }
            else
            {
                robotCommand = vt2dDecompoeVelocidadeRobo(localCmd, _robot);
                _robot->vAndarXY(robotCommand.x(), robotCommand.y(), vw,
                                 _robot->fMaxLinearVelocity(), localCmd);
            }
        }
    }
    else
    {
        vControleMatLab(_robot, _environment);
    }
}

void MovimentacaoRobo::applySafeController()
{
    QElapsedTimer prsbcTimer;
    prsbcTimer.start();

    int nAlly = acMovimentacao.allies->getRobotsDetected(),
        nOpponents = acMovimentacao.opponents->getRobotsDetected();
    safeController.config(1, 4, 200e-3 / 2, 0.95, nAlly);

    Eigen::MatrixXd u, x, v, obstaclesU, obstacles, uSafe, stateNoiseAllies,
        inputNoiseAllies, stateNoiseObstacles, inputNoiseObstacles;

    uSafe.resize(2, nAlly);
    u.resize(2, nAlly);
    v.resize(1, nAlly);
    x.resize(2, nAlly);
    stateNoiseAllies.resize(2, nAlly);
    inputNoiseAllies.setZero(2, nAlly);

    Robo* robot;
    int usedRobots = 0;
    QVector3D robotU;
    bool ballIsObstacle = false;
    for (int id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robot = acMovimentacao.allies->getPlayer(id);
        if (robot->bRoboDetectado())
        {
            robotU = robot->vt3dPegaVelocidades();
            u(0, usedRobots) = robotU.x();
            u(1, usedRobots) = robotU.y();

            v(0, usedRobots) = robot->dModuloVelocidade();

            x(0, usedRobots) = robot->vt2dPosicaoAtual().x() / 1e3;
            x(1, usedRobots) = robot->vt2dPosicaoAtual().y() / 1e3;

            stateNoiseAllies(0, usedRobots) = 0.00;
            stateNoiseAllies(1, usedRobots) = 0.00;
            usedRobots++;

            qint8 nearestRobot = acMovimentacao.iAchaRoboProximoBola();

            if (nearestRobot == robot->iID() &&
                robot->bPegaIgnorarBola() == bConsideraBola &&
                robot->vt2dPosicaoAtual().distanceToPoint(
                    acMovimentacao.vt2dPosicaoBola()) < 1e3)
            {
                ballIsObstacle = true;
            }
        }
    }

    obstaclesU.resize(2, nOpponents);
    obstacles.resize(2, nOpponents);
    stateNoiseObstacles.resize(2, nOpponents);
    inputNoiseObstacles.setZero(2, nOpponents);

    if (ballIsObstacle)
    {
        obstaclesU.resize(2, nOpponents + 1);
        obstacles.resize(2, nOpponents + 1);
        stateNoiseObstacles.resize(2, nOpponents + 1);
        inputNoiseObstacles.setZero(2, nOpponents + 1);
    }

    usedRobots = 0;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robot = acMovimentacao.opponents->getPlayer(id);
        if (robot->bRoboDetectado())
        {
            obstacles(0, usedRobots) = robot->vt2dPosicaoAtual().x() / 1e3;
            obstacles(1, usedRobots) = robot->vt2dPosicaoAtual().y() / 1e3;
            obstaclesU(0, usedRobots) = robot->vt2dVelocidadeXY().x();
            obstaclesU(1, usedRobots) = robot->vt2dVelocidadeXY().y();

            stateNoiseObstacles(0, usedRobots) = 0.05;
            stateNoiseObstacles(1, usedRobots) = 0.05;
            usedRobots++;
        }
    }
    if (ballIsObstacle)
    {
        obstacles(0, usedRobots) = acMovimentacao.vt2dPosicaoBola().x() / 1e3;
        obstacles(1, usedRobots) = acMovimentacao.vt2dPosicaoBola().y() / 1e3;
        obstaclesU(0, usedRobots) = acMovimentacao.vt2dVelocidadeBola().x();
        obstaclesU(1, usedRobots) = acMovimentacao.vt2dVelocidadeBola().y();

        stateNoiseObstacles(0, usedRobots) = 0.05;
        stateNoiseObstacles(1, usedRobots) = 0.05;
    }

    uSafe = safeController.getSafeInput(
        u, x, v, obstaclesU, obstacles, stateNoiseAllies, inputNoiseAllies,
        stateNoiseObstacles, inputNoiseObstacles, globalConfig.robotVelocities.linear.limit);

    usedRobots = 0;
    QVector2D cmd, localCmd;
    for (int id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robot = acMovimentacao.allies->getPlayer(id);
        if (robot->bRoboDetectado())
        {
            robotU = robot->vt3dPegaVelocidades();
            cmd.setX(uSafe(0, usedRobots));
            cmd.setY(uSafe(1, usedRobots));
            // if (cmd.length() > robot->fMaxLinearVelocity())
            // {
            //     cmd = cmd.normalized() * robot->fMaxLinearVelocity();
            // }

            localCmd = vt2dDecompoeVelocidadeRobo(cmd, robot);
            robot->vAndarXY(localCmd.x(), localCmd.y(), robotU.z(),
                            robot->fMaxLinearVelocity(), cmd);
            robot->prsbcConstraint = safeController.getRobotConstraint(id);
            usedRobots++;
        }
    }
    prsbcComputationalTime.prepend(prsbcTimer.nsecsElapsed() / 1e6);

    // if (prsbcComputationalTime.size() > 10)
    // {
    //     prsbcComputationalTime.pop_back();
    // }
}

QVector3D MovimentacaoRobo::vt3dControleSimulacao(Robo* _rbtRobo)
{
    QVector2D dP;
    float Vx, Vy, Vw, anguloDestino, anguloBackUpRobo, anguloRobo;

    // Se o caminho esta vazio a velocidade é zero, Se o robo esta fora da visao
    // a velocidade é zero
    if (_rbtRobo->path->bIsEmpty() || !_rbtRobo->bRoboDetectado())
    {
        _rbtRobo->vSetMaxLinearVelocity(0);
        _rbtRobo->vAndarXY(0, 0, 0, 0);

        return {0, 0, 0};
    }

    QVector2D nextWaypoint =
        _rbtRobo->path->vt2dGetNextPoint(_rbtRobo->vt2dPosicaoAtual());
    anguloDestino = fCalculaAnguloRoboPonto(_rbtRobo->vt2dPosicaoAtual(),
                                            _rbtRobo->vt2dPontoAnguloDestino());
    anguloBackUpRobo = _rbtRobo->fAnguloBackup();
    anguloRobo = _rbtRobo->fAnguloAtual();

    vCalculaAngulosControle(anguloDestino);

    // Calculo do sinal de controle
    // Set model inputs here
    ExtU_ControlePosicao_T inputs;
    bool ok;
    // Setpoint
    inputs.PosX =
        (nextWaypoint.x() - _rbtRobo->path->getFullPathAt(0, ok).x()) / 1e3;
    inputs.PosY =
        (nextWaypoint.y() - _rbtRobo->path->getFullPathAt(0, ok).y()) / 1e3;
    inputs.PosAngular = (anguloDestino); // Angulo destino

    // Realimentação
    inputs.ReAlimX = (_rbtRobo->vt2dPosicaoAtual().x() -
                      _rbtRobo->path->getFullPathAt(0, ok).x()) /
                     1e3;
    inputs.ReAlimY = (_rbtRobo->vt2dPosicaoAtual().y() -
                      _rbtRobo->path->getFullPathAt(0, ok).y()) /
                     1e3;
    inputs.ReAlimAngular =
        (anguloRobo - anguloBackUpRobo); // Angulo atual do robô

    PIDSimulacao.setExternalInputs(&inputs);

    // Step the model
    PIDSimulacao.step();

    // Get model outputs here
    const ExtY_ControlePosicao_T& outputs = PIDSimulacao.getExternalOutputs();
    dP.setX(outputs.SinalControleX);
    dP.setY(outputs.SinalControleY);
    Vw = outputs.SinalControleW;

    QVector2D dPAux = nextWaypoint - _rbtRobo->vt2dPosicaoAtual();
    dPAux.normalize();

    dP = dPAux * dP.length();
    //     atbAtributosRobo.vt2dVelocidadeNaoConvertida = dP;

    // Decompôe a velocidade gerada (em coordenadas do campo) para o frame de
    // referência do robô
    dP = this->vt2dDecompoeVelocidadeRobo(dP, _rbtRobo);
    Vx = dP.x();
    Vy = dP.y();

    _rbtRobo->vAndarXY(Vx, Vy, Vw, static_cast<int>(50));
    return {Vx, Vy, Vw};
}

void MovimentacaoRobo::vSimularModelo(int IDRobo)
{
    QVector3D posicaoModelo;
    Robo* backRobo = acMovimentacao.allies->getPlayer(IDRobo);

    velocidadesRobo.prepend(backRobo->vt3dPegaVelocidades());

    if (velocidadesRobo.size() > 20)
        velocidadesRobo.pop_back();

    posicoesRobo.prepend(QVector3D(backRobo->vt2dPosicaoAtual().x(),
                                   backRobo->vt2dPosicaoAtual().y(),
                                   backRobo->fAnguloAtual()));

    if (posicoesRobo.size() > 20)
        posicoesRobo.pop_back();

    QVector<QVector3D> backPos;
    backPos.clear();
    backPos.append(posicoesRobo);

    QVector<QVector3D> backVel;
    backVel.clear();
    backVel.append(velocidadesRobo);

    QVector<QVector2D> vt2dCaminho, vt2dBackupCaminho;
    vt2dCaminho.clear();
    vt2dBackupCaminho.clear();

    vt2dCaminho.append(
        acMovimentacao.allies->getPlayer(IDRobo)->path->getPath());
    vt2dBackupCaminho.append(
        acMovimentacao.allies->getPlayer(IDRobo)->path->getFullPath());

    for (int a = 0; a < iTamanhoTracoRobo; a++)
    {
        posicaoModelo = modelARX.calculateActualPosition(backPos, backVel);
        backRobo->vSetaPosicao(posicaoModelo.toVector2D());
        backRobo->vSetaAngulo(posicaoModelo.z());

        backPos.prepend(posicaoModelo);
        backPos.pop_back();

        QVector3D velControle = vt3dControleSimulacao(backRobo);

        backVel.prepend(velControle);
        backVel.pop_back();
    }

    acMovimentacao.vt3dPosicaoSimulacaoRobo.prepend(posicaoModelo);

    if (!backVel.isEmpty())
        acMovimentacao.vt3dVelocidadeSimulacaoRobo.prepend(QVector3D(
            backRobo->vt2dGetRawCommandVelocity(), backVel.constFirst().z()));

    if (acMovimentacao.vt3dPosicaoSimulacaoRobo.size() > 500)
        acMovimentacao.vt3dPosicaoSimulacaoRobo.pop_back();

    if (acMovimentacao.vt3dVelocidadeSimulacaoRobo.size() > 500)
        acMovimentacao.vt3dVelocidadeSimulacaoRobo.pop_back();
}

void MovimentacaoRobo::setConfig(ControlePosicaoModelClass& _pid,
                                 QSharedPointer<PIDConfig> _config)
{
    _pid.initialize();
    _pid.kPx = _config->kPx;
    _pid.kPy = _config->kPy;
    _pid.kPw = _config->kPw;
    _pid.kIx = _config->kIx;
    _pid.kIy = _config->kIy;
    _pid.kIw = _config->kIw;
    _pid.kDx = _config->kDx;
    _pid.kDy = _config->kDy;
    _pid.kDw = _config->kDw;
}

void MovimentacaoRobo::vReceiveVisionData_(const VisionPacket _packet)
{
    if (!bAtivo)
        return;
    if (bRecebeuPrimeiroPacote == false)
        bRecebeuPrimeiroPacote = true;

    acMovimentacao.vAtualizaDadosVisao(_packet);

    // Se a movimentação ja recebeu o primeiro pacote de visao, ja recebeu o
    // sinal para iniciar e o timer ainda nao foi iniciado -> Inicia o timer
    if (bRecebeuPrimeiroPacote == true && bAtivo == true && !tmrLoopControle)
    {
        // Inicializa o timer do loop de controle
        tmrLoopControle.reset(new QTimer(this));

        tmrLoopControle->setTimerType(Qt::PreciseTimer);

        // Conecta oo slot do loop com o timer
        connect(tmrLoopControle.get(), &QTimer::timeout, this,
                &MovimentacaoRobo::vMovimentacaoRobos);
        tmrLoopControle->start(globalConfig.samplingTime.motion);
        // tmrLoopControle->start(5);
    }

    /// FIXME isso vem da interface/referee
    //     acMovimentacao.allies->vSetTeamColor(_acAmbiente.allies->teamColor());
}

void MovimentacaoRobo::vRecebeInformacoesEstrategia(
    const AmbienteCampo& _acAmbiente)
{
    acMovimentacao.bTipoAmbiente = _acAmbiente.bTipoAmbiente;
    acMovimentacao.tsDadosTeste = _acAmbiente.tsDadosTeste;

    if (acMovimentacao.allies->iGetGoalieID() !=
        _acAmbiente.allies->iGetGoalieID())
    {
        for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
        {
            setConfig(PID[n], defaultConfig);
        }
        acMovimentacao.allies->vSetGoalieID(_acAmbiente.allies->iGetGoalieID());
        setConfig(PID[acMovimentacao.allies->iGetGoalieID()], goalieConfig);
        qInfo() << "[Movimentacao] Configurando PID goleiro!";
    }
    acMovimentacao.allies->vSetSide(_acAmbiente.allies->iGetSide());
    acMovimentacao.allies->vSetTeamColor(_acAmbiente.allies->teamColor());

    acMovimentacao.debugInfo->merge(*_acAmbiente.debugInfo.get());

    // if (acMovimentacao.allies->teamColor() == timeAmarelo)
    // {
    //     useSafeController = false;
    // }

    acMovimentacao.opponents->vSetGoalieID(
        _acAmbiente.opponents->iGetGoalieID());
    acMovimentacao.opponents->vSetSide(_acAmbiente.opponents->iGetSide());
    acMovimentacao.opponents->vSetTeamColor(_acAmbiente.opponents->teamColor());

    acMovimentacao.vSetRemainingPlayTime(_acAmbiente.iGetRemainingPlayTime());

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        Atributos atbRobo(_acAmbiente.allies->getPlayer(i)->atbRetornaRobo());
        acMovimentacao.allies->getPlayer(i)->vAtualizaDadosEstrategia(
            _acAmbiente.allies->getCPlayer(i));

        if (acMovimentacao.tsDadosTeste.tstTesteAtual == tstRobotController &&
            i == acMovimentacao.tsDadosTeste.iIDRoboTeste)
        {
            // Movimentação com o Joystick
            acMovimentacao.allies->getPlayer(i)->vSetaRobo(atbRobo);
            continue;
        }

        if (_acAmbiente.allies->getPlayer(i)->path->pathChanged())
        {
            acMovimentacao.allies->getPlayer(i)->path->vSetPath(
                _acAmbiente.allies->getPlayer(i)->path->getPath(),
                _acAmbiente.allies->getPlayer(i)->path->pathLength(), false);
            acMovimentacao.allies->getPlayer(i)->path->setPathAngle(
                _acAmbiente.allies->getPlayer(i)->path->getPathAngle());
            acMovimentacao.allies->getPlayer(i)->path->setSpeedProfile(
                _acAmbiente.allies->getPlayer(i)->path->getSpeedProfile());

            if (!acMovimentacao.allies->getPlayer(i)
                     ->path->getFullPath()
                     .isEmpty())
            {
                Fuzzy[i].ControleFuzzy_U.deslocamento =
                    acMovimentacao.allies->getPlayer(i)
                        ->path->getFullPath()
                        .constLast()
                        .distanceToPoint(acMovimentacao.allies->getPlayer(i)
                                             ->vt2dPosicaoAtual());
            }
        }

        acMovimentacao.opponents->getPlayer(i)->vAtualizaDadosEstrategia(
            _acAmbiente.opponents->getPlayer(i)->atbRetornaRobo());
    }
}

void MovimentacaoRobo::vRecebeDadosPortaSerial(const ConfiguracaoRadio& _config)
{
    if (rdRadio.isNull())
    {
        rdRadio.reset(new RadioBase);
    }

    if (_config.bConectar)
    {
        rdRadio->vSetaConfigs(_config.Porta, _config.baudRate);
        rdRadio->bConfigurarPortaSerial();
    }
    else
    {
        rdRadio->vFecharPorta();
    }
}

void MovimentacaoRobo::vAtivaThread(const bool _bAtivaThread)
{
    bAtivo = _bAtivaThread;
    if (bAtivo == false)
    {
        rdRadio.reset();
        //        fileX.setFileName("LogMov.csv");
        //        fileX.open(QIODevice::ReadOnly | QIODevice::Text |
        //        QIODevice::ReadWrite);

        //        if(fileX.isOpen())
        //        {
        //            fileX.flush();
        //            QTextStream stream(&fileX);
        //            stream << dadosX;
        //            fileX.close();
        //            dadosX.clear();
        //        }
        bRecebeuPrimeiroPacote = false;
        if (tmrLoopControle)
        {
            tmrLoopControle->stop();
            tmrLoopControle.reset();
        }
    }
}

void MovimentacaoRobo::vRecebeConfiguracoesSimulador(
    int portaControle, int portaRobo, const QString& strInterface)
{
    simulador.reset(
        new RoboFei_Simulator(portaControle, portaRobo, strInterface));
    connect(simulador.get(), &RoboFei_Simulator::_vSendRobotFeedback, this,
            &MovimentacaoRobo::vReceiveRobotFeedback_);
}

void MovimentacaoRobo::vMovimentacaoRobos()
{
    vAtualizarFPS();

    QElapsedTimer tempoLoop;
    tempoLoop.start();
    Atributos atbRobo;

    for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
    {
        // Teste de controlar o robô com o Joystick
        if (acMovimentacao.tsDadosTeste.tstTesteAtual == tstRobotController &&
            n == acMovimentacao.tsDadosTeste.iIDRoboTeste)
        {
            continue;
        }

        atbRobo = acMovimentacao.allies->getPlayer(n)->atbRetornaRobo();

        if (acMovimentacao.allies->getPlayer(n)->bRoboDetectado())
        {
            // if (acMovimentacao.allies->getPlayer(n)->bArrived(500) == false)
            // {
            //     trackPath(acMovimentacao.allies->getPlayer(n),
            //               acMovimentacao.bTipoAmbiente);
            // }
            // else
            // {
            vControleMatLab(acMovimentacao.allies->getPlayer(n),
                            acMovimentacao.bTipoAmbiente);
            // }
            // vSimularModelo(0);
            atbRobo.dTempoRobo += globalConfig.samplingTime.motion * 1e-3;
        }
        else
        {
            acMovimentacao.allies->getPlayer(n)->vAndarXY(0, 0, 1, 0);
            acMovimentacao.allies->getPlayer(n)->vDesligaRoller();
        }
    }
    if (useSafeController)
    {
        applySafeController();
    }

    if (simulador)
    {
        vEnviaComandosGrSim();
    }
    else
    {
        vEnviaComandosRadio();
    }
    currentPacket->vUpdateInfo(
        acMovimentacao.debugInfo.get(), acMovimentacao.allies.get(),
        acMovimentacao.opponents.get(), acMovimentacao.iGetRemainingPlayTime());

    emit vRetornaCaminhosAlterados(*currentPathPacket.get());
    emit vRetornaVelocidadesRobos(*currentPacket.get());

    acMovimentacao.debugInfo->clearAll();

    double tempoTotal = tempoLoop.nsecsElapsed() * 1e-6;

    if (tempoTotal < globalConfig.samplingTime.motion)
        return;

    // if(tempoTotal < 1.2*globalConfig.samplingTime.motion)
    //     qWarning() << "Tempo Movimentação " << QString::number(tempoTotal,
    //     'f', 2)
    //                << " ms";
    // else
    //     qCritical() << "Tempo Movimentação " << QString::number(tempoTotal,
    //     'f', 2)
    //                 << " ms";
}

void MovimentacaoRobo::SLOT_mudaPosicaoBolaSimulacao(QVector2D vt2dPosicao,
                                                     QVector2D vt2dVelocidade)
{
    /// \todo Essa função de mudar a posição da bola precisa ser adaptada com os
    /// novos protocolos.
    if (simulador)
    {
        simulador->send_control_packet(
            simulador->create_packet_ball(vt2dPosicao, vt2dVelocidade));
    }
}

void MovimentacaoRobo::SLOT_mudarPosicaoRoboSimulacao(bool amarelo,
                                                      QVector2D destino,
                                                      qint8 id)
{
    if (simulador)
    {
        simulador->send_control_packet(
            simulador->create_packet_robot_control(amarelo, id, destino));
    }
}

void MovimentacaoRobo::SLOT_mudarAnguloRoboSimulacao(bool amarelo, float angulo,
                                                     qint8 id)
{
    if (simulador)
    {
        simulador->send_control_packet(
            simulador->create_packet_robot_control(amarelo, id, angulo));
    }
}

void MovimentacaoRobo::vReceiveRobotFeedback_(qint8 _id, bool _ballSensor)
{
    static RadioFeedback feedback;
    if (_id < globalConfig.robotsPerTeam)
    {

        acMovimentacao.allies->getPlayer(_id)->vSetaKickSensor(_ballSensor);

        feedback.vSetKickSensor(_id, _ballSensor);
        emit _vSendSimulationFeedback(feedback);
    }
}

void MovimentacaoRobo::receiveComputationalTimeRequest(float& _time)
{
    _time = 0;

    for (float val : qAsConst(prsbcComputationalTime))
    {
        _time += val;
    }
    _time /= prsbcComputationalTime.size();
    prsbcComputationalTime.clear();
}

void MovimentacaoRobo::receivePrSBCConfigRequest(QString& _config)
{
    if (useSafeController)
    {
        _config = safeController.exportConfig();
    }
    else
    {
        _config = QString("PrSBC_Turned Off");
    }
}

float MovimentacaoRobo::fCalculaAnguloRoboPonto(QVector2D _posRobo,
                                                QVector2D _ponto)
{
    int _x = _ponto.x() - _posRobo.x();
    int _y = _ponto.y() - _posRobo.y();

    float anguloRoboPonto = qAtan2(_y, _x);

    return anguloRoboPonto;
}
