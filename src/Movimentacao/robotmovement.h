/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ROBOTMOVEMENT_H
#define ROBOTMOVEMENT_H

///
/// \file robotmovement.h
/// \brief \a MovimentacaoRobo
///

#include <QDebug>
#include <QElapsedTimer>
#include <QTimer>
#include <QVector2D>
#include <QVector3D>
#include <QtMath>

#include <algorithm>
#include <cmath>
#include <queue>
#include <set>
#include <stdlib.h>

#include "Ambiente/futbolenvironment.h"
#include "Config/globalconfig.h"
#include "Config/pidconfig.h"
#include "Movimentacao/Controle/ControleFuzzy.h"
#include "Movimentacao/Controle/ControlePosicao.h"
#include "Movimentacao/Modelos/arxmodel.h"
#include "Movimentacao/pathtracking.h"
#include "Movimentacao/motionfeedbackpacket.h"
#include "Movimentacao/motionpathfeedback.h"
#include "ObstacleAvoidance/prsbc.h"
#include "Radio/radiobase.h"
#include "Simulador/simulator.h"

#include "qvector.h"

/**
 * @brief Classe que realiza o controle de posição dos robôs no campo.
 *
 */
class MovimentacaoRobo : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Construtor da classe.
     */
    MovimentacaoRobo();

    /**
     * @brief Destrutor da classe.
     *
     */
    ~MovimentacaoRobo();

    /**
     * @brief Retorna a taxa de FPS da thread de movimentação
     * @return FPS
     */
    float fGetFPSControle();

    /**
     * @brief Retorna true se o rádio está conectado
     * @return
     */
    bool bRadioConectado();

    QScopedPointer<RadioBase> rdRadio;
    AmbienteCampo acMovimentacao; /**< Objeto do ambiente de campo. */
    QSharedPointer<MotionFeedbackPacket> currentPacket;
    QSharedPointer<MotionPathFeedback> currentPathPacket;
    ARXModel modelARX; /**< Objeto da classe para simular o modelo ARX. */
    QVector<QVector3D> velocidadesRobo; /**< Velocidades anteriores do robô,
                                                para usar com o modelo ARX*/
    QVector<QVector3D> posicoesRobo; /**< Posições anteriores do robô, para usar
                                       com o modelo ARX*/

    QFile fileX, fileY; /**< Arquivos para criar o log de movimentação. */
    QString dadosX;
    QString dadosY;        /**< Armazena os dados que serão colocados
                               no log.*/
    QString dadosControle; /**< Armazena os dados que serão colocados no log
                                        do controlador.*/

    PrSBC safeController;
    bool useSafeController;
    QVector<float> prsbcComputationalTime;
    
    ControlePosicaoModelClass PID[MAX_IDS]; /**< Controlador PID para
                                                       cada robô. */
    QSharedPointer<PIDConfig> defaultConfig;
    QSharedPointer<PIDConfig> goalieConfig;

    ControlePosicaoModelClass PIDSimulacao; /**< Controlador PID para simular o
                                              modelo ARX. */
    
    PathTracking pathTracking[MAX_IDS];
    ControleFuzzyModelClass Fuzzy[MAX_IDS];

    QFile fileDadosControle; /**< Arquivo para criar o log do controlador.*/
    QElapsedTimer etmTempoDadosControle; /**< Timer para contabilizar os
                       timestamps que serão colocados no log do controlador.*/

private:
    /**
     * @brief Envia os comandos para os robôs no grSim
     */
    void vEnviaComandosGrSim();

    /**
     * @brief Envia os comandos para os robôs pelo rádio
     */
    void vEnviaComandosRadio();

    /**
     * @brief Atualiza o valor atual do FPS
     */
    void vAtualizarFPS();

    /**
     * @brief Calcula o ângulo a ser fornecido para o controlador
     *
     * @param anguloDestino - Ângulo entre o robô e o ponto em que ele deve
     * "olhar".  Esse ângulo é calculado pela função #fCalculaAnguloRoboPonto
     */
    void vCalculaAngulosControle(float& targetAngle);

    /**
     * @brief Calcula o ângulo entre a posição do robô e o ponto fornecido
     *
     * @param _posRobo - Posição do robô
     * @param _ponto - Ponto de mira
     * @return float - Ângulo em radianos
     */
    float fCalculaAnguloRoboPonto(QVector2D _posRobo, QVector2D _ponto);

    /**
     * @brief Decompõe a velocidade fornecida nos eixos X,Y do robô
     *
     * @param tempVel - Vetor de velocidade
     * @param _robot - Atributos do robô
     * @return QVector2D - Vetor de velocidade decomposto
     */
    QVector2D vt2dDecompoeVelocidadeRobo(QVector2D tempVel, Atributos& _robot);

    /**
     * @brief Decompõe a velocidade fornecida nos eixos X,Y do robô
     * @param tempVel - Vetor de velocidade
     * @param _robot - Ponteiro do robô.
     * @return QVector2D - Vetor de velocidade decomposto
     */
    QVector2D vt2dDecompoeVelocidadeRobo(QVector2D tempVel, const Robo* _robot);

    /**
     * @brief Faz o cálculo do esforço de controle para o robô
     * @param _rbtRobo - Objeto do robô a ser controlado
     */
    void vControleMatLab(Robo* _rbtRobo, bool bAmbiente);

    void vControleMatLabFuzzy(Robo* _rbtRobo, bool bAmbiente);

    void trackPath(Robo* _robot, bool _environment);

    void applySafeController();

    /**
     * @brief Calcula o esforço de controle para utilizar na simulação do
     * modelo ARX.
     * @param _rbtRobo - Objeto do robô a ser controlado
     * @return QVector3D - Esforço de controle em X, Y e rotação.
     */
    QVector3D vt3dControleSimulacao(Robo* _rbtRobo);

    /**
     * @brief Simula o modelo de identificação de sistemas para o robô fornecido
     * @param IDRobo - ID do robô a ser simulado
     */
    void vSimularModelo(int IDRobo);

    void setConfig(ControlePosicaoModelClass& _pid, QSharedPointer<PIDConfig> _config);

    QScopedPointer<QTimer> tmrLoopControle; /**< Timer para execução do loop
                                                de controle.*/
    bool bRecebeuPrimeiroPacote; /**< Indica se a thread de movimentação já
                                    recebeu o primeiro dado da visão.*/
    float fFPSControle; /**< Valor médio atual do FPS do loop de controle.*/
    QVector<int> iFPSCounter; /**< Armazena um histórico de FPS da thread para
                                    calcular a média.*/
    QElapsedTimer etmTimerFPSControle; /**< Timer utilizado para medir o FPS
                                            da thread.*/

    QScopedPointer<RoboFei_Simulator> simulador; /**< Objeto para
                                                        conectar-se ao grSim.*/
    bool bAtivo; /**< Indica se a thread está ativa.*/

public slots:
    /**
     * @brief Recebe as informações da visão (Visao)
     *
     * @param _packet - Pacote com os dados
     */
    void vReceiveVisionData_(const VisionPacket _packet);
    /**
     * @brief Recebe as informações da estratégia (Estrategia)
     *
     * @param _acAmbiente - Objeto do ambiente de campo
     */
    void vRecebeInformacoesEstrategia(const AmbienteCampo& _acAmbiente);

    /**
     * @brief Recebe os dados da porta serial (Radio)
     */
    void vRecebeDadosPortaSerial(const ConfiguracaoRadio& _config);

    /**
     * @brief Ativa/Desativa a thread
     *
     * @param _bAtivaThread - True/False para ativar/desativa a thread
     */
    void vAtivaThread(const bool _bAtivaThread);

    /**
     * @brief Recebe os dados necessários para conectar com o grSim
     * @param ipGrSim - IP do grSim
     * @param portaGrSim - Porta do grSim
     */
    void vRecebeConfiguracoesSimulador(int portaControle, int portaRobo,
                                       const QString& strInterface);

    /**
     * @brief Função principal da thread. É executada pelo timer
     * tmrLoopControle.
     */
    void vMovimentacaoRobos();

    /**
     * @brief Envia um comando para o grSim para mudar a posição/velocidade
     * da bola
     *
     * @param vt2dPosicao - Nova posição da bola
     * @param vt2dVelocidade - Nova velocidade da bola
     */
    void SLOT_mudaPosicaoBolaSimulacao(QVector2D vt2dPosicao,
                                       QVector2D vt2dVelocidade);

    void SLOT_mudarPosicaoRoboSimulacao(bool amarelo, QVector2D destino,
                                        qint8 id);

    void SLOT_mudarAnguloRoboSimulacao(bool amarelo, float angulo, qint8 id);

    void vReceiveRobotFeedback_(qint8 _id, bool _ballSensor);

    void receiveComputationalTimeRequest(float& _time);

    void receivePrSBCConfigRequest(QString& _config);

signals:

    /**
     * @brief Retorna os dados dos robôs para a thread da interface (RoboFeiSSL)
     *
     * @param 
     */
    void vRetornaVelocidadesRobos(const MotionFeedbackPacket& _feedback);

    /**
     * @brief Retorna os dados com os trajetos alterados dos robôs para a thread
     * da estratégia (Estrategia)
     *
     * @param 
     */
    void vRetornaCaminhosAlterados(const MotionPathFeedback& _pathFeedback);

    /**
     * @brief vEnviaStatusConexaoRadio Envia se a conexão com o rádio deu certo
     * ou não
     * @param strConexao - Se deu certo envia a string "Conectado", senão, envia
     * "TipoErro"
     */
    void vEnviaStatusConexaoRadio(const QString& strConexao);

    /**
     * @brief vEnviaErroPacoteRadio Envia o tipo de erro que houve com o pacote
     * recebido
     * @param erro - Tipo do erro
     */
    void vEnviaErroPacoteRadio(const ErrorCode& erro);

    /**
     * @brief _vSendSimulationFeedback Envia os dados recebidos pela simulação.
     * @param _feedback - Objeto com os dados de cada robô.
     */
    void _vSendSimulationFeedback(const RadioFeedback& _feedback);
};

#endif // ROBOTMOVEMENT_H
