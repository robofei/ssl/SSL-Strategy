/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "velocidade.h"
#include "ui_velocidade.h"

Velocidade::Velocidade(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Velocidade),
    vtdVelocidadesBola(QVector<double>(pontosGraficoBola, 0)),
    vtdTempoBola(QVector<double>(pontosGraficoBola)),
    vtdVelocidadesRobo0(QVector<double>(pontosGraficoRobo, 0)),
    vtdVelocidadesRobo1(QVector<double>(pontosGraficoRobo, 0)),
    vtdTempoRobos(QVector<double>(pontosGraficoRobo))
{
    ui->setupUi(this);

    this->setWindowTitle(QStringLiteral("Velocidades"));

    ui->qcp_GraficoVelocidadeBola->addGraph();
    ui->qcp_GraficoVelocidadeBola->xAxis->setRange(0, pontosGraficoBola);
    ui->qcp_GraficoVelocidadeBola->yAxis->setLabel(QStringLiteral("v (m/s)"));
    ui->qcp_GraficoVelocidadeBola->yAxis->setRange(0, 10);

    ui->qcp_VelocidadeRobo_0->addGraph();
    ui->qcp_VelocidadeRobo_0->xAxis->setRange(0, pontosGraficoRobo);
    ui->qcp_VelocidadeRobo_0->yAxis->setLabel(QStringLiteral("v (m/s)"));
    ui->qcp_VelocidadeRobo_0->yAxis->setRange(0, 3);

    ui->qcp_VelocidadeRobo_1->addGraph();
    ui->qcp_VelocidadeRobo_1->xAxis->setRange(0, pontosGraficoRobo);
    ui->qcp_VelocidadeRobo_1->yAxis->setLabel(QStringLiteral("v (m/s)"));
    ui->qcp_VelocidadeRobo_1->yAxis->setRange(0, 3);

    for (quint16 i=0; i<pontosGraficoBola; i++){

        vtdTempoBola[i] = i;
    }

    for (quint16 i=0; i<pontosGraficoRobo; i++){

        vtdTempoRobos[i] = i;

    }

    ui->qcp_GraficoVelocidadeBola->graph(0)->setData(vtdTempoBola, vtdVelocidadesBola);
    ui->qcp_VelocidadeRobo_0->graph(0)->setData(vtdTempoRobos, vtdVelocidadesRobo0);
    ui->qcp_VelocidadeRobo_1->graph(0)->setData(vtdTempoRobos, vtdVelocidadesRobo1);

    Auxiliar::vConfiguraDarkModePlot(ui->qcp_GraficoVelocidadeBola);
    Auxiliar::vConfiguraDarkModePlot(ui->qcp_VelocidadeRobo_0);
    Auxiliar::vConfiguraDarkModePlot(ui->qcp_VelocidadeRobo_1);
}

Velocidade::~Velocidade()
{
    delete ui;
}

int Velocidade::iIdRobo0() const
{
    return ui->sb_Robo0->value();
}

int Velocidade::iIdRobo1() const
{
    return ui->sb_Robo1->value();
}

void Velocidade::closeEvent(QCloseEvent *event)
{
    event->accept();
    emit vVelocidadeClose();
}

void Velocidade::reject()
{
    emit vVelocidadeClose();
}


void Velocidade::vRecebeVelocidadeBola(const double& dNovaVelocidade)
{
    vtdVelocidadesBola.removeFirst();
    vtdVelocidadesBola.append(dNovaVelocidade);

    ui->qcp_GraficoVelocidadeBola->graph(0)->setData(vtdTempoBola, vtdVelocidadesBola);

    ui->qcp_GraficoVelocidadeBola->replot();
}

void Velocidade::vRecebeVelocidadeRobo(const double& _dNovaVelocidade, const  qint8& _iID)
{
    if(_iID == ui->sb_Robo0->value()){

        vtdVelocidadesRobo0.removeFirst();
        vtdVelocidadesRobo0.append(_dNovaVelocidade);

        ui->qcp_VelocidadeRobo_0->graph(0)->setData(vtdTempoRobos, vtdVelocidadesRobo0);

        ui->qcp_VelocidadeRobo_0->replot();

        return;
    }

    if(_iID == ui->sb_Robo1->value()){

        vtdVelocidadesRobo1.removeFirst();
        vtdVelocidadesRobo1.append(_dNovaVelocidade);

        ui->qcp_VelocidadeRobo_1->graph(0)->setData(vtdTempoRobos, vtdVelocidadesRobo1);

        ui->qcp_VelocidadeRobo_1->replot();

        return;
    }
}


void Velocidade::on_sb_Robo0_valueChanged(int arg1)
{
    vtdVelocidadesRobo0.fill(0);

    ui->qcp_VelocidadeRobo_0->setVisible(arg1 != -1);
}

void Velocidade::on_sb_Robo1_valueChanged(int arg1)
{
    vtdVelocidadesRobo1.fill(0);

    ui->qcp_VelocidadeRobo_1->setVisible(arg1 != -1);
}



