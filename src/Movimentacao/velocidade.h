/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VELOCIDADE_H
#define VELOCIDADE_H

///
/// \file velocidade.h
/// \brief UI com gráficos da velocidade de robôs e da bola \a Velocidade
///

#include <QDialog>

#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

namespace Ui {
class Velocidade;
}

const quint16 pontosGraficoBola = 900;
const quint16 pontosGraficoRobo = 400;

/**
 * @brief Classe do diálogo que mostra a velocidade da bola e de alguns robôs
 *
 */
class Velocidade : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construtor da classe
     *
     * @param parent
     */
    explicit Velocidade(QWidget *parent = nullptr);

    /**
     * @brief Destrutor da classe
     *
     */
    ~Velocidade();

    int iIdRobo0()const;
    int iIdRobo1()const;

protected:

    /**
     * @brief Overload do slot da classe QDialog
     *
     * @details Esse slot é chamado quando o usuário finaliza a janela 
     * (por um atalho ou por algum botão).
     *
     * O sinal \p vRefClose() é emitido indicando que o objeto da classe pode 
     * ser finalizado.
     *
     * @param event
     */
    void closeEvent(QCloseEvent *event = nullptr);

    /**
     * @brief Overload do slot da classe QDialog
     *
     * @details Esse slot é chamado quando o usuário pressiona a tecla ESCAPE.
     *
     * O sinal \p vRefClose() é emitido indicando que o objeto da classe pode 
     * ser finalizado.
     */
    void reject();

private:
    Ui::Velocidade *ui; /**< Objeto da interface.  */

    QVector<double> vtdVelocidadesBola; /**< Vetor com as velocidades da bola.*/
    QVector<double> vtdTempoBola; /**< Vetor com o tempo da bola.*/

    QVector<double> vtdVelocidadesRobo0; /**< Vetor com as velocidades do 
                                           primeiro robô.*/
    QVector<double> vtdVelocidadesRobo1; /**< Vetor com as velocidades do 
                                           segundo robô.*/
    QVector<double> vtdTempoRobos; /**< Vetor com o tempo dos robôs.*/

public slots:
    /**
     * @brief Recebe uma nova velocidade da bola
     *
     * @param _dVelocidade - Velocidade da bola
     */
    void vRecebeVelocidadeBola(const double& dNovaVelocidade);

    /**
     * @brief Recebe uma nova velocidade para o robô
     *
     * @param _dVelocidade - Velocidade do robô
     * @param _iID - ID do robô
     */
    void vRecebeVelocidadeRobo(const double& _dNovaVelocidade,
            const qint8& _iID);

private slots:
    void on_sb_Robo0_valueChanged(int arg1);
    void on_sb_Robo1_valueChanged(int arg1);


signals:

    /**
     * @brief Sinal emitido quando a janela é fechada
     */
    void vVelocidadeClose();
};

#endif // VELOCIDADE_H
