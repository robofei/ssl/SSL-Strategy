#include "prsbc.h"

long PrSBC::nchoosek(const int n, const int k)
{
    long prevSolution = n - k + 1, solution;

    for (int i = 1; i < k; ++i)
    {
        solution = prevSolution * (n - k + 1 + i) / (i + 1);
        prevSolution = solution;
    }

    return prevSolution;
}

TrapezoidalCDF PrSBC::trapezoidalCDFInverse(double a, double c, double delta)
{
    TrapezoidalCDF result;
    double sigma = confidence;
    double b1 = 0, b2 = 0;
    double A, C;

    // [-A A] is the large one, and [-C C] is the smaller one
    if (a > c)
    {
        A = a;
        C = c;
    }
    else
    {
        A = c;
        C = a;
    }

    if (A == 0 && C == 0)
    {
        result.b2 = delta;
        result.b1 = delta;
        return result;
    }

    // O_vec = [-(A+C) -(A-C) (A-C) (A+C)]; // vector of vertices on the trap
    // distribution pdf
    double h = 1.0 / (2.0 * A); // height of the trap distribution
    Eigen::Matrix<double, 1, 3> area_seq;
    area_seq << 1.0 / 2 * 2.0 * C * h, 2.0 * (A - C) * h, 1.0 / 2 * 2.0 * C * h;

    Eigen::Matrix<double, 1, 2> area_vec;
    area_vec << area_seq(0, 0), area_seq(0, 1) + area_seq(0, 2);

    if (abs(A - C) < 1e-5) // then is triangle
    {
        // assuming sigma > 50%
        // 1-area_vec(0, 1) should be very close to 0.5
        b1 = (A + C) - 2 * C * sqrt((1.0 - sigma) / (1.0 - area_vec(0, 1)));
        b2 = -b1;

        b1 = b1 + delta;
        b2 = b2 + delta; // apply shift here due to xi-xj
    }
    else // is trapezoidal
    {
        if (sigma > area_vec(0, 1)) // right triangle area
        {
            b1 = (A + C) - 2 * C * sqrt((1.0 - sigma) / (1.0 - area_vec(0, 1)));
            b2 =
                -(A + C) + 2 * C * sqrt((1.0 - sigma) / (1.0 - area_vec(0, 1)));

            b1 = b1 + delta;
            b2 = b2 + delta; // apply shift here due to xi-xj
        }
        else if (sigma > area_vec(0, 0) &&
                 sigma <= area_vec(0, 1)) // in between the triangle part
        {
            b1 = -(A - C) + (sigma - area_vec(0, 0)) /
                                h; // assuming >50%, then b1 should >0
            b2 = -b1;

            b1 = b1 + delta;
            b2 = b2 + delta; // apply shift here due to xi-xj
            // note that b1 could be > or < b2, depending on whether sigma > or
            // < .5
        }
        else if (sigma <= area_vec(0, 0))
        {
            b1 = -(A + C) +
                 2 * C *
                     sqrt(sigma /
                          area_vec(0, 0)); // assuming >50%, then b1 should >0
            b2 = -b1;

            b1 = b1 + delta;
            b2 = b2 + delta; // apply shift here due to xi-xj
        }
        else // first triangle, which is not allowed as long as we assume sigma
             // > 50%
        {
            std::cout << "bug in trap_cdf_inv, what is wrong?\n";
        }
    }

    result.b1 = b1;
    result.b2 = b2;
    return result;
}

Eigen::Matrix<double, 2, 1> PrSBC::computeE(
    const Eigen::Matrix<double, 2, 1>& _xiNoise,
    const Eigen::Matrix<double, 2, 1>& _xjNoise,
    const Eigen::Matrix<double, 2, 1>& _deltaX)
{
    Eigen::Matrix<double, 2, 1> e;

    TrapezoidalCDF bX = trapezoidalCDFInverse(_xiNoise(0), _xjNoise(0),
                                              _deltaX(0)),
                   bY = trapezoidalCDFInverse(_xiNoise(1), _xjNoise(1),
                                              _deltaX(1));

    double bx, by;
    if ((bX.b2 < 0 && bX.b1 > 0) || (bX.b2 > 0 && bX.b1 < 0))
    {
        // distance between robots on x smaller than error bound!
        bx = 0;
    }
    else if ((bX.b1 < 0 && bX.b2 < bX.b1) || (bX.b2 < 0 && bX.b2 > bX.b1))
    {
        bx = bX.b1;
    }
    else if ((bX.b2 > 0 && bX.b2 < bX.b1) || (bX.b1 > 0 && bX.b2 > bX.b1))
    {
        bx = bX.b2;
    }
    else
    {
        // no uncertainty or sigma = 0.5 on x
        bx = bX.b1;
    }

    if ((bY.b2 < 0 && bY.b1 > 0) || (bY.b2 > 0 && bY.b1 < 0))
    {
        // distance between robots on y smaller than error bound!
        by = 0;
    }
    else if ((bY.b1 < 0 && bY.b2 < bY.b1) || (bY.b2 < 0 && bY.b2 > bY.b1))
    {
        by = bY.b1;
    }
    else if ((bY.b2 > 0 && bY.b2 < bY.b1) || (bY.b1 > 0 && bY.b2 > bY.b1))
    {
        by = bY.b2;
    }
    else
    {
        // no uncertainty or sigma = 0.5 on y
        by = bY.b1;
    }

    e << bx, by;
    return e;
}

void PrSBC::fillRobots(const MatrixXd& _x, const MatrixXd& _uNoiseSpan,
                       const MatrixXd& _xNoiseSpan, MatrixXd& A, MatrixXd& b,
                       int& count)
{
    for (int i = 0; i < N - 1; ++i) // Robot loop
    {
        for (int j = i + 1; j < N; ++j)
        {
            MatrixXd maxDVij =
                (_uNoiseSpan.col(i) + _uNoiseSpan.col(j)).rowwise().norm();
            MatrixXd maxDXij =
                (_x.col(i) - _x.col(j)).rowwise().norm() +
                (_xNoiseSpan.col(i) + _xNoiseSpan.col(j)).rowwise().norm();

            Eigen::Matrix<double, 2, 1> safetyMatrix = MatrixXd::Ones(2, 1);
            safetyMatrix *= pow(robotSafetyRadius(0, i), 2);

            //                 MatrixXd BB =  -safetyMatrix - 2/gamma *
            //                 maxDVij.cwiseProduct(maxDXij);

            Eigen::Matrix<double, 2, 1> eVec = computeE(
                _xNoiseSpan.col(i), _xNoiseSpan.col(j), _x.col(i) - _x.col(j));
            A(count, seq(2 * i, 2 * i + 1)) = -2 * eVec;
            A(count, seq(2 * j, 2 * j + 1)) = 2 * eVec;

            MatrixXd absMaxDVij = maxDVij.cwiseAbs(),
                     absMaxDXij = maxDXij.cwiseAbs();
            safetyMatrix.setOnes();
            safetyMatrix *= robotSafetyRadius(0, i);

            // $ \| e_{i,j} \|^2 - d.R_{ij}^2 + B_{ij} + 2 e_{i,j}^T \Delta
            // F_{i,j} / \gamma$
            MatrixXd h = eVec.cwiseProduct(eVec) -
                         safetyMatrix.cwiseProduct(safetyMatrix) -
                         2 * absMaxDVij.cwiseProduct(absMaxDXij) / gamma;

            b(count) = gamma * pow(h.sum(), kappaExpoent);
            count++;
        }
    }
}

void PrSBC::fillObstacles(const MatrixXd& _x, const MatrixXd& _obstacles,
                          const MatrixXd& _obstaclesU,
                          const MatrixXd& _uNoiseSpan,
                          const MatrixXd& _xNoiseSpan,
                          const MatrixXd& _uNoiseSpanObstacles,
                          const MatrixXd& _xNoiseSpanObstacles, MatrixXd& A,
                          MatrixXd& b, int& count)
{
    if (_obstacles.cols() > 0 && _obstacles.rows() == 2)
    {
        for (int i = 0; i < N; ++i) // Robot loop
        {
            for (int j = 0; j < _obstacles.cols(); ++j) // Obstacle loop
            {
                MatrixXd maxDVij =
                    (_uNoiseSpan.col(i) + _uNoiseSpanObstacles.col(j))
                        .rowwise()
                        .norm();
                MatrixXd maxDXij =
                    (_x.col(i) - _obstacles.col(j)).rowwise().norm() +
                    (_xNoiseSpan.col(i) + _xNoiseSpanObstacles.col(j))
                        .rowwise()
                        .norm();

                Eigen::Matrix<double, 2, 1> safetyMatrix = MatrixXd::Ones(2, 1);
                safetyMatrix *= pow(robotSafetyRadius(0, i), 2);

                //                 MatrixXd BB =  -safetyMatrix - 2/gamma *
                //                 maxDVij.cwiseProduct(maxDXij);

                Eigen::Matrix<double, 2, 1> eVec =
                    computeE(_xNoiseSpan.col(i), _xNoiseSpanObstacles.col(j),
                             _x.col(i) - _obstacles.col(j));
                A(count, seq(2 * i, 2 * i + 1)) = -2 * eVec;

                MatrixXd absMaxDVij = maxDVij.cwiseAbs(),
                         absMaxDXij = maxDXij.cwiseAbs();
                safetyMatrix.setOnes();
                safetyMatrix *= robotSafetyRadius(0, i);

                MatrixXd h = eVec.cwiseProduct(eVec) -
                             safetyMatrix.cwiseProduct(safetyMatrix) -
                             2 * absMaxDVij.cwiseProduct(absMaxDXij) / gamma -
                             2 * eVec.cwiseProduct(_obstaclesU.col(j)) / gamma;
                b(count) = gamma * pow(h.sum(), kappaExpoent);
                count++;
            }
        }
    }
}

void PrSBC::setAmplitudeConstraint(MatrixXd& A, MatrixXd& b, MatrixXd& l,
                                   float _maxSpeed)
{
    A(seq(A.rows() - 2 * N, A.rows() - 1), seq(0, A.cols() - 1)) =
        MatrixXd::Identity(2 * N, 2 * N);

    b(seq(b.rows() - 2 * N, b.rows() - 1), 0) =
        MatrixXd::Ones(2 * N, 1) * _maxSpeed;

    l.fill(-OsqpEigen::INFTY);
    l(seq(b.rows() - 2 * N, b.rows() - 1), 0) =
        -MatrixXd::Ones(2 * N, 1) * _maxSpeed;
}

MatrixXd PrSBC::solveQP(int _nConstraints,
                        const Eigen::SparseMatrix<double>& _A,
                        const Eigen::SparseMatrix<double>& _H, MatrixXd& _f,
                        MatrixXd& _l, MatrixXd& _b)
{
    OsqpEigen::Solver solver;
    // settings
    solver.settings()->setVerbosity(false);
    solver.settings()->setWarmStart(true);
    solver.settings()->setPrimalInfeasibilityTolerance(1e-15);

    // configuring the QP
    solver.data()->setNumberOfVariables(N * 2);
    solver.data()->setNumberOfConstraints(_nConstraints);
    if (!solver.data()->setHessianMatrix(_H))
    {
        qWarning() << "Failed to set Hessian Matrix!";
        exit(1);
    }
    if (!solver.data()->setGradient(_f))
    {
        qWarning() << "Failed to set Gradient Matrix!";
        exit(1);
    }
    if (!solver.data()->setLinearConstraintsMatrix(_A))
    {
        qWarning() << "Failed to set Linear Constraints Matrix!";
        exit(1);
    }
    if (!solver.data()->setUpperBound(_b))
    {
        qWarning() << "Failed to set Upper Bound Matrix!";
        exit(1);
    }

    if (!solver.data()->setLowerBound(_l))
    {
        qWarning() << "Failed to set Lower Bound Matrix!";
        exit(1);
    }
    // instantiate the solver
    if (!solver.initSolver())
    {
        qWarning() << "Failed to init Solver!";
        exit(1);
    }

    // solve the QP problem
    OsqpEigen::ErrorExitFlag flag = solver.solveProblem();
    MatrixXd solution = solver.getSolution().reshaped(2, N);
    if (flag != OsqpEigen::ErrorExitFlag::NoError)
    {
        // std::cout << int(flag) << " Error flag\n";
        qDebug() << int(flag) << " Error flag\n";
        solution.setZero();
    }

    if ((solution.array() > 2e3).any() || (solution.array() < -2e3).any())
    {
        solution.setZero();
    }

    return solution;
}

void PrSBC::config(int _kappaExpoent, float _barrierGain, float _safetyRadius,
                   float _confidence, int _robots)
{
    configured = true;
    gamma = _barrierGain;
    safetyRadius = 2 * _safetyRadius;
    confidence = _confidence;
    N = _robots;
    kappaExpoent = _kappaExpoent;
    robotSafetyRadius.resize(1, N);
    robotSafetyRadius.setOnes();
    robotSafetyRadius *= safetyRadius;

    constraintSimulation = 11;
    // robotsConstraints.resize(N);
    // for (int n = 0; n < N; n++)
    // {
    // robotsConstraints[n].resize(constraintSimulation, constraintSimulation);
    // robotsConstraints[n].setZero();
    // }
    robotsConstraints.resize(constraintSimulation, constraintSimulation);
    robotsConstraints.setZero();
}

MatrixXd PrSBC::getSafeInput(MatrixXd _u, MatrixXd _x, MatrixXd _v,
                             MatrixXd _obstaclesU, MatrixXd _obstacles,
                             MatrixXd _xNoiseSpan, MatrixXd _uNoiseSpan,
                             MatrixXd _xNoiseSpanObstacles,
                             MatrixXd _uNoiseSpanObstacles,
                             float _maxVelocity)

{
    if (!configured)
    {
        MatrixXd none;
        std::cout << "Trying to use without configuring!\n";
        return none;
    }

    int nConstraints = N - 1;
    if (N > 2)
    {
        nConstraints = nchoosek(N, 2);
    }
    if (_obstacles.cols() > 0)
    {
        nConstraints += _obstacles.cols() * N;
    }
    nConstraints += 2 * N; // amplitude constraint

    if (nConstraints > 0)
    {
        MatrixXd A = MatrixXd::Zero(nConstraints, 2 * N),
                 b = MatrixXd::Zero(nConstraints, 1),
                 l = MatrixXd::Ones(nConstraints, 1);

        int count = 0;

#ifdef USE_DYNAMIC_RADIUS
        for (int i = 0; i < _u.cols(); ++i)
        {
            if (_v(0, i) < 0.2)
            {
                updateSafetyRadius(i, 2 * 20e-3);
            }
            else
            {
                updateSafetyRadius(i, safetyRadius);
            }
        }
#endif // USE_DYNAMIC_RADIUS

        fillRobots(_x, _uNoiseSpan, _xNoiseSpan, A, b, count);
        fillObstacles(_x, _obstacles, _obstaclesU, _uNoiseSpan, _xNoiseSpan,
                      _uNoiseSpanObstacles, _xNoiseSpanObstacles, A, b, count);
        setAmplitudeConstraint(A, b, l, _maxVelocity);

        MatrixXd vhat = _u.reshaped(2 * N, 1), f = -2 * vhat,
                 H = 2 * MatrixXd::Identity(2 * N, 2 * N);

        //     if(b.minCoeff() < 0)
        //     {
        //         std::cout << "\nu:\n" << _u << "\n";
        //         std::cout << "\nx:\n" << _x << "\n";
        //         std::cout << "\nx noise:\n" << _xNoiseSpan << "\n";
        //         std::cout << "\nu noise:\n" << _uNoiseSpan << "\n";
        //         std::cout << "\nA:\n" << A << "\n";
        //         std::cout << "\nb:\n" << b << "\n";
        //     }
        MatrixXd uSafe =
            solveQP(nConstraints, A.sparseView(), H.sparseView(), f, l, b);

        double maxU = 2;
        int simulatedID = 0;

        if (constraintSimulation > 0)
        {
            MatrixXd uTest = uSafe;
            int xIndex = 0, yIndex = 0;

            for (double vx = -maxU; vx < maxU;
                 vx += 2 * maxU / constraintSimulation, xIndex++)
            {

                yIndex = 0;
                for (double vy = -maxU; vy < maxU;
                     vy += 2 * maxU / constraintSimulation, yIndex++)
                {
                    uTest(0, simulatedID) = vx;
                    uTest(1, simulatedID) = vy;

                    MatrixXd aux = uTest.reshaped(2 * N, 1);
                    robotsConstraints(xIndex, yIndex) =
                        (b + A * aux).minCoeff();
                }
            }
        }

        // std::cout << "Robot Constraints\n" << robotsConstraints << "\n\n\n";
        return uSafe;
    }
    return _u;
}

void PrSBC::updateSafetyRadius(int _id, double _radius)
{
    robotSafetyRadius(0, _id) = _radius;
}

MatrixXd PrSBC::getRobotConstraint(qint8 _id)
{
    return robotsConstraints;
}

QString PrSBC::exportConfig() const
{
    return QString("PrSBC_Config(kappa=%1|gamma=%2|radius=%3|confidence=%4)")
        .arg(kappaExpoent)
        .arg(gamma)
        .arg(safetyRadius / 2.f)
        .arg(confidence);
}
