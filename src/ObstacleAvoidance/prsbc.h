#ifndef PrSBC_H
#define PrSBC_H

#include <chrono>
#include <cmath>
#include <complex>
#include <iostream>
#include <QVector>
#include <QDebug>

#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "Eigen/src/Core/ArithmeticSequence.h"
#include "Eigen/src/Core/Matrix.h"
#include "Eigen/src/SparseCore/SparseMatrix.h"
#include "OsqpEigen/OsqpEigen.h"

// For cmake
// #include "OsqpEigen/OsqpEigen.h"

using Eigen::MatrixXd;
using Eigen::seq;

#define USE_DYNAMIC_RADIUS 1

typedef struct TrapezoidalCDF
{
    double b2;
    double b1;
} TrapezoidalCDF;

class PrSBC
{
    bool configured;
    float gamma;
    int kappaExpoent;
    float safetyRadius;
    MatrixXd robotSafetyRadius;
    float confidence;
    int N;
    int constraintSimulation;
    MatrixXd robotsConstraints;

    long nchoosek(const int n, const int k);

    /**
     * @brief This function constructs the trap distribution resultant from
     * convolution of two different central uniform distribution.
     *
     * i.e. from measurements of two robots positions
     * bot 1 : uniformly distributed between [-a,a]
     * bot 2 : uniformly distributed between [-c,c]
     * sigma: requred confidence level (>50%)
     *
     * @param a should be positive
     * @param c should be positive
     * @param delta x_bot1 - x_bot2 error between the two noisy measurements
     *
     * @return when sigma >.5
     *         b2: <b1 whose CDF corresponds to 1-sigma
     *         b1: >b2 whose CDF corresponds to sigma
     *         when sigma < .5
     *         b2: >b1 whose CDF corresponds to 1-sigma
     *         b1: <b2 whose CDF corresponds to sigma

     */
    TrapezoidalCDF trapezoidalCDFInverse(double a, double c, double delta);

    Eigen::Matrix<double, 2, 1> computeE(
        const Eigen::Matrix<double, 2, 1>& _xiNoise,
        const Eigen::Matrix<double, 2, 1>& _xjNoise,
        const Eigen::Matrix<double, 2, 1>& _deltaX);

    void fillRobots(const MatrixXd& _x, const MatrixXd& _uNoiseSpan,
                    const MatrixXd& _xNoiseSpan, MatrixXd& A, MatrixXd& b,
                    int& count);

    void fillObstacles(const MatrixXd& _x, const MatrixXd& _obstacles,
                       const MatrixXd& _obstaclesU, const MatrixXd& _uNoiseSpan,
                       const MatrixXd& _xNoiseSpan,
                       const MatrixXd& _uNoiseSpanObstacles,
                       const MatrixXd& _xNoiseSpanObstacles, MatrixXd& A,
                       MatrixXd& b, int& count);

    void setAmplitudeConstraint(MatrixXd& A, MatrixXd& b, MatrixXd& l,
                                float _maxSpeed);

    /**
     * @brief Solve
     *
     *       min_x  1/2 x'Px + q'x
     *
     *       s.t. l <= Ax <= u
     *
     * l, Lower bound
     *
     * @param _nConstraints
     * @param _A - A, linear constraints
     * @param _H - P, Hessian matrix
     * @param _f - q, Gradient
     * @param _l - l, Lower bound
     * @param _b - u, Upper bound
     *
     * @return
     */
    MatrixXd solveQP(int _nConstraints, const Eigen::SparseMatrix<double>& _A,
                     const Eigen::SparseMatrix<double>& _H, MatrixXd& _f,
                     MatrixXd& _l, MatrixXd& _b);

public:
    PrSBC()
    {
        std::cout.precision(5);
        kappaExpoent = 1;
        configured = false;
        gamma = safetyRadius = confidence = 0;
    }

    ~PrSBC()
    {
        configured = false;
    }

    void config(int _kappaExpoent = 1, float _barrierGain = 1e4,
                float _safetyRadius = 0.1f, float _confidence = 1,
                int _robots = 6);

    /**
     * @brief
     *
     * @param _u - [2xN] Comando atual dos robôs. Somente x e y [m/s].
     * @param _states - [2xN] Estados dos robôs. Somente x e y [m].
     * @param _obstaclesU - [2xNk] Velocidade dos obstáculos. Somente x e y
     * [m/s].
     * @param _obstacles - [2xNk] Estados dos obstáculos. Somente x e y [m].
     * @param _xNoiseSpan
     * @param _uNoiseSpan
     * @param _xNoiseSpanObstacles
     * @param _uNoiseSpanObstacles
     *
     * @return
     */
    MatrixXd getSafeInput(MatrixXd _u, MatrixXd _x, MatrixXd _v,
                          MatrixXd _obstaclesU, MatrixXd _obstacles,
                          MatrixXd _xNoiseSpan, MatrixXd _uNoiseSpan,
                          MatrixXd _xNoiseSpanObstacles,
                          MatrixXd _uNoiseSpanObstacles,
                          float _maxVelocity);

    void updateSafetyRadius(int _id, double _radius);

    MatrixXd getRobotConstraint(qint8 _id);

    QString exportConfig() const;
};

#endif // PrSBC_H
