/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "a_star_vg.h"

A_StarVG::A_StarVG(int iResolucaoGrafo)
{
    vgGrafo = new VisibilityGraph(iResolucaoGrafo);
}

A_StarVG::~A_StarVG()
{
    delete vgGrafo;
}


inline bool operator==(Celula &_cel1, Celula &_cel2)
{
    if(_cel1.vt2dPosicao.x() == _cel2.vt2dPosicao.x() && _cel1.vt2dPosicao.y() == _cel2.vt2dPosicao.y() )
        return true;
    else return false;
}

/*
 * Expande e calcula os valores de f(n) para todas as células adjacentes à célula atual
*/
void A_StarVG::vCalculaCelulasVizinhas(const Celula& _cllCelulaAtual)
{
    QVector<ArestaVG> vgVizinhos = vgGrafo->vgVerticesVisiveis(_cllCelulaAtual.iIndice);

    foreach (ArestaVG vertice, vgVizinhos)
    {
        Celula tempCel;

        //Calculo o custo ( g(n, n') )
        tempCel.cost = vertice.pos2.distanceToPoint(_cllCelulaAtual.vt2dPosicao) + _cllCelulaAtual.cost;

        tempCel.vt2dPosicao = vertice.pos2;
        tempCel.heuristic = vt2dDestino.distanceToPoint(tempCel.vt2dPosicao);
        tempCel.f = tempCel.cost + tempCel.heuristic;//f(n) = g(n,n') + h(n)
        tempCel.isOpen = true;
        tempCel.isClosed = false;
        tempCel.iIndice = vertice.indice;

        if(vertice.closed.second == false)
        {
            if(vertice.open.second == false)
            {
                tempCel.cllPai = new Celula;
                *tempCel.cllPai = _cllCelulaAtual;

                cllOpenList.push(tempCel);
                vgGrafo->vSetaOpenClosed(tempCel.isOpen, tempCel.isClosed, tempCel.iIndice, tempCel.cost);
            }
        }

//        if(tempCel.h < 1)
//        {
//            bTrajetoCalculado = true;
//            return;
//        }
    }
}

/*
 * Calcula a sequencia dos pontos gerados na closed list para chegar ao destino
*/
QVector<QVector2D> A_StarVG::vt2dExtraiCaminhoEncontrado()
{
    QVector<Celula> temp;
    QVector<QVector2D> caminhoFinal; caminhoFinal.clear();

    temp.append(cllClosedList[cllClosedList.size()-1]);
    caminhoFinal.prepend(temp.last().vt2dPosicao);

    while(temp.last().vt2dPosicao != cllCelulaInicial.vt2dPosicao)
    {
        temp.append(*temp.last().cllPai);
        caminhoFinal.prepend(temp.last().vt2dPosicao);
    }

    return caminhoFinal;
}

void A_StarVG::vSetup(const AmbienteCampo *_acAmbientePathPlanner, int _iRoboID, bool stop)
{
    if(_iRoboID == _acAmbientePathPlanner->allies->iGetGoalieID())
    {
        globalConfig.safety.ball.normal = globalConfig.robotDiameter*1.5;
    }
    else if(stop == true &&
            _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->vt2dPosicaoAtual()
                        .distanceToPoint(_acAmbientePathPlanner->vt2dPosicaoBola()) > globalConfig.safety.ball.stop)
    {
        globalConfig.safety.ball.normal = globalConfig.safety.ball.stop;
    }
    else if(stop == true &&
            _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->vt2dPosicaoAtual()
                        .distanceToPoint(_acAmbientePathPlanner->vt2dPosicaoBola()) < globalConfig.safety.ball.stop)
    {
        globalConfig.safety.ball.normal = 300;
    }

    vt3dObstaculos.clear();

    //Seta o destino e a posição inicial
    vt2dDestino = _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->vt2dDestino();
    vt2dPosicaoInicial = _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->vt2dPosicaoAtual();

    int iRegiaoInicial = Auxiliar::iCalculaRegiaoPonto(vt2dPosicaoInicial,
                                                       _acAmbientePathPlanner->geoCampo->szField().width(),
                                                       globalConfig.fieldDivisionWidth),
        iRegiaoFinal = Auxiliar::iCalculaRegiaoPonto(vt2dDestino,
                                                     _acAmbientePathPlanner->geoCampo->szField().width(),
                                                     globalConfig.fieldDivisionWidth);

    if(iRegiaoInicial > iRegiaoFinal)//Inverte as regiões caso a inicial seja maior que a final
    {
        iRegiaoInicial = iRegiaoInicial + iRegiaoFinal;
        iRegiaoFinal = iRegiaoInicial - iRegiaoFinal;
        iRegiaoInicial = iRegiaoInicial - iRegiaoFinal;
    }

    vt3dObstaculos = _acAmbientePathPlanner->vt3dPegaPosicaoTodosObjetos(otAliado,
                                                                         iRegiaoInicial,
                                                                         iRegiaoFinal,
                                                                         _iRoboID);
    vt3dObstaculos.append(_acAmbientePathPlanner->vt3dPegaPosicaoTodosObjetos(otOponente,
                                                                              iRegiaoInicial,
                                                                              iRegiaoFinal));
    if(_acAmbientePathPlanner->allies->getPlayer(_iRoboID)->atbRetornaRobo().bIgnorarBola == bConsideraBola)
    {
        if(_acAmbientePathPlanner->bBolaDentroAreaDefesa(1.5) == false)
            vt3dObstaculos.append(QVector3D(_acAmbientePathPlanner->vt2dPosicaoBola(), otBola));
    }
}

/*
 * Função principal, onde é feito o calculo do trajeto
*/
QVector<QVector2D> A_StarVG::vt2dCalculaTrajeto(AmbienteCampo *_acAmbientePathPlanner,
                                                int _iRoboID, bool stop,
                                                int *_iCelulasExpandidas,
                                                QVector<QVector2D> *_vt2dCaminhoOriginal)
{
    startTimer();
    QVector<QVector2D> _vt2dCaminhoAnterior; _vt2dCaminhoAnterior.clear();
    QVector<QVector2D> vt2dTrajetoFinal; vt2dTrajetoFinal.clear();

    _vt2dCaminhoAnterior.append(_acAmbientePathPlanner->allies->getPlayer(_iRoboID)->path->getPath());
    dTempoCalculoCaminho = 0;

    int backupRaioSegurancaBola = globalConfig.safety.ball.normal;

    //Faz alguns calculos e atribuicoes iniciais
    vSetup(_acAmbientePathPlanner, _iRoboID, stop);

    //Se esta muito perto, retorna o destino direto
    const float distToGoal = vt2dDestino.distanceToPoint(vt2dPosicaoInicial);
    QVector<QVector2D> vt2dCaminhoOriginal;
    if(distToGoal < globalConfig.robotDiameter)
    {
        registerMapTime();
        vt2dTrajetoFinal.clear();
        vt2dTrajetoFinal.append(vt2dPosicaoInicial);
        vt2dTrajetoFinal.append(vt2dDestino);
        vt2dTrajetoFinal = vt2dTrataCaminhoFinal(vt2dTrajetoFinal,
                                                 vt2dCaminhoOriginal);
        globalConfig.safety.ball.normal = backupRaioSegurancaBola;
        _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->path->vSetPath(vt2dTrajetoFinal,
                                                                            distToGoal);
        _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->path->setPathAngle(pathAngle);
        _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->path->setSpeedProfile(speedProfile);
        stopTimer();
        return vt2dTrajetoFinal;
    }
    //Checa se o caminho deve ser recalculado a partir do ponto em que o robo esta
    if(bChecaCaminhoAnterior(_vt2dCaminhoAnterior) == false)
    {
        globalConfig.safety.ball.normal = backupRaioSegurancaBola;
        return _vt2dCaminhoAnterior;
    }

    QElapsedTimer etmAstar;
    etmAstar.start();

    vgGrafo->vCriaGrafo(_acAmbientePathPlanner, _iRoboID);
    registerMapTime();
    dTempoCalculoGrafo = etmAstar.nsecsElapsed()/1e6;

    cllClosedList.clear();

    if(vgGrafo->bDestinoConectado() == true)
    {
        bCalculandoCaminho = true;
        bTrajetoCalculado = false;

        cllCelulaAtual.setValues(vt2dPosicaoInicial, 0, nullptr, 0,
                                 vt2dDestino.distanceToPoint(vt2dPosicaoInicial), true, false);

        cllCelulaInicial = cllCelulaAtual;

        cllOpenList.clear();
        cllOpenList.push(cllCelulaAtual);


        while(bTrajetoCalculado == false)
        {
            vCalculaCelulasVizinhas(cllCelulaAtual);

            if(cllOpenList.size() == 0 || cllOpenList.size() > 1000)//Não foi possível calcular um trajeto
            {
                bTrajetoCalculado = false;
//                 qDebug() << cllOpenList.size() <<
//                             "Falha ao encontrar caminho AStar, Open = " +
//                             QString::number(cllOpenList.size());
                break;
            }

            cllCelulaAtual = cllOpenList.top();
            cllCelulaAtual.isOpen = false;
            cllCelulaAtual.isClosed = true;
            cllOpenList.pop();

            vgGrafo->vSetaOpenClosed(cllCelulaAtual.isOpen, cllCelulaAtual.isClosed,
                                     cllCelulaAtual.iIndice, cllCelulaAtual.cost);

            cllClosedList.append(cllCelulaAtual);
            if(cllCelulaAtual.heuristic < 1)
                bTrajetoCalculado = true;

        }
    }
    else
        bTrajetoCalculado = false;

    if(bTrajetoCalculado == true)
    {
        vt2dTrajetoFinal = vt2dTrataCaminhoFinal(vt2dExtraiCaminhoEncontrado(),
                                                 vt2dCaminhoOriginal);

        bTrajetoCalculado = false;
        bCalculandoCaminho = false;
        dTempoCalculoCaminho = (double)(etmAstar.nsecsElapsed()/1e6);

        if(_vt2dCaminhoOriginal != nullptr)
        {
            _vt2dCaminhoOriginal->clear();
            _vt2dCaminhoOriginal->append(vt2dCaminhoOriginal);
            vCalculaDistanciaObstaculos(vt2dTrajetoFinal);
        }

        if(_iCelulasExpandidas != nullptr)
        {
            *_iCelulasExpandidas = cllClosedList.size();
        }
    }
    else if(Auxiliar::bPontoForaCampo(_acAmbientePathPlanner->geoCampo->szField(),
                                      vt2dPosicaoInicial))
    {
        vt2dTrajetoFinal.clear();
        vt2dTrajetoFinal.append(vt2dPosicaoInicial);
        vt2dTrajetoFinal.append(vt2dDestino);
        vt2dTrajetoFinal = vt2dTrataCaminhoFinal(vt2dTrajetoFinal,
                                                 vt2dCaminhoOriginal);
        fComprimentoCaminho = fCalculaComprimentoCaminho(vt2dTrajetoFinal);
    }
// #ifdef ROBO_TODOS_OS_RECURSOS
//     else if(vt2dPosicaoInicial.distanceToPoint(vt2dDestino) > 1e3 &&
//             vt2dDestino.distanceToPoint(vt2dPosicaoInicial) > 500)
//     {
//
//         // Se não calculou trajeto para uma distância maior que 1m
//         // Gera um log do grafo
//         vgGrafo->vCriarLogGrafo(_iRoboID, cllClosedList.size());
//     }
// #endif

    for(auto & i : cllClosedList)
        delete i.cllPai;

    while(!cllOpenList.empty())
    {
        Celula aux = cllOpenList.top();
        cllOpenList.pop();
        if( aux.cllPai != nullptr){
           delete aux.cllPai;
           aux.cllPai = nullptr;
        }
    }
    globalConfig.safety.ball.normal = backupRaioSegurancaBola;
    _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->path->vSetPath(vt2dTrajetoFinal,
                                                                       fComprimentoCaminho);
    _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->path->setPathAngle(pathAngle);
    _acAmbientePathPlanner->allies->getPlayer(_iRoboID)->path->setSpeedProfile(speedProfile);
    stopTimer();

    return vt2dTrajetoFinal;
}

double A_StarVG::dRetornaTempoCalculoCaminho()
{
    if(qIsFinite(dTempoCalculoCaminho))
        return dTempoCalculoCaminho;
    else
        return -1;
}

double A_StarVG::dRetornaTempoGrafo()
{
   if(qIsFinite(dTempoCalculoGrafo))
       return dTempoCalculoGrafo;
   else
       return -1;
}
