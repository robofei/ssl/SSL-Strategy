/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef A_STAR_VG_H
#define A_STAR_VG_H

///
/// \file a_star_vg.h
/// \brief \a A_StarVG
///

#include <QElapsedTimer>
#include <QThread>
#include <QTime>
#include <QDebug>

#include <cmath>
#include <Path_Planning/priorityqueue.h>

#include "Mapa/visibilitygraph.h"
#include "Path_Planning/pathplanninggeneric.h"

/**
 * @brief Classe que implementa o algoritmo A* utilizando um grafo de visibilidade (VisibilityGraph) como mapa.
 *
 */
class A_StarVG : public PathPlanningGeneric
{

public:
    /**
     * @brief Construtor da classe.
     *
     * @param iResolucaoGrafo - Ângulo de resolução do grafo. Esse ângulo determina quantos graus serão utilizados
     * entre cada vértice de um robô.
     */
    A_StarVG(int iResolucaoGrafo);

    /**
     * @brief Destrutor da classe.
     *
     */
    ~A_StarVG();

    VisibilityGraph *vgGrafo; /**< Objeto que representa o grafo de visibilidade. \see VisibilityGraph */

    /**
     * @brief Calcula um trajeto para o robô cujo ID deve ser fornecido.
     *
     * @param _acAmbientePathPlanner - Ambiente de campo.
     * @param _iRoboID - ID do robô que terá seu trajeto calculado.
     * @param stop - Indica se o estado atual da Estrategia é #STOP.
     * @param _iCelulasExpandidas - Caso um ponteiro diferente de nullptr seja fornecido, será retornado por referência
     * quantas células foram expandidas para encontrar o trajeto.
     * @param _vt2dCaminhoOriginal - Caso um ponteiro diferente de nullptr seja fornecido, será retornado por referência
     * o trajeto sem nenhum tratamento (suavizações por exemplo).
     * @return QVector<QVector2D> - Trajeto calculado.
     */
    QVector<QVector2D> vt2dCalculaTrajeto(AmbienteCampo *_acAmbientePathPlanner, int _iRoboID, bool stop = false, int *_iCelulasExpandidas = nullptr, QVector<QVector2D> *_vt2dCaminhoOriginal = nullptr);

    /**
     * @brief Retorna o tempo de cálculo do último trajeto calculado.
     *
     * @return double
     */
    double dRetornaTempoCalculoCaminho();

    /**
     * @brief Retorna o tempo de cálculo do grafo.
     * @return double
     */
    double dRetornaTempoGrafo();

private:

    /**
     * @brief Extrai o trajeto encontrado da #cllClosedList
     * @return QVector<QVector2D> - Trajeto encontrado.
     */
    QVector<QVector2D> vt2dExtraiCaminhoEncontrado();

    /**
     * @brief Expande as células vizinhas da célula atual e as adiciona na #cllOpenList.
     *
     * @param _cllCelulaAtual - Célula atual.
     */
    void vCalculaCelulasVizinhas(const Celula &_cllCelulaAtual);

    /**
     * @brief Realiza o setup necessário para iniciar o algoritmo.
     *
     * @param _acAmbientePathPlanner - Ambiente do campo.
     * @param _iRoboID - ID do robô que terá seu trajeto calculado.
     * @param stop - Indica se o estado atual da Estrategia é #STOP.
     */
    void vSetup(const AmbienteCampo *_acAmbientePathPlanner, int _iRoboID, bool stop);

    double dTempoCalculoGrafo;   /**< Armazena o tempo de cálculo do grafo. */

    PriorityQueue<Celula> cllOpenList; /**< Open list do algoritmo. */
    QVector<Celula> cllClosedList; /**< Closed list do algoritmo. */

    Celula cllCelulaAtual; /**< Célula atual do algoritmo. */
    Celula cllCelulaInicial; /**< Célula inicial do algoritmo. */
    bool bTrajetoCalculado = false; /**< True se foi possível encontrar um trajeto. */
    bool bCalculandoCaminho = false; /**< True se o algoritmo está calculando um trajeto no momento. */
};

#endif // A_STAR_VG_H
