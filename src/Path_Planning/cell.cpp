#include "cell.h"

Cell::Cell()
{
    f = heuristic = cost = 0;
    open = closed = false;

    row = col = 0;
    occupied = false;
    parent = nullptr;

    usedInPaths.clear();
}

Cell::Cell(const Cell &_other)
{
    f = _other.f;
    heuristic = _other.h();
    cost = _other.g();

    open = _other.isOpen();
    closed = _other.isClosed();

    row = _other.getRow();
    col = _other.getCol();

    occupied = _other.isOccupied();
    parent = _other.getParent();

    usedInPaths = _other.usedInPaths;
}

Cell::Cell(const Cell *_other)
{
    f = _other->f;
    heuristic = _other->h();
    cost = _other->g();

    open = _other->isOpen();
    closed = _other->isClosed();

    row = _other->getRow();
    col = _other->getCol();

    occupied = _other->isOccupied();
    parent = _other->getParent();

    usedInPaths = _other->usedInPaths;
}

Cell::~Cell()
{

}

Cell::Cell(int _row, int _col)
{
    row = _row;
    col = _col;
    open = closed = false;
    f = heuristic = cost = 0;
    occupied = false;
    parent = nullptr;
    usedInPaths.clear();
}

int Cell::getRow() const
{
    return row;
}

void Cell::setRow(const int &_row)
{
    row = _row;
}

int Cell::getCol() const
{
    return col;
}

void Cell::setCol(const int &_col)
{
    col = _col;
}

float Cell::h() const
{
    return heuristic;
}

float Cell::g() const
{
    return cost;
}

Cell* Cell::getParent() const
{
    return parent;
}

void Cell::setParent(Cell *_parent)
{
    parent = _parent;
}

void Cell::setOccupied(const bool &_occupied)
{
    occupied = _occupied;
}

bool Cell::isOccupied() const
{
    return occupied;
}

void Cell::update(float _h, float _g)
{
    heuristic = _h;
    cost = _g;
    f = cost + heuristic;
}

void Cell::setOpen(bool _open)
{
    open = _open;
    closed = !open;
}

bool Cell::isOpen() const
{
    return open;
}

void Cell::setClosed(bool _closed)
{
    closed = _closed;
    open = !closed;
}

bool Cell::isClosed() const
{
    return closed;
}

void Cell::clear()
{
    open = closed = occupied = false;
    f = heuristic = cost = 0;
    parent = nullptr;
    usedInPaths.clear();
}

Cell* Cell::refresh()
{
    open = closed = false;
    f = heuristic = cost = 0;
    return parent;
}

void Cell::refreshNotShared()
{
    if(!isShared())
    {
        open = closed = false;
        f = heuristic = cost = 0;
        parent = nullptr;
        usedInPaths.clear();
    }
}

float Cell::distanceTo(const Cell &_other) const
{
    return sqrt((_other.getRow() - getRow()) * (_other.getRow() - getRow()) +
                (_other.getCol() - getCol()) * (_other.getCol() - getCol()));
}

QVector<int> Cell::getSharedPaths() const
{
    return usedInPaths;
}

void Cell::removeFromShared(int _n)
{
    if(isShared())
    {
        for(auto it = usedInPaths.begin(); it < usedInPaths.end(); ++it)
        {
            if(*it == _n)
            {
                usedInPaths.erase(it);
            }
        }
    }
}

void Cell::addToPath(int _n)
{
    for(auto i = usedInPaths.begin(); i < usedInPaths.end(); i++)
    {
        if(*i == _n)
        {
            return;
        }
    }
    usedInPaths.push_back(_n);
}

void Cell::removeFromPath(int _n, bool _removeShared)
{
    open = closed = false;
    f = heuristic = cost = 0;
    parent = nullptr;
    if(!isShared() || _removeShared)
    {
        for(auto i = usedInPaths.begin(); i < usedInPaths.end(); i++)
        {
            if(*i == _n)
            {
                usedInPaths.erase(i);
                break;
            }
        }
    }
}

bool Cell::isShared() const
{
    return usedInPaths.size() > 1;
}
