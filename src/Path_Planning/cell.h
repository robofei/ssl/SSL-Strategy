#ifndef CELL_H
#define CELL_H

#include <QVector>
#include <string>
#include <vector>
#include <chrono>
#include <iostream>
#include <math.h>
#include <cmath>

#include <cstdlib>

class Cell
{
    float heuristic;
    float cost;
    bool open;
    bool closed;
    QVector<int> usedInPaths;

    int row;
    int col;
    bool occupied;
    Cell *parent;

public:
    Cell();

    Cell(const Cell &_other);

    Cell(const Cell *_other);

    Cell(int _row, int _col);

    ~Cell();

    float f;

    void operator=(const Cell &_other)
    {
        f = _other.f;
        heuristic = _other.heuristic;
        cost = _other.cost;

        open = _other.open;
        closed = _other.closed;

        row = _other.row;
        col = _other.col;

        occupied = _other.occupied;
        parent = _other.parent;

        usedInPaths = _other.usedInPaths;
    }

    int getRow() const;

    void setRow(const int &_row);

    int getCol() const;

    void setCol(const int &_col);

    float h() const;

    float g() const;

    Cell* getParent() const;

    void setParent(Cell *_parent);

    void setOccupied(const bool &_occupied);

    bool isOccupied() const;

    void update(float _h, float _g);

    void setOpen(bool _open);

    bool isOpen() const;

    void setClosed(bool _closed);

    bool isClosed() const;

    void clear();

    Cell* refresh();

    void refreshNotShared();

    float distanceTo(const Cell &_other) const;

    QVector<int> getSharedPaths() const;

    void removeFromShared(int _n);

    void addToPath(int _n);

    void removeFromPath(int _n, bool _removeShared);

    bool isShared() const;
};

#endif // CELL_H
