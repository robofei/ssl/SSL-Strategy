#include "grid.h"
#include "qglobal.h"


#define RED "\033[0;31m"
#define BLUE "\033[0;34m"
#define GREEN "\033[0;92m"
#define PURPLE "\033[0;95m"
#define RESET "\033[0m"
// #define RED ""
// #define BLUE ""
// #define GREEN ""
// #define PURPLE ""
// #define RESET ""

Grid::Grid(int _fieldWidth, int _fieldHeight, int _gridResolution)
{
    maxCols = _fieldWidth / _gridResolution;
    maxRows = _fieldHeight / _gridResolution;
    resolution = _gridResolution;

    gridMap.reserve(maxCols * maxRows);
    occupiedCells.clear();
    sharedCells.clear();
    previousSharedCells.clear();

    fieldWidth = _fieldWidth;
    fieldHeight = _fieldHeight;

    for(int row = 0; row < maxRows; ++row)
    {
        for(int col = 0; col < maxCols; ++col)
        {
            gridMap.push_back(Cell(row, col));
        }
    }
}

Grid::~Grid()
{

}

Cell* Grid::at(int _row, int _col, bool _debug)
{
    using std::min;
    using std::max;
    _row = max(min(_row, maxRows - 1), 0);
    _col = max(min(_col, maxCols - 1), 0);
    int index = _row * maxCols + _col;
    if(_debug)
    {
        std::cout << "Accessing: " << _row << ", " << _col << " | i = " << index << "\n";
    }
    if(index >= 0 && index < gridMap.size())
    {
        return &gridMap[index];
    }
    return nullptr;
}

bool Grid::isIndexValid(int _row, int _col)
{
    if(_row < 0 || _row > maxRows)
    {
        return false;
    }
    if(_col < 0 || _col > maxCols)
    {
        return false;
    }
    return true;
}

void Grid::getNeighbors(const Cell &_reference, QVector<Cell*> &_neighbors)
{
    Cell *neighbor = nullptr;

    for(int row = -1; row < 2; ++row)
    {
        for(int col = -1; col < 2; ++col)
        {
            if(row == 0 && col == 0)
            {
                // skip the reference cell
            }
            else if(isIndexValid(_reference.getRow() + row,
                                 _reference.getCol() + col))
            {
                neighbor = this->at(_reference.getRow() + row,
                        _reference.getCol() + col);

                if(neighbor != nullptr)
                {
                    _neighbors.push_back(neighbor);
                }
            }
        }
    }
}

void Grid::toMapScale(int &_xCol, int &_yRow)
{
    using std::min;
    using std::max;
    _yRow = max(min(floor((_yRow + fieldHeight / 2.0) / resolution),
                     (double)maxRows), 0.0),
    _xCol = max(min(floor((_xCol + fieldWidth / 2.0) / resolution),
                     (double)maxCols), 0.0);
}

void Grid::toFieldScale(int &_xCol, int &_yRow)
{
    _xCol = _xCol * resolution - fieldWidth / 2.0;
    _yRow = _yRow * resolution - fieldHeight / 2.0;
}

void Grid::clearObjects()
{
    for(Cell *cell : occupiedCells)
    {
        cell->clear();
    }
    for(Cell *cell : sharedCells)
    {
        cell->clear();
    }
    occupiedCells.clear();
    sharedCells.clear();
}

void Grid::addObject(int _x, int _y, int _size)
{
    using std::min;
    using std::max;
    int objRow = _y, objCol = _x;
    toMapScale(objCol, objRow);

    int rowSpan = ceil(0.5 * _size / resolution),
        colSpan = ceil(0.5 * _size / resolution);

    Cell *cell = nullptr;
    for(int row = -rowSpan; row <= rowSpan; ++row)
    {
        for(int col = -colSpan; col <= colSpan; ++col)
        {
            cell = this->at(row + objRow, col + objCol);
            if(cell != nullptr)
            {
                cell->setOccupied(true);
                occupiedCells.push_back(cell);
            }
        }
    }

}

void Grid::addCellToPath(Cell *_cell, int _n)
{
    _cell->addToPath(_n);

    if(_cell->isShared())
    {
        if(_cell->getSharedPaths().size() <= 2)
        {
            sharedCells.push_back(_cell);
        }
    }
}

void Grid::removePathFromShared(QVector<Cell*> _path, int _n)
{
    for(Cell *cell : _path)
    {
        cell->removeFromShared(_n);
    }
}

void Grid::finishSharedList()
{
//     previousSharedCells.insert(previousSharedCells.end(),
//                                sharedCells.begin(), sharedCells.end());
    previousSharedCells = sharedCells;
    sharedCells.clear();
}

void Grid::clearPreviousSharedList()
{
    if(previousSharedCells.empty())
    {
        return;
    }
    while(!previousSharedCells.empty())
    {
        auto it  = previousSharedCells.begin();
        (*it)->clear();
        previousSharedCells.erase(it);
        it++;
    }
}

void Grid::clear()
{
    for(int i = 0; i < occupiedCells.size(); ++i)
    {
        occupiedCells.at(i)->clear();
    }
    for(int i = 0; i < sharedCells.size(); ++i)
    {
        sharedCells.at(i)->clear();
    }
    for(int i = 0; i < previousSharedCells.size(); ++i)
    {
        previousSharedCells.at(i)->clear();
    }
    occupiedCells.clear();
    sharedCells.clear();
    previousSharedCells.clear();
}

void Grid::clearSharedCells()
{
    for(int i = 0; i < sharedCells.size(); ++i)
    {
        sharedCells.at(i)->clear();
    }
    for(int i = 0; i < previousSharedCells.size(); ++i)
    {
        previousSharedCells.at(i)->clear();
    }
    sharedCells.clear();
    previousSharedCells.clear();
}

void Grid::addPath(QVector<Cell*> _path, int _n)
{
    for(Cell *_cell : _path)
    {
        _cell->addToPath(_n);
    }
}

void Grid::removePath(QVector<Cell*> _path, int _n, bool _removeShared)
{
    for(Cell *_cell : _path)
    {
        _cell->removeFromPath(_n, _removeShared);
    }
}

void Grid::show(const QVector<QString> &_icon)
{
    Cell *cell;
    for(int row = 0; row < maxRows; ++row)
    {
        for(int col = 0; col < maxCols; ++col)
        {
            cell = this->at(row, col);

            if(cell->isOccupied())
            {
                std::cout << RED << "" << RESET;
            }
            else if(cell->isOpen())
            {
                std::cout << RED << "" << RESET;
            }
            else if(cell->isClosed())
            {
                std::cout << PURPLE << "" << RESET;
            }
            else if(cell->getSharedPaths().size() == 1)
            {
                std::cout << BLUE << _icon.at(*cell->getSharedPaths().cbegin()).toStdString() << RESET;
            }
            else if(cell->getSharedPaths().size() > 1)
            {
                std::cout << RED << "" << RESET;
            }
            else
            {
                std::cout << GREEN << "" << RESET;
            }
            std::cout << " ";
        }
        std::cout << "\n";
    }
}
