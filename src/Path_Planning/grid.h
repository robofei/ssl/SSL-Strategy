#ifndef GRID_H
#define GRID_H

#include <QVector>

#include "cell.h"

// #define MAP_COLS 90 // columns [cells]
// #define MAP_ROWS 60 // rows [cells]
// #define MAP_RESOLUTION 100 // [mm]
// #define MAP_COLS 20 // columns [cells]
// #define MAP_ROWS 20 // rows [cells]
// #define MAP_RESOLUTION 1 // [mm]

class Grid
{
    QVector<Cell> gridMap;
    QVector<Cell*> occupiedCells;
    QVector<Cell*> sharedCells;
    QVector<Cell*> previousSharedCells;
    int fieldWidth;
    int fieldHeight;
    int maxCols;
    int maxRows;
    int resolution;

public:
    Grid(int _fieldWidth = 9000, int _fieldHeight = 6000, int _resolution = 100);

    ~Grid();

    Cell* at(int _row, int _col, bool _debug = false);

    bool isIndexValid(int _row, int _col);

    void getNeighbors(const Cell &_reference, QVector<Cell*> &_neighbors);

    void toMapScale(int &_xCol, int &_yRow);

    void toFieldScale(int &_xCol, int &_yRow);

    void clearObjects();

    void addObject(int _x, int _y, int _size = 180);

    void addCellToPath(Cell *_cell, int _n);

    void removePathFromShared(QVector<Cell*> _path, int _n);

    void finishSharedList();

    void clearPreviousSharedList();

    void clear();

    void clearSharedCells();

    void addPath(QVector<Cell*> _path, int _n);

    void removePath(QVector<Cell*> _path, int _n, bool _removeShared);

    void show(const QVector<QString> &_icon);
};

#endif // GRID_H
