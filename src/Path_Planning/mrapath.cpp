#include "Path_Planning/mrapath.h"

MRAPath::MRAPath()
{
    pathCost = pathLength = 0;
    path.clear();
}

MRAPath::~MRAPath()
{

}

void MRAPath::update(MRAPath _other)
{
    path.clear();
    path.append(_other.getPath());
    pathLength = _other.getLength();
    pathCost = _other.getCost();
}

void MRAPath::setPath(QVector<Cell*> &_path)
{
    path.clear();
    path.append(_path);
}

QVector<Cell*> MRAPath::getPath()
{
    return path;
}

void MRAPath::setLength(float _length)
{
    pathLength = _length;
}

float MRAPath::getLength() const
{
    return pathLength;
}

void MRAPath::setCost(float _cost)
{
    pathCost = _cost;
}

float MRAPath::getCost() const
{
    return pathCost;
}

void MRAPath::getConflicts(int &_nConflicts, int &_nRobots) const
{
    _nConflicts = _nRobots = 0;

    int aux;
    for(Cell *cell : path)
    {
        aux = cell->getSharedPaths().size();

        if(aux > 1)
        {
            _nConflicts++;
            _nRobots = std::max(_nRobots, aux);
        }
    }
}
