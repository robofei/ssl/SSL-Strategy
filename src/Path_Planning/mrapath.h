#ifndef MRAPATH_H
#define MRAPATH_H

#include "Path_Planning/cell.h"

class MRAPath
{
    QVector<Cell*> path;
    float pathLength;
    float pathCost;

public:
    MRAPath();

    ~MRAPath();

    void update(MRAPath _other);

    void setPath(QVector<Cell*> &_path);

    QVector<Cell*> getPath();

    void setLength(float _length);

    float getLength() const;

    void setCost(float _cost);

    float getCost() const;

    void getConflicts(int &_nConflicts, int &_nRobots) const;
};


#endif // MRAPATH_H
