#include "mrastar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Path_Planning/mrapath.h"
#include "grid.h"
#include "qvector.h"
#include "qvector2d.h"
#include "ssl_gc_common.pb.h"
#include <algorithm>
#include <cstdlib>
#include <random>
#include <string>
#include <vector>

MRAStar::MRAStar(int _N, QSize _fieldSize, int _gridResolution)
{
    nRobots = _N;
    gridmap = Grid(_fieldSize.width(), _fieldSize.height(), _gridResolution);
    closedList.clear();
    openList.clear();
    gridmap.clear();

    internalPaths.clear();
    coordinatedPaths.clear();

    goal = nullptr;
    start = nullptr;

    internalPaths.reserve(nRobots);
    coordinatedPaths.reserve(nRobots);

    deadlockTimer.invalidate();
    deadlockThreshold = 1; // em segundos
    deadlockCooldown = 5; // em segundos
    deadlockCooldownTimer.restart();

    // Cost = W x P_len + B * Con + E * Ki
    // P_len - Tamanho do trajeto
    // Con - Número de conflitos no trajeto
    // Ki - Número de robôs envolvidos nos conflitos
    W = 1;
    B = 50;
    E = 50;
}

MRAStar::~MRAStar()
{
}

void MRAStar::updateObstacles(const AmbienteCampo* _environment)
{
    Robo* robot;
    QVector2D pos;

    gridmap.clearObjects();

    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robot = _environment->allies->getPlayer(id);

        if (robot->iID() < globalConfig.robotsPerTeam && robot->bRoboDetectado())
        {
            pos = robot->vt2dPosicaoAtual();
            gridmap.addObject(pos.x(), pos.y());

            if (robot->bPegaIgnorarBola() == bConsideraBola)
            {
                pos = _environment->vt2dPosicaoBola();
                gridmap.addObject(pos.x(), pos.y());
            }
        }
    }

    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robot = _environment->opponents->getPlayer(id);

        if (robot->iID() < globalConfig.robotsPerTeam && robot->bRoboDetectado())
        {
            pos = robot->vt2dPosicaoAtual();
            gridmap.addObject(pos.x(), pos.y());
        }
    }
}

MRAPath MRAStar::findPath(Cell* _begin, Cell* _end, int _n, bool _coordinate)
{
    MRAPath solution;
    // Setup
    start = gridmap.at(_begin->getRow(), _begin->getCol());
    goal = gridmap.at(_end->getRow(), _end->getCol());

    start->setOpen(true);
    start->setParent(nullptr);
    start->update(start->distanceTo(*_end), 0);

    openList.push(start);
    // Setup

    Cell* currentCell;
    bool foundPath = false;
    while (openList.size() > 0)
    {
        currentCell = openList.top();
        currentCell->setClosed(true);
        closedList.push_back(currentCell);
        openList.pop();

        if (currentCell->h() == 0)
        {
            foundPath = true;
            break;
        }
        else
        {
            addNeighbors(*currentCell, _coordinate);
        }
    }

    QVector<Cell*> path;
    path.clear();
    if (foundPath)
    {
        while (currentCell != nullptr)
        {
            if (currentCell->h() == 0)
            {
                solution.setLength(currentCell->g());
            }
            gridmap.addCellToPath(currentCell, _n);
            path.insert(0, currentCell);

            currentCell = currentCell->refresh();
        }
    }

    while (!openList.empty())
    {
        currentCell = openList.top();
        openList.pop();
        currentCell->refresh();
    }
    while (!closedList.empty())
    {
        currentCell = closedList.back();
        closedList.pop_back();
        currentCell->refresh();
    }

    solution.setPath(path);
    return solution;
}

MRAPath MRAStar::refinePath(QVector2D _start, QVector2D _end, int _n)
{
    Cell *begin, *end;
    convertPositions(_start, _end, &begin, &end);

    return findPath(begin, end, _n, COORDINATE_PATHS);
}

void MRAStar::addNeighbors(Cell& _cell, bool _coordinate)
{
    QVector<Cell*> neighbors;
    gridmap.getNeighbors(_cell, neighbors);
    Cell* neighbor;
    float cost, heuristic, step;
    bool useCoordination, obstacleOnCorner;

    for (int i = 0; i < neighbors.size(); ++i)
    {
        neighbor = neighbors.at(i);
        heuristic = neighbor->distanceTo(*goal);
        useCoordination = (_coordinate & !neighbor->isShared()) | !_coordinate |
                          (heuristic <= 0);

        bool isMyself = neighbor->distanceTo(*start) < 2,
             isGoal = neighbor->distanceTo(*goal) < 2;

        if (!neighbor->isClosed() &&
            ((!neighbor->isOccupied() && useCoordination) || isMyself ||
             isGoal))
        {
            step = _cell.distanceTo(*neighbor);
            cost = _cell.g() + step;

            obstacleOnCorner = false;

            if (step > 1) // diagonal step
            {
                int dx = neighbor->getCol() - _cell.getCol(),
                    dy = neighbor->getRow() - _cell.getRow();

                if (gridmap.at(_cell.getRow() + dy, _cell.getCol())
                        ->isOccupied())
                {
                    obstacleOnCorner = true;
                }
                if (gridmap.at(_cell.getRow(), _cell.getCol() + dx)
                        ->isOccupied())
                {
                    obstacleOnCorner = true;
                }
            }

            if (!neighbor->isOpen() && !obstacleOnCorner)
            {
                neighbor->setParent(&_cell);
                neighbor->setOpen(true);
                neighbor->update(heuristic, cost);

                openList.push(neighbor);
            }
            else if (cost < neighbor->g() && !obstacleOnCorner)
            {
                neighbor->setParent(&_cell);
                neighbor->update(heuristic, cost);
                openList.replace(neighbor);
            }
        }
    }
}

float MRAStar::calculatePathCosts(QVector<MRAPath>& _solution)
{
    // Cost = W x P_len + B * Con + E * Ki
    // P_len - Tamanho do trajeto
    // Con - Número de conflitos no trajeto
    // Ki - Número de robôs envolvidos nos conflitos
    float pLen = 0, cost = 0, totalCost = 0;
    int conflicts, robotsConflicts;

    for (int i = 0; i < _solution.size(); ++i)
    {
        pLen = _solution.at(i).getLength();
        if (pLen <= 0)
        {
            pLen = 1000;
        }
        _solution.at(i).getConflicts(conflicts, robotsConflicts);

        cost = W * pLen + B * conflicts + E * robotsConflicts;
        totalCost += cost;
        _solution[i].setCost(cost);
    }
    return totalCost;
}

float MRAStar::sumCosts(QVector<MRAPath>& _solution)
{
    float total = 0;
    for (MRAPath& _sol : _solution)
    {
        total += _sol.getCost();
    }
    return total;
}

void MRAStar::convertPositions(const QVector<QVector2D>& _starts,
                               const QVector<QVector2D>& _ends, Cell** _begin,
                               Cell** _end, int _n)
{
    int col, row;
    col = _starts.at(_n).x();
    row = _starts.at(_n).y();
    gridmap.toMapScale(col, row);
    *_begin = gridmap.at(row, col);

    col = _ends.at(_n).x();
    row = _ends.at(_n).y();
    gridmap.toMapScale(col, row);
    *_end = gridmap.at(row, col);
}

void MRAStar::convertPositions(QVector2D _start, QVector2D _end, Cell** _begin,
                               Cell** _goal)
{
    int col, row;
    col = _start.x();
    row = _start.y();
    gridmap.toMapScale(col, row);
    *_begin = gridmap.at(row, col);

    col = _end.x();
    row = _end.y();
    gridmap.toMapScale(col, row);
    *_goal = gridmap.at(row, col);
}

void MRAStar::setupNewSearch()
{
    for (int n = 0; n < internalPaths.size(); ++n)
    {
        for (Cell* cell : internalPaths[n].getPath())
        {
            cell->refreshNotShared();
        }
    }
    gridmap.clearPreviousSharedList();
    gridmap.finishSharedList();
    internalPaths.clear();
}

void MRAStar::clearPaths(QVector<MRAPath>& _paths)
{
    for (int n = 0; n < _paths.size(); ++n)
    {
        for (Cell* cell : _paths[n].getPath())
        {
            cell->clear();
        }
    }
}

void MRAStar::clearPaths()
{
    for (int n = 0; n < internalPaths.size(); ++n)
    {
        for (Cell* cell : internalPaths[n].getPath())
        {
            cell->refreshNotShared();
        }
    }
    gridmap.clearPreviousSharedList();
    gridmap.finishSharedList();
}

void MRAStar::updatePaths()
{
    float newCost, currentCost = sumCosts(coordinatedPaths);
    QVector<MRAPath> newSolution;

    for (int n = 0; n < internalPaths.size(); ++n)
    {
        newCost = currentCost - coordinatedPaths.at(n).getCost() +
                  internalPaths.at(n).getCost();
        if (newCost < currentCost)
        {
            coordinatedPaths[n] = internalPaths[n];
        }
        currentCost = sumCosts(coordinatedPaths);
    }
}

float MRAStar::findPaths(const QVector<QVector2D>& _start,
                         const QVector<QVector2D>& _end, bool _coordinate)
{
    Cell *begin, *end;
    if (_start.size() != _end.size())
    {
        qWarning() << "Numero de inicios diferente do número de fins! "
                   << _start.size() << " != " << _end.size() << "\n";
        return 99999999.f;
    }

    for (int n = 0; n < _start.size(); ++n)
    {
        convertPositions(_start, _end, &begin, &end, n);
        internalPaths.push_back(findPath(begin, end, n, _coordinate));
    }
    return calculatePathCosts(internalPaths);
}

bool MRAStar::checkPaths(const AmbienteCampo* _environment)
{
    bool recalculate = false;
    Robo* robot;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robot = _environment->allies->getPlayer(id);
        if (robot->bRoboDetectado())
        {
            vt2dDestino = robot->vt2dDestino();
            vt2dPosicaoInicial = robot->vt2dPosicaoAtual();
            // QVector<int> ids = {id};
            // vt3dObstaculos =
            // _environment->vt3dPegaPosicaoTodosObjetos(otAliado, false, ids);
            // ids.clear();
            // vt3dObstaculos.append(_environment->vt3dPegaPosicaoTodosObjetos(otOponente,
            // false, ids)); ids.clear();
            // vt3dObstaculos.append(_environment->vt3dPegaPosicaoTodosObjetos(otBola,
            // false, ids)); recalculate = recalculate |
            // bChecaCaminhoAnterior(robot->path->getFullPath());
            recalculate = recalculate | robot->path->getFullPath().isEmpty();

            if (recalculate)
            {
                // qDebug() << "Recalculating for robot" << id;
                break;
            }
        }
    }
    return recalculate;
}

void MRAStar::setup(const AmbienteCampo* _environment,
                    QVector<QVector2D>& _start, QVector<QVector2D>& _end)
{
    updateObstacles(_environment);

    _start.clear();
    _end.clear();

    Robo* robot;
    robotIDs.clear();
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robot = _environment->allies->getPlayer(id);
        if (robot->bRoboDetectado())
        {
            robotIDs.append(robot->iID());
            _start.append(robot->vt2dPosicaoAtual());
            _end.append(robot->vt2dDestino());
        }
    }
}

void MRAStar::findCoordinatedPaths(AmbienteCampo* _environment)
{
    startTimer();
    QVector<QVector2D> start, end;

    if (!checkPaths(_environment))
    {
        stopTimer();
        return;
    }

    setup(_environment, start, end);
    registerMapTime();

    coordinatedPaths.clear();
    internalPaths.clear();

    float currentCost = findPaths(start, end), newCost;
    coordinatedPaths = internalPaths;
    MRAPath backup, refinedPath;

    auto rng = std::default_random_engine{};
    QVector<int> pathIDs;
    for (int n = 0; n < coordinatedPaths.size(); ++n)
    {
        pathIDs.push_back(n);
    }

    int iterations = 0, lastPathChange = 0;
    do
    {
        std::shuffle(std::begin(pathIDs), std::end(pathIDs), rng);
        for (int n : pathIDs)
        {
            currentCost = calculatePathCosts(coordinatedPaths);
            backup = coordinatedPaths[n];
            gridmap.removePath(backup.getPath(), n, false);

            refinedPath.update(refinePath(start.at(n), end.at(n), n));
            coordinatedPaths.replace(n, refinedPath);
            gridmap.removePathFromShared(backup.getPath(), n);
            gridmap.addPath(coordinatedPaths[n].getPath(), n);

            newCost = calculatePathCosts(coordinatedPaths);

            if (newCost >= currentCost)
            {
                gridmap.removePath(coordinatedPaths[n].getPath(), n, true);
                coordinatedPaths.replace(n, backup);
                gridmap.addPath(backup.getPath(), n);
            }
            else
            {
                lastPathChange = iterations;
            }
        }
        iterations++;
    } while (iterations < 30 && iterations - lastPathChange < 5);

    setSolution(_environment, start, end);
    stopTimer();
}

QString MRAStar::exportConfig() const
{
    return QString("MRA_Config(W=%1|B=%2|E=%3)").arg(W).arg(B).arg(E);
}

bool MRAStar::isInDeadlock(AmbienteCampo* _environment)
{
    Robo* robot;
    int robotsStopped = 0;

    for (qint8 id = 0; id < globalConfig.robotsPerTeam; id++)
    {
        robot = _environment->allies->getPlayer(id);

        if (robot->bRoboDetectado() && !robot->bArrived() &&
            robot->dModuloVelocidade() < 0.1)
        {
            robotsStopped++;
        }
    }

    if (robotsStopped > 1 && !deadlockTimer.isValid())
    {
        deadlockTimer.restart();
    }
    else if (robotsStopped == 0)
    {
        deadlockTimer.invalidate();
    }

    if (deadlockTimer.nsecsElapsed() / 1e9 > deadlockThreshold &&
        deadlockCooldownTimer.nsecsElapsed() / 1e9 > deadlockCooldown)
    {
        for (qint8 id = 0; id < globalConfig.robotsPerTeam; id++)
        {
            robot = _environment->allies->getPlayer(id);
            robot->path->clear();
        }
        deadlockCooldownTimer.restart();
        return true;
    }
    return false;
}

void MRAStar::setSolution(AmbienteCampo* _environment,
                          const QVector<QVector2D>& _start,
                          const QVector<QVector2D>& _end)
{
    QVector<QVector2D> path, originalPath;
    int x, y, n = 0;
    for (MRAPath& solution : coordinatedPaths)
    {
        path.clear();
        path.append(_start.at(n));
        for (Cell* cell : solution.getPath())
        {
            x = cell->getCol();
            y = cell->getRow();
            gridmap.toFieldScale(x, y);
            path.append(QVector2D(x, y));
        }
        // if(solution.getCost() < 1000.f)
        // {
        path.append(_end.at(n));
        // path = vt2dSimplificaCaminho(_environment, robotIDs.at(n), path,
        // 400);
        path = vt2dTrataCaminhoFinal(path, originalPath);

        _environment->allies->getPlayer(robotIDs.at(n))
            ->path->vSetPath(path, fComprimentoCaminho);
        _environment->allies->getPlayer(robotIDs.at(n))
            ->path->setPathAngle(pathAngle);
        _environment->allies->getPlayer(robotIDs.at(n))
            ->path->setSpeedProfile(speedProfile);
        // }
        n++;
    }
}
