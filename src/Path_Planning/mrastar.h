#ifndef MRASTAR_H
#define MRASTAR_H

#include <QVector2D>

#include "Ambiente/futbolenvironment.h"
#include "Path_Planning/cell.h"
#include "Path_Planning/pathplanninggeneric.h"
#include "Path_Planning/priorityqueue.h"
#include "Path_Planning/grid.h"
#include "Path_Planning/mrapath.h"
#include "qvector.h"
#include "qvector2d.h"

#define DEBUG 0

class MRAStar : public PathPlanningGeneric
{
#define COORDINATE_PATHS true

    PriorityQueue<Cell*> openList;
    QVector<Cell*> closedList;
    Grid gridmap;
    float deadlockThreshold;
    float deadlockCooldown;
    int W; /**< Peso que pondera o tamanho do trajeto. */
    int B; /**< Peso que pondera o número de conflito no trajeto. */
    int E; /**< Peso que pondera o número de robôs envolvidos nos conflitos. */

    QVector<MRAPath> internalPaths;
    QVector<MRAPath> coordinatedPaths;
    QVector<qint8> robotIDs;
    Cell *goal;
    Cell *start;
    int nRobots;
    QElapsedTimer deadlockTimer;
    QElapsedTimer deadlockCooldownTimer;

//     QVector<std::string> pathIcons = {"", "", "", "", "", "", "", "", "", "", ""};
    QVector<QString> pathIcons = {"\033[0;34m\033[0m",
                                  "\033[0;96m\033[0m",
                                  "\033[0;90m\033[0m",
                                  "\033[0;33m\033[0m",
                                  "\033[0;95m\033[0m",
                                  "\033[0;21m\033[0m"};

    void updateObstacles(const AmbienteCampo *_environment);
    MRAPath findPath(Cell *_begin, Cell *_end, int _n, bool _coordinate);
    MRAPath refinePath(QVector2D _start, QVector2D _end, int _n);

    void addNeighbors(Cell &_cell, bool _coordinate);
    float calculatePathCosts(QVector<MRAPath> &_solution);
    float sumCosts(QVector<MRAPath> &_solution);
    void convertPositions(const QVector<QVector2D> &_starts,
                          const QVector<QVector2D> &_ends, Cell **_begin,
                          Cell **_end, int _n);
    void convertPositions(QVector2D _start, QVector2D _end, Cell **_begin,
                          Cell **_goal);
    void setupNewSearch();
    void clearPaths(QVector<MRAPath> &_paths);
    void clearPaths();
    void updatePaths();
    float findPaths(const QVector<QVector2D> &_start,
                    const QVector<QVector2D> &_end, bool _coordinate = false);
    bool checkPaths(const AmbienteCampo *_environment);
    void setup(const AmbienteCampo *_environment, QVector<QVector2D> &_start,
               QVector<QVector2D> &_end);
    void setSolution(AmbienteCampo *_environment,
                     const QVector<QVector2D> &_start,
                     const QVector<QVector2D> &_end);

public:
    MRAStar(int _N, QSize _fieldSize, int _gridResolution);
    ~MRAStar();

    void findCoordinatedPaths(AmbienteCampo *_environment);
    QString exportConfig() const;
    bool isInDeadlock(AmbienteCampo* _environment);
};

#endif // MRASTAR_H
