#include "path.h"
#include "qglobal.h"

Path::Path()
{
    reducedPath.clear();
    fullPath.clear();
    pathAngle.clear();
    speedProfile.clear();
    bChanged = false;
    fLength = 0;

    rgbColor.setRed(QRandomGenerator::global()->bounded(256));
    rgbColor.setGreen(QRandomGenerator::global()->bounded(256));
    rgbColor.setBlue(QRandomGenerator::global()->bounded(256));
}

Path::Path(const Path& _other)
{
    vUpdatePath(_other.getPath());
    fullPath.clear();
    fullPath.append(_other.getFullPath());
    setPathAngle(_other.getPathAngle());
    setSpeedProfile(_other.getSpeedProfile());

    fLength = _other.pathLength();

    bChanged = true;
    if (_other.pathChanged() == false)
    {
        bChanged = false;
    }

    vSetColor(_other.rgbGetColor());
}

void Path::setFromPath(const Path &_other)
{
    vUpdatePath(_other.getPath());
    fullPath.clear();
    fullPath.append(_other.getFullPath());
    setPathAngle(_other.getPathAngle());
    setSpeedProfile(_other.getSpeedProfile());

    fLength = _other.pathLength();

    bChanged = true;
    if (_other.pathChanged() == false)
    {
        bChanged = false;
    }

    vSetColor(_other.rgbGetColor());
}

void Path::vSetPath(const QVector<QVector2D> _points, const float _length,
                    const bool _changed)
{
    fLength = _length;

    bChanged = true;
    if (_changed == false)
    {
        bChanged = false;
    }

    reducedPath.clear();
    fullPath.clear();

    reducedPath.append(_points);
    fullPath.append(_points);
}

bool Path::setSpeedProfile(const QVector<float> _profile,
                           const QString _callerFunc)
{
    if (_profile.size() == fullPath.size())
    {
        speedProfile.clear();
        speedProfile.append(_profile);
        return true;
    }
    //     qInfo() << "O trajeto deve ser atribuído antes do perfil de
    //     velocidades! " << _callerFunc;
    return false;
}

bool Path::setPathAngle(const QVector<float> _angles, const QString _callerFunc)
{
    if (_angles.size() == fullPath.size())
    {
        pathAngle.clear();
        pathAngle.append(_angles);
        return true;
    }
    //     qInfo() << "O trajeto deve ser atribuído antes dos ângulos! " <<
    //     _callerFunc;
    return false;
}

void Path::vUpdatePath(const QVector<QVector2D> _points)
{
    bChanged = false;
    reducedPath.clear();
    reducedPath.append(_points);
}

void Path::updateTarget(const QVector2D& _target)
{
    if (fullPath.size() > 0)
    {
        QVector<QVector2D> updatedPath = getFullPath();
        updatedPath.replace(updatedPath.size() - 1, _target);
        vSetPath(updatedPath, pathLength(), true);
    }
}

void Path::clear()
{
    bChanged = true;
    reducedPath.clear();
    fullPath.clear();
    pathAngle.clear();
    speedProfile.clear();
    fLength = 0;
}
QVector<QVector2D> Path::getPath() const
{
    return reducedPath;
}

QVector<QVector2D> Path::getFullPath() const
{
    return fullPath;
}

QVector<float> Path::getPathAngle() const
{
    return pathAngle;
}

QVector<float> Path::getSpeedProfile() const
{
    return speedProfile;
}

float Path::pathLength() const
{
    return fLength;
}

bool Path::pathChanged() const
{
    return bChanged;
}

bool Path::bIsEmpty() const
{
    return reducedPath.isEmpty();
}

void Path::vSetColor(const QColor& color)
{
    rgbColor = color;
}

QColor Path::rgbGetColor() const
{
    return rgbColor;
}

QVector2D Path::vt2dGetNextPoint(QVector2D _robotPosition)
{
    QVector2D nextPoint;
    //     float minDistance = 1e10;
    int thresholdDistance = 400;
    //     int n = 0;
    //     int minN = 0;

    if (reducedPath.isEmpty())
    {
        nextPoint = _robotPosition;
    }
    else
    {
        //         for(QVector2D point : fullPath)
        //         {
        //             if(point.distanceToPoint(_robotPosition) < minDistance)
        //             {
        //                 nextPoint = point;
        //                 minDistance = point.distanceToPoint(_robotPosition);
        //                 minN = n;
        //             }
        //             n++;
        //         }
        if (_robotPosition.distanceToPoint(reducedPath.constLast()) < 700)
        {
            thresholdDistance = globalConfig.robotDiameter;
        }

        if (_robotPosition.distanceToPoint(reducedPath.constFirst()) <
            thresholdDistance)
        {
            reducedPath.removeFirst();

            if (reducedPath.isEmpty())
            {
                reducedPath.append(fullPath.constLast());
            }
        }
        nextPoint = reducedPath.constFirst();
    }

    return nextPoint;
    //     return fullPath.at(qMin(minN+1, fullPath.size() - 1));
}

QVector2D Path::followPoint(QVector2D _robotPosition)
{
    if (fullPath.isEmpty())
    {
        return {1.0e10, 1.0e10};
    }

    int nPoints = qMax(fullPath.size() * 0.1, 1.0);
    
    float minDistance = 999999, distance;
    int pointIndex = 0;
    for (int i = 0; i < fullPath.size(); ++i)
    {
        distance = fullPath.at(i).distanceToPoint(_robotPosition);

        if (distance < minDistance)
        {
            minDistance = distance;
            pointIndex = i;
        }
    }
    pointIndex = qMin(pointIndex + nPoints, fullPath.size()-1);

    return fullPath.at(pointIndex);
}

QVector2D Path::getPathAt(int _index, bool& ok) const
{
    QVector2D point;
    if (!reducedPath.isEmpty() && reducedPath.size() > _index && _index >= 0)
    {
        ok = true;
        point = reducedPath.at(_index);
    }
    else
    {
        ok = false;
    }
    return point;
}

QVector2D Path::getFullPathAt(int _index, bool& ok) const
{
    QVector2D point;
    if (!fullPath.isEmpty() && fullPath.size() > _index && _index >= 0)
    {
        ok = true;
        point = fullPath.at(_index);
    }
    else
    {
        ok = false;
    }
    return point;
}
