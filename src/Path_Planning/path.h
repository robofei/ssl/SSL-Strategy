#ifndef PATH_H
#define PATH_H

#include <QDebug>
#include <QVector>
#include <QVector2D>
#include <QRandomGenerator>
#include <QScopedPointer>
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include <QColor>

class Path
{
    QVector<QVector2D> reducedPath;
    QVector<QVector2D> fullPath;
    QVector<float> speedProfile;
    QVector<float> pathAngle;

    bool bChanged;
    float fLength;
    QColor rgbColor;

public:
    Path();
    Path(const Path &_other);

    void setFromPath(const Path &_other);
    void vSetPath(const QVector<QVector2D> _points, const float _length,
                  const bool _changed = true);
    bool setSpeedProfile(const QVector<float> _profile,
                         const QString _callerFunc = QString(__builtin_FUNCTION()));
    bool setPathAngle(const QVector<float> _angles,
                      const QString _callerFunc = QString(__builtin_FUNCTION()));

    void vUpdatePath(const QVector<QVector2D> _points);
    void updateTarget(const QVector2D &_target);
    void clear();

    QVector<QVector2D> getPath() const;
    QVector<QVector2D> getFullPath() const;
    QVector<float> getPathAngle() const;
    QVector<float> getSpeedProfile() const;

    float pathLength() const;
    bool pathChanged() const;
    bool bIsEmpty() const;

    void vSetColor(const QColor& color);
    QColor rgbGetColor() const;

    QVector2D vt2dGetNextPoint(QVector2D _robotPosition);
    QVector2D followPoint(QVector2D _robotPosition);

    QVector2D getPathAt(int _index, bool &ok) const;
    QVector2D getFullPathAt(int _index, bool &ok) const;
    float getSpeed(int _index, bool &ok) const;
    float getAngle(int _index, bool &ok) const;
};

#endif // PATH_H
