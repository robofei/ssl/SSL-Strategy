/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "path_p_rrt_2.h"

RRT::RRT(QSize _szCampo)
{
    fDistanciaAvancoBase    = 360;
    iNumeroMaximoIteracoes  = 200;
    iDistanciaMinimaDestino = 360;
    iLeafSize               = 500;
    szCampo = _szCampo;
}

RRT::~RRT()
{
}

void RRT::vSetaParametrosRRT(const float &distAvanco, const int &maxIte, const int &distDest, const int &leafSize)
{
    fDistanciaAvancoBase    = distAvanco;
    iNumeroMaximoIteracoes  = maxIte;
    iDistanciaMinimaDestino = distDest;
    iLeafSize 				= leafSize;
}

double RRT::dRetornaTempoCalculoCaminho()
{
    if(qIsFinite(dTempoCalculoCaminho))
        return dTempoCalculoCaminho;
    else
        return -1;
}

Point RRT::ptEscolherAleatorio()
{
    Point ptAuxiliar;

//    ptVerticeAleatorio = ptDestino;
    ptVerticeAleatorio.x = rndGen.bounded(static_cast<int>(-szCampo.width()/2),
                                          static_cast<int>(szCampo.width()/2 + 1));

    ptVerticeAleatorio.y = rndGen.bounded(static_cast<int>(-szCampo.height()/2),
                                          static_cast<int>(szCampo.height()/2 + 1));

//    while(QVector2D(ptVerticeAleatorio.x ptVerticeAleatorio.y).distanceToPoint( QVector2D(ptDestino.x, ptDestino.y) ) >
//          QVector2D(ptVerticeRaiz.x, ptVerticeRaiz.y).distanceToPoint( QVector2D(ptDestino.x, ptDestino.y) ) )
//    {
//        ptVerticeAleatorio.x = ((qrand() % (int)(vt2dTamanhoCampo.x()/2)) * ( ( (qrand() )%2 == 0)? 1 : -1) );
//        ptVerticeAleatorio.y = ((qrand() % (int)(vt2dTamanhoCampo.y()/2)) * ( ( (qrand() )%2 == 0)? 1 : -1) );

//    }

//    vDefineVerticeProximo();

//    if(bNovoVertice(ptVerticeAleatorio) == true)
//    {
        ptAuxiliar = ptVerticeAleatorio;

//        fDistanciaAvancoBase = QVector2D(ptVerticeProximo.x, ptVerticeProximo.y).distanceToPoint(
//                                                              QVector2D(ptDestino.x, ptDestino.y));
//    }
//    else
//    {
//        fDistanciaAvancoBase = 70;
//        ptAuxiliar.x = qCeil( (qrand() % (int)(vt2dTamanhoCampo.x()/2) ) * ( ( (qrand() )%2 == 0)? 1 : -1) );
//        ptAuxiliar.y = qCeil( (qrand() % (int)(vt2dTamanhoCampo.y()/2) ) * ( ( (qrand() )%2 == 0)? 1 : -1) );
//    }

    return ptAuxiliar;
}

void RRT::vDefineVerticeProximo(KDTree &index)
{
    double query_pt[2] = {ptVerticeAleatorio.x, ptVerticeAleatorio.y};
    size_t num_results = 1;
    size_t ret_index;
    double out_dist_sqr;

    nanoflann::KNNResultSet<double> resultSet(num_results);
            resultSet.init(&ret_index, &out_dist_sqr );
    index.findNeighbors(resultSet, query_pt, nanoflann::SearchParams(10));

    if(sqrt(out_dist_sqr) < 50e3 && ret_index < Arvore.pts.size() )
    {
        ptVerticeProximo.x = Arvore.pts.at(ret_index).x;
        ptVerticeProximo.y = Arvore.pts.at(ret_index).y;
    }
}

Mensagem RRT::msgExtenderArvore(KDTree &index)
{
    Point ptNovoVertice;
    vDefineVerticeProximo(index);

    if(bNovoVertice(ptNovoVertice))
    {
        Arvore.pts.push_back(ptNovoVertice);
        Arvore.parent.push_back(ptVerticeProximo);

        index.addPoints(static_cast<long int>(Arvore.pts.size()) - 2 < 0 ? 0 : Arvore.pts.size() - 2,
                        Arvore.pts.size() - 1);

        if(QVector2D(ptNovoVertice.x, ptNovoVertice.y).distanceToPoint(vt2dDestino) < iDistanciaMinimaDestino)
        {
            iIndiceDestino = Arvore.pts.size()-1;
            return Chegou;
        }
        else return Avancou;
    }

    return EstadoVazio;
}

bool RRT::bNovoVertice(Point &_ptNovoVertice)
{
    QVector2D vt2dVersorDirecaoNovoVertice =
            QVector2D(ptVerticeAleatorio.x, ptVerticeAleatorio.y) - QVector2D(ptVerticeProximo.x, ptVerticeProximo.y);

    vt2dVersorDirecaoNovoVertice.normalize();

    _ptNovoVertice.x = qCeil(ptVerticeProximo.x + vt2dVersorDirecaoNovoVertice.x() * fDistanciaAvancoBase);
    _ptNovoVertice.y = qCeil(ptVerticeProximo.y + vt2dVersorDirecaoNovoVertice.y() * fDistanciaAvancoBase);

    if(!bChecaColisao(_ptNovoVertice))
        return true;
    else return false;
}

bool RRT::bChecaColisao(Point &ptPonto)
{
    for(auto n : vt3dObstaculos)
    {
        if(QVector2D(ptPonto.x, ptPonto.y).distanceToPoint(n.toVector2D()) < globalConfig.safety.ally)
            return true;
    }

    if(Auxiliar::bChecaInterseccaoObjetosLinha(QVector2D(ptPonto.x, ptPonto.y),
                                               QVector2D(ptVerticeProximo.x, ptVerticeProximo.y),
                                               bNaoConsideraBola, globalConfig.safety.ally, vt3dObstaculos))
        return true;

    return false;
}

QVector<QVector2D> RRT::vt2dExtraiCaminhoFinal(KDTree &index)
{
    QVector<Point> ptCaminhoFinal;
    QVector<QVector2D> vt2dCaminhoFinal;
    QVector<Point> temp;

    ptCaminhoFinal.clear();
    vt2dCaminhoFinal.clear();
    temp.clear();

    temp.append(Arvore.pts[iIndiceDestino]);
    vt2dCaminhoFinal.append(QVector2D(Arvore.pts[iIndiceDestino].x, Arvore.pts[iIndiceDestino].y));

    double query_pt[2] = {Arvore.parent[iIndiceDestino].x, Arvore.parent[iIndiceDestino].y};
    size_t num_results = 1;
    size_t ret_index;
    double out_dist_sqr;

    Point ptAux;
    nanoflann::KNNResultSet<double> resultSet(num_results);

    while(QVector2D(temp.last().x, temp.last().y).distanceToPoint(QVector2D(ptVerticeRaiz.x, ptVerticeRaiz.y)) > 0)
    {
        resultSet.init(&ret_index, &out_dist_sqr );
        index.findNeighbors(resultSet, query_pt, nanoflann::SearchParams(10));

        ptAux.x = Arvore.pts[ret_index].x;
        ptAux.y = Arvore.pts[ret_index].y;

        temp.append(ptAux);
        vt2dCaminhoFinal.prepend(QVector2D(temp.last().x, temp.last().y));

        query_pt[0] = Arvore.parent[ret_index].x;
        query_pt[1] = Arvore.parent[ret_index].y;

        index.removePoint(ret_index);
    }
    vt2dCaminhoFinal.append(vt2dDestino);

    return vt2dCaminhoFinal;
}

QVector<QVector2D> RRT::vt2dCalculaTrajeto(AmbienteCampo *acAmbiente, int _iRoboID,
                                           QVector<QVector2D> *_vt2dPontos,
                                           QVector<QVector2D> *_vt2dParents,
                                           QVector<QVector2D> *_vt2dCaminhoNaoReduzido,
                                           int *_iteracoes)
{
    Arvore.pts.clear();
    Arvore.parent.clear();

    QVector<QVector2D> vt2dCaminhoFinal, vt2dCaminhoAnterior;
    vt2dCaminhoFinal.clear(); vt2dCaminhoAnterior.clear();
    vt2dCaminhoAnterior.append(acAmbiente->allies->getPlayer(_iRoboID)->path->getPath());
    KDTree index(2, Arvore, nanoflann::KDTreeSingleIndexAdaptorParams(iLeafSize));

    vSetup(acAmbiente, _iRoboID, index);
    dTempoCalculoCaminho = 0;

    if(bChecaCaminhoAnterior(vt2dCaminhoAnterior) == false)
    {
        return acAmbiente->allies->getPlayer(_iRoboID)->path->getPath();
    }

    QElapsedTimer etmRRT;
    etmRRT.start();
    int i = 0;
    for(i=0; i < iNumeroMaximoIteracoes; ++i)
    {
        ptVerticeAleatorio = ptEscolherAleatorio();
        if(msgExtenderArvore(index) == Chegou)
        {
            bCalculandoCaminho = false;

            if(_vt2dPontos != nullptr)
            {
                _vt2dPontos->clear();

                for(auto &n : Arvore.pts)
                    _vt2dPontos->append(QVector2D(n.x, n.y));
            }
            if(_vt2dParents != nullptr)
            {
                _vt2dParents->clear();

                for(auto &n : Arvore.parent)
                    _vt2dParents->append(QVector2D(n.x, n.y));
            }

            if(_iteracoes != nullptr)
                *_iteracoes = i;

            QVector<QVector2D> dummy;
            vt2dCaminhoFinal = vt2dExtraiCaminhoFinal(index);

            if(_vt2dCaminhoNaoReduzido != nullptr)
            {
                _vt2dCaminhoNaoReduzido->clear();
                _vt2dCaminhoNaoReduzido->append(vt2dCaminhoFinal);
            }

            vt2dCaminhoFinal = vt2dSimplificaCaminho(acAmbiente, _iRoboID,
                                                    vt2dCaminhoFinal, globalConfig.safety.ally);
            QVector<float> pathAngle, speedProfile;
            vt2dCaminhoFinal = vt2dTrataCaminhoFinal(vt2dCaminhoFinal, dummy);
//            vt2dCaminhoFinal.prepend(vt2dPosicaoInicial);

            vCalculaDistanciaObstaculos(vt2dCaminhoFinal);
            dTempoCalculoCaminho  = (double)(etmRRT.nsecsElapsed()/1e6);

            acAmbiente->allies->getPlayer(_iRoboID)->path->vSetPath(vt2dCaminhoFinal,
                                                                    fComprimentoCaminho);
            acAmbiente->allies->getPlayer(_iRoboID)->path->setPathAngle(pathAngle);
            acAmbiente->allies->getPlayer(_iRoboID)->path->setSpeedProfile(speedProfile);
            return vt2dCaminhoFinal;
        }
    }

    if(_iteracoes != nullptr)
        *_iteracoes = i;

    bCalculandoCaminho = false;
    dTempoCalculoCaminho = (double)(etmRRT.nsecsElapsed()/1e6);
    acAmbiente->allies->getPlayer(_iRoboID)->path->vSetPath(vt2dCaminhoFinal,
                                                                fComprimentoCaminho);
    return vt2dCaminhoFinal;
}

void RRT::vSetup(AmbienteCampo *acAmbiente, const int &_iRoboID, KDTree &index)
{
    bCalculandoCaminho = true;

    bIgnorarBola = acAmbiente->allies->getPlayer(_iRoboID)->bPegaIgnorarBola();
    vt2dDestino = acAmbiente->allies->getPlayer(_iRoboID)->vt2dDestino();
    vt2dPosicaoInicial = acAmbiente->allies->getPlayer(_iRoboID)->vt2dPosicaoAtual();

    dTempoCalculoCaminho = 0;
    vt3dObstaculos.clear();

    Arvore.pts.clear(); Arvore.parent.clear();

    QVector<int> IDRobo;
    IDRobo.clear();

    IDRobo.append(_iRoboID);
    vt3dObstaculos.append(acAmbiente->vt3dPegaPosicaoTodosObjetos(otOponente));
    vt3dObstaculos.append(acAmbiente->vt3dPegaPosicaoTodosObjetos(otAliado, true, IDRobo));

    if(bIgnorarBola == bConsideraBola)
        vt3dObstaculos.append(acAmbiente->vt3dPegaPosicaoTodosObjetos(otBola));

    ptVerticeRaiz.x = vt2dPosicaoInicial.x();
    ptVerticeRaiz.y = vt2dPosicaoInicial.y();

    Point aux;

    aux.x = vt2dPosicaoInicial.x();
    aux.y = vt2dPosicaoInicial.y();

    Arvore.pts.push_back(aux);
    Arvore.parent.push_back(aux);
    index.addPoints(0, 1);

    ptVerticeProximo.x = vt2dPosicaoInicial.x();
    ptVerticeProximo.y = vt2dPosicaoInicial.y();
}
