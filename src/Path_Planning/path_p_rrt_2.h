/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PATH_P_RRT_2_H
#define PATH_P_RRT_2_H

///
/// \file path_p_rrt_2.h
/// \brief \a Point, \a Tree e \a RRT
///

#include "Path_Planning/nanoflann.hpp"
#include "Path_Planning/pathplanninggeneric.h"

#include <QDebug>


/**
 * @brief
 *
 */
enum Mensagem
{
    EstadoVazio,
    Chegou,
    Avancou,
};

/**
 * @brief Struct que representa os pontos da árvore.
 *
 */
struct Point
{
    double  x,y; /**< Coordenadas x e y do ponto. */
};

/**
 * @brief Struct que representa a árvore do algoritmo.
 *
 */
struct Tree
{
    std::vector<Point> pts; /**< Pontos da árvore. */
    std::vector<Point> parent; /**< Pais dos pontos da árvore. */

    /**
     * @brief Must return the number of data points
     *
     * @return size_t
     */
    inline size_t kdtree_get_point_count() const { return pts.size(); }

    /**
     * @brief
     *
     * @param idx Returns the dim'th component of the idx'th point in the class:
     * Since this is inlined and the "dim" argument is typically an immediate value, the
     * "if/else's" are actually solved at compile time.
     *
     * @param dim
     * @return float
     */
    inline float kdtree_get_pt(const size_t idx, int dim) const
    {
        if (dim == 0) return pts[idx].x;
        else if (dim == 1) return pts[idx].y;
        return INFINITY;
    }

    // Optional bounding-box computation: return false to default to a standard bbox computation loop.
    //   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo
    //   it again.
    //   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
    template <class BBOX>
    bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }

};

/**
 * @brief Classe do path-planner RRT.
 *
 */
class RRT : public PathPlanningGeneric
{
public:
    /**
     * @brief Construtor da classe.
     *
     * @param _vt2dTamanhoCampo
     */
    RRT(QSize _szCampo);//

    /**
     * @brief Destrutor da classe.
     *
     */
    ~RRT();

    /**
     * @brief Seta os parâmetros do RRT.
     * @param distAvanco - #fDistanciaAvancoBase.
     * @param maxIte - #iNumeroMaximoIteracoes.
     * @param distDest - #iDistanciaMinimaDestino.
     * @param leafSize - #iLeafSize.
     */
    void vSetaParametrosRRT(const float &distAvanco, const int &maxIte,
                            const int &distDest  , const int &leafSize);

    /**
     * @brief Retorna o tempo de cálculo do algoritmo.
     *
     * @return double
     */
    double dRetornaTempoCalculoCaminho();

    /**
     * @brief Calcula um trajeto para o robô cujo ID deve ser fornecido.
     *
     * @param acAmbiente - Ambiente do campo.
     * @param _iRoboID - ID do robô que terá seu trajeto calculado.
     * @param _vt2dPontos - Utilizado para retornar os pontos da #Arvore gerada.
     * @param _vt2dParents - Utilizado para retornar os pontos dos pais dos pontos da #Arvore gerada.
     * @param _vt2dCaminhoNaoReduzido - Utilizado para retornar o trajeto encontrado sem tratamento nenhum.
     * @param _iteracoes - Utilizado para retornar a quantidade de iterações do algoritmo.
     * @return QVector<QVector2D> - Trajeto calculado.
     */
    QVector<QVector2D> vt2dCalculaTrajeto(AmbienteCampo *acAmbiente, int _iRoboID,
                                          QVector<QVector2D> *_vt2dPontos = nullptr,
                                          QVector<QVector2D> *_vt2dParents = nullptr,
                                          QVector<QVector2D> *_vt2dCaminhoNaoReduzido = nullptr,
                                          int *_iteracoes = nullptr);

private:
    /**
     * @brief Tipo que implementa uma KDTree.
     */
    typedef nanoflann::KDTreeSingleIndexDynamicAdaptor<nanoflann::L2_Simple_Adaptor<double, Tree > ,Tree, 2> KDTree;

    Tree Arvore;   /**< Árvore do algoritmo. */

    float fDistanciaAvancoBase; /**< Distância de avaço do algoritmo. */
    int iNumeroMaximoIteracoes; /**< Número máximo de iterações do algoritmo. */
    int iDistanciaMinimaDestino; /**< Distância mínima para considerar que o algoritmoo
                                        chegou ao destino.*/
    int iLeafSize; /**< Leaf size da KD-Tree. */

    Point ptVerticeProximo;     /**< Vértice da #Arvore mais próximo do #ptVerticeAleatorio gerado. */
    Point ptVerticeRaiz;        /**< Vértice raiz da #Arvore. */
    Point ptVerticeAleatorio;   /**< Vértice aleatório gerado no campo. */
    QSize szCampo; /**< Tamanho do campo. */
    int iIndiceDestino;         /**< Índice do vértice mais próximo do #vt2dDestino.
                                    Este índice é determinado quando o algoritmo chega ao destino.*/

    bool bCalculandoCaminho = false; /**< True se o algoritmo está claculando um trajeto no momento. */
    bool bIgnorarBola;               /**< Indica se a bola deve ser ignorada. */
    QRandomGenerator rndGen;         /**< Gerador de números aleatórios. */

    /**
     * @brief Realiza o setup necessário para iniciar o algoritmo.
     *
     * @param acAmbiente - Ambiente do campo.
     * @param _iRoboID - ID do robô que terá seu trajeto calculado.
     */
    void vSetup(AmbienteCampo *acAmbiente, const int &_iRoboID, KDTree &index);

    /**
     * @brief Gera um vértice aleatório no campo.
     *
     * @return Point
     */
    Point ptEscolherAleatorio();//

    /**
     * @brief Determina qual o vértice da árvore mais próximo do #ptVerticeAleatorio gerado.
     *
     */
    void vDefineVerticeProximo(KDTree &index);//

    /**
     * @brief Extende a #Arvore uma distância #fDistanciaAvancoBase em direção ao vértice aleatório.
     *
     * @return Mensagem
     */
    Mensagem msgExtenderArvore(KDTree &index);//

    /**
     * @brief Gera o vértice utilizado para extender a #Arvore (#msgExtenderArvore) e retorna true
     * caso este vértice seja válido (não cause colisão).
     *
     * @param ptNovoVertice - Vértice utilizado na extensão.
     * @return bool
     */
    bool bNovoVertice(Point &ptNovoVertice);//

    /**
     * @brief Checa se o vértice fornecido causa colisão com algum obstáculo (#vt3dObstaculos).
     *
     * @param ptPonto - Vértice de teste.
     * @return bool - True se ocorre colisão.
     */
    bool bChecaColisao(Point &ptPonto);//

    /**
     * @brief Extrai o caminho encontrado da árvore.
     *
     * @return QVector<QVector2D>
     */
    QVector<QVector2D> vt2dExtraiCaminhoFinal(KDTree &index);//
};

#endif // PATH_P_RRT_2_H
