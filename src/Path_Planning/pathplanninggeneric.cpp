/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pathplanninggeneric.h"
#include "Path_Planning/path.h"

// Descomentar para utilizar com o DVG
// #define NO_AVOIDANCE_ALGORITHM

PathPlanningGeneric::PathPlanningGeneric()
{
    dTempoCalculoCaminho = -1;
    fComprimentoCaminho = -1;
    fDistanciaObstaculos = -1;
    vt3dObstaculos.clear();
}

float PathPlanningGeneric::fRetornaDistanciaObstaculos()
{
    return fDistanciaObstaculos;
}

int PathPlanningGeneric::iDetectaColisao()
{
    int colisao = 0;
    for (const QVector3D& obs : vt3dObstaculos)
    {
        if (obs.toVector2D().distanceToPoint(vt2dPosicaoInicial) <=
            1.05 * globalConfig.robotDiameter)
            colisao += 1;
    }
    return colisao;
}

double PathPlanningGeneric::computationalTime() const
{
    if (qIsFinite(dTempoCalculoCaminho))
    {
        return dTempoCalculoCaminho;
    }
    else
    {
        return -1;
    }
}

double PathPlanningGeneric::mapUpdateTime() const
{
    if (qIsFinite(mapTime))
    {
        return mapTime;
    }
    else
    {
        return -1;
    }
}

float PathPlanningGeneric::fCalculaComprimentoCaminho(
    const QVector<QVector2D>& _caminho)
{
    float comp = 0;
    for (int j = 0; j < _caminho.size() - 1; ++j)
    {
        comp += _caminho.at(j).distanceToPoint(_caminho.at(j + 1));
    }
    return comp;
}

void PathPlanningGeneric::calculatePathAngle(const QVector<QVector2D>& _path,
                                             QVector<float>& _angle)
{
    _angle.clear();

    for (int i = 0; i < _path.size() - 1; ++i)
    {
        QVector2D dPath = _path.at(i + 1) - _path.at(i);

        _angle.append(qAtan2(dPath.y(), dPath.x()));
    }
    if (_angle.isEmpty())
    {
        _angle.append(0);
    }
    else
    {
        _angle.append(_angle.at(_angle.size() - 1));
    }
}

void PathPlanningGeneric::generateSpeedProfile(QVector<float>& _profile,
                                               int _size, float _maxVelocity)
{
    if (_size < 1)
        return;
    _profile.clear();
    float speed = 0, minSpeed = 1.0f;
    int minPoints = 2;

    int breakingPoints = 0;
    float deacceleration = 0;

    breakingPoints = qMax(_size * 0.10f, (float)minPoints);
    deacceleration = _maxVelocity / breakingPoints;

    if (_size > minPoints && _size >= breakingPoints * 2)
    {
        _profile.fill(_maxVelocity, _size);
    }
    else
    {
        _profile.fill(minSpeed, _size);
        minPoints = 0;
        breakingPoints = qMax(_size * 0.05f, (float)minPoints);
        deacceleration = _maxVelocity / breakingPoints;
    }

    for (int i = 0, j = _size - 1; i < _size; ++i, --j)
    {
        speed = deacceleration * i;
        _profile.replace(i, qMin(qMax(speed, minSpeed), _maxVelocity));
        _profile.replace(j, qMin(qMax(speed, minSpeed), _maxVelocity));
        if (i == j)
        {
            break;
        }
    }
    _profile.replace(_size - 1, 0.8f);
}

QVector<QVector2D> PathPlanningGeneric::vt2dTrataCaminhoFinal(
    const QVector<QVector2D>& caminhoFinal,
    QVector<QVector2D>& _vt2dCaminhoSemFiltro)
{
    QVector<QVector2D> caminhoReduzido, caminhoAux;
    caminhoReduzido.clear();
    caminhoAux.clear();
    pathAngle.clear();
    speedProfile.clear();
    _vt2dCaminhoSemFiltro.clear();
    _vt2dCaminhoSemFiltro.append(caminhoFinal);

    if (!caminhoFinal.isEmpty())
    {
        float comprimentoInicial = 0;
        caminhoAux = caminhoFinal;
            // vt2dInserePontosCaminho(caminhoFinal, comprimentoInicial, 1000);

        const int anguloCurva = 15;

        QVector<QVector2D> vt2dAux, vt2dCurva;
        vt2dCurva.clear();
        vt2dAux.clear();
        while (caminhoAux.size() > 0)
        {
            int n = caminhoAux.size() - 1;
            for (; n > 3; n--)
            {
                if (Auxiliar::dAnguloVetor1Vetor2(
                        caminhoAux.at(n) - caminhoAux.at(n - 1),
                        caminhoAux.at(n - 1) - caminhoAux.at(n - 2)) >
                    anguloCurva)
                {
                    vt2dAux.prepend(caminhoAux.at(n));
                    vt2dAux.prepend(caminhoAux.at(n - 1));
                    vt2dAux.prepend(caminhoAux.at(n - 2));
                    n -= 2;
                    break;
                }
                else
                {
                    caminhoReduzido.prepend(caminhoAux.at(n));
                }
            }

            if (vt2dAux.size() > 1)
            {
                vt2dCurva = Auxiliar::vt2dCalculaCurvaBezier(vt2dAux);
                for (QVector<QVector2D>::iterator it = vt2dCurva.end() - 1;
                     it >= vt2dCurva.begin(); it--)
                {
                    caminhoReduzido.prepend(*it);
                }
            }
            caminhoAux.remove(n, caminhoAux.size() - n);
            vt2dAux.clear();
        }
        caminhoReduzido =
            vt2dInserePontosCaminho(caminhoReduzido, comprimentoInicial, 500);

        fComprimentoCaminho = fCalculaComprimentoCaminho(caminhoReduzido);

        calculatePathAngle(caminhoReduzido, pathAngle);
        generateSpeedProfile(speedProfile, caminhoReduzido.size(),
                             globalConfig.robotVelocities.linear.limit);
        if (speedProfile.size() != caminhoReduzido.size())
        {
            qCritical() << "Something weird is happening!";
        }
    }
    return caminhoReduzido;
}

QVector<QVector2D> PathPlanningGeneric::vt2dSimplificaCaminho(
    const AmbienteCampo* acAmbiente, const int iRoboID,
    QVector<QVector2D>& _vt2dCaminho, const int distSeguranca)
{
    QVector<QVector2D> caminhoReduzido;
    caminhoReduzido.clear();

    int n = _vt2dCaminho.size() - 1;
    int i = 0;

    caminhoReduzido.append(_vt2dCaminho.at(n));
    while (n > 0)
    {
        while (Auxiliar::bChecaInterseccaoObjetosLinha(
            _vt2dCaminho[n], _vt2dCaminho[i],
            acAmbiente->allies->getPlayer(iRoboID)->bPegaIgnorarBola(),
            distSeguranca, vt3dObstaculos))
        {
            ++i;
        }
        caminhoReduzido.prepend(_vt2dCaminho[i]);

        if (i == 0)
        {
            bool falseNeg = Auxiliar::bChecaInterseccaoObjetosLinha(
                caminhoReduzido[0], caminhoReduzido[1],
                acAmbiente->allies->getPlayer(iRoboID)->bPegaIgnorarBola(),
                distSeguranca, vt3dObstaculos);
            //            Q_ASSERT_X(falseNeg == true, "vt2dSimplificaCaminho",
            //            "Falso positivo"
            //                                                                       "gerado na "
            //                                                                       "simplificação"
            //                                                                       "do caminho");
            if (falseNeg)
            {
                qCritical()
                    << Q_FUNC_INFO << '\n'
                    << "Falso positivo gerado na simplificação do caminho";
            }
        }

        // Manter esse código aqui por um tempo, ele estava causando um bug
        // mas acredito que ele servia para prevenir algo de acontecer, fiz
        // alguns testes e não houve nenhum problema, se não der problema nenhum
        // daqui algum tempo, pode remover essas linhas comentadas. - Leonardo
        // (31/06/2020)
        //        if(n < i)
        //            n = i;
        //        else
        //            n = i - 1;

        n = i;
        i = 0;

        if (caminhoReduzido.size() >= _vt2dCaminho.size())
            break;
    }
    return caminhoReduzido;
}

QVector<QVector2D> PathPlanningGeneric::vt2dInserePontosCaminho(
    const QVector<QVector2D>& _caminho, float& comprimentoInicial,
    int distanciaLimite)
{
    QVector<QVector2D> caminhoAux;
    caminhoAux.clear();
    comprimentoInicial = 0;
    for (int i = _caminho.size() - 1; i > 0; --i)
    {
        comprimentoInicial +=
            _caminho.at(i).distanceToPoint(_caminho.at(i - 1));

        if (_caminho.at(i).distanceToPoint(_caminho.at(i - 1)) >
            distanciaLimite)
        {
            vInserePontos(_caminho.at(i), _caminho.at(i - 1), caminhoAux);
        }
        else
        {
            caminhoAux.prepend(_caminho.at(i));
            caminhoAux.prepend(_caminho.at(i - 1));
        }
    }
    if (_caminho.isEmpty() == false)
    {
        caminhoAux.prepend(_caminho.at(0));
    }

    return caminhoAux;
}

void PathPlanningGeneric::vInserePontos(const QVector2D vt2dPontoInicial,
                                        const QVector2D vt2dPontoFinal,
                                        QVector<QVector2D>& vt2dSaidaPontos)
{
    vt2dSaidaPontos.prepend(vt2dPontoInicial);

    int iEspacoPontos = 50; // mm de espaço entre cada ponto, era 350
    int nPontos = qFloor(
        static_cast<double>((vt2dPontoFinal - vt2dPontoInicial).length()) /
        iEspacoPontos);

    QVector2D vt2dAux = vt2dPontoFinal - vt2dPontoInicial;
    vt2dAux.normalize();
    vt2dAux *= iEspacoPontos;

    if (nPontos < 2000)
    {
        while (nPontos > 0)
        {
            vt2dSaidaPontos.prepend(vt2dSaidaPontos.constFirst() + vt2dAux);
            nPontos--;
        }
    }
}

bool PathPlanningGeneric::bChecaCaminhoAnterior(
    const QVector<QVector2D>& vt2dCaminho)
{
    // Checa se o caminho deve ser recalculado
    bool bRecalcularCaminho = false;
    //  Analisa o caminho gerado anteriormente, caso o mesmo ainda seja
    //  utilizavel
    // nao sera calculado um novo
    if (!vt2dCaminho.isEmpty())
    {

#ifdef NO_AVOIDANCE_ALGORITHM
        if (vt2dCaminho.size() >= 2) // Se o caminho tiver mais de 2 pontos
        {

            //  Checa se existe algum objeto entre o ponto inicial
            // do caminho e a posicao inicial
            if (Auxiliar::bChecaInterseccaoObjetosLinha(
                    vt2dCaminho.constFirst(), vt2dPosicaoInicial,
                    globalConfig.safety.distance, vt3dObstaculos) == true)
            {
                bRecalcularCaminho = true;
            }
            else // Checa se existe algum objeto no meio do caminho
            {
                for (int n = 0; n < vt2dCaminho.size() - 1; ++n)
                {
                    if (Auxiliar::bChecaInterseccaoObjetosLinha(
                            vt2dCaminho[n], vt2dCaminho[n + 1],
                            globalConfig.safety.distance, vt3dObstaculos) == true)
                    {
                        bRecalcularCaminho = true;
                        break;
                    }
                }
            }
        }
        else // Se o caminho possui menos de 2 pontos, checa se existe algum
             // objeto entre a posicao inicial e o ponto inicial do caminho
        {
            if (Auxiliar::bChecaInterseccaoObjetosLinha(
                    vt2dCaminho.constFirst(), vt2dPosicaoInicial,
                    globalConfig.safety.distance, vt3dObstaculos) == true)
            {
                bRecalcularCaminho = true;
            }
        }
#endif // NO_AVOIDANCE_ALGORITHM

        //         Caso o destino nao tenha variado mais de 180/4 mm não é
        //         necessário calcular um novo caminho
        if (vt2dCaminho.last().distanceToPoint(vt2dDestino) > 200)
        {
            bRecalcularCaminho = true;
            //             qDebug() << "Recalculando | Destino diferente";
        }
        if (vt2dCaminho.constFirst().distanceToPoint(vt2dPosicaoInicial) > 3e3)
        {
            bRecalcularCaminho = true;
            //             qDebug() << "Recalculando | Caminho Longe";
        }
    }
    else // Caminho anterior nulo
    {
        bRecalcularCaminho = true;
    }
    return bRecalcularCaminho;
}

void PathPlanningGeneric::vCalculaDistanciaObstaculos(
    const QVector<QVector2D>& caminho)
{
    float dAux = 0, distCaminho = 10e3;
    QVector2D direcao;
    fDistanciaObstaculos = 0;

    for (int x = 0; x < vt3dObstaculos.size(); ++x)
    {
        for (int n = 0; n < caminho.size() - 1; ++n)
        {
            direcao = (caminho.at(n + 1) - caminho.at(n));
            direcao.normalize();
            dAux = vt3dObstaculos.at(x).toVector2D().distanceToLine(
                caminho.at(n + 1), direcao);

            if (dAux < distCaminho)
                distCaminho = dAux;
        }
        fDistanciaObstaculos += distCaminho;
        distCaminho = 10e3;
    }
}

void PathPlanningGeneric::startTimer()
{
    pathfindingTimer.restart();
}

void PathPlanningGeneric::registerMapTime()
{
    mapTime = pathfindingTimer.nsecsElapsed() / 1e6;
    pathfindingTimer.restart();
}

void PathPlanningGeneric::stopTimer()
{
    dTempoCalculoCaminho = pathfindingTimer.nsecsElapsed() / 1e6;
}
