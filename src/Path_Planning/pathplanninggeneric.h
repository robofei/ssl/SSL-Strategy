/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PATHPLANNINGGENERIC_H
#define PATHPLANNINGGENERIC_H

///
/// \file pathplanninggeneric.h
/// \brief \a PathPlanningGeneric
///

#include "Ambiente/futbolenvironment.h"
#include "qelapsedtimer.h"
#include "qvector.h"
#include "qvector2d.h"
#include <QVector2D>
#include <QVector3D>
#include <QVector>

/**
 * @brief Classe com algumas funções genéricas que os path-planners utilizam
 */
class PathPlanningGeneric
{
public:
    PathPlanningGeneric();

    /**
     * @brief Retorna a distância entre o último trajeto calculado e os
     * obstáculos
     * (#vt3dObstaculos).
     * @return
     */
    float fRetornaDistanciaObstaculos();

    /**
     * @brief Retorna em quantos obstáculos o robô colidiu.
     * @details Uma colisão é considerada quando a distância entre o robô e um
     * obstáculo é menor do que #globalConfig.robotDiameter, com um acréscimo de 5%.
     * @return
     */
    int iDetectaColisao();

    /**
     * @brief Retorna o tempo computacional para calcular um trajeto.
     *
     * @return Tempo em ms.
     */
    double computationalTime() const;

    /**
     * @brief Retorna o tempo computacional para atualizar o mapa.
     *
     * @return Tempo em ms.
     */
    double mapUpdateTime() const;

protected:
    /**
     * @brief Calcula o comprimento do trajeto fornecido.
     * @param _caminho - Trajeto.
     * @return float - Comprimento do trajeto.
     */
    float fCalculaComprimentoCaminho(const QVector<QVector2D>& _caminho);

    /**
     * @brief Calcula o ângulo de direção de todos os pontos do trajeto
     *
     * @param _path
     * @param _angle
     */
    void calculatePathAngle(const QVector<QVector2D>& _path,
                            QVector<float>& _angle);

    /**
     * @brief Calcula o perfil de velocidades para o trajeto a ser seguido
     *
     * @param _profile
     * @param _size
     */
    void generateSpeedProfile(QVector<float>& _profile, int _size,
                              float _maxVelocity);

    /**
     * @brief Interpola uma curva de bezier no trajeto encontrado para suavizar
     * as curvas.
     *
     * @param caminhoFinal - Caminho extraído do algoritmo de path-planning.
     * @param _vt2dCaminhoSemFiltro - Trajeto extraído da #cllClosedList
     * @return QVector<QVector2D> - Trajeto final suavizado.
     */
    QVector<QVector2D> vt2dTrataCaminhoFinal(
        const QVector<QVector2D>& caminhoFinal,
        QVector<QVector2D>& _vt2dCaminhoSemFiltro);

    /**
     * @brief Simplifica o trajeto fornecido, i.e. remove os pontos que podem
     * ser removidos sem causar colisões.
     * @param acAmbiente - Ambiente de campo.
     * @param iRoboID - ID do robô que o trajeto está sendo calculado.
     * @param _vt2dCaminho - Trajeto a ser simplificado.
     * @param distSeguranca - Distância de segurança considerada para fazer a
     * simplificação.
     * @return QVector<QVector2D> - Retorna o trajeto simplificado.
     */
    QVector<QVector2D> vt2dSimplificaCaminho(const AmbienteCampo* acAmbiente,
                                             const int iRoboID,
                                             QVector<QVector2D>& _vt2dCaminho,
                                             const int distSeguranca);

    /**
     * @brief Insere pontos no meio do trajeto caso a distância seja maior que
     * um certo limite.
     * @param _caminho - Trajeto sujeito à inserção de pontos.
     * @param comprimentoInicial - Comprimento do primeiro ao último ponto antes
     * da inserção (retornado por referência).
     * @param distanciaLimite - Distância mínima para inserção de pontos.
     * @return QVector<QVector2D> - Trajeto com os pontos inseridos.
     */
    QVector<QVector2D> vt2dInserePontosCaminho(
        const QVector<QVector2D>& _caminho, float& comprimentoInicial,
        int distanciaLimite = 1000);

    /**
     * @brief Insere pontos igualmente espaçados (250mm) entre o ponto inicial e
     * final, retorna o vetor obtido por referência.
     *
     * @param vt2dPontoInicial - Ponto incial.
     * @param vt2dPontoFinal - Ponto final.
     * @param vt2dSaidaPontos - Vetor com os pontos inseridos.
     */
    void vInserePontos(const QVector2D vt2dPontoInicial,
                       const QVector2D vt2dPontoFinal,
                       QVector<QVector2D>& vt2dSaidaPontos);

    /**
     * @brief Checa o trajeto anterior e determina se ele precisa ser
     * recalculado.
     *
     * @param vt2dCaminho - Trajeto anterior.
     * @return bool - True se o trajeto precisa ser recalculado.
     */
    bool bChecaCaminhoAnterior(const QVector<QVector2D>& vt2dCaminho);

    /**
     * @brief Calcula a distância entre os obstáculos (#vt3dObstaculos) e o
     * trajeto fornecido.
     * @param caminho - Trajeto calculado.
     */
    void vCalculaDistanciaObstaculos(const QVector<QVector2D>& caminho);

    void startTimer();
    void registerMapTime();
    void stopTimer();

    QElapsedTimer pathfindingTimer;
    double dTempoCalculoCaminho; /**< Armazena o tempo de cálculo do último
                                    trajeto encontrado. */
    double mapTime; /**< Armazena o tempo atualizar o mapa para cálculo do
                       último trajeto. */
    float fComprimentoCaminho;    /**< Armazena o comprimento do último trajeto
                                     encontrado.  */
    float fDistanciaObstaculos;   /**< Armazena a distância do trajeto para os
                                     obstáculos considerados. */
    QVector2D vt2dDestino;        /**< Destino atual. */
    QVector2D vt2dPosicaoInicial; /**< Posição inicial atual. */
    QVector<QVector3D> vt3dObstaculos; /**< Lista de obstáculos considerados
                                          durante o cálculo do trajeto. */
    QVector<float> pathAngle;
    QVector<float> speedProfile;
};

#endif // PATHPLANNINGGENERIC_H
