/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "priorityqueue.h"

// PriorityQueue::PriorityQueue()
// {
//     this->queue.clear();
// }
//
// void PriorityQueue::push(const Celula cel)
// {
//     int n = this->binarySearch(cel);
//
//     if(this->queue.size() > 0)
//     {
//         this->queue.insert(this->getIterator(n), cel);
//     }
//     else
//         this->queue.push_back(cel);
// }

// Celula PriorityQueue::top()
// {
//     return *this->queue.begin();
// }

// void PriorityQueue::pop()
// {
//     this->queue.pop_front();
// }

// bool PriorityQueue::empty()
// {
//     return this->queue.empty();
// }

// int PriorityQueue::size()
// {
//     return this->queue.size();
// }

// void PriorityQueue::clear()
// {
//     this->queue.clear();
// }

// int PriorityQueue::binarySearch(const Celula cel)
// {
//     int l = 0;
//     int r = this->queue.size();
//     int m = 0;
//     while (l < r)
//     {
//         m = qFloor((l + r) / 2.f);
//         if( (this->getIterator(m))->f < cel.f)
//             l = m + 1;
//         else
//             r = m;
//     }
//     return l;
// }

// std::_List_iterator<Celula> PriorityQueue::getIterator(int n)
// {
//     std::_List_iterator<Celula> it;
//     qint8 dir = 1;
//     if(static_cast<long unsigned int>(n) > this->queue.size()/2)
//     {
//         it = this->queue.end();
//         n = this->queue.size() - n;
//         dir = -1;
//     }
//     else
//         it = this->queue.begin();
//
//     for( ; n > 0; --n)
//     {
//         if(dir == 1)
//             it++;
//         else
//             it--;
//     }
//     return it;
// }
