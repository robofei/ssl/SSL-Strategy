/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file priorityqueue.h
 * @brief Arquivo header da classe que implementa uma fila de prioridade.
 * @author Leonardo da Silva Costa
 * @version 1.0
 * @date 2020-08-27
 */
#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <QtMath>
#include <QLinkedList>
#include <QVector2D>
#include "Path_Planning/cell.h"

/**
 * @brief Struct que representa uma célula/vértice avaliado pelo path-planner.
 *
 */
typedef struct Celula
{
public:
    /**
     * @brief Construtor da struct
     */
    Celula()
    {
        this->cllPai = nullptr;
    }

    /**
     * @brief Construtor com inicialização dos parâmetros da struct
     *
     * @param pos - Posição da célula no campo [mm]
     * @param indice - Índice da célula no VisibilityGraph
     * @param pai - Célula pai deste célula
     * @param g - Valor do custo da célula
     * @param h - Valor da heurística da célula
     * @param open - Indica se a célula está na open list
     * @param closed - Indica se a célula está na closed list
     */
    Celula(QVector2D pos, int indice, Celula *pai, float g, float h, bool open, bool closed)
    {
        this->setValues(pos, indice, pai, g, h, open, closed);
    }

    /**
     * @brief Construtor por cópia
     *
     * @param cel - Objeto a ser utilizado para a cópia
     */
    Celula(const Celula &cel)
    {
        this->vt2dPosicao = cel.vt2dPosicao;
        this->iIndice     = cel.iIndice;
        this->cllPai      = cel.cllPai;
        this->cost           = cel.cost;
        this->heuristic           = cel.heuristic;
        this->f           = cel.f;
        this->isOpen      = cel.isOpen;
        this->isClosed    = cel.isClosed;
    }

    /**
     * @brief Atualiza os atributos da célula.
     * @param pos - Nova posicao atual.
     * @param indice -  Novo índice.
     * @param pai - Novo ponteiro para a célula pai.
     * @param g - Novo valor do custo.
     * @param h - Novo valor da heurística.
     * @param open - Novo flag da open list.
     * @param closed - Novo flag da #cllClosedList
    */
    void setValues(const QVector2D &pos, const int &indice, Celula *pai, const float &g,
                   const float &h, const bool &open, const bool &closed)
    {
        this->vt2dPosicao = pos;
        this->iIndice = indice;
        this->cllPai = pai;
        this->cost = g;
        this->heuristic = h;
        this->f = g + h;
        this->isOpen = open;
        this->isClosed = closed;
    }

    /**
     * @brief Operador de comparação entre as células
     *
     * @param c1
     *
     * @return
     */
    bool operator==(const Celula &c1)
    {
        return c1.iIndice == this->iIndice;
    }

    /**
     * @brief Operador de atribuição entre as células
     *
     * @param cel
     */
    void operator=(const Celula &cel)
    {
        this->vt2dPosicao = cel.vt2dPosicao;
        this->iIndice     = cel.iIndice;
        this->cllPai      = cel.cllPai;
        this->cost           = cel.cost;
        this->heuristic           = cel.heuristic;
        this->f           = cel.f;
        this->isOpen      = cel.isOpen;
        this->isClosed    = cel.isClosed;
    }

    QVector2D vt2dPosicao; /**< Posição no campo desta célula. */
    int iIndice; /**< Índica desta célula no grafo. */
    Celula *cllPai; /**< Ponteiro para a célula pai. */

    float cost; /**< Valor do custo. */
    float heuristic; /**< Valor da heurística. */
    float f; /**< Valor de avaliação. */
    bool isOpen; /**< Indica se está na open list. */
    bool isClosed; /**< Indica se está na #cllClosedList  */

    float h()
    {
        return heuristic;
    }

    float g()
    {
        return cost;
    }

}Celula;

/**
 * @brief Classe que implementa uma priority queue
 * @details Alguns testes foram feitos e esta implementação ficou mais rápida
 * do que o container da STL
 * [std::priority_queue](http://www.cplusplus.com/reference/queue/priority_queue/).
 *
 * Esta priority queue é utilizada na open list do A_StarVG
 */
template<class CELL_T>
class PriorityQueue
{
public:
    /**
     * @brief Construtor da classe
     */
    PriorityQueue()
    {
        queue.clear();
    }

    /**
     * @brief Adiciona um elemento na fila
     *
     * @param cel - Elemento
     */
    void push(CELL_T _cell)
    {
        int n = this->binarySearch(_cell);

        if(this->queue.size() > 0)
        {
            // insert before n -> _cell.f() < queue.at(n).f()
            this->queue.insert(this->getIterator(n), _cell);
        }
        else
        {
            this->queue.push_back(_cell);
        }
    }

    void replace(CELL_T _cell)
    {
        int n = binarySearch(_cell);
        auto it = getIterator(n);
        while(*it != _cell)
        {
            it++;
        }
        queue.erase(it);
        push(_cell);
    }

    /**
     * @brief Retorna o topo da fila
     *
     * @return
     */
    CELL_T top()
    {
        return *this->queue.begin();
    }

    /**
     * @brief Remove o topo da fila
     */
    void pop()
    {
        this->queue.pop_front();
    }

    /**
     * @brief Retorna se a fila está vazia
     *
     * @return
     */
    bool empty()
    {
        return this->queue.empty();
    }

    /**
     * @brief Retorna o tamanho da fila
     *
     * @return
     */
    int size()
    {
        return this->queue.size();
    }

    /**
     * @brief Limpa a fila
     */
    void clear()
    {
        this->queue.clear();
    }

private:
    /**
     * @brief Executa uma busca binária procurando pela célula fornecida
     * @details Esse método é utlizado para encontrar a posição certa de inserção
     * de um novo elemento na fila. @see #push
     *
     * @param cel - Célula a ser procurada
     *
     * @return int - Índice da célula caso ela seja encontrada. Se ela não for
     * encontrada, retorna 0
     */
    int binarySearch(Cell *_cell)
    {
        int l = 0;
        int r = queue.size();
        int m = 0;
        while (l < r)
        {
            m = qFloor((l + r) / 2.f);
            if((*getIterator(m))->f < (_cell)->f)
            {
                l = m + 1;
            }
            else
            {
                r = m;
            }
        }
        return l;
    }

    int binarySearch(Celula _cell)
    {
        int l = 0;
        int r = queue.size();
        int m = 0;
        while (l < r)
        {
            m = qFloor((l + r) / 2.f);
            if((*getIterator(m)).f < (_cell).f)
            {
                l = m + 1;
            }
            else
            {
                r = m;
            }
        }
        return l;
    }

    /**
     * @brief Retorna um iterador para o índice desejado da fila
     *
     * @param n - Índice
     * @return std::_List_iterator - Iterador para **n**
     */
    std::_List_iterator<CELL_T> getIterator(int n)
    {
        std::_List_iterator<CELL_T> it;
        qint8 dir = 1;
        if(static_cast<long unsigned int>(n) > this->queue.size()/2)
        {
            it = this->queue.end();
            n = this->queue.size() - n;
            dir = -1;
        }
        else
        {
            it = this->queue.begin();
        }

        for( ; n > 0; --n)
        {
            if(dir == 1)
            {
                it++;
            }
            else
            {
                it--;
            }
        }
        return it;
    }

    CELL_T* ptr(CELL_T &_obj)
    {
        //turn reference into pointer!
        return &_obj;
    }

    CELL_T* ptr(CELL_T *_obj)
    {
        return _obj;
    }

    std::list<CELL_T> queue; /**< Fila.*/
};

#endif // PRIORITYQUEUE_H
