#include "passpositionoptimizer.h"

PassPositionOptimizer::PassPositionOptimizer(const AmbienteCampo* _fieldEnv,
                                             QObject* parent)
    : Positioning{_fieldEnv, parent}
{
    minXSolution = 0;
}
PassPositionOptimizer::~PassPositionOptimizer()
{
}

void PassPositionOptimizer::setZone(const FieldZone& _zone)
{
    if (_zone.ztZoneType() != ZONE_TYPE::RECTANGLE)
    {
        qFatal("[Pass Position Optimizer] Zone type must be a "
               "ZONE_TYPE::RECTANGLE!");
    }
    zone.vConfigZone(_zone);
    QRect zoneRect = zone.rctGetZone();
    minXSolution = zoneRect.topLeft().x();

    bounds.x.min = zoneRect.topLeft().x();
    bounds.x.max = zoneRect.topRight().x();

    bounds.y.min = zoneRect.topLeft().y();
    bounds.y.max = zoneRect.bottomLeft().y();

    bounds.row.max = (bounds.y.max - bounds.y.min) / step + 1;
    bounds.col.max = (bounds.x.max - bounds.x.min) / step + 1;

    fieldCost.resize(qAbs(zoneRect.height()) / step,
                     qAbs(zoneRect.width()) / step);
}

int PassPositionOptimizer::rowToY(int _row)
{
    return _row * step + bounds.y.min;
}

int PassPositionOptimizer::colToX(int _col)
{
    return _col * step + bounds.x.min;
}

int PassPositionOptimizer::yToRow(int _y)
{
    return qMin(qMax((_y - bounds.y.min) / step, 0), bounds.row.max);
}

int PassPositionOptimizer::xToCol(int _x)
{
    return qMin(qMax((_x - bounds.x.min) / step, 0), bounds.col.max);
}

void PassPositionOptimizer::fillAround(Eigen::Matrix<int, 2, 1>& _pos,
                                       int _range, int _cost)
{
    int row = yToRow(_pos(Y)), col = xToCol(_pos(X));

    for (int y = qMax(row - _range, 0);
         y <= qMin(row + _range, bounds.row.max - 1); y++)
    {
        for (int x = qMax(col - _range, 0);
             x <= qMin(col + _range, bounds.col.max - 1); x++)
        {
            fieldCost(y, x) += _cost;
        }
    }
}

void PassPositionOptimizer::fillShootDirection(
    const Eigen::Matrix<float, 2, 1>& _dir, Eigen::Matrix<int, 2, 1> _pos,
    int _cost)
{
    Eigen::Matrix<float, 2, 1> Pshoot = _pos.cast<float>();
    Eigen::Matrix<int, 2, 1> shootI;

    while (yToRow(Pshoot(Y)) >= 0 && yToRow(Pshoot(Y)) <= bounds.row.max - 1 &&
           xToCol(Pshoot(X)) >= 0 && xToCol(Pshoot(X)) <= bounds.col.max - 1)
    {
        // fieldCost(yToRow(Pshoot(Y)), xToCol(Pshoot(X))) += _cost;
        shootI = Pshoot.cast<int>();
        fillAround(shootI, 1, _cost);
        Pshoot = (Pshoot + _dir);
    }
}

void PassPositionOptimizer::vUpdate()
{
    if (iAvailableRobots.isEmpty())
    {
        qWarning() << QString(
            "[Pass Position Optimizer] Nenhum robô foi atribuído!");
        return;
    }
    if (env == nullptr)
        return;
}
