#ifndef PASSPOSITIONOPTIMIZER_H
#define PASSPOSITIONOPTIMIZER_H

#include <QDebug>
#include <QTimer>
#include <QVector>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Posicionamento/positioning.h"
#include <Eigen/Dense>
#include <Eigen/src/Core/ArithmeticSequence.h>
#include <Eigen/src/Core/Matrix.h>

class PassPositionOptimizer : public Positioning
{
    Q_OBJECT
public:
    explicit PassPositionOptimizer(const AmbienteCampo* _fieldEnv,
                                   QObject* parent = nullptr);
    ~PassPositionOptimizer();

    void setZone(const FieldZone& _zone);

private:
    int step;
    int minXSolution;
    struct Bounds
    {
        struct
        {
            int min;
            int max;
        } x, y;
        struct
        {
            int max;
        } row, col; // row.max -> yStep, col.max -> xStep
    } bounds;
    Eigen::MatrixXf fieldCost;

    static constexpr int X = 0;
    static constexpr int Y = 1;

    int rowToY(int _row);
    int colToX(int _col);
    int yToRow(int _y);
    int xToCol(int _x);
    void fillAround(Eigen::Matrix<int, 2, 1>& _pos, int _range, int _cost);
    void fillShootDirection(const Eigen::Matrix<float, 2, 1>& _dir,
                            Eigen::Matrix<int, 2, 1> _pos, int _cost);

protected:
    void vUpdate() override;
};

#endif // PASSPOSITIONOPTIMIZER_H
