#include "deltadefense.h"

DeltaDefense::DeltaDefense(const AmbienteCampo *_fieldEnv, QObject *parent) :
    Positioning(_fieldEnv, parent)

{
    const int allySide = _fieldEnv->allies->iGetSide();

    /// \todo Transformar essa zona em um retângulo que cobre um pouco mais do retângulo
    ///  da área de defesa
    zone.vConfigZone(_fieldEnv->geoCampo->vt2dGoalCenter(allySide).toPoint(), 2e3);
//    zone.vConfigZone(QRect())
}

DeltaDefense::~DeltaDefense()
{

}

void DeltaDefense::vUpdate()
{
    if(iAvailableRobots.isEmpty())
    {
        qWarning() << QString("[Delta Defense] Nenhum robô foi atribuído!");
        return;
    }
    if(env == nullptr)
        return;

    const QVector<QVector2D> oppPos = env->vt2dPegaPosicaoTodosObjetos(otOponente, false);
    const QVector2D ballPos = env->vt2dPosicaoBola();
    const int defenseWidth = env->geoCampo->szDefenseArea().height() + 2*globalConfig.distanceToDefenseArea;
    const int defenseDepth = env->geoCampo->szDefenseArea().width() + globalConfig.distanceToDefenseArea;
    const QVector2D allyGoal = env->geoCampo->vt2dGoalCenter(env->allies->iGetSide());
    const float maxDistance = env->geoCampo->szField().width()/2;

    QVector<QVector2D> defensePos =
            Auxiliar::vt_vt2dPosicoesDestinoDeltaInteligente(iAvailableRobots.size(),
                                                             oppPos, ballPos,
                                                             defenseWidth,
                                                             defenseDepth,
                                                             allyGoal, maxDistance);

    vt2dPositions = env->mapGetBestGoalAssignment(iAvailableRobots,
                                                  defensePos);
}

