#ifndef DELTADEFENSE_H
#define DELTADEFENSE_H

#include <QObject>
#include <QVector>
#include <QTimer>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Posicionamento/positioning.h"

class DeltaDefense : public Positioning
{
    Q_OBJECT

public:
    explicit DeltaDefense(const AmbienteCampo *_fieldEnv, QObject *parent = nullptr);
    ~DeltaDefense();

protected:
    void vUpdate() override;
};

#endif // DELTADEFENSE_H
