#ifndef INDIVIDUALDEFENSE_H
#define INDIVIDUALDEFENSE_H

#include <QObject>
#include <QVector>
#include <QTimer>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Posicionamento/positioning.h"

class IndividualDefense : public QObject
{
    Q_OBJECT
public:
    explicit IndividualDefense(const AmbienteCampo* _fieldEnv);
    ~IndividualDefense();

    const FieldZone& getZone() const;

protected:
    ///
    /// \brief Esse slot é chamada continuamente para atualizar a posição dos robôs.
    /// Este método deve ser reimplementado na classe filha.
    ///
    void vUpdate();

    QVector<qint8> iAvailableRobots; /**< IDs dos robôs  */
    QMap<qint8, QVector2D> vt2dPositions /**< Posição destino dos robôs */;
    QMap<qint8, QVector2D> vt2dPositionsopp;
    const AmbienteCampo* env;
    FieldZone zone /**< Zona de campo permitida para o posicionamento */;

private:
    QScopedPointer<QTimer> tmrUpdate;
    QVector<QVector2D> vt_vt2dPositionsIndividualMarking(const AmbienteCampo* env, QVector2D centroGolAliado, const float maxDistance);

public slots:
    void vReceivePositionRequest_(const qint8 _id, QVector2D &_pos);
    void vAddRobot(const qint8 _id);
};

#endif // INDIVIDUALDEFENSE_H
