#include "pressuredefense.h"

PressureDefense::PressureDefense(const AmbienteCampo *_fieldEnv)
{
    const int allySide = _fieldEnv->allies->iGetSide();
    zone.vConfigZone(_fieldEnv->geoCampo->vt2dGoalCenter(allySide).toPoint(), 2e3);

    env = _fieldEnv;
    tmrUpdate.reset(new QTimer());
    tmrUpdate->setTimerType(Qt::PreciseTimer);

    connect(tmrUpdate.get(), &QTimer::timeout,
            this, &PressureDefense::vUpdate);
}

PressureDefense::~PressureDefense()
{
    if(tmrUpdate->isActive())
    {
        tmrUpdate->stop();
    }
}

const FieldZone& PressureDefense::getZone() const
{
    return zone;
}

void PressureDefense::vUpdate()
{
    if(iAvailableRobots.isEmpty())
    {
        qWarning() << QString("[Individual Defense] Nenhum robô foi atribuído!");
        return;
    }
    if(env == nullptr)
        return;

    const QVector2D allyGoal = env->geoCampo->vt2dGoalCenter(env->allies->iGetSide());
    const float maxDistance = env->geoCampo->szField().width();

    zone.vConfigZone(env->vt2dPosicaoBola().toPoint(), 2e3);

    QVector<QVector2D> defensePos = vt_vt2dPositionsIndividualMarking(env,
                                                                      allyGoal,
                                                                      maxDistance);

    vt2dPositions = env->mapGetBestGoalAssignment(iAvailableRobots,
                                                  defensePos);
}

void PressureDefense::vReceivePositionRequest_(const qint8 _id, QVector2D& _pos)
{
    if(vt2dPositions.contains(_id))
    {
        _pos = vt2dPositions.value(_id);
    }
}

void PressureDefense::vAddRobot(const qint8 _id)
{
    if(!iAvailableRobots.contains(_id))
    {
        iAvailableRobots.append(_id);
    }

    if(!tmrUpdate->isActive())
    {
        tmrUpdate->start(iTempoLoopEstrategia);
    }
}

QVector<QVector2D> PressureDefense::vt_vt2dPositionsIndividualMarking(const AmbienteCampo* env, QVector2D centroGolAliado, const float maxDistance)
{
    QVector<QVector2D> listPositions;
    QVector<QVector2D> defensePos;
    const int opponentDistance = 350;

    // Compute the marking positions for each opponent robot in the field
    for(int i=0; i < globalConfig.robotsPerTeam; ++i)
    {
        if(env->opponents->getPlayer(i)->bRoboDetectado() == true)
        {
            QVector2D opponentPos = env->opponents->getPlayer(i)->vt2dPosicaoAtual();
            QVector2D opponentFrontPos = env->opponents->getPlayer(i)->vt2dPontoDirecaoMira();
            QVector2D markingPos = Auxiliar::vt2dReajustaPonto(opponentPos, opponentFrontPos, opponentDistance);
            listPositions.append(markingPos);
        }
    }

    // Ordena o vetor posicaoAdversarios, colocando as posições mais próximas ao gol aliado nas
    //  primeiras posições
    for(int i=0; i<listPositions.size(); ++i)
    {
        for(int j=i+1; j<listPositions.size(); ++j)
        {
            if(listPositions.at(i).distanceToPoint(centroGolAliado) > listPositions.at(j).distanceToPoint(centroGolAliado))
            {

#if QT_VERSION > QT_VERSION_CHECK(5, 14, 0)
                listPositions.swapItemsAt(i, j);
#else
                std::swap(posicaoAdversarios[i], posicaoAdversarios[j]);
#endif
            }
        }
    }

    for(int i=0; i<listPositions.size(); ++i)
    {
        if(env->bPontoDentroAreaDefesaOponente(listPositions.at(i), 1) == true)
        {
            listPositions.removeAt(i);
        }
    }
    // Remove os adersários que estão muito distantes do gol (considerando o parâmetro `distanciaMaximaConsiderada`)

    while(!listPositions.isEmpty() && listPositions.last().distanceToPoint(centroGolAliado) > maxDistance)
    {
        listPositions.removeLast();
    }

    qint8 robotsRemaining = iAvailableRobots.size() - listPositions.size();

    const QVector<QVector2D> oppPos = env->vt2dPegaPosicaoTodosObjetos(otOponente, false);
    const QVector2D ballPos = env->vt2dPosicaoBola();
    const int defenseWidth = env->geoCampo->szDefenseArea().height() + 2*globalConfig.distanceToDefenseArea;
    const int defenseDepth = env->geoCampo->szDefenseArea().width() + globalConfig.distanceToDefenseArea*3;
    const QVector2D allyGoal = env->geoCampo->vt2dGoalCenter(env->allies->iGetSide());

    if(robotsRemaining > 0)
    {

        defensePos =
                Auxiliar::vt_vt2dPosicoesDestinoDeltaInteligente(robotsRemaining,
                                                                 oppPos, ballPos,
                                                                 defenseWidth,
                                                                 defenseDepth,
                                                                 allyGoal, maxDistance);
        for(int i=0; i<defensePos.size(); ++i)
        {
            listPositions.append(defensePos.at(i));
        }
    }
    else if(robotsRemaining <= 0)
    {
        defensePos = Auxiliar::vt_vt2dPosicoesDestinoDeltaInteligente(1,
                                                                      oppPos, ballPos,
                                                                      defenseWidth,
                                                                      defenseDepth,
                                                                      allyGoal, maxDistance);
        listPositions.removeLast();
        listPositions.prepend(defensePos.at(0));
    }

    return listPositions;
}
