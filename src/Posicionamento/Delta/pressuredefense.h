#ifndef PRESSUREDEFENSE_H
#define PRESSUREDEFENSE_H

#include <QObject>
#include <QVector>
#include <QTimer>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Posicionamento/positioning.h"

class PressureDefense : public QObject
{
    Q_OBJECT
public:
    explicit PressureDefense(const AmbienteCampo* _fieldEnv);
    ~PressureDefense();

    const FieldZone& getZone() const;

protected:
    void vUpdate();

    QVector<qint8> iAvailableRobots; /**< IDs dos robôs  */
    QMap<qint8, QVector2D> vt2dPositions /**< Posição destino dos robôs */;
    QMap<qint8, QVector2D> vt2dPositionsopp;
    const AmbienteCampo* env;
    FieldZone zone /**< Zona de campo permitida para o posicionamento */;

private:
    QScopedPointer<QTimer> tmrUpdate;
    QVector<QVector2D> vt_vt2dPositionsIndividualMarking(const AmbienteCampo* env, QVector2D centroGolAliado, const float maxDistance);

public slots:
    void vReceivePositionRequest_(const qint8 _id, QVector2D &_pos);
    void vAddRobot(const qint8 _id);
};

#endif // PRESSUREDEFENSE_H
