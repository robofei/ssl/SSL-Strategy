#include "marking.h"

Marking::Marking(const AmbienteCampo *_fieldEnv, QObject *parent) :
    Positioning(_fieldEnv, parent)

{

}

Marking::~Marking()
{

}

void Marking::vSetMarkingRegion(MarkingRegion type)
{
    double beginY = -env->geoCampo->szField().height()/2;
    double endY  =  env->geoCampo->szField().height()/2;

    double beginXgen = 0;
    double endXgen  = 0;

    switch(type)
    {
    case Marking::MARKING_DEFENSE:
        beginXgen = -2;

        endXgen  = -.5;
        break;
    case Marking::MARKING_MIDFIELD:
        beginXgen = -.5;
        endXgen  = .5;
        break;
    case Marking::MARKING_ATTACK:
        beginXgen = .5;
        endXgen  = 2;
        break;
    }

    double beginX = Auxiliar::vt2dCoordenadasGenericasParaCoordenadasCampo(
                             QVector2D(beginXgen, 0), env->allies->iGetSide(),
                             env->geoCampo->szField(),
                             env->geoCampo->szDefenseArea()).x();

    double endX = Auxiliar::vt2dCoordenadasGenericasParaCoordenadasCampo(
                            QVector2D(endXgen, 0), env->allies->iGetSide(),
                            env->geoCampo->szField(),
                            env->geoCampo->szDefenseArea()).x();

    if(beginX > endX)
        std::swap(beginX, endX);

    zone.vConfigZone(QRect(
        QPoint(beginX, beginY),
        QPoint(endX, endY)
        ));
}

void Marking::vUpdate()
{
    const QVector2D allyGoal = env->geoCampo->vt2dGoalCenter(env->allies->iGetSide());

    if(iAvailableRobots.isEmpty())
    {
        qWarning() << QString("[Delta Defense] Nenhum robô foi atribuído!");
        return;
    }
    if(env == nullptr)
        return;

    QVector<QVector2D> oppPos = env->vt2dPegaPosicaoTodosObjetos(otOponente, false);


//    std::sort(oppPos.first(), oppPos.last(),
//          [allyGoal](const QVector2D& a, const QVector2D& b)
//          { return a.distanceToPoint(allyGoal) < b.distanceToPoint(allyGoal); }
//          );

    // std::sort acabou não dando, então fiz 'manualmente' mesmo
    for (int i=0; i<oppPos.size(); ++i)
    {
        for (int j=i; j<oppPos.size(); ++j)
        {
            if (oppPos[i].distanceToPoint(allyGoal) > oppPos[j].distanceToPoint(allyGoal))
            {
                std::swap(oppPos[i], oppPos[j]);
            }
        }
    }

    for(auto it = oppPos.begin(); it != oppPos.end(); )
    {
        if(zone.bInsideZone(*it))
        {
            ++it;
        }
        else
        {
            it = oppPos.erase(it);
        }

    }

    QVector<QVector2D> defensePos;
    defensePos.reserve(iAvailableRobots.size());

    QVector<QVector2D> posicoesAdicionais =
    {
        QVector2D(zone.rctGetZone().center()),
        QVector2D(zone.rctGetZone().bottomLeft()),
        QVector2D(zone.rctGetZone().bottomRight()),
        QVector2D(zone.rctGetZone().topLeft()),
        QVector2D(zone.rctGetZone().topRight())
    };

    for(qint8 i=0; i<iAvailableRobots.size(); ++i)
    {
        // Se não tiver mais ninguém para marcar, pega uma das posições genéricas
        if(oppPos.isEmpty())
        {
            defensePos.append(posicoesAdicionais.first());
            posicoesAdicionais.removeFirst();
        }
        else
        {
            QVector2D pos = oppPos.first();
            oppPos.removeFirst();

            const float distAdversario = 300;

            // Ponto onde o robô recebe a bola
            pos += globalConfig.robotDistanceToBall*(env->vt2dPosicaoBola() - pos).normalized();
            pos += distAdversario*(allyGoal - pos).normalized();

            // Outra alternativa seria fazer o robô ficar no caminho entre a bola e o
            //  adversário. Dessa forma, ele já ficaria posicionado para interceptar o
            //  passe.

            defensePos.append(pos);
        }
    }

    vt2dPositions = env->mapGetBestGoalAssignment(iAvailableRobots,
                                                  defensePos);
}

