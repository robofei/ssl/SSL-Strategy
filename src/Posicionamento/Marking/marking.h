#ifndef MARKING_H
#define MARKING_H

#include "Ambiente/fieldzone.h"
#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Posicionamento/positioning.h"

class Marking : public Positioning
{
    Q_OBJECT

public:
    enum MarkingRegion
    {
        MARKING_DEFENSE,
        MARKING_MIDFIELD,
        MARKING_ATTACK
    };

    explicit Marking(const AmbienteCampo *_fieldEnv, QObject *parent = nullptr);
    ~Marking();

    void vSetMarkingRegion(MarkingRegion type);

protected:
    void vUpdate() override;
};

#endif // MARKING_H
