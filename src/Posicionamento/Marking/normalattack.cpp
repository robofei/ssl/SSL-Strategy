#include "normalattack.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include <QRandomGenerator>

NormalAttack::NormalAttack(AmbienteCampo* _fieldEnv)
{
    const int allySide = _fieldEnv->allies->iGetSide();

    env = _fieldEnv;
    tmrUpdate.reset(new QTimer());
    tmrUpdate->setTimerType(Qt::PreciseTimer);
    zone.vConfigZone(env->vt2dPosicaoBola().toPoint(), 2e3);
    
    connect(tmrUpdate.get(), &QTimer::timeout, this, &NormalAttack::vUpdate);
}

NormalAttack::~NormalAttack()
{
    if (tmrUpdate->isActive())
    {
        tmrUpdate->stop();
    }
}

const FieldZone& NormalAttack::getZone() const
{
    return zone;
}

void NormalAttack::vUpdate()
{
    zone.vConfigZone(env->vt2dPosicaoBola().toPoint(), 2e3);
    if (iAvailableRobots.isEmpty())
    {
        qWarning() << QString("[Normal Attack] Nenhum robô foi atribuído!");
        return;
    }
    if (env == nullptr)
        return;

    //    const QVector2D allyGoal =
    //    env->geoCampo->vt2dGoalCenter(env->allies->iGetSide()); const float
    //    maxDistance = env->geoCampo->szField().width()/2;

    QVector<QVector2D> attackPos = vt_vt2dPositionsAttackers(env);

    for (qint8 i = 0; i < iAvailableRobots.size(); ++i)
    {
        if (vt2dPositions.value(iAvailableRobots.at(i)).length() == 0)
        {
            qDebug() << "fudeu de vez";
        }
    }

    vt2dPositions = env->mapGetBestGoalAssignment(iAvailableRobots, attackPos);
}

void NormalAttack::vReceivePositionRequest_(const qint8 _id, QVector2D& _pos)
{
    if (vt2dPositions.contains(_id))
    {
        _pos = vt2dPositions.value(_id);
        if (_pos.length() == 0)
        {
            qDebug() << "fudeu";
        }
    }
}

void NormalAttack::vAddRobot(const qint8 _id)
{
    if (!iAvailableRobots.contains(_id))
    {
        iAvailableRobots.append(_id);
        vt2dPositions.insert(_id, env->vt2dAllyPosition(_id));
    }

    if (!tmrUpdate->isActive())
    {
        tmrUpdate->start(iTempoLoopEstrategia);
    }
}

void NormalAttack::vRemoveRobot(const qint8 _id)
{
    if (iAvailableRobots.contains(_id))
    {
        iAvailableRobots.removeOne(_id);
    }
}

QVector<QVector2D> NormalAttack::vt_vt2dPositionsAttackers(
    const AmbienteCampo* env)
{
    QVector<QVector2D> listPositions;
    QVector<QVector2D> listPositions_b;
    QVector<double> listAngles_a;
    QVector<double> listAngles_b;
    double angle;
    QVector2D bestPoint;

    for (int i = 0; i < iAvailableRobots.size(); ++i)
    {
        bestPoint = vt2dFindBestPosition(angle);

        for (int j = 0; j < listPositions.size(); ++j)
        {
            if (bestPoint.distanceToPoint(listPositions.at(j)) < 200 && i != j)
            {
                bestPoint = vt2dFindBestPosition(angle);
            }
        }
        listPositions.append(bestPoint);
        listAngles_b.append(angle);
    }

    for (int i = 0; i < iAvailableRobots.size(); ++i)
    {
        double old_angle = Auxiliar::dAnguloEmGrausLivreParaChute(
            vt2dPositions.value(iAvailableRobots.at(i)),
            env->allies->iGetSide(), env->geoCampo->szGoal().height(),
            env->geoCampo->szField(),
            env->vt3dPegaPosicaoTodosObjetos(otOponente, true));
        listAngles_a.append(old_angle);
    }

    // QSharedPointer<DebugBestPositionAttack> debugBestPos;
    // debugBestPos.reset(new DebugBestPositionAttack);
    // debugBestPos->addPoints(listPositions);
    // env->debugInfo->addDebugData(debugBestPos, 1);

    for (int i = 0; i < iAvailableRobots.size(); ++i)
    {
        double max_value =
            *std::max_element(listAngles_b.begin(), listAngles_b.end());
        int max_index = listAngles_b.indexOf(qMax(0.0, max_value));

        if (max_value - listAngles_a.at(i) >= 10)
        {
            listPositions_b.append(listPositions.at(max_index));
            listAngles_b.remove(max_index);
            listPositions.remove(max_index);
        }
        else
        {
            listPositions_b.append(vt2dPositions.value(iAvailableRobots.at(i)));
        }
    }

    return listPositions_b;
}

QVector2D NormalAttack::vt2dFindBestPosition(double& _angle)
{
    int lado_campo_ataque = env->opponents->iGetSide();
    QVector2D centerPos =
        env->geoCampo->vt2dGoalCenter(env->opponents->iGetSide());
    const int y_max = (env->geoCampo->szField().height()) / 2;
    const int y_min = -(env->geoCampo->szField().height()) / 2;
    const int x_max = (env->geoCampo->szField().width() / 2);
    const int x_min = (env->geoCampo->szField().width() / 2 - 3000);
    //    qDebug() << y_max << " " << y_min << " " << x_min << " " << x_max;
    QVector2D bestPoint(lado_campo_ataque * x_min, 1000);
    double bestInterval = 0;
    double freeInterval;
    QVector<QVector3D> opponents = env->vt3dPegaPosicaoTodosObjetos(otOponente);
    QVector<QVector3D> allies = env->vt3dPegaPosicaoTodosObjetos(otAliado);

    int x_random;
    int y_random;

    QVector2D testPoint;

    for (int i = 0; i < 500; ++i)
    {
        x_random = QRandomGenerator::global()->bounded(x_min, x_max);
        y_random = QRandomGenerator::global()->bounded(y_min, y_max);
        testPoint = QVector2D(lado_campo_ataque * x_random, y_random);

        if (env->geoCampo->bIsInsideField(testPoint))
        {
            if (!env->geoCampo->bPontoDentroAreaRestricao(testPoint.toPoint(),
                                                          XNegativo) &&
                !env->geoCampo->bPontoDentroAreaRestricao(testPoint.toPoint(),
                                                          XPositivo))
            {
                QVector2D ballPos = env->vt2dPosicaoBola(),
                          goalCenter = env->geoCampo->vt2dGoalCenter(
                              env->opponents->iGetSide());

                if (Auxiliar::bChecaInterseccaoObjetosLinha(
                        ballPos, goalCenter, 1000, testPoint) == true)
                {
                    if (Auxiliar::bChecaInterseccaoObjetosLinha(
                            centerPos, testPoint, globalConfig.robotDiameter * 2,
                            opponents) == false)
                    {
                        freeInterval = Auxiliar::dAnguloEmGrausLivreParaChute(
                            testPoint, env->allies->iGetSide(),
                            env->geoCampo->szGoal().height(),
                            env->geoCampo->szField(),
                            env->vt3dPegaPosicaoTodosObjetos(otOponente));

                        if (freeInterval > bestInterval)
                        {
                            int conta_robos_perto = 0;
                            //                        QVector<qint8>
                            for (int k = 0; k < iAvailableRobots.size(); ++k)
                            {
                                if (testPoint.distanceToPoint(
                                        vt2dPositions.value(
                                            iAvailableRobots.at(k))) < 1000)
                                {
                                    conta_robos_perto = conta_robos_perto + 1;
                                }
                            }
                            if (conta_robos_perto == 0)
                            {
                                bestInterval = freeInterval;
                                bestPoint = testPoint;
                            }
                        }
                    }
                }
            }
        }
    }
    _angle = bestInterval;
    return bestPoint;
}

// QVector2D NormalAttack::vt2dFindBestPosition(QVector2D centerPos)
//{
//     const float angleResolution = 1; // graus
//     int lado_campo_ataque = env->opponents->iGetSide();
//     const float y_max = (env->geoCampo->szField().height())/2;
//     const float y_min = -(env->geoCampo->szField().height())/2;
//     const float x_max = lado_campo_ataque *
//     (env->geoCampo->szField().width()/2); const float x_min =
//     lado_campo_ataque * (env->geoCampo->szField().width()/2 - 3000);
//     QVector2D bestPoint(0, 0);
//     int bestInterval = 0;
//     QVector<QVector2D> freeInterval;
//     QVector<QVector3D> opponents =
//     env->vt3dPegaPosicaoTodosObjetos(otOponente); double posRadiius = 1000;

//    for(int n = 0; bestPoint.distanceToPoint(QVector2D(0, 0)) <= 0 &&
//    posRadiius < 2500; ++n)
//    {
//        // Aumenta em 10% a cada interação que nao encontrar um ponto
//        QVector2D testPoint = centerPos + QVector2D(posRadiius, 0);
//        posRadiius = posRadiius * pow(1.10, n);
//        for(int angle = 0; angle < 360; angle += angleResolution)
//        {
//            if(Auxiliar::bChecaInterseccaoObjetosLinha(centerPos, testPoint,
//                                                       globalConfig.robotDiameter*2,
//                                                       opponents) == false)
//            {
//                if(env->geoCampo->bIsInsideField(testPoint))
//                {
//                    if(!env->geoCampo->bPontoDentroAreaRestricao(testPoint.toPoint(),
//                    XNegativo) &&
//                            !env->geoCampo->bPontoDentroAreaRestricao(testPoint.toPoint(),
//                            XPositivo))
//                    {
//                        freeInterval =
//                        Auxiliar::vt2dIntervaloLivreGol(testPoint,
//                                env->allies->iGetSide(),
//                                env->geoCampo->szGoal().height(),
//                                env->geoCampo->szGoal().width(),
//                                env->geoCampo->szField(),
//                                opponents);

//                        if(freeInterval.size() > bestInterval)
//                        {
//                            bestInterval = freeInterval.size();
//                            bestPoint = testPoint;
//                        }
//                    }
//                }
//            }

//            testPoint = Auxiliar::vt2dRotaciona(centerPos, testPoint,
//            angleResolution);
//        }
//        testPoint = centerPos + QVector2D(iDistanciaReceptorCobranca/2.0, 0);
//    }

//    return bestPoint;
//}

// QVector<QVector2D> NormalAttack::vt_vt2dPositionsAttackers(const
// AmbienteCampo* env)
//{

//    QVector<QVector2D> listPositions;
//    QVector<QVector2D> attackPos;
//    QVector<QVector3D> allies = env->vt3dPegaPosicaoTodosObjetos(otAliado);
//    QVector2D bestPoint;
//    const int opponentSide = env->opponents->iGetSide();

//    for(int i=0; i < iAvailableRobots.size(); ++i)
//    {
//        bestPoint =
//        vt2dFindBestPosition(env->geoCampo->vt2dGoalCenter(opponentSide));

//        for(int j=0; j < listPositions.size(); ++j)
//        {
//            if(bestPoint.distanceToPoint(listPositions.at(j)) < 200 && i != j)
//             {
//                bestPoint = vt2dFindBestPosition(listPositions.at(j));
//                if(bestPoint.distanceToPoint(env->vt2dPosicaoBola()) >= 2500)
//                {
//                    bestPoint = vt2dFindBestPosition(listPositions.at(i-1));
//                }
//            }
//        }
//        listPositions.append(bestPoint);
//    }
//    return listPositions;
//}
