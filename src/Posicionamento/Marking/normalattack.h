#ifndef NORMALATTACK_H
#define NORMALATTACK_H

#include <QObject>
#include <QVector>
#include <QTimer>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Posicionamento/positioning.h"

#include "Debug/debugbestpositionattack.h"

class NormalAttack : public QObject
{
    Q_OBJECT
public:
    explicit NormalAttack(AmbienteCampo* _fieldEnv);
    ~NormalAttack();

    const FieldZone& getZone()const;
protected:
    void vUpdate();

    QVector<qint8> iAvailableRobots; /**< IDs dos robôs  */
    QMap<qint8, QVector2D> vt2dPositions /**< Posição destino dos robôs */;
    QMap<qint8, QVector2D> vt2dPositionsopp;
    QMap<qint8, double> vt2dAngles;
    const AmbienteCampo* env;
    FieldZone zone /**< Zona de campo permitida para o posicionamento */;
private:
    QScopedPointer<QTimer> tmrUpdate;
    QVector<QVector2D> vt_vt2dPositionsAttackers(const AmbienteCampo* env);
    QVector2D vt2dFindBestPosition(double &_angle);

public slots:
    void vReceivePositionRequest_(const qint8 _id, QVector2D &_pos);
    void vAddRobot(const qint8 _id);
    void vRemoveRobot(const qint8 _id);
};

#endif // NORMALATTACK_H
