/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MINES_H
#define MINES_H

///
/// \file mines.h
/// \brief \a Mines e \a RobotMines
///

#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Ambiente/futbolenvironment.h"
#include "Posicionamento/positioning.h"

///
/// \brief Este struct contém os parâmetros do robô utilizados na classe Mines
///
struct RobotMines {

    RobotMines() :
      id(-1),
      vt2dPosicaoAtualRobo(QVector2D(0 , 0)),
      vt2dPosicaoDestino(QVector2D(0 , 0)),
      bRoboEmCampo(false){}

    ~RobotMines() = default;

    qint8 id; ///< ID do robô
    QVector2D vt2dPosicaoAtualRobo; ///< Posição atual do Robô
    QVector2D vt2dPosicaoDestino;///< Última Posição gerada pelo algorítimo
    bool bRoboEmCampo; ///< bRoboEmCampo

};

///
/// \brief Usado na classe Mines
///
enum LIBERDADE : qint8
{
    L_negativo = -1,

    VERMELHO = 0,

    L_00 =  0,
    L_01 =  1,
    L_02 =  2,

    LARANJA = 3,

    L_03 =  3,
    L_04 =  4,
    L_05 =  5,

    AMARELO = 6,

    L_06 =  6,
    L_07 =  7,
    L_08 =  8,

    VERDE = 9,

    L_09 =  9
};

struct ParametrosPosicionamentoMines
{
    qint8 id;
    QVector<QVector2D> vt_vt2dPosicoesMinasAdicionais;
    bool aliadoMines;
    bool adversarioMines;
    double inicioY;
    double finalY;
    double inicioX;
    double finalX;
    QVector2D fortuna;
    double distanciaMinimaFortuna;
    double distanciaMaximaFortuna;
    double larguraAreaMina;
    double raioMina;

    ParametrosPosicionamentoMines();
};

///
/// \brief Classe utilizada para determinar o destino dos robôs
/// \details Esta classe contém métodos para gerar a posição de destino do robô aliado de
/// forma que este fique o mais distante de pontos pré-determinados (como a posição dos robôs
/// adversários) e o mais próximo possível de outro ponto específico (como o gol adversário).
///
class Mines : public Positioning
{
public:

    ///
    /// \brief Mines
    ///
    Mines(const AmbienteCampo *_fieldEnv, QObject *parent = nullptr);

    ~Mines();

    void vSetaParametros(const ParametrosPosicionamentoMines& param);

    void vSetaParametrosNormalAtaque(const AmbienteCampo* environ);
    void vSetaParametrosNormalMeio(const AmbienteCampo* environ);

    ParametrosPosicionamentoMines getParameters() const;

    void vExecutaMines();

    ///
    /// \brief Atualiza as informações necessárias para realizar os cálculos
    /// \param acAuxiliar
    ///
    void vAtualizaInformacoesAmbiente(const AmbienteCampo* acAuxiliar, const float distanciaSegurancaAreaPenalty = 0);

    ///
    /// \brief Retorna a posição calculada do robô
    /// \param id - ID do robô
    ///
    QVector2D vt2dPosicaoFinal(qint8 id) const;

    ///
    /// \brief Reinicia a Matriz_Principal
    ///
    void vResetaMatrizPrincipal();

    ///
    /// \brief Desenha o mapa de calor da #matrizPrincipal
    /// \details Desenha:
    ///  * As linhas do campo;
    ///  * A linha das áreas;
    ///  * A pontuação de cada ponto em uma escala de cores do vermelho ao verde;
    ///  * Os robôs aliados e adversários;
    ///  * As bombas.
    ///
    /// A imagem é salva no diretório de trabalho em um arquivo png
    /// \param parametros - Parâmetros da Classe
    ///
    void vDesenhaHeatMap();

private:
    ParametrosPosicionamentoMines parametros;

    ///
    /// \brief Matriz que representa as posições no campo
    ///
    std::vector<std::vector<LIBERDADE>> matrizPrincipal;

    ///
    /// \brief Escala de transformaçãode um ponto real a um ponto da matriz matrizPrincipal
    ///
    const double dEscalaMatriz {.010};

    ///
    /// \brief Número de colunas da matrizPrincipal
    ///
    qint16 colunasMatriz {static_cast<qint16>(9000*dEscalaMatriz)};

    ///
    /// \brief Número de linhas da matrizPrincipal
    ///
    qint16 linhasMatriz {static_cast<qint16>(6000*dEscalaMatriz)};

    qint8 idRobo;

    ///
    /// \brief Liberdade máxima após a atribuição da Liberdade em todos os pontos
    ///
    LIBERDADE libGlobalMaixma;

    ///
    /// \brief Número de aliados
    ///
    qint8 aliados;

    ///
    /// \brief Número de inimigos
    ///
    qint8 adversarios;

    ///
    /// \brief Coordenadas das minas
    ///
    QList<ROBOVector2D_int16> list_vt2di16_CoordenadasMinas;

    ///
    /// \brief coordenadas onde a liberdade tem o valor máximo (lib_G_Max)
    ///
    QList<ROBOVector2D_int16> list_vt2di16_coordenadasMaiorLiberdade;


    ///
    /// \brief Coordenadas do robô analisado (escala da Matriz)
    ///
    ROBOVector2D_int16 vt2di16_coordenadasRobo;

    ///
    /// \brief Coordenadas da bola (escala da Matriz)
    ///
    ROBOVector2D_int16 vt2di16_coordenadasBola;

    ///
    /// \brief Coordenadas do destino geradas (escala da Matriz)
    ///
    ROBOVector2D_int16 vt2di16_coordenadasDestino;

    ///
    /// \brief iProfundidadeAreaPenalty
    ///
    qint16 iProfundidadeAreaPenalty;

    ///
    /// \brief iLarguraAreaPenalty
    ///
    qint16 iLarguraAreaPenalty;

    ///
    /// \brief iLadoCampo
    ///
    qint16 iLadoCampo;

    ///
    /// \brief RobotMines Aliados
    ///
    RobotMines roboAliado[MAX_IDS];

    ///
    /// \brief RobotMines Adversários
    ///
    RobotMines roboAdversario[MAX_IDS];

    ///
    /// \brief ID do goleiro adversário
    ///
    qint8 idGoleiroAdversario;

    ///
    /// \brief ID do goleiro aliado
    ///
    qint8 idGoleiroAliado;

    ///
    /// \brief Tamanho do Campo
    ///
    QSize szCampo;

    ///
    /// \brief Utilizado para reiniciar a Matriz periodicamente
    ///
    quint16 contadorAuxiliar;


    ///
    /// \brief Reinicia os resíduos do cálculo anterior e atualiza as posições dos robôs
    /// \param bConsiderarAdversario - caso deseja-se considerar a posição do adversário selecionar true
    /// \param vt_vt2dPosicoesMinasExternas - Minas adicionais
    ///
    void vAtribuicoesIniciais();

    void vAtribuiLiberdadeDeCadaPonto();

//    ///
//    /// \brief Atribui a liberdade de cada ponto para a função vCampoNormal
//    ///
//    void vAtribuiLiberdadeDeCadaPontoNormal();

//    ///
//    /// \brief Atribui a liberdade de cada ponto para a função vCampoBallPlacementAtaque
//    /// \param raio - raio de alcance da mina
//    ///
//    void vAtribuiLiberdadeDeCadaPontoBallPlacementAtaque(float raio);

//    ///
//    /// \brief Atribui a liberdade de cada ponto para a função vCampoBallPlacementDefesa
//    /// \param raio - raio de alcance da mina
//    ///
//    void vAtribuiLiberdadeDeCadaPontoBallPlacementDefesa(float raio);

    ///
    /// \brief Acha a melhor posição de destino para as posições em vt_vt2di16_Coordenadas_MaiorLiberdade
    ///
    void vAchaMelhorDestinoFinal();

    ///
    /// \brief Conversão de um ponto da matriz Matriz_Principal a um ponto real
    /// \param posMatriz - Coordenadas do ponto da matriz
    ///
    QVector2D vt2dConverteMatrizParaCoordenadasCampo(ROBOVector2D_int16 posMatriz);

    ///
    /// \brief Conversão de um ponto real a um ponto da matriz Matriz_Principal
    /// \param posCoordenadasCampo - Coordenadas do ponto real
    ///
    ROBOVector2D_int16 vt2di16ConverteCoordenadasCampoParaMatriz(QVector2D posCoordenadasCampo);

protected:
    void vUpdate() override;

};

#endif // MINES_H
