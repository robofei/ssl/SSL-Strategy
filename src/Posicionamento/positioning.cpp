#include "positioning.h"


Positioning::Positioning(const AmbienteCampo* _fieldEnv, QObject* parent) :
    QObject(parent),
    iAvailableRobots(),
    vt2dPositions(),
    env(_fieldEnv),
    tmrUpdate(new QTimer())

{
    tmrUpdate->setTimerType(Qt::PreciseTimer);

    connect(tmrUpdate.get(), &QTimer::timeout,
            this, &Positioning::vUpdate);
}

Positioning::~Positioning()
{
    if(tmrUpdate->isActive())
    {
        tmrUpdate->stop();
    }
}

const FieldZone& Positioning::getZone() const
{
    return zone;
}

void Positioning::vReceivePositionRequest_(const qint8 _id, QVector2D& _pos)
{
    if(vt2dPositions.contains(_id))
    {
        _pos = vt2dPositions.value(_id);
    }
}

void Positioning::vAddRobot(const qint8 _id)
{
    if(!iAvailableRobots.contains(_id))
    {
        iAvailableRobots.append(_id);
    }

    if(!tmrUpdate->isActive())
    {
        tmrUpdate->start(iTempoLoopEstrategia);
    }
}
