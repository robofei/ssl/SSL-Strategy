#ifndef POSITIONING_H
#define POSITIONING_H

#include <QObject>
#include "Ambiente/fieldzone.h"
#include "Ambiente/futbolenvironment.h"

/**
 * @brief Classe genérica para implementação de posicionamento grupal.
 */

class Positioning : public QObject
{
    Q_OBJECT
public:
    explicit Positioning(const AmbienteCampo* _fieldEnv, QObject *parent = nullptr);
    ~Positioning();

    const FieldZone& getZone()const;

protected:
    ///
    /// \brief Esse slot é chamada continuamente para atualizar a posição dos robôs.
    /// Este método deve ser reimplementado na classe filha.
    ///
    Q_SLOT virtual void vUpdate() = 0;

    QVector<qint8> iAvailableRobots; /**< IDs dos robôs  */
    QMap<qint8, QVector2D> vt2dPositions /**< Posição destino dos robôs */;
    const AmbienteCampo* env;
    FieldZone zone /**< Zone de campo permitida para o posicionamento */;

private:
    QScopedPointer<QTimer> tmrUpdate;

public slots:
    void vReceivePositionRequest_(const qint8 _id, QVector2D &_pos);
    void vAddRobot(const qint8 _id);
};

#endif // POSITIONING_H
