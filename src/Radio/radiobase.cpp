/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "radiobase.h"

RadioBase::RadioBase()
{
    spPorta = new QSerialPort();
    cfgRadioConfig = new ConfiguracaoRadio;
    iTipoSensor = 0;
    buffer_maxed_counter = 0;

    etmTimer.start();

    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        iRxCounter[i] = iRxLost[i] = 0;
        bSensoresChute[i] = false;
        fBaterias[i] = 0;
    }

    vConexoesPorta();
}

RadioBase::~RadioBase()
{
    if (spPorta)
        if (spPorta->isOpen())
            spPorta->close();

    delete spPorta;
    spPorta = nullptr;
    delete cfgRadioConfig;
    cfgRadioConfig = nullptr;
}

bool RadioBase::bConfigurarPortaSerial()
{
    spPorta->setPortName(cfgRadioConfig->Porta);
    spPorta->setBaudRate(cfgRadioConfig->baudRate);
    spPorta->setDataBits(cfgRadioConfig->dataBits);
    spPorta->setParity(cfgRadioConfig->parity);
    spPorta->setStopBits(cfgRadioConfig->stopBits);
    spPorta->setFlowControl(cfgRadioConfig->flowControl);
    spPorta->setReadBufferSize(32 * 15);

    return bAbrirPorta();
}

bool RadioBase::bAbrirPorta()
{
    if (spPorta->open(QIODevice::ReadWrite))
    {
        connect(spPorta, &QSerialPort::bytesWritten, this,
                &RadioBase::vParseSerialPortData_);

        emit _vSendPortError("Conectado");
        return true;
    }

    emit _vSendPortError(spPorta->errorString());
    spPorta->clearError();
    return false;
}

void RadioBase::vFecharPorta()
{
    if (spPorta->isOpen())
    {
        spPorta->close();
        emit _vSendPortError("Desconectado");
    }
}

ErrorCode RadioBase::errLerDados(QByteArray& retPack)
{
    //    retPack.append(spPorta->readAll());
    //    if(retPack.size() > 0)
    //    {
    //        qInfo() << "Packet received";
    //    }

    while (spPorta->bytesAvailable())
    {
        retPack.append(spPorta->readAll());
    }

    if (!retPack.isEmpty())
    {
        return errDecodificaPacote(retPack);
    }
    return ErrorCode::PACKET_INCOMPLETE;
}

ErrorCode RadioBase::errDecodificaPacote(QByteArray& encodedPacket)
{
    const int byte_start = 6;
    int delim_pos = -1;
    int packet_size;
    int robotid;
    // char checksum = 0; O robo nao usa o checksum

    // Variáveis utilizadas na conversão dos valores recebidos da odometria e
    // sensor de corrente
    float a1, a2;
    ErrorCode error_code = ErrorCode::NOPCKT_ERROR;

    const int INTERM_MAX_SIZE = 256;
    // const int SAFETY_THRESHOLD = 50;

    //    if(encodedPacket.size() > INTERM_MAX_SIZE)
    //    { //if buffer became too big, delete older packets - temporary - to be
    //    re-evaluated
    //        encodedPacket.remove(0, encodedPacket.size() - INTERM_MAX_SIZE);
    //        //remove older packets .... temporary buffer_maxed_counter++;
    //    }

    while (encodedPacket.size() > 4 && encodedPacket.size() < 32)
    {
        QByteArray packet = barrayGetNextPackage(encodedPacket);
        if (packet.size() < 32)
        {
            return ErrorCode::PACKET_INCOMPLETE;
        }

        // Procura o delimitador de inicio a partir do primeiro byte do pacote
        error_code = errEncontraDelimitador(packet, 0, delim_pos);

        // Checando se houve algum erro ao procurar o delimitador
        if (error_code != ErrorCode::NOPCKT_ERROR)
        {
            return error_code;
        }

        // O tamanho do pacote é igual a:
        // TamanhoEmBytes + 1ByteDelimitador + 2BytesComprimento + 1ByteChecksum
        // = TamanhoTotalBytes + 4bytes
        packet_size = (int)(packet[1] * 256 + packet[2]) + 4;

        // Pacote incompleto
        if (packet.size() < delim_pos + PREAMBLE_SIZE)
        {
            return ErrorCode::PACKET_INCOMPLETE;
        }
        else if (packet.size() < packet_size) // the packet isn't complete
            return ErrorCode::PACKET_INCOMPLETE;

        if (packet.at(3) != ROBOFEI_RADIO_SIGNATURE)
        {
            //             encodedPacket.clear(); //erase data, unhandled type
            //             of packet return ErrorCode::OTHER_RADIO_PCKT;
            continue;
        }
        // Aparentemente o checksum nao esta funcionando, por isso esta parte
        // está comentada

        //        for(int n = delim_pos + 3; n < packet_size-1; n++)//calcula o
        //        valor do checksum para o pacote recebido { //'delim_pos+3' é a
        //        posição do primeiro valor significativo do pacote (0x90)
        //            checksum += encodedPacket[n];
        //        }
        //        checksum = ~checksum;

        //        if(checksum != encodedPacket[packet_size-1])//'packet_size-1'
        //        é a posição onde está o valor do checksum
        //        {
        //            error_code = ErrorCode::CHECKSUM_ERROR;
        //            return error_code;
        //        }

        if (packet.at(byte_start) == PACKET_START &&
            packet.at(packet_size - 2) == PACKET_END)
        {
            robotid = packet.at(byte_start + 2);
            if (robotid < globalConfig.robotsPerTeam && robotid >= 0)
            {
                // Calcula para converter o valor recebido pelo rádio para o
                // valor em volts da bateria

                iRxCounter[robotid] = 0;
                float newBatteryValue = (float)packet.at(byte_start + 3) *
                                        5.0f / 4096.f * 11.f * 11.f;
                if (newBatteryValue <= 0)
                    newBatteryValue = fBaterias[robotid];
                fBaterias[robotid] = newBatteryValue;

                if ((int)packet.at(byte_start + 1) == 0x01)
                {
                    bSensoresChute[robotid] = true;
                }
                else if ((int)packet.at(byte_start + 1) == 0x00)
                {
                    bSensoresChute[robotid] = false;
                }

                if (packet.at(byte_start + 4) == static_cast<char>(iTipoSensor))
                { // type of sensor data received. 0=odometry, 1=current
                    if (iTipoSensor == 0)
                    {
                        a1 = 0;
                        a2 = 1.0f; // offset and scale multiplier for odometry -
                                   // basically there's no conversion
                    }
                    else
                    {
                        a1 = -37.5f;
                        a2 = 0.01831f;
                    } // offset and scale multiplier for the current sensor -
                      // values according to IC datasheet
                    // according to the datasheet the current is i = -37.5 +
                    // 0.01831*ADCRead

                    if (packet_size > byte_start + 20)
                    { // if packet is big enough to have full sensor data

                        // Atribuição do valor da odometria de cada motor
                        dOdometria_1[robotid].append(
                            a1 + a2 * (float)packet.at(byte_start + 5));
                        dOdometria_2[robotid].append(
                            a1 + a2 * (float)packet.at(byte_start + 9));
                        dOdometria_3[robotid].append(
                            a1 + a2 * (float)packet.at(byte_start + 13));
                        dOdometria_4[robotid].append(
                            a1 + a2 * (float)packet.at(byte_start + 17));
                        dTempoOdometria.append(
                            etmTimer.nsecsElapsed() /
                            1e9); // Salva o tempo em segundos

                        if (dTempoOdometria.constLast() > 10)
                        {
                            for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
                            {
                                dOdometria_1[n].clear();
                                dOdometria_2[n].clear();
                                dOdometria_3[n].clear();
                                dOdometria_4[n].clear();
                            }
                            dTempoOdometria.clear();
                            etmTimer.restart();
                        }
                    }
                }
            }
        }
    }
    encodedPacket.clear();
    return error_code;
}

QByteArray RadioBase::barrayGetNextPackage(QByteArray& _encodedPacket)
{
    QByteArray newPacket;
    newPacket.resize(32);

    if (_encodedPacket.size() < 32)
    {
        newPacket.clear();
        _encodedPacket.clear();
        return newPacket;
    }

    for (int n = 0; n < _encodedPacket.size(); ++n)
    {
        if (_encodedPacket.at(n) == INIT_DELIMITER)
        {
            if (_encodedPacket.size() > n + 32)
            {
                newPacket = _encodedPacket.mid(n, 32);
                _encodedPacket = _encodedPacket.remove(0, n + 32);
                return newPacket;
            }
            else
            {
                _encodedPacket.clear();
            }
            break;
        }
    }

    return newPacket;
}

ErrorCode RadioBase::errEncontraDelimitador(QByteArray& packet, int bytestart,
                                            int& delimiter_pos)
{
    for (int n = bytestart; n < packet.size(); n++)
    {
        if (packet.at(n) == INIT_DELIMITER)
        {
            delimiter_pos = n;
            //            packet.remove(0, n); //remove older packets ....
            //            temporary
            return ErrorCode::NOPCKT_ERROR;
        }
    }
    return ErrorCode::DELIMITER_NOT_FOUND;
}

bool RadioBase::bSendData(Atributos& robot)
{
    QByteArray encodedPacket = this->barrayCriarPacote(robot);
    return bSendData(encodedPacket);
}

bool RadioBase::bSendData(const QByteArray& pacote)
{
    if (!spPorta->isOpen())
        return false;

    QDataStream stream(spPorta);
    stream << pacote;

    return true;
}

void RadioBase::vRecebeErroSerial(QSerialPort::SerialPortError error)
{
    if (error != QSerialPort::NoError && error != QSerialPort::TimeoutError)
    {
        vFecharPorta();
        emit _vSendPortError(spPorta->errorString());
    }
}

void RadioBase::vConexoesPorta()
{
    connect(spPorta,
            static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(
                &QSerialPort::error),
            this, &RadioBase::vRecebeErroSerial);
}

void RadioBase::vSetaConfigs(QString _port, int _baudrate)
{
    cfgRadioConfig->Porta = _port;
    cfgRadioConfig->baudRate = static_cast<QSerialPort::BaudRate>(_baudrate);
    cfgRadioConfig->dataBits = QSerialPort::DataBits::Data8;
    cfgRadioConfig->flowControl = QSerialPort::FlowControl::NoFlowControl;
    cfgRadioConfig->parity = QSerialPort::Parity::NoParity;
    cfgRadioConfig->stopBits = QSerialPort::StopBits::OneStop;
    cfgRadioConfig->localEchoEnabled = false;
}

QByteArray RadioBase::barrayCodificar(QByteArray& packet)
{
    const int packetsize = PREAMBLE_SIZE + packet.size() + 1;
    const int packetlength = packetsize - 4;
    const int padding = 4;
    QByteArray encodedPacket(packetsize, 0x00);

    // Preamble - Nordic RoboFei protocol v1.0
    encodedPacket[0] = INIT_DELIMITER;                    // delimiter
    encodedPacket[1] = (unsigned char)packetlength / 256; // Length MSB
    encodedPacket[2] = (unsigned char)packetlength;       // Length LSB
    encodedPacket[3] = 0x10;                              // TX_request
    encodedPacket[4] = 0x0;                               // Frame ID (byte 5)
    encodedPacket[5] = packet[1];                         // ID do robô destino

    for (int n = 0; n < (int)packet.size(); n++) // counts packet size
    {
        encodedPacket[PREAMBLE_SIZE + n] = packet[n];
    }

    // checksum calculation
    int checksum = 0;
    for (int n = 3; n < encodedPacket.length() - 1; n++)
    {
        checksum += encodedPacket[n];
    }

    checksum = ~checksum;
    encodedPacket[packetsize - 1] = static_cast<unsigned char>(checksum);

    // Para evitar que algum pacote seja perdido pelos robôs
    encodedPacket.append(QByteArray(padding, 0x00));

    return encodedPacket;
}

QByteArray RadioBase::barrayCriarPacote(Atributos& robot)
{
    QByteArray data_pack(16, 0x00);

    // Insert the begin and end bytes into the packet. A simple safety measure
    // against interference
    data_pack[0] = PACKET_START;
    data_pack[15] = PACKET_END;
    // packet counter
    iRxCounter[robot.id]++;
    data_pack[7] = robot.radiotx_counter;

    // if(iRxCounter[robot.id] % 2 == 0)
    // {
    // Mandar 0x80 faz com que o robo retorne pacotes
    data_pack[8] = 0x80; // sync_radio
    // }
    // else
    //     data_pack[8] = 0x00;//sync_radio

    if (iRxCounter[robot.id] % 200 == 0)
    {
        iRxCounter[robot.id] = 0;
        bSensoresChute[robot.id] = false;
        fBaterias[robot.id] = 0;
    }

    data_pack[1] = (unsigned char)robot.id; // robot id

    data_pack[2] = (unsigned char)qCeil(robot.cmd_vx * 100 / globalConfig.robotVelocities.linear.max);
    data_pack[3] = (unsigned char)qCeil(robot.cmd_vy * 100 / globalConfig.robotVelocities.linear.max);
    data_pack[4] = (unsigned char)qCeil(robot.cmd_w * 100 / globalConfig.robotVelocities.angular.max);
    data_pack[5] =
        (unsigned char)(robot.fMaxLinearVelocity / globalConfig.robotVelocities.linear.max * 100);

    // PACOTE NOVO
    // roller + kick
    //  ============================== SOMENTE PARA FW ITA ===================
    if (robot.kick == KICK_CUSTOM)
    {
        robot.kick = KICK_CUSTOM;
    }
    // ============================== SOMENTE PARA FW ITA ===================

    if (robot.roller)
    {
        data_pack[6] = 0x40 | robot.kick;
        data_pack[10] = (unsigned char)robot.cmd_rollerspd; // roller_spd
    }
    else
    {
        data_pack[6] = 0x00 | robot.kick;
    }

    //     //kick
    //     if(robot.kick == KICK_NONE)
    //     {
    //         u_int8_t temp = 0x00;
    //         if(robot.roller)
    //         {
    //             temp = temp | 0x40;
    //         }
    //         data_pack[6]=temp;
    //     }
    //
    //     else if (robot.kick == KICK_STRONG)
    //     {
    //         u_int8_t temp = 0x00;
    //         if(robot.roller)
    //         {
    //             temp = temp | 0x40;
    //         }
    //
    //         data_pack[6] = (temp | 0x04);
    //     }
    //
    //     else if (robot.kick == KICK_CUSTOM)
    //     {
    //         u_int8_t temp = 0x00;
    //         if(robot.roller)
    //         {
    //             temp = temp | 0x40;
    //         }
    //
    //         data_pack[6] = (temp | 0x02);
    //     }
    //
    //     else if (robot.kick == KICK_SOFT)
    //     {
    //         u_int8_t temp = 0x00;
    //         if(robot.roller)
    //         {
    //             temp = temp | 0x40;
    //         }
    //
    //         data_pack[6] = (temp | 0x01);
    //     }
    //
    // //    //chip kick
    //     else if (robot.kick == CHIP_KICK_STRONG)
    //     {
    //         u_int8_t temp = 0x00;
    //         if(robot.roller)
    //         {
    //             temp = temp | 0x40;
    //         }
    //
    //         data_pack[6] = (temp | 0x20);
    //     }
    //
    //     else if (robot.kick == CHIP_KICK_CUSTOM)
    //     {
    //         u_int8_t temp = 0x00;
    //         if(robot.roller)
    //         {
    //             temp = temp | 0x40;
    //         }
    //
    //         data_pack[6] = (temp | 0x10);
    //     }
    //
    //     else if (robot.kick == CHIP_KICK_SOFT)
    //     {
    //         u_int8_t temp = 0x00;
    //         if(robot.roller)
    //         {
    //             temp = temp | 0x40;
    //         }
    //
    //         data_pack[6] = (temp | 0x08);
    //     }
    // PACOTE NOVO

    // PACOTE VELHO

    //    if(robot.kick == KICK_STRONG)
    //        data_pack[6] =0x03;

    //    else if(robot.kick == KICK_CUSTOM)
    //        data_pack[6] =0x02;

    //    else if(robot.kick == KICK_SOFT)
    //        data_pack[6] =0x01;

    //    else if(robot.kick == CHIP_KICK_STRONG)
    //        data_pack[6] =0x0C;

    //    else if(robot.kick == CHIP_KICK_CUSTOM)
    //        data_pack[6] =0x08;

    //    else if(robot.kick == CHIP_KICK_SOFT)
    //        data_pack[6] =0x04;

    //    if(robot.roller)
    //        data_pack[6]= 0x30;

    // PACOTE VELHO

    // directional kick //
    //    else if(robot.kick >= ANGLE_KICK_SOFT)
    //    {
    //        char temp=0;
    //        //aqui o kickStrength terá a regiao do angulo a ser chutado e seu
    //        respectivo sinal temp = robot.kickStrength;

    //        temp = temp | robot.kick; /*mascara para definir o tipo do chute,
    //        utilizada para introduzir os
    //                                           valores nos bits 4 e 3 da
    //                                           variavel temp*/
    //        data_pack[6]=temp;

    //    }

    data_pack[9] = (unsigned char)robot.kickStrength;

    // Enviar 0 pois esta forma de enviar as velocidades nao e mais utilizada
    data_pack[11] = data_pack[12] = data_pack[13] = data_pack[14] = 0;

    return barrayCodificar(data_pack);
}

void RadioBase::vParseSerialPortData_(qint64 _bytesWritten)
{
    QByteArray PacoteRecebido;
    ErrorCode erro;

    // Nao há dados suficientes
    if (_bytesWritten < 32)
    {
        return;
    }
    erro = errLerDados(PacoteRecebido);

    if (erro != NOPCKT_ERROR) // Ocorreu um erro no pacote
    {
        PacoteRecebido.clear();
        emit _vSendPacketError(erro);
        //        return;
    }

    // Atribuicao dos valores dos sensores e nivel de bateria
    RadioFeedback feedback;

    Atributos atbAtributoRobo;
    for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
    {
        feedback.vSetKickSensor(n, bSensoresChute[n]);
        feedback.vSetBattery(n, fBaterias[n]);

        if (dOdometria_1[n].size() > 0)
        {
            feedback.vSetOdometry(n, 0, dOdometria_1[n].constLast());
            feedback.vSetOdometry(n, 1, dOdometria_2[n].constLast());
            feedback.vSetOdometry(n, 2, dOdometria_3[n].constLast());
            feedback.vSetOdometry(n, 3, dOdometria_4[n].constLast());
        }
    }

    emit _vSendRobotFeedback(feedback);
}
