/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RADIOBASE_H
#define RADIOBASE_H

///
/// \file radiobase.h
/// \brief \a ConfiguracaoRadio e \a RadioBase
///

#include <QDataStream>
#include <QDebug>
#include <QDir>
#include <QMutex>
#include <QTextStream>
#include <QThread>
#include <QTime>
#include <QVector>
#include <QtCore/QtGlobal>
#include <QtEndian>
#include <QtSerialPort/QSerialPort>


#include <iostream>

#include "Ambiente/futbolenvironment.h"
#include "Robo/atributos.h"

#include "Radio/radiofeedback.h"
#include "Radio/robotcommands.h"

static const char INIT_DELIMITER = 0x7E; /**< Byte delimitador do pacote. */
static const int PREAMBLE_SIZE = 6; /**< Tamanho do cabeçalho do pacote. */
static const char PACKET_START = 0x62;
static const char PACKET_END = 0x65;
static const char ROBOFEI_RADIO_SIGNATURE = 0x90;

/**
 * @brief Struct com as configurações da porta serial utilizada para o rádio
 *
 */
typedef struct
{
    bool
        bConectar; /**< Indica se o radio deve conectar-se ou desconectar-se. */
    QString Porta; /**< Porta em que o radio ira conectar-se. */
    QSerialPort::DataBits dataBits;       /**< Data bits da conexão. */
    QSerialPort::FlowControl flowControl; /**< Flow control da conexão. */
    QSerialPort::Parity parity;           /**< Paridade da conexão. */
    QSerialPort::StopBits stopBits;       /**< Stop bits da conexão. */
    int baudRate;                         /**< Baud rate da conexão. */
    bool localEchoEnabled;                /**< Habilitar local echo. */

} ConfiguracaoRadio;

/**
 * @brief Códigos de erro que podem acontecer ao receber um pacote.
 *
 */
enum ErrorCode
{
    CHECKSUM_ERROR = 0x0A,
    DELIMITER_NOT_FOUND = 0x0B,
    NOPCKT_ERROR = 0x0C,
    PACKET_INCOMPLETE = 0x0D,
    OTHER_RADIO_PCKT = 0x0E,
    SAFETY_THRESHOLD_ERROR = 0x0F,
};

// Declaração dos parâmetros do radio para que seja possível enviá-los via sinal
Q_DECLARE_METATYPE(ConfiguracaoRadio)
Q_DECLARE_METATYPE(ErrorCode)

/**
 * @brief Classe que realiza o envio e recebimento de dados via porta serial
 * para o rádio.
 *
 */
class RadioBase : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Construtor da classe
     *
     */
    RadioBase();

    /**
     * @brief Destrutor da classe
     *
     */
    ~RadioBase() override;

    // Métodos utilizados no envio de um pacote
    /**
     * @brief Envia um pacote do robô fornecido pela porta serial
     *
     * @param robot - Atributos do robô a serem transmitidos
     * @return bool - True se o pacote foi enviado com sucesso
     */
    bool bSendData(Atributos& robot);

    /**
     * @brief Envia um pacote já codificado pela porta serial
     *
     * @param pacote - Pacote codificado pelo método #barrayCodificar
     * @return bool - True se o pacote foi enviado com sucesso
     */
    bool bSendData(const QByteArray& pacote);

    /**
     * @brief Codifica um pacote com os dados do robô
     *
     * @param data - Pacote com os dados do robô gerado pelo método
     * #barrayCriarPacote
     * @return QByteArray - Pacote codificado pronto para ser enviado
     */
    QByteArray barrayCodificar(QByteArray& data);

    /**
     * @brief Codifica as informações do robô em um pacote
     *
     * @param robot - Atributos do robô
     * @return QByteArray - Pacote pronto para ser codificado pelo método
     * #barrayCodificar
     */
    QByteArray barrayCriarPacote(Atributos& robot);

    // Métodos utilizados no recebimento de um pacote
    /**
     * @brief Lê os dados contidos no pacote fornecido
     *
     * @param retPack - Pacote vindo da porta serial
     * @return ErrorCode - Código do erro caso tenha algo de errado com o pacote
     */
    ErrorCode errLerDados(QByteArray& retPack);

    /**
     * @brief Decodifica o pacote fornecido
     *
     * @param encodedPacket - Pacote vindo da porta serial
     * @return ErrorCode - Código do erro caso tenha algo de errado com o pacote
     */
    ErrorCode errDecodificaPacote(QByteArray& encodedPacket);

    /**
     * @brief Retorna o próximo pacote da fila
     *
     * @param _encodedPacket - Fila de pacotes
     *
     * @return
     */
    QByteArray barrayGetNextPackage(QByteArray& _encodedPacket);

    // Métodos utilizados na configuração da porta serial
    /**
     * @brief Conecta alguns slots da classe
     *
     */
    void vConexoesPorta();

    /**
     * @brief Fecha a porta serial (#spPorta)
     *
     */
    void vFecharPorta();

    /**
     * @brief Seta as configurações do rádio
     *
     * @param _port - Porta serial
     * @param _baudrate - Baud rate
     */
    void vSetaConfigs(QString _port, int _baudrate);

    QSerialPort* spPorta;      /**< Porta serial.*/
    QElapsedTimer etmTimer;    /**< Timer utilizado para gerar uma base de tempo
                                   para os dados que chegam pela porta serial.*/

    // Variáveis utilizadas na decodificação dos pacotes recebidos
    QVector<double> dOdometria_1[MAX_IDS]; /**< Vetor com os dados de
                                                      odometria dos robôs. */
    QVector<double> dOdometria_2[MAX_IDS]; /**< Vetor com os dados de
                                                      odometria dos robôs. */
    QVector<double> dOdometria_3[MAX_IDS]; /**< Vetor com os dados de
                                                      odometria dos robôs. */
    QVector<double> dOdometria_4[MAX_IDS]; /**< Vetor com os dados de
                                                      odometria dos robôs. */
    QVector<double> dTempoOdometria; /**< Vetor com o timestamp de chegada dos
                                       dados de odometria. */
    
    float fBaterias[MAX_IDS];     /**< Vetor com o valor da bateria de
                                            todos     os robôs.*/
bool bSensoresChute[MAX_IDS]; /**< Vetor com o estado do sensor de
                                        chute de todos os robôs.*/
        int iRxCounter[MAX_IDS];      /**< Conta quantos pacotes foram
                                              recebidos. */
int iRxLost[MAX_IDS]; /**< ??? (Faz parte do código antigo). */
    int iTipoSensor; /**< Tipo do sensor que o robô está enviando. Em teoria
    ele envia os dados da odometria e de corrente dos motores, porém os dados
    de corrente parecem não funcionar direito. */
    int buffer_maxed_counter; /**< ??? (Faz parte do código antigo). */

     Atributos atbRobos[MAX_IDS]; /**< Atributos dos robôs.*/

private:
    /**
     * @brief Abre a porta serial (#spPorta)
     *
     * @return bool - True se conseguiu abrir a porta sem problemas
     */
    bool bAbrirPorta(); // Abre a porta serial

    /**
     * @brief Encontra o byte delimitador do pacote
     *
     * @param packet - Pacote
     * @param bytestart - Índice do byte inicial
     * @param delimiter_pos - Posição do delimitador, caso exista
     * @return ErrorCode - Código do erro caso não encontre o delimitador
     */
    ErrorCode errEncontraDelimitador(QByteArray& packet, int bytestart,
                                     int& delimiter_pos);

    ConfiguracaoRadio* cfgRadioConfig; /**< Configurações do rádio. */

public slots:
    /**
     * @brief Configura e abre a porta serial
     *
     * @return bool - True se consguiu abrir a porta
     */
    bool bConfigurarPortaSerial();

    /**
     * @brief Slot que recebe o código do erro caso não seja possível abrir a
     * porta serial, por exemplo
     *
     * @param error - Erro que ocorreu
     */
    void vRecebeErroSerial(QSerialPort::SerialPortError error);

    /**
     * @brief Slot para receber os dados da porta serial
     */
    void vParseSerialPortData_(qint64 _bytesWritten);

signals:
    /**
     * @brief _vSendRobotFeedback Envia os dados recebidos pelo rádio.
     * Este sinal é utilizado em especial para enviar os dados recebidos pelo
     * rádio diretamente para a thread de estratégia (Estrategia)
     * @param _feedback - Objeto com os dados recebidos pelo rádio
     */
    void _vSendRobotFeedback(const RadioFeedback& _feedback);

    void _vSendPortError(const QString& _error);

    void _vSendPacketError(const ErrorCode& _error);
};
#endif // RADIOBASE_H
