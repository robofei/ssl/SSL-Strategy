#include "radiofeedback.h"

RadioFeedback::RadioFeedback()
{
    for(qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robotData[id].bKickSensor = false;
        robotData[id].fBattery = 0;
        robotData[id].iOdometry[0] = 0;
        robotData[id].iOdometry[1] = 0;
        robotData[id].iOdometry[2] = 0;
        robotData[id].iOdometry[3] = 0;
    }
}

RadioFeedback::RadioFeedback(const RadioFeedback &_other)
{
    for(qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robotData[id].bKickSensor  = _other.bGetKickSensor(id);
        robotData[id].fBattery     = _other.fGetBattery(id);
        robotData[id].iOdometry[0] = _other.iGetOdometry(id, 0);
        robotData[id].iOdometry[1] = _other.iGetOdometry(id, 1);
        robotData[id].iOdometry[2] = _other.iGetOdometry(id, 2);
        robotData[id].iOdometry[3] = _other.iGetOdometry(id, 3);
    }
}

RadioFeedback::~RadioFeedback()
{

}

bool RadioFeedback::bGetKickSensor(const qint8 _id) const
{
    if(_id >= 0 && _id < globalConfig.robotsPerTeam)
        return robotData[_id].bKickSensor;
    return false;
}

float RadioFeedback::fGetBattery(const qint8 _id) const
{
    if(_id >= 0 && _id < globalConfig.robotsPerTeam)
        return robotData[_id].fBattery;
    return false;
}

int RadioFeedback::iGetOdometry(const qint8 _id, const quint8 _wheelN) const
{
    if(_id >= 0 && _id < globalConfig.robotsPerTeam && _wheelN >= 0 && _wheelN < 4)
        return robotData[_id].iOdometry[_wheelN];
    return 0;
}

void RadioFeedback::vSetKickSensor(const qint8 _id, bool _sensor)
{
    if(_id >= 0 && _id < globalConfig.robotsPerTeam)
        robotData[_id].bKickSensor = _sensor;
}

void RadioFeedback::vSetBattery(const qint8 _id, float _voltage)
{
    _voltage = qMax(_voltage, 0.f);
    _voltage = qMin(_voltage, 30.f);

    if(_id >= 0 && _id < globalConfig.robotsPerTeam)
        robotData[_id].fBattery = _voltage;
}

void RadioFeedback::vSetOdometry(const qint8 _id, quint8 _wheelN, int _value)
{
    if(_id >= 0 && _id < globalConfig.robotsPerTeam && _wheelN >= 0 && _wheelN < 4)
        robotData[_id].iOdometry[_wheelN] = _value;
}
