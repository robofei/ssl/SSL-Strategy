#ifndef RADIOFEEDBACK_H
#define RADIOFEEDBACK_H

#include <QObject>
#include "qglobal.h"

#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

struct FeedbackData
{
    bool bKickSensor;
    float fBattery;
    int iOdometry[4];
};

class RadioFeedback
{
    FeedbackData robotData[MAX_IDS];

public:
    RadioFeedback();
    RadioFeedback(const RadioFeedback &_other);

    ~RadioFeedback();
    bool bGetKickSensor(const qint8 _id) const;
    float fGetBattery(const qint8 _id) const;
    int iGetOdometry(const qint8 _id, const quint8 _wheelN) const;

    void vSetKickSensor(const qint8 _id, bool _sensor);
    void vSetBattery(const qint8 _id, float _voltage);
    void vSetOdometry(const qint8 _id, const quint8 _wheelN, int _value);
};

Q_DECLARE_METATYPE(RadioFeedback)

#endif // RADIOFEEDBACK_H
