/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ROBOFEICONTROLLER_H
#define ROBOFEICONTROLLER_H

///
/// \file logger.h
/// \brief \a Logger
/// \details Detalhes
///

#include <QFile>
#include <QMutex>
#include <QString>
#include <QTextStream>
#include <QVector>

#include <sstream>

#include "ssl_vision_detection.pb.h"
#include "ssl_vision_geometry.pb.h"
#include "ssl_refbox_log.pb.h"
#include "ssl_wrapper.pb.h"
#include "referee.pb.h"

#include "Ambiente/futbolenvironment.h"

#define QTD_MAX_PCT 5

/**
 * @brief Classe que cria logs da visão e referee
 */
class Logger
{
public:
    /**
    * @brief Construtor da classe
    *
    */
    Logger();

    /**
    * @brief Atualiza os dados da visão com o pacote fornecido
    *
    * @param _s
    */
    void addVision( const SSL_WrapperPacket& _s);

    /**
    * @brief Atualiza os dados do referee com o pacote fornecido
    *
    * @param _s
    */
    void addReferee( const SSL_Referee& _s );

    /**
    * @brief Retorna o último pacote da visão fornecido
    * @see #addVision
    *
    * @return SSL_WrapperPacket
    */
    SSL_WrapperPacket getLastVision();

    /**
    * @brief Retorna o último pacote do referee fornecido
    * @see #addReferee
    *
    * @return SSL_Referee
    */
    SSL_Referee getLastReferee();

    /**
    * @brief Retorna uma string referente ao comando do referee fornecido
    *
    * @param _s - Comando do referee
    * @return QString
    */
    QString toStringReferee( const  SSL_Referee& _s);

    /**
    * @brief Retorna uma string contendo os dados do pacote fornecido
    *
    *  @param _p - Pacote
    * @return QString
    */
    QString toStringWrapperPacket(const  SSL_WrapperPacket& _p );

    /**
    * @brief Inicia os arquivos de log da visão e referee
    *
    * @param _lr - Nome do arquivo de log do referee
    * @param _lv - Nome do arquivo de log da visão
    */
    void start_log(const QString& _lr, const QString& _lv);

    /**
    * @brief Escreve os dados do último pacote do referee fornecido no log
    */
    void write_log_referee();

    /**
    * @brief Escreve os dados do último pacote da visão no log
    *
    */
    void write_log_vision();

    /**
    * @brief Para a escrita de logs
    *
    */
    void stop_log();

    /**
    * @brief Retorna uma string com o nome do estágio atual de jogo
    *
    * @param ssl_referee - Dados do referee
    * @return QString - String com o nome do estágio atual de jogo
    */
    static QString getStage(const SSL_Referee & ssl_referee);

    /**
    * @brief Retorna uma string com o nome do comando atual do referee
    *
    * @param ssl_referee - Dados do referee
    * @return QString - String com o nome do comando atual do referee
    */
    static QString getCommand(const SSL_Referee & ssl_referee);

    /**
     * @brief Retorna uma string com o nome do comando atual do referee a partir
     * do próprio comando
     *
     * @param ssl_referee_command - Comando do referee
     * @return QString - String com o nome do comando atual do referee
     */
    static QString getCommand(const SSL_Referee_Command ssl_referee_command);

private:
    //trocado de vector por qlist em 09/09/2017 - MAL
    QVector<SSL_WrapperPacket> ssl_vision; /**< Vetor com os últimos pacotes da
                                                visão fornecidos.*/
    QVector<SSL_Referee> ssl_referee; /**< Vetor com os últimos pacotes do 
                                                referee fornecidos.*/

    //log formato humano em 11/09/2017 - MAL
    QFile refereeLogFile; /**< Arquivo de log do referee. */
    QFile visionLogFile; /**< Arquivo de log da visão. */
    bool logActive; /**< Indica que a coleta de logs está ativa. 
                       @see #start_log */

};

#endif // ROBOFEICONTROLLER_H
