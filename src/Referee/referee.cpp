#include "referee.h"

Referee::Referee(QObject *parent) : QObject(parent)
{
    sslrefCommand = SSL_Referee_Command_HALT;
    sslrefStage   = SSL_Referee_Stage_NORMAL_FIRST_HALF_PRE;

    vt2dBallPosition         = QVector2D(0, 0);
    vt2dStartingBallPosition = QVector2D(0, 0);
    vt2dBallPlacement        = QVector2D(0, 0);

    tmrReferee.reset();

    timeLeft = QTime(0, 0, 0);
    bBallInPlay = false;
}

void Referee::vSetCommand(SSL_Referee_Command _command)
{
    if(_command == SSL_Referee_Command_STOP ||
            _command == SSL_Referee_Command_HALT ||
            _command == SSL_Referee_Command_PREPARE_PENALTY_BLUE ||
            _command == SSL_Referee_Command_PREPARE_PENALTY_YELLOW ||
            _command == SSL_Referee_Command_PREPARE_KICKOFF_BLUE ||
            _command == SSL_Referee_Command_PREPARE_KICKOFF_YELLOW ||
            _command == SSL_Referee_Command_BALL_PLACEMENT_BLUE||
            _command == SSL_Referee_Command_BALL_PLACEMENT_YELLOW)
    {
        vt2dStartingBallPosition = vt2dBallPosition;
        bBallInPlay = false;
    }
    if(sslrefCommand == SSL_Referee_Command_STOP && sslrefCommand != _command &&
            _command != SSL_Referee_Command_HALT)
    {
        vt2dStartingBallPosition = vt2dBallPosition;
        qInfo() << QString("[Referee]@%1) Posição inicial da bola (%2, %3)!")
            .arg(strGetCommand())
            .arg(vt2dStartingBallPosition.x())
            .arg(vt2dStartingBallPosition.y());
    }

    sslrefCommand = _command;
}

SSL_Referee_Command Referee::sslrefGetCommand() const
{
    return sslrefCommand;
}

QString Referee::strGetCommand() const
{
    return QString::fromStdString(SSL_Referee::Command_Name(sslrefCommand));
}

bool Referee::bInPlay() const
{
    return bBallInPlay;
}

void Referee::vSetStage(SSL_Referee_Stage _stage)
{
    sslrefStage = _stage;
}

SSL_Referee_Stage Referee::sslrefGetStage() const
{
    return sslrefStage;
}

QString Referee::strGetStage() const
{
    return QString::fromStdString(SSL_Referee::Stage_Name(sslrefStage));
}

void Referee::vUpdateGameTime(QTime _time)
{
    timeLeft = _time;
}


QTime Referee::tmGetTimeLeft() const
{
    return timeLeft;
}

void Referee::vUpdateBallPosition(QVector2D _position)
{
    vt2dBallPosition = _position;

    float dist = vt2dBallPosition.distanceToPoint(vt2dStartingBallPosition);
    if(bBallInPlay == false)
    {
        if((dist > globalConfig.ballDistanceToPlay
             && sslrefCommand != SSL_Referee_Command_STOP
             && sslrefCommand != SSL_Referee_Command_HALT
             && sslrefCommand != SSL_Referee_Command_PREPARE_KICKOFF_BLUE
             && sslrefCommand != SSL_Referee_Command_PREPARE_KICKOFF_YELLOW
             && sslrefCommand != SSL_Referee_Command_PREPARE_PENALTY_BLUE
             && sslrefCommand != SSL_Referee_Command_PREPARE_PENALTY_YELLOW
             && sslrefCommand != SSL_Referee_Command_BALL_PLACEMENT_BLUE
             && sslrefCommand != SSL_Referee_Command_BALL_PLACEMENT_YELLOW
             && sslrefCommand != SSL_Referee_Command_TIMEOUT_BLUE
             && sslrefCommand != SSL_Referee_Command_TIMEOUT_YELLOW)
             || sslrefCommand == SSL_Referee_Command_FORCE_START)
        {
            bBallInPlay = true;
            emit _vBallInPlay(bBallInPlay);
            qInfo() << QString("[Referee] Ball in play! D(%1)").arg(dist);
        }
    }
}

void Referee::vSetGoalie(TeamColor _color, qint8 _id)
{
    if(_color == timeAmarelo)
    {
        yellowTeam.iGoalieID = _id;
    }
    else
    {
        blueTeam.iGoalieID = _id;
    }
}

void Referee::vUpdateDataFromPacket(const RefereePacket &_packet)
{
    vSetCommand(_packet.sslrefCommand);
    vSetStage(_packet.sslrefStage);
    vUpdateGameTime(_packet.timeLeft);
    vt2dBallPlacement = _packet.vt2dBallPlacement;

    yellowTeam.vUpdate(*_packet.yellowTeam);
    blueTeam.vUpdate(*_packet.blueTeam);
}

QVector2D Referee::ballPlacementPosition() const
{
    return vt2dBallPlacement;
}

void Referee::vRefereeTimerExpired_()
{

}

