#ifndef REFEREE_H
#define REFEREE_H

#include <QObject>
#include <QVector2D>
#include <QTimer>
#include <QTime>
#include <QScopedPointer>

#include "Ambiente/sslteam.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

#include "referee.pb.h"
#include "Referee/refereepacket.h"
#include "Referee/refereeteamdata.h"

/**
 * @brief Implementa as funcionalidades do Referee
 */
class Referee : public QObject
{
    Q_OBJECT

private:
    QScopedPointer<QTimer> tmrReferee;
    bool bBallInPlay;

protected:
    SSL_Referee_Command sslrefCommand;
    SSL_Referee_Stage sslrefStage;
    QTime timeLeft;

    QVector2D vt2dBallPosition;
    QVector2D vt2dStartingBallPosition;
    QVector2D vt2dBallPlacement;

public:
    RefereeTeamData yellowTeam;
    RefereeTeamData blueTeam;
    explicit Referee(QObject *parent = nullptr);

    void vSetCommand(SSL_Referee_Command _command);
    SSL_Referee_Command sslrefGetCommand() const;
    QString strGetCommand() const;

    bool bInPlay() const;

    void vSetStage(SSL_Referee_Stage _stage);
    SSL_Referee_Stage sslrefGetStage() const;
    QString strGetStage() const;

    void vUpdateGameTime(QTime _time);
    QTime tmGetTimeLeft() const;

    void vUpdateBallPosition(QVector2D _position);
    void vSetGoalie(TeamColor _color, qint8 _id);

    void vUpdateDataFromPacket(const RefereePacket &_packet);
    QVector2D ballPlacementPosition() const;

private slots:
    void vRefereeTimerExpired_();

signals:
    void _vBallInPlay(const bool _inPlay);
};

#endif // REFEREE_H
