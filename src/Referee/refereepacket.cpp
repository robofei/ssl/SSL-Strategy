#include "refereepacket.h"

RefereePacket::RefereePacket()
{
    sslrefCommand = SSL_Referee_Command_HALT;
    strCommand    = "HALT";

    sslrefStage = SSL_Referee_Stage_NORMAL_FIRST_HALF_PRE;
    strStage = "Normal First Half Pre";

    timeLeft.setHMS(0, 0, 0);
    vt2dBallPlacement.setX(0); vt2dBallPlacement.setY(0);

    yellowTeam = blueTeam = nullptr;
}

RefereePacket::RefereePacket(const RefereePacket &_other)
{
    sslrefCommand = _other.sslrefCommand;
    strCommand    = _other.strCommand;
    sslrefStage   = _other.sslrefStage;
    strStage      = _other.strStage;

    timeLeft          = _other.timeLeft;
    vt2dBallPlacement = _other.vt2dBallPlacement;

    yellowTeam = new RefereeTeamData(*_other.yellowTeam);
    blueTeam   = new RefereeTeamData(*_other.blueTeam);
}

RefereePacket::RefereePacket(SSL_Referee_Command _command, QString _scommand,
                             SSL_Referee_Stage _stage, QString _sstage,
                             QTime _time, QVector2D _ballPlacement,
                             RefereeTeamData *_yellow, RefereeTeamData *_blue)
{
    sslrefCommand = _command;
    strCommand    = _scommand;
    sslrefStage   = _stage;
    strStage      = _sstage;

    timeLeft          = _time;
    vt2dBallPlacement = _ballPlacement;

    yellowTeam = new RefereeTeamData(*_yellow);
    blueTeam   = new RefereeTeamData(*_blue);
}

RefereePacket::~RefereePacket()
{
    if(blueTeam != nullptr)
        delete blueTeam;
    if(yellowTeam != nullptr)
        delete yellowTeam;
}
