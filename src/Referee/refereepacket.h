#ifndef REFEREEPACKET_H
#define REFEREEPACKET_H

#include <QObject>
#include <QVector2D>
#include <QTime>

#include "referee.pb.h"
#include "Ambiente/sslteam.h"
#include "Referee/refereeteamdata.h"

class RefereePacket
{
public:
    SSL_Referee_Command sslrefCommand;
    QString strCommand;
    SSL_Referee_Stage sslrefStage;
    QString strStage;

    QTime timeLeft;

    QVector2D vt2dBallPlacement;

    RefereeTeamData *yellowTeam;
    RefereeTeamData *blueTeam;

    RefereePacket();
    RefereePacket(const RefereePacket &_other);
    RefereePacket(SSL_Referee_Command _command, QString _scommand,
                  SSL_Referee_Stage _stage, QString _sstage,
                  QTime _time, QVector2D _ballPlacement,
                  RefereeTeamData *_yellow, RefereeTeamData *_blue);
    ~RefereePacket();
};

Q_DECLARE_METATYPE(RefereePacket)

#endif // REFEREEPACKET_H
