#include "refereereceiver.h"

RefereeReceiver::RefereeReceiver(QObject *parent) : Referee(parent)
{
    udpSocket.reset(new QUdpSocket(this));
    tmrNoPacketTimer.reset(new QTimer());

    tmrNoPacketTimer->setInterval(1e3);

    connect(tmrNoPacketTimer.get(), &QTimer::timeout,
            this, &RefereeReceiver::vNoPacketReceived_);
}

void RefereeReceiver::vSetConnectionInfo(QString _ip, int _port, QString _interface)
{
    strIP = _ip;
    iPort = _port;
    strInterface = _interface;
}

bool RefereeReceiver::bConnect()
{
    qInfo() << QString("[Referee Rec.] Conectando em %1:%2 @ %3").arg(strIP).arg(iPort).arg(strInterface);

    if(udpSocket->bind(QHostAddress::AnyIPv4, iPort, QUdpSocket::ShareAddress) &&
       udpSocket->joinMulticastGroup(QHostAddress(strIP),
                                     QNetworkInterface::interfaceFromName(strInterface)))
    {
       connect(udpSocket.get(), &QIODevice::readyRead,
               this,            &RefereeReceiver::vParseRefereeData_);
       tmrNoPacketTimer->start();
       return true;
    }

    qCritical() << "[Referee Rec.] Não foi possível se conectar com o referee!";
    return false;
}

void RefereeReceiver::vDisconnect()
{
    udpSocket->disconnect(SIGNAL(readyRead()));
    udpSocket->close();
}

void RefereeReceiver::vParseTeamInfo(RefereeTeamData &team,
                             const SSL_Referee_TeamInfo &info)
{
    int activeCards = 0;
    if(info.has_name())
    {
        team.strName = QString::fromStdString(info.name());
    }
    if(info.has_score())
    {
        team.iGoals = info.score();
    }
    if(info.has_red_cards())
    {
        team.iRedCards = info.red_cards();
        activeCards += team.iRedCards;
    }
    if(info.has_yellow_cards())
    {
        team.iYellowCards = info.yellow_cards();
        team.timeYellowLeft.clear();
        for(int n = 0; n < info.yellow_card_times_size(); ++n)
        {
            QTime yellowTime = QTime(0,0,0).addMSecs(info.yellow_card_times(n)/1e3);
            team.timeYellowLeft.append(yellowTime);

            if(QTime(0,0,0).secsTo(yellowTime) > 1)
            {
                activeCards++;
            }
        }
    }
    if(info.has_timeouts())
    {
        team.iTimeouts = info.timeouts();
    }
    if(info.has_timeout_time())
    {
        team.timeTimeout = QTime(0,0,0).addMSecs(info.timeout_time()/1e3);
    }
    if(info.has_goalie())
    {
        team.iGoalieID = info.goalie();
    }

#if ROBO_DIVISAO == ROBO_DIVISAO_A
    team.iMaxRobots = 11 - activeCards;
#else
    team.iMaxRobots = 6 - activeCards;
#endif
}

void RefereeReceiver::vSimulateReferee(SSL_Referee_Command _command,
                               QVector2D ballPlacement)
{
    vt2dBallPlacement = ballPlacement;
    sslrefCommand = _command;
    sslrefStage = SSL_Referee_Stage_NORMAL_FIRST_HALF;
    timeLeft.setHMS(0, 5, 0);

    emit _vRefereeData(RefereePacket(sslrefCommand, strGetCommand(),
                                     sslrefStage, strGetStage(),
                                     timeLeft, vt2dBallPlacement,
                                     &yellowTeam, &blueTeam));
}

void RefereeReceiver::vParseRefereeData_()
{
    while(udpSocket->hasPendingDatagrams())
    {
        tmrNoPacketTimer->start();

        QByteArray datagram;
        datagram.resize(udpSocket->pendingDatagramSize());
        datagram.fill(0, udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(datagram.data(), datagram.size());

        SSL_Referee sslreferee;
        sslreferee.ParseFromArray(datagram, datagram.size());

        timeLeft = QTime(0,0,0).addMSecs(sslreferee.stage_time_left()/1e3);
        sslrefCommand = sslreferee.command();
        sslrefStage = sslreferee.stage();

        // Amarelo
        vParseTeamInfo(yellowTeam, sslreferee.yellow());

        // Azul
        vParseTeamInfo(blueTeam, sslreferee.blue());

        if(sslreferee.has_designated_position())
        {
            const SSL_Referee_Point ballPlacement = sslreferee.designated_position();
            vt2dBallPlacement.setX(ballPlacement.x());
            vt2dBallPlacement.setY(ballPlacement.y());
        }

        emit _vRefereeData(RefereePacket(sslrefCommand, strGetCommand(),
                                         sslrefStage, strGetStage(),
                                         timeLeft, vt2dBallPlacement,
                                         &yellowTeam, &blueTeam));
    }
}


void RefereeReceiver::vNoPacketReceived_()
{
    emit _vRefereeData(RefereePacket(sslrefCommand, strGetCommand(),
                                     sslrefStage, strGetStage(),
                                     timeLeft, vt2dBallPlacement,
                                     &yellowTeam, &blueTeam));
}
