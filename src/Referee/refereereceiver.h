#ifndef REFEREERECEIVER_H
#define REFEREERECEIVER_H

#include <QObject>
#include <QVector2D>
#include <QUdpSocket>
#include <QtNetwork/qnetworkinterface.h>
#include <QScopedPointer>
#include <QTimer>

#include "Ambiente/sslteam.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

#include "qscopedpointer.h"
#include "referee.pb.h"
#include "Referee/referee.h"
#include "Referee/refereepacket.h"
#include "Referee/refereeteamdata.h"

/**
 * @brief Implementa as funcinalidades para receber os pacotes do Referee pela
 * rede.
 */
class RefereeReceiver : public Referee
{
    Q_OBJECT

    QScopedPointer<QUdpSocket> udpSocket;
    QString strIP;
    int iPort;
    QString strInterface;
    QScopedPointer<QTimer> tmrNoPacketTimer;

public:
    explicit RefereeReceiver(QObject *parent = nullptr);

    void vSetConnectionInfo(QString _ip, int _port, QString _interface);
    bool bConnect();
    void vDisconnect();
    void vParseTeamInfo(RefereeTeamData &team, const SSL_Referee_TeamInfo &info);
    void vSimulateReferee(SSL_Referee_Command _command, QVector2D ballPlacement);

private slots:
    void vParseRefereeData_();
    void vNoPacketReceived_();

signals:
    void _vRefereeData(const RefereePacket _packet);
};

#endif // REFEREERECEIVER_H
