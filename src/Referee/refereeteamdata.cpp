#include "refereeteamdata.h"

RefereeTeamData::RefereeTeamData()
{
    iYellowCards = 0;
    timeYellowLeft.clear();

    iRedCards = 0;

    iGoals = 0;

    iTimeouts = 5;
    timeTimeout = QTime(0, 5, 0);

    strName = QString("Unknow");
    iGoalieID = 0;
    iFieldSide = XNegativo;
    iMaxRobots = globalConfig.robotsPerTeam;
}


RefereeTeamData::RefereeTeamData(const RefereeTeamData &_other)
{
    iYellowCards = _other.iYellowCards;
    timeYellowLeft.clear();
    timeYellowLeft.append(_other.timeYellowLeft);

    iRedCards = _other.iRedCards;

    iGoals = _other.iGoals;

    iTimeouts = _other.iTimeouts;
    timeTimeout = _other.timeTimeout;

    strName = _other.strName;
    iGoalieID = _other.iGoalieID;
    iFieldSide = _other.iFieldSide;
    iMaxRobots = _other.iMaxRobots;
}

void RefereeTeamData::vUpdate(const RefereeTeamData &_other)
{
    iYellowCards = _other.iYellowCards;
    timeYellowLeft.clear();
    timeYellowLeft.append(_other.timeYellowLeft);

    iRedCards = _other.iRedCards;

    iGoals = _other.iGoals;

    iTimeouts = _other.iTimeouts;
    timeTimeout = _other.timeTimeout;

    strName = _other.strName;
    iGoalieID = _other.iGoalieID;
    iFieldSide = _other.iFieldSide;
    iMaxRobots = _other.iMaxRobots;
}
