#ifndef REFEREETEAMDATA_H
#define REFEREETEAMDATA_H

#include <QTime>
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "qvector.h"

class RefereeTeamData
{
public:
    quint8 iYellowCards;
    QVector<QTime> timeYellowLeft;

    quint8 iRedCards;

    quint8 iGoals;

    quint8 iTimeouts;
    QTime timeTimeout;

    QString strName;
    quint8 iGoalieID;
    int iFieldSide;
    qint8 iMaxRobots;

    RefereeTeamData();
    RefereeTeamData(const RefereeTeamData &_other);
    void vUpdate(const RefereeTeamData &_other);
};

#endif // REFEREETEAMDATA_H
