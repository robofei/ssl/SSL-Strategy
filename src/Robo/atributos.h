/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file atributos.h
 * @brief Arquivo header com a definição dos atributos do robô.
 * @author Leonardo da Silva Costa e outros
 * @version 1.0
 * @date 2020-08-26
 */

#ifndef ATRIBUTOS_H
#define ATRIBUTOS_H

#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QElapsedTimer>
#include <QMap>
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Bola/bola.h"

/**
 * @brief Tipos de chute que o robô possui
 *
 */
enum KICKTYPE
{
    KICK_NONE         = 0x00,
    KICK_SOFT         = 0x01, // kick
    KICK_CUSTOM       = 0x02, // kick
    KICK_STRONG       = 0x04, // chip
    CHIP_KICK_SOFT    = 0x08,
    CHIP_KICK_CUSTOM  = 0x10, // chip
    CHIP_KICK_STRONG  = 0x20, // chip
    ANGLE_KICK_SOFT   = 0x18, // Valores utilizados na mascara para construir
    ANGLE_KICK_STRONG = 0x24, // o protocolo que identifica o chute
    ANGLE_KICK_CUSTOM = 0x12, // OBS: Estes valores provavelmente mudarão
};                            // quando o chute direcional for implementado de
                              // fato

struct ModeloChute
{

    // v = t < tCorte ? velMax - t * (velMax - velCorte) / tCorte
    //                  :
    //                  velCorte * tMax / (tMax - tCorte) - t * velCorte / (tMax - tCorte)
    float velMax;
    float velCorte;

    float tempoCorte; // tCorte
    float tempoMax; // tMax

    // dist = distMax * (1 - exp(-distCoeficiente * t))
    float distMax;
    float distCoeficiente;
};

struct ModeloChuteCobertura
{
    float distPrimeiroPulo;
    float distSegundoPulo;
};

/**
 * @brief Estrutura com os atributos que caracterizam o robô
 *
 */
typedef struct Atributos
{
    int id; /**< ID do robô. */

    QVector2D vt2dPosicaoAtual;/**< Posição atual do robô [mm]. */
    QVector2D vt2dPosicaoAnterior;/**< Posição anterior do robô [mm]. */
    QVector2D vt2dDestino;/**< Posição de destino do robô [mm]. */
    QVector2D vt2dPontoAnguloDestino;/**< Ponto em que o robô deve estar olhando
                                       ao chegar no destino */
    float fAnguloBackupRobo; /**< Angulo de orientação que o robô está quando um
                               outro ângulo é atribuído como destino.
                               @details Isso serve para que o controlador de
                               rotação sempre enxergue o ângulo do robô variando
                               de 0 até o valor de setpoint. O mesmo é feito
                               para a posição em X e Y. @see MovimentacaoRobo,
                                                        Visao::fCalculaAnguloRobo*/
    float rotation; /**< Ângulo de orientação do robô [rad].
                        @details Esse ângulo varia de \f$ -\infty \f$ a
                        \f$ +\infty \f$, para que não ocorram descontinuidades
                        na variação do ângulo, como ocorre com o valor puro vindo
                        da visão, que vai de \f$ -\pi \f$ a \f$ +\pi \f$ .*/

    QVector2D vt2dVelocidade; /**< Velocidade em X e Y do robô no campo [m/s].*/
    double dVelocidade; /**< Módulo da velocidade em X e Y do robô no campo [m/s].*/
    double dTempoRobo; /**< Tempo do robô [s]. @details Isto era utilizado para
                                desenhar os gráficos de velocidade do robô.*/

    bool bDetectadoVisao;/**< Mostra se o robô está sendo detectado pela visão.*/
    bool bTitular; /**< True se o robô deve estar em campo. */
    bool bIgnorarBola; /**< Indica se o robô deve ignorar a bola como obstaculo ou não */

    bool kickSensor; /**< Contém o estado atual do sensor de chute. */
    float battery; /**< Contém o valor atual da bateria [V].  */
    QVector<double> dOdometriaRoda1; /**< Vetor com os valores de odometria da roda 1.*/
    QVector<double> dOdometriaRoda2; /**< Vetor com os valores de odometria da roda 2.*/
    QVector<double> dOdometriaRoda3; /**< Vetor com os valores de odometria da roda 3.*/
    QVector<double> dOdometriaRoda4; /**< Vetor com os valores de odometria da roda 4.*/

    ID_Jogadas jogadaAtual; /**< Jogada que o robô está executando. */
    QVector2D vt2dVelocidadeNaoConvertida; /**< Velocidade de comando do robô antes
                                    de decompor no sistema de referência do robô.
                                    @see MovimentacaoRobo::vt2dDecompoeVelocidadeRobo*/

    unsigned char radiotx_counter; /**< Conta quantos pacotes do rádio foram enviados
    para o robô. No décimo pacote solicita um pacote de resposta.*/

    // robot actuators
    float fMaxLinearVelocity; /**< Velocidade linear máxima permitida para o robô [m/s]. */
    float fMaxAngularVelocity; /**< Velocidade angular máxima permitida para o robô [rad/s]. */
    float cmd_vx; /**< Velocidade de comando no eixo X do robô [0 - 100].*/
    float cmd_vy; /**< Velocidade de comando no eixo Y do robô [0 - 100].*/
    float cmd_w; /**< Velocidade de comando no eixo de rotação do robô [0 - 100].*/
    int cmd_rollerspd; /**< Velocidade do roller [0 - 100]. */

    KICKTYPE kick; /**< Tipo do chute que o robô deve executar. @see #KICKTYPE */
    bool roller; /**< Ativa/Desativa o roller. */
    float kickStrength; /**< Força do chute caso seja algum chute **Custom**.
                            @see #KICKTYPE*/

    QString strFuncaoDestino; /**< Ultima função que atribuiu um destino para o robô. */
    QString strFuncaoJogada;  /**< Última função que atribuiu uma jogada ao robô. */

    QElapsedTimer etmControleKick; /**< Utilizado para controle de quando o robô
                                               chutou a bola pela última vez */

    QMap<float, ModeloChute> modcModelos;

    QMap<float, ModeloChuteCobertura> modelosChuteCobertura;
    /**
     * @brief Construtor dos atributos
     *
     */
    Atributos()
    {
        id = -1;

        fAnguloBackupRobo = 0;
        rotation = 0;

        dVelocidade=0;
        dTempoRobo=0;

        bDetectadoVisao = false;
        bTitular = false;
        bIgnorarBola = bNaoConsideraBola;

        kickSensor = false;
        battery = 0;

        jogadaAtual = Nenhuma;

        radiotx_counter = 0;

        // robot actuators
        fMaxLinearVelocity = 0;
        cmd_vx = 0;
        cmd_vy = 0;
        cmd_w = 0;
        cmd_rollerspd = 0;

        kick = KICK_NONE;
        roller = 0;
        kickStrength = 0;

        strFuncaoDestino = "";
        strFuncaoJogada = "";

        etmControleKick.start();
    }

    Atributos(const Atributos &_other)
    {
        id = _other.id;

        vt2dPosicaoAtual = _other.vt2dPosicaoAtual;
        vt2dPosicaoAnterior = _other.vt2dPosicaoAnterior;
        vt2dDestino = _other.vt2dDestino;
        vt2dPontoAnguloDestino = _other.vt2dPontoAnguloDestino;
        fAnguloBackupRobo = _other.fAnguloBackupRobo;
        rotation = _other.rotation;

        vt2dVelocidade = _other.vt2dVelocidade;
        dVelocidade = _other.dVelocidade;
        dTempoRobo = _other.dTempoRobo;

        bDetectadoVisao = _other.bDetectadoVisao;
        bTitular = _other.bTitular;
        bIgnorarBola = _other.bIgnorarBola;

        kickSensor = _other. kickSensor;
        battery = _other.battery;

        jogadaAtual = _other.jogadaAtual;
        vt2dVelocidadeNaoConvertida = _other.vt2dVelocidadeNaoConvertida;
        radiotx_counter = _other. radiotx_counter;

        // robot actuators
        fMaxLinearVelocity = _other.fMaxLinearVelocity;
        fMaxAngularVelocity = _other.fMaxAngularVelocity;
        cmd_vx = _other. cmd_vx;
        cmd_vy = _other.cmd_vy;
        cmd_w = _other.cmd_w;
        cmd_rollerspd = _other.cmd_rollerspd;

        kick = _other.kick;
        roller = _other.roller;
        kickStrength = _other.kickStrength;

        strFuncaoDestino = _other.strFuncaoDestino;
        strFuncaoJogada = _other.strFuncaoJogada;

        etmControleKick = _other.etmControleKick;
        modcModelos = _other.modcModelos;
        modelosChuteCobertura = _other.modelosChuteCobertura;
    }

} Atributos;


#endif // ATRIBUTOS_H
