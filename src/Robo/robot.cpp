/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "robot.h"
#include "qvector2d.h"

#include <QFile>

Robo::Robo(int _robotID)
{
    path.reset(new Path());
    strRole.clear();
    strTactic.clear();
    strSkill.clear();

    atributos.id = _robotID;
    atributos.bDetectadoVisao = false;
    atributos.bIgnorarBola = bNaoConsideraBola;
    atributos.jogadaAtual = Nenhuma; // Nenhuma jogada

    atributos.dTempoRobo = 0;
    atributos.dVelocidade = 0;

    atributos.kick = KICK_NONE;
    atributos.kickSensor = false;
    atributos.kickStrength = 0;
    atributos.roller = false;
    atributos.battery = 0;

    atributos.fMaxLinearVelocity = 0;
    atributos.cmd_vx = 0;
    atributos.cmd_vy = 0;
    atributos.cmd_w = 0;
    atributos.cmd_rollerspd = 0;

    atributos.fMaxAngularVelocity = globalConfig.robotVelocities.angular.max;

    atributos.vt2dPosicaoAtual = QVector2D(99999999.f, 99999999.f);
    atributos.vt2dPosicaoAnterior = QVector2D(0, 0);
    atributos.vt2dPontoAnguloDestino = QVector2D(0, 0);
    atributos.vt2dVelocidadeNaoConvertida = QVector2D(0, 0);
    atributos.vt2dVelocidade = QVector2D(0, 0);
    atributos.fAnguloBackupRobo = 0;

    atributos.strFuncaoDestino = "";
    atributos.strFuncaoJogada = "";

    predictedPosition = atributos.vt2dPosicaoAtual;

    vCarregaModeloChute(
        QString(":/modelo_chute/modelo_%1.txt").arg(strModeloForcaChute));
    vCarregaModeloChuteCobertura(
        QString(":/modelo_chute/modelo_%1_chip_kick.txt")
            .arg(strModeloForcaChute));
}

Robo::Robo(const Robo& outro)
{
    atributos = outro.atbRetornaRobo();
    path.reset(new Path(*outro.path.get()));
    strRole = outro.strGetRole();
    strTactic = outro.strGetTactic();
    strSkill = outro.strGetSkill();
    myZone.vConfigZone(outro.fzGetZone());

    predictedPosition = outro.getPrediction();

    prsbcConstraint = outro.prsbcConstraint;

    vCarregaModeloChute(
        QString(":/modelo_chute/modelo_%1.txt").arg(strModeloForcaChute));
    vCarregaModeloChuteCobertura(
        QString(":/modelo_chute/modelo_%1_chip_kick.txt")
            .arg(strModeloForcaChute));
}

void Robo::vSetaRobo(const Atributos& _atributos)
{
    this->atributos = _atributos;
}

void Robo::vAtualizaDadosVisao(const Robo* _other)
{
    atributos.vt2dPosicaoAtual = _other->vt2dPosicaoAtual();
    atributos.vt2dPosicaoAnterior = _other->vt2dPosicaoAnterior();
    atributos.rotation = _other->fAnguloAtual();

    atributos.bDetectadoVisao = _other->bRoboDetectado();

    atributos.dVelocidade = _other->dModuloVelocidade();
    atributos.vt2dVelocidade = _other->vt2dVelocidadeXY();
    atributos.dTempoRobo = _other->dTime();

    predictedPosition = _other->getPrediction();
}

void Robo::vAtualizaDadosEstrategia(const Atributos& _atributos)
{
    atributos.vt2dDestino = _atributos.vt2dDestino;
    atributos.vt2dPontoAnguloDestino = _atributos.vt2dPontoAnguloDestino;
    atributos.fAnguloBackupRobo = _atributos.fAnguloBackupRobo;
    atributos.bIgnorarBola = _atributos.bIgnorarBola;
    atributos.jogadaAtual = _atributos.jogadaAtual;
    atributos.roller = _atributos.roller;
    atributos.kickStrength = _atributos.kickStrength;
    atributos.kick = _atributos.kick;
    atributos.etmControleKick = _atributos.etmControleKick;
    atributos.cmd_rollerspd = _atributos.cmd_rollerspd;
    atributos.strFuncaoDestino = _atributos.strFuncaoDestino;
    atributos.strFuncaoJogada = _atributos.strFuncaoJogada;
    atributos.fMaxLinearVelocity = _atributos.fMaxLinearVelocity;
    atributos.fMaxAngularVelocity = _atributos.fMaxAngularVelocity;

    atributos.battery = _atributos.battery;
    atributos.kickSensor = _atributos.kickSensor;
    atributos.dOdometriaRoda1 = _atributos.dOdometriaRoda1;
    atributos.dOdometriaRoda2 = _atributos.dOdometriaRoda2;
    atributos.dOdometriaRoda3 = _atributos.dOdometriaRoda3;
    atributos.dOdometriaRoda4 = _atributos.dOdometriaRoda4;
}

void Robo::vAtualizaDadosEstrategia(const Robo* _other)
{
    vAtualizaDadosEstrategia(_other->atbRetornaRobo());
    strRole = _other->strGetRole();
    strTactic = _other->strGetTactic();
    strSkill = _other->strGetSkill();
    myZone.vConfigZone(_other->fzGetZone());
}

void Robo::vAtualizaDadosRadio(const RadioFeedback& _feedback)
{
    atributos.battery = _feedback.fGetBattery(iID());
    atributos.kickSensor = _feedback.bGetKickSensor(iID());

    atributos.dOdometriaRoda1.append(_feedback.iGetOdometry(iID(), 0));
    atributos.dOdometriaRoda2.append(_feedback.iGetOdometry(iID(), 1));
    atributos.dOdometriaRoda3.append(_feedback.iGetOdometry(iID(), 2));
    atributos.dOdometriaRoda4.append(_feedback.iGetOdometry(iID(), 3));

    if (atributos.dOdometriaRoda1.size() > 50)
        atributos.dOdometriaRoda1.pop_front();
    if (atributos.dOdometriaRoda2.size() > 50)
        atributos.dOdometriaRoda2.pop_front();
    if (atributos.dOdometriaRoda3.size() > 50)
        atributos.dOdometriaRoda3.pop_front();
    if (atributos.dOdometriaRoda4.size() > 50)
        atributos.dOdometriaRoda4.pop_front();
}

void Robo::vAtualizaDadosEstrategiaMovimentacao(const Robo* _robot)
{
    vAtualizaDadosEstrategia(_robot->atbRetornaRobo());
    strRole = _robot->strGetRole();
    strTactic = _robot->strGetTactic();
    strSkill = _robot->strGetSkill();
    myZone.vConfigZone(_robot->fzGetZone());

    QVector3D cmdVel = _robot->vt3dPegaVelocidades();
    atributos.cmd_vx = cmdVel.x();
    atributos.cmd_vy = cmdVel.y();
    atributos.cmd_w = cmdVel.z();
    atributos.vt2dVelocidadeNaoConvertida = _robot->vt2dGetRawCommandVelocity();

    path->vSetPath(_robot->path->getFullPath(), _robot->path->pathLength(),
                   false);
    path->vUpdatePath(_robot->path->getPath());

    prsbcConstraint = _robot->prsbcConstraint;
}

const Atributos& Robo::atbRetornaRobo() const
{
    return this->atributos;
}

void Robo::vSetaJogada(const ID_Jogadas _IDProcesso,
                       const QString funcaoDeChamada)
{
    atributos.strFuncaoJogada = funcaoDeChamada + "()";
    if (atributos.bDetectadoVisao == true)
    {
        atributos.jogadaAtual = _IDProcesso;
    }
}

void Robo::vSetaDestino(const QVector2D _destino, const QString funcaoDeChamada)
{
    if (atributos.vt2dDestino.distanceToPoint(_destino) > 10)
    {
        path->clear();
    }
    else
    {
        path->updateTarget(_destino);
    }
    atributos.vt2dDestino = _destino;
    atributos.strFuncaoDestino = funcaoDeChamada + "()";
}

void Robo::vSetaPosicao(const QVector2D _vt2dPos)
{
    atributos.vt2dPosicaoAnterior = atributos.vt2dPosicaoAtual;
    atributos.vt2dPosicaoAtual = _vt2dPos;

    //    qDebug() << "Setou a posição" << QString::fromStdString(str);
}

void Robo::vSetaAngulo(const float _dAngulo)
{
    atributos.rotation = _dAngulo;
}

void Robo::vSetaPontoAnguloDestino(const QVector2D _pontoAngulo)
{
    atributos.vt2dPontoAnguloDestino = _pontoAngulo;
    atributos.fAnguloBackupRobo = atributos.rotation;
}

void Robo::vSetaIgnorarBola(const bool _bIgnorarBola)
{
    atributos.bIgnorarBola = _bIgnorarBola;
}

void Robo::vSetMaxLinearVelocity(float v, const QString funcaoDeChamada)
{
    // Apenas o goleiro pode ter uma velocidade maior que
    // `dVelocidadeMaximaRobo`
    if (v > globalConfig.robotVelocities.linear.limit + 0.1 && atributos.jogadaAtual != Robo_Goleiro)
    {
        v = globalConfig.robotVelocities.linear.limit;
        //         qWarning() << "Velocidade máxima permitida de " <<
        //         dVelocidadeMaximaRobo << "| Funcao: " << funcaoDeChamada;
    }

    atributos.fMaxLinearVelocity = v;
}

void Robo::vSetMaxAngularVelocity(float w)
{
    if (w > globalConfig.robotVelocities.angular.max)
    {
        //         qWarning() << QString("[Robo %1] Tentando atribuir uma
        //         velocidade maior que o limite real do robô (%2 > %3)")
        //             .arg(atributos.id)
        //             .arg(w)
        //             .arg(dRotacaoMaximaReal);
        w = globalConfig.robotVelocities.angular.max;
    }
    if (w > globalConfig.robotVelocities.angular.max)
    {
        //         qWarning() << QString("[Robo %1] Tentando atribuir uma
        //         velocidade maior que o limite especificado (%2 > %3)")
        //             .arg(atributos.id)
        //             .arg(w)
        //             .arg(dRotacaoMaximaRobo);
        w = globalConfig.robotVelocities.angular.max;
    }

    atributos.fMaxAngularVelocity = w;
}

bool Robo::bPodeChutar(const QVector2D _vt2dMira,
                       const float AnguloMaximo) const
{
    QVector2D vetorRobo =
        QVector2D(qCos(atributos.rotation), qSin(atributos.rotation));

    // Calcula o ângulo entre a direção fornecida e a direção que o robô está
    // olhando.
    double dAnguloBola =
        abs((180 / M_PI) * qAcos(vetorRobo.dotProduct(vetorRobo, _vt2dMira) /
                                 (vetorRobo.length() * _vt2dMira.length())));
    return (dAnguloBola < AnguloMaximo);
}

KICKTYPE Robo::kickChuteAtual() const
{
    return atributos.kick;
}

bool Robo::bRoboDetectado() const
{
    return atributos.bDetectadoVisao;
}

void Robo::vLigaRoller(const int _velRoller)
{
    atributos.roller = true;
    atributos.cmd_rollerspd = _velRoller;
}

void Robo::vDesligaRoller()
{
    atributos.roller = false;
    atributos.cmd_rollerspd = 0;
}

void Robo::vChutar(const KICKTYPE _kick, const float _forca,
                   const int _distancia, const int _anguloMax)
{
    atributos.kick = _kick;
    atributos.kickStrength = _forca;

    // Reinicia o timer de controle quando o robô chuta.
    if (_kick != KICK_NONE)
    {
        atributos.etmControleKick.restart();
    }

    if (_kick == KICK_CUSTOM || _kick == CHIP_KICK_CUSTOM)
    {
        if (_forca < 0)
        {
            qCritical() << "Robô tentando chutar com força negativa (" << _forca
                        << ").";

            atributos.kickStrength = 0;
        }
        else
        {
            atributos.kickStrength = _forca;
        }
    }

    // if(atributos.kickStrength < 10)
    // {
    //     atributos.kickStrength *= globalConfig.calibratedKickForce / 6.0;
    // }

    // if(_kick >= ANGLE_KICK_SOFT)//Se for chute direcional
    // {
    //     int kickAngle = abs(_angulo);
    //
    //     // Ajusta o valor do angulo no slider para alguma das regiões,
    //     // neste caso esta sendo considerado 4 regiões
    //
    //     if(kickAngle >= 0 && kickAngle < _anguloMax/3)
    //         kickAngle = 0; //primeira região
    //     else if(kickAngle >= _anguloMax/3 && kickAngle < _anguloMax*2/3)
    //         kickAngle = 1;//segunda região
    //     else if(kickAngle >= _anguloMax*2/3 && kickAngle < _anguloMax)
    //         kickAngle = 2;//terceira região
    //     else
    //         kickAngle = 3;//quarta região
    //
    //     // Máscara para definir se o angulo é positivo ou negativo
    //     if(_angulo < 0)kickAngle = kickAngle | 0x04;
    //
    //     // O angulo do chute é passado no campo da força de chute
    //     atributos.kickStrength = kickAngle;
    // }

    // if(_distancia < 4e3)
    // {
    //     atributos.kickStrength = 0.5*globalConfig.calibratedKickForce;
    // }
    // else
    // {
    // }
    // atributos.kickStrength = kickStrength;
}

void Robo::vAndarXY(const float _VelX, const float _VelY, const float _VelRot,
                    const float _VelMaxima, const QVector2D _velNotDecomp)
{
    atributos.cmd_vx = _VelX;
    atributos.cmd_vy = _VelY;
    atributos.cmd_w = _VelRot;

    if (qIsNaN(atributos.cmd_vx))
    {
        atributos.cmd_vx = 0;
    }
    if (qIsNaN(atributos.cmd_vy))
    {
        atributos.cmd_vy = 0;
    }
    if (qIsNaN(atributos.cmd_w))
    {
        atributos.cmd_w = 0;
    }

    atributos.vt2dVelocidadeNaoConvertida = _velNotDecomp;
    vSetMaxLinearVelocity(_VelMaxima);
}

void Robo::vStay()
{
    vSetaDestino(atributos.vt2dPosicaoAtual);
    vSetaIgnorarBola(bNaoConsideraBola);
}

float Robo::fMinhaBateria() const
{
    return atributos.battery;
}

QVector2D Robo::vt2dDestino() const
{
    return atributos.vt2dDestino;
}

bool Robo::bArrived(float _threshold) const
{
    const float dist =
        atributos.vt2dDestino.distanceToPoint(atributos.vt2dPosicaoAtual);
    return dist <= _threshold && atributos.dVelocidade < 0.2 &&
           vt3dPegaVelocidades().toVector2D().length() < 0.3;
}

QVector2D Robo::vt2dPosicaoAtual() const
{
    return atributos.vt2dPosicaoAtual;
}

QVector2D Robo::vt2dPosicaoAnterior() const
{
    return atributos.vt2dPosicaoAnterior;
}

QVector2D Robo::vt2dPontoAnguloDestino() const
{
    return atributos.vt2dPontoAnguloDestino;
}

QVector3D Robo::vt3dPegaVelocidades() const
{
    return {atributos.cmd_vx, atributos.cmd_vy, atributos.cmd_w};
}

ID_Jogadas Robo::jJogadaAtual() const
{
    return atributos.jogadaAtual;
}

bool Robo::bPegaIgnorarBola() const
{
    return atributos.bIgnorarBola;
}

float Robo::fAnguloBackup() const
{
    return atributos.fAnguloBackupRobo;
}

float Robo::fAnguloAtual() const
{
    return atributos.rotation;
}

QVector2D Robo::vt2dOrientation() const
{
    return QVector2D(cosf(atributos.rotation), sinf(atributos.rotation));
}

qint8 Robo::iID() const
{
    return static_cast<qint8>(atributos.id);
}

void Robo::setVelocity(const QVector2D& _vel)
{
    atributos.vt2dVelocidade = _vel;
}

QVector2D Robo::vt2dVelocidadeXY() const
{
    return atributos.vt2dVelocidade;
}

double Robo::dModuloVelocidade() const
{
    return atributos.dVelocidade;
}

float Robo::fMaxLinearVelocity() const
{
    return atributos.fMaxLinearVelocity;
}

float Robo::fMaxAngularVelocity() const
{
    return atributos.fMaxAngularVelocity;
}

int Robo::iVelocidadeRoller() const
{
    return atributos.cmd_rollerspd;
}

void Robo::vAtualizaBateria(const float bat)
{
    atributos.battery = bat;
}

void Robo::vSetaEmCampo(const bool emCampo)
{
    atributos.bDetectadoVisao = emCampo;
}

void Robo::vAdicionaTempo(const double tempo)
{
    atributos.dTempoRobo += tempo;
    if (atributos.dTempoRobo > 9.6)
    {
        atributos.dTempoRobo = 0;
    }
}

double Robo::dTime() const
{
    return atributos.dTempoRobo;
}

QVector2D Robo::vt2dPontoDirecaoMira() const
{
    const float fAnguloRobo = atributos.rotation;

    return (vt2dPosicaoAtual() +
            100 * QVector2D(qCos(fAnguloRobo), qSin(fAnguloRobo)));
}

QVector2D Robo::vt2dAimDirectionNormalized() const
{
    const float fAnguloRobo = atributos.rotation;
    return QVector2D(qCos(fAnguloRobo), qSin(fAnguloRobo));
}

void Robo::vSetaModuloVelocidade(const float vel)
{
    atributos.dVelocidade = vel;
}

void Robo::vSetaKickSensor(const bool sensor)
{
    atributos.kickSensor = sensor;
}

bool Robo::bKickSensor() const
{
    return atributos.kickSensor;
}

bool Robo::bChutePermitido() const
{
    return fTempoDesdeUltimoChute() > iTempoRecargaChute;
}

float Robo::fTempoDesdeUltimoChute() const
{
    return atributos.etmControleKick.nsecsElapsed() * 1e-9f;
}

float Robo::fForcaChuteDistanciaVelocidade(const float fDistanciaMilimetros,
                                           const float fVelChegada)
{
    // Distância do chute
    const float fDistancia = fDistanciaMilimetros * 1e-3;

    float forcaBaixa = -1;
    float forcaAlta = 10000000;

    float velChegadaBaixa = -999999999999.f;
    float velChegadaAlta = 999999999999.f;

    for (auto x = atributos.modcModelos.constBegin();
         x != atributos.modcModelos.constEnd(); ++x)
    {
        const ModeloChute& md = x.value();

        const float tempoChegada = 1 / md.distCoeficiente *
                                   logf(md.distMax / (md.distMax - fDistancia));

        const float velChegadaAtual =
            tempoChegada < md.tempoCorte
                ? md.velMax -
                      tempoChegada * (md.velMax - md.velCorte) / md.tempoCorte
                : md.velCorte * md.tempoMax / (md.tempoMax - md.tempoCorte) -
                      tempoChegada * md.velCorte /
                          (md.tempoMax - md.tempoCorte);

        if (velChegadaAtual > 0 && velChegadaAtual < fVelChegada &&
            velChegadaAtual > velChegadaBaixa)
        {
            forcaBaixa = x.key();
            velChegadaBaixa = velChegadaAtual;
        }
        else if (velChegadaAtual > 0 && velChegadaAtual > fVelChegada &&
                 velChegadaAtual < velChegadaAlta)
        {
            forcaAlta = x.key();
            velChegadaAlta = velChegadaAtual;
        }
    }

    // Não consegui achar nenhuma força adequada, chuta com a força padrão mesmo
    if (forcaBaixa < 0 && forcaAlta > 1000)
    {
        return (float)globalConfig.calibratedKickForce;
    }
    else if (forcaBaixa < 0)
    {
        return qMin((float)globalConfig.calibratedKickForce, forcaAlta);
    }
    else if (forcaAlta > 1000)
    {
        return qMin((float)globalConfig.calibratedKickForce, forcaBaixa);
    }
    else if ((fVelChegada - velChegadaBaixa) > (velChegadaAlta - fVelChegada))
    {
        // Interpola para achar o melhor valor

        const float forca = (fVelChegada - velChegadaBaixa) *
                                (forcaAlta - forcaBaixa) /
                                (velChegadaAlta - velChegadaBaixa) +
                            forcaBaixa;

        return qMin((float)globalConfig.calibratedKickForce, forca);

        //        qDebug() << "Força: " << forca;
    }
    else
    {
        // Interpola para achar o melhor valor

        const float forca = forcaAlta - (velChegadaAlta - fVelChegada) *
                                            (forcaAlta - forcaBaixa) /
                                            (velChegadaAlta - velChegadaBaixa);

        return qMin((float)globalConfig.calibratedKickForce, forca);

        //        qDebug() << "Força: " << forca;
    }
}

void Robo::vChutarDistanciaVelocidade(const float fDistanciaMilimetros,
                                      const float fVelChegada,
                                      const KICKTYPE kicktype)
{
    vChutar(kicktype,
            fForcaChuteDistanciaVelocidade(fDistanciaMilimetros, fVelChegada));
}

void Robo::vChutarCoberturaDistancia(const float fDistanciaMilimetros)
{
    const float distancia = fDistanciaMilimetros * 1e-3;

    float forcaBaixa = -1;
    float forcaAlta = 10000000;

    float distanciaBaixa = -1;
    float distanciaAlta = 999999999999.f;

    for (auto x = atributos.modelosChuteCobertura.constBegin();
         x != atributos.modelosChuteCobertura.constEnd(); ++x)
    {
        const ModeloChuteCobertura& md = x.value();
        //        const float d = md.distPrimeiroPulo + md.distSegundoPulo;
        const float d = (md.distPrimeiroPulo + md.distSegundoPulo) * 1.2;
        //        const float d = md.distPrimeiroPulo;
        const float f = x.key();

        if (d > distanciaBaixa && d < distancia)
        {
            distanciaBaixa = d;
            forcaBaixa = f;
        }

        if (d < distanciaAlta && d > distancia)
        {
            distanciaAlta = d;
            forcaAlta = f;
        }
    }

    // Não consegui achar nenhuma força adequada, chuta com a força padrão mesmo
    if (forcaBaixa < 0 && forcaAlta > 1000)
    {
        vChutar(CHIP_KICK_STRONG);
    }
    else if (forcaBaixa < 0)
    {
        vChutar(CHIP_KICK_CUSTOM,
                qMin(forcaAlta, (float)FORCA_CHIP_KICK_STRONG));
    }
    else if (forcaAlta > 1000)
    {
        vChutar(CHIP_KICK_CUSTOM, forcaBaixa);
    }
    else if ((distancia - distanciaBaixa) > (distanciaAlta - distancia))
    {
        // Interpola para achar o melhor valor

        const float forca = (distancia - distanciaBaixa) *
                                (forcaAlta - forcaBaixa) /
                                (distanciaAlta - distanciaBaixa) +
                            forcaBaixa;

        vChutar(CHIP_KICK_CUSTOM, qMin((float)FORCA_CHIP_KICK_STRONG, forca));
    }
    else
    {
        const float forca = forcaAlta - (distanciaAlta - distancia) *
                                            (forcaAlta - forcaBaixa) /
                                            (distanciaAlta - distanciaBaixa);

        vChutar(CHIP_KICK_CUSTOM, qMin((float)FORCA_CHIP_KICK_STRONG, forca));
    }

    //    vChutar(CHIP_KICK_CUSTOM, qMin((float)FORCA_MAXIMA_CHIP_KICK_CUSTOM,
    //    forcaBaixa));
}

void Robo::vDesligaChute()
{
    vChutar(KICK_NONE);
}

QVector2D Robo::vt2dGetRawCommandVelocity() const
{
    return atributos.vt2dVelocidadeNaoConvertida;
}

void Robo::vSetRole(QString _role)
{
    strRole = _role;
}

QString Robo::strGetRole() const
{
    return strRole;
}

void Robo::vSetTactic(QString _tactic)
{
    strTactic = _tactic;
}

QString Robo::strGetTactic() const
{
    return strTactic;
}

void Robo::vSetSkill(QString _skill)
{
    strSkill = _skill;
}

QString Robo::strGetSkill() const
{
    return strSkill;
}

void Robo::vSetZone(const FieldZone& _zone)
{
    myZone.vConfigZone(_zone);
}

FieldZone Robo::fzGetZone() const
{
    return myZone;
}

QPolygonF Robo::polGetRoller() const
{
    const int rollerWidth = 63, robotToRollerTip = 25; // 45
    const QVector2D angle = QVector2D(qCos(atributos.rotation),
                                      qSin(atributos.rotation)),
                    beginY = atributos.vt2dPosicaoAtual + angle * 50,
                    endY = atributos.vt2dPosicaoAtual +
                           angle * (50 + robotToRollerTip),
                    rollerVecLeft = angle * 23,
                    rollerVecRight =
                        angle *
                        (rollerWidth - rollerVecLeft.length()); // 48 - 12 = 36

    const QVector2D leftFront = Auxiliar::vt2dRotaciona(
                        beginY, beginY + rollerVecLeft, 90),
                    rightFront = Auxiliar::vt2dRotaciona(
                        beginY, beginY + rollerVecRight, -90),
                    leftOutter =
                        Auxiliar::vt2dRotaciona(endY, endY + rollerVecLeft, 90),
                    rightOutter = Auxiliar::vt2dRotaciona(
                        endY, endY + rollerVecRight, -90);

    const QPolygonF rollerPoly({leftFront.toPointF(), leftOutter.toPointF(),
                                rightOutter.toPointF(), rightFront.toPointF()});

    return rollerPoly;
}

QVector2D Robo::vt2dRollerCenter() const
{
    const QList<QPointF> rollerPoints = polGetRoller().toList();
    QVector2D center;
    center.setX((rollerPoints.at(1).x() + rollerPoints.at(2).x()) / 2);
    center.setY((rollerPoints.at(1).y() + rollerPoints.at(2).y()) / 2);

    return center;
}

QVector2D Robo::vt2dPlaceRollerAt(const QVector2D& _target) const
{
    QVector2D vec = vt2dPosicaoAtual() - vt2dRollerCenter();
    return _target + vec;
}

void Robo::setPrediction(QVector2D &_pos)
{
    predictedPosition = _pos;
}

QVector2D Robo::getPrediction() const
{
    return predictedPosition;
}

void Robo::vCarregaModeloChute(const QString& caminhoModelo)
{
    return;
    QFile fileModelo(caminhoModelo);

    if (!fileModelo.open(QIODevice::ReadOnly))
    {
        qFatal("%s", qUtf8Printable("Não foi possível abrir arquivo " +
                                    fileModelo.fileName() + ' ' +
                                    fileModelo.errorString()));
    }

    // 7 para o simulador da ER-Force e 10 para o simulador grSim
    const int nModelos = fileModelo.fileName().contains("erforce") ? 7 : 10;

    for (int i = 0; i < nModelos; ++i)
    {

        // ######################################
        fileModelo.readLine();
        // forcaChute
        // FIXME: Memory leak
        ModeloChute& model =
            atributos.modcModelos[fileModelo.readLine().toFloat()];
        //
        fileModelo.readLine();

        model.velMax = fileModelo.readLine().toFloat();
        model.velCorte = fileModelo.readLine().toFloat();

        model.tempoCorte = fileModelo.readLine().toFloat();
        model.tempoMax = fileModelo.readLine().toFloat();

        model.distMax = fileModelo.readLine().toFloat();
        model.distCoeficiente = fileModelo.readLine().toFloat();
    }
}

void Robo::vCarregaModeloChuteCobertura(const QString& caminhoModelo)
{
    return;
    QFile fileModelo(caminhoModelo);

    if (!fileModelo.open(QIODevice::ReadOnly))
    {
        // qFatal("%s", qUtf8Printable("Não foi possível abrir arquivo " +
        // fileModelo.fileName() + ' ' + fileModelo.errorString()));
        return;
    }

    const int nModelos = 7;

    for (int i = 0; i < nModelos; ++i)
    {
        // ######################################
        fileModelo.readLine();
        // forcaChute
        // FIXME: Memory leak
        ModeloChuteCobertura& model =
            atributos.modelosChuteCobertura[fileModelo.readLine().toFloat()];
        //
        fileModelo.readLine();

        QString strDistancias = fileModelo.readLine();
        strDistancias.remove(" ", Qt::CaseInsensitive);
        QStringList listDistancias = strDistancias.split(';');

        model.distPrimeiroPulo = listDistancias.at(0).toFloat();
        model.distSegundoPulo = listDistancias.at(1).toFloat();
    }
}
