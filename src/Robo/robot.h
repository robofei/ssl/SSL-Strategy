/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file robot.h
 * @brief Arquivo header da classe do robô
 * @author Leonardo da Silva Costa e outros
 * @version 1.0
 * @date 2020-08-26
 */

#ifndef ROBOT_H
#define ROBOT_H

#include <QDebug>
#include <QPolygonF>

#include "Ambiente/fieldzone.h"
#include "atributos.h"
#include "Path_Planning/path.h"
#include "Radio/radiofeedback.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include <iostream>

#define FORCA_CHIP_KICK_STRONG 5.5

/**
 * @brief Classe que representa um robô aliado ou oponente
 *
 */
class Robo
{

public:
    QScopedPointer<Path> path;
    Eigen::MatrixXd prsbcConstraint;

    /**
     * @brief Construtor da classe
     *
     * @param _robotID - ID do robô para inicializar
     */
    Robo(int _robotID = 0);

    /**
     * @brief Destrutor da classe
     *
     */
    ~Robo()=default;

    /**
     * @brief Construtor de cópia
     *
     * @param outro - Objeto do outro robô a ser utilizado para cópia
     */

    Robo(const Robo& outro);

    /**
     * @brief Seta os atributos do robô
     *
     * @param _atributos - Novos atributos
     */
    void vSetaRobo(const Atributos &_atributos);

    /**
     * @brief Atualiza os dados do robô que dependem da visão.
     * @see Visao
     * @param _other - Dados do outro robô
     */
    void vAtualizaDadosVisao(const Robo *_other);

    /**
     * @brief Atualiza os dados do robô que dependem da estratégia.
     * @see clsEstrategia
     * @param _atributos - Atributos atualizados.
     */
    void vAtualizaDadosEstrategia(const Atributos &_atributos);
    void vAtualizaDadosEstrategia(const Robo *_other);

    /**
     * @brief Atualiza os dados recebidos pelo rádio.
     * @see RadioBase
     * @param _feedback - Dados de feedback
     */
    void vAtualizaDadosRadio(const RadioFeedback &_feedback);

    /**
     * @brief Atualiza os dados do robô que dependem da estratégia e da movimentação.
     * @details Essa função é basicamente um incremento da vAtualizaDadosEstrategia
     * e da vAtualizaDadosRadio, para atualizar os dados dos robôs na thread da interface.
     * @param _robot - Robô com os dados atualizados
     */
    void vAtualizaDadosEstrategiaMovimentacao(const Robo *_robot);

    /**
     * @brief Retorna uma referência constante para consultar os atributos do robô
     * @return Atributos
     */
    const Atributos& atbRetornaRobo() const;

    /**
     * @brief Seta a jogada atual do robô
     *
     * @param _IDProcesso - ID da jogada conforme #ID_Jogadas
     * @note O segundo parâmetro **funcaoDeChamada** não deve ser fornecido ao
     * chamar esta função, ele serve para detectar qual foi a função que trocou
     * a jogada do robô, isso serve para ajudar a debugar
     */
    void vSetaJogada(const ID_Jogadas _IDProcesso,
                     const QString funcaoDeChamada = QString(__builtin_FUNCTION()));

    /**
     * @brief Seta a posição de destino do robô
     *
     * @param _destino - Posição de destino
     * @note O segundo parâmetro **funcaoDeChamada** não deve ser fornecido ao
     * chamar esta função, ele serve para detectar qual foi a função que trocou
     * a jogada do robô, isso serve para ajudar a debugar
     */
    void vSetaDestino(const QVector2D _destino,
                      const QString funcaoDeChamada = QString(__builtin_FUNCTION()));

    /**
     * @brief Atualiza a posição anterior do robô e seta a posição atual
     *
     * @param _vt2dPos - Nova posição atual
     */
    void vSetaPosicao(const QVector2D _vt2dPos);

    /**
     * @brief Seta o ângulo de rotação atual do robô
     * @details Detalhes de como o ângulo de rotação funciona podem ser vistos em
     * Visao::fCalculaAnguloRobo e Atributos::rotation
     *
     * @param _dAngulo - Novo ângulo de rotação [rad]
     */
    void vSetaAngulo(const float _dAngulo);

    /**
     * @brief Seta a posição em que o robô deve olhar
     * @details Isso serve para controlar a rotação do robô, é mais simples
     * definir um ponto para o robô olhar do que o ângulo diretamente
     *
     * @param _pontoAngulo - Posição que o robô deve olhar
     */
    void vSetaPontoAnguloDestino(const QVector2D _pontoAngulo);

    /**
     * @brief Seta o flag de ignorar a bola
     *
     * @param _bIgnorarBola - Indica se o robô deve ou não ignorar a colisão com
     * a bola no seu trajeto
     */
    void vSetaIgnorarBola(const bool _bIgnorarBola);

    /**
     * @brief Seta a velocidade linear máxima do robô.
     * @param v - Velocidade máxima [m/s]
     */
    void vSetMaxLinearVelocity(float v, const QString funcaoDeChamada = QString(__builtin_FUNCTION()));

    /**
     * @brief Seta a velocidade angular máxima.
     *
     * @param w - Velocidade máxima [rad/s]
     */
    void vSetMaxAngularVelocity(float w);

    /**
     * @brief Liga o roller com uma determinada velocidade
     *
     * @param _velRoller - Velocidade do roller [0-100]
     */
    void vLigaRoller(const int _velRoller);

    /**
     * @brief Desliga o roller
     *
     */
    void vDesligaRoller();

    /**
     * @brief Faz com que o robô chute quando a bola estiver no sensor de chute
     *
     * @param _kick - Tipo do chute
     * @param _forca - Força do chute caso seja algum chute do tipo **Custom**
     * @param _angulo - **Para o chute direcional apenas** Ângulo desejado de chute
     * @param _anguloMax - **Para o chute direcional apenas** Ângulo máximo do chute
     * @see KICKTYPE
     *
     * @note Até o momento não jogamos com nenhum robô com chute direcional, porém,
     * o que foi pensado na época em que esse código foi feito é que o firmware do
     * robô seria calibrado para chutar em 4 regiões. Por exemplo, se a escursão
     * máxima da chuteira é 16 graus, seria possível escolher entre 4 regiões de
     * 4 graus para chutar. O que essa função faz é ajustar o ângulo desejado
     * (\a _angulo) em uma dessas 4 regiões
     */
    void vChutar(const KICKTYPE _kick, const float _forca = 0,
                 const  int _distancia = 10e4, const int _anguloMax = 0);

    /**
     * @brief Seta as velocidade X, Y, rotação e máxima para o robô andar
     *
     * @param _VelX - Velocidade no **eixo X do robô**
     * @param _VelY - Velocidade no **eixo Y do robô**
     * @param _VelRot - Velocidade no **eixo Z do robô**
     * @param _VelMaxima - Velocidade máxima [0 - 100]
     * @param _velNotDecomp - Velocidade não decomposta
     */
    void vAndarXY(const float _VelX, const float _VelY,
            const float _VelRot, const float _VelMaxima,
            const QVector2D _velNotDecomp = {0,0});

    /**
     * @brief Faz o robô ficar na própria posição
     */
    void vStay();

    /**
     * @brief Retorna o nível da bateria do robô
     * @see Atributos::battery
     *
     * @return float - Valor da bateria
     */
    float fMinhaBateria()const;

    /**
     * @brief Retorna a posição de destino do robô
     * @see Atributos::vt2dDestino
     *
     * @return QVector2D - Posição de destino
     */
    QVector2D vt2dDestino()const;

    /**
     * @brief Retorna true se o robô chegou ao seu destino
     *
     * @return
     */
    bool bArrived(float _threshold = 30.0) const;

    /**
     * @brief Retorna a posição atual do robô
     * @see Atributos::vt2dPosicaoAtual
     *
     * @return QVector2D - Posição atual
     */
    QVector2D vt2dPosicaoAtual()const;

    /**
     * @brief Retorna a posição anterior do robô
     * @see Atributos::vt2dPosicaoAnterior
     *
     * @return QVector2D - Posição anterior
     */
    QVector2D vt2dPosicaoAnterior()const;

    /**
     * @brief Retorna o ponto de orientação destino do robô
     * @see Atributos::vt2dPontoAnguloDestino
     *
     * @return QVector2D - Ponto de orientação destino
     */
    QVector2D vt2dPontoAnguloDestino()const;

    /**
     * @brief Retorna as velocidades **de comando** em X, Y e W do robô
     * @details São as velocidade setadas pelo método #vAndarXY
     * @see Atributos::cmd_vx, Atributos::cmd_vy, Atributos::cmd_w
     *
     * @return QVector3D - Velocidade X, Y e W [0 - 100]
     */
    QVector3D vt3dPegaVelocidades()const;

    /**
     * @brief Retorna a jogada atual do robô
     * @see Atributos::jogadaAtual
     *
     * @return ID_Jogadas
     */
    ID_Jogadas jJogadaAtual()const;

    /**
     * @brief Retorna o flag de ignorar a bola
     * @see Atributos::bIgnorarBola
     *
     * @return bool
     */
    bool bPegaIgnorarBola()const;

    /**
     * @brief Retorna o ângulo de backup do robô
     * @see Atributos::fAnguloBackupRobo
     *
     * @return float
     */
    float fAnguloBackup()const;

    /**
     * @brief Retorna true se o robô pode chutar na direção fornecida
     * @details Essa função checa se o robô está na orientação certa para chutar
     * na direção dada
     *
     * @param _vt2dMira - Vetor da direção em que o robô tem que chutar, por exemplo,
     * PosicaoDeMira - PosicaoInicialBola
     *
     * @param AnguloMaximo - Ângulo máximo de variação possível da mira [graus]
     *
     * @return bool - True se o robô está alinhado e pode chutar, senão, false
     */
    bool bPodeChutar(const QVector2D _vt2dMira, const float AnguloMaximo = 7)const;

    /**
     * @brief Retorna o tipo de chute atual do robô.
     *
     * @return #KICKTYPE
     */
    KICKTYPE kickChuteAtual()const;

    /**
     * @brief Retorna true se o robô está sendo detectado pela visão
     * @see Atributos::bDetectadoVisao
     *
     * @return bool
     */
    bool bRoboDetectado()const;

    /**
     * @brief Retorna o ângulo de orientação do robô
     * @see Atributos::rotation
     *
     * @return float - Ângulo de orientação do robô
     */
    float fAnguloAtual()const;

    /**
     * @brief Retorna o vetor de orientação do robô
     *
     * @return
     */
    QVector2D vt2dOrientation() const;

    /**
     * @brief Retorna o ID do robô
     * @return int - ID
     */
    qint8 iID()const;

    /**
     * @brief Seta as velocidades X e Y do robôs medidas pela visão.
     *
     * @param _vel
     */
    void setVelocity(const QVector2D &_vel);

    /**
     * @brief Retorna as velocidades X e Y do robô
     * @see Atributos::vt2dVelocidade
     *
     * @return QVector2D - Velocidade X, Y [m/s]
     */
    QVector2D vt2dVelocidadeXY()const;

    /**
     * @brief Retorna o módulo da velocidade do robô
     * @see Atributos::dVelocidade
     *
     * @return double - Módulo da velocidade [m/s]
     */
    double dModuloVelocidade()const;

    /**
     * @brief Retorna a velocidade linear máxima do robô
     *
     * @return float - Velocidade máxima [m/s]
     */
    float fMaxLinearVelocity()const;

    /**
     * @brief Retorna a velocidade angular máxima do robô
     *
     * @return float - Velocidade máxima [rad/s]
     */
    float fMaxAngularVelocity()const;

    /**
     * @brief Retorna a velocidade do roller
     * @see Atributos::cmd_rollerspd
     *
     * @return int - Velocidade do roller [0 - 100]
     */
    int iVelocidadeRoller()const;

    /**
     * @brief Atualiza o valor da bateria do robô
     *
     * @param bat - Novo valor da bateria
     */
    void vAtualizaBateria(const float bat);

    /**
     * @brief Seta se o robô está sendo detectado pela visão
     *
     * @param emCampo - True se está sendo detectado, senão, false
     */
    void vSetaEmCampo(const bool emCampo);

    /**
     * @brief Soma um tempo no tempo do robô
     * @see Atributos::dTempoRobo
     *
     * @param tempo - Incremento de tempo [s]
     */
    void vAdicionaTempo(const double tempo);

    /**
     * @brief Retorna o tempo do robô.
     *
     * @return
     */
    double dTime() const;

    /**
     * @brief Retorna o ponto da direção em que o robô está olhando
     * @details Esse ponto está sempre à uma distância fixa de 100mm do robô
     *
     * @return QVector2D - Ponto da direção
     */
    QVector2D vt2dPontoDirecaoMira() const;

    /**
     * @brief Ponto da direção da mira do robô normalizado.
     *
     * Ou seja:
     *  v  = vt2dAimDirectionNormalized - vt2dPosicaoAtual -> |v| = 1
     *
     * @return
     */
    QVector2D vt2dAimDirectionNormalized() const;

    /**
     * @brief Seta o módulo da velocidade do robô no campo
     *
     * @param vel - Módulo da velocidade [m/s]
     */
    void vSetaModuloVelocidade(const float vel);

    /**
     * @brief Seta o estado do sensor de chute do robô
     *
     * @param sensor - True se o sensor estiver ativado, senão, false
     */
    void vSetaKickSensor(const bool sensor);

    /**
     * @brief Retorna o estado do sensor de chute do robô
     * @see Atributos::kickSensor
     *
     * @return bool - Estado do sensor de chute
     */
    bool bKickSensor() const;

    /**
     * @brief bChutePermitido
     * @return
     */
    bool bChutePermitido()const;

    /**
     * @brief fTempoDesdeUltimoChute
     * @return
     */
    float fTempoDesdeUltimoChute()const;

    float fForcaChuteDistanciaVelocidade(const float fDistanciaMilimetros, const float fVelChegada);

    /**
     * @brief vChutarDistanciaVelocidade
     * @param fDistanciaMilimetros
     * @param fVelocidadeChegada
     * @param kicktype
     */
    void vChutarDistanciaVelocidade(const float fDistanciaMilimetros, const float fVelChegada,
                                    const KICKTYPE kicktype = KICK_CUSTOM);

    /**
     * @brief vChutarCoberturaDistancia
     * @param fDistanciaMilimetros
     */
    void vChutarCoberturaDistancia(const float fDistanciaMilimetros);

    /**
     * @brief Equivalente a `vChutar(KICK_NONE);`
     */
    void vDesligaChute();

    /**
     * @brief Retorna a vt2dVelocidadeNaoConvertida
     *
     * @return
     */
    QVector2D vt2dGetRawCommandVelocity() const;

    void vSetRole(QString _role);

    QString strGetRole() const;

    void vSetTactic(QString _tactic);

    QString strGetTactic() const;

    void vSetSkill(QString _skill);

    QString strGetSkill() const;

    void vSetZone(const FieldZone &_zone);

    FieldZone fzGetZone() const;

    QPolygonF polGetRoller() const;

    QVector2D vt2dRollerCenter() const;

    /**
     * @brief Retorna a posição que o robô tem que estar para que o centro do
     * roller esteja na posição desejada.
     *
     * @param _target - Posição desejada.
     *
     * @return QVector2D - Posição do robô.
     */
    QVector2D vt2dPlaceRollerAt(const QVector2D &_target) const;

    void setPrediction(QVector2D &_pos); 
    QVector2D getPrediction() const; 

private:
    Atributos atributos; /**< Atributos do robô.*/
    QString strRole;
    QString strTactic;
    QString strSkill;
    FieldZone myZone;
    QVector2D predictedPosition; /**< Predição da posição 1 segundo à frente. */

    /**
     * @brief vCarregaModeloChute
     * @param caminhoModelo
     */
    void vCarregaModeloChute(const QString& caminhoModelo);

    /**
     * @brief vCarregaModeloChuteCobertura
     * @param caminhoModelo
     */
    void vCarregaModeloChuteCobertura(const QString& caminhoModelo);
};

//Declaração dos parâmetros do radio para que seja possível enviá-los via sinal
Q_DECLARE_METATYPE(QVector<Robo>)

#endif // ROBOT_H
