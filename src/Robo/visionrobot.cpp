#include "visionrobot.h"

VisionRobot::VisionRobot(int _robotID) : Robo(_robotID)
{
    vvt2dRaw.clear();
    vvt2dFiltered.clear();
    vvt2dPredicted.clear();
    detectedFrames = 0;

    kfFilter.reset(new KalmanFilter());
}

void VisionRobot::vAddPosition(QVector<QVector2D> &_vector, QVector2D _position)
{
    _vector.prepend(_position);
    if(_vector.size() > iTamanhoBufferRobo)
    {
        _vector.pop_back();
    }
}

void VisionRobot::vAddRawPosition(QVector2D _position)
{
    vAddPosition(vvt2dRaw, _position);
}

QVector<QVector2D> VisionRobot::vvt2dGetRawPositions() const
{
    return vvt2dRaw;
}

void VisionRobot::vAddFilteredPosition(QVector2D _position)
{
    vAddPosition(vvt2dFiltered, _position);
}

QVector<QVector2D> VisionRobot::vvt2dGetFilteredPositions() const
{
    return vvt2dFiltered;
}

void VisionRobot::vAddPredictedPosition(QVector2D _position)
{
    vAddPosition(vvt2dPredicted, _position);
}

QVector<QVector2D> VisionRobot::vvt2dGetPredictedPositions() const
{
    return vvt2dPredicted;
}

void VisionRobot::vResetFrames()
{
    for(uint & i : iCameraFrame)
    {
        i = 0;
    }
}
