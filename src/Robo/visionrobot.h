#ifndef VISIONROBOT_H
#define VISIONROBOT_H

#include "Robo/robot.h"
#include "Visao/Kalman_Filter/kalmanfilter.h"
#include <QScopedPointer>
#include <QVector>

class VisionRobot : public Robo
{
    QVector<QVector2D> vvt2dRaw;
    QVector<QVector2D> vvt2dFiltered;
    QVector<QVector2D> vvt2dPredicted;

    void vAddPosition(QVector<QVector2D> &_vector, QVector2D _position);
public:
    VisionRobot(int _robotID = 0);

    QScopedPointer<KalmanFilter> kfFilter;
    uint iCameraID; /**< Indica o ID da última câmera em que o robô foi visto  */
    uint iCameraFrame[iNumeroTotalCameras]; /**< Guarda o número do frame em que o robô
                                              foi visto.  @details Isso serve para detectar
                                              quando o robô some da visão.*/
    int detectedFrames;


    void vAddRawPosition(QVector2D _position);
    QVector<QVector2D> vvt2dGetRawPositions() const;

    void vAddFilteredPosition(QVector2D _position);
    QVector<QVector2D> vvt2dGetFilteredPositions() const;

    void vAddPredictedPosition(QVector2D _position);
    QVector<QVector2D> vvt2dGetPredictedPositions() const;

    void vResetFrames();
};

#endif // VISIONROBOT_H
