#ifndef CONTROLESIMULACAO_H
#define CONTROLESIMULACAO_H

#include <QtCore>

class ControleSimulacao
{
public:
    enum ObjetoControleSimulacao : qint8
    {
        ObjetoRoboAmarelo,
        ObjetoRoboAzul,
        ObjetoBola,
        ObjetoNenhum
    };

    ControleSimulacao();

    ObjetoControleSimulacao tipoObjeto;
    qint8 idAtual;
    int mouseButton;

};

#endif // CONTROLESIMULACAO_H
