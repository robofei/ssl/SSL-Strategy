/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "simulator.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include <QNetworkInterface>

// velocidade do roller em rpm
#define VELOCIDADE_ROTACAO_ROLLER (float)100e3

RoboFei_Simulator::RoboFei_Simulator(int _controlPort, int _teamPort,
                                     QString _iface, QObject *parent) : QObject(parent)
{
    udpTeam.reset(new QUdpSocket(this));
    udpSimulationControl.reset(new QUdpSocket(this));

    const QNetworkInterface iface = QNetworkInterface::interfaceFromName(_iface);
    const QNetworkAddressEntry addrEntry = iface.addressEntries().constFirst();
    ipSimulator = addrEntry.broadcast();

    iControlPort = _controlPort;
    iTeamPort = _teamPort;

    qInfo() << QString("[Simulador] Conectando em %1:%2 @ %3")
        .arg(ipSimulator.toString())
        .arg(_teamPort)
        .arg(_iface);

    // Conecta o sinal de chegada de pacote ao slot que irá tratá-los
    connect(udpTeam.get(), &QUdpSocket::readyRead, this,
            &RoboFei_Simulator::vGotFeedback_);
}

RoboFei_Simulator::~RoboFei_Simulator()
{
}



RobotControl RoboFei_Simulator::create_packet_robot(const QList<ROBO_SIM> &robos)
{
    RobotControl control;

    for(const ROBO_SIM& robo : robos)
    {
        // Adiciona um novo comando ao pacote `control`
        RobotCommand* command = control.add_robot_commands();

        command->set_id(robo.id);

        RobotMoveCommand* moveCommand = command->mutable_move_command();
        MoveLocalVelocity* localVelocity = moveCommand->mutable_local_velocity();

        localVelocity->set_forward(robo.vy);
        localVelocity->set_left(robo.vx);
        localVelocity->set_angular(robo.w);

        command->set_kick_speed(robo.kickSpeed);
        command->set_kick_angle(robo.kickVerticalAngle);
        command->set_dribbler_speed(float(robo.roller) * VELOCIDADE_ROTACAO_ROLLER);
//        command->set_dribbler_speed(0); // desabilita para testes
    }

    return control;
}

SimulatorCommand RoboFei_Simulator::create_packet_ball(QVector2D position, QVector2D velocity)
{
    SimulatorCommand command;
    SimulatorControl* control = command.mutable_control();
    TeleportBall* ball = control->mutable_teleport_ball();

    ball->set_x(position.x());
    ball->set_y(position.y());
    ball->set_z(0);

    ball->set_vx(velocity.x());
    ball->set_vy(velocity.y());

    return command;
}

SimulatorCommand RoboFei_Simulator::create_packet_robot_control(bool yellow, qint8 id, QVector2D position)
{
    SimulatorCommand command;
    SimulatorControl* control = command.mutable_control();
    TeleportRobot* robot = control->add_teleport_robot();

    RobotId* robotId = robot->mutable_id();
    robotId->set_id(id);
    robotId->set_team(yellow ? Team::YELLOW : Team::BLUE);

    robot->set_x(position.x());
    robot->set_y(position.y());

    // necessário para funcionar com o simulador da ER-FORCE
    robot->set_by_force(true);

    return command;
}

SimulatorCommand RoboFei_Simulator::create_packet_robot_control(bool yellow, qint8 id, float angle)
{
    SimulatorCommand command;
    SimulatorControl* control = command.mutable_control();
    TeleportRobot* robot = control->add_teleport_robot();

    RobotId* robotId = robot->mutable_id();
    robotId->set_id(id);
    robotId->set_team(yellow ? Team::YELLOW : Team::BLUE);

    robot->set_orientation(angle);

    return command;
}


void RoboFei_Simulator::send_robot_packet(RobotControl packet)
{
    QByteArray dgram;
    dgram.resize(packet.ByteSizeLong());
    packet.SerializeToArray(dgram.data(), dgram.size());

    if(udpTeam->writeDatagram(dgram, ipSimulator, iTeamPort) == -1)
    {
        // qCritical() << "Erro ao enviar uma mensagem para controlar os robôs pela simulação:\n"
        //             << qPrintable(udpTeam->errorString());
    }
}

void RoboFei_Simulator::send_control_packet(SimulatorCommand packet)
{
    QByteArray dgram;
    dgram.resize(packet.ByteSizeLong());
    packet.SerializeToArray(dgram.data(), dgram.size());

    if(udpSimulationControl->writeDatagram(dgram, ipSimulator, iControlPort) == -1)
    {
        qWarning() << "Erro ao enviar uma mensagem para controlar a simulação:\n"
                   << qPrintable(udpSimulationControl->errorString());
    }
}

void RoboFei_Simulator::vGotFeedback_()
{
    while(udpTeam->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpTeam->pendingDatagramSize());
        datagram.fill(0, udpTeam->pendingDatagramSize());
        udpTeam->readDatagram(datagram.data(), datagram.size());

        RobotControlResponse response;
        response.ParseFromArray(datagram, datagram.size());

        if(response.ByteSizeLong() > 0)
        {
            for(int i = 0; i < response.feedback_size(); ++i)
            {
                const RobotFeedback& feedback = response.feedback(i);
                if(feedback.has_id() && feedback.has_dribbler_ball_contact())
                {
                    emit _vSendRobotFeedback(static_cast<qint8>(feedback.id()),
                                             feedback.dribbler_ball_contact());
                }
            }
        }
    }
}
