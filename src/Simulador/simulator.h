/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SIMULATOR_H
#define SIMULATOR_H

///
/// \file simulator.h
/// \brief \a ROBO_SIM e \a RoboFei_Simulator
///

//#include <QtNetwork>
#include <QUdpSocket>
#include <QHostAddress>
#include <QVector2D>

#include "Robo/atributos.h"
#include "qscopedpointer.h"
#include "ssl_simulation_robot_control.pb.h"
#include "ssl_simulation_robot_feedback.pb.h"
#include "ssl_simulation_control.pb.h"

// Esses includes aqui embaixo só foram adicionados para eviar um erro que ocorria no processo de build,
//  pois alguns arquivos protobuf não eram gerados.
#include "ssl_simulation_error.pb.h"
#include "ssl_gc_common.pb.h"
#include "ssl_simulation_config.pb.h"

/**
 * @brief Classe com os dados sobre o robô que são enviados para o grSim.
 *
 */
class ROBO_SIM
{
public:
    ROBO_SIM()
    {

    }

    ROBO_SIM(const Atributos &_atb)
    {
        id = _atb.id;

        w = _atb.cmd_w; //Gira
        vx = -_atb.cmd_vx; //Anda pra cima (-x)
        vy  = _atb.cmd_vy; //Anda pra frente (y)

        if(_atb.kick == KICK_SOFT)
        {
            kickSpeed = 3;
            kickVerticalAngle = 0;
        }
        else if(_atb.kick == KICK_CUSTOM)
        {
            kickSpeed = _atb.kickStrength;
            kickVerticalAngle = 0;
        }
        else if(_atb.kick == KICK_STRONG)
        {
            kickSpeed = globalConfig.calibratedKickForce;
            kickVerticalAngle = 0;
        }
        else if(_atb.kick == CHIP_KICK_STRONG)
        {
            //                QLineF kick = QLineF(QPointF(3.5, 3.5), QPointF(0, 0));
            //                kickSpeed = qMin(kick.length(), (double)globalConfig.calibratedKickForce);
            //                kickVerticalAngle = 180 - kick.angle();

            kickSpeed = qMin(5.2, (double)globalConfig.calibratedKickForce);
            kickVerticalAngle = 45;
        }
        else if(_atb.kick == CHIP_KICK_CUSTOM)
        {
            // temporário
            const double FORCA_MAXIMA_CHIP_KICK_CUSTOM = 6.5;
            //                QLineF kick = QLineF(QPointF(_atb.kickStrength, 3.5), QPointF(0, 0));
            //                // Apenas na simulação. Não suportamos isso no robô real
            //                // Por gentileza, não mexer nesses valores.
            //                kickSpeed = qMin(kick.length(), (double)globalConfig.calibratedKickForce);
            //                kickVerticalAngle = 180 - kick.angle();
            kickSpeed = qMin((double)_atb.kickStrength, FORCA_MAXIMA_CHIP_KICK_CUSTOM);
            kickVerticalAngle = 45;
        }
        else
        {
            kickSpeed = 0;
            kickVerticalAngle = 0;
        }

        roller = _atb.roller;
    }

    int id; /**< ID do robô. */
    float kickSpeed; /**< Módulo da velocidade do chute. */
    float kickVerticalAngle; /**< Ângulo vertical (em graus). */
    float vy; /**< Velocidade tangente ao robô. */
    float vx;  /**< Velocidade normal ao robô. */
    float w; /**< Velocidade angular do robô. */
    bool roller; /**< Liga/Desliga o roller. */
    float rollerSpeed; /**< Velocidade do roller. */
};

/**
 * @brief Classe responsável por criar os pacotes no formato do grSim e enviá-los
 * na rede.
 *
 */
class RoboFei_Simulator : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Construtor da classe
     *
     * @param _controlPort - Porta de controle do simulador
     * @param _teamPort - Porta de controle do time
     * @param _iface - Interface de rede
     * @param parent
     */
    RoboFei_Simulator(int _controlPort, int _teamPort,
                      QString _iface, QObject* parent = nullptr);

    /**
     * @brief Destrutor da classe
     */
    ~RoboFei_Simulator();

    /**
    * @brief Envia o pacote à rede
    *
    * @note Com o endereço da porta do grSim sendo #port_simulator
    * e o ip #addr_simulator
    *
    * @param packet - Pacote a ser enviado
    */
    void send_robot_packet(RobotControl packet);

    void send_control_packet(SimulatorCommand packet);

    /**
    * @brief Cria um pacote do grSim (\p grSim_Packet) com os dados do robô
    *
    * @param  _r - dados do robô (id, velocidades, posição, etc.).
    * @param  _team - time do robô
    * @return Pacote criado (\p grSim_Packet)
    */
    RobotControl create_packet_robot(const QList<ROBO_SIM>& robos);

    /**
     * @brief Cria um pacote do grSim (\p grSim_Packet) com as velocidades e a
     * posição da bola
     *
     * @param position - Posição da bola
     * @param velocity - Velocidade da bola
     *
     * @return grSim_Packet - Pacote criado
     */
    SimulatorCommand create_packet_ball(QVector2D position, QVector2D velocity);
    SimulatorCommand create_packet_robot_control(bool yellow, qint8 id, QVector2D position);
    SimulatorCommand create_packet_robot_control(bool yellow, qint8 id, float angle);

private:
    QScopedPointer<QUdpSocket> udpTeam; /**< Socket UDP para controlar os robôs. */
    QScopedPointer<QUdpSocket> udpSimulationControl; /**< Socket UDP para controlar a simulação. */
    QHostAddress ipSimulator; /**< Endereço do grSim. */
    int iTeamPort; /**< Porta de controle dos robôs do time. */
    int iControlPort;/**< Porta de controle do simulador. */

public slots:
    void vGotFeedback_();

signals:
    void _vSendRobotFeedback(qint8 _id, bool _ballSensor);
};

#endif // SIMULATOR_H
