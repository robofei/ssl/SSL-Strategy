/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "simuladorrefbox.h"

SimuladorRefbox::SimuladorRefbox(QSize tamanhoCampo)
{
    strComandos.clear();
    strComandos << "Timeout" << "Direct Kick" << "Indirect Kick" <<
                   "Kickoff" << "Penalti Kick" << "Ball Placement";

    szCampo = tamanhoCampo;
    vt2dPosicaoPlacement = QVector2D(0,0);

    lstBotoesAmarelo            = new QList<QPushButton*>;
    lstBotoesAzul               = new QList<QPushButton*>;
    lstBotoesGerais             = new QList<QPushButton*>;
    lstLabels                   = new QList<QLabel*>;
    lstCoordenadasBallPlacement = new QList<QSpinBox*>;

    gridLayout = new QGridLayout;

    comandoUltimoComando = SSL_Referee_Command_HALT;
    vInicializaBotoes(AZUL);
    vInicializaBotoes(AMARELO);
    vInicializaBotoesGerais();
    vInicializaLabels();
    vInicializaSpinBox();

    setLayout(gridLayout);

    connect(lstBotoesGerais->at(0), &QPushButton::clicked, this, &SimuladorRefbox::vStop);
    connect(lstBotoesGerais->at(1), &QPushButton::clicked, this, &SimuladorRefbox::vHalt);
    connect(lstBotoesGerais->at(2), &QPushButton::clicked, this, &SimuladorRefbox::vNormal);
    connect(lstBotoesGerais->at(3), &QPushButton::clicked, this, &SimuladorRefbox::vForce);

    connect(lstBotoesAzul->at(0), &QPushButton::clicked, this, &SimuladorRefbox::vTimeoutAzul);
    connect(lstBotoesAzul->at(1), &QPushButton::clicked, this, &SimuladorRefbox::vDirectAzul);
    connect(lstBotoesAzul->at(2), &QPushButton::clicked, this, &SimuladorRefbox::vIndirectAzul);
    connect(lstBotoesAzul->at(3), &QPushButton::clicked, this, &SimuladorRefbox::vKickoffAzul);
    connect(lstBotoesAzul->at(4), &QPushButton::clicked, this, &SimuladorRefbox::vPenaltiAzul);
    connect(lstBotoesAzul->at(5), &QPushButton::clicked, this, &SimuladorRefbox::vPlacementAzul);

    connect(lstBotoesAmarelo->at(0), &QPushButton::clicked, this, &SimuladorRefbox::vTimeoutAmarelo);
    connect(lstBotoesAmarelo->at(1), &QPushButton::clicked, this, &SimuladorRefbox::vDirectAmarelo);
    connect(lstBotoesAmarelo->at(2), &QPushButton::clicked, this, &SimuladorRefbox::vIndirectAmarelo);
    connect(lstBotoesAmarelo->at(3), &QPushButton::clicked, this, &SimuladorRefbox::vKickoffAmarelo);
    connect(lstBotoesAmarelo->at(4), &QPushButton::clicked, this, &SimuladorRefbox::vPenaltiAmarelo);
    connect(lstBotoesAmarelo->at(5), &QPushButton::clicked, this, &SimuladorRefbox::vPlacementAmarelo);

    connect(lstCoordenadasBallPlacement->at(0), QOverload<int>::of(&QSpinBox::valueChanged), this, &SimuladorRefbox::vMudouPosicaoPlacement);
    connect(lstCoordenadasBallPlacement->at(1), QOverload<int>::of(&QSpinBox::valueChanged), this, &SimuladorRefbox::vMudouPosicaoPlacement);

    connect(this, &SimuladorRefbox::vEnviaComando, this, &SimuladorRefbox::vPegaComando);
    emit vEnviaComando(SSL_Referee_Command_HALT, vt2dPosicaoPlacement);

}

SimuladorRefbox::~SimuladorRefbox()
{    
    for (auto& n : *lstBotoesAzul){
        delete n;
        n = nullptr;
    }

    for (auto& n : *lstBotoesAmarelo){
        delete n;
        n = nullptr;
    }

    for (auto& n : *lstBotoesGerais){
        delete n;
        n = nullptr;
    }

    for (auto& n : *lstLabels){
        delete n;
        n = nullptr;
    }

    for (auto& n : *lstCoordenadasBallPlacement){
        delete n;
        n = nullptr;
    }

    delete lstBotoesAzul;
    delete lstBotoesAmarelo;
    delete lstBotoesGerais;
    delete lstLabels;
    delete lstCoordenadasBallPlacement;
    delete gridLayout;
}



void SimuladorRefbox::vInicializaBotoes(bool azul)
{
    QColor cor(Qt::blue);
    QList<QPushButton*> *botoes;

    botoes = lstBotoesAzul;

    if(azul == AMARELO)
    {
        botoes = lstBotoesAmarelo;
        cor = QColor(Qt::yellow);
    }

    for (int n = 0; n < COMANDOS; ++n)
    {
        botoes->append(new QPushButton(strComandos.at(n), this));

        QPalette palette = botoes->at(n)->palette();
        palette.setBrush(QPalette::Button, cor);
        palette.setBrush(QPalette::ButtonText, Qt::black);
        botoes->at(n)->setPalette(palette);

        gridLayout->addWidget(botoes->at(n), n+2, static_cast<int>(azul));
    }
}

void SimuladorRefbox::vInicializaBotoesGerais()
{
    QList<QString> textoBotoes;
    textoBotoes << "Stop" << "Halt" << "Normal" << "Force";
    bool col = true;
    for (int n = 0; n < textoBotoes.size(); ++n)
    {
        lstBotoesGerais->append(new QPushButton(textoBotoes.at(n), this));

        col = !col;
        gridLayout->addWidget(lstBotoesGerais->at(n), n/2, static_cast<int>(col));
    }
}

void SimuladorRefbox::vInicializaLabels()
{
    lstLabels->append(new QLabel("Posicao Ball Placement", this));
    lstLabels->last()->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    gridLayout->addWidget(lstLabels->last(), COMANDOS+2, 0, 1, 2);
}

void SimuladorRefbox::vInicializaSpinBox()
{
    lstCoordenadasBallPlacement->append(new QSpinBox(this));
    lstCoordenadasBallPlacement->append(new QSpinBox(this));

    lstCoordenadasBallPlacement->at(0)->setMaximum(szCampo.width() / 2);
    lstCoordenadasBallPlacement->at(0)->setMinimum(-szCampo.width() / 2);
    lstCoordenadasBallPlacement->at(0)->setPrefix("X = ");
    lstCoordenadasBallPlacement->at(0)->setSuffix(" mm");
    lstCoordenadasBallPlacement->at(0)->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    lstCoordenadasBallPlacement->at(0)->setSingleStep(100);
    gridLayout->addWidget(lstCoordenadasBallPlacement->at(0), COMANDOS+3, 0);

    lstCoordenadasBallPlacement->at(1)->setMaximum(szCampo.height() / 2);
    lstCoordenadasBallPlacement->at(1)->setMinimum(-szCampo.height() / 2);
    lstCoordenadasBallPlacement->at(1)->setPrefix("Y = ");
    lstCoordenadasBallPlacement->at(1)->setSuffix(" mm");
    lstCoordenadasBallPlacement->at(1)->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    lstCoordenadasBallPlacement->at(1)->setSingleStep(100);
    gridLayout->addWidget(lstCoordenadasBallPlacement->at(1), COMANDOS+3, 1);
}

void SimuladorRefbox::closeEvent(QCloseEvent *event)
{
    event->accept();
    emit vRefClose();
}

void SimuladorRefbox::reject()
{
    emit vRefClose();
}

void SimuladorRefbox::vStop()
{
    emit vEnviaComando(SSL_Referee_Command_STOP, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vHalt()
{
    emit vEnviaComando(SSL_Referee_Command_HALT, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vNormal()
{
    emit vEnviaComando(SSL_Referee_Command_NORMAL_START, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vForce()
{
    emit vEnviaComando(SSL_Referee_Command_FORCE_START, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vTimeoutAzul()
{
    emit vEnviaComando(SSL_Referee_Command_TIMEOUT_BLUE, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vTimeoutAmarelo()
{
    emit vEnviaComando(SSL_Referee_Command_TIMEOUT_YELLOW, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vDirectAzul()
{
    emit vEnviaComando(SSL_Referee_Command_DIRECT_FREE_BLUE, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vDirectAmarelo()
{
    emit vEnviaComando(SSL_Referee_Command_DIRECT_FREE_YELLOW, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vIndirectAzul()
{
    emit vEnviaComando(SSL_Referee_Command_INDIRECT_FREE_BLUE, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vIndirectAmarelo()
{
    emit vEnviaComando(SSL_Referee_Command_INDIRECT_FREE_YELLOW, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vKickoffAzul()
{
    emit vEnviaComando(SSL_Referee_Command_PREPARE_KICKOFF_BLUE, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vKickoffAmarelo()
{
    emit vEnviaComando(SSL_Referee_Command_PREPARE_KICKOFF_YELLOW, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vPenaltiAzul()
{
    emit vEnviaComando(SSL_Referee_Command_PREPARE_PENALTY_BLUE, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vPenaltiAmarelo()
{
    emit vEnviaComando(SSL_Referee_Command_PREPARE_PENALTY_YELLOW, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vPlacementAzul()
{
    emit vEnviaComando(SSL_Referee_Command_BALL_PLACEMENT_BLUE, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vPlacementAmarelo()
{
    emit vEnviaComando(SSL_Referee_Command_BALL_PLACEMENT_YELLOW, vt2dPosicaoPlacement);
}

void SimuladorRefbox::vMudouPosicaoPlacement()
{
    emit vEnviaComando(comandoUltimoComando,
                       QVector2D(lstCoordenadasBallPlacement->at(0)->value(),
                                 lstCoordenadasBallPlacement->at(1)->value()));

}

void SimuladorRefbox::vPegaComando(SSL_Referee_Command comando, QVector2D posBallPlacement)
{
    comandoUltimoComando = comando;
    vt2dPosicaoPlacement = posBallPlacement;
}
