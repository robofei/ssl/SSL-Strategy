/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SIMULADORREFBOX_H
#define SIMULADORREFBOX_H

///
/// \file simuladorrefbox.h
/// \brief \a SimuladorRefbox
///

#include <QtWidgets>
#include <QDialog>
#include <QLabel>
#include <QSpinBox>
#include <QGridLayout>
#include <QVector2D>
#include <QDebug>

#include "referee.pb.h"

#define COMANDOS 6 /**< Quantidade de comandos para cada time. */
#define AZUL 1 /**< Índice dos botões do time azul.*/
#define AMARELO 0 /**< Índice dos botões do time amarelo.*/

/**
 * @brief Interface que simula os comandos que o Refbox envia.
 */
class SimuladorRefbox : public QDialog
{
    Q_OBJECT
public:

    /**
     * @brief Construtor da classe
     *
     * @param tamanhoCampo - Tamanho do campo
     * @see AmbienteCampo::vt2dPegaTamanhoCampo
     */
    SimuladorRefbox(QSize tamanhoCampo);

    /**
     * @brief Destrutor da clsse
     */
    ~SimuladorRefbox();

protected:

    ///
    /// \brief Overload do slot da classe QDialog
    ///
    /// \details Esse slot é chamado quando o usuário finaliza
    /// a janela (por um shortcut ou por algum botão).
    ///
    /// O sinal \p vRefClose() é emitido indicando que o objeto
    /// da classe pode ser finalizado.
    ///
    void closeEvent(QCloseEvent *event = nullptr);

    ///
    /// \brief Overload do slot da classe QDialog
    /// \details Esse slot é chamado quando o usuário
    /// pressiona a tecla ESCAPE.
    ///
    /// O sinal \p vRefClose() é emitido indicando que o objeto
    /// da classe pode ser finalizado.
    ///
    void reject();

private:
    /**
     * @brief Inicializa os botões dos comandos individuais de time. Ex: Kickoff,
     * Direct...
     *
     * @param azul - True se for para o time azul, senão, false
     */
    void vInicializaBotoes(bool azul);

    /**
     * @brief Inicializa os botões gerais para os dois times. Ex: Stop, Halt...
     */
    void vInicializaBotoesGerais();

    /**
     * @brief Inicializa os labels da interface
     */
    void vInicializaLabels();

    /**
     * @brief Inicializa os spins boxes da interface
     */
    void vInicializaSpinBox();

    QVector2D vt2dPosicaoPlacement; /**< Posição atual do ball placement.*/

    QSize szCampo; /**< Tamanho do campo. */
    QStringList strComandos; /**< Lista com os comandos de time do refbox. */
    QGridLayout *gridLayout; /**< Layout em grade para organizar a interface.*/

    QList<QPushButton*> *lstBotoesAzul; /**< Lista com os botões do time azul.
                    @details Os botões estão na mesma ordem da #strComandos.*/
    QList<QPushButton*> *lstBotoesAmarelo; /**< Lsita com os botões do time
                                                amarelo.
                    @details Os botões estão na mesma ordem da #strComandos.*/
    QList<QPushButton*> *lstBotoesGerais; /**< Lista com os botões gerais. */
    QList<QLabel*> *lstLabels; /**< Lista com os labels.*/
    QList<QSpinBox*> *lstCoordenadasBallPlacement; /**< Lista com os spin boxes
                                                da posição do ball placement.*/ 
    SSL_Referee_Command comandoUltimoComando; /**< Ultimo comando eviado para
                                                a interface. @see RoboFeiSSL.*/
private slots:
    /**
     * @brief Recebe o sinal do botão de stop e envia o comando para a interface
     */
    void vStop();

    /**
     * @brief Recebe o sinal do botão de halt e envia o comando para a interface
     */
    void vHalt();

    /**
     * @brief Recebe o sinal do botão de normal start e envia o comando para a
     * interface
     */
    void vNormal();

    /**
     * @brief Recebe o sinal do botão de force start e envia o comando para a 
     * interface
     */
    void vForce();

    /**
     * @brief Recebe o sinal do botão de timeout azul e envia o comando para a
     * interface
     */
    void vTimeoutAzul();

    /**
     * @brief Recebe o sinal do botão de timeout amarelo e envia o comando para
     * a interface
     */
    void vTimeoutAmarelo();

    /**
     * @brief Recebe o sinal do botão de direct azul e envia o comando para a 
     * interface
     */
    void vDirectAzul();

    /**
     * @brief Recebe o sinal do botão de direct amarelo e envia o comando para 
     * a interface
     */
    void vDirectAmarelo();

    /**
     * @brief Recebe o sinal do botão de indirect azul e envia o comando para a 
     * interface
     */
    void vIndirectAzul();

    /**
     * @brief Recebe o sinal do botão de indirect amarelo e envia o comando 
     * para a interface
     */
    void vIndirectAmarelo();

    /**
     * @brief Recebe o sinal do botão de kickoff azul e envia o comando para a 
     * interface
     */
    void vKickoffAzul();

    /**
     * @brief Recebe o sinal do botão de kickoff amarelo e envia o comando para 
     * a interface
     */
    void vKickoffAmarelo();

    /**
     * @brief Recebe o sinal do botão de penalti azul e envia o comando para a 
     * interface
     */
    void vPenaltiAzul();

    /**
     * @brief Recebe o sinal do botão de penalti amarelo e envia o comando para 
     * a interface
     */
    void vPenaltiAmarelo();

    /**
     * @brief Recebe o sinal do botão de ball placement azul e envia o comando 
     * para a interface
     */
    void vPlacementAzul();

    /**
     * @brief Recebe o sinal do botão de ball placement amarelo e envia o 
     * comando para a interface
     */
    void vPlacementAmarelo();

    /**
     * @brief Recebe o sinal de alteração na posição do ball placement e envia 
     * a nova posição para a interface
     */
    void vMudouPosicaoPlacement();

    /**
     * @brief Recebe um comando e posição do ball placement e atualiza os 
     * atributos da classe
     * @see #comandoUltimoComando, #vt2dPosicaoPlacement
     *
     * @param comando - Comando do referee
     * @param posBallPlacement - Posição do ball placement
     */
    void vPegaComando(SSL_Referee_Command comando, QVector2D posBallPlacement);

signals:

    /**
     * @brief Envia um comando do referee e a posição do ball placement
     *
     * @param comando - Comando do referee
     * @param posBallPlacement - Posição do ball placement
     */
    void vEnviaComando(SSL_Referee_Command comando, QVector2D posBallPlacement);

    /**
     * @brief Sinal emitido ao fechar a janela
     */
    void vRefClose();
};

#endif // SIMULADORREFBOX_H
