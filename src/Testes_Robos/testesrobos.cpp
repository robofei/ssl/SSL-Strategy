/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "testesrobos.h"
#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Path_Planning/mrastar.h"
#include "qglobal.h"
#include "qrandom.h"
#include "qvector2d.h"

TestesRobos::TestesRobos()
{
    qRegisterMetaType<QVector<QVector2D>>();
    qRegisterMetaType<DADOS_TESTES>();
    qRegisterMetaType<VirtualJoystickRobot>();

    acTestes.reset(new AmbienteCampo);
    asvgPathPlanner.reset();
    mraPathPlanner.reset();
    rrtPathPlanner.reset();
    collisionTimesAllies.resize(globalConfig.robotsPerTeam, globalConfig.robotsPerTeam);
    collisionTimesAllies.setZero(globalConfig.robotsPerTeam, globalConfig.robotsPerTeam);
    collisionTimesOpponents.resize(globalConfig.robotsPerTeam, globalConfig.robotsPerTeam);
    collisionTimesOpponents.setZero(globalConfig.robotsPerTeam, globalConfig.robotsPerTeam);
    discardLog = false;

    bIndoDestino = false;
    iNTeste1 = 0;
    iNTeste2 = 0;
    iTrajetosObstaculos.clear();

    tmrTestes.reset();

    bRecebeuPrimeiroPacote = false;
    bThreadAtivada = false;

    fileLogTeste1.close();
    fileLogTeste2.close();
    fileLogTesteMRADVG.close();

    QVector2D pos(-1000, 0);
    if (globalConfig.robotsPerTeam == 11)
    {
        pos.setX(-1500);
    }
    else if (globalConfig.robotsPerTeam == 16)
    {
        pos.setX(-2000);
    }
    
    for (int id = 0; id < MAX_IDS; ++id)
    {
        antipodalStart.append(pos);
        pos = Auxiliar::vt2dRotaciona({0, 0}, pos, -360.f / MAX_IDS);
    }
    for (int n = 0; n < antipodalStart.size(); ++n)
    {
        antipodalEnd.append(
            Auxiliar::vt2dRotaciona({0, 0}, antipodalStart.at(n), 180));
    }
    randomRng.seed(1);
    runPlanning = true;
}

void TestesRobos::vAtivaThread(const bool _bThreadAtivada)
{
    bThreadAtivada = _bThreadAtivada;

    if (bThreadAtivada == false)
    {
        bRecebeuPrimeiroPacote = false;
        if (tmrTestes)
        {
            tmrTestes->stop();
            tmrTestes.reset();
        }
    }
}

void TestesRobos::vReceiveVisionData_(const VisionPacket _packet)
{
    // Atualiza os dados dos robôs e da bola
    acTestes->vAtualizaDadosVisao(_packet);

    if (bRecebeuPrimeiroPacote == false)
        bRecebeuPrimeiroPacote = true;

    // Se ja recebeu o primeiro pacote de visao, ja recebeu o sinal para
    // iniciar e o timer ainda nao foi iniciado -> Inicia o timer
    if (bRecebeuPrimeiroPacote == true && bThreadAtivada == true && !tmrTestes)
    {
        tmrTestes.reset(new QTimer(this));
        tmrTestes->setTimerType(Qt::PreciseTimer);
        connect(tmrTestes.get(), &QTimer::timeout, this,
                &TestesRobos::vExecutaTestes);
        tmrTestes->start(iTempoLoopEstrategia);

        // Inicializa path-planners
        QSize tamanhoCampo = acTestes->geoCampo->szField();

        rrtPathPlanner.reset(new RRT(tamanhoCampo));
        asvgPathPlanner.reset(new A_StarVG(iAnguloGrafo));
        mraPathPlanner.reset(
            new MRAStar(MAX_IDS, acTestes->geoCampo->szField(), 250));
    }
}

void TestesRobos::vRecebeDadosInterface(QVector2D posTimeout, int idGoleiro,
                                        int corTime, int ladoCampo,
                                        bool tipoAmbiente)
{
    acTestes->vt2dPosicaoTimeout = posTimeout;

    // Retira a jogada de goleiro do robo anterior
    if (acTestes->allies->iGetGoalieID() != idGoleiro)
    {
        acTestes->allies->getPlayer(acTestes->allies->iGetGoalieID())
            ->vSetaJogada(Nenhuma);
    }
    acTestes->allies->vSetGoalieID(idGoleiro);
    acTestes->allies->vSetTeamColor(static_cast<TeamColor>(corTime));
    acTestes->allies->vSetSide(ladoCampo);
    acTestes->bTipoAmbiente = tipoAmbiente;
}

void TestesRobos::vRecebeDadosTestes(const DADOS_TESTES& dados)
{
    if (acTestes->tsDadosTeste.tstTesteAtual == tstNenhum &&
        dados.tstTesteAtual == tstPathPlanner)
    {
        acTestes->tsDadosTeste = dados;
        iNTeste1 = 0;
        iNTeste2 = 0;
        bIndoDestino = false;
        if (dados.iTipoTestePathPlanner == 1)
            vLogNovoTeste1(dados);
        else if (dados.iTipoTestePathPlanner == 2)
        {
            acTestes->tsDadosTeste.iPathPlannerUtilizado = 1;
            vLogNovoTeste2("RRT");
        }
        else if (dados.iTipoTestePathPlanner >= 3 &&
                 acTestes->allies->teamColor() == timeAzul)
        {
            Robo* robot;
            for (int id = 0; id < MAX_IDS; ++id)
            {
                robot = acTestes->allies->getPlayer(id);
                dlppDadosPP.totalDistance[id] = 0;
                dlppDadosPP.robotsLastPosition[id] = robot->vt2dPosicaoAtual();
            }
            if (USE_MRASTAR)
            {
                newLogTestsMRADVG("MRA");
            }
            else
            {
                newLogTestsMRADVG("DVG");
            }
        }
    }
    else if (dados.tstTesteAtual == tstMovClick)
    {
        iTrajetosObstaculos = dados.iTrajetosRobosMouse;
    }

    if (dados.tstTesteAtual == tstNenhum)
    {
        fileLogTeste1.close();
        fileLogTeste2.close();
        fileLogTesteMRADVG.close();
    }
    acTestes->tsDadosTeste = dados;
    acTestes->tsDadosTeste.iPathPlannerUtilizado = 1;
}

void TestesRobos::vRecebeCaminhosAlterados(const MotionPathFeedback& _paths)
{
    for (int i = 0; i < MAX_IDS; ++i)
    {
        // acTestes->allies->getPlayer(i)->path->vSetPath(
        //     acCaminhos.allies->getPlayer(i)->path->getPath(),
        //     acCaminhos.allies->getPlayer(i)->path->pathLength(), false);
        // acTestes->opponents->getPlayer(i)->path->vSetPath(
        //     acCaminhos.opponents->getPlayer(i)->path->getPath(),
        //     acCaminhos.opponents->getPlayer(i)->path->pathLength(), false);

        acTestes->allies->getPlayer(i)->path->vUpdatePath(_paths.robotsPaths[i].getPath());
    }
}

void TestesRobos::vRecebeDadosRoboVirtualJoystick(VirtualJoystickRobot robot)
{
    if (robot.id == -1)
        return;

    acTestes->tsDadosTeste.iIDRoboTeste = robot.id;
    acTestes->tsDadosTeste.tstTesteAtual = tstRobotController;
    acTestes->tsDadosTeste.bMovimentarRobos = true;

    //===================================================================
    Robo* _robot = acTestes->allies->getPlayer(robot.id);
    QVector2D tempVel(robot.iVx, robot.iVy);
    float anguloRobo = _robot->fAnguloAtual();

    float anguloY = 0.0, anguloX = 0.0;
    float _vMax =
        tempVel.length(); // sqrt(pow(tempVel.x(),2) + pow(tempVel.y(),2));

    QVector2D vetorXRobo, vetorYRobo(qCos(anguloRobo), qSin(anguloRobo));

    vetorXRobo =
        Auxiliar::vt2dRotaciona(_robot->vt2dPosicaoAtual(),
                                vetorYRobo + _robot->vt2dPosicaoAtual(), -90);
    vetorXRobo = vetorXRobo - _robot->vt2dPosicaoAtual();

    anguloX = Auxiliar::dAnguloVetor1Vetor2(vetorXRobo, tempVel) * M_PI / 180;
    anguloY = Auxiliar::dAnguloVetor1Vetor2(vetorYRobo, tempVel) * M_PI / 180;

    tempVel.setX(_vMax * qCos(anguloX));
    tempVel.setY(_vMax * qCos(anguloY));
    //===================================================================

    if (acTestes->bTipoAmbiente == GR_SIM)
    {
        //         acTestes->allies->getPlayer(robot.id)->vAndarXY(robot.iVx*dVelocidadeMaximaReal/100.f,
        //                                                         robot.iVy*dVelocidadeMaximaReal/100.f,
        //                                                         robot.iVw*dRotacaoMaximaReal/100.f,
        //                                                         100);
        acTestes->allies->getPlayer(robot.id)->vAndarXY(
            tempVel.x() * globalConfig.robotVelocities.linear.max / 100.f,
            tempVel.y() * globalConfig.robotVelocities.linear.max / 100.f,
            robot.iVw * globalConfig.robotVelocities.angular.max / 100.f, 100);
    }
    else
    {
        acTestes->allies->getPlayer(robot.id)->vAndarXY(robot.iVx, robot.iVy,
                                                        robot.iVw, 100);
    }

    acTestes->allies->getPlayer(robot.id)->vChutar(robot.kickType, 250);

    if (robot.bRollerLigado)
    {
        acTestes->allies->getPlayer(robot.id)->vLigaRoller(
            robot.iVelocidadeRoller);
    }
    else
    {
        acTestes->allies->getPlayer(robot.id)->vDesligaRoller();
    }
}

void TestesRobos::SLOT_recebeSolicitacaoTesteDecisaoPasse()
{
    emit SIGNAL_decisoesPasse(acTestes.get());
}

void TestesRobos::SLOT_recebeSolicitacaoTesteDecisaoChute()
{
    emit SIGNAL_decisoesChute(acTestes.get());
}

void TestesRobos::vTestePathPlanner()
{
    rrtPathPlanner->vSetaParametrosRRT(
        acTestes->tsDadosTeste.fDistanciaAvancoRRT,
        acTestes->tsDadosTeste.iNumeroIteracoesRRT,
        acTestes->tsDadosTeste.iDistanciaMinimaRRT,
        acTestes->tsDadosTeste.iLeafSizeRRT);

    if (acTestes->tsDadosTeste.iTipoTestePathPlanner == 3 &&
        iNTeste2 < N_TESTS_PP)
    {
        antipodalTest();
        emit vEnviaProgressoTestePP(N_TESTS_PP, iNTeste2);
    }
    else if (acTestes->tsDadosTeste.iTipoTestePathPlanner == 4 &&
             iNTeste2 < N_TESTS_PP)
    {
        randomTest();
        emit vEnviaProgressoTestePP(N_TESTS_PP, iNTeste2);
    }
    else if (acTestes->tsDadosTeste.iTipoTestePathPlanner == 5 &&
             iNTeste2 < N_TESTS_PP)
    {
        markingTest();
    }
    else
    {
        for (int n = 0; n < MAX_IDS; ++n)
        {
            const Robo* robo = acTestes->allies->getPlayer(n);

            if (robo->iID() == acTestes->tsDadosTeste.iIDRoboTeste &&
                robo->bRoboDetectado())
            {
                if (acTestes->tsDadosTeste.iTipoTestePathPlanner == 1)
                {
                    vTestePP1(n);
                }
                if (acTestes->tsDadosTeste.iTipoTestePathPlanner == 2)
                {
                    vTestePP2();
                }

                acTestes->allies->getPlayer(n)->vSetaPontoAnguloDestino(
                    acTestes->tsDadosTeste.vt2dDestinoTesteRobo);
            }
        }
    }
}

void TestesRobos::vLogDadosTeste1()
{
    QTextStream stream(&fileLogTeste1);

    stream << dlppDadosPP.iIteracoesRRT << ";" << dlppDadosPP.dTempoRRT << ";"
           << dlppDadosPP.fComprimentoRRT << ";"
           << dlppDadosPP.fDistanciaObjetosRRT << ";"
           << dlppDadosPP.iCelulasExpandidas << ";" << dlppDadosPP.dTempoA
           << ";" << dlppDadosPP.dTempoGrafo << ";" << dlppDadosPP.fComprimentoA
           << ";" << dlppDadosPP.fDistanciaObjetosA << ";"
           << "\n";
}

void TestesRobos::vLogDadosTeste2()
{
    const double tempo = dlppDadosPP.etmTempoInicioDestino.nsecsElapsed() / 1e9;
    QTextStream stream(&fileLogTeste2);

    stream << dlppDadosPP.dTempoTotal << ";" << dlppDadosPP.fComprimentoTrajeto
           << ";" << dlppDadosPP.iNRecalculos << ";" << tempo << ";"
           << dlppDadosPP.iNColisoes << "\n";
}

void TestesRobos::logDataTestsMRADVG()
{
    if (!discardLog)
    {
        QTextStream stream(&fileLogTesteMRADVG);

        float avoidanceTime = 0;
        emit requestAvoidanceComputationalTime(avoidanceTime);
        dlppDadosPP.avoidanceTime = avoidanceTime;

        float minD = 1e10, maxD = 0;
        for (float dist : qAsConst(dlppDadosPP.minDistances))
        {
            minD = qMin(dist, minD);
        }

        for (float& dist : dlppDadosPP.totalDistance)
        {
            maxD = qMax(dist, maxD);
            dist = 0;
        }

        dlppDadosPP.minDistance = minD / 1e3;
        maxD /= 1e3;
        qInfo() << "Min distance" << dlppDadosPP.minDistance;

        stream << dlppDadosPP.computationTime << ";" << dlppDadosPP.mapTime
               << ";" << dlppDadosPP.avoidanceTime << ";"
               << dlppDadosPP.navigationTime << ";" << dlppDadosPP.minDistance
               << ";" << maxD / dlppDadosPP.navigationTime << ";"
               << dlppDadosPP.collisions << ";\n";
        // int n = 0;
        //         for(double dist : qAsConst(dlppDadosPP.mraMinDistances))
        //         {
        //             if(n == dlppDadosPP.mraMinDistances.size() - 1)
        //             {
        //                 stream << dist << ";";
        //             }
        //             else
        //             {
        //                 stream << dist << ",";
        //             }
        //             n++;
        //         }
        // stream << "\n";
    }
    discardLog = false;

    dlppDadosPP.minDistances.clear();
    dlppDadosPP.computationTime = 0;
    dlppDadosPP.mapTime = 0;
    dlppDadosPP.avoidanceTime = 0;
    dlppDadosPP.navigationTime = 0;
    dlppDadosPP.minDistance = 0;
    dlppDadosPP.collisions = 0;
    collisionTimesAllies.setZero(MAX_IDS, MAX_IDS);
    collisionTimesOpponents.setZero(MAX_IDS, MAX_IDS);
}

void TestesRobos::vVaiParaInicio()
{
    const int id = acTestes->tsDadosTeste.iIDRoboTeste;
    acTestes->allies->getPlayer(id)->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
    const Robo* robo = acTestes->allies->getPlayer(id);
    const float dist = robo->vt2dPosicaoAtual().distanceToPoint(
        acTestes->tsDadosTeste.vt2dPosicaoInicialTeste);

    if (bIndoDestino == false && dist > globalConfig.robotDiameter / 2.0)
    {
        acTestes->allies->getPlayer(id)->vSetaDestino(
            acTestes->tsDadosTeste.vt2dPosicaoInicialTeste);
        asvgPathPlanner->vt2dCalculaTrajeto(acTestes.get(), id);
    }
    else if (bIndoDestino == false && dist < globalConfig.robotDiameter / 2.0)
    {
        bIndoDestino = true;
        acTestes->allies->getPlayer(id)->path->vSetPath({}, 0);
        dlppDadosPP.etmTempoInicioDestino.start();
        dlppDadosPP.iNRecalculos = -1;
        dlppDadosPP.iNColisoes = 0;
        dlppDadosPP.dTempoTotal = 0;
        dlppDadosPP.fComprimentoTrajeto = 0;
    }
}

void TestesRobos::vVaiParaDestino()
{
    const int id = acTestes->tsDadosTeste.iIDRoboTeste;
    acTestes->allies->getPlayer(id)->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
    const Robo* robo = acTestes->allies->getPlayer(id);
    const float dist = robo->vt2dPosicaoAtual().distanceToPoint(
        acTestes->tsDadosTeste.vt2dDestinoTesteRobo);

    if (bIndoDestino == true && dist > globalConfig.robotDiameter / 2.0)
    {
        QVector<QVector2D> dummy;
        acTestes->allies->getPlayer(id)->vSetaDestino(
            acTestes->tsDadosTeste.vt2dDestinoTesteRobo);
        if (acTestes->tsDadosTeste.iPathPlannerUtilizado == 1)
        {
            //             vRRT(id, dummy, dlppDadosPP.fComprimentoTrajeto,
            //             dlppDadosPP.dTempoTotal);
            multiRobotAStar(id, dlppDadosPP.fComprimentoTrajeto,
                            dlppDadosPP.dTempoTotal);
        }
        else if (acTestes->tsDadosTeste.iPathPlannerUtilizado == 2)
        {
            vAStar(id, dummy, dlppDadosPP.fComprimentoTrajeto,
                   dlppDadosPP.dTempoTotal);
        }
    }
    else if (bIndoDestino == true && dist < globalConfig.robotDiameter / 2.0)
    {
        bIndoDestino = false;
        vLogDadosTeste2();
        iNTeste2++;
        emit vEnviaProgressoTestePP(
            N_TESTS_PP * 2,
            iNTeste2 + N_TESTS_PP *
                           (acTestes->tsDadosTeste.iPathPlannerUtilizado - 1));
    }
}

void TestesRobos::vRRT(const int& id, QVector<QVector2D>& traj, float& comp,
                       double& tempo)
{
    rrtPathPlanner->vt2dCalculaTrajeto(acTestes.get(), id);
    if (dlppDadosPP.iNRecalculos == -1 || comp < 1)
        comp = acTestes->allies->getPlayer(id)->path->pathLength();
    tempo += rrtPathPlanner->dRetornaTempoCalculoCaminho();

    const int ncol = rrtPathPlanner->iDetectaColisao();
    static double tcol = 0;
    double tAtual = dlppDadosPP.etmTempoInicioDestino.nsecsElapsed() / 1e9;
    if (tAtual < tcol)
        tcol = 0;
    if (ncol != 0 && tAtual - tcol > 0.5)
    {
        dlppDadosPP.iNColisoes += ncol;
        tcol = tAtual;
    }

    if (acTestes->allies->getPlayer(id)->path->pathChanged())
        dlppDadosPP.iNRecalculos++;
}

void TestesRobos::vAStar(const int& id, QVector<QVector2D>& traj, float& comp,
                         double& tempo)
{
    asvgPathPlanner->vt2dCalculaTrajeto(acTestes.get(), id);
    if (dlppDadosPP.iNRecalculos == -1)
        comp = acTestes->allies->getPlayer(id)->path->pathLength();
    tempo += asvgPathPlanner->dRetornaTempoCalculoCaminho();

    const int ncol = asvgPathPlanner->iDetectaColisao();
    static double tcol = 0;
    double tAtual = dlppDadosPP.etmTempoInicioDestino.nsecsElapsed() / 1e9;
    if (tAtual < tcol)
        tcol = 0;
    if (ncol != 0 && tAtual - tcol > 0.5)
    {
        dlppDadosPP.iNColisoes += ncol;
        tcol = tAtual;
    }

    if (acTestes->allies->getPlayer(id)->path->pathChanged())
        dlppDadosPP.iNRecalculos++;
}

void TestesRobos::multiRobotAStar(qint8 _id, float& _comp, double& _time)
{
    if (!acTestes->allies->getPlayer(_id)->bArrived() &&
        acTestes->allies->getPlayer(_id)->path->pathLength() < 1)
    {
        mraPathPlanner->findCoordinatedPaths(acTestes.get());
        _comp = acTestes->allies->getPlayer(_id)->path->pathLength();
        _time += asvgPathPlanner->computationalTime();
    }
}

void TestesRobos::vTestePP1(int& n)
{
    bIndoDestino = true;
    if (iNTeste1 < N_TESTS_PP)
    {
        acTestes->allies->getPlayer(n)->vSetaDestino(
            acTestes->tsDadosTeste.vt2dDestinoTesteRobo);
        dlppDadosPP.vt2dAStarPath = asvgPathPlanner->vt2dCalculaTrajeto(
            acTestes.get(), n, false, &dlppDadosPP.iCelulasExpandidas,
            &dlppDadosPP.vt2dAStarPathOriginal);
        dlppDadosPP.fComprimentoA =
            acTestes->allies->getPlayer(n)->path->pathLength();
        dlppDadosPP.dTempoA = asvgPathPlanner->dRetornaTempoCalculoCaminho();
        dlppDadosPP.dTempoGrafo = asvgPathPlanner->dRetornaTempoGrafo();
        dlppDadosPP.fDistanciaObjetosA =
            asvgPathPlanner->fRetornaDistanciaObstaculos();
        int itAux = 0;
        dlppDadosPP.iIteracoesRRT = 0;
        dlppDadosPP.dTempoRRT = 0;

        do
        {
            dlppDadosPP.vt2dRRTPath = rrtPathPlanner->vt2dCalculaTrajeto(
                acTestes.get(), n, &dlppDadosPP.vt2dPtsTree,
                &dlppDadosPP.vt2dPaisTree, &dlppDadosPP.vt2dRRTPathOriginal,
                &itAux);
            dlppDadosPP.fComprimentoRRT =
                acTestes->allies->getPlayer(n)->path->pathLength();
            dlppDadosPP.fDistanciaObjetosRRT =
                rrtPathPlanner->fRetornaDistanciaObstaculos();
            dlppDadosPP.dTempoRRT +=
                rrtPathPlanner->dRetornaTempoCalculoCaminho();
            dlppDadosPP.iIteracoesRRT += itAux;
        } while (dlppDadosPP.fComprimentoRRT < 1);

        iNTeste1++;
        emit vEnviaProgressoTestePP(N_TESTS_PP, iNTeste1);

        vLogDadosTeste1();
    }
    else
    {
        acTestes->tsDadosTeste.tstTesteAtual = tstNenhum;
        dlppDadosPP.vt2dAStarPath.clear();
        dlppDadosPP.vt2dAStarPathOriginal.clear();
        dlppDadosPP.vt2dRRTPath.clear();
        dlppDadosPP.vt2dRRTPathOriginal.clear();
        dlppDadosPP.vt2dPtsTree.clear();
        dlppDadosPP.vt2dPaisTree.clear();
        asvgPathPlanner->vgGrafo->vt2dPontosObjetos.clear();
    }
    emit vEnviaTrajetosTestePathPlanner(
        asvgPathPlanner->vgGrafo, dlppDadosPP.vt2dAStarPath,
        dlppDadosPP.vt2dAStarPathOriginal, dlppDadosPP.vt2dPtsTree,
        dlppDadosPP.vt2dPaisTree, dlppDadosPP.vt2dRRTPath,
        dlppDadosPP.vt2dRRTPathOriginal);
}

void TestesRobos::vTestePP2()
{
    if (iNTeste2 < N_TESTS_PP)
    {
        vVaiParaInicio();
        vVaiParaDestino();
        // Movimenta os obstáculos
        vTesteMovimentacaoClick(iTrajetosObstaculos);
    }
    else if (acTestes->tsDadosTeste.iPathPlannerUtilizado == 1)
    {
        acTestes->tsDadosTeste.iPathPlannerUtilizado = 2;
        iNTeste2 = 0;
        vLogNovoTeste2("AStar");
    }
    else
    {
        acTestes->tsDadosTeste.tstTesteAtual = tstNenhum;
    }
}

void TestesRobos::antipodalTest()
{
    Robo* robot;
    bool allArrived = true;
    static bool arrivedGoal = false;

    dlppDadosPP.minDistances.append(computeMinDistance());
    
    for (int id = 0; id < MAX_IDS; ++id)
    {
        robot = acTestes->allies->getPlayer(id);
        dlppDadosPP.totalDistance[id] +=
            dlppDadosPP.robotsLastPosition[id].distanceToPoint(
                robot->vt2dPosicaoAtual());
        dlppDadosPP.robotsLastPosition[id] = robot->vt2dPosicaoAtual();
    }

    bool deadLocked = false;
    if (!runPlanning)
    {
        runPlanning = mraPathPlanner->isInDeadlock(acTestes.get());
        deadLocked = runPlanning;
    }

    if (!bIndoDestino && runPlanning)
    {
        for (int id = 0; id < MAX_IDS; ++id)
        {
            robot = acTestes->allies->getPlayer(id);
            robot->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
            robot->vSetaDestino(antipodalStart.at(id));
        }
        if (USE_MRASTAR)
        {
            mraPathPlanner->findCoordinatedPaths(acTestes.get());
            qInfo() << "MRA* -> Inicio | CP = "
                    << mraPathPlanner->computationalTime() << " ms";
            dlppDadosPP.computationTime += mraPathPlanner->computationalTime();
            dlppDadosPP.mapTime += mraPathPlanner->mapUpdateTime();
            runPlanning = false;
            if (deadLocked == false)
            {
                navigationTimer.restart();
            }
        }
    }
    else if (runPlanning)
    {
        for (int id = 0; id < MAX_IDS; ++id)
        {
            robot = acTestes->allies->getPlayer(id);
            robot->vSetaDestino(antipodalEnd.at(id));
        }
        if (USE_MRASTAR)
        {
            mraPathPlanner->findCoordinatedPaths(acTestes.get());
            qInfo() << "MRA* -> Destino | CP = "
                    << mraPathPlanner->computationalTime() << " ms";
            dlppDadosPP.computationTime += mraPathPlanner->computationalTime();
            dlppDadosPP.mapTime += mraPathPlanner->mapUpdateTime();
            runPlanning = false;
            if (deadLocked == false)
            {
                navigationTimer.restart();
            }
        }
    }

    if (!USE_MRASTAR && runPlanning)
    {
#ifdef RUN_DVG_ONCE
        runPlanning = false;
#endif
        double computationalTime = 0, mapTime = 0;
        for (int id = 0; id < MAX_IDS; ++id)
        {
            asvgPathPlanner->vt2dCalculaTrajeto(acTestes.get(), id);
            computationalTime += asvgPathPlanner->computationalTime();
            mapTime += asvgPathPlanner->mapUpdateTime();
        }
        dlppDadosPP.computationTime += computationalTime;
        dlppDadosPP.mapTime += mapTime;
    }
    
    for (int id = 0; id < MAX_IDS; ++id)
    {
        robot = acTestes->allies->getPlayer(id);
        allArrived = allArrived & robot->bArrived();
    }

    if (allArrived)
    {
        if (!arrivedGoal)
        {
            double navTime = navigationTimer.nsecsElapsed() / 1e9;
            qInfo() << "Tempo de navegação:" << navTime << "s";
            if (!discardLog)
            {
                iNTeste2++;
            }

            dlppDadosPP.navigationTime = navTime;
            arrivedGoal = true;
            for (int id = 0; id < MAX_IDS; ++id)
            {
                robot = acTestes->allies->getPlayer(id);
                dlppDadosPP.robotsLastPosition[id] = robot->vt2dPosicaoAtual();
            }

            logDataTestsMRADVG();
            navigationTimer.restart();
        }
        else if (navigationTimer.nsecsElapsed() / 1e9 > 1.5)
        {
            qInfo() << "Finished waiting...";
            bIndoDestino = !bIndoDestino;
            runPlanning = true;
            arrivedGoal = false;
            navigationTimer.restart();
        }
    }
    if (navigationTimer.nsecsElapsed() / 1e9 > 30)
    {
        qInfo() << "Failed!";
        bIndoDestino = !bIndoDestino;
        runPlanning = true;
        arrivedGoal = false;
        dlppDadosPP.navigationTime = 30;
        for (int id = 0; id < MAX_IDS; ++id)
        {
            robot = acTestes->allies->getPlayer(id);
            dlppDadosPP.robotsLastPosition[id] = robot->vt2dPosicaoAtual();
        }
        logDataTestsMRADVG();
        discardLog = true;
        navigationTimer.restart();
    }
}

void TestesRobos::markingTest()
{
    if (acTestes->allies->teamColor() == timeAzul)
    {
        randomTest();
    }
    else
    {
        opponentMarking();
    }

    emit vEnviaProgressoTestePP(N_TESTS_PP, iNTeste2);
}

void TestesRobos::opponentMarking()
{
    Robo* robot;
    QVector2D pos;
    float angle;
    const int dist = 600 + 100 - globalConfig.robotDiameter;
    for (int id = 0; id < MAX_IDS; ++id)
    {
        robot = acTestes->allies->getPlayer(id);
        pos = acTestes->opponents->getPlayer(id)->vt2dPosicaoAtual();
        angle = acTestes->opponents->getPlayer(id)->fAnguloAtual();

        robot->vSetaPontoAnguloDestino(pos);
        pos = pos + QVector2D(qCos(angle), qSin(angle)) * dist;

        robot->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);
        robot->vSetaDestino(pos);
        asvgPathPlanner->vt2dCalculaTrajeto(acTestes.get(), id);
    }
}

void TestesRobos::randomTest()
{
    Robo* robot;
    bool allArrived = true;
    static bool arrivedGoal = false;
    const bool recalculationMode = true;

    dlppDadosPP.minDistances.append(computeMinDistance());
    
    for (int id = 0; id < MAX_IDS; ++id)
    {
        robot = acTestes->allies->getPlayer(id);
        dlppDadosPP.totalDistance[id] +=
            dlppDadosPP.robotsLastPosition[id].distanceToPoint(
                robot->vt2dPosicaoAtual());
        dlppDadosPP.robotsLastPosition[id] = robot->vt2dPosicaoAtual();
    }

    bool deadLocked = false;
    if (!runPlanning)
    {
        runPlanning = mraPathPlanner->isInDeadlock(acTestes.get());
        deadLocked = runPlanning;
    }

    if (!bIndoDestino && runPlanning)
    {
        for (int id = 0; id < MAX_IDS; ++id)
        {
            robot = acTestes->allies->getPlayer(id);
            robot->vSetMaxLinearVelocity(globalConfig.robotVelocities.linear.limit);

            if (!deadLocked)
            {
                QVector2D randomGoal;
                randomGoal = generateRandomGoal();
                for (int n = id - 1; n >= 0; --n)
                {
                    QVector2D otherGoal =
                        acTestes->allies->getPlayer(n)->vt2dDestino();
                    while (randomGoal.distanceToPoint(otherGoal) < 700)
                    {
                        randomGoal = generateRandomGoal();
                        n = id - 1;
                    }
                }

                robot->vSetaDestino(randomGoal);
            }
        }
        if (USE_MRASTAR)
        {
            mraPathPlanner->findCoordinatedPaths(acTestes.get());
            qInfo() << "MRA* -> Inicio | CP = "
                    << mraPathPlanner->computationalTime() << " ms";
            dlppDadosPP.computationTime += mraPathPlanner->computationalTime();
            dlppDadosPP.mapTime += mraPathPlanner->mapUpdateTime();
            runPlanning = false;
            if (deadLocked == false)
            {
                navigationTimer.restart();
            }
        }
    }
    else if (runPlanning)
    {
        for (int id = 0; id < MAX_IDS; ++id)
        {
            robot = acTestes->allies->getPlayer(id);

            if (!deadLocked)
            {
                QVector2D randomGoal;
                randomGoal = generateRandomGoal();
                for (int n = id - 1; n >= 0; --n)
                {
                    QVector2D otherGoal =
                        acTestes->allies->getPlayer(n)->vt2dDestino();
                    while (randomGoal.distanceToPoint(otherGoal) < 700)
                    {
                        randomGoal = generateRandomGoal();
                        n = id - 1;
                    }
                }

                robot->vSetaDestino(randomGoal);
            }
        }
        if (USE_MRASTAR)
        {
            mraPathPlanner->findCoordinatedPaths(acTestes.get());
            qInfo() << "MRA* -> Destino | CP = "
                    << mraPathPlanner->computationalTime() << " ms";
            dlppDadosPP.computationTime += mraPathPlanner->computationalTime();
            dlppDadosPP.mapTime += mraPathPlanner->mapUpdateTime();
            if (deadLocked == false)
            {
                navigationTimer.restart();
            }
            runPlanning = false;
        }
    }

    if ((!USE_MRASTAR && runPlanning) ||
        (!USE_MRASTAR && !runPlanning && recalculationMode))
    {
        qInfo() << "Finding paths...";
        runPlanning = false;
        float computationalTime = 0, mapTime = 0;
        for (int id = 0; id < MAX_IDS; ++id)
        {
            if (acTestes->allies->getPlayer(id)->bRoboDetectado())
            {
                asvgPathPlanner->vt2dCalculaTrajeto(acTestes.get(), id);
                computationalTime += asvgPathPlanner->computationalTime();
                mapTime += asvgPathPlanner->mapUpdateTime();
            }
        }
        dlppDadosPP.computationTime += computationalTime;
        dlppDadosPP.mapTime += mapTime;
    }
    
    for (int id = 0; id < MAX_IDS; ++id)
    {
        robot = acTestes->allies->getPlayer(id);
        if (robot->bRoboDetectado())
        {
            allArrived = allArrived & robot->bArrived(60);
        }
    }

    if (allArrived)
    {
        qInfo() << "Robos chegaram!" << arrivedGoal
                << navigationTimer.nsecsElapsed() / 1e9;
        if (!arrivedGoal)
        {
            double navTime = navigationTimer.nsecsElapsed() / 1e9;
            arrivedGoal = true;
            qInfo() << "Tempo de navegação:" << navTime << "s"
                    << "Discard?" << discardLog;
            iNTeste2++;

            dlppDadosPP.navigationTime = navTime;
            for (int id = 0; id < MAX_IDS; ++id)
            {
                robot = acTestes->allies->getPlayer(id);
                dlppDadosPP.robotsLastPosition[id] = robot->vt2dPosicaoAtual();
            }

            logDataTestsMRADVG();
            navigationTimer.restart();
        }
        else if (navigationTimer.nsecsElapsed() / 1e9 > 3)
        {
            qInfo() << "Finished waiting...";
            bIndoDestino = !bIndoDestino;
            runPlanning = true;
            arrivedGoal = false;
            navigationTimer.restart();
        }
    }
    if (navigationTimer.nsecsElapsed() / 1e9 > 30)
    {
        qInfo() << "Failed!";
        bIndoDestino = !bIndoDestino;
        iNTeste2++;
        runPlanning = true;
        arrivedGoal = false;
        dlppDadosPP.navigationTime = 30;
        for (int id = 0; id < MAX_IDS; ++id)
        {
            robot = acTestes->allies->getPlayer(id);
            dlppDadosPP.robotsLastPosition[id] = robot->vt2dPosicaoAtual();
        }
        discardLog = false;
        logDataTestsMRADVG();
        // discardLog = true;
        navigationTimer.restart();
    }
}

QVector2D TestesRobos::generateRandomGoal()
{
    QSize fieldSz = acTestes->geoCampo->szField();
    QVector2D goal;

    goal.setX(randomRng.bounded(-fieldSz.width() / 2, fieldSz.width() / 2));
    goal.setY(randomRng.bounded(-fieldSz.height() / 2, fieldSz.height() / 2));
    return goal;
}

float TestesRobos::computeMinDistance()
{
    Robo *_a, *_b;
    float minDist = 1e15, dist;
    for (int ally = 0; ally < MAX_IDS; ++ally)
    {
        _a = acTestes->allies->getPlayer(ally);
        for (int opp = 0; opp < MAX_IDS; ++opp)
        {
            _b = acTestes->opponents->getPlayer(opp);
            if (_b->bRoboDetectado())
            {
                dist = _a->vt2dPosicaoAtual().distanceToPoint(
                    _b->vt2dPosicaoAtual());
                minDist = qMin(minDist, dist);
                if (dist < 180 && navigationTimer.nsecsElapsed() -
                                          collisionTimesOpponents(ally, opp) >
                                      0.3 * 1e9)
                {
                    collisionTimesOpponents(ally, opp) =
                        navigationTimer.nsecsElapsed();
                    dlppDadosPP.collisions++;
                }
            }
        }
    }
    for (int ally = 0; ally < MAX_IDS - 1; ++ally)
    {
        _a = acTestes->allies->getPlayer(ally);
        for (int ally2 = ally + 1; ally2 < MAX_IDS; ++ally2)
        {
            _b = acTestes->allies->getPlayer(ally2);

            dist =
                _a->vt2dPosicaoAtual().distanceToPoint(_b->vt2dPosicaoAtual());
            minDist = qMin(minDist, dist);
            if (dist < 180 && navigationTimer.nsecsElapsed() -
                                      collisionTimesAllies(ally, ally2) >
                                  0.3 * 1e9)
            {
                collisionTimesAllies(ally, ally2) =
                    navigationTimer.nsecsElapsed();
                dlppDadosPP.collisions++;
            }
        }
    }
    return minDist;
}

void TestesRobos::vTesteMovimentacaoClick(const QList<QVector<int>>& trajetos)
{
    int time = static_cast<int>(acTestes->allies->teamColor()) - 1;
    const ID_Testes testeAtual = acTestes->tsDadosTeste.tstTesteAtual;
    Robo* robot;
    if (!trajetos.isEmpty())
    {
        for (int n = 0; n < MAX_IDS; ++n)
        {
            robot = acTestes->allies->getPlayer(n);
            const QVector2D posRobo = robot->vt2dPosicaoAtual();

            if (testeAtual == tstMovClick ||
                (testeAtual == tstPathPlanner &&
                 n != acTestes->tsDadosTeste.iIDRoboTeste &&
                 acTestes->tsDadosTeste.iTipoTestePathPlanner == 2))
            {
                robot->vSetMaxLinearVelocity(globalConfig.stopLinearVelocity);
                QVector<QVector2D> traj, robotTraj = robot->path->getPath();
                const QVector<int>& trajClick =
                    trajetos.at(n + time * MAX_IDS);

                vConverteTrajetoMovimentacaoClick(traj, trajClick);

                bool reached = false, forward = false;
                if (!robotTraj.isEmpty())
                {
                    reached = robotTraj.constLast().distanceToPoint(posRobo) < globalConfig.robotDiameter;

                    if (robotTraj.constLast().distanceToPoint(traj.constLast()) < globalConfig.robotDiameter)
                    {
                        forward = true;
                    }
                }
                if (reached && forward)
                {
                    QVector<QVector2D> trajReverse;
                    for (int i = 0; i < traj.size(); i++)
                    {
                        trajReverse.prepend(traj.at(i));
                    }
                    traj = trajReverse;
                }
                if(reached || robotTraj.isEmpty())
                {
                    float length = 0;
                    for (int i = 1; i < traj.size() - 1; i++)
                    {
                        length += traj.at(i).distanceToPoint(traj.at(i - 1));
                    }
                    robot->path->vSetPath(traj, length);
                }
            }
        }
    }
}

void TestesRobos::vLogNovoTeste1(const DADOS_TESTES& dados)
{
    if (fileLogTeste1.isOpen())
        fileLogTeste1.close();

    QDir dir = QDir::current();
    if (!dir.cd("TestesPathPlanners"))
    {
        dir.mkdir("TestesPathPlanners");
        dir.cd("TestesPathPlanners");
    }

    QString fileName = QLatin1String("");
    fileName += dir.path() + QLatin1String("/Teste1_");
    fileName += QDateTime::currentDateTime().toString(QStringLiteral("h:m"));
    fileName += QLatin1String(".csv");
    fileLogTeste1.setFileName(fileName);
    fileLogTeste1.open(QIODevice::ReadOnly | QIODevice::Text |
                       QIODevice::ReadWrite);

    QTextStream stream(&fileLogTeste1);

    stream << "RRT | Iteracoes"
           << ";"
           << "TempoRRT"
           << ";"
           << "ComprimentoRRT"
           << ";"
           << "InimigosRRT"
           << ";"
           << "A* | Celulas"
           << ";"
           << "TempoA"
           << ";"
           << "Grafo"
           << ";"
           << "ComprimentoA"
           << ";"
           << "InimigosA"
           << ";"
           << "Leaf Size = " << dados.iLeafSizeRRT << ";"
           << "Numero maximo iteracoes = " << dados.iNumeroIteracoesRRT << ";"
           << "Distancia minima destino = " << dados.iDistanciaMinimaRRT
           << "[mm]"
           << ";"
           << "Distancia avanco base = " << dados.fDistanciaAvancoRRT << "[mm]"
           << ";"
           << "\n";
}

void TestesRobos::vLogNovoTeste2(const QString&& pp)
{
    if (fileLogTeste2.isOpen())
        fileLogTeste2.close();

    QDir dir = QDir::current();
    dir.cd("TestesPathPlanners");

    QString fileName = QLatin1String("");
    fileName +=
        dir.path() + QStringLiteral("/Teste2_") + pp + QStringLiteral("_");
    fileName += QDateTime::currentDateTime().toString(QStringLiteral("h:m"));
    fileName += QLatin1String(".csv");
    fileLogTeste2.setFileName(fileName);
    fileLogTeste2.open(QIODevice::ReadOnly | QIODevice::Text |
                       QIODevice::ReadWrite);

    QTextStream stream(&fileLogTeste2);

    stream << "Tempo total de calculo[ms]"
           << ";"
           << "Comprimento Caminho[mm]"
           << ";"
           << "Quantidade de re-calculos"
           << ";"
           << "Tempo inicio|destino[s]"
           << ";"
           << "Quantidade de colisões"
           << ";"
           << "\n";
}

void TestesRobos::newLogTestsMRADVG(const QString&& pp)
{
    if (fileLogTesteMRADVG.isOpen())
        fileLogTesteMRADVG.close();

    QDir dir = QDir::current();
    dir.cd("TestesPathPlanners");
    if (acTestes->tsDadosTeste.iTipoTestePathPlanner == 3)
    {
        dir.cd("Teste3");
    }
    else if (acTestes->tsDadosTeste.iTipoTestePathPlanner == 4)
    {
        dir.cd("Teste4");
    }
    else if (acTestes->tsDadosTeste.iTipoTestePathPlanner == 5)
    {
        dir.cd("Teste5");
    }

    // dir.cd("ParametrizacaoPRSBC");
    // dir.cd("ParametrizacaoMRA");
    dir.cd(QString("%1_Robos").arg(MAX_IDS));

    QString prsbcConfig;
    emit requestPrSBCConfig(prsbcConfig);

    QString fileName = QLatin1String("");
    fileName += dir.path() + QStringLiteral("/") + pp + QStringLiteral("_");
    fileName += QDateTime::currentDateTime().toString(QStringLiteral("h:m"));
    fileName += QString("_%1R").arg(MAX_IDS);
    if (prsbcConfig != "PrSBC_Turned Off")
    {
        fileName += "_PrSBC";
    }
    fileName += QLatin1String(".csv");

    fileLogTesteMRADVG.setFileName(fileName);
    fileLogTesteMRADVG.open(QIODevice::ReadOnly | QIODevice::Text |
                            QIODevice::ReadWrite);

    qInfo() << "Starting log file:" << fileName;
#ifdef RUN_DVG_ONCE
    qInfo() << "RUN_DVG_ONCE";
#endif // RUN_DVG_ONCE

    QTextStream stream(&fileLogTesteMRADVG);

    stream << mraPathPlanner->exportConfig() << " | " << prsbcConfig << "\n";
    stream << "ComputationalTime_ms"
           << ";"
           << "MappingTime_ms"
           << ";"
           << "AvoidanceTime_ms"
           << ";"
           << "NavigationTime_s"
           << ";"
           << "MinimumDistance_m"
           << ";"
           << "AverageSpeed_m/s"
           << ";"
           << "Collisions"
           << ";"
           << "\n";
    runPlanning = true;
}

void TestesRobos::vConverteTrajetoMovimentacaoClick(
    QVector<QVector2D>& trajetoRobo, const QVector<int>& trajMClick)
{
    trajetoRobo.clear();
    for (int i = 2; i < trajMClick.size(); i += 2)
    {
        trajetoRobo.append(QVector2D(trajMClick.at(i), trajMClick.at(i + 1)));
    }
}

void TestesRobos::vExecutaTestes()
{
    if (bThreadAtivada)
    {
        if (acTestes->tsDadosTeste.tstTesteAtual == tstPathPlanner)
            vTestePathPlanner();
        else if (acTestes->tsDadosTeste.tstTesteAtual == tstMovClick)
            vTesteMovimentacaoClick(acTestes->tsDadosTeste.iTrajetosRobosMouse);

        if (!acTestes->tsDadosTeste.bMovimentarRobos)
        {
            for (int n = 0; n < MAX_IDS; ++n)
            {
                acTestes->allies->getPlayer(n)->vSetMaxLinearVelocity(0);
                acTestes->allies->getPlayer(n)->path->vSetPath({}, 0);
                acTestes->opponents->getPlayer(n)->path->vSetPath({}, 0);
            }
        }
        // Envia os dados para movimentar os robos
        emit vEnviaInformacoesMovimentacao(*acTestes);
    }
}
