/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file testesrobos.h
 * @brief Arquivo header com a definição da classe que executa os testes dos
 * robôs.
 * @author Leonardo da Silva Costa
 * @version 1.0
 * @date 2020-08-27
 */
#ifndef TESTESROBOS_H
#define TESTESROBOS_H

#include <QDir>
#include <QObject>
#include <QScopedPointer>
#include <QThread>
#include <QTimer>

#include "Ambiente/futbolenvironment.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Goleiro/goleiro.h"
#include "Interface/joystickrobotcontrollerinterface.h"
// #include "Jogadas/jogadas.h"
#include "Movimentacao/robotmovement.h"
#include "Path_Planning/a_star_vg.h"
#include "Path_Planning/mrastar.h"
#include "Path_Planning/path_p_rrt_2.h"
#include "qelapsedtimer.h"
#include "qrandom.h"
#include "qvector.h"
#include "qvector2d.h"

#define N_TESTS_PP 25 /**< Quantidade de vezes que o teste é repetido. */
#define USE_MRASTAR true
#ifndef NO_AVOIDANCE_ALGORITHM
#define RUN_DVG_ONCE 1
#endif // !NO_AVOIDANCE_ALGORITHM

/**
 * @brief Estrutura que contém os dados que são feitos os logs nos teste 1 e 2.
 */
typedef struct DADOS_LOG_PP
{
    QVector<QVector2D>
        vt2dAStarPath; /**< Trajeto do AStar_VG após tratamento.*/
    QVector<QVector2D>
        vt2dAStarPathOriginal;      /**< Trajeto do AStar_VG sem tratamento.*/
    QVector<QVector2D> vt2dRRTPath; /**< Trajeto do RRT após tratamento. */
    QVector<QVector2D>
        vt2dRRTPathOriginal;         /**< Trajeto do RRT sem tratamento.*/
    QVector<QVector2D> vt2dPtsTree;  /**< Pontos da árvore do RRT.*/
    QVector<QVector2D> vt2dPaisTree; /**< Pais dos pontos da árvore do RRT.*/
    float fComprimentoA;             /**< Comprimento do trajeto do AStar_VG.*/
    float fComprimentoRRT;           /**< Comprimento do trajeto do RRT.*/
    float fDistanciaObjetosA;   /**< Distância entre o trajeto do AStar_VG e os
                                   objetos.*/
    float fDistanciaObjetosRRT; /**< Distância entre o trajeto do RRT e os
                                   objetos.*/
    float
        fComprimentoTrajeto; /**< Comprimento do trajeto utilizado no teste 2.*/
    double dTempoA;          /**< Tempo de cálculo do AStar_VG. */
    double dTempoGrafo;      /**< Tempo de cálculo do VisibilityGraph.*/
    double dTempoRRT;        /**< Tempo de cálculo do RRT.*/
    double dTempoTotal;      /**< Tempo de cálculo do path-planner utilizado no
                                teste 2.*/
    int iCelulasExpandidas; /**< Células expandidas pelo AStar_VG até calcular o
                               trajeto.*/
    int iIteracoesRRT; /**< Iterações feitas pelo RRT até encontrar o trajeto.*/
    int iNRecalculos; /**< Quantidade de vezes que o trajeto foi recalculado no
                         teste 2,*/
    int iNColisoes; /**< Quantidade de colisões até chegar no destino durante o
                       teste 2.*/
    QElapsedTimer
        etmTempoInicioDestino; /**< Utilizado para medir o tempo que levou para
                                 ir do ponto inicial ao destino no teste 2.*/

    float computationTime;
    float navigationTime;
    float mapTime;
    float avoidanceTime;
    float minDistance;
    QVector<float> minDistances;
    int collisions;
    
    QVector2D robotsLastPosition[MAX_IDS];
    float totalDistance[MAX_IDS];

} DADOS_LOG_PP;

/**
 * @brief Classe utilizada para executar diversos testes com os robôs.
 * @details A ideia é que esta classe seja como se fosse a classe de Estrategia
 *
 * Até o momento, os testes disponíveis são:
 * * MovimentacaoClick
 * * TestePathPlanners
 */
class TestesRobos : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Construtor da classe.
     */
    TestesRobos();

public slots:
    /**
     * @brief
     * @param _bThreadAtivada
     */
    void vAtivaThread(const bool _bThreadAtivada);

    /**
     * @brief Recebe os dados da visão filtrados pelo Kalman
     * @param ambiente - Objeto do AmbienteCampo que possui as informações
     * atualizadas
     * @param mudouGeometria - Indica de a geometria do campo foi alterada
     */
    void vReceiveVisionData_(const VisionPacket _packet);

    /**
     * @brief Recebe os dados da interface necessários para a estratégia
     * funcionar
     * @param posTimeout - Posição inicial que os robôs ficam na hora do timeout
     * @param idGoleiro - ID do nosso goleiro
     * @param corTime - Cor do nosso time
     * @param ladoCampo - Lado que defendemos
     * @param tipoAmbiente - Tipo do ambiente de jogo (grSim/CampoReal)
     */
    void vRecebeDadosInterface(QVector2D posTimeout, int idGoleiro, int corTime,
                               int ladoCampo, bool tipoAmbiente);

    /**
     * @brief Recebe os dados do teste ativo.
     * @param dados - Dados dos testes.
     */
    void vRecebeDadosTestes(const DADOS_TESTES& dados);

    /**
     * @brief Recebe os caminhos alterados pela movimentação
     * @param acCaminhos - Caminhos dos robôs
     */
    void vRecebeCaminhosAlterados(const MotionPathFeedback& _paths);

    /**
     * @brief Trata os dados do teste de Controlar o robô com o joystick
     * @param robot
     */
    void vRecebeDadosRoboVirtualJoystick(VirtualJoystickRobot robot);

    void SLOT_recebeSolicitacaoTesteDecisaoPasse();

    void SLOT_recebeSolicitacaoTesteDecisaoChute();

signals:
    void SIGNAL_decisoesPasse(AmbienteCampo* ac);
    void SIGNAL_decisoesChute(AmbienteCampo* ac);

private:
    /**
     * @brief Realiza os teste dos path-planners.
     */
    void vTestePathPlanner();

    /**
     * @brief Atualiza o log atual do teste 1.
     */
    void vLogDadosTeste1();

    /**
     * @brief Atualiza o log atual do teste 2.
     */
    void vLogDadosTeste2();

    void logDataTestsMRADVG();

    /**
     * @brief Faz com que o robô vá para o ponto inicial do teste.
     * @details Essa função é utilizada somente no teste 2.
     */
    void vVaiParaInicio();

    /**
     * @brief Faz com que o robô vá para o ponto de destino do teste.
     * @details Essa função é utilizada somente no teste 2.
     */
    void vVaiParaDestino();

    /**
     * @brief Calcula o trajeto atual utilizando o RRT.
     * @param id ID do robô sendo utilizado para o teste.
     * @param traj Trajeto do robô. É uma referência para o
     * #AmbienteCampo::vt2dCaminhosPathPlanner do robô.
     * @param comp Comprimento do trajeto calculado.
     * @param tempo Tempo de cálculo do trajeto.
     * @details Essa função é utilizada somente no teste 2.
     */
    void vRRT(const int& id, QVector<QVector2D>& traj, float& comp,
              double& tempo);

    /**
     * @brief Calcula o trajeto atual utilizando o AStar_VG.
     * @param id ID do robô sendo utilizado para o teste.
     * @param traj Trajeto do robô. É uma referência para o
     * #AmbienteCampo::vt2dCaminhosPathPlanner do robô.
     * @param comp Comprimento do trajeto calculado.
     * @param tempo Tempo de cálculo do trajeto.
     * @details Essa função é utilizada somente no teste 2.
     */
    void vAStar(const int& id, QVector<QVector2D>& traj, float& comp,
                double& tempo);

    void multiRobotAStar(qint8 _id, float& _comp, double& _time);

    /**
     * @brief Executa o teste 1 para o robô de ID #n.
     * @param n ID do robô utilizado no teste.
     */
    void vTestePP1(int& n);

    /**
     * @brief Realiza o teste 2.
     */
    void vTestePP2();

    void antipodalTest();

    void markingTest();

    void opponentMarking();

    void randomTest();

    QVector2D generateRandomGoal();

    float computeMinDistance();

    /**
     * @brief Realiza o teste de movimentação com o click.
     * @see MovimentacaoClick.
     */
    void vTesteMovimentacaoClick(const QList<QVector<int>>& trajetos);

    /**
     * @brief Inicia um arquivo novo de logs para o teste 1.
     * @param dados Contém alguns dados/constantes que serão utilizados no RRT.
     */
    void vLogNovoTeste1(const DADOS_TESTES& dados);

    /**
     * @brief Iniciar um arquivo novo de logs para o teste 2.
     */
    void vLogNovoTeste2(const QString&& pp);

    void newLogTestsMRADVG(const QString&& pp);

    /**
     * @brief Converte um trajeto do formato utilizado na movimentação click
     * (QVector<int>para um QVector<QVector2D>.
     * @param trajetoRobo Referência para o trajeto a ser retornado.
     * @param trajMClick Trajeto no formato utliziado na MovimentacaoClick.
     */
    void vConverteTrajetoMovimentacaoClick(QVector<QVector2D>& trajetoRobo,
                                           const QVector<int>& trajMClick);

    QScopedPointer<AmbienteCampo>
        acTestes; /**< Ambiente de campo da thread de testes.*/

    QScopedPointer<A_StarVG> asvgPathPlanner; /**< Objeto do path-planner
                                                AStar_VG usado nos testes.*/
    QScopedPointer<MRAStar> mraPathPlanner; /**< Objeto do path-planner MRAStar
                                               usado nos testes.*/
    QScopedPointer<RRT> rrtPathPlanner; /**< Objeto do path-planner RRT usado
                                          nos testes.*/
    int iNTeste1; /**< Conta quantas vezes o teste 1 foi executado. */
    int iNTeste2; /**< Conta quantas vezes o teste 2 foi executado. */
    DADOS_LOG_PP dlppDadosPP; /**< Dados para armazenar no 
                                    log dos path-planners. */
    Eigen::MatrixXd collisionTimesAllies;
    Eigen::MatrixXd collisionTimesOpponents;
    bool discardLog;
        
    QList<QVector<int>>
        iTrajetosObstaculos;  /**< Trajetos que os obstáculos(robôs) no
                                teste 2 irão realizar. */
    QFile fileLogTeste1;      /**< Arquivo de log teste 1. */
    QFile fileLogTeste2;      /**< Arquivo de log do A*. */
    QFile fileLogTesteMRADVG; /**< Arquivo de log do teste 3. */
    bool bIndoDestino; /**< Indica se o robô deve mover-se em direção ao destino
                         ou ao ponto inicial. */
    QVector<QVector2D> antipodalStart;
    QVector<QVector2D> antipodalEnd;
    bool runPlanning;
    QElapsedTimer navigationTimer;
    QRandomGenerator randomRng;

    QScopedPointer<QTimer>
        tmrTestes; /**< Timer para execução das funções na thread.*/

    bool bRecebeuPrimeiroPacote; /**< Indica se a thread ja recebeu o primeiro
                                   pacote da visão*/
    bool bThreadAtivada;         /**< Indica se a thread ja foi ativada.*/
private slots:
    /**
     * @brief Slot do #tmrTestes para executar as funções de teste.
     */
    void vExecutaTestes();

signals:
    /**
     * @brief Envia os dados do teste dos path-planners para fazer o desenho.
     * @param grafo - Grafo (VisibilityGraph) utilizado pelo A_StarVG.
     * @param caminhoAStar - Caminho gerado pelo A_StarVG e devidamente tratado.
     * @param caminhoAStarOriginal - Caminho gerado pelo A_StarVG sem nenhum
     * tratamento.
     * @param pontosArvore - Pontos da árvore gerada pelo RRT.
     * @param paisArvore - Pais dos pontos da árvore gerada pelo RRT.
     * @param caminhoRRT - Caminho gerado pelo RRT e devidamente tratado.
     * @param caminhoRRTOriginal - Caminho gerado pelo RRT sem nenhum
     * tratamento.
     */
    void vEnviaTrajetosTestePathPlanner(
        const VisibilityGraph* grafo, const QVector<QVector2D>& caminhoAStar,
        const QVector<QVector2D>& caminhoAStarOriginal,
        const QVector<QVector2D>& pontosArvore,
        const QVector<QVector2D>& paisArvore,
        const QVector<QVector2D>& caminhoRRT,
        const QVector<QVector2D>& caminhoRRTOriginal);

    /**
     * @brief Envia informacoes sobre a movimentacao do robo para a thread de
     * movimentacao
     * @see MovimentacaoRobo.
     * @param _acAmbiente
     */
    void vEnviaInformacoesMovimentacao(const AmbienteCampo& _acAmbiente); //

    /**
     * @brief Envia o progresso atual do teste dos path-planners.
     * @see TestePathPlanners.
     * @param max Valor máximo dos testes.
     * @param atual Valor atual dos testes.
     */
    void vEnviaProgressoTestePP(const int& max, const int& atual);

    void requestAvoidanceComputationalTime(float& _time);

    void requestPrSBCConfig(QString& _config);
};

#endif // TESTESROBOS_H
