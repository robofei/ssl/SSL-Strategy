/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file kalmanfilter.h
 * @brief Arquivo header da classe do Filtro de Kalman.
 * @author Guilherme Luis Pauli
 * @version 1.0
 * @date 2020-08-25
 */

#ifndef KALMANFILTER_H
#define KALMANFILTER_H

#include <QDebug>
#include <QRandomGenerator>
#include <QTimer>
#include <QFileDialog>
#include <QApplication>
#include <QVector2D>

#include <iostream>

#include <Eigen/Core>
#include <Eigen/LU>      // Eigen p/ matrizes
#include <Eigen/Dense>   // Eigen p/ matrizes


/**
 * @brief Classe que implementa um Filtro de Kalman para filtrar os dados de posição
 * da bola e dos robôs
 *
 */
class KalmanFilter
{
public:
    /**
     * @brief Construtor da classe
     *
     */
    KalmanFilter();

    /**
     * @brief Destrutor da classe
     */
    ~KalmanFilter() = default;

    /**
     * @brief Reseta o Filtro de Kalman
     *
     * @param _vt2dUltimaPos - Última posição do objeto sendo filtrado (robô/bola)
     */
    void vResetKalman(QVector2D _vt2dUltimaPos);

    /**
     * @brief Executa o passo de atualização do Filtro de Kalman
     *
     * @param _vt2dPosicaoAtual - Posição atual do objeto
     */
    void vAtualizaKalman(QVector2D _vt2dPosicaoAtual);

    /**
     * @brief Executa o passo de predição do Filtro de Kalman
     *
     */
    void vPredizKalman();

    /**
     * @brief Atualiza as matrizes do Filtro de Kalman
     *
     * @param _deltaT - Intervalo de tempo entre as amostras [s]
     */
    void vAtualizaMatrizes(double _deltaT);

    /**
     * @brief Atualiza a variância em X e Y
     *
     * @param _varX
     * @param _varY
     */
    void vAtualizaVariancia(double _varX, double _varY);

    /**
     * @brief Realiza a predição de #num passos à frente
     *
     * @param _vtKalman - Vetor com as posições filtradas do objeto
     * @param _xPred - Estado anterior
     * @param _pPred - Ganho no tempo anterior
     * @param _dT - Intervalo de tempo entre as amostras [s]
     * @param num - Número de passos para a predição
     *
     * @return QVector2D - Posição no futuro
     */
    QVector2D vt2dPredizFuturo(QVector<QVector2D> _vtKalman,
                                Eigen::Matrix<double, 4, 1> _xPred,
                                Eigen::Matrix<double, 4, 4> _pPred,
                                double _dT, int num);

    // Atributos Kalman ------------------------------------------------------------------
    // Variáveis de Estado escolhidas
    double posX;/**< Posição em X [m].*/
    double posY;/**< Posição em Y [m].*/
    double velX;/**< Velocidade em X [m/s].*/
    double velY;/**< Velocidade em Y [m/s].*/

    double dt;/**< Intervalo de tempo entre as amostras [s].*/

    // Esses ângulos são utilizados para resetar o filtro caso o objeto mude
    // repentinamente de trajetória, e.g. a bola rebater em algum robô.
    double dAngAtual; /**< Ângulo da direção atual do objeto.*/
    double dAngAnterior; /**< Ângulo da direção anterior do objeto.*/

    QVector<QVector2D> vtVelocidade; /**< Vetor com a velocidade do objeto. .*/

    Eigen::Matrix<double,4,1> x;/**< Vetor de Estados.*/
    Eigen::Matrix<double,4,4> P;/**< Ganho de Kalman.*/
    Eigen::Matrix<double,4,1> x_k_1;/**< x(k-1) Estado anterior.*/
    Eigen::Matrix<double,4,4> P_k_1;/**< P(k-1) Ganho no tempo anterior.*/
    Eigen::Matrix<double,4,2> K;/**< Kk Ganho de Kalman.*/

    Eigen::Matrix<double,4,1> xPred;/**< Matriz de Transicao.*/
    Eigen::Matrix<double,4,4> pPred;/**< Matriz de Transicao.*/

    Eigen::Array<double,1,1> varX;/**< Variacao da posicao em X.*/
    Eigen::Array<double,1,1> varY;/**< Variacao da posicao em Y.*/

    Eigen::Matrix<double,4,4> ident;/**< Matriz identidade.*/

    Eigen::Matrix<double,4,4> A;/**< Matriz de Transicao.*/
    Eigen::Matrix<double,4,2> B;/**< Matriz de Controle.*/
    Eigen::Matrix<double,4,4> Q;/**< Matriz da covariancia do ruido do processo.*/
    Eigen::Matrix<double,2,2> R;/**< Matriz da covariancia do ruida da medicao.*/
    Eigen::Matrix<double,2,4> H;/**< Matriz Jacobiana do modelo.*/
    Eigen::Matrix<double,2,1> u;/**< Matriz dos coeficientes de desaceleracao(atrito).*/
};

#endif // KALMANFILTER_H
