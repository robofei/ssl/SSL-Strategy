/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "visao.h"
#include "Ambiente/sslteam.h"
#include "qvector2d.h"

Visao::Visao()
{
    udpSSLVision.reset();
    tmrKalman.reset();

    timeAtual = timeAzul;
    iTesteMeioCampo = CampoInteiro;

    fFPSKalman = 0;
    iFPSCounter.clear();

    fieldGeometry.reset(new Geometria());
    blueTeam.reset(new SSLTeam<VisionRobot>(0, XNegativo, timeAzul));
    yellowTeam.reset(new SSLTeam<VisionRobot>(0, XPositivo, timeAmarelo));
    ball.reset(new VisionBall());

    currentPacket.reset(new VisionPacket());
    iLoopCount = 0;
}

Visao::~Visao()
{
}

float Visao::fGetFPSKalman()
{
    return fFPSKalman;
}

void Visao::vAtualizaInformacoesCampo(
    const SSL_WrapperPacket& _pkgUltimoPacoteRecebido)
{
    if (_pkgUltimoPacoteRecebido.ByteSizeLong() > 0)
    {
        // Atualiza as informacoes dos robos e da bola
        // Aqui sera detectado se o robo saiu da visao ou entao, caso ele esteja
        // no campo sera adicionada sua posicao atual no vetor utilizado pelo
        // kalman para calcular sua posicao real, o mesmo vale para a bola.
        if (_pkgUltimoPacoteRecebido.has_detection())
        {
            const SSL_DetectionFrame& frameDeteccao =
                _pkgUltimoPacoteRecebido.detection();

            vDetectaObjetosForaCampo(frameDeteccao);
            vAtualizaObjetosDetectados(frameDeteccao);
        }

        // Recebe os dados de geometria do campo
        if (_pkgUltimoPacoteRecebido.has_geometry())
        {
            const SSL_GeometryData& frameGeometria =
                _pkgUltimoPacoteRecebido.geometry();
            vAtualizaGeometriaCampo(frameGeometria);
        }
    }

    if (!tmrKalman->isActive())
    {
        tmrKalman->start(iTempoLoopKalman);
        etmTimerFPSKalman.start();
    }
}

void Visao::vDetectaObjetosForaCampo(const SSL_DetectionFrame& frameDeteccao)
{
    // Se o objeto esta na mesma camera e ja se passou o numero de frames para
    // sair da visao, consideramos que ele esta fora de campo Se o frame number
    // ficar atrasado, reseta o frame number (deve acontecer somente quando
    // rodando o software com logs)
    //    acVisao->vPegaBola(blBola);

    if (frameDeteccao.camera_id() == ball.get()->iCameraID)
    {
        if (frameDeteccao.frame_number() >
            (ball.get()->iCameraFrame[ball.get()->iCameraID] +
             globalConfig.framesUntilMissing))
        {
            if (ball->bEmCampo == true)
            {
                ball->detectedFrames = 0;
            }
            ball->bEmCampo = false;
            // qInfo() << QString("[Visão] Bola sumiu! %1 > %2")
            //                    .arg(frameDeteccao.frame_number())
            //                    .arg(ball->iCameraFrame[ball->iCameraID] +
            //                         globalConfig.framesUntilMissing);
        }
        if (frameDeteccao.frame_number() -
                ball.get()->iCameraFrame[ball.get()->iCameraID] >
            static_cast<unsigned int>(2 * globalConfig.framesUntilMissing))
        {
            // ball->vResetFrames();
        }
    }

    VisionRobot* robot;
    for (qint8 n = 0; n < globalConfig.robotsPerTeam; ++n)
    {
        robot = blueTeam->getPlayer(n);

        if (frameDeteccao.camera_id() == robot->iCameraID)
        {
            if (frameDeteccao.frame_number() >
                robot->iCameraFrame[robot->iCameraID] +
                    globalConfig.framesUntilMissing)
            {
                if (robot->bRoboDetectado())
                {
                    robot->detectedFrames = 0;
                }
                robot->vSetaEmCampo(false);
                // qInfo() << QString("[Visão] Robô %1 sumiu! %2 > %3")
                //                .arg(robot->iID())
                //                .arg(frameDeteccao.frame_number())
                //                .arg(robot->iCameraFrame[robot->iCameraID] +
                //                     globalConfig.framesUntilMissing);
                // FIXME calcular isso aqui quando enviar os dados
                //                 acVisao->vSetaRobosEmCampo(0);
            }
            if (frameDeteccao.frame_number() -
                    robot->iCameraFrame[robot->iCameraID] >
                static_cast<unsigned int>(2 * globalConfig.framesUntilMissing))
            {
                // robot->vResetFrames();
            }
        }

        robot = yellowTeam->getPlayer(n);

        if (frameDeteccao.camera_id() == robot->iCameraID)
        {
            if (frameDeteccao.frame_number() >
                (robot->iCameraFrame[robot->iCameraID] +
                 globalConfig.framesUntilMissing))
            {
                if (robot->bRoboDetectado())
                {
                    robot->detectedFrames = 0;
                }
                robot->vSetaEmCampo(false);
                // FIXME calcular isso aqui quando enviar os dados
                //                 acVisao->vSetaRobosEmCampo(0);
            }
            if (frameDeteccao.frame_number() -
                    robot->iCameraFrame[robot->iCameraID] >
                static_cast<unsigned int>(2 * globalConfig.framesUntilMissing))
            {
                // robot->vResetFrames();
            }
        }
    }
}

void Visao::vAtualizaObjetosDetectados(const SSL_DetectionFrame& frameDeteccao)
{
    // Atualiza as informacoes da bola
    if (bUsarKickSensorNaVisao && frameDeteccao.balls_size() == 0)
    {
        vOverrideBallDetection(frameDeteccao.camera_id(),
                               frameDeteccao.frame_number());
    }
    else
    {
        int iNumeroBolas = frameDeteccao.balls_size();
        for (int n = 0; n < iNumeroBolas; ++n)
        {
            vAtualizaBola(frameDeteccao, n);
        }
    }

    ball->vAddTime(1.0 / dFPS);
    vUpdateTeam(blueTeam.get(), frameDeteccao);
    vUpdateTeam(yellowTeam.get(), frameDeteccao);

    // Conta quantos robos aliados tem em campo
    //  FIXME
    //     int nAux = 0;
    //     for(int n=0; n < globalConfig.robotsPerTeam; ++n)
    //     {
    //         if(acVisao->allies->getPlayer(n)->bRoboDetectado() == true)
    //             nAux++;
    //     }
    //     acVisao->vSetaRobosEmCampo(nAux);
}

void Visao::vAtualizaBola(const SSL_DetectionFrame& frameDeteccao,
                          const int nBola)
{
    const SSL_DetectionBall& ballData = frameDeteccao.balls(nBola);
    QVector2D ballPos = QVector2D(ballData.x(), ballData.y());

    // Atualiza se a bola esta no campo e o seu frame number e salva a posicao
    // recebida no vetor que sera usado no kalman
    if (iTesteMeioCampo == CampoInteiro ||
        ((ballPos.x() > 0 && iTesteMeioCampo == XPositivo) ||
         (ballPos.x() < 0 && iTesteMeioCampo == XNegativo)))
    {
        bool useConfidence = false;
        bool confidenceOk =
            ballData.confidence() > 0.9 && !qIsNaN(ballData.confidence());
        bool positionIsValid =
            !ball->vvt2dGetRawPositions().isEmpty() &&
            ball->vvt2dGetRawPositions().constFirst().distanceToPoint(ballPos) <
                500;

        if ((useConfidence && confidenceOk) || !ball->bEmCampo ||
            !useConfidence)
        {
            if (positionIsValid || !ball->bEmCampo)
            {
                if (ball->detectedFrames < 60)
                {
                    ball->detectedFrames++;
                }
                ball->iCameraID = frameDeteccao.camera_id();
                ball->iCameraFrame[frameDeteccao.camera_id()] =
                    frameDeteccao.frame_number();
            }
            if (ball->detectedFrames >= 30)
            {
                ball->bEmCampo = true;
                // Buffer da bola
                ball->vAddRawPosition(ballPos);
            }
        }
    }
}

// void Visao::vAtualizaRoboAliado(const SSL_DetectionFrame& frameDeteccao,
// const int nRobo)
// {
//     const SSL_DetectionRobot& detectAliado =
//             acVisao->allies->teamColor() == timeAmarelo ?
//             frameDeteccao.robots_yellow(nRobo) :
//                                               frameDeteccao.robots_blue(nRobo);
//
//     if(detectAliado.robot_id() >= globalConfig.robotsPerTeam)
//         return;
//
//     Robo* roboAliado = acVisao->allies->getPlayer(detectAliado.robot_id());
//     const float distRobo = roboAliado->vt2dPosicaoAtual().
//             distanceToPoint(QVector2D(detectAliado.x(),detectAliado.y()));
//
//     if(distRobo <= globalConfig.robotDiameter*0.7 ||
//     roboAliado->bRoboDetectado() == false)
//     {
//         if(iTesteMeioCampo == CampoInteiro ||
//            ( (detectAliado.x() > 0 && iTesteMeioCampo == XPositivo) ||
//              (detectAliado.x() < 0 && iTesteMeioCampo == XNegativo)) )
//         {
//             float anguloAtualizado = roboAliado->fAnguloAtual() +
//                     fCalculaAnguloRobo(detectAliado.orientation(),
//                     roboAliado->fAnguloAtual());
//
//             roboAliado->vSetaAngulo(anguloAtualizado);
//             roboAliado->vSetaEmCampo(true);
//             roboAliado->vAdicionaTempo(1/dFPS);
//             roboAliado->vSetaCamera(frameDeteccao.camera_id());
//             roboAliado->vSetaCameraFrame(frameDeteccao.frame_number(),
//             frameDeteccao.camera_id());
//
//             //Buffer utilizado no calculo do kalman
//             acVisao->vtRobosAliadosVisao[roboAliado->iID()].prepend(QVector2D(detectAliado.x(),
//             detectAliado.y()));
//
//             if(acVisao->vtRobosAliadosVisao[roboAliado->iID()].size() >
//             iTamanhoBufferRobo)
//                 acVisao->vtRobosAliadosVisao[roboAliado->iID()].pop_back();
//         }
//     }
// }
//
// void Visao::vAtualizaRoboAdversario(const SSL_DetectionFrame& frameDeteccao,
// const int nRobo)
// {
//     const SSL_DetectionRobot& detectAdversaio =
//             acVisao->opponents->teamColor() == timeAmarelo ?
//             frameDeteccao.robots_yellow(nRobo) :
//                                            frameDeteccao.robots_blue(nRobo);
//
//     if(detectAdversaio.robot_id() >= globalConfig.robotsPerTeam)
//         return;
//
//     Robo* roboAdversario =
//     acVisao->opponents->getPlayer(detectAdversaio.robot_id()); const float
//     distRobo = roboAdversario->vt2dPosicaoAtual().
//             distanceToPoint(QVector2D(detectAdversaio.x(),detectAdversaio.y()));
//
//     if(distRobo <= globalConfig.robotDiameter*0.7 ||
//     roboAdversario->bRoboDetectado() == false)
//     {
//         if(iTesteMeioCampo == CampoInteiro ||
//            ( (detectAdversaio.x() > 0 && iTesteMeioCampo == XPositivo) ||
//              (detectAdversaio.x() < 0 && iTesteMeioCampo == XNegativo)) )
//         {
//             roboAdversario->vSetaAngulo(detectAdversaio.orientation());
//             roboAdversario->vSetaEmCampo(true);
//             roboAdversario->vAdicionaTempo(1/dFPS);
//             roboAdversario->vSetaCamera(frameDeteccao.camera_id());
//             roboAdversario->vSetaCameraFrame(frameDeteccao.frame_number(),
//             frameDeteccao.camera_id());
//
//             //Buffer utilizado no calculo do kalman
//             acVisao->vtRobosAdversariosVisao[roboAdversario->iID()].prepend(QVector2D(detectAdversaio.x(),
//             detectAdversaio.y()));
//
//             if(acVisao->vtRobosAdversariosVisao[roboAdversario->iID()].size()
//             > iTamanhoBufferRobo)
//                 acVisao->vtRobosAdversariosVisao[roboAdversario->iID()].pop_back();
//         }
//     }
// }

void Visao::vUpdateTeam(SSLTeam<VisionRobot>* _team,
                        const SSL_DetectionFrame& _detection)
{
    SSL_DetectionRobot robotData;
    qint8 detectedRobots = _detection.robots_yellow_size();
    bool isYellow = true;
    if (_team->teamColor() == timeAzul)
    {
        detectedRobots = _detection.robots_blue_size();
        isYellow = false;
    }

    for (qint8 n = 0; n < detectedRobots; ++n)
    {
        if (_team->teamColor() == timeAzul)
        {
            robotData = _detection.robots_blue(n);
        }
        else
        {
            robotData = _detection.robots_yellow(n);
        }

        int id = robotData.robot_id();
        if (isYellow)
        {
            id = robotData.robot_id();
        }

        if (id >= globalConfig.robotsPerTeam)
        {
            // qWarning() << "[Visao] Recebendo IDs fora do limite máximo";
            continue;
        }

        VisionRobot* robot = _team->getPlayer(id);

        float distRobo = robot->vt2dPosicaoAtual().distanceToPoint(
            QVector2D(robotData.x(), robotData.y()));

        if (distRobo <= globalConfig.robotDiameter * 0.7 ||
            robot->bRoboDetectado() == false)
        {
            if (iTesteMeioCampo == CampoInteiro ||
                ((robotData.x() > 0 && iTesteMeioCampo == XPositivo) ||
                 (robotData.x() < 0 && iTesteMeioCampo == XNegativo)))
            {
                if (robot->detectedFrames < 30)
                {
                    robot->detectedFrames++;
                }
                robot->iCameraID = _detection.camera_id();
                robot->iCameraFrame[_detection.camera_id()] =
                    _detection.frame_number();
            }

            if (robot->detectedFrames >= 30)
            {
                float anguloAtualizado =
                    robot->fAnguloAtual() +
                    fCalculaAnguloRobo(robotData.orientation(),
                                       robot->fAnguloAtual());

                robot->vSetaAngulo(anguloAtualizado);
                robot->vSetaEmCampo(true);
                robot->vAdicionaTempo(1 / dFPS);

                // Buffer utilizado no calculo do kalman
                robot->vAddRawPosition(QVector2D(robotData.x(), robotData.y()));
            }
        }
    }
}

void Visao::vAtualizaGeometriaCampo(const SSL_GeometryData& frameGeometria)
{
    const SSL_GeometryFieldSize& geometriaCampo = frameGeometria.field();
    const google::protobuf::RepeatedPtrField<SSL_FieldCircularArc>& arcosCampo =
        geometriaCampo.field_arcs();
    const google::protobuf::RepeatedPtrField<SSL_FieldLineSegment>& retasCampo =
        geometriaCampo.field_lines();

    SSL_FieldCircularArc circulosCampo[arcosCampo.size()];
    SSL_FieldLineSegment linhasCampo[retasCampo.size()];

    // Recebe os circulos do campo
    for (int n = 0; n < arcosCampo.size(); ++n)
    {
        circulosCampo[n] = arcosCampo.Get(n);

        if (circulosCampo[n].name() == "CenterCircle")
            fieldGeometry->vSetCenterCircleSize(circulosCampo[n].radius());
    }

    // Dimensoes da area de defesa
    int profundidadeDefesa = -1, larguraDefesa = -1;
    // Recebe linhas do campo
    for (int n = 0; n < retasCampo.size(); ++n)
    {
        linhasCampo[n] = retasCampo.Get(n);

        QPoint p1, p2;
        p1.setX(linhasCampo[n].p1().x());
        p1.setY(linhasCampo[n].p1().y());

        p2.setX(linhasCampo[n].p2().x());
        p2.setY(linhasCampo[n].p2().y());

        if (linhasCampo[n].name() == "LeftPenaltyStretch" ||
            linhasCampo[n].name() == "RightPenaltyStretch")
        {

            larguraDefesa = QVector2D(p2 - p1).length();
        }

        else if (linhasCampo[n].name() == "LeftFieldLeftPenaltyStretch" ||
                 linhasCampo[n].name() == "LeftFieldRightPenaltyStretch" ||
                 linhasCampo[n].name() == "RightFieldRightPenaltyStretch" ||
                 linhasCampo[n].name() == "RightFieldLeftPenaltyStretch")
        {
            profundidadeDefesa = QVector2D(p2 - p1).length();
        }
    }

    if (profundidadeDefesa != -1 && larguraDefesa != -1)
    {
        fieldGeometry->vSetDefenseAreaSize(
            QSize(profundidadeDefesa, larguraDefesa));
    }

    // Dimensoes do gol
    fieldGeometry->vSetGoalSize(
        QSize(geometriaCampo.goal_depth(), geometriaCampo.goal_width()));

    // Atualiza o tamanho do campo
    if (fieldGeometry->szField() !=
        QSize(geometriaCampo.field_length(), geometriaCampo.field_width()))
    {
        currentPacket->bGeometryChanged = true;
        fieldGeometry->vSetFieldSize(
            QSize(geometriaCampo.field_length(), geometriaCampo.field_width()));
    }

    if (currentPacket->bGeometryChanged == true)
    {
        ball->vResetFrames();

        for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
        {
            blueTeam->getPlayer(n)->vResetFrames();
            yellowTeam->getPlayer(n)->vResetFrames();
        }
    }
}

float Visao::fCalculaAnguloRobo(float orientacaoVisao, float anguloAtual)
{
    // Usar angulo de -inf a +inf
    float anguloVisao = orientacaoVisao, anguloAtualReduzido = anguloAtual;
    if (anguloVisao < 0) // [-180;180] -> [0,360]
        anguloVisao += 2 * M_PI;

    if (qAbs(anguloAtualReduzido) > 2 * M_PI)
    {
        float fatorConversao = anguloAtualReduzido / (2 * M_PI) -
                               trunc(anguloAtualReduzido / (2 * M_PI));
        anguloAtualReduzido = fatorConversao * 2 * M_PI;
    }

    QVector2D vt2dAnguloVisao(qCos(anguloVisao), qSin(anguloVisao)),
        vt2dAnguloAtual(qCos(anguloAtualReduzido), qSin(anguloAtualReduzido));

    // Calcula a variacao de angulo em graus
    float anguloIncremento =
        Auxiliar::dAnguloVetor1Vetor2(vt2dAnguloVisao, vt2dAnguloAtual) * M_PI /
        180;

    // Se o sinal do anguloVisao e do anguloReduzido forem diferentes,
    // transforma o anguloReduzido para um angulo positivo
    if (qRound(anguloAtualReduzido / qAbs(anguloAtualReduzido)) !=
        qRound(anguloVisao / qAbs(anguloVisao)))
        anguloAtualReduzido += 2 * M_PI;

    // Multiplica o incremento pelo sinal da diferença entre os angulos, isso
    // fara com que o incremento seja somado caso anguloVisao > anguloReduzido,
    // ou seja subtraido caso anguloVisao < anguloReduzido
    anguloIncremento *= qRound((anguloVisao - anguloAtualReduzido) /
                               qAbs(anguloVisao - anguloAtualReduzido));

    // Se houve troca de quadrante do angulo visao do 1 para o 4 ou 4 para o 1,
    // devemos inverter o sinal
    if (Auxiliar::iCalculaQuadrante(anguloVisao) == 4 &&
        Auxiliar::iCalculaQuadrante(anguloAtualReduzido) == 1)
        anguloIncremento *= -1;
    else if (Auxiliar::iCalculaQuadrante(anguloVisao) == 1 &&
             Auxiliar::iCalculaQuadrante(anguloAtualReduzido) == 4)
        anguloIncremento *= -1;

    return anguloIncremento;
}

void Visao::vRunRobotKalman(VisionRobot* _robot)
{
    QVector2D vt2dPosicaoKalmanRobos; //, vt2dVar;

    if (_robot->vvt2dGetRawPositions().size() > 2 && _robot->bRoboDetectado())
    {
        vt2dPosicaoKalmanRobos = _robot->vvt2dGetRawPositions().constFirst();
        //         vt2dVar =
        //         vt2dCalculaVariancia(_robot->vvt2dGetRawPositions());
        //
        //         vt2dVar.setX(qPow(vt2dVar.x(), 2));
        //         vt2dVar.setY(qPow(vt2dVar.y(), 2));

        _robot->kfFilter->vAtualizaVariancia(
            0.1, 0.1); // vt2dVar.x(),vt2dVar.y()); comentado p/ testes

        // Reset do filtro quando a direção mudar muito
        QVector2D rawDifference = _robot->vvt2dGetRawPositions().at(0) -
                                  _robot->vvt2dGetRawPositions().at(1);

        if (qAbs(rawDifference.y()) < 2 && qAbs(rawDifference.x()) < 2)
        {
            _robot->kfFilter->dAngAtual = 0.0;
        }
        else
        {
            _robot->kfFilter->dAngAtual =
                qAtan2(rawDifference.y(), rawDifference.x());
        }

        // 0.52 ~= 30⁰
        if (qAbs(qAbs(_robot->kfFilter->dAngAtual) -
                 qAbs(_robot->kfFilter->dAngAnterior)) > 0.52f &&
            qAbs(rawDifference.length()) > 15.0f)
        {
            _robot->kfFilter->vResetKalman(vt2dPosicaoKalmanRobos);
        }

        // Velocidade do robô em m/s
        _robot->kfFilter->vAtualizaMatrizes(21e-3 /*iTempoLoopKalman*1e-3*/);
        _robot->kfFilter->vPredizKalman();
        _robot->kfFilter->vAtualizaKalman(vt2dPosicaoKalmanRobos);

        _robot->kfFilter->xPred = _robot->kfFilter->x;
        _robot->kfFilter->pPred = _robot->kfFilter->P;

        // Conversão de m para mm
        vt2dPosicaoKalmanRobos =
            QVector2D(_robot->kfFilter->x(0, 0), _robot->kfFilter->x(1, 0)) *
            1e3;
        _robot->vAddFilteredPosition(vt2dPosicaoKalmanRobos);

        if (vt2dPosicaoKalmanRobos.length() < 10e6 &&
            qIsFinite(vt2dPosicaoKalmanRobos.length()))
        {
            _robot->vSetaPosicao(vt2dPosicaoKalmanRobos);

            if (_robot->vvt2dGetFilteredPositions().size() ==
                iTamanhoBufferRobo)
            {
                double deslocamentoTotal = 0, velX = 0, velY = 0,
                       tempo = iTamanhoBufferRobo * iTempoLoopKalman;
                QVector2D initPos =
                    _robot->vvt2dGetFilteredPositions().constFirst();
                for (QVector2D pos : _robot->vvt2dGetFilteredPositions())
                {
                    velX += (initPos - pos).x();
                    velY += (initPos - pos).y();
                    deslocamentoTotal += initPos.distanceToPoint(pos);
                    initPos = pos;
                }
                QVector2D robotVel(velX / tempo, velY / tempo);
                _robot->vSetaModuloVelocidade(deslocamentoTotal / tempo);
                _robot->setVelocity(robotVel);
                
                // velocidade m/s -> mm/s
                QVector2D prediction = vt2dPosicaoKalmanRobos + robotVel * 1000;
                _robot->setPrediction(prediction);
            }
        }
        _robot->kfFilter->dAngAnterior = _robot->kfFilter->dAngAtual;

        // -------------------- prediçao
        //        if(vtPredicao != nullptr)
        //        {
        //            QVector2D vt2dPontoFuturo = vtBufferPosVisao.constFirst();
        //            vt2dPontoFuturo =
        //            kalmanRobos.vt2dPredizFuturo(vtBufferPosVisao,
        //                                                           kalmanRobos.xPred,kalmanRobos.pPred,1/dFPS,
        //                                                           15);

        //            vtPredicao->prepend(vt2dPontoFuturo);
        //            if(vtPredicao->size() > 4)
        //                vtPredicao->pop_back();
        //        }
        // --------------------
    }
}

void Visao::vRunBallKalman()
{
    QVector2D vt2dKalmanBola;

    // Atualiza a posicao da bola com o filtro de kalman
    if (ball->vvt2dGetRawPositions().size() >= 2)
    {
        // ------------- RESET
        QVector2D rawDifference = ball->vvt2dGetRawPositions().at(0) -
                                  ball->vvt2dGetRawPositions().at(1);
        if (qAbs(rawDifference.y()) < 2 && qAbs(rawDifference.x()) < 2)
        {
            ball->kfFilter->dAngAtual = 0.0;
        }
        else
        {
            ball->kfFilter->dAngAtual =
                qAtan2(rawDifference.y(), rawDifference.x());
        }

        // 0.26 ~= 15⁰
        if (qAbs(qAbs(ball->kfFilter->dAngAtual) -
                 qAbs(ball->kfFilter->dAngAnterior)) > 0.26f &&
            qAbs(rawDifference.length()) > 5.0f)
        {
            ball->kfFilter->vResetKalman(
                ball->vvt2dGetRawPositions().constFirst());
        }
        ball->kfFilter->dAngAnterior = ball->kfFilter->dAngAtual;
        // ------------- RESET

        // ------------- CALCULO VELOCIDADE
        //        double dVel;
        //        vt2dkb.prepend( QVector2D(kalmanBola.x(0),kalmanBola.x(1)) );

        //        if(vt2dkb.size() > 1)
        //        {
        //            if(kalmanBola.dAngAtual == 0.0 ||
        //            (acVisao->vtBufferPosBolaVisao.at(0) -
        //            acVisao->vtBufferPosBolaVisao.at(1)).length() <= 10 )
        //                dVel = 0.0;
        //            else
        //                dVel = (QVector2D(acVisao->vtBufferPosBolaVisao.at(0)
        //                - acVisao->vtBufferPosBolaVisao.at(1))*dFPS).length();

        //            dVelTeste = (dVelocidadeBola(dVel))/1e3;
        //        }

        //        if(vt2dkb.size() > 2)
        //            vt2dkb.pop_back();
        // ------------- CALCULO VELOCIDADE
        // QVector2D vt2dVar =
        //         vt2dCalculaVariancia(ball->vvt2dGetRawPositions());
        // ball->kfFilter->vAtualizaVariancia(vt2dVar.x(), vt2dVar.y());
        ball->kfFilter->vAtualizaVariancia(0.01, 0.01);

        if (ball->bDetected() == false)
        {
            ball->kfFilter->vAtualizaMatrizes(21e-3);
            ball->kfFilter->vPredizKalman();
            ball->kfFilter->x = ball->kfFilter->x_k_1;
            ball->kfFilter->P = ball->kfFilter->P_k_1;
        }
        else
        {
            ball->kfFilter->vAtualizaMatrizes(21e-3);
            ball->kfFilter->vPredizKalman();
            ball->kfFilter->vAtualizaKalman(
                ball->vvt2dGetRawPositions().constFirst());
        }

        ball->kfFilter->xPred = ball->kfFilter->x;
        ball->kfFilter->pPred = ball->kfFilter->P;

        // Conversão de m para mm
        vt2dKalmanBola =
            QVector2D(ball->kfFilter->x(0), ball->kfFilter->x(1)) * 1e3;
        QVector2D ballCovariance;
        ballCovariance.setX(ball->kfFilter->P(0, 0));
        ballCovariance.setY(ball->kfFilter->P(1, 1));

        // Esta funcao ja atribui a posicao atual na anterior e a atual na atual
        ball->vSetPosition(vt2dKalmanBola, ballCovariance);
        ball->vAddFilteredPosition(vt2dKalmanBola);

        // -------------------- prediçao
        // QVector2D vt2dPontoFuturo = ball->kfFilter->vt2dPredizFuturo(
        //     ball->vvt2dGetFilteredPositions(), ball->kfFilter->xPred,
        //     ball->kfFilter->pPred, 1 / dFPS, 100);
        // ball->vAddPredictedPosition(vt2dPontoFuturo *
        //                             1e3); // conversão de m -> mm
        //                                   // --------------------

        // Calcula a velocidade da bola se existem pelo menos duas posiçoes
        if (ball->vvt2dGetFilteredPositions().size() == iTamanhoBufferBola)
        {
            double dt = iTempoLoopKalman * iTamanhoBufferBola;
            QVector2D ds(0, 0);

            for (quint16 i = 0;
                 i < ball->vvt2dGetFilteredPositions().size() - 1; i++)
            {
                ds += ball->vvt2dGetFilteredPositions().at(i) -
                      ball->vvt2dGetFilteredPositions().at(i + 1);
            }
            ball->vSetVelocity(ds / dt);
        }
    }
}

QVector2D Visao::vt2dCalculaVariancia(QVector<QVector2D> _vtBufferPosicoes)
{
    float fMediaPosX = 0, fMediaPosY = 0;
    float varX = 0.7, varY = 0.7;

    if (_vtBufferPosicoes.size() >= 0)
    {
        foreach (QVector2D pos,
                 _vtBufferPosicoes) // for(int i=0;i<(_iqtdAmostras);i++)
        {
            fMediaPosX += pos.x();
            fMediaPosY += pos.y();
        }

        fMediaPosX = fMediaPosX / _vtBufferPosicoes.size();
        fMediaPosY = fMediaPosY / _vtBufferPosicoes.size();
        varX = varY = 0;

        foreach (QVector2D pos, _vtBufferPosicoes)
        {
            varX += qPow((pos.x() - fMediaPosX), 2);
            varY += qPow((pos.y() - fMediaPosY), 2);
        }

        varX = varX / (_vtBufferPosicoes.size() - 1);
        varY = varY / (_vtBufferPosicoes.size() - 1);
    }

    //    std::cout<<"id: "<<_atbRobo.id<<"varX: "<<varX<<" || varY:
    //    "<<varY<<std::endl;
    return QVector2D(varX, varY);
}

void Visao::vOverrideBallDetection(uint _cameraID, uint _cameraFrame)
{
    // FIXME: Isso aqui é um caso de fusão de sensores e deve ser feito da
    // maneira apropriada (usando o kalman)
    VisionRobot* r;

    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        if (timeAtual == timeAzul)
        {
            r = blueTeam->getPlayer(id);
        }
        else
        {
            r = yellowTeam->getPlayer(id);
        }

        // Verifica se algum robô está detectando a bola no kick sensor
        // Caso haja, rouba e diz que a bola está detectada na frente do robô
        if (r->bKickSensor())
        {
            const QVector2D sensorBall =
                r->vt2dPosicaoAtual() +
                r->vt2dOrientation() * globalConfig.robotDistanceToBall;

            if ((!ball->vvt2dGetFilteredPositions().isEmpty() &&
                 ball->vvt2dGetFilteredPositions().constFirst().distanceToPoint(
                     sensorBall) < 500) ||
                !ball->bEmCampo)
            {
                ball->bEmCampo = true;
                ball->iCameraID = _cameraID;
                ball->iCameraFrame[ball->iCameraID] = _cameraFrame;

                // Buffer da bola
                ball->vAddRawPosition(sensorBall);
            }
            break;
        }
    }
}

void Visao::vConectarInterfaceRede(const QString& strInterface)
{
    udpSSLVision.reset(new QUdpSocket(this));
    int visionPort = globalConfig.network.vision.port;
    QString visionIP = globalConfig.network.vision.ip;

    // Coneta-se à porta que envia os pacotes da visão
    if (udpSSLVision->bind(QHostAddress::AnyIPv4, visionPort,
                           QUdpSocket::ShareAddress) &&
        udpSSLVision->joinMulticastGroup(
            QHostAddress(visionIP),
            QNetworkInterface::interfaceFromName(strInterface)))
    {
        // Conecta o sinal de chegada de pacote ao slot que irá tratá-los
        connect(udpSSLVision.get(), &QUdpSocket::readyRead, this,
                &Visao::vRecebePacotesVisao);
    }
    else
    {
        qCritical() << "Não foi possível se conectar com a visão adequadamente";
    }

    // Inicializa o timer do Kalman
    tmrKalman.reset(new QTimer(this));
    tmrKalman->setTimerType(Qt::PreciseTimer);

    // Conecta o slot do Kalman com o timer
    connect(tmrKalman.get(), &QTimer::timeout, this, &Visao::vFiltroKalman);
}

void Visao::vDesconectar()
{
    // Desconecta-se do sinal de chegada de pacote e fecha a porta de
    // recebimento de dados
    udpSSLVision->disconnect(SIGNAL(readyRead()));
    udpSSLVision->close();

    // Desconecta o slot do Kalman
    tmrKalman->stop();
    tmrKalman.reset();
}

void Visao::vRecebePacotesVisao()
{
    while (udpSSLVision->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpSSLVision->pendingDatagramSize());
        datagram.fill(0, udpSSLVision->pendingDatagramSize());
        udpSSLVision->readDatagram(datagram.data(), datagram.size());

        SSL_WrapperPacket sslvision;
        sslvision.ParseFromArray(datagram, datagram.size());

        vAtualizaInformacoesCampo(sslvision);
    }
}

void Visao::vSetaTesteMeioCampo(int _testeCampo)
{
    iTesteMeioCampo = _testeCampo;
}

void Visao::vTrocaTimeAtual(int _timeAtual)
{
    timeAtual = static_cast<TeamColor>(_timeAtual);
}

void Visao::vReceiveRadioData_(const RadioFeedback& _feedback)
{
    VisionRobot* r;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        if (timeAtual == timeAzul)
        {
            r = blueTeam->getPlayer(id);
        }
        else
        {
            r = yellowTeam->getPlayer(id);
        }

        if (r->bRoboDetectado())
        {
            r->vAtualizaDadosRadio(_feedback);
        }
    }
}

void Visao::vFiltroKalman()
{
    QElapsedTimer elapsedTempoDeExecucao;
    elapsedTempoDeExecucao.start();

    fFPSKalman = 1.f / (etmTimerFPSKalman.nsecsElapsed() / 1e9);
    iFPSCounter.prepend(qRound(fFPSKalman));
    fFPSKalman = Auxiliar::iCalculaFPS(iFPSCounter);

    //    qDebug() << "Tempo Kalman = " <<
    //    qRound(etmTimerFPSKalman.nsecsElapsed()/1e6) << " ms";
    etmTimerFPSKalman.restart();

    // Kalman para os robos
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        if (blueTeam->getPlayer(id)->bRoboDetectado() == true)
        {
            vRunRobotKalman(blueTeam->getPlayer(id));
        }
        if (yellowTeam->getPlayer(id)->bRoboDetectado() == true)
        {
            vRunRobotKalman(yellowTeam->getPlayer(id));
        }
    }

    // Kalman para bola
    vRunBallKalman();

    // Atualiza os dados filtrados a cada 60Hz para as outras threads
    if (iLoopCount++ >= 5.0 / iTempoLoopKalman)
    {
        iLoopCount = 0;
        emit _vVisionData(*currentPacket.get());
        currentPacket->bGeometryChanged = false;
    }
    else
    {
        currentPacket->vUpdateInfo(fieldGeometry.get(), blueTeam.get(),
                                   yellowTeam.get(), ball.get());
    }

    // double tempoTotal = elapsedTempoDeExecucao.nsecsElapsed()*1e-6;
    // if(tempoTotal < iTempoLoopKalman)
    //     return;
    //
    // if(tempoTotal <1.2*iTempoLoopKalman)
    //     qWarning() << "[W] Tempo Kalman " << QString::number(tempoTotal, 'f',
    //     2) << " ms";
    // else
    //     qCritical() << "[C] Tempo Kalman " << QString::number(tempoTotal,
    //     'f', 2) << " ms";
}
