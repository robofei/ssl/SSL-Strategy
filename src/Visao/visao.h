/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file visao.h
 * @brief Arquivo header da classe da Visao.
 * @author Leonardo da Silva Costa
 * @version 1.0
 * @date 2020-08-25
 */

#ifndef VISAO_H
#define VISAO_H

#include <QThread>
#include <QtCore>
#include <QDebug>
#include <QPointer>
#include <QNetworkInterface>
#include <QUdpSocket>
#include <QTimer>
#include <QScopedPointer>

#include "Ambiente/sslteam.h"
#include "Robo/visionrobot.h"
#include "Ambiente/geometria.h"
#include "Bola/visionball.h"
#include "Visao/Kalman_Filter/kalmanfilter.h"
#include "Rede/logger.h"
#include "Radio/radiofeedback.h"

#include "Visao/visionpacket.h"

/**
 * @brief Classe que faz o recebimento dos dados da visão.
 */
class Visao : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Construtor da classe
     */
    Visao();

    /**
     * @brief Destrutor da classe
     */
    ~Visao();

    /**
     * @brief Retorna o FPS do Filtro de Kalman (KalmanFilter)
     *
     * @return float - FPS
     */
    float fGetFPSKalman();

private:

    /**
     * @brief Detecta os objetos que saíram da visão e atualiza a posição daqueles que
     * ainda estão sendo detectados
     *
     * @param _pkgUltimoPacoteRecebido - Pacote da visão contendo as informações de
     * detecção
     */
    void vAtualizaInformacoesCampo(const SSL_WrapperPacket& _pkgUltimoPacoteRecebido);

    /**
     * @brief Detecta quais objetos saíram da visão
     *
     * @param frameDeteccao - Frame com as informações de detecção
     */
    void vDetectaObjetosForaCampo(const SSL_DetectionFrame& frameDeteccao);

    /**
     * @brief Atualiza a posição dos objetos que estão sendo detectados pela visão
     *
     * @param frameDeteccao
     */
    void vAtualizaObjetosDetectados(const SSL_DetectionFrame &frameDeteccao);

    /**
     * @brief Atualiza a posição da bola
     *
     * @param frameDeteccao - Frame com as informações de detecção
     * @param nBola - Número da bola. Podem haver mais de uma bola em um único pacote
     * de detecção
     */
    void vAtualizaBola(const SSL_DetectionFrame& frameDeteccao, const int nBola);

    /**
     * @brief Atualiza a posição de um robô aliado
     *
     * @param frameDeteccao - Frame com as informações de detecção
     * @param nRobo - ID do robô
     */
    void vAtualizaRoboAliado(const SSL_DetectionFrame& frameDeteccao, const int nRobo);

    /**
     * @brief Atualiza a posição de um robô adverśario
     *
     * @param frameDeteccao - Frame com as informações de detecção
     * @param nRobo - ID do robô
     */
    void vAtualizaRoboAdversario(const SSL_DetectionFrame& frameDeteccao, const int nRobo);

    /**
     * @brief Atualiza a posição dos robôs de um time
     *
     * @param _team
     * @param detection
     */
    void vUpdateTeam(SSLTeam<VisionRobot>* _team,
                     const SSL_DetectionFrame &_detection);

    /**
     * @brief Atualiza os dados da geometria do campo
     *
     * @param frameGeometria - Frame com as informações de geometria
     */
    void vAtualizaGeometriaCampo(const SSL_GeometryData& frameGeometria);

    /**
     * @brief Calcula o incremento de ângulo a ser atribuído ao ângulo de orientação
     * atual do robô
     *
     * @details Na visão, o ângulo de orientação do robô varia de \f$ -\pi \f$ a
     * \f$ +\pi \f$ [rad]. Porém, para que o controlador de rotação funcione
     * corretamente, o ângulo deve variar continuamente, ou seja, não pode haver
     * o salto entre \f$ \pm \pi \f$. Para isso, o ângulo é ajustado para variar de
     * \f$ -\infty \f$ a \f$ +\infty \f$ [rad]. Esta função é a que calcula o quanto o
     *  ângulo variou desde a última atualização.
     *
     * @param orientacaoVisao - Ângulo de orientação do robô na visão
     * (\f$ -\pi; \; +\pi \f$)
     * @param anguloAtual - Ângulo de orientação atual do robô
     * (\f$ -\infty; \; +\infty \f$)
     *
     * @return float - Incremento que deve ser adiciona ao ângulo atual do robô
     */
    float fCalculaAnguloRobo(float orientacaoVisao, float anguloAtual);

    /**
     * @brief Executa o Filtro de Kalman para o robô fornecido
     * @see KalmanFilter
     *
     * @param rbtRobo - Objeto do robô
     */
    void vRunRobotKalman(VisionRobot *_robot);

    /**
     * @brief Executa o Filtro de Kalman para a bola
     * @see KalmanFilter
     */
    void vRunBallKalman();

    /**
     * @brief Calcula a variância em X e Y do vetor fornecido
     *
     * @param _vtBufferPosicoes - Vetor de posição
     *
     * @return QVector2D - Variância em X e Y
     */
    QVector2D vt2dCalculaVariancia(QVector<QVector2D> _vtBufferPosicoes);

    void vOverrideBallDetection(uint _cameraID, uint _cameraFrame);

    QSharedPointer<Geometria> fieldGeometry;
    QSharedPointer<SSLTeam<VisionRobot>> blueTeam;
    QSharedPointer<SSLTeam<VisionRobot>> yellowTeam;
    QSharedPointer<VisionBall> ball;
    QSharedPointer<VisionPacket> currentPacket;
    qint8 iLoopCount;

    QScopedPointer<QUdpSocket> udpSSLVision; /**< Socket UDP para conectar-se à visão.*/
    QScopedPointer<QTimer> tmrKalman; /**< Timer para execução do loop do Filtro de
                                        Kalman.*/
    int iTesteMeioCampo; /**< Indica se é para utilizar todo o campo ou somente metade.
                            @details Isso é utilizando nas competições, onde nós
                            geralmente dividimos o campo com alguma outra equipe
                            para realizar os testes. Dessa forma, é possível
                            ignorar a metade do campo utilizada pela outra equipe.*/
    TeamColor timeAtual; /**< Cor do nosso time.*/
    QElapsedTimer etmTimerFPSKalman; /**< Timer utilizado para medir o FPS do loop
                                            do Filtro de Kalman.*/
    float fFPSKalman; /**< Valor médio atual do FPS do Filtro de Kalman.*/
    QVector<int> iFPSCounter; /**< Vetor com o histórico de FPS para calcular o valor
                                    médio.*/

public slots:
    /**
     * @brief Conecta-se à visão
     *
     * @param ipVisao - IP da visão
     * @param portaVisao - Porta da visão
     * @param indiceInterface - Interface da visão
     */
    void vConectarInterfaceRede(const QString &strInterface);

    /**
     * @brief Desconecta-se da visão
     */
    void vDesconectar();

    /**
     * @brief Muda o valor do #iTesteMeioCampo
     *
     * @param _testeCampo
     */
    void vSetaTesteMeioCampo(int _testeCampo);

    /**
     * @brief Muda a cor do nosso time (#timeAtual)
     *
     * @param _timeAtual
     */
    void vTrocaTimeAtual(int _timeAtual);

    void vReceiveRadioData_(const RadioFeedback &_feedback);

private slots:
    /**
     * @brief Recebe os pacotes de dados da visão
     */
    void vRecebePacotesVisao();

    /**
     * @brief Executa o Filtro de Kalman utilizando o timer #tmrKalman
     * @see KalmanFilter
     */
    void vFiltroKalman();

signals:
    /**
     * @brief Envia os dados da visão filtrados para as outras threads
     */
    void _vVisionData(const VisionPacket _packet);

    /**
     * @brief Envia um sinal indicando que chegou um pacote de geometria
     */
    void vChegouGeometria();
};

#endif // VISAO_H
