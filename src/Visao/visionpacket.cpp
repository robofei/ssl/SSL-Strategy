#include "visionpacket.h"
#include "Ambiente/sslteam.h"

VisionPacket::VisionPacket()
{
    fieldGeometry = nullptr;
    blueTeam = nullptr;
    yellowTeam = nullptr;
    ball = nullptr;
    bGeometryChanged = false;
}

VisionPacket::VisionPacket(const VisionPacket& _other)
{
    fieldGeometry = new Geometria(*_other.fieldGeometry);
    blueTeam = new SSLTeam<Robo>(*_other.blueTeam);
    yellowTeam = new SSLTeam<Robo>(*_other.yellowTeam);
    ball = new Bola(*_other.ball);
    bGeometryChanged = _other.bGeometryChanged;
}

VisionPacket::VisionPacket(Geometria* _geometry, SSLTeam<VisionRobot>* _blue,
                           SSLTeam<VisionRobot>* _yellow, Bola* _ball,
                           bool _geometryChanged)
{
    fieldGeometry = new Geometria(*_geometry);
    blueTeam = new SSLTeam<Robo>(_blue->toRobot());
    yellowTeam = new SSLTeam<Robo>(_yellow->toRobot());
    ball = new Bola(*_ball);
    bGeometryChanged = _geometryChanged;
}

VisionPacket::~VisionPacket()
{
    if (blueTeam != nullptr)
        delete blueTeam;
    if (yellowTeam != nullptr)
        delete yellowTeam;
    if (fieldGeometry != nullptr)
        delete fieldGeometry;
    if (ball != nullptr)
        delete ball;
}

void VisionPacket::vUpdateInfo(const Geometria* _geometry,
                               const SSLTeam<VisionRobot>* _blue,
                               const SSLTeam<VisionRobot>* _yellow,
                               const Bola* _ball)
{
    if (blueTeam != nullptr)
        delete blueTeam;
    if (yellowTeam != nullptr)
        delete yellowTeam;
    if (ball != nullptr)
        delete ball;
    if (fieldGeometry != nullptr)
        delete fieldGeometry;

    blueTeam = new SSLTeam<Robo>(_blue->toRobot());
    yellowTeam = new SSLTeam<Robo>(_yellow->toRobot());
    ball = new Bola(*_ball);
    fieldGeometry = new Geometria(*_geometry);
}
