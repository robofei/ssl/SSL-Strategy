#ifndef VISIONPACKET_H
#define VISIONPACKET_H

#include <QObject>

#include "Ambiente/geometria.h"
#include "Ambiente/sslteam.h"
#include "Bola/bola.h"
#include "Robo/robot.h"

class VisionPacket
{
public:
    Geometria* fieldGeometry;
    SSLTeam<Robo>* blueTeam;
    SSLTeam<Robo>* yellowTeam;
    Bola* ball;
    bool bGeometryChanged;

    VisionPacket();
    VisionPacket(const VisionPacket& _other);
    VisionPacket(Geometria* _geometry, SSLTeam<VisionRobot>* _blue,
                 SSLTeam<VisionRobot>* _yellow, Bola* _ball,
                 bool _geometryChanged);
    ~VisionPacket();

    void vUpdateInfo(const Geometria* _geometry,
                     const SSLTeam<VisionRobot>* _blue,
                     const SSLTeam<VisionRobot>* _yellow, const Bola* _ball);
};

Q_DECLARE_METATYPE(VisionPacket)

#endif // VISIONPACKET_H
