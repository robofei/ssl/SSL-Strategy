/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "robofei_cli.h"
#include <iostream>
#include <string>
#include <QMutex>


FiltroDeMensagensCLI::FlagsDepuracao operator~ (FiltroDeMensagensCLI::FlagsDepuracao a)
{ return (FiltroDeMensagensCLI::FlagsDepuracao)~(quint8)a; }
FiltroDeMensagensCLI::FlagsDepuracao operator| (FiltroDeMensagensCLI::FlagsDepuracao a, FiltroDeMensagensCLI::FlagsDepuracao b)
{ return (FiltroDeMensagensCLI::FlagsDepuracao)((quint8)a | (quint8)b); }
FiltroDeMensagensCLI::FlagsDepuracao operator& (FiltroDeMensagensCLI::FlagsDepuracao a, FiltroDeMensagensCLI::FlagsDepuracao b)
{ return (FiltroDeMensagensCLI::FlagsDepuracao)((quint8)a & (quint8)b); }
FiltroDeMensagensCLI::FlagsDepuracao operator^ (FiltroDeMensagensCLI::FlagsDepuracao a, FiltroDeMensagensCLI::FlagsDepuracao b)
{ return (FiltroDeMensagensCLI::FlagsDepuracao)((quint8)a ^ (quint8)b); }
FiltroDeMensagensCLI::FlagsDepuracao& operator|= (FiltroDeMensagensCLI::FlagsDepuracao& a, FiltroDeMensagensCLI::FlagsDepuracao b)
{ return (FiltroDeMensagensCLI::FlagsDepuracao&)((quint8&)a |= (quint8)b); }
FiltroDeMensagensCLI::FlagsDepuracao& operator&= (FiltroDeMensagensCLI::FlagsDepuracao& a, FiltroDeMensagensCLI::FlagsDepuracao b)
{ return (FiltroDeMensagensCLI::FlagsDepuracao&)((quint8&)a &= (quint8)b); }
FiltroDeMensagensCLI::FlagsDepuracao& operator^= (FiltroDeMensagensCLI::FlagsDepuracao& a, FiltroDeMensagensCLI::FlagsDepuracao b)
{ return (FiltroDeMensagensCLI::FlagsDepuracao&)((quint8&)a ^= (quint8)b); }


FiltroDeMensagensCLI::FlagsDepuracao FiltroDeMensagensCLI::flags =
        FiltroDeMensagensCLI::FlagsDepuracao::Warning |
        FiltroDeMensagensCLI::FlagsDepuracao::Critical |
        FiltroDeMensagensCLI::FlagsDepuracao::Info |
        FiltroDeMensagensCLI::FlagsDepuracao::Debug;


RoboFEI_CLI::RoboFEI_CLI(QObject *parent) : QThread(parent)
{

}

RoboFEI_CLI::~RoboFEI_CLI()
{

}

#include <QDebug>

void RoboFEI_CLI::run()
{
    qInfo() << "\n\nAVISO!\n"
               "Esse programa ainda não é 100% estável, em caso de emergência\n"
               " finalize o processo à força. Abra o programa `htop` em um novo\n"
               " terminal, procure (pressionando F3) pelo processo robofeissl\n"
               " pressione a tecla enter ao identificar o processo correto e \n"
               " pressiona a tecla K. Após isso confirme o procedimento.\n\n"

               "Não se atreva a acionar a sequência (Ctrl + D), isso \n"
               "certamente causará mal funcionamento do programa.\n\n";

    while(true)
    {
        std::cout << "\n\033[95m<<< \033[0m";

        std::string in;
        std::getline(std::cin, in);

        if(in.empty())
            continue;

        QStringList args = QString::fromStdString(in).split(" ");

        if(args[0] == "exit" || args[0] == "quit")
        {
            if(args.size() == 1)
            {
                // Emite sinal para finalizar RoboFeiSSL
                emit SIGNAL_finaliza();
            }
            else
            {
                vComandoInvalido();
            }
        }
        else if(args[0] == "help")
        {
            if(args.size() == 1)
            {
                vHelp();
            }
            else if(args.size() == 2 && args[1] == "all")
            {
                vHelpAll();
            }
            else
            {
                vComandoInvalido();
            }
        }
        else if(args[0] == "set")
        {
            if(args.size() <= 2)
            {
                vComandoInvalido();
            }
            else if(args[1] == "debug")
            {
                if(args.size() == 3)
                {
                    vSelecionaModoDebug(args[2]);
                }
                else
                {
                    vComandoInvalido();
                }
            }
            else if(args[1] == "network")
            {
                if(args.size() != 3)
                {
                    vComandoInvalido();
                }
                else if(args[2] == "on")
                {
                    emit SIGNAL_mudarEstadoRede(true);
                }
                else if(args[2] == "off")
                {
                    emit SIGNAL_mudarEstadoRede(false);
                }
                else
                {
                    vComandoInvalido();
                }
            }
            else if(args[1] == "strategy")
            {
                if(args.size() != 3)
                {
                    vComandoInvalido();
                }
                else if(args[2] == "on")
                {
                    emit SIGNAL_mudarEstadoEstrategia(true);
                }
                else if(args[2] == "off")
                {
                    emit SIGNAL_mudarEstadoEstrategia(false);
                }
                else
                {
                    vComandoInvalido();
                }
            }
        }
        else if(args[0] == "reload-config")
        {
            if(args.size() == 1)
            {
                emit SIGNAL_recarregarConfiguracoes();
            }
            else if(args.size() == 2)
            {
                emit SIGNAL_recarregarConfiguracoes(args[1]);
            }
            else
            {
                vComandoInvalido();
            }
        }
        else
        {
            vComandoInvalido();
        }
    }
}

void RoboFEI_CLI::vComandoInvalido()
{
//    printf("\033[31m%s\033[0m", "Estupido");
    printf("\033[31m%s\033[0m",
           "\nCar* usuári*,\n"
           "O comando que você tentou utilizar infelizmente não existe.\n"
           "Para ajuda, digite: help\n");
}

void RoboFEI_CLI::vHelp()
{

    std::cout << "Comandos:\n\n"
                 "{exit}; {quit}:\t\tFinaliza o programa.\n\n"

                 "{help}\t\tMostra essa mensagem.\n\n"

                 "{help all}\t\tMostra uma mensagem de ajuda mais detalhada.\n\n"

                 "{set <VARIABLE> <ATRRIBUTE>}\t\tModifica variável dada como\n"
                 " argumento.\n\n"

                 "{reload-config <FILE_NAME>}\t\t Modifica as variáveis da Interface\n"
                 " com os dados do arquivo passado. Se esse comadndo for\n"
                 " chamado sem argumento, ele carrega o arquivo padrão (CurrentSession.cfg).\n\n";
}

void RoboFEI_CLI::vHelpAll()
{
    std::cout << "Comandos:\n\n"
                 "{exit}; {quit}:\t\tFinaliza o programa.\n\n"

                 "{help}\t\tMostra essa mensagem.\n\n"

                 "{help all}\t\tMostra uma mensagem de ajuda mais detalhada.\n\n"

                 "{set <VARIABLE> <ATRRIBUTE>}\t\tModifica variável dada"
                 "Variáveis aceitas com seus respectivos argumentos:\n"
                 "\t<{debug}> <{critical+warning+info+debug}>\n"
                 "Por exemplo: `set debug 1110` deve habilitar todas as mensagens\n"
                 " excetuando a do tipo debug.\n"
                 "\t<{network},{strategy} <{on},{off}>\n\n"

                 "{reload-config <FILE_NAME>}\t\t Modifica as variáveis da Interface\n"
                 " com os dados do arquivo passado. Se esse comadndo for\n"
                 " chamado sem argumento, ele carrega o arquivo padrão (CurrentSession.cfg).\n\n";
}

void RoboFEI_CLI::vSelecionaModoDebug(const QString &modo)
{
    if(modo.size() == 4 &&
       (modo.count('1') + modo.count('0') == 4))
    {
        quint8 var = modo.toUInt(nullptr, 2);

        FiltroDeMensagensCLI::flags = (FiltroDeMensagensCLI::FlagsDepuracao)var;
    }
}



void FiltroDeMensagensCLI::filtroMensagens(const QtMsgType type,
                                           const QMessageLogContext &context,
                                           const QString &msg)
{
    switch (type)
    {
        case QtDebugMsg:
            if(FiltroDeMensagensCLI::flags & FiltroDeMensagensCLI::FlagsDepuracao::Debug)
            {
                std::cout << ("\n\033[36m" + msg.toStdString() + "\033[0m\n");
            }
            break;
        case QtInfoMsg:
            if(FiltroDeMensagensCLI::flags & FiltroDeMensagensCLI::FlagsDepuracao::Info)
            {
                std::cout << ("\n\033[32m" + msg.toStdString() + "\033[0m\n");
            }
            break;
        case QtWarningMsg:
            if(FiltroDeMensagensCLI::flags & FiltroDeMensagensCLI::FlagsDepuracao::Warning)
            {
                std::cout << ("\n\033[33m" + msg.toStdString() + "\033[0m\n");
            }
            break;
        case QtCriticalMsg:
            if(FiltroDeMensagensCLI::flags & FiltroDeMensagensCLI::FlagsDepuracao::Critical)
            {
                std::cout << ("\n\033[31m" + msg.toStdString() + "\033[0m\n");
            }
            break;
        case QtFatalMsg:
            std::cout << ("\n\033[31mFatal: " + msg.toStdString() + "\033[0m\n");
            break;
    }
}

