/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ROBOFEI_CLI_H
#define ROBOFEI_CLI_H

#include <QObject>
#include <QThread>
#include <QApplication>

class RoboFEI_CLI : public QThread
{
    Q_OBJECT
public:
    explicit RoboFEI_CLI(QObject *parent = nullptr);
    ~RoboFEI_CLI();

protected:

private:
    void run();
    void vComandoInvalido();
    void vHelp();
    void vHelpAll();
    void vSelecionaModoDebug(const QString& modo);

signals:
    void SIGNAL_mudarEstadoRede(const bool ativar);
    void SIGNAL_mudarEstadoEstrategia(const bool ativar);
    void SIGNAL_recarregarConfiguracoes(const QString& nomeArquivo = "CurrentSession.cfg");
    void SIGNAL_finaliza();

};

struct FiltroDeMensagensCLI{
    static void filtroMensagens(const QtMsgType type,
                                const QMessageLogContext& context,
                                const QString& msg);

    enum FlagsDepuracao : quint8
    {
        None = 0x00,
        Debug = 0x01,
        Info = 0x02,
        Warning = 0x04,
        Critical = 0x08
    };

    static FlagsDepuracao flags;
};

#endif // ROBOFEI_CLI_H
