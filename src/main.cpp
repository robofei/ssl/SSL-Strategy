/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

///
/// \file main.cpp
/// \brief MAIN
///

#include <QApplication>
#include <QMutex>
#include <QScreen>
#include <QCommandLineParser>

#include "robofeissl.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "cli/robofei_cli.h"
#include "QtMessageFilter/qtmessagefilter.h"

RoboFeiSSL *r = nullptr;

enum ModosDeOperacao : quint8
{
    NONE    = 0x00,
    CLI     = 0x01
};

inline ModosDeOperacao operator|(const ModosDeOperacao& a, const ModosDeOperacao& b)
{
    return (ModosDeOperacao)((quint8)(a) | (quint8)(b));
};
inline ModosDeOperacao& operator|=(const ModosDeOperacao& a, const ModosDeOperacao& b)
{
    return (ModosDeOperacao&)((quint8&)a |= (quint8)b);;
};

inline ModosDeOperacao operator& (ModosDeOperacao a, ModosDeOperacao b)
{
    return (ModosDeOperacao)((quint8)a & (quint8)b);
}

ModosDeOperacao opcoesDeInicializacao(const QStringList& args)
{
    QCommandLineParser parser;

    parser.setApplicationDescription("Software de estrategia da equipe RoboFEI - Small Size League (SSL).");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption optionCli({"cli", "no-gui"},
                                 QApplication::translate("main", "Rodar apenas pela linha de comando"));

    parser.addOption(optionCli);

    parser.process(args);

    ModosDeOperacao ops = ModosDeOperacao::NONE;

    if(parser.isSet(optionCli))
        ops |= ModosDeOperacao::CLI;

    return ops;
}

int main(int argc, char** argv)
{
    QApplication a(argc, argv);
    a.setApplicationName("SSL-Strategy");
    a.setOrganizationName("RoboFEI");
    a.setApplicationVersion("1.0");

    ModosDeOperacao opcoes = opcoesDeInicializacao(a.arguments());

    bool inicializaGUI = !(opcoes & ModosDeOperacao::CLI);
//    bool inicializaGUI = false;

    if(inicializaGUI)
    {
        // Inicializa Interface
        a.setStyle(QStyleFactory::create("Fusion"));
        // Intercepta as mensagens do Qt (qDebug, qWarning, etc) para mostrar
        // na GUI
#ifndef DONT_USE_MESSAGE_GUI
//        QtMessageFilter::resetInstance(nullptr, true);
#endif
    }
    else
    {
        // Inicia CLI
        qInstallMessageHandler(FiltroDeMensagensCLI::filtroMensagens);
    }

    // globalConfig.create("global_config.json");
    globalConfig.read("global_config.json");

    r = new RoboFeiSSL();

    if(inicializaGUI)
        r->show();
    else
    {
        RoboFEI_CLI* cli = new RoboFEI_CLI();

        QObject::connect(cli, &RoboFEI_CLI::SIGNAL_mudarEstadoRede,
                         r, &RoboFeiSSL::SLOT_mudarEstadoRede);
        QObject::connect(cli, &RoboFEI_CLI::SIGNAL_mudarEstadoEstrategia,
                         r, &RoboFeiSSL::SLOT_mudarEstadoEstrategia);
        QObject::connect(cli, &RoboFEI_CLI::SIGNAL_recarregarConfiguracoes,
                         r, &RoboFeiSSL::SLOT_recarregarConfiguracoes);
        QObject::connect(cli, &RoboFEI_CLI::SIGNAL_finaliza,
                         r, &RoboFeiSSL::deleteLater);

        cli->start();
    }

    return a.exec();
}
