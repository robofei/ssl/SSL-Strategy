#
# SSL-Strategy
# Copyright (C) 2020  RoboFEI / Small Size League
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import subprocess
import os
import joblib
import numpy
from random import randint
from sklearn.model_selection import train_test_split
from console_progressbar import ProgressBar
import time
import warnings

output_path: str = "robofeissl/decisoes_jogadas/models/"

# Comente a liha abaixo para permitir WARNINGS
warnings.simplefilter("ignore")


def import_database():

    if os.path.isdir('DatabaseForKicksAndPassesRobocup'):
        print("Updating the database")
        os.chdir('DatabaseForKicksAndPassesRobocup')
        subprocess.check_call(['git'] + ['pull'])
        os.chdir('..')
    else:
        # Clona o repositório com a database utilizada para criar os modelos
        subprocess.check_call(['git'] + ['clone', 'https://github.com/Bollos00/DatabaseForKicksAndPassesRobocup.git'])


def print_time_of_each_prediction(start: float, end: float, x_size: int, y_size: int):
    print("Time of each prediction: {:.3f} us".format(
        (end-start)*1e6/(x_size*y_size))
          )


def print_score(score_test: float, score_train: float):
    print("\nScore test: {:.3f}/100".format(100*score_test))
    print("Score train: {:.3f}/100".format(100*score_train))


def get_array_from_pattern(pattern):

    from glob import glob

    array = []

    for f in glob("./DatabaseForKicksAndPassesRobocup/{}".format(pattern)):
        array.append(
            numpy.genfromtxt(
                f,
                dtype=numpy.uint8,
                delimiter=";",
                skip_header=1
            )
        )

    return numpy.concatenate(array)


def get_x_y_passes(array_passes: numpy.ndarray, version=0):

    if version >= 1:
        return (array_passes[:, [0, 1, 2, 3, 4, 5, 6, 7]], array_passes[:, 8])
    return (array_passes[:, [1, 2, 3, 4, 5, 6, 7, 8]], array_passes[:, 0])


def get_x_y_shoots(array_shoot: numpy.ndarray, version=0):
    if version >= 1:
        return (array_shoot[:, [0, 1, 2]], array_shoot[:, 3])
    return (array_shoot[:, [1, 2, 3]], array_shoot[:, 0])


def find_prediction_time(model, x_size, predictions=10000):
    X_in = numpy.random.randint(
        low=0, high=250, size=(predictions, x_size), dtype=numpy.uint8
    )

    start: float = time.time()

    model.predict(X_in)

    end: float = time.time()

    print("Time of each prediction: {:.3f} us".format((end-start)*1e6/predictions))


def test_model(model, title, X, y):
    print('\n******************************')
    print("{}".format(title))


    predictions_time = 10000
    predictions_score = 100

    pb = ProgressBar(
        total=predictions_score - 1,
        prefix='Testing model', suffix='', decimals=3,
        length=50, fill='#', zfill='.'
    )

    find_prediction_time(model, X.shape[1], predictions=predictions_time)

    x_axis: numpy.ndarray = numpy.fromiter(range(0, predictions_score, 1), dtype=numpy.uint16)
    score_train: numpy.ndarray = numpy.full(x_axis.shape, 0, dtype=numpy.float64)
    score_test: numpy.ndarray = numpy.full(x_axis.shape, 0, dtype=numpy.float64)

    start: float = time.time()

    for j, i in enumerate(x_axis):

        (X_train, X_test, y_train, y_test) = train_test_split(
            X, y, test_size=.2, random_state=randint(0, 1000))

        model.fit(X_train, y_train)

        score_test[j] = model.score(X_test, y_test)
        score_train[j] = model.score(X_train, y_train)

        pb.print_progress_bar(i)

    end: float = time.time()

    print_score(numpy.mean(score_test), numpy.mean(score_train))
    print('******************************\n')



def print_feature_importance_of_pass_model(name, coef):
    print("""
{8} model for pass move feature importances:
Angle of free path of pass:\t{0:.3%}
Distance of the pass:\t{1:.3%}
Opponent marker of receiver parameter:\t{2:.3%}
Redirect shoot angle:\t{3:.3%}
Angle of free path for the receiver to shoot on the opponent goal:\t{4:.3%}
Distance from the receiver towards the goal:\t{5:.3%}
Opponent marker of the passer:\t{6:.3%}
Delta X parameter:\t{7:.3%}
    """.format(coef[0], coef[1], coef[2], coef[3], coef[4], coef[5], coef[6], coef[7], name))

def print_feature_importance_of_shoot_model(name, coef):
    print("""
{3} model for shoot move coeficients:
Angle of free path of shoot:\t{0:.3%}
Distance of the shoot:\t{1:.3%}
Opponent marker of shooter parameter:\t{2:.3%}
    """.format(coef[0], coef[1], coef[2], name))

def create_knn_models(x_passe: numpy.ndarray,
                      y_passe: numpy.ndarray,
                      x_chute: numpy.ndarray,
                      y_chute: numpy.ndarray,
                      bool_test_model: bool = False):

    from sklearn.neighbors import KNeighborsRegressor

    print('\nCreating model of KNearestNeighbor for pass move...')

    # Passe
    model_out: KNeighborsRegressor = KNeighborsRegressor(
        n_neighbors=20,
        n_jobs=1
    ).fit(x_passe, y_passe)

    joblib.dump(model_out, output_path + "avaliacao_passe_knn.sav")

    if bool_test_model:
        test_model(model_out, 'Passes - KNearestNeighbor', x_passe, y_passe)

    print('\nCreating model of KNearestNeighbor for shoot move...')

    # Chute
    model_out: KNeighborsRegressor = KNeighborsRegressor(
        n_neighbors=50,
        n_jobs=1
    ).fit(x_chute, y_chute)

    joblib.dump(model_out, output_path + "avaliacao_chute_knn.sav")

    if bool_test_model:
        test_model(model_out, 'Shoots - KNearestNeighbor', x_chute, y_chute)


def create_linear_regression_models(x_passe: numpy.ndarray,
                                    y_passe: numpy.ndarray,
                                    x_chute: numpy.ndarray,
                                    y_chute: numpy.ndarray,
                                    bool_test_model: bool = False):

    from sklearn.linear_model import ElasticNet

    # TODO: mudar para regressão linear simples

    print('\nCreating model of linear regression for pass move...')

    # Passe
    model_out: ElasticNet = ElasticNet(
        alpha=.01,
        l1_ratio=.5,
        fit_intercept=True,
        normalize=False,
        precompute=False,
        max_iter=1000,
        copy_X=True,
        tol=1e-3,
        warm_start=False,
        positive=False,
        random_state=randint(0, 1000),
        selection='cyclic'
    ).fit(x_passe, y_passe)

    coef = model_out.coef_

    print("""
Linear regression model for pass move coeficients:
Angle of free path of pass:\t{0:.3f}
Distance of the pass:\t{1:.3f}
Opponent marker of receiver parameter:\t{2:.3f}
Redirect shoot angle:\t{3:.3f}
Angle of free path for the receiver to shoot on the opponent goal:\t{4:.3f}
Distance from the receiver towards the goal:\t{5:.3f}
Opponent marker of the passer:\t{6:.3f}
Delta X parameter:\t{7:.3f}
    """.format(coef[0], coef[1], coef[2], coef[3], coef[4], coef[5], coef[6], coef[7]))

    joblib.dump(model_out, output_path + "avaliacao_passe_lr.sav")

    if bool_test_model:
        test_model(model_out, 'Passes - Linear Regression', x_passe, y_passe)

    print('\nCreating model of linear regression for pass move...')

    # Chute
    model_out: ElasticNet = ElasticNet(
        alpha=.01,
        l1_ratio=.5,
        fit_intercept=True,
        normalize=False,
        precompute=False,
        max_iter=1000,
        copy_X=True,
        tol=1e-3,
        warm_start=False,
        positive=False,
        random_state=randint(0, 1000),
        selection='cyclic'
    ).fit(x_chute, y_chute)

    coef = model_out.coef_

    print("""
Linear regression model for shoot move coeficients:
Angle of free path of shoot:\t{0:.3f}
Distance of the shoot:\t{1:.3f}
Opponent marker of shooter parameter:\t{2:.3f}
    """.format(coef[0], coef[1], coef[2]))


    joblib.dump(model_out, output_path + "avaliacao_chute_lr.sav")

    if bool_test_model:
        test_model(model_out, 'Shoots - Linear Regression', x_chute, y_chute)


def create_ada_boost_models(x_passe: numpy.ndarray,
                            y_passe: numpy.ndarray,
                            x_chute: numpy.ndarray,
                            y_chute: numpy.ndarray,
                            bool_test_model: bool = False):

    from sklearn.ensemble import AdaBoostRegressor
    from sklearn.tree import DecisionTreeRegressor

    print('\nCreating model of adaBoost for pass move...')

    # Passe
    tree_aux_out: DecisionTreeRegressor = DecisionTreeRegressor(
        criterion='squared_error',
        splitter='best',
        max_depth=3,
        min_samples_split=1*1e-3,
        min_samples_leaf=40*1e-3,
        min_weight_fraction_leaf=0,
        max_features='auto',
        random_state=randint(0, 1000),
        max_leaf_nodes=None,
        min_impurity_decrease=0,
        ccp_alpha=0
    )
    model_out: AdaBoostRegressor = AdaBoostRegressor(
        base_estimator=tree_aux_out,
        n_estimators=40,
        learning_rate=50*1e-3,
        loss='square',
        random_state=randint(0, 1000)
    ).fit(x_passe, y_passe)

    print_feature_importance_of_pass_model("AdaBoost", model_out.feature_importances_)

    joblib.dump(model_out, output_path + "avaliacao_passe_ada_boost.sav")

    if bool_test_model:
        test_model(model_out, 'Passes - Ada Boost', x_passe, y_passe)

    print('\nCreating model of adaBoost for shoot move...')

    # Chute
    tree_aux_out = DecisionTreeRegressor(
        criterion='squared_error',
        splitter='best',
        max_depth=2,
        min_samples_split=1*1e-3,
        min_samples_leaf=100*1e-3,
        min_weight_fraction_leaf=0,
        max_features='auto',
        random_state=randint(0, 1000),
        max_leaf_nodes=None,
        min_impurity_decrease=0,
        ccp_alpha=0
    )

    model_out: AdaBoostRegressor = AdaBoostRegressor(
        base_estimator=tree_aux_out,
        n_estimators=30,
        learning_rate=100*1e-3,
        loss='square',
        random_state=randint(0, 1000)
    ).fit(x_chute, y_chute)

    print_feature_importance_of_shoot_model("AdaBoost", model_out.feature_importances_)

    joblib.dump(model_out, output_path + "avaliacao_chute_ada_boost.sav")

    if bool_test_model:
        test_model(model_out, 'Shoots - Ada Boost', x_chute, y_chute)


def create_random_forest_models(x_passe: numpy.ndarray,
                                y_passe: numpy.ndarray,
                                x_chute: numpy.ndarray,
                                y_chute: numpy.ndarray,
                                bool_test_model: bool = False):

    from sklearn.ensemble import RandomForestRegressor

    print('\nCreating model of random forest for pass move...')

    # Passe
    model_out: RandomForestRegressor = RandomForestRegressor(
        n_estimators=60,
        criterion='squared_error',
        max_depth=5,
        min_samples_split=1*1e-3,
        min_samples_leaf=50*1e-3,
        min_weight_fraction_leaf=0,
        max_features='auto',
        max_leaf_nodes=None,
        min_impurity_decrease=0,
        bootstrap=True,
        oob_score=False,
        n_jobs=None,
        random_state=randint(0, 1000),
        verbose=0,
        warm_start=False,
        ccp_alpha=0,
        max_samples=.5
    ).fit(x_passe, y_passe)

    print_feature_importance_of_pass_model("random forest", model_out.feature_importances_)

    joblib.dump(model_out, output_path + "avaliacao_passe_forest.sav")

    if bool_test_model:
        test_model(model_out, 'Passes - Random Forest Regressor', x_passe, y_passe)

    print('\nCreating model of random forest for shoot move...')

    # Chute
    model_out: RandomForestRegressor = RandomForestRegressor(
        n_estimators=30,
        criterion='squared_error',
        max_depth=3,
        min_samples_split=50*1e-3,
        min_samples_leaf=100*1e-3,
        min_weight_fraction_leaf=0,
        max_features='auto',
        max_leaf_nodes=None,
        min_impurity_decrease=0,
        bootstrap=True,
        oob_score=False,
        n_jobs=None,
        random_state=randint(0, 1000),
        verbose=0,
        warm_start=False,
        ccp_alpha=0,
        max_samples=0.5
    ).fit(x_chute, y_chute)

    print_feature_importance_of_shoot_model("random forest", model_out.feature_importances_)

    joblib.dump(model_out, output_path + "avaliacao_chute_forest.sav")

    if bool_test_model:
        test_model(model_out, 'Shoots - Random Forest Regressor', x_chute, y_chute)


def create_gradient_boosting_models(x_passe: numpy.ndarray,
                                    y_passe: numpy.ndarray,
                                    x_chute: numpy.ndarray,
                                    y_chute: numpy.ndarray,
                                    bool_test_model: bool = False):

    from sklearn.ensemble import GradientBoostingRegressor

    print('\nCreating model of gradient boosting for pass move...')

    # Passe
    model_out: GradientBoostingRegressor = GradientBoostingRegressor(
        loss='squared_error',
        learning_rate=50*1e-3,
        n_estimators=40,
        subsample=1,
        criterion='squared_error',
        min_samples_split=1*1e-3,
        min_samples_leaf=50*1e-3,
        min_weight_fraction_leaf=0,
        max_depth=4,
        min_impurity_decrease=0,
        init=None,
        random_state=randint(0, 1000),
        max_features='auto',
        verbose=0,
        max_leaf_nodes=None,
        warm_start=False,
        validation_fraction=.1,
        n_iter_no_change=None,
        tol=1e-4,
        ccp_alpha=0.0
    ).fit(x_passe, y_passe)

    print_feature_importance_of_pass_model("gradient boosting", model_out.feature_importances_)


    joblib.dump(model_out, output_path + "avaliacao_passe_gradient.sav")

    if bool_test_model:
        test_model(model_out, 'Passes - Gradient Boosting', x_passe, y_passe)

    print('\nCreating model of gradient boosting for shoot move...')

    # Chute
    model_out: GradientBoostingRegressor = GradientBoostingRegressor(
        loss='squared_error',
        learning_rate=100*1e-3,
        n_estimators=30,
        subsample=1,
        criterion='squared_error',
        min_samples_split=200*1e-3,
        min_samples_leaf=300*1e-3,
        min_weight_fraction_leaf=0,
        max_depth=5,
        min_impurity_decrease=0,
        init=None,
        random_state=randint(0, 1000),
        max_features='auto',
        verbose=0,
        max_leaf_nodes=None,
        warm_start=False,
        validation_fraction=.1,
        n_iter_no_change=None,
        tol=1e-4,
        ccp_alpha=0.0
    ).fit(x_chute, y_chute)

    print_feature_importance_of_shoot_model("gradient boosting", model_out.feature_importances_)

    joblib.dump(model_out, output_path + "avaliacao_chute_gradient.sav")

    if bool_test_model:
        test_model(model_out, 'Shoots - Gradient Boosting', x_chute, y_chute)


def create_models(test_models: bool, database_name: str):

    array_chute: numpy.ndarray = numpy.ndarray(0)
    array_passe: numpy.ndarray = numpy.ndarray(0)

    if database_name == "ROBOCUP-2019":
        array_chute = numpy.concatenate([
            get_array_from_pattern("ROBOCUP-2019/ER_FORCE/ATA/*Chute.csv"),
            get_array_from_pattern("ROBOCUP-2019/ZJUNlict/ATA/*Chute.csv")
        ])

        array_passe = get_array_from_pattern("ROBOCUP-2019/ALL/*Passe.csv")

    elif database_name == "LARC-2020-VIRTUAL":
        array_chute = numpy.concatenate([
            get_array_from_pattern("LARC-2020-VIRTUAL/RoboCin/ATA/*Chute.csv"),
            get_array_from_pattern("LARC-2020-VIRTUAL/RoboFEI/ATA/*Chute.csv"),
            get_array_from_pattern("LARC-2020-VIRTUAL/Maracatronics/ATA/*Chute.csv")
        ])
        
        array_passe = numpy.concatenate([
            get_array_from_pattern("LARC-2020-VIRTUAL/RoboCin/ATA/*Passe.csv"),
            get_array_from_pattern("LARC-2020-VIRTUAL/RoboFEI/ATA/*Passe.csv"),
            get_array_from_pattern("LARC-2020-VIRTUAL/Maracatronics/ATA/*Passe.csv")
        ])

    elif database_name == "ROBOCUP-2021-VIRTUAL":
        array_chute = numpy.concatenate([
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/ER_FORCE/ATA/*Shoot.csv"),
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/KIKS/ATA/*Shoot.csv"),
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/RoboFEI/ATA/*Shoot.csv"),
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/TIGERs_Mannheim/ATA/*Shoot.csv")
        ])

        array_passe: numpy.ndarray = numpy.concatenate([
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/ER_FORCE/ATA/*Pass.csv"),
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/KIKS/ATA/*Pass.csv"),
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/RoboCin/ATA/*Pass.csv"),
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/RoboFEI/ATA/*Pass.csv"),
            get_array_from_pattern("ROBOCUP-2021-VIRTUAL/DIVISION-B/TIGERs_Mannheim/ATA/*Pass.csv")
        ])


    else:
        raise Exception("Error: \"{}\" is not a valid database name.".format(database_name))

    # a versão será maior que 1 para todas as versões depois da "ROBOCUP-2019"
    log_analyser_version = 0 if database_name == "ROBOCUP-2019" else 1.2

    x_chute, y_chute = get_x_y_shoots(array_chute, log_analyser_version)
    x_passe, y_passe = get_x_y_passes(array_passe, log_analyser_version)

    create_linear_regression_models(x_passe, y_passe, x_chute, y_chute, test_models)
    create_knn_models(x_passe, y_passe, x_chute, y_chute, test_models)
    create_ada_boost_models(x_passe, y_passe, x_chute, y_chute, test_models)
    create_random_forest_models(x_passe, y_passe, x_chute, y_chute, test_models)
    create_gradient_boosting_models(x_passe, y_passe, x_chute, y_chute, test_models)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="""
Create models for robofeissl/decisoes_jogadas package
""")

    parser.add_argument(
        '--test-models',
        help='Test models when they are created',
        action="store_true"
    )

    parser.add_argument(
        '--database-name',
        help='The database which the models will be created from',
        default='ROBOCUP-2021-VIRTUAL',
        type=str
    )

    args = parser.parse_args()

    import_database()
    create_models(args.test_models, args.database_name)
