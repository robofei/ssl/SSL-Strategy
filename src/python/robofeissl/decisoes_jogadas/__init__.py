"""
Módulo utilizado na decisão de jogadas.

Neste módulo encontra-se métodos de Supervised Learning que permitem avaliar --
 a partir de certos parâmetros e entrada -- uma jogada de passe ou chute.
"""
__version__ = "0.0"
