#
# SSL-Strategy
# Copyright (C) 2020  RoboFEI / Small Size League
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


"""
Decisão das jogadas chute ao gol.

Mais detalhes.
"""

import numpy
import joblib
import os


class AvaliacaoJogadaChute:
    """
    Classe AvaliacaoJogadaChute.

    More details etc etc.

    Attributes
    ----------
    model
        Modelo de Supervised Learning utilizado na previsão das jogadas

    """

    def __init__(self, arg: str):
        """
        Construtor padrão da classe.

        Carrega o modelo indicado pelo parâmetro 'arg' utilizado nas previsões.

        Parameters
        ----------
        self
            Referência à própria instância da classe
        arg : str{'knn', 'linear_regression', 'ada', 'gradient', 'random_forest'}
            Indica o nome do modelo a ser utilizado.

        """

        path_to_model: str = os.path.dirname(os.path.realpath(__file__))+"/models"

        if arg == "knn":

            from sklearn.neighbors import KNeighborsRegressor

            self.model: KNeighborsRegressor = joblib.load(
                path_to_model+"/avaliacao_chute_knn.sav"
            )

        elif arg == "linear_regression":

            from sklearn.linear_model import ElasticNet

            self.model: ElasticNet = joblib.load(
                path_to_model+"/avaliacao_chute_lr.sav"
            )

        elif arg == "ada":
            from sklearn.ensemble import AdaBoostRegressor

            self.model: AdaBoostRegressor = joblib.load(
                path_to_model+"/avaliacao_chute_ada_boost.sav"
            )

        elif arg == "gradient":
            from sklearn.ensemble import GradientBoostingRegressor

            self.model: GradientBoostingRegressor = joblib.load(
                path_to_model+"/avaliacao_chute_gradient.sav"
            )

        elif arg == "random_forest":
            from sklearn.ensemble import RandomForestRegressor

            self.model: RandomForestRegressor = joblib.load(
                path_to_model+"/avaliacao_chute_forest.sav"
            )

        else:
            raise ValueError("'{}' como 'arg' incorreto".format(arg))

    def realiza_avaliacao(self,
                          angulo_livre_caminho: numpy.uint8,
                          distancia_ao_gol_adversario: numpy.uint8,
                          liberdade_marcacao_chutador: numpy.uint8) -> numpy.int16:
        """
        Realiza avaliação.

        Todas as entradas devem estar em uma escala de 0 a 250

        Parameters
        ----------
        angulo_livre_caminho : numpy.uint8
            Ângulo livre para finalização
        distancia_ao_gol_adversario : numpy.uint8:
            Distância entre a posição da bola e o gol adversário (em milimetros)
        liberdade_marcacao_chutador : numpy.uint8
            Este parâmetro representa a liberdade de marcação do chutador,
            equivale à distância do adversário mais próximo

        Returns
        -------
        numpy.int16
            Avaliação da jogada em uma escala de 0 a 250

        """
        return numpy.int16(self.model.predict([[
            angulo_livre_caminho,
            distancia_ao_gol_adversario,
            liberdade_marcacao_chutador
            ]])[0])
