#
# SSL-Strategy
# Copyright (C) 2020  RoboFEI / Small Size League
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


"""
Decisão das jogadas de passe.

Mais detalhes.
"""

import numpy
import joblib
import os


class AvaliacaoJogadaPasse:
    """
    Classe AvaliacaoJogadaPasse.

    More details etc etc.

    Attributes
    ----------
    model
        Modelo de Supervised Learning utilizado na previsão das jogadas

    """

    def __init__(self, arg: str):
        """
        Construtor padrão da classe.

        Carrega o modelo indicado pelo parâmetro 'arg' utilizado nas previsões.

        Parameters
        ----------
        self
            Referência à própria instância da classe
        arg : str{'knn', 'linear_regression', 'ada', 'gradient', 'random_forest'}
            Indica o nome do modelo a ser utilizado

        """

        path_to_model: str = os.path.dirname(os.path.realpath(__file__))+"/models"

        if arg == "knn":

            from sklearn.neighbors import KNeighborsRegressor

            self.model: KNeighborsRegressor = joblib.load(
                path_to_model+"/avaliacao_passe_knn.sav"
            )

        elif arg == "linear_regression":

            from sklearn.linear_model import ElasticNet

            self.model: ElasticNet = joblib.load(
                path_to_model+"/avaliacao_passe_lr.sav"
            )

        elif arg == "ada":
            from sklearn.ensemble import AdaBoostRegressor

            self.model: AdaBoostRegressor = joblib.load(
                path_to_model+"/avaliacao_passe_ada_boost.sav"
            )

        elif arg == "gradient":
            from sklearn.ensemble import GradientBoostingRegressor

            self.model: GradientBoostingRegressor = joblib.load(
                path_to_model+"/avaliacao_passe_gradient.sav"
            )

        elif arg == "random_forest":
            from sklearn.ensemble import RandomForestRegressor

            self.model: RandomForestRegressor = joblib.load(
                path_to_model+"/avaliacao_passe_forest.sav"
            )

        else:
            raise ValueError("'{}' como 'arg' incorreto".format(arg))

    def realiza_avaliacao(self,
                          angulo_livre_caminho: numpy.uint8,
                          distancia_passe: numpy.uint8,
                          liberdade_marcacao_receptor: numpy.uint8,
                          angulo_redirect: numpy.uint8,
                          angulo_livre_para_chute_receptor: numpy.uint8,
                          distancia_gol_receptor: numpy.uint8,
                          liberdade_marcacao_passador: numpy.uint8,
                          delta_xis: numpy.uint8) -> numpy.int16:
        """
        Realiza avaliação.

        Todas as entradas devem estar em uma escala de 0 a 250

        Parameters
        ----------
        angulo_livre_caminho : numpy.uint8
            Ângulo livre para finalização
        distancia_passe : numpy.uint8
            Distância entre o passador e o receptor
        liberdade_marcacao_receptor : numpy.uint8
            Este parâmetro representa a liberdade de marcação do receptor,
            equivale à distância do adversário mais próximo
        angulo_redirect : numpy.uint8
            Ângulo de rotação para o receptor rotacionar até estar em frente
            ao gol, estando inicialmente mirando no passador
        angulo_livre_para_chute_receptor : numpy.uint8
            Ângulo livre para o receptor chutar ao gol após receber o passe
        distancia_gol_receptor : numpy.uint8
            Distância do receptor ao gol
        liberdade_marcacao_passador : numpy.uint8
            Este parâmetro representa a liberdade de marcação do passador,
            equivale à distância do adversário mais próximo
        delta_xis : numpy.uint8
            Indica se o passe é progressivo (em direção ao ataque) ou
            regressivo (recuo). Valores mais altos indicam passes progressivos,
            125 é o valor neutro.

        Returns
        -------
        numpy.int16
            Avaliação da jogada em uma escala de 0 a 250

        """
        return numpy.int16(self.model.predict([[
            angulo_livre_caminho,
            distancia_passe,
            liberdade_marcacao_receptor,
            angulo_redirect,
            angulo_livre_para_chute_receptor,
            distancia_gol_receptor,
            liberdade_marcacao_passador,
            delta_xis
            ]])[0])
