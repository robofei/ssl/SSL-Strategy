from setuptools import setup, find_packages

setup(
    name='robofeissl',
    version='0.0',
    url='https://gitlab.com/robofei/ssl/SSL-Strategy/-/tree/master/src/python',
    author='Bruno Bollos Correa',
    author_email='bollos@outlook.com.br',
    license='GPLv3',
    zip_safe=False,
    packages=find_packages(where='robofeissl'),
    install_requires=['numpy', 'sklearn', 'joblib'],
    package_dir={'decisoes_jogadas': 'robofeissl/decisoes_jogadas'},
    package_data={'decisoes_jogadas': ['models/*.sav']}
)
