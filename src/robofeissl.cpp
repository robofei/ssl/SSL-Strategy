/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "robofeissl.h"
#include "ui_robofeissl.h"

#include <QColorDialog>
#include <QDebug>
#include <QElapsedTimer>
#include <QScreen>
#include <QStandardPaths>
#include <QVBoxLayout>

#include "QtMessageFilter/qtmessagefilter.h"

RoboFeiSSL::RoboFeiSSL(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::RoboFeiSSL)
{
    this->setObjectName("RoboFeiSSL");

    ui->setupUi(this);

    player = new QMediaPlayer;
    player->setMuted(true);
    bMutar = false;
    bFechar = false;
    bRodando = false;
    bInicializaInterface = false;

    threadVisao = new QThread(this);
    threadEstrategia = new QThread(this);
    threadMovimentacao = new QThread(this);
    threadTestes = new QThread(this);

    this->thread()->setObjectName("Main");
    threadVisao->setObjectName("Visao");
    threadEstrategia->setObjectName("Estrategia");
    threadMovimentacao->setObjectName("Movimentacao");
    threadTestes->setObjectName("Testes");

    vAjustaTamanhoInterface();

    iFPSCounter.clear();

    //==================================== Campo
    //==================================

    acInterface = new AmbienteCampo();
    referee.reset(new RefereeReceiver());

    //=============================================================================

    //==================================== Interface
    //==============================

    opOpcoesInterface.reset(new OpcoesInterface);
    connect(opOpcoesInterface.get(), &OpcoesInterface::vOpcaoMudou, this,
            &RoboFeiSSL::vRecebeOpcaoInterface);

    dmDesenhaCampo.reset();
    tmrAtualizacaoDesenhos.reset();
    tmrAtualizacaoDadosInterface.reset();

    simRefBox.reset();
    velDialog.reset();

    fileMatrizJogadas.setFileName(QStringLiteral("MatrizJogadas.txt"));
    fileMatrizJogadas.close();

    vConfiguraInterface();

    //==================================================================================================================

    //===================================== Testes
    //================================
    trTestesRobos.reset();
    mcTesteMovimentacaoClick.reset(new MovimentacaoClick);
    tppTestePathPlanners.reset();
    fileLogsMovimentacao.close();

    //=============================================================================
    vInicializaShortcutsAdicionais();

    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setStyleSheet("");
    this->showMaximized();

    config = RoboFeiConfig(ui);
    vCarregarConfiguracoes(true);
    vConfiguraTemaAplicacao(ui->cb_TemaInterface->currentText());
}

void RoboFeiSSL::resizeEvent(QResizeEvent* e)
{
    Q_UNUSED(e)

    vAjustaTamanhoInterface();
}

RoboFeiSSL::~RoboFeiSSL()
{
    vSalvarConfiguracoes("LastSession.cfg", true);

    bFechar = true;

    emit vAtivarThread(false);

    emit vDesconectarVisao();

    QThread::currentThread()->msleep(20);

    if (threadMovimentacao->isRunning())
    {
        threadMovimentacao->quit();
        if (!threadMovimentacao->wait(1000))
            qFatal("Thread Movimentação não finalizou em 1000ms");
    }

    if (threadEstrategia->isRunning())
    {
        threadEstrategia->quit();
        if (!threadEstrategia->wait(1000))
            qFatal("Thread Estratégia não finalizou em 1000ms");
    }

    if (threadTestes->isRunning())
    {
        threadTestes->quit();
        if (!threadTestes->wait(1000))
            qFatal("Thread Testes não finalizou em 1000ms");
    }

    if (threadVisao->isRunning())
    {
        threadVisao->quit();
        if (!threadVisao->wait(1000))
            qFatal("Thread Visão não finalizou em 1000ms");
    }

    QtMessageFilter::releaseInstance();

    delete acInterface;
    qApp->quit();
}

void RoboFeiSSL::vConectarRede()
{
    if (ui->pb_Rede_RecebePacotes->text() == "Desconectar")
    {
        vDesconectarRede();
        return;
    }

    qInfo() << "Rede conectada";

    ui->pb_Rede_RecebePacotes->setText(QStringLiteral("Desconectar"));
    ui->pb_Rede_RecebePacotes->setIcon(QIcon(":/Imagens/disconnect.png"));

    referee->vSetConnectionInfo(globalConfig.network.referee.ip,
                                globalConfig.network.referee.port,
                                ui->cb_Rede_InterfaceRede->currentText());
    bool refOk = referee->bConnect();
    Q_UNUSED(refOk);

    ui->pb_Rede_RecebePacotes->disconnect(SIGNAL(pressed()));
    connect(ui->pb_Rede_RecebePacotes, &QAbstractButton::pressed, this,
            &RoboFeiSSL::vDesconectarRede);

    // Inicia a visão
    visVisao.reset(new Visao());
    connect(this, &RoboFeiSSL::vEnviaConfiguracoesRedeVisao, visVisao.get(),
            &Visao::vConectarInterfaceRede);

    connect(this, &RoboFeiSSL::vDesconectarVisao, visVisao.get(),
            &Visao::vDesconectar);

    connect(this, &RoboFeiSSL::vEnviarTesteMeioCampo, visVisao.get(),
            &Visao::vSetaTesteMeioCampo);

    connect(this, &RoboFeiSSL::vEnviarTimeAtual, visVisao.get(),
            &Visao::vTrocaTimeAtual);

    connect(visVisao.get(), &Visao::_vVisionData, this,
            &RoboFeiSSL::vReceiveVisionData_);

    connect(referee.get(), &RefereeReceiver::_vRefereeData, this,
            &RoboFeiSSL::vReceiveRefereeData_);

    visVisao->moveToThread(threadVisao);
    threadVisao->start();

    emit vEnviaConfiguracoesRedeVisao(ui->cb_Rede_InterfaceRede->currentText());
    emit vEnviarTimeAtual(static_cast<int>(acInterface->allies->teamColor()));

    if (opOpcoesInterface.get()->bOpcaoAtivada("Criar Logs"))
    {
        QString fileNameVisao = QFileDialog::getSaveFileName(
                    this, "LogVisao", "LogVisao.txt", "TXT files (.txt)",
                    nullptr, QFileDialog::DontUseNativeDialog),

                fileNameReferee = QFileDialog::getSaveFileName(
                    this, "LogReferee", "LogReferee.txt", "TXT files (.txt)",
                    nullptr, QFileDialog::DontUseNativeDialog);

        if (fileNameVisao != "" && fileNameReferee != "")
            RoboController.start_log(fileNameReferee, fileNameVisao);
    }
}

void RoboFeiSSL::vDesconectarRede()
{
    vPararJogo();

    qInfo() << "Rede desconectada";

    // Atualiza os elementos da interface
    ui->pb_Rede_RecebePacotes->setText(QStringLiteral("Conectar"));
    ui->pb_Rede_RecebePacotes->setIcon(QIcon(":/Imagens/connect.png"));

    bInicializaInterface = false;
    ui->pb_ComecarJogo->setEnabled(false);
    ui->pb_ComecarTestes->setEnabled(false);

    ui->pb_Rede_RecebePacotes->disconnect(SIGNAL(pressed()));
    connect(ui->pb_Rede_RecebePacotes, &QPushButton::pressed, this,
            &RoboFeiSSL::vConectarRede);

    // Sinaliza para a visão desconectar-se da rede
    emit vDesconectarVisao();
    QThread::currentThread()->msleep(
        20); // pausa a thread atual para dar tempo da visão se desconectar

    // Finaliza a thread
    threadVisao->quit();
    if (!threadVisao->wait(1000))
        qFatal("Thread Visão não finalizou em 1000ms");
    visVisao.reset();

    // Desconecta o refbox
    referee->vDisconnect();

    if (opOpcoesInterface.get()->bOpcaoAtivada("Criar Logs"))
        RoboController.stop_log();
}

void RoboFeiSSL::vReceiveRefereeData_(const RefereePacket _packet)
{
    if (bFechar == true)
        return;

    acInterface->sslrefComandoRefereeAtual = _packet.sslrefCommand;

    if (acInterface->allies->teamColor() == timeAzul)
    {
        // Retira a jogada de goleiro do robo anterior
        if (acInterface->allies->iGetGoalieID() != _packet.blueTeam->iGoalieID)
        {
            acInterface->allies->getPlayer(acInterface->allies->iGetGoalieID())
                ->vSetaJogada(Nenhuma);
        }
        acInterface->allies->vSetGoalieID(_packet.blueTeam->iGoalieID);
        acInterface->opponents->vSetGoalieID(_packet.yellowTeam->iGoalieID);
    }
    else
    {
        // Retira a jogada de goleiro do robo anterior
        if (acInterface->allies->iGetGoalieID() !=
            _packet.yellowTeam->iGoalieID)
        {
            acInterface->allies->getPlayer(acInterface->allies->iGetGoalieID())
                ->vSetaJogada(Nenhuma);
        }
        acInterface->allies->vSetGoalieID(_packet.yellowTeam->iGoalieID);
        acInterface->opponents->vSetGoalieID(_packet.blueTeam->iGoalieID);
    }
    if (ui->cb_ConfigGerais_Goleiro->currentIndex() !=
        acInterface->allies->iGetGoalieID())
    {
        qInfo() << "[UI] Atualizando o goleiro -> "
                << acInterface->allies->iGetGoalieID();
        ui->cb_ConfigGerais_Goleiro->setCurrentIndex(
            acInterface->allies->iGetGoalieID());
    }

    acInterface->vt2dPosicaoBallPlacement = _packet.vt2dBallPlacement;

    ui->time_TempoRestante->setTime(_packet.timeLeft);
    ui->le_EstadoJogoAtual->setText(_packet.strStage);
    if (ui->tb_ComandosRecebidosReferee->toPlainText().size() > 2000)
        ui->tb_ComandosRecebidosReferee->clear();
    ui->tb_ComandosRecebidosReferee->append(_packet.strCommand);

    ui->gb_TimeAmarelo->setTitle(_packet.yellowTeam->strName);
    ui->lcd_TimeAmarelo_Gols->display(
        QString::number(_packet.yellowTeam->iGoals));
    ui->lcd_TimeAmarelo_Cartoes_Vermelho->display(
        QString::number(_packet.yellowTeam->iRedCards));
    ui->lcd_TimeAmarelo_Cartoes_Amarelo->display(
        QString::number(_packet.yellowTeam->iYellowCards));
    if (!_packet.yellowTeam->timeYellowLeft.isEmpty())
    {
        ui->time_TimeAmarelo_TempoCartaoAmarelo->setTime(
            _packet.yellowTeam->timeYellowLeft.constFirst());
    }
    ui->lcd_TimeAmarelo_TimeoutsPedidos->display(
        QString::number(_packet.yellowTeam->iTimeouts));
    ui->time_TimeAmarelo_TempoTimeout->setTime(_packet.yellowTeam->timeTimeout);
    ui->lcd_TimeAmarelo_IDGoleiro->display(
        QString::number(_packet.yellowTeam->iGoalieID));

    ui->gb_TimeAzul->setTitle(_packet.blueTeam->strName);
    ui->lcd_TimeAzul_Gols->display(QString::number(_packet.blueTeam->iGoals));
    ui->lcd_TimeAzul_Cartoes_Vermelho->display(
        QString::number(_packet.blueTeam->iRedCards));
    ui->lcd_TimeAzul_Cartoes_Amarelo->display(
        QString::number(_packet.blueTeam->iYellowCards));
    if (!_packet.blueTeam->timeYellowLeft.isEmpty())
    {
        ui->time_TimeAzul_TempoCartaoAmarelo->setTime(
            _packet.blueTeam->timeYellowLeft.constFirst());
    }
    ui->lcd_TimeAzul_TimeoutsPedidos->display(
        QString::number(_packet.blueTeam->iTimeouts));
    ui->time_TimeAzul_TempoTimeout->setTime(_packet.blueTeam->timeTimeout);
    ui->lcd_TimeAzul_IDGoleiro->display(
        QString::number(_packet.blueTeam->iGoalieID));

    // TODO: Reimplementar isso numa classe separada que se conecta no sinal de
    // dados do Referee
    //         if(bMutar == false)
    //         {
    //             if(sslreferee.command() == SSL_Referee_Command_GOAL_BLUE &&
    //             acInterface->allies->teamColor() == timeAzul)
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/vinheta_gol_fox_sports.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if(sslreferee.command() ==
    //             SSL_Referee_Command_GOAL_YELLOW &&
    //             acInterface->allies->teamColor() == timeAmarelo)
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/vinheta_gol_fox_sports.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if((sslreferee.command() ==
    //             SSL_Referee_Command_GOAL_YELLOW &&
    //             acInterface->allies->teamColor() != timeAmarelo) ||
    //                     (sslreferee.command() ==
    //                     SSL_Referee_Command_GOAL_BLUE &&
    //                     acInterface->allies->teamColor() != timeAzul))
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/tomou-na-jabiraca.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if((sslreferee.command() ==
    //             SSL_Referee_Command_PREPARE_PENALTY_BLUE &&
    //             acInterface->allies->teamColor() == timeAzul)
    //                     || (sslreferee.command() ==
    //                     SSL_Referee_Command_PREPARE_PENALTY_YELLOW &&
    //                     acInterface->allies->teamColor() == timeAmarelo))
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/velho_oeste.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if((sslreferee.command() ==
    //             SSL_Referee_Command_PREPARE_PENALTY_BLUE &&
    //             acInterface->allies->teamColor() != timeAzul)
    //                     || (sslreferee.command() ==
    //                     SSL_Referee_Command_PREPARE_PENALTY_YELLOW &&
    //                     acInterface->allies->teamColor() != timeAmarelo))
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/suspense.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if((sslreferee.command() ==
    //             SSL_Referee_Command_PREPARE_KICKOFF_BLUE ||
    //             sslreferee.command() ==
    //             SSL_Referee_Command_PREPARE_KICKOFF_YELLOW) &&
    //                     sslreferee.stage() ==
    //                     SSL_Referee_Stage_NORMAL_FIRST_HALF_PRE)
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/vinheta_champions_league.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if((sslreferee.command() ==
    //             SSL_Referee_Command_PREPARE_KICKOFF_BLUE ||
    //             sslreferee.command() ==
    //             SSL_Referee_Command_PREPARE_KICKOFF_YELLOW) &&
    //                     sslreferee.stage() ==
    //                     SSL_Referee_Stage_NORMAL_SECOND_HALF_PRE)
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/century-fox-flute.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if((sslreferee.command() ==
    //             SSL_Referee_Command_TIMEOUT_BLUE &&
    //             acInterface->allies->teamColor() == timeAzul) ||
    //                     (sslreferee.command() ==
    //                     SSL_Referee_Command_TIMEOUT_YELLOW &&
    //                     acInterface->allies->teamColor() == timeAmarelo))
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/mission-impossible.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if((sslreferee.command() ==
    //             SSL_Referee_Command_TIMEOUT_BLUE &&
    //             acInterface->allies->teamColor() != timeAzul) ||
    //                     (sslreferee.command() ==
    //                     SSL_Referee_Command_TIMEOUT_YELLOW &&
    //                     acInterface->allies->teamColor() != timeAmarelo))
    //             {
    //                 if(player->isMuted())
    //                 {
    //                     player->setMuted(false);
    //                     player->setMedia(QUrl("qrc:/Sons/Elevator-music.mp3"));
    //                     player->setVolume(50);
    //                     player->play();
    //                     etmTempoMusica.restart();
    //                 }
    //             }
    //             else if(sslreferee.command() == SSL_Referee_Command_STOP ||
    //             sslreferee.command() == SSL_Referee_Command_HALT)
    //             {
    //                 player->stop();
    //                 player->setMuted(true);
    //             }
    //
    //             if(!player->isMuted() && etmTempoMusica.nsecsElapsed()/1e9 >
    //             10)//toca por 5s
    //             {
    //                 player->stop();
    //                 player->setMuted(true);
    //             }
    //         }
}

void RoboFeiSSL::vReceiveVisionData_(const VisionPacket _packet)
{
    if (bFechar == true)
        return;

    acInterface->vAtualizaDadosVisao(_packet);

    if (_packet.bGeometryChanged == true ||
        (dmDesenhaCampo && dmDesenhaCampo->sizeField() == QSize(0, 0)))
    {
        bInicializaInterface = true;
        // Assim que chega o pacote de geometria habilita os botoes de comecar
        // jogo e comecar teste
        if (!ui->pb_ComecarJogo->isEnabled() &&
            ui->pb_ComecarTestes->text() != QLatin1String("Parar"))
            ui->pb_ComecarJogo->setEnabled(true);

        if (!ui->pb_ComecarTestes->isEnabled() &&
            ui->pb_ComecarJogo->text() != QLatin1String("Parar"))
            ui->pb_ComecarTestes->setEnabled(true);
        vInicializaDesenhos();
    }

    if (bInicializaInterface == true)
    {
        if (tmrAtualizacaoDesenhos->isActive() == false)
        {
            tmrAtualizacaoDesenhos->setTimerType(Qt::PreciseTimer);
            tmrAtualizacaoDesenhos->start(iTempoLoopInterface);
        }
        if (tmrAtualizacaoDadosInterface->isActive() == false)
        {
            tmrAtualizacaoDadosInterface->setTimerType(Qt::PreciseTimer);
            tmrAtualizacaoDadosInterface->start(4 * iTempoLoopInterface);
        }
    }
}

void RoboFeiSSL::vReceiveFeedbackData_(const RadioFeedback& _feedback)
{
    Robo* robo;
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        robo = acInterface->allies->getPlayer(id);
        robo->vAtualizaDadosRadio(_feedback);
    }
}

void RoboFeiSSL::vRecebeErroRadio(const QString& strErro)
{
    // Mostra o erro durante 5 segundos
    if (strErro != "Conectado" && strErro != "Desconectado")
        ui->sb_StatusRadio->showMessage("Falha ao conectar rádio: " + strErro,
                                        5000);
    else
        ui->sb_StatusRadio->showMessage("Rádio " + strErro, 5000);
}

void RoboFeiSSL::vRecebeErroPacote(ErrorCode erro)
{
    switch (erro)
    {

    case CHECKSUM_ERROR: {
        ui->sb_StatusRadio->showMessage(QStringLiteral("CHECKSUM ERROR"), 400);
    }
    break;

    case DELIMITER_NOT_FOUND: {
        ui->sb_StatusRadio->showMessage(
            QStringLiteral("DELIMITER NOT FOUND ERROR"), 400);
    }
    break;

    case PACKET_INCOMPLETE: {
        ui->sb_StatusRadio->showMessage(
            QStringLiteral("PACKET INCOMPLETE ERROR"), 400);
    }
    break;

    case OTHER_RADIO_PCKT: {
        ui->sb_StatusRadio->showMessage(
            QStringLiteral("OTHER RADIO PACKET ERROR"), 400);
    }
    break;

    case SAFETY_THRESHOLD_ERROR: {
        ui->sb_StatusRadio->showMessage(
            QStringLiteral("SAFETY THRESHOLD ERROR"), 400);
    }
    break;

    default:
        break;
    }
}

void RoboFeiSSL::vConfiguraInterface()
{
    tmrAtualizacaoDesenhos.reset(new QTimer(this));
    tmrAtualizacaoDadosInterface.reset(new QTimer(this));

    connect(tmrAtualizacaoDesenhos.get(), &QTimer::timeout, this,
            &RoboFeiSSL::vAtualizaGraficosVelocidades);

    // Conecxão do timer que atualiza os dados da interface
    connect(tmrAtualizacaoDadosInterface.get(), &QTimer::timeout, this,
            &RoboFeiSSL::vAtualizaDadosInterface);

    connect(ui->pb_ComecarJogo, &QPushButton::pressed, this,
            &RoboFeiSSL::vComecarJogo);
    connect(ui->pb_ComecarTestes, &QPushButton::pressed, this,
            &RoboFeiSSL::vComecarTestes);

    QPixmap pmLed;
    pmLed.load(":/Imagens/Leds/circle_red.svg", "svg");
    pmLed = pmLed.scaled(25, 25);
    ui->lb_LED_Jogo->setPixmap(pmLed);
    pmLed.load(":/Imagens/Leds/circle_red.svg", "svg");
    pmLed = pmLed.scaled(25, 25);
    ui->lb_LED_Teste->setPixmap(pmLed);

    ui->pb_ComecarJogo->setEnabled(false);
    ui->pb_ComecarTestes->setEnabled(false);

    etmTempoGraficoVelocidades.start();

    vInicializaStatusRobos();
    //======================================================== Rede
    //====================================

    QList<QNetworkInterface> ifInterfaceLocal =
        QNetworkInterface::allInterfaces();
    for (auto& i : ifInterfaceLocal)
    {
        ui->cb_Rede_InterfaceRede->addItem(i.humanReadableName());
    }

    connect(ui->pb_Rede_RecebePacotes, &QPushButton::pressed, this,
            &RoboFeiSSL::vConectarRede);

    //==================================================================================================

    //======================================================== Radio
    //===================================
    ui->cb_Radio_Portas->clear();
    ui->cb_Radio_BaudRate->clear();

    QList<QSerialPortInfo> portasDisponiveis =
        QSerialPortInfo::availablePorts();

    ui->cb_Radio_BaudRate->setInsertPolicy(QComboBox::NoInsert);

    ui->cb_Radio_BaudRate->addItem(QStringLiteral("9600"),
                                   QSerialPort::Baud9600);
    ui->cb_Radio_BaudRate->addItem(QStringLiteral("19200"),
                                   QSerialPort::Baud19200);
    ui->cb_Radio_BaudRate->addItem(QStringLiteral("38400"),
                                   QSerialPort::Baud38400);
    ui->cb_Radio_BaudRate->addItem(QStringLiteral("115200"),
                                   QSerialPort::Baud115200);
    ui->cb_Radio_BaudRate->addItem(QStringLiteral("230400"), 230400);

    for (const QSerialPortInfo& info : qAsConst(portasDisponiveis))
    {
        QStringList list;
        list << info.portName();
        ui->cb_Radio_Portas->addItem(list.first(), list);
    }

    ui->cb_Radio_BaudRate->setCurrentIndex(4);

    connect(ui->pb_Radio_AtualizarPortas, SIGNAL(pressed()), this,
            SLOT(vAtualizaPortas()));

    //==================================================================================================

    //======================================================== Estrategia
    //==============================

    qRegisterMetaType<AmbienteCampo>();
    qRegisterMetaType<VisionPacket>();
    qRegisterMetaType<RefereePacket>();
    qRegisterMetaType<RadioFeedback>();
    qRegisterMetaType<ConfiguracaoRadio>();
    qRegisterMetaType<MotionFeedbackPacket>();
    qRegisterMetaType<MotionPathFeedback>();
    qRegisterMetaType<ErrorCode>();

    //==================================================================================================

    //=================================================== Configuracoes gerais
    //=========================

    ui->cb_ConfigGerais_CorTime->clear();
    ui->cb_ConfigGerais_LadoDefesa->clear();

    ui->cb_ConfigGerais_CorTime->addItem(QStringLiteral("Azul"),
                                         TeamColor::timeAzul);
    ui->cb_ConfigGerais_CorTime->addItem(QStringLiteral("Amarelo"),
                                         TeamColor::timeAmarelo);

    ui->cb_ConfigGerais_LadoDefesa->addItem(QStringLiteral("X Negativo"),
                                            XNegativo);
    ui->cb_ConfigGerais_LadoDefesa->addItem(QStringLiteral("X Positivo"),
                                            XPositivo);

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
        ui->cb_ConfigGerais_Goleiro->addItem(QStringLiteral("Robo %1").arg(i),
                                             i);

    ui->cb_CampoReal_Grsim->addItem(QStringLiteral("GrSim"), GR_SIM);
    ui->cb_CampoReal_Grsim->addItem(QStringLiteral("Campo Real"), CAMPO_REAL);

    ui->cb_ConfigGerais_TesteMeioCampo->addItem(
        QStringLiteral("Campo Completo"), CampoInteiro);
    ui->cb_ConfigGerais_TesteMeioCampo->addItem(QStringLiteral("X Positivo"),
                                                XPositivo);
    ui->cb_ConfigGerais_TesteMeioCampo->addItem(QStringLiteral("X Negativo"),
                                                XNegativo);

    connect(ui->cb_ConfigGerais_CorTime,
            QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            &RoboFeiSSL::vCorTimeMudou);
    connect(ui->cb_ConfigGerais_LadoDefesa,
            QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            &RoboFeiSSL::vLadoDefesaMudou);
    connect(ui->cb_ConfigGerais_Goleiro,
            QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            &RoboFeiSSL::vGoleiroMudou);
    connect(ui->cb_CampoReal_Grsim,
            QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            &RoboFeiSSL::vTipoAmbienteMudou);

    ui->qcp_Movimentacao_Grafico->addGraph();

    Auxiliar::vConfiguraDarkModePlot(ui->qcp_Movimentacao_Grafico);

    ui->qcp_Movimentacao_Grafico->xAxis->setLabel(QStringLiteral("t (s)"));
    ui->qcp_Movimentacao_Grafico->xAxis->setRange(0, 10);
    ui->qcp_Movimentacao_Grafico->yAxis->setLabel(QStringLiteral("v (m/s)"));
    ui->qcp_Movimentacao_Grafico->yAxis->setRange(0, 3);
    ui->qcp_Movimentacao_Grafico->setInteractions(QCP::iRangeDrag |
                                                  QCP::iRangeZoom);

    { // Configurações Decisões de Jogadas
        const QStringList modelos{"Linear Model", "AdaBoost",
                                  "Gradient Boosting", "Random Forest",
                                  "KNearestNeighbor"};

        ui->cb_ModeloPasse->addItems(modelos);
        ui->cb_ModeloChute->addItems(modelos);

        // Seta os modelos padrões
        ui->cb_ModeloPasse->setCurrentText("AdaBoost");
        ui->cb_ModeloChute->setCurrentText("AdaBoost");

        // Desabilita o item com a opção "KNearestNeighbor" enquanto este ainda
        // está em fase de teste
        //        QStandardItemModel* model =
        //        qobject_cast<QStandardItemModel*>(ui->cb_ModeloPasse->model());
        //        model->item(4)->setFlags(model->item(2)->flags() &
        //        ~Qt::ItemIsEnabled);

        //        model =
        //        qobject_cast<QStandardItemModel*>(ui->cb_ModeloChute->model());
        //        model->item(4)->setFlags(model->item(2)->flags() &
        //        ~Qt::ItemIsEnabled);
    }

    {
        const QStringList opcoes{"Contorna a bola",
                                 "Gira com a bola no roller"};

        ui->cb_DribleDoRobo->addItems(opcoes);

        ui->cb_DribleDoRobo->setCurrentText("Contorna a bola");
    }
    //==================================================================================================

    //=================================================== Graficos
    //=====================================

    //==================================================================================================

    //==================================================================================================

    //=================================================== Configuracoes Matriz
    // Jogadas =================
    QStringList strlLabels;
    QVector<QString> strJogadas = {"Cobrador KickOff",  "Cobrador Penalty",
                                   "Cobrador Indirect", "Cobrador Direct",
                                   "Robo Extra Bola",   "Receptor Indirect"};

    ui->tbw_ConfigJogadas->setColumnCount(globalConfig.robotsPerTeam);
    ui->tbw_ConfigJogadas->setRowCount(6);
    ui->tbw_ConfigJogadas->verticalHeader()->setVisible(true);

    for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
        strlLabels << QString::number(n);
    ui->tbw_ConfigJogadas->setHorizontalHeaderLabels(strlLabels);

    strlLabels.clear();

    for (int n = 0; n < strJogadas.size(); ++n)
        strlLabels << strJogadas.at(n);
    ui->tbw_ConfigJogadas->setVerticalHeaderLabels(strlLabels);

    if (bCarregaMatrizJogadas() == false)
    {
        for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
        {
            for (int j = 0; j < iJogadasAtaque; ++j)
            {
                QTableWidgetItem* tbwiAux = new QTableWidgetItem(true);
                tbwiAux->setBackground(Qt::green);
                ui->tbw_ConfigJogadas->setItem(j, i, tbwiAux);
                ui->tbw_ConfigJogadas->item(j, i)->setData(0, true);
            }
        }
    }
    else
    {
        bool bEstado = true;
        QColor clrCor = Qt::green;

        for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
        {
            for (int j = 0; j < iJogadasAtaque; ++j)
            {
                if (acInterface->bMatrizJogadas[i][j] == true)
                {
                    bEstado = true;
                    clrCor = Qt::green;
                }
                else
                {
                    bEstado = false;
                    clrCor = Qt::red;
                }

                QTableWidgetItem* tbwiAux = new QTableWidgetItem(bEstado);
                tbwiAux->setBackground(clrCor);
                ui->tbw_ConfigJogadas->setItem(j, i, tbwiAux);
                ui->tbw_ConfigJogadas->item(j, i)->setData(0, bEstado);
            }
        }
    }

    ui->tbw_ConfigJogadas->setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(ui->pb_ConfigJogadas_Salvar, &QPushButton::clicked, this,
            &RoboFeiSSL::vAtualizaMatrizJogadas);
    connect(ui->pb_ConfigJogadas_Reset, &QPushButton::clicked, this,
            &RoboFeiSSL::vResetaMatriz);
    connect(ui->tbw_ConfigJogadas, &QTableWidget::cellDoubleClicked, this,
            &RoboFeiSSL::vCelulaAlterada);
    //==================================================================================================
}

void RoboFeiSSL::vAjustaTamanhoInterface()
{
    const int margem = 5;

    // Arruma aba 0 (jogo)
    const QSize tamanhoAbaJogo = ui->tab_Jogo->size();

    ui->scrollAreaTabJogo->setGeometry(margem, margem,
                                       tamanhoAbaJogo.width() - 2 * margem,
                                       tamanhoAbaJogo.height() - 2 * margem);

    ui->scrollAreaTabJogoWidgetContents->resize(ui->scrollAreaTabJogo->size());

    ui->widgetContentsTabJogo->resize(
        ui->scrollAreaTabJogoWidgetContents->size());

    // Arruma aba 1 (configurações)
    const QSize tamanhoAbaConfiguracoes = ui->tab_ConfiguracoesGerais->size();

    ui->scrollAreaTabConfiguracoes->setGeometry(
        margem, margem, tamanhoAbaConfiguracoes.width() - 2 * margem,
        tamanhoAbaConfiguracoes.height() - 2 * margem);

    ui->scrollAreaTabConfiguracoesWidgetContents->resize(
        ui->scrollAreaTabConfiguracoes->size());

    ui->widgetContentsTabConfiguracoes->resize(
        ui->scrollAreaTabConfiguracoesWidgetContents->size());
}

void RoboFeiSSL::vInicializaStatusRobos()
{
    gridStatusRobos = new QVector<QGridLayout*>;
    textsJogada = new QVector<QTextBrowser*>;

    layoutStatus = new QVBoxLayout(this);

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        gridStatusRobos->append(new QGridLayout(this));
        layoutStatus->addItem(gridStatusRobos->at(i));

        lstStatusRobos[i].reset(new IndicadorStatusRobo(this));
        textsJogada->append(new QTextBrowser(this));
        textsJogada->at(i)->setFontPointSize(9);

        connect(lstStatusRobos[i].get(), &IndicadorStatusRobo::clicked, this,
                &RoboFeiSSL::vMudaCorRobo);

        if (i < gridStatusRobos->size())
        {
            gridStatusRobos->at(i)->addWidget(lstStatusRobos[i].get(), 0, 0);
            gridStatusRobos->at(i)->addWidget(textsJogada->at(i), 0, 1);
        }
    }
    ui->scrollStatusRobos->setLayout(layoutStatus);
}

void RoboFeiSSL::vInicializaDesenhos()
{
    // Campo Jogo
    //    if(!dmDesenhaCampo)
    {
        ui->labelTemporarioCampo->hide();

        dmDesenhaCampo.reset(new DrawMap(acInterface, this));

        connect(tmrAtualizacaoDesenhos.get(), &QTimer::timeout,
                dmDesenhaCampo.get(), &DrawMap::vAnimar);

        connect(dmDesenhaCampo.get(), &DrawMap::clicked, this,
                &RoboFeiSSL::vControlaSimulacaoMouseClicked);

        connect(dmDesenhaCampo.get(), &DrawMap::released, this,
                &RoboFeiSSL::vControlaSimulacaoMouseReleased);

        connect(dmDesenhaCampo.get(), &DrawMap::moved, this,
                &RoboFeiSSL::vControlaSimulacaoMouseMoved);

        connect(dmDesenhaCampo.get(), &DrawMap::clicked,
                mcTesteMovimentacaoClick.get(),
                &MovimentacaoClick::vRecebeMouseClick);

        connect(mcTesteMovimentacaoClick.get(),
                &MovimentacaoClick::vEnviaTrajetoAtual, dmDesenhaCampo.get(),
                &DrawMap::vRecebeTrajetoAtualMClick);

        ui->gridCampo->addWidget(dmDesenhaCampo.get());
    }

    if (!tppTestePathPlanners)
    {
        if (!acInterface->geoCampo->szField().isEmpty())
        {
            tppTestePathPlanners.reset(
                new TestePathPlanners(acInterface->geoCampo->szField()));
            connect(dmDesenhaCampo.get(), &DrawMap::clicked,
                    tppTestePathPlanners.get(),
                    &TestePathPlanners::vRecebeMouseClick);
        }
    }
}

void RoboFeiSSL::vIniciaEstrategia()
{
    //     Decisoes::resetInstance();

    estEstrategia.reset(new Estrategia());

    // Dados da visao
    connect(visVisao.get(), &Visao::_vVisionData, estEstrategia.get(),
            &Estrategia::vReceiveVisionData_);

    // Dados do referee/game controller
    connect(referee.get(), &RefereeReceiver::_vRefereeData, estEstrategia.get(),
            &Estrategia::vReceiveRefereeData_);

    // Dados da interface
    connect(this, &RoboFeiSSL::vEnviaDadosInterface, estEstrategia.get(),
            &Estrategia::vRecebeDadosInterface);

    // Habilitacão da estratégia
    connect(this, &RoboFeiSSL::vAtivarThread, estEstrategia.get(),
            &Estrategia::vAtivaThread);

    // Comunicação entre estrategia e movimentacao
    connect(estEstrategia.get(), &Estrategia::vEnviaInformacoesMovimentacao,
            moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vRecebeInformacoesEstrategia);

    // Comunicação entre estrategia e movimentacao
    connect(moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vRetornaCaminhosAlterados, estEstrategia.get(),
            &Estrategia::vRecebeCaminhosAlterados);

    // Dados do rádio (Feedback dos robôs)
    connect(moveMovimentacaoRobo->rdRadio.get(),
            &RadioBase::_vSendRobotFeedback, estEstrategia.get(),
            &Estrategia::vReceiveRadioData_);
    connect(moveMovimentacaoRobo->rdRadio.get(),
            &RadioBase::_vSendRobotFeedback, visVisao.get(),
            &Visao::vReceiveRadioData_);
    connect(moveMovimentacaoRobo->rdRadio.get(),
            &RadioBase::_vSendRobotFeedback, this,
            &RoboFeiSSL::vReceiveFeedbackData_);

    // Dados da movimentação (Feedback da simulação)
    connect(moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::_vSendSimulationFeedback, estEstrategia.get(),
            &Estrategia::vReceiveRadioData_);
    connect(moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::_vSendSimulationFeedback, visVisao.get(),
            &Visao::vReceiveRadioData_);
    connect(moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::_vSendSimulationFeedback, this,
            &RoboFeiSSL::vReceiveFeedbackData_);

    estEstrategia->moveToThread(threadEstrategia);

    threadEstrategia->start();
}

void RoboFeiSSL::vIniciaTestes()
{
    trTestesRobos.reset(new TestesRobos());

    // Dados da visao
    connect(visVisao.get(), &Visao::_vVisionData, trTestesRobos.get(),
            &TestesRobos::vReceiveVisionData_);

    // Dados da interface
    connect(this, &RoboFeiSSL::vEnviaDadosInterface, trTestesRobos.get(),
            &TestesRobos::vRecebeDadosInterface);
    connect(this, &RoboFeiSSL::vEnviaDadosTestes, trTestesRobos.get(),
            &TestesRobos::vRecebeDadosTestes);
    connect(mcTesteMovimentacaoClick.get(),
            &MovimentacaoClick::vEnviaDadosTestes, trTestesRobos.get(),
            &TestesRobos::vRecebeDadosTestes);
    connect(tppTestePathPlanners.get(), &TestePathPlanners::vEnviaDadosTestes,
            trTestesRobos.get(), &TestesRobos::vRecebeDadosTestes);
    connect(trTestesRobos.get(), &TestesRobos::vEnviaProgressoTestePP,
            tppTestePathPlanners.get(),
            &TestePathPlanners::vRecebeProgressoTeste);

    // Habilitacão dos testes
    connect(this, &RoboFeiSSL::vAtivarThread, trTestesRobos.get(),
            &TestesRobos::vAtivaThread);

    // Comunicação entre os testes e a movimentacao
    connect(trTestesRobos.get(), &TestesRobos::vEnviaInformacoesMovimentacao,
            moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vRecebeInformacoesEstrategia);
    connect(trTestesRobos.get(),
            &TestesRobos::requestAvoidanceComputationalTime,
            moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::receiveComputationalTimeRequest,
            Qt::BlockingQueuedConnection);
    connect(trTestesRobos.get(), &TestesRobos::requestPrSBCConfig,
            moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::receivePrSBCConfigRequest,
            Qt::BlockingQueuedConnection);

    // Comunicação entre os testes e a movimentacao
    connect(moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vRetornaCaminhosAlterados, trTestesRobos.get(),
            &TestesRobos::vRecebeCaminhosAlterados);

    connect(trTestesRobos.get(), &TestesRobos::vEnviaTrajetosTestePathPlanner,
            dmDesenhaCampo.get(), &DrawMap::vRecebeTrajetosTestePathPlanner);

    // Conecta testes com o Joystick
    if (joystickRobotControllerInterface)
    {
        connect(joystickRobotControllerInterface.get(),
                &JoystickRobotControllerInterface::SIGNAL_enviaDadosRobo,
                trTestesRobos.get(),
                &TestesRobos::vRecebeDadosRoboVirtualJoystick);
    }

    // Conecta testes Decisões
    //     if(decisoesTeste)
    //     {
    //         connect(decisoesTeste.get(),
    //         &DecisoesTesteInterface::SIGNAL_solicitaTesteDecisoesPasse,
    //                 trTestesRobos.get(),
    //                 &TestesRobos::SLOT_recebeSolicitacaoTesteDecisaoPasse);
    //
    //         connect(decisoesTeste.get(),
    //         &DecisoesTesteInterface::SIGNAL_solicitaTesteDecisoesChute,
    //                 trTestesRobos.get(),
    //                 &TestesRobos::SLOT_recebeSolicitacaoTesteDecisaoChute);
    //
    //         connect(trTestesRobos.get(), &TestesRobos::SIGNAL_decisoesPasse,
    //                 decisoesTeste.get(),
    //                 &DecisoesTesteInterface::SLOT_decisoesPasse);
    //
    //         connect(trTestesRobos.get(), &TestesRobos::SIGNAL_decisoesChute,
    //                 decisoesTeste.get(),
    //                 &DecisoesTesteInterface::SLOT_decisoesChute);
    //     }

    trTestesRobos->moveToThread(threadTestes);

    threadTestes->start();
}

bool RoboFeiSSL::bIniciaMovimentacao()
{
    QString campoReal_GrSim = ui->cb_CampoReal_Grsim->currentText();
    bool OK = false;
    moveMovimentacaoRobo.reset(new MovimentacaoRobo());

    // Dados da visao
    connect(visVisao.get(), &Visao::_vVisionData, moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vReceiveVisionData_);

    // Habilitação da movimentação
    connect(this, &RoboFeiSSL::vAtivarThread, moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vAtivaThread);

    // Sinal de configuracao do radio
    connect(this, &RoboFeiSSL::sendRadioConfig, moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vRecebeDadosPortaSerial,
            Qt::BlockingQueuedConnection);

    // Envio dos dados de movimentação dos robôs
    connect(moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vRetornaVelocidadesRobos, this,
            &RoboFeiSSL::vRecebeDadosMovimentacao);

    // Envio das configurações para conectar com o simulador
    connect(this, &RoboFeiSSL::vEnviaConfiguracoesRedeSimulador,
            moveMovimentacaoRobo.get(),
            &MovimentacaoRobo::vRecebeConfiguracoesSimulador,
            Qt::BlockingQueuedConnection);

    moveMovimentacaoRobo->moveToThread(threadMovimentacao);
    threadMovimentacao->start();

    if (campoReal_GrSim == QLatin1String("GrSim"))
    {
        connect(this, &RoboFeiSSL::vMudarBolaSimulador,
                moveMovimentacaoRobo.get(),
                &MovimentacaoRobo::SLOT_mudaPosicaoBolaSimulacao);
        connect(this, &RoboFeiSSL::SIGNAL_mudarPosicaoRoboSimulador,
                moveMovimentacaoRobo.get(),
                &MovimentacaoRobo::SLOT_mudarPosicaoRoboSimulacao);
        connect(this, &RoboFeiSSL::SIGNAL_mudarAnguloRoboSimulacao,
                moveMovimentacaoRobo.get(),
                &MovimentacaoRobo::SLOT_mudarAnguloRoboSimulacao);

        int teamPort = globalConfig.network.blueTeam.cmdPort;
        if (acInterface->allies->teamColor() == timeAmarelo)
        {
            teamPort = globalConfig.network.yellowTeam.cmdPort;
        }

        emit vEnviaConfiguracoesRedeSimulador(
            globalConfig.network.simulation.controlPort,
            teamPort,
            ui->cb_Rede_InterfaceRede->currentText());
        /// \todo Se possível, confirmar a conexão com o simulador. Se não,
        /// remover esse todo
        OK = true; // Não há confirmação de conexão com o GrSim
    }
    else if (campoReal_GrSim == QLatin1String("Campo Real"))
    {
        ConfiguracaoRadio radioConfig;
        radioConfig.bConectar = false;
        emit sendRadioConfig(radioConfig);

        radioConfig.bConectar = true;
        radioConfig.baudRate =
            ui->cb_Radio_BaudRate
                ->itemData(ui->cb_Radio_BaudRate->currentIndex())
                .toInt();
        radioConfig.Porta = ui->cb_Radio_Portas->currentText();
        emit sendRadioConfig(radioConfig);

        // Dados do rádio (Feedback dos robôs)
        connect(moveMovimentacaoRobo->rdRadio.get(),
                &RadioBase::_vSendRobotFeedback, visVisao.get(),
                &Visao::vReceiveRadioData_);
        connect(moveMovimentacaoRobo->rdRadio.get(),
                &RadioBase::_vSendRobotFeedback, this,
                &RoboFeiSSL::vReceiveFeedbackData_);
        // Envio do status da conexão do rádio
        connect(moveMovimentacaoRobo->rdRadio.get(),
                &RadioBase::_vSendPortError, this,
                &RoboFeiSSL::vRecebeErroRadio);
        // Envio dos erros do pacote
        connect(moveMovimentacaoRobo->rdRadio.get(),
                &RadioBase::_vSendPacketError, this,
                &RoboFeiSSL::vRecebeErroPacote);

        OK = moveMovimentacaoRobo->bRadioConectado();
    }

    return OK;
}

void RoboFeiSSL::vSalvaMatrizJogadas()
{
    if (!fileMatrizJogadas.isOpen())
        fileMatrizJogadas.open(QIODevice::ReadOnly | QIODevice::Text |
                               QIODevice::ReadWrite);

    QTextStream stream(&fileMatrizJogadas);

    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        for (int j = 0; j < iJogadasAtaque; ++j)
        {
            stream << acInterface->bMatrizJogadas[i][j];
        }
    }
    fileMatrizJogadas.close();
}

bool RoboFeiSSL::bCarregaMatrizJogadas()
{
    if (!fileMatrizJogadas.isOpen())
        fileMatrizJogadas.open(QIODevice::ReadOnly | QIODevice::Text |
                               QIODevice::ReadWrite);

    QByteArray btAux = fileMatrizJogadas.readAll();

    if (btAux.size() == globalConfig.robotsPerTeam * iJogadasAtaque)
    {
        for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
        {
            for (int j = 0; j < iJogadasAtaque; ++j)
            {
                acInterface->bMatrizJogadas[i][j] =
                    btAux.at(i * iJogadasAtaque + j) - 48;
            }
        }

        fileMatrizJogadas.close();

        return true;
    }

    fileMatrizJogadas.close();
    return false;
}

void RoboFeiSSL::vAtualizaOpcoesDesenho()
{
    int FPSKalman = 0, FPSEst = 0, FPSMov = 0, FPS = iFPSInterface;

    dmDesenhaCampo.get()->vSetPathPlanners(
        opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Path Planner"));
    dmDesenhaCampo.get()->vSetTimeout(
        opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Posição Timeout"));
    dmDesenhaCampo.get()->vSetSaboia(
        opOpcoesInterface.get()->bOpcaoAtivada("Saboia"));
    dmDesenhaCampo.get()->vSetDirecaoRobo(
        opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Direção Robô"));
    dmDesenhaCampo.get()->vSetPosicoesSimulacao(
        opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Posições Simulação"));
    dmDesenhaCampo.get()->vSetMiraBola(
        opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Mira da Bola"));
    dmDesenhaCampo.get()->vSetLinhasGoleiro(
        opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Linhas Goleiro"));
    dmDesenhaCampo.get()->vSetPredicoesKalman(
        opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Predições Kalman"));
    dmDesenhaCampo.get()->vSetBallPlacement(
        opOpcoesInterface.get()->bOpcaoAtivada(
            "Mostrar Posição Ball Placement"));
    dmDesenhaCampo.get()->vSetRobosMines(
        opOpcoesInterface.get()->bOpcaoAtivada("Destacar Robôs Mines"));
    dmDesenhaCampo.get()->vSetDestacaReceptor(
        opOpcoesInterface.get()->bOpcaoAtivada("Destacar Receptor"));
    dmDesenhaCampo.get()->vSetVetorBola(
        opOpcoesInterface.get()->bOpcaoAtivada("Desenha Vetor Da Bola"));
    dmDesenhaCampo.get()->vSetReposicaoMouse(
        opOpcoesInterface.get()->bOpcaoAtivada("Reposição com Mouse") &&
        (ui->cb_CampoReal_Grsim->currentText() == QLatin1String("GrSim")) &&
        moveMovimentacaoRobo);
    dmDesenhaCampo.get()->vSetAntiAliasing(
        opOpcoesInterface.get()->bOpcaoAtivada("Anti-Aliasing"));
    dmDesenhaCampo.get()->vSetDestacaRegiaoRoles(
        opOpcoesInterface.get()->bOpcaoAtivada("Destacar Regiões das Roles"));

    //     if(Decisoes::good())
    //     {
    //         dmDesenhaCampo.get()->vSetLiberdadeRobos(
    //                 opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Liberdade
    //                 Receptores"), Decisoes::vtiAvalicoesReceptores(),
    //                 Decisoes::iTempoUltimaPrevisao(),
    //                 Decisoes::iAvaliacaoChuteAoGol());
    //     }

    // Mostra o grafo gerado para o robo selecionado
    if (opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Grafo"))
    //            acInterface->tsDadosTeste.tstTesteAtual != tstPathPlanner)
    {
        int id = ui->sb_Movimentacao_IDRobo->value();
        if (id >= globalConfig.robotsPerTeam)
            id = 0;
        VisibilityGraph grafo(iAnguloGrafo);
        grafo.vCriaGrafo(acInterface, id);
        dmDesenhaCampo.get()->vSetGrafo(true, &grafo);
    }
    else
        dmDesenhaCampo.get()->vSetGrafo(false);

    int cbIndex = ui->cb_ConfigGerais_TesteMeioCampo->currentIndex();
    dmDesenhaCampo.get()->vSetMeioCampo(qvariant_cast<int>(
        ui->cb_ConfigGerais_TesteMeioCampo->itemData(cbIndex)));

    if (threadVisao->isRunning())
        FPSKalman = visVisao->fGetFPSKalman();
    if (threadEstrategia->isRunning())
        FPSEst = estEstrategia->fGetFPSEstrategia();
    if (threadMovimentacao->isRunning())
        FPSMov = moveMovimentacaoRobo->fGetFPSControle();

    dmDesenhaCampo.get()->vSetaFPS(FPS, FPSKalman, FPSEst, FPSMov);
}

void RoboFeiSSL::vAtualizaStatusRobo(const Robo* _robot)
{
    Atributos atb = _robot->atbRetornaRobo();
    if (atb.id < gridStatusRobos->size()) //
    {
        int rodaParada = -1;
        // Detecta se existe alguma roda em que a odometria esta vindo como
        // zero, ou seja, está parada
        if (atb.dOdometriaRoda1.size() > 0)
        {
            if (atb.dOdometriaRoda1.last() * atb.dOdometriaRoda2.last() *
                    atb.dOdometriaRoda3.last() * atb.dOdometriaRoda4.last() <
                0.2)
            {
                if (atb.dOdometriaRoda1.last() < 0.2)
                    rodaParada = 1;
                else if (atb.dOdometriaRoda2.last() < 0.2)
                    rodaParada = 1;
                else if (atb.dOdometriaRoda3.last() < 0.2)
                    rodaParada = 3;
                else if (atb.dOdometriaRoda4.last() < 0.2)
                    rodaParada = 4;
            }
        }

        lstStatusRobos[atb.id].get()->vAtualizaStatus(
            atb, rodaParada, static_cast<int>(acInterface->allies->teamColor()),
            _robot->path->rgbGetColor());
        const float vel = atb.dVelocidade, vx = atb.cmd_vx, vy = atb.cmd_vy,
                    vw = atb.cmd_w;

        const char format = 'f';
        QString info = QString("RTS = [%1 | %2 | %3]<br> \
                V = %4 m/s; V<sub>max</sub> = %5 m/s; F<sub>kick</sub> = %6 <br>\
                V<sub>x,y,&omega;</sub> = [%7; %8; %9]; D = [%10; %11]; D<sub>&theta;</sub> = [%12; %13]")
                           .arg(_robot->strGetRole())
                           .arg(_robot->strGetTactic())
                           .arg(_robot->strGetSkill())
                           .arg(vel, 0, format, 2)
                           .arg(_robot->fMaxLinearVelocity(), 0, format, 1)
                           .arg(atb.kickStrength)
                           .arg(vx, 0, format, 2)
                           .arg(vy, 0, format, 2)
                           .arg(vw, 0, format, 2)
                           .arg(atb.vt2dDestino.x(), 0, format, 0)
                           .arg(atb.vt2dDestino.y(), 0, format, 0)
                           .arg(atb.vt2dPontoAnguloDestino.x(), 0, format, 0)
                           .arg(atb.vt2dPontoAnguloDestino.y(), 0, format, 0);

        textsJogada->at(atb.id)->setHtml(info);
    }
}

void RoboFeiSSL::vCarregarParametrosInterface(QFile* arquivoConfiguracoes)
{
    QFile& configuracoes = *arquivoConfiguracoes;

    if (!configuracoes.open(QIODevice::ReadOnly))
    {
        qWarning() << "Não foi possível carregar configurações de "
                   << qPrintable(configuracoes.fileName());
        return;
    }

    config.carregar(&configuracoes, *opOpcoesInterface);

    // Atualiza o valor das variáveis globais
    // iRaioSegurancaBolaStop = ui->spin_ConfigGerais_RefereeCircle->value();
    // iDistanciaRobosAreaDefesa =
    //     ui->spin_ConfigGerais_DistanciaRobosDefesaGolAreaPenalty->value();
    // iRaioSegurancaRobo = ui->spin_ConfigGerais_RaioSegurancaAliado->value();
    // iRaioSegurancaOponente =
    //     ui->spin_ConfigGerais_RaioSegurancaOponente->value();
    // iRaioSegurancaBola = ui->spin_ConfigGerais_RaioSegurancaBola->value();
    vCorTimeMudou(ui->cb_ConfigGerais_CorTime->currentIndex());
    vGoleiroMudou(ui->cb_ConfigGerais_Goleiro->currentIndex());
    dFPS = ui->spin_ConfigGerais_FPS->value();
    // iOffsetCampo = ui->spin_ConfigGerais_OffsetCampo->value();
    // iLarguraDivisaoCampo = ui->spin_ConfigGerais_LarguraDivisaoCampo->value();
    iAnguloGolContra = ui->spin_ConfigGerais_AnguloGolContra->value();
    // iDeslocamentoBola = ui->spin_ConfigGerais_DeslocamentoBola->value();
    // iForcaChuteCalibrado = ui->spin_ConfigGerais_ForcaChute->value();
    iDistanciaReceptorCobranca =
        ui->spin_ConfigGerais_DistanciaReceptorCobranca->value();
    // iLarguraMinimaMira = ui->spin_ConfigGerais_LarguraMira->value();
    // iFramesParaSairDaVisao = ui->spin_ConfigGerais_PacotesSairVisao->value();
    iTamanhoBufferBola = ui->spin_ConfigGerais_TamanhoBufferBola->value();
    // STOP_LINEAR_LIMIT = ui->spin_ConfigGerais_VelocidadeStop->value();
    iTamanhoTracoRobo = ui->spin_ConfigGerais_TamanhoTraco->value();
    // dVelocidadeDelta = ui->spin_ConfigGerais_VelocidadeDelta->value();
    //     Decisoes::dFatorControleAvaliacaoPasse =
    //     ui->dsb_FatorControlePasse->value();
    //     Decisoes::dFatorControleAvaliacaoChute =
    //     ui->dsb_FatorControleChute->value();
    //     Decisoes::dAnguloMinimoFatorRotacaoPasse =
    //     double(ui->sb_FatorAvaliacaoAnguloMinimoPasse->value() );
    //     Decisoes::dAnguloMinimoFatorRotacaoChute =
    //     double(ui->sb_FatorAvaliacaoAnguloMinimoChute->value() );
    //     Decisoes::dAnguloMaximoFatorRotacaoPasse =
    //     double(ui->sb_FatorAvaliacaoAnguloMaximoPasse->value() );
    //     Decisoes::dAnguloMaximoFatorRotacaoChute =
    //     double(ui->sb_FatorAvaliacaoAnguloMaximoChute->value() );
    //     Decisoes::bFatorRotacao = ui->ckb_FatorDeRotacao->isChecked();
    iTempoRecargaChute = ui->spin_TempoRecargaChute->value();
    bGiraComABolaNoRoller = (bool)ui->cb_DribleDoRobo->currentIndex();
    fDistanciaParaChutePuxadinha =
        ui->spin_distanciaParaChutePuxadinha->value();
    fTempoPosicionamentoFreeKick =
        ui->spin_tempoPosicionamentoFreeKick->value();
    fAnguloDeErroChuteNormal = ui->spin_anguloErroChuteNormal->value();
    fAnguloDeErroPasseNormal = ui->spin_anguloErroPasseNormal->value();
    fDistanciaCobradorBolaParada =
        ui->spin_distanciaCobradorBolaParada->value();
    fVelocidadeBolaChegadaPasse =
        ui->spin_ConfigGerais_VelocidadeBolaChegadaPasse->value();
    bUsarKickSensorNaVisao = ui->cbKickSensorInfluenciaVisao->isChecked();
    bUtilizarJogadasEnsaiadasFreeKick =
        ui->cb_utilizarJogadasEnsaiadas->isChecked();
    strModeloForcaChute = ui->cb_ModeloForcaChute->currentText();
    bDeltaInteligenteNormal = ui->cb_TipoDeltaNormal->currentText().compare(
                                  "Inteligente", Qt::CaseInsensitive) == 0;
    //     Estrategia::comportamento =
    //     static_cast<ComportamentoTatico>(ui->cb_ComportamentoTatico->currentIndex());
    // ROBOT_ANGULAR_LIMIT = ui->spin_ConfigGerais_RotacaoMaxima->value();
    //     Estrategia::fDistanciaMaximaParaPasse =
    //     ui->spin_distanciaMaximaPasse->value();
    //     Estrategia::fDistanciaMinimaParaPasse =
    //     ui->spin_distanciaMinimaPasse->value();
    //     Estrategia::jogadaDefensorNormal =
    //     ui->cb_comportamentoDefensorNormal->currentText() == "Delta"
    //         ? Normal_Defensor_Delta : Normal_Defensor_Marcacao;
    //     Estrategia::iNotaNecessariaParaChuteAoGol =
    //     ui->spin_notaChute->value(); Estrategia::iNotaMinimaJogadaPasse =
    //     ui->spin_notaMinimaPasse->value();

    acInterface->allies->vSetSide(
        ui->cb_ConfigGerais_LadoDefesa->currentIndex() == 0 ? XNegativo
                                                            : XPositivo);
}

void RoboFeiSSL::vSalvarConfiguracoes(QString filename, bool ultimaSessao)
{
    QScopedPointer<QFile> fileConfiguracoes(nullptr);

    if (ultimaSessao)
    {
        QDir destDir =
            QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

        if (!destDir.exists())
        {
            QDir().mkpath(destDir.path());
        }

        fileConfiguracoes.reset(
            new QFile(destDir.absoluteFilePath("LastSession.cfg")));
    }
    else
    {
        const QString fileName = QFileDialog::getSaveFileName(
            this, "Salvar Configuracões Interface", filename, "*.cfg", nullptr,
            QFileDialog::DontUseNativeDialog);
        if (!fileName.isEmpty())
        {
            fileConfiguracoes.reset(new QFile(fileName));
        }
    }

    if (fileConfiguracoes && fileConfiguracoes->open(QIODevice::WriteOnly))
    {
        config.salvar(fileConfiguracoes.get(), *opOpcoesInterface);
        fileConfiguracoes->close();
    }
}

void RoboFeiSSL::vCorTimeMudou(int _iNovoIndice)
{
    // Enviar sinal para a thread de jogo que o time mudou
    acInterface->allies->vSetTeamColor(static_cast<TeamColor>(
        ui->cb_ConfigGerais_CorTime->itemData(_iNovoIndice).toInt()));
    if (acInterface->allies->teamColor() == timeAzul)
    {
        acInterface->opponents->vSetTeamColor(timeAmarelo);
    }
    else
    {
        acInterface->opponents->vSetTeamColor(timeAzul);
    }

    emit vEnviarTimeAtual(static_cast<int>(acInterface->allies->teamColor()));
}

void RoboFeiSSL::vLadoDefesaMudou(int _iNovoIndice)
{
    // Enviar sinal para a thread de jogo que o lado de defesa mudou
    int side = qvariant_cast<int>(
        ui->cb_ConfigGerais_LadoDefesa->itemData(_iNovoIndice));
    acInterface->allies->vSetSide(side);
    acInterface->opponents->vSetSide(-side);
}

void RoboFeiSSL::vGoleiroMudou(int _iNovoIndice)
{
    qint8 id = qvariant_cast<qint8>(
        ui->cb_ConfigGerais_Goleiro->itemData(_iNovoIndice));
    referee->vSetGoalie(acInterface->allies->teamColor(), id);
    acInterface->allies->vSetGoalieID(id);
}
void RoboFeiSSL::vTipoAmbienteMudou(int _iNovoIndice)
{
    _iNovoIndice = ui->cb_CampoReal_Grsim->currentIndex();
    acInterface->bTipoAmbiente = (_iNovoIndice);

    if (_iNovoIndice == GR_SIM)
    {
        opOpcoesInterface.get()->vSetEnabledOpcao(
            "Reposição Automática da Bola", true);
        opOpcoesInterface.get()->vSetEnabledOpcao("Reposição com Mouse", true);
    }
    else
    {
        opOpcoesInterface.get()->vSetEnabledOpcao(
            "Reposição Automática da Bola", false);
        opOpcoesInterface.get()->vSetEnabledOpcao("Reposição com Mouse", false);
        opOpcoesInterface.get()->vDesmarcarOpcao(
            "Reposição Automática da Bola");
        opOpcoesInterface.get()->vDesmarcarOpcao("Reposição com Mouse");
    }
}

void RoboFeiSSL::vAtualizaGraficosVelocidades()
{
    iFPSCounter.prepend(qRound(1 / (etmFPSInterface.nsecsElapsed() / 1e9)));
    etmFPSInterface.restart();
    iFPSInterface = Auxiliar::iCalculaFPS(iFPSCounter);

    if (velDialog) // Atualiza graficos das velocidades dos robos e da bola
    {
        emit vEnviaVelocidadeBola(acInterface->dVelocidadeBola());

        for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
        {
            const Robo* r = acInterface->opponents->getCPlayer(i);
            if (r->iID() == velDialog->iIdRobo0() ||
                r->iID() == velDialog->iIdRobo1())
            {
                emit vEnviaVelocidadeRobo(r->dModuloVelocidade(), r->iID());
            }
        }
    }

    if (opOpcoesInterface.get()->bOpcaoAtivada(
            "Reposição Automática da Bola") &&
        globalConfig.controlSimulation)
    {
        if (Auxiliar::bPontoForaCampo(acInterface->geoCampo->szField(),
                                      acInterface->vt2dPosicaoBola()))
        {
            emit vMudarBolaSimulador(QVector2D(0, 0), QVector2D(0, 0));
        }
    }
}

void RoboFeiSSL::vAtualizaDadosInterface()
{
    bDesenharAreaSeguranca =
        opOpcoesInterface.get()->bOpcaoAtivada("Mostrar Área de Segurança");

    acInterface->vt2dPosicaoTimeout.setX(
        ui->le_ConfigGerais_PosicaoTimeoutX->text().toInt());
    acInterface->vt2dPosicaoTimeout.setY(
        ui->le_ConfigGerais_PosicaoTimeoutY->text().toInt());

    vAtualizaOpcoesDesenho();
    vAtualizaDadosRobos();
}

void RoboFeiSSL::vMostraVelocidades()
{
    if (!velDialog)
    {
        velDialog.reset(new Velocidade(this));

        connect(velDialog.get(), &Velocidade::vVelocidadeClose, this,
                &RoboFeiSSL::vFinalizaVelocidades);

        connect(this, &RoboFeiSSL::vEnviaVelocidadeBola, velDialog.get(),
                &Velocidade::vRecebeVelocidadeBola);

        connect(this, &RoboFeiSSL::vEnviaVelocidadeRobo, velDialog.get(),
                &Velocidade::vRecebeVelocidadeRobo);

        velDialog->show();
    }
    else
    {
        velDialog.reset();
    }
}

void RoboFeiSSL::vConfiguraTemaAplicacao(const QString& temaAplicacao)
{
    if (temaAplicacao == "Escuro")
    {

        QPalette darkPalette;
        darkPalette.setColor(QPalette::BrightText, Qt::red);
        darkPalette.setColor(QPalette::WindowText, Qt::white);
        darkPalette.setColor(QPalette::ToolTipBase, QColor(25, 25, 25));
        darkPalette.setColor(QPalette::ToolTipText, Qt::white);
        darkPalette.setColor(QPalette::Text, Qt::white);
        darkPalette.setColor(QPalette::ButtonText, Qt::white);
        darkPalette.setColor(QPalette::HighlightedText, Qt::black);
        darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
        darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));
        darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
        darkPalette.setColorGroup(QPalette::Disabled, Qt::gray,
                                  QColor(100, 100, 100), Qt::gray, Qt::gray,
                                  Qt::gray, Qt::gray, Qt::gray,
                                  QColor(100, 100, 100), Qt::gray);

        qApp->setPalette(darkPalette);
    }
    else if (temaAplicacao == "Claro")
    {

        QPalette lightPalette;
        lightPalette.setColor(QPalette::BrightText, Qt::cyan);
        lightPalette.setColor(QPalette::WindowText, Qt::black);
        lightPalette.setColor(QPalette::ToolTipBase, QColor(240, 240, 240));
        lightPalette.setColor(QPalette::ToolTipText, Qt::black);
        lightPalette.setColor(QPalette::Text, Qt::black);
        lightPalette.setColor(QPalette::ButtonText, Qt::black);
        lightPalette.setColor(QPalette::HighlightedText, Qt::white);
        lightPalette.setColor(QPalette::Window, QColor(200, 200, 200));
        lightPalette.setColor(QPalette::Base, QColor(240, 240, 240));
        lightPalette.setColor(QPalette::AlternateBase, QColor(200, 200, 200));
        lightPalette.setColor(QPalette::Button, QColor(200, 200, 200));
        lightPalette.setColor(QPalette::Link, QColor(213, 125, 37));
        lightPalette.setColor(QPalette::Highlight, QColor(213, 225, 37));
        lightPalette.setColorGroup(QPalette::Disabled, Qt::gray,
                                   QColor(100, 100, 100), Qt::gray, Qt::gray,
                                   Qt::gray, Qt::gray, Qt::gray,
                                   QColor(100, 100, 100), Qt::gray);

        qApp->setPalette(lightPalette);
    }
    else if (temaAplicacao == "Azul")
    {

        QPalette customPalette;
        customPalette.setColor(QPalette::BrightText, QColor(55, 90, 55));
        customPalette.setColor(QPalette::WindowText, Qt::yellow);
        customPalette.setColor(QPalette::ToolTipBase, QColor(0, 0, 50));
        customPalette.setColor(QPalette::ToolTipText, Qt::yellow);
        customPalette.setColor(QPalette::Text, Qt::yellow);
        customPalette.setColor(QPalette::ButtonText, Qt::yellow);
        customPalette.setColor(QPalette::HighlightedText, QColor(0, 0, 63));
        customPalette.setColor(QPalette::Window, QColor(4, 4, 100));
        customPalette.setColor(QPalette::Base, QColor(0, 0, 50));
        customPalette.setColor(QPalette::AlternateBase, QColor(4, 4, 100));
        customPalette.setColor(QPalette::Button, QColor(4, 4, 100));
        customPalette.setColor(QPalette::Link, Qt::darkYellow);
        customPalette.setColor(QPalette::Highlight, Qt::darkYellow);
        customPalette.setColorGroup(
            QPalette::Disabled, Qt::gray, QColor(0, 0, 120), Qt::gray, Qt::gray,
            Qt::gray, Qt::gray, Qt::gray, QColor(0, 0, 120), Qt::gray);

        qApp->setPalette(customPalette);
    }
}

void RoboFeiSSL::on_actionPrintCampo_triggered()
{
    if (dmDesenhaCampo)
    {
        dmDesenhaCampo.get()->vPrint();
    }
}

void RoboFeiSSL::vAtualizaMatrizJogadas()
{
    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        for (int j = 0; j < iJogadasAtaque; ++j)
        {
            QTableWidgetItem* tbwiAux = new QTableWidgetItem(true);

            tbwiAux = ui->tbw_ConfigJogadas->item(j, i);
            acInterface->bMatrizJogadas[i][j] = tbwiAux->data(0).toBool();
        }
    }

    vSalvaMatrizJogadas();
}

void RoboFeiSSL::vCelulaAlterada(int nRow, int nCol)
{
    QTableWidgetItem* tbwiAux = new QTableWidgetItem(false);

    tbwiAux = ui->tbw_ConfigJogadas->item(nRow, nCol);

    if (tbwiAux->data(0).toBool() == false)
    {
        ui->tbw_ConfigJogadas->item(nRow, nCol)->setBackground(Qt::green);
        ui->tbw_ConfigJogadas->item(nRow, nCol)->setData(0, true);
    }
    else
    {
        ui->tbw_ConfigJogadas->item(nRow, nCol)->setBackground(Qt::red);
        ui->tbw_ConfigJogadas->item(nRow, nCol)->setData(0, false);
    }
}

void RoboFeiSSL::vResetaMatriz()
{
    for (int i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        for (int j = 0; j < iJogadasAtaque; ++j)
        {
            ui->tbw_ConfigJogadas->item(j, i)->setBackground(Qt::green);
            ui->tbw_ConfigJogadas->item(j, i)->setData(0, true);

            acInterface->bMatrizJogadas[i][j] = true;
        }
    }

    vSalvaMatrizJogadas();
}

void RoboFeiSSL::vMudaCorRobo(const int& id)
{
    if (id != -1)
    {
        const QColor currentColor =
            acInterface->allies->getPlayer(id)->path->rgbGetColor();
        const QColor newColor = QColorDialog::getColor(
            currentColor, this,
            "Defina uma cor para o Robô " + QString::number(id),
            QColorDialog::DontUseNativeDialog);
        if (newColor.isValid())
        {
            acInterface->allies->getPlayer(id)->path->vSetColor(newColor);
        }
    }
}

void RoboFeiSSL::vAtualizaDadosRobos()
{
    for (qint8 id = 0; id < globalConfig.robotsPerTeam; ++id)
    {
        vAtualizaStatusRobo(acInterface->allies->getCPlayer(id));
    }

    int iRoboID = ui->sb_Movimentacao_IDRobo->value();
    Robo* robo = acInterface->allies->getPlayer(iRoboID);

    if (ui->rb_Movimentacao_On->isChecked())
    {
        double dTempo = etmTempoGraficoVelocidades.nsecsElapsed() / 1e9;

        if (dTempo > 10)
        {
            dTempo = 0;
            etmTempoGraficoVelocidades.restart();

            ui->qcp_Movimentacao_Grafico->graph(0)->setData(QVector<double>(),
                                                            QVector<double>());
        }

        ui->qcp_Movimentacao_Grafico->graph(0)->addData(
            dTempo, robo->dModuloVelocidade());
        ui->qcp_Movimentacao_Grafico->replot();

        vt2dTracoRobo.prepend(robo->vt2dPosicaoAtual());
        if (vt2dTracoRobo.size() > iTamanhoTracoRobo)
        {
            vt2dTracoRobo.pop_back();
        }
        dmDesenhaCampo->setRobotTrajectory(true, vt2dTracoRobo);
    }
    else
    {
        dmDesenhaCampo->setRobotTrajectory(false, {});
    }
}

void RoboFeiSSL::vAtualizaPortas()
{
    ui->cb_Radio_Portas->clear();

    QList<QSerialPortInfo> portasDisponiveis =
        QSerialPortInfo::availablePorts();

    for (const QSerialPortInfo& info : qAsConst(portasDisponiveis))
    {
        QStringList list;
        list << info.portName();
        ui->cb_Radio_Portas->addItem(list.first(), list);
    }
}

void RoboFeiSSL::vRecebeDadosMovimentacao(const MotionFeedbackPacket& _packet)
{
    // Atualizacao dos dados dos robos na interface
    if (bFechar == true)
        return;

    acInterface->vSetRemainingPlayTime(_packet.remainingPlayTime);
    acInterface->debugInfo->merge(*_packet.debugInfo);

    for (int n = 0; n < globalConfig.robotsPerTeam; ++n)
    {
        // Atualizar aqui as variaveis alteradas pela estrategia, por exemplo,
        // os caminhos do path planner
        const Robo* roboAliado = _packet.allies->getCPlayer(n);
        // Atributos atbRobo(_packet.allies->getPlayer(n)->atbRetornaRobo());

        if (roboAliado->bRoboDetectado())
        {
            acInterface->allies->getPlayer(n)
                ->vAtualizaDadosEstrategiaMovimentacao(roboAliado);
            acInterface->opponents->getPlayer(n)
                ->vAtualizaDadosEstrategiaMovimentacao(
                    _packet.opponents->getPlayer(n));
        }
    }
}

void RoboFeiSSL::vComecarJogo()
{
    if (!threadVisao->isRunning())
    {
        return;
    }
    if (bRodando)
    {
        vPararJogo();
        return;
    }

    bRodando = true;

    if (bIniciaMovimentacao() == true)
    {
        //         Decisoes::strModeloChute = ui->cb_ModeloChute->currentText();
        //         Decisoes::strModeloPasse = ui->cb_ModeloPasse->currentText();

        vIniciaEstrategia();

        // Emit dados interface -> estrategia

        emit vAtivarThread(bRodando);
        emit vEnviaDadosInterface(
            acInterface->vt2dPosicaoTimeout,
            acInterface->allies->iGetGoalieID(),
            static_cast<int>(acInterface->allies->teamColor()),
            acInterface->allies->iGetSide(), acInterface->bTipoAmbiente);

        ui->gb_ConfiguracoesGerais->setEnabled(false);
        ui->pb_ComecarJogo->disconnect(SIGNAL(pressed()));
        connect(ui->pb_ComecarJogo, &QPushButton::pressed, this,
                &RoboFeiSSL::vPararJogo);
        ui->pb_ComecarJogo->setText(QStringLiteral("Parar"));

        QPixmap pmLed;
        pmLed.load(":/Imagens/Leds/circle_green.svg", "svg");
        pmLed = pmLed.scaled(25, 25);
        ui->lb_LED_Jogo->setPixmap(pmLed);
        pmLed.load(":/Imagens/Leds/circle_red.svg", "svg");
        pmLed = pmLed.scaled(25, 25);
        ui->lb_LED_Teste->setPixmap(pmLed);
        ui->pb_ComecarJogo->setMinimumSize(QSize(97, 38));

        ui->pb_ComecarTestes->setEnabled(false);
    }
    else
    {
        bRodando = false;
    }
}

void RoboFeiSSL::vPararJogo()
{
    bRodando = false;
    // Sinaliza para a estratégia parar
    emit vAtivarThread(bRodando);
    QThread::currentThread()->msleep(300);

    threadMovimentacao->quit();
    if (!threadMovimentacao->wait(1000))
        qFatal("Thread Movimentação não finalizou em 1000ms");
    moveMovimentacaoRobo.reset(); // Importante para não haver memory leak

    // Finaliza a thread
    threadEstrategia->quit();
    if (!threadEstrategia->wait(10000))
    {
        qFatal("Thread Estratégia não finalizou em 10000ms");
    }
    estEstrategia.reset(); // Importante para não haver memory leak
    //     Decisoes::releaseInstance();

    ConfiguracaoRadio radioConfig;
    radioConfig.bConectar = false;
    emit sendRadioConfig(radioConfig);

    ui->gb_ConfiguracoesGerais->setEnabled(true);
    disconnect(ui->pb_ComecarJogo, SIGNAL(pressed()));
    connect(ui->pb_ComecarJogo, &QPushButton::pressed, this,
            &RoboFeiSSL::vComecarJogo);
    ui->pb_ComecarJogo->setText(QStringLiteral("Jogar"));

    QPixmap pmLed;
    pmLed.load(":/Imagens/Leds/circle_red.svg", "svg");
    pmLed = pmLed.scaled(25, 25);
    ui->lb_LED_Jogo->setPixmap(pmLed);
    ui->pb_ComecarJogo->setMinimumSize(QSize(97, 38));

    ui->pb_ComecarTestes->setEnabled(true);
}

void RoboFeiSSL::vComecarTestes()
{
    bRodando = true;

    if (bIniciaMovimentacao() == true)
    {
        vIniciaTestes();

        // Emit dados interface -> estrategia

        emit vAtivarThread(bRodando);
        emit vEnviaDadosInterface(
            acInterface->vt2dPosicaoTimeout,
            acInterface->allies->iGetGoalieID(),
            static_cast<int>(acInterface->allies->teamColor()),
            acInterface->allies->iGetSide(), acInterface->bTipoAmbiente);

        emit vEnviaDadosTestes(acInterface->tsDadosTeste);

        ui->gb_ConfiguracoesGerais->setEnabled(false);
        ui->pb_ComecarTestes->disconnect(SIGNAL(pressed()));
        connect(ui->pb_ComecarTestes, &QPushButton::pressed, this,
                &RoboFeiSSL::vPararTestes);
        ui->pb_ComecarTestes->setText(QStringLiteral("Parar"));

        QPixmap pmLed;
        pmLed.load(":/Imagens/Leds/circle_red.svg", "svg");
        pmLed = pmLed.scaled(25, 25);
        ui->lb_LED_Jogo->setPixmap(pmLed);
        pmLed.load(":/Imagens/Leds/circle_green.svg", "svg");
        pmLed = pmLed.scaled(25, 25);
        ui->lb_LED_Teste->setPixmap(pmLed);
        ui->pb_ComecarTestes->setMinimumSize(QSize(97, 38));

        ui->pb_ComecarJogo->setEnabled(false);
    }
    else
        bRodando = false;
}

void RoboFeiSSL::vPararTestes()
{
    bRodando = false;
    emit vAtivarThread(bRodando);

    disconnect(trTestesRobos.get(),
               &TestesRobos::vEnviaTrajetosTestePathPlanner, nullptr, nullptr);

    threadTestes->quit();
    threadTestes->wait();
    trTestesRobos.reset(); // Importante para não haver memory leak

    threadMovimentacao->quit();

    threadMovimentacao->wait();
    moveMovimentacaoRobo.reset(); // Importante para não haver memory leak

    ui->gb_ConfiguracoesGerais->setEnabled(true);
    disconnect(ui->pb_ComecarTestes, SIGNAL(pressed()), nullptr, nullptr);
    connect(ui->pb_ComecarTestes, &QPushButton::pressed, this,
            &RoboFeiSSL::vComecarTestes);
    ui->pb_ComecarTestes->setText(QStringLiteral("Testar"));

    QPixmap pmLed;
    pmLed.load(":/Imagens/Leds/circle_red.svg", "svg");
    pmLed = pmLed.scaled(25, 25);
    ui->lb_LED_Teste->setPixmap(pmLed);
    ui->pb_ComecarTestes->setMinimumSize(QSize(97, 38));

    ui->pb_ComecarJogo->setEnabled(true);
}

void RoboFeiSSL::vRecebeComandosSimuladorReferee(SSL_Referee_Command comando,
                                                 QVector2D posBallPlacement)
{
    ui->le_EstadoJogoAtual->setText("Simulando o Refbox");
    acInterface->opponents->vSetGoalieID(0);
    referee->vSimulateReferee(comando, posBallPlacement);

    if (ui->tb_ComandosRecebidosReferee->toPlainText().size() > 2000)
        ui->tb_ComandosRecebidosReferee->clear();
    ui->tb_ComandosRecebidosReferee->append(referee->strGetCommand());
}

void RoboFeiSSL::vInicializaShortcutsAdicionais()
{
    connect(new QShortcut(QKeySequence(Qt::ALT + Qt::Key_1), this),
            &QShortcut::activated, this, &RoboFeiSSL::vMudaParaAba1);

    connect(new QShortcut(QKeySequence(Qt::ALT + Qt::Key_2), this),
            &QShortcut::activated, this, &RoboFeiSSL::vMudaParaAba2);

    connect(new QShortcut(QKeySequence(Qt::ALT + Qt::Key_3), this),
            &QShortcut::activated, this, &RoboFeiSSL::vMudaParaAba3);

    connect(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Tab), this),
            &QShortcut::activated, this, &RoboFeiSSL::vSwitchTab);

    connect(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q), this),
            &QShortcut::activated, this, &RoboFeiSSL::close);

    connect(new QShortcut(QKeySequence(Qt::ALT + Qt::Key_V), this),
            &QShortcut::activated, this, &RoboFeiSSL::vConectarRede);

    connect(new QShortcut(QKeySequence(Qt::ALT + Qt::Key_J), this),
            &QShortcut::activated, this, &RoboFeiSSL::vComecarJogo);
}

void RoboFeiSSL::on_actionSalvarConfiguracoes_triggered()
{
    vSalvarConfiguracoes("ConfiguracoesInterface.cfg");
}

void RoboFeiSSL::on_actionCarregarConfiguracoes_triggered()
{
    vCarregarConfiguracoes(false);
}

void RoboFeiSSL::vCarregarConfiguracoes(const bool arquivoPadrao)
{
    QScopedPointer<QFile> fileConfiguracoes(nullptr);

    if (arquivoPadrao)
    {
        QDir destDir =
            QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

        fileConfiguracoes.reset(
            new QFile(destDir.absoluteFilePath("LastSession.cfg")));

        qInfo() << qPrintable(destDir.path());
        qInfo() << qPrintable(fileConfiguracoes->fileName());
    }
    else
    {
        // Criar on FileDialog para selecionar o arquivo de configuração
        QString filename = QFileDialog::getOpenFileName(
            this, "Abrir Arquivo de Configuração", "ConfiguracoesInterface.cfg",
            "*.cfg", nullptr, QFileDialog::DontUseNativeDialog);

        if (filename.isEmpty())
            return;

        fileConfiguracoes.reset(new QFile(filename));
    }

    if (fileConfiguracoes && fileConfiguracoes->exists())
    {
        qInfo() << "Carregando arquivo de configuração de "
                << qPrintable(fileConfiguracoes->fileName());
        vCarregarParametrosInterface(fileConfiguracoes.take());
        vConfiguraTemaAplicacao(ui->cb_TemaInterface->currentText());
    }
    else
    {
        qWarning() << "Não foi possível carregar arquivo de configuração da "
                      "última sessão";
    }
}

void RoboFeiSSL::on_actionMute_triggered()
{
    if (bMutar == false)
    {
        bMutar = true;
        player->stop();
        ui->actionMute->setText("Unmute (M)");
    }
    else
    {
        bMutar = false;
        ui->actionMute->setText("Mute (M)");
    }
}

void RoboFeiSSL::on_actionAbrir_Refbox_triggered()
{
    if (!simRefBox)
    {
        if (!acInterface->geoCampo->szField().isEmpty())
        {
            simRefBox.reset(
                new SimuladorRefbox(acInterface->geoCampo->szField()));

            connect(simRefBox.get(), &SimuladorRefbox::vEnviaComando, this,
                    &RoboFeiSSL::vRecebeComandosSimuladorReferee);

            connect(simRefBox.get(), &SimuladorRefbox::vRefClose, this,
                    [this] { this->simRefBox.reset(); });

            simRefBox->show();
        }
        else
        {
            QMessageBox message;
            message.setText(
                "Ainda não possui dados da visão para iniciar o refbox!");
            message.exec();
        }
    }

    else
    {
        simRefBox->close();
        simRefBox.reset();
    }
}

void RoboFeiSSL::on_spin_ConfigGerais_FPS_editingFinished()
{
    dFPS = static_cast<double>(ui->spin_ConfigGerais_FPS->value());
}

void RoboFeiSSL::on_spin_ConfigGerais_PacotesSairVisao_editingFinished()
{
    // iFramesParaSairDaVisao = ui->spin_ConfigGerais_PacotesSairVisao->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_RaioSegurancaBola_editingFinished()
{
    //    iRaioDeSegurancaBolaNotNormal =
    //    ui->spin_ConfigGerais_RaioSegurancaBola->value();
    // iRaioSegurancaBola = ui->spin_ConfigGerais_RaioSegurancaBola->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_RaioSegurancaOponente_editingFinished()
{
    // iRaioSegurancaOponente =
    //     ui->spin_ConfigGerais_RaioSegurancaOponente->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_RaioSegurancaAliado_editingFinished()
{
    // iRaioSegurancaRobo = ui->spin_ConfigGerais_RaioSegurancaAliado->value();
    //    fDistanciaSeguranca = iRaioSegurancaRobo*1.2f;
}

void RoboFeiSSL::on_spin_ConfigGerais_LarguraDivisaoCampo_editingFinished()
{
    // iLarguraDivisaoCampo = ui->spin_ConfigGerais_LarguraDivisaoCampo->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_RefereeCircle_editingFinished()
{
    // iRaioSegurancaBolaStop = ui->spin_ConfigGerais_RefereeCircle->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_DeslocamentoBola_editingFinished()
{
    // iDeslocamentoBola = ui->spin_ConfigGerais_DeslocamentoBola->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_LarguraMira_editingFinished()
{
    // iLarguraMinimaMira = ui->spin_ConfigGerais_LarguraMira->value();
}

void RoboFeiSSL::
    on_spin_ConfigGerais_DistanciaRobosDefesaGolAreaPenalty_editingFinished()
{
    // iDistanciaRobosAreaDefesa =
    //     ui->spin_ConfigGerais_DistanciaRobosDefesaGolAreaPenalty->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_ForcaChute_editingFinished()
{
    // iForcaChuteCalibrado = ui->spin_ConfigGerais_ForcaChute->value();
}

void RoboFeiSSL::
    on_spin_ConfigGerais_DistanciaReceptorCobranca_editingFinished()
{
    iDistanciaReceptorCobranca =
        ui->spin_ConfigGerais_DistanciaReceptorCobranca->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_VelocidadeStop_editingFinished()
{
    // STOP_LINEAR_LIMIT = ui->spin_ConfigGerais_VelocidadeStop->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_TamanhoBufferBola_editingFinished()
{
    iTamanhoBufferBola = ui->spin_ConfigGerais_TamanhoBufferBola->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_TamanhoTraco_editingFinished()
{
    iTamanhoTracoRobo = ui->spin_ConfigGerais_TamanhoTraco->value();
    vt2dTracoRobo.clear();
}

void RoboFeiSSL::on_spin_ConfigGerais_VelocidadeDelta_editingFinished()
{
    // dVelocidadeDelta = ui->spin_ConfigGerais_VelocidadeDelta->value();
}

void RoboFeiSSL::on_spin_ConfigGerais_AnguloGolContra_editingFinished()
{
    iAnguloGolContra = ui->spin_ConfigGerais_AnguloGolContra->value();
}

void RoboFeiSSL::on_cb_ConfigGerais_TesteMeioCampo_currentIndexChanged(
    int index)
{
    emit vEnviarTesteMeioCampo(qvariant_cast<int>(
        ui->cb_ConfigGerais_TesteMeioCampo->itemData(index)));
}

void RoboFeiSSL::on_spin_ConfigGerais_OffsetCampo_editingFinished()
{
    // iOffsetCampo = ui->spin_ConfigGerais_OffsetCampo->value();
}

void RoboFeiSSL::vMudaParaAba1()
{
    ui->tabAbas->setCurrentIndex(0);
}

void RoboFeiSSL::vMudaParaAba2()
{
    ui->tabAbas->setCurrentIndex(1);
}

void RoboFeiSSL::vMudaParaAba3()
{
    ui->tabAbas->setCurrentIndex(2);
}

void RoboFeiSSL::vSwitchTab()
{
    quint8 tabAtual = ui->tabAbas->currentIndex();

    if (!(tabAtual == 3))
    {

        ui->tabAbas->setCurrentIndex(tabAtual + 1);
    }
    else
    {

        ui->tabAbas->setCurrentIndex(0);
    }
}

void RoboFeiSSL::vControlaSimulacaoMouseClicked(const QVector2D position,
                                                const Qt::MouseButton button)
{
    const QString campoReal_GrSim = ui->cb_CampoReal_Grsim->currentText();

    if (campoReal_GrSim != QLatin1String("GrSim") ||
        !opOpcoesInterface.get()->bOpcaoAtivada("Reposição com Mouse") ||
        !globalConfig.controlSimulation)
        return;

    // reinicia a instância (garantia)
    controleSimulacao = ControleSimulacao();
    controleSimulacao.mouseButton = button;

    // Acha robô aliado com a bola ou adversário com a bola
    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        if (acInterface->allies->getPlayer(i)->bRoboDetectado() &&
            acInterface->allies->getPlayer(i)
                    ->vt2dPosicaoAtual()
                    .distanceToPoint(position) < globalConfig.robotDiameter / 2.0)
        {
            controleSimulacao.tipoObjeto =
                acInterface->allies->teamColor() == timeAmarelo
                    ? ControleSimulacao::ObjetoRoboAmarelo
                    : ControleSimulacao::ObjetoRoboAzul;
            controleSimulacao.idAtual = i;

            break;
        }

        if (acInterface->opponents->getPlayer(i)->bRoboDetectado() &&
            acInterface->opponents->getPlayer(i)
                    ->vt2dPosicaoAtual()
                    .distanceToPoint(position) < globalConfig.robotDiameter / 2.0)
        {
            controleSimulacao.tipoObjeto =
                acInterface->opponents->teamColor() == timeAmarelo
                    ? ControleSimulacao::ObjetoRoboAmarelo
                    : ControleSimulacao::ObjetoRoboAzul;
            controleSimulacao.idAtual = i;

            break;
        }
    }

    if (controleSimulacao.tipoObjeto == ControleSimulacao::ObjetoNenhum)
    {
        controleSimulacao.tipoObjeto = ControleSimulacao::ObjetoBola;
        if (controleSimulacao.mouseButton == Qt::LeftButton)
        {
            emit vMudarBolaSimulador(position * 1e-3, QVector2D(0, 0));
        }
    }
    else
    {
        if (controleSimulacao.mouseButton == Qt::LeftButton)
        {
            emit SIGNAL_mudarPosicaoRoboSimulador(
                controleSimulacao.tipoObjeto ==
                    ControleSimulacao::ObjetoRoboAmarelo,
                position * 1e-3, controleSimulacao.idAtual);
        }
    }
}

void RoboFeiSSL::vControlaSimulacaoMouseReleased(const QVector2D position,
                                                 const Qt::MouseButton button)
{
    const QString campoReal_GrSim = ui->cb_CampoReal_Grsim->currentText();

    if (campoReal_GrSim != QLatin1String("GrSim") ||
        !opOpcoesInterface.get()->bOpcaoAtivada("Reposição com Mouse") ||
        !globalConfig.controlSimulation)
        return;

    if (controleSimulacao.tipoObjeto == ControleSimulacao::ObjetoBola)
    {
        // Muda posição e mantém a velocidade atual medida
        if (controleSimulacao.mouseButton == Qt::LeftButton)
        {
            emit vMudarBolaSimulador(position * 1e-3, {0, 0});
        }
        // muda velocidade
        else if (controleSimulacao.mouseButton == Qt::RightButton)
        {
            const QVector2D vt2dVelocidade =
                (position -
                 dmDesenhaCampo.get()->vt2dPosicaoInicialCliqueDireito()) /
                500.f;

            emit vMudarBolaSimulador(acInterface->vt2dPosicaoBola() * 1e-3,
                                     vt2dVelocidade);
        }
    }
    else if (controleSimulacao.tipoObjeto ==
             ControleSimulacao::ObjetoRoboAmarelo)
    {
        // muda posição
        if (controleSimulacao.mouseButton == Qt::LeftButton)
        {
            emit SIGNAL_mudarPosicaoRoboSimulador(true, position * 1e-3,
                                                  controleSimulacao.idAtual);
        }
        // muda ângulo
        else if (controleSimulacao.mouseButton == Qt::RightButton)
        {
            const QVector2D posAtual =
                acInterface->allies->teamColor() == timeAmarelo
                    ? acInterface->allies->getPlayer(controleSimulacao.idAtual)
                          ->vt2dPosicaoAtual()
                    : acInterface->opponents
                          ->getPlayer(controleSimulacao.idAtual)
                          ->vt2dPosicaoAtual();

            const QVector2D deslocamento = position - posAtual;

            const float angulo = atan2f(deslocamento.y(), deslocamento.x());
            emit SIGNAL_mudarAnguloRoboSimulacao(true, angulo,
                                                 controleSimulacao.idAtual);
        }
    }
    else if (controleSimulacao.tipoObjeto == ControleSimulacao::ObjetoRoboAzul)
    {
        // muda posição
        if (controleSimulacao.mouseButton == Qt::LeftButton)
        {
            emit SIGNAL_mudarPosicaoRoboSimulador(false, position * 1e-3,
                                                  controleSimulacao.idAtual);
        }
        // muda ângulo
        else if (controleSimulacao.mouseButton == Qt::RightButton)
        {
            const QVector2D posAtual =
                acInterface->allies->teamColor() == timeAzul
                    ? acInterface->allies->getPlayer(controleSimulacao.idAtual)
                          ->vt2dPosicaoAtual()
                    : acInterface->opponents
                          ->getPlayer(controleSimulacao.idAtual)
                          ->vt2dPosicaoAtual();

            const QVector2D deslocamento = position - posAtual;

            const float angulo = atan2f(deslocamento.y(), deslocamento.x());
            emit SIGNAL_mudarAnguloRoboSimulacao(false, angulo,
                                                 controleSimulacao.idAtual);
        }
    }

    // Reinicia a instância
    controleSimulacao = ControleSimulacao();
}

void RoboFeiSSL::vControlaSimulacaoMouseMoved(const QVector2D position)
{
    const QString campoReal_GrSim = ui->cb_CampoReal_Grsim->currentText();

    if (campoReal_GrSim != QLatin1String("GrSim") ||
        !opOpcoesInterface.get()->bOpcaoAtivada("Reposição com Mouse") ||
        !globalConfig.controlSimulation)
        return;

    if (controleSimulacao.tipoObjeto == ControleSimulacao::ObjetoBola)
    {
        if (controleSimulacao.mouseButton == Qt::LeftButton)
        {
            emit vMudarBolaSimulador(position * 1e-3, QVector2D(0, 0));
        }
    }
    else if (controleSimulacao.tipoObjeto ==
             ControleSimulacao::ObjetoRoboAmarelo)
    {
        // muda posição
        if (controleSimulacao.mouseButton == Qt::LeftButton)
        {
            emit SIGNAL_mudarPosicaoRoboSimulador(true, position * 1e-3,
                                                  controleSimulacao.idAtual);
        }
        // muda ângulo
        else if (controleSimulacao.mouseButton == Qt::RightButton)
        {
            const QVector2D posAtual =
                acInterface->allies->teamColor() == timeAmarelo
                    ? acInterface->allies->getPlayer(controleSimulacao.idAtual)
                          ->vt2dPosicaoAtual()
                    : acInterface->opponents
                          ->getPlayer(controleSimulacao.idAtual)
                          ->vt2dPosicaoAtual();

            const QVector2D deslocamento = position - posAtual;

            const float angulo = atan2f(deslocamento.y(), deslocamento.x());
            emit SIGNAL_mudarAnguloRoboSimulacao(true, angulo,
                                                 controleSimulacao.idAtual);
        }
    }
    else if (controleSimulacao.tipoObjeto == ControleSimulacao::ObjetoRoboAzul)
    {
        // muda posição
        if (controleSimulacao.mouseButton == Qt::LeftButton)
        {
            emit SIGNAL_mudarPosicaoRoboSimulador(false, position * 1e-3,
                                                  controleSimulacao.idAtual);
        }
        // muda ângulo
        else if (controleSimulacao.mouseButton == Qt::RightButton)
        {
            const QVector2D posAtual =
                acInterface->allies->teamColor() == timeAzul
                    ? acInterface->allies->getPlayer(controleSimulacao.idAtual)
                          ->vt2dPosicaoAtual()
                    : acInterface->opponents
                          ->getPlayer(controleSimulacao.idAtual)
                          ->vt2dPosicaoAtual();

            const QVector2D deslocamento = position - posAtual;

            const float angulo = atan2f(deslocamento.y(), deslocamento.x());
            emit SIGNAL_mudarAnguloRoboSimulacao(false, angulo,
                                                 controleSimulacao.idAtual);
        }
    }
}

void RoboFeiSSL::vFinalizaVelocidades()
{
    //    opOpcoesInterface.get()->vDesmarcarOpcao("Velocidade");
    velDialog.reset();
}

void RoboFeiSSL::vRecebeOpcaoInterface(const QString& opcao,
                                       const bool& estadoAtual)
{
    if (opcao == "Anti-Aliasing" && dmDesenhaCampo)
        dmDesenhaCampo.get()->vSetAntiAliasing(estadoAtual);
}

void RoboFeiSSL::on_actionOpcoes_triggered()
{
    if (!opOpcoesInterface.get()->isVisible())
        opOpcoesInterface.get()->show();
    else
        opOpcoesInterface.get()->hide();
}

void RoboFeiSSL::on_actionMovimentacaoClick_triggered()
{
    if (!mcTesteMovimentacaoClick.get()->isVisible())
        mcTesteMovimentacaoClick.get()->show();
    else
        mcTesteMovimentacaoClick.get()->close();
}

void RoboFeiSSL::on_actionPathPlanners_triggered()
{
    if (tppTestePathPlanners)
    {
        if (tppTestePathPlanners->isVisible())
            tppTestePathPlanners->close();
        else
            tppTestePathPlanners->show();
    }
}

void RoboFeiSSL::on_dsb_FatorControlePasse_valueChanged(double arg1)
{
    //     Decisoes::dFatorControleAvaliacaoPasse = arg1;
    qWarning()
        << "on_dsb_FatorControlePasse_valueChanged :: Valor sendo ignorado!"
        << arg1;
}

void RoboFeiSSL::on_dsb_FatorControleChute_valueChanged(double arg1)
{
    //     Decisoes::dFatorControleAvaliacaoChute = arg1;
    qWarning()
        << "on_dsb_FatorControleChute_valueChanged :: Valor sendo ignorado!"
        << arg1;
}

void RoboFeiSSL::on_sb_FatorAvaliacaoAnguloMinimoPasse_valueChanged(int arg1)
{
    //     Decisoes::dAnguloMinimoFatorRotacaoPasse = double(arg1);
    qWarning() << "on_sb_FatorAvaliacaoAnguloMinimoPasse_valueChanged :: Valor "
                  "sendo ignorado!"
               << arg1;
}

void RoboFeiSSL::on_sb_FatorAvaliacaoAnguloMinimoChute_valueChanged(int arg1)
{
    //     Decisoes::dAnguloMinimoFatorRotacaoChute = double(arg1);
    qWarning() << "on_sb_FatorAvaliacaoAnguloMinimoChute_valueChanged :: Valor "
                  "sendo ignorado!"
               << arg1;
}

void RoboFeiSSL::on_sb_FatorAvaliacaoAnguloMaximoPasse_valueChanged(int arg1)
{
    //     Decisoes::dAnguloMaximoFatorRotacaoPasse = double(arg1);
    qWarning() << "on_sb_FatorAvaliacaoAnguloMaximoPasse_valueChanged :: Valor "
                  "sendo ignorado!"
               << arg1;
}

void RoboFeiSSL::on_sb_FatorAvaliacaoAnguloMaximoChute_valueChanged(int arg1)
{
    //     Decisoes::dAnguloMaximoFatorRotacaoChute = double(arg1);
    qWarning() << "on_sb_FatorAvaliacaoAnguloMaximoChute_valueChanged :: Valor "
                  "sendo ignorado!"
               << arg1;
}
void RoboFeiSSL::on_ckb_FatorDeRotacao_released()
{
    //     Decisoes::bFatorRotacao = ui->ckb_FatorDeRotacao->isChecked();
}

void RoboFeiSSL::on_actionVirtualJoystick_triggered()
{
    joystickRobotControllerInterface.reset(
        new JoystickRobotControllerInterface());
    joystickRobotControllerInterface->show();

    connect(joystickRobotControllerInterface.get(),
            &JoystickRobotControllerInterface::vCloseVirtualJoystickInterface,
            this, [this] { this->joystickRobotControllerInterface.reset(); });

    if (trTestesRobos)
    {
        connect(joystickRobotControllerInterface.get(),
                &JoystickRobotControllerInterface::SIGNAL_enviaDadosRobo,
                trTestesRobos.get(),
                &TestesRobos::vRecebeDadosRoboVirtualJoystick);
    }
}

void RoboFeiSSL::on_actionDecisoes_triggered()
{
    //     qDebug() << Q_FUNC_INFO;
    //
    //     Decisoes::strModeloChute = ui->cb_ModeloChute->currentText();
    //     Decisoes::strModeloPasse = ui->cb_ModeloPasse->currentText();
    //
    //     decisoesTeste.reset( new DecisoesTesteInterface() );
    //     decisoesTeste->show();
    //
    //     connect(decisoesTeste.get(), &DecisoesTesteInterface::close,
    //             this, [this]{this->decisoesTeste.reset();});
    //
    //     if(trTestesRobos)
    //     {
    //         connect(decisoesTeste.get(),
    //         &DecisoesTesteInterface::SIGNAL_solicitaTesteDecisoesPasse,
    //                 trTestesRobos.get(),
    //                 &TestesRobos::SLOT_recebeSolicitacaoTesteDecisaoPasse);
    //
    //         connect(decisoesTeste.get(),
    //         &DecisoesTesteInterface::SIGNAL_solicitaTesteDecisoesChute,
    //                 trTestesRobos.get(),
    //                 &TestesRobos::SLOT_recebeSolicitacaoTesteDecisaoChute);
    //
    //         connect(trTestesRobos.get(), &TestesRobos::SIGNAL_decisoesPasse,
    //                 decisoesTeste.get(),
    //                 &DecisoesTesteInterface::SLOT_decisoesPasse);
    //
    //         connect(trTestesRobos.get(), &TestesRobos::SIGNAL_decisoesChute,
    //                 decisoesTeste.get(),
    //                 &DecisoesTesteInterface::SLOT_decisoesChute);
    //
    //
    //     }
    //
}

void RoboFeiSSL::on_cb_DribleDoRobo_currentIndexChanged(int index)
{
    bGiraComABolaNoRoller = (bool)index;
}

void RoboFeiSSL::on_spin_TempoRecargaChute_valueChanged(int arg1)
{
    iTempoRecargaChute = arg1;
}

void RoboFeiSSL::on_actionMensagens_triggered()
{
    if (QtMessageFilter::isDialogVisible())
        QtMessageFilter::hideDialog();
    else
        QtMessageFilter::showDialog();
}

void RoboFeiSSL::SLOT_mudarEstadoRede(const bool ativar)
{
    if (ativar)
    {
        if (!visVisao)
            vConectarRede();
    }
    else if (visVisao)
        vDesconectarRede();
}

void RoboFeiSSL::SLOT_mudarEstadoEstrategia(const bool ativar)
{
    if (ativar)
    {
        if (!visVisao)
        {
            qWarning() << "Por favor habilite a rede antes de habilitar a "
                          "estratégia.\n"
                          "O faça com o seguinte comando: "
                          "set network on";
        }
        else if (!estEstrategia)
            vComecarJogo();
    }
    else if (estEstrategia)
        vPararJogo();
}

void RoboFeiSSL::SLOT_recarregarConfiguracoes(const QString& nomeArquivo)
{

    QFileInfo fileInfo(nomeArquivo);

    if (fileInfo.exists() && fileInfo.isFile())
    {
        vCarregarConfiguracoes(true);

        QString time =
            acInterface->allies->teamColor() == timeAzul ? "Azul" : "Amarelo";
        QString ladoCampo = acInterface->allies->iGetSide() == XNegativo
                                ? "Negativo"
                                : "Positivo";

        qInfo() << "Cor do time " << time << '\n'
                << "Lado " << ladoCampo << '\n'
                << "ID golero " << acInterface->allies->iGetGoalieID() << '\n'
                << "Porta Referee " << globalConfig.network.referee.port << '\n'
                << "Porta Visão " << globalConfig.network.vision.port;
    }
    else
    {
        qWarning() << "Não foi possível carregar arquivo de configurações.";
    }
}

void RoboFeiSSL::on_spin_distanciaParaChutePuxadinha_valueChanged(double arg1)
{
    fDistanciaParaChutePuxadinha = arg1;
}

void RoboFeiSSL::on_spin_tempoPosicionamentoFreeKick_valueChanged(double arg1)
{
    fTempoPosicionamentoFreeKick = arg1;
}

void RoboFeiSSL::on_spin_anguloErroChuteNormal_valueChanged(double arg1)
{
    fAnguloDeErroChuteNormal = arg1;
}

void RoboFeiSSL::on_spin_anguloErroPasseNormal_valueChanged(double arg1)
{
    fAnguloDeErroPasseNormal = arg1;
}

void RoboFeiSSL::on_spin_distanciaCobradorBolaParada_valueChanged(double arg1)
{
    fDistanciaCobradorBolaParada = arg1;
}

void RoboFeiSSL::on_tabAbas_currentChanged(int index)
{
    Q_UNUSED(index)

    vAjustaTamanhoInterface();
}

void RoboFeiSSL::on_spin_ConfigGerais_VelocidadeBolaChegadaPasse_valueChanged(
    double arg1)
{
    fVelocidadeBolaChegadaPasse = arg1;
}

void RoboFeiSSL::on_cbControlarSimulacao_stateChanged(int arg1)
{
    Q_UNUSED(arg1)
    // globalConfig.controlSimulation = ui->cbControlarSimulacao->isChecked();
}

void RoboFeiSSL::on_cbKickSensorInfluenciaVisao_stateChanged(int arg1)
{
    bUsarKickSensorNaVisao = arg1 == Qt::Checked;
}

void RoboFeiSSL::on_cb_utilizarJogadasEnsaiadas_stateChanged(int arg1)
{
    bUtilizarJogadasEnsaiadasFreeKick = arg1 == Qt::Checked;
}

void RoboFeiSSL::on_actionMudar_Cores_do_Mapa_triggered()
{
    QColor color = QColorDialog::getColor(DrawMap::corFundoCampo, this,
                                          "Escolha a cor do fundo de campo.");
    if (color.isValid())
        DrawMap::corFundoCampo = color;

    color = QColorDialog::getColor(DrawMap::corRoboAmarelo, this,
                                   "Escolha a cor do robô amarelo.");
    if (color.isValid())
        DrawMap::corRoboAmarelo = color;

    color = QColorDialog::getColor(DrawMap::corBordaRoboAmarelo, this,
                                   "Escolha a cor da borda do robô amarelo.");
    if (color.isValid())
        DrawMap::corBordaRoboAmarelo = color;

    color = QColorDialog::getColor(DrawMap::corRoboAzul, this,
                                   "Escolha a cor do robô azul.");
    if (color.isValid())
        DrawMap::corRoboAzul = color;

    color = QColorDialog::getColor(DrawMap::corBordaRoboAzul, this,
                                   "Escolha a cor da borda do robô azul.");
    if (color.isValid())
        DrawMap::corBordaRoboAzul = color;

    color = QColorDialog::getColor(DrawMap::corBolaEmCampo, this,
                                   "Escolha a cor da bola em campo.");
    if (color.isValid())
        DrawMap::corBolaEmCampo = color;

    color = QColorDialog::getColor(DrawMap::corBolaForaDeCampo, this,
                                   "Escolha a cor da bola fora de campo.");
    if (color.isValid())
        DrawMap::corBolaForaDeCampo = color;

    color = QColorDialog::getColor(DrawMap::corLinhasCampo, this,
                                   "Escolha a cor das linhas do campo.");
    if (color.isValid())
        DrawMap::corLinhasCampo = color;

    color = QColorDialog::getColor(DrawMap::corIdRobo, this,
                                   "Escolha a cor do ID dos robôs.");
    if (color.isValid())
        DrawMap::corIdRobo = color;

    color = QColorDialog::getColor(DrawMap::corFpsBaixo, this,
                                   "Escolha a cor do texto para FPS baixo.");
    if (color.isValid())
        DrawMap::corFpsBaixo = color;

    color = QColorDialog::getColor(DrawMap::corFpsAlto, this,
                                   "Escolha a cor do texto para FPS alto.");
    if (color.isValid())
        DrawMap::corFpsAlto = color;
}

void RoboFeiSSL::on_actionGraficoVelocidades_triggered()
{
    vMostraVelocidades();
}

void RoboFeiSSL::on_cb_ModeloForcaChute_currentTextChanged(const QString& arg1)
{
    strModeloForcaChute = arg1;
}

void RoboFeiSSL::on_cb_TipoDeltaNormal_currentTextChanged(const QString& arg1)
{
    bDeltaInteligenteNormal = arg1.compare("Inteligente") == 0;
}

void RoboFeiSSL::on_cb_ComportamentoTatico_currentIndexChanged(int index)
{
    /// \todo Remover comportamento antigo
    //     Estrategia::comportamento = static_cast<ComportamentoTatico>(index);
    qWarning() << "on_cb_ComportamentoTatico_currentIndexChanged :: Valor "
                  "sendo ignorado no momento!"
               << index;
}

void RoboFeiSSL::on_spin_ConfigGerais_RotacaoMaxima_valueChanged(double arg1)
{
    // ROBOT_ANGULAR_LIMIT = arg1;
}

void RoboFeiSSL::on_spin_distanciaMaximaPasse_valueChanged(double arg1)
{
    //     Estrategia::fDistanciaMaximaParaPasse = arg1;
    qWarning() << "on_spin_distanciaMaximaPasse_valueChanged :: Valor sendo "
                  "ignorado no momento!"
               << arg1;
}

void RoboFeiSSL::on_spin_distanciaMinimaPasse_valueChanged(double arg1)
{
    //     Estrategia::fDistanciaMinimaParaPasse = arg1;
    qWarning() << "on_spin_distanciaMinimaPasse_valueChanged :: Valor sendo "
                  "ignorado no momento!"
               << arg1;
}

void RoboFeiSSL::on_cb_comportamentoDefensorNormal_currentTextChanged(
    const QString& arg1)
{
    //     Estrategia::jogadaDefensorNormal = arg1 == "Delta" ?
    //     Normal_Defensor_Delta : Normal_Defensor_Marcacao;
    qWarning() << "on_cb_comportamentoDefensorNormal_currentTextChanged :: "
                  "Valor sendo ignorado no momento!"
               << arg1;
}

void RoboFeiSSL::on_spin_notaChute_valueChanged(int arg1)
{
    //     Estrategia::iNotaNecessariaParaChuteAoGol = arg1;
    qWarning()
        << "on_spin_notaChute_valueChanged :: Valor sendo ignorado no momento!"
        << arg1;
}

void RoboFeiSSL::on_spin_notaMinimaPasse_valueChanged(int arg1)
{
    //     Estrategia::iNotaMinimaJogadaPasse = arg1;
    qWarning() << "on_spin_notaMinimaPasse_valueChanged :: Valor sendo "
                  "ignorado no momento!"
               << arg1;
}

void RoboFeiSSL::on_cb_TemaInterface_currentTextChanged(const QString& arg1)
{
    vConfiguraTemaAplicacao(arg1);
}

void RoboFeiSSL::on_cb_ComportamentoTatico_currentTextChanged(
    const QString& arg1)
{
    qWarning() << "on_cb_ComportamentoTatico_currentTextChanged :: Valor sendo "
                  "ignorado no momento!"
               << arg1;
}
