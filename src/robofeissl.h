﻿/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file robofeissl.h
 * @brief Arquivo header da classe da interface do software.
 * @author Leonardo da Silva Costa, Guilherme Luis Pauli,
 * Bruno Bolos e outros
 * @version 1.0
 * @date 2020-08-25
 */

#ifndef ROBOFEISSL_H
#define ROBOFEISSL_H

#include <QDebug>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include <QMutex>
#include <QMessageBox>
#include <QTableView>
#include <QTableWidget>
#include <QFileDialog>
#include <QColorDialog>
#include <QLCDNumber>
#include <QTextBrowser>
#include <QDateTime>
#include <QMediaPlayer>
#include <QScopedPointer>

#include "Estrategia/estrategia.h"
#include "Mapa/visibilitygraph.h"
#include "Movimentacao/velocidade.h"
#include "Radio/radiobase.h"
#include "Rede/logger.h"
#include "Interface/drawmap.h"
#include "Interface/indicadorstatusrobo.h"
#include "Interface/opcoesinterface.h"
#include "Interface/movimentacaoclick.h"
#include "Interface/pathplanners.h"
#include "Interface/joystickrobotcontrollerinterface.h"
// #include "Decisoes/decisoes.h"
#include "Testes_Robos/testesrobos.h"
#include "Visao/visao.h"
#include "Visao/visionpacket.h"
#include "Simulador/controlesimulacao.h"
#include "Interface/decisoestesteinterface.h"
#include "Config/robofeiconfig.h"
#include "SimuladorRefbox/simuladorrefbox.h"
#include "Referee/refereepacket.h"

namespace Ui {
class RoboFeiSSL;
} // namespace Ui

/**
 * @brief Classe principal e da interface do software
 *
 * @details Essa classe lida com todas as interações que o software realiza com
 * o usuário, por exemplo, apresentar dados via desenhos no campo, coletar
 * parâmetros de configuração, configurar a execução de algum tipo de teste e etc.
 * Nenhuma outra classe deve fazer este tipo de coisa, tudo deve ser concentrado
 * aqui.
 *
 * O software pode apresentar outras janelas além da principal (diálogos),
 * como o que é feito na configuração dos testes dos path-planners, opções dos
 * desenhos simulador do refbox e outros. Porém, estes diálogos devem ser uma
 * classe separada que interagem com esta classe principal. Mais especificamente,
 * esta classe lida diretamente somente com os elementos da janela principal do
 * software.
 *
 * As outras atividades do software estão todas separadas em outras threads
 * (visão, movimentação, estratégia ...), isso é feito para melhorar o desempenho
 * dessas atividades. O tratamento do referee só está nesta classe/thread, pois
 * não é uma tarefa que necessita de alta performance, logo, não haveriam ganhos
 * em criar uma thread individual para tratar o referee ou colocar ele em outra
 * thread.
 */
class RoboFeiSSL : public QMainWindow
{
    Q_OBJECT

public:

    /**
     * @brief Construtor da classe
     *
     * @param parent
     */
    explicit RoboFeiSSL(QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent* e)override;

private:

    /**
     * @brief Destrutor da classe
     */
    ~RoboFeiSSL() override;

    Ui::RoboFeiSSL *ui; /**< Ponteiro para manipulação da interface. */

    bool bFechar; /**< Indica se o software está no processo de finalização.*/

    QElapsedTimer etmFPSInterface; /**< Timer para medir o FPS de atualização da
                                        interface.*/
    QVector<int> iFPSCounter; /**< Vetor para cálculo do valor médio do FPS da interface.*/
    int iFPSInterface; /**< FPS médio da thread da interface. */

    QMediaPlayer *player;          /**< Utilizado para tocar alguns sons de acordo
                                        com os comandos do Referee.*/
    QElapsedTimer etmTempoMusica;  /**< Controla quanto tempo o som vai tocar.*/
    bool bMutar;                   /**< Muta todos os sons emitidos. */


    //=============================================================== Interface =========================================
    QScopedPointer<QTimer> tmrAtualizacaoDesenhos; /**< Timer do loop de atualização do
                                                        desenho do campo.*/
    QScopedPointer<QTimer> tmrAtualizacaoDadosInterface; /**< Timer do loop de atualização
                        dos indicadores de status dos robôs. @see IndicadorStatusRobo*/
    QElapsedTimer etmTempoGraficoVelocidades; /**< Timer que gera a base de tempo para os
                                                gráficos de velocidade. @see Velocidade*/
    bool bInicializaInterface; /**< Indica que a interface pode ser inicializada. Isso
                                só ocorre após a chegada do primeiro pacote de visão.*/

    QScopedPointer<DrawMap> dmDesenhaCampo; /**< Objeto da classe que faz os desenhos
                                                do campo. @see DrawMap*/

    QScopedPointer<Velocidade> velDialog; /**< Objeto do diálogo que mostra a
                                        velocidade da bola e dos robôs. @see Velocidade*/
    QScopedPointer<SimuladorRefbox> simRefBox; /**< Objeto da classe utilizada para
                                                simular o Refbox. @see SimuladorRefbox*/

    QVector<QGridLayout*> *gridStatusRobos; /**< Layout em grade para posicionar o
                                                 indicador de status dos robôs e a janela
                                                 de texto com algumas informações sobre
                                                 o robô.*/
    QVBoxLayout *layoutStatus;
    QScopedPointer<IndicadorStatusRobo> lstStatusRobos[MAX_IDS];/**< Indicadores
                                                                  dos status dos robôs */
    QScopedPointer<OpcoesInterface> opOpcoesInterface; /**< Diálogo com as opções da
                                                         interface. @see OpcoesInterface*/
    QVector<QTextBrowser*>* textsJogada; /**< Janelas de texto com algumas informações
                                                sobre o robô.*/

    QVector<QVector2D> vt2dTracoRobo;/**< Guarda o traço do robô selecionado na aba de
                                          movimentação da tela de jogo.*/

    QFile fileMatrizJogadas; /**< Arquivo da matriz de restrição de jogadas.*/

    //=============================================================== Estrategia ========================================
    QScopedPointer<Estrategia> estEstrategia; /**< Objeto da thread de estratégia.
                                                   @see Estrategia*/
    QScopedPointer<MovimentacaoRobo> moveMovimentacaoRobo; /**< Objeto da thread de
                                                   movimentação. @see MovimentacaoRobo*/
    QThread *threadEstrategia; /**< Thread que roda a estratégia.*/
    QThread *threadMovimentacao; /**< Thread que roda o controle de movimentação.*/

    bool bRodando; /**< Indica se o software está rodando. Tanto no modo Jogo ou Teste.*/

    //=============================================================== Rede ==============================================
    QScopedPointer<QUdpSocket> udpRefbox; /**< Socket UDP de comunicação com o Refbox.*/
    Logger RoboController; /**< Utilizado para gerar logs da visão e do
                                        referee.*/
    QScopedPointer<Visao>  visVisao; /**< Objeto da thread da visão.
                                        @see Visao*/
    QThread *threadVisao; /**< Thread que roda a visão.*/

    //=============================================================== Interface =========================================
    AmbienteCampo *acInterface; /**< Objeto do ambiente de campo da thread da interface.*/
    QScopedPointer<RefereeReceiver> referee;

    //=============================================================== Testes ============================================
    QScopedPointer<MovimentacaoClick> mcTesteMovimentacaoClick; /**< Interface de
                              configuração do teste de movimentação definida pelo mouse.
                              @see MovimentacaoClick*/
    QScopedPointer<TestePathPlanners> tppTestePathPlanners; /**< Interface de configuração
                                                             do teste dos path-planners.
                                                             @see TestePathPlanners*/
    QScopedPointer<TestesRobos> trTestesRobos; /**< Objeto da thread que executa os
                                                 testes com os robôs.  @see TestesRobos*/
    QThread *threadTestes; /**< Thread que executa os testes.*/

    QFile fileLogsVelocidadeBola; /**< Arquivo de log da bola. */

    QScopedPointer<JoystickRobotControllerInterface> joystickRobotControllerInterface; /**< Interface com os status e
                                                                                         configurações do teste de controlar
                                                                                         o robô com o joystick. */

//     QScopedPointer<DecisoesTesteInterface> decisoesTeste;

    //=============================================================== Log Movimentacao ==================================
    QElapsedTimer etmTimerLogMovimentacao;/**< \todo: Mover para a thread de movimentação */
    QFile fileLogsMovimentacao;/**< \todo: Mover para a thread de movimentação */
    //=============================================================== Interface =========================================

    ControleSimulacao controleSimulacao;

    RoboFeiConfig config;

    /**
    * @brief Realiza algumas configurações iniciais da interface
    */
    void vConfiguraInterface();

    /**
    * @brief Ajusta o tamanho da interface para caber no monitor
    */
    void vAjustaTamanhoInterface();

    /**
    * @brief Inicializa os widgets de status dos robôs
    * @see IndicadorStatusRobo
    */
    void vInicializaStatusRobos();

    /**
    * @brief Inicializa o desenho do campo
    */
    void vInicializaDesenhos();

    /**
    * @brief Inicia a thread de estratégia
    * @see Estrategia
    */
    void vIniciaEstrategia();

    /**
    * @brief Inicia a thread de testes
    * @see TestesRobos
    */
    void vIniciaTestes();

    /**
    * @brief Inicia a thread de controle de movimentação
    * @return Retorna true se conseguiu iniciar corretamente
    */
    bool bIniciaMovimentacao();

    /**
    * @brief Salva a matriz de restrição de jogadas
    * @see AmbienteCampo::bMatrizJogadas
    */
    void vSalvaMatrizJogadas();

    /**
    * @brief Carrega a matriz de restrição de jogadas
    * @see AmbienteCampo::bMatrizJogadas
    * @return bool - True se o arquivo estava no formato correto e foi carregado
    */
    bool bCarregaMatrizJogadas();


    /**
    * @brief Atualiza as opções do desenho definidas no diálogo de opções
    * @see OpcoesInterface
    */
    void vAtualizaOpcoesDesenho();

    /**
    * @brief Atualiza o indicador de status do robô e a janela de texto com suas
    * informações
    * @see IndicadorStatusRobo
    *
    * @param _robot - Robô a ser atualizado
    */
    void vAtualizaStatusRobo(const Robo *_robot);

    /**
    * @brief Carrega os parâmetros da interface a partir de um arquivo
    *
    * @param nomeArquivo - Nome do arquivo com as configurações
    */
    void vCarregarParametrosInterface(QFile* arquivoConfiguracoes);

    /**
    * @brief Salva as configurações atuais da interface em um arquivo
    * @param filename - Nome do arquivo para salvar
    * @param ultimaSessao - Indica se o processo para salvar está sendo feito ao
    * finalizar o programa.
    *
    * @details Caso o usuário que solicite para salvar as configurações, uma janela
    * irá aparecer perguntando qual o caminho para salvar. Ao fechar, o programa também
    * sempre salva as configurações atuais automaticamente num arquivo chamado
    * "LastSession.cfg"
    */
    void vSalvarConfiguracoes(QString filename, bool ultimaSessao = false);

    /**
    * @brief Carrega as configurações da interface a partir de um arquivo
    *
    * @param arquivoPadrao - Indica se o arquivo a ser carregado é o arquivo padrão
    *  do usuário (`~/.local/share/robofei-ssl/strategy/LastSession.cfg`) se o parâmetro
    *  for `false`, uma GUI FileDialog será criada para que o usuário escolha o arquivo
    *  a ser carregado.
    */
    void vCarregarConfiguracoes(const bool arquivoPadrao = false);

    //=============================================================== Radio =============================================
    /**
    * @brief Atualiza os status dos robôs na interface
    */
    void vAtualizaDadosRobos();

    /**
     * @brief Inicializa os atalhos para mudança entre abas.
     */
    void vInicializaShortcutsAdicionais();

    /**
    * @brief Mostra a janela de velocidades
    *
    * @see Velocidade
    */
    void vMostraVelocidades();

    /**
     * @brief Configura o esquema de cores do software, as opções estão em
     * #TEMAS_INTERFACE
     */
    void vConfiguraTemaAplicacao(const QString& temaAplicacao);

private slots:
    //=============================================================== Rede ==============================================
    /**
     * @brief Conecta-se ao Refbox, inicia a thread da visão e inicia os arquivos
     * de log caso estejam habilitados
     *
     * @see Visao
     */
    void vConectarRede();

    /**
     * @brief Desconecta-se do Refbox, finaliza a thread da visão e para de
     * coletar os logs
     */
    void vDesconectarRede();

    /**
     * @brief Processa o ultimo pacote recebido do Refbox e envia os dados para
     * as outras threads
     */
    void vReceiveRefereeData_(const RefereePacket _packet);

    /**
     * @brief Recebe os dados da visão filtrados
     * @param _packet - Pacote com os dados
     *
     * @see Visao
     * @see VisionPacket
     */
    void vReceiveVisionData_(const VisionPacket _packet);

    /**
     * @brief Recebe os dados de feedback da simulação
     *
     * @param _feedback
     */
    void vReceiveFeedbackData_(const RadioFeedback &_feedback);

    /**
     * @brief Recebe a string de erro ocorrido no momento de conectar o rádio
     * @param strErro - Erro ocorrido
     */
    void vRecebeErroRadio(const QString &strErro);

    /**
     * @brief Recebe o código do erro ocorrido no pacote recebido pelo rádio
     * @param erro - Código do erro
     */
    void vRecebeErroPacote(ErrorCode erro);

    //=============================================================== Interface =========================================

    /**
     * @brief Começa o jogo real
     */
    void vComecarJogo();

    /**
     * @brief Para o jogo real
     */
    void vPararJogo();

    /**
     * @brief Recebe o sinal indicando mudança na cor do time
     * @param _iNovoIndice - Índice da nova cor no seletor da interface
     */
    void vCorTimeMudou(int _iNovoIndice);

    /**
     * @brief Recebe o sinal indicando mudança no lado de defesa
     * @param _iNovoIndice - Índice do novo lado de defesa no seletor da interface
     */
    void vLadoDefesaMudou(int _iNovoIndice);

    /**
     * @brief Recebe o sinal indicando mudança no ID do goleiro
     * @param _iNovoIndice - Índice do novo ID do goleiro no seletor da interface
     */
    void vGoleiroMudou(int _iNovoIndice);

    /**
     * @brief Recebe o sinal indicando mudança no tipo de ambiente de jogo
     * @details O ambiente de jogo pode ser #GR_SIM ou #CAMPO_REAL
     * @param _iNovoIndice - Índice do novo tipo de ambiente no seletor da interface
     */
    void vTipoAmbienteMudou(int _iNovoIndice);

    /**
     * @brief Atualiza os gráficos de velocidade caso eles estejam visíveis
     */
    void vAtualizaGraficosVelocidades();

    /**
     * @brief Atualiza dados da interface, como, opções do desenho do campo,
     * dados dos robôs e posição do timeout
     */
    void vAtualizaDadosInterface();

    /**
     * @brief Ação para tirar um print da imagem do campo
     */
    void on_actionPrintCampo_triggered();

    /**
     * @brief Atualiza a matriz de jogadas a partir da matriz da interface
     * @see AmbienteCampo::bMatrizJogadas
     */
    void vAtualizaMatrizJogadas();

    /**
     * @brief Recebe o sinal quando uma célula da matriz de jogadas é alterada,
     * i.e. recebe um duplo click
     * @param nRow - Linha da célula acionada
     * @param nCol - Coluna da célula acionada
     */
    void vCelulaAlterada(int nRow, int nCol);

    /**
     * @brief Reseta a matriz de jogadas
     * @details Neste caso, as jogadas são habilitadas para todos os robôs
     * @see AmbienteCampo::bMatrizJogadas
     */
    void vResetaMatriz();

    /**
    * @brief Recebe o sinal de click no status do robô
    * @details Atualmente, o sinal de click só é responsável por mudar a cor
    * do trajeto do robô. Porém, seria legal criar uma janela de estatísticas
    * do robô e apresentar esta janela ao clicar no botão de status
    * @see IndicadorStatusRobo
    *
    * @param id - ID do robô clicado
    */
    void vMudaCorRobo(const int &id);

    /**
     * @brief Ação para salvar as configurações da interface
     *
     */
    void on_actionSalvarConfiguracoes_triggered();

    /**
     * @brief Ação para carregar as configuração da interface
     *
     */
    void on_actionCarregarConfiguracoes_triggered();

    /**
     * @brief Ação para mutar todos os sons do software
     *
     */
    void on_actionMute_triggered();

    /**
     * @brief Ação para abrir o simulador do Refbox
     * @see SimuladorRefbox
     */
    void on_actionAbrir_Refbox_triggered();

    /**
     * @brief Recebe o sinal de alteração do FPS das câmeras
     */
    void on_spin_ConfigGerais_FPS_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no número de pacotes para sair da visão
     * @see #globalConfig.framesUntilMissing
     */
    void on_spin_ConfigGerais_PacotesSairVisao_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no raio de segurança da bola
     * @see #iRaioSegurancaBola
     */
    void on_spin_ConfigGerais_RaioSegurancaBola_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no raio de segurança do oponente
     * @see #globalConfig.safety.opponent
     */
    void on_spin_ConfigGerais_RaioSegurancaOponente_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no raio de segurança aliado
     * @see #globalConfig.safety.ally
     */
    void on_spin_ConfigGerais_RaioSegurancaAliado_editingFinished();

    /**
     * @brief Recebe o sinal de alteração na largura das divisões do campo
     * @see #globalConfig.fieldDivisionWidth
     */
    void on_spin_ConfigGerais_LarguraDivisaoCampo_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no raio do referee circle
     * @see #iRaioRefereeCircle
     */
    void on_spin_ConfigGerais_RefereeCircle_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no deslocamento da bola
     * @see #globalConfig.ballDistanceToPlay
     */
    void on_spin_ConfigGerais_DeslocamentoBola_editingFinished();

    /**
     * @brief Recebe o sinal de alteração na largura da mira
     * @see #iLarguraMinimaMira
     */
    void on_spin_ConfigGerais_LarguraMira_editingFinished();

    /**
     * @brief Recebe o sinal de alteração na distância entre os robôs e a area de defesa
     * @see #globalConfig.distanceToDefenseArea
     */
    void on_spin_ConfigGerais_DistanciaRobosDefesaGolAreaPenalty_editingFinished();

    /**
     * @brief Recebe o sinal de alteração na forca máxima do chute
     * @see #globalConfig.calibratedKickForce
     */
    void on_spin_ConfigGerais_ForcaChute_editingFinished();

    /**
     * @brief Recebe o sinal de alteração na distância entre o receptor e cobrador
     * @see #iDistanciaReceptorCobranca
     */
    void on_spin_ConfigGerais_DistanciaReceptorCobranca_editingFinished();

    /**
     * @brief Recebe o sinal de alteração na velocidade máxima durante o stop
     * @see #dVelocidadeStop
     */
    void on_spin_ConfigGerais_VelocidadeStop_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no tamanho do buffer de posições da bola
     * @see #iTamanhoBufferBola
     */
    void on_spin_ConfigGerais_TamanhoBufferBola_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no tamanho do traço do robô
     * @see #iTamanhoTracoRobo
     */
    void on_spin_ConfigGerais_TamanhoTraco_editingFinished();

    /**
     * @brief Recebe o sinal de alteração na velocidade máxima do delta
     * @see #iVelocidadeDelta
     */
    void on_spin_ConfigGerais_VelocidadeDelta_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no ângulo de impedimento de gol contra
     * @note Acredito que seja possível remover este parâmetro agora
     * @see #iAnguloGolContra
     */
    void on_spin_ConfigGerais_AnguloGolContra_editingFinished();

    /**
     * @brief Recebe o sinal de alteração no uso do campo inteiro ou só metade
     * @see #TesteMeioCampo, #CampoInteiro
     * @param index - Índice do item atual com a opção atualizada
     */
    void on_cb_ConfigGerais_TesteMeioCampo_currentIndexChanged(int index);

    /**
     * @brief Recebe o sinal de alteração no offset do campo
     * @see #globalConfig.fieldOffset
     */
    void on_spin_ConfigGerais_OffsetCampo_editingFinished();

    /**
     * @brief Muda para a primeira aba da interface
     */
    void vMudaParaAba1();

    /**
     * @brief Muda para a segunda aba da interface
     */
    void vMudaParaAba2();

    /**
     * @brief Muda para a terceira aba da interface
     */
    void vMudaParaAba3();

    /**
     * @brief Alterna entre as abas da interface
     */
    void vSwitchTab();

    /**
     * @brief Recebe o sinal de click do mouse no desenho do campo e muda a
     * posição da bola no GrSim
     * @param position - Posição em que o mouse clicou já referenciada na escala
     * do campo [mm]
     * @param button - Botão que foi pressionado
     */
    void vControlaSimulacaoMouseClicked(const QVector2D position, const Qt::MouseButton button);

    /**
     * @brief Recebe o sinal de click do mouse no desenho do campo e muda a
     * posição da bola no GrSim
     * @param position - Posição em que o mouse clicou já referenciada na escala
     * do campo [mm]
     * @param button - Botão que foi solto
     */
    void vControlaSimulacaoMouseReleased(const QVector2D position, const Qt::MouseButton button);

    /**
     * @brief Recebe o sinal de movimentação do mouse no desenho do campo e muda
     * a posição da bola no GrSim se o botão esquerdo estiver pressionado
     * @param position - Posição do mouse no campo já referenciada na escala do
     * campo [mm]
     */
    void vControlaSimulacaoMouseMoved(const QVector2D position);

    //======================================================= Radio =====================================================
    /**
     * @brief Atualiza a lista de portas USB/Serial disponíveis para conectar-se
     * ao rádio
     * @see RadioBase
     */
    void vAtualizaPortas();

    //================================================== Movimentação ===================================================
    /**
     * @brief Recebe os dados dos robôs modificados pela thread de controle de
     * movimentação
     * @see MovimentacaoRobo
     * @param _packet
     */
    void vRecebeDadosMovimentacao(const MotionFeedbackPacket &_packet);

    //=============================================================== Testes ============================================
    /**
     * @brief Começa a realizar os testes
     */
    void vComecarTestes();

    /**
     * @brief Para a execução dos testes
     */
    void vPararTestes();

    //Referee
    /**
     * @brief Recebe o sinal contendo o comando atual do simulador do Refbox
     * @see SimuladorRefbox
     * @param comando - Comando atual
     * @param posBallPlacement - Posição do ball placement
     */
    void vRecebeComandosSimuladorReferee(SSL_Referee_Command comando, QVector2D posBallPlacement);
    /**
     * @brief Recebe o sinal para destruir o objeto RoboFeiSSL::velDialogVelocidades
     */
    void vFinalizaVelocidades();

    /**
     * @brief Recebe o sinal com o estado e o nome da opção que mudou
     * @param opcao - Nome da opção
     * @param estadoAtual - Estado da opção. (true/false = habilitada/desabilitada)
     * @see OpcoesInterface
     */
    void vRecebeOpcaoInterface(const QString &opcao, const bool &estadoAtual);

    /**
     * @brief Ação para abrir o diálogo de opções da interface
     */
    void on_actionOpcoes_triggered();

    /**
     * @brief Ação para abrir o diálogo de configuração do teste de movimentação
     * com o click
     * @see MovimentacaoClick
     */
    void on_actionMovimentacaoClick_triggered();

    /**
     * @brief Ação para abrir o diálogo de configuração do teste dos path-planners
     * @see TestePathPlanners
     */
    void on_actionPathPlanners_triggered();

    /**
     * @brief Recebe o sinal de alteração no valor do fator controle passe
     * @see Decisoes::dFatorControleAvaliacaoPasse
     * @param arg1 - Novo valor
     */
    void on_dsb_FatorControlePasse_valueChanged(double arg1);

    /**
     * @brief Recebe o sinal de alteração no valor do fator controle chute
     * @see Decisoes::dFatorControleAvaliacaoChute
     * @param arg1 - Novo valor
     */
    void on_dsb_FatorControleChute_valueChanged(double arg1);

    /**
     * @brief Recebe o sinal de alteração no valor do fator avaliação ângulo mínimo
     * passe
     * @see Decisoes::dAnguloMinimoFatorRotacaoPasse
     * @param arg1 - Novo valor
     */
    void on_sb_FatorAvaliacaoAnguloMinimoPasse_valueChanged(int arg1);

    /**
     * @brief Recebe o sinal de alteração no valor do fator avaliação ângulo mínimo
     * chute
     * @see Decisoes::dAnguloMinimoFatorRotacaoChute
     * @param arg1 - Novo valor
     */
    void on_sb_FatorAvaliacaoAnguloMinimoChute_valueChanged(int arg1);

    /**
     * @brief Recebe o sinal de alteração no valor do fator avaliação ângulo máximo
     * passe
     * @see Decisoes::dAnguloMaximoFatorRotacaoPasse
     * @param arg1 - Novo valor
     */
    void on_sb_FatorAvaliacaoAnguloMaximoPasse_valueChanged(int arg1);

    /**
     * @brief Recebe o sinal de alteração no valor do fator avaliação ângulo máximo
     * chute
     * @see Decisoes::dAnguloMaximoFatorRotacaoChute
     * @param arg1 - Novo valor
     */
    void on_sb_FatorAvaliacaoAnguloMaximoChute_valueChanged(int arg1);

    /**
     * @brief Recebe o sinal de ativação do fator de rotação
     * @see Decisoes::bFatorRotacao
     */
    void on_ckb_FatorDeRotacao_released();


    void on_actionVirtualJoystick_triggered();

    void on_actionDecisoes_triggered();

    void on_cb_DribleDoRobo_currentIndexChanged(int index);

    void on_spin_TempoRecargaChute_valueChanged(int arg1);

    void on_actionMensagens_triggered();

    void on_spin_distanciaParaChutePuxadinha_valueChanged(double arg1);

    void on_spin_tempoPosicionamentoFreeKick_valueChanged(double arg1);

    void on_spin_anguloErroChuteNormal_valueChanged(double arg1);

    void on_spin_anguloErroPasseNormal_valueChanged(double arg1);

    void on_spin_distanciaCobradorBolaParada_valueChanged(double arg1);

    void on_tabAbas_currentChanged(int index);

    void on_spin_ConfigGerais_VelocidadeBolaChegadaPasse_valueChanged(double arg1);

    void on_cbControlarSimulacao_stateChanged(int arg1);

    void on_cbKickSensorInfluenciaVisao_stateChanged(int arg1);

    void on_cb_utilizarJogadasEnsaiadas_stateChanged(int arg1);

    void on_actionMudar_Cores_do_Mapa_triggered();

    void on_actionGraficoVelocidades_triggered();

    void on_cb_ModeloForcaChute_currentTextChanged(const QString &arg1);

    void on_cb_TipoDeltaNormal_currentTextChanged(const QString &arg1);

    void on_cb_ComportamentoTatico_currentIndexChanged(int index);

    void on_spin_ConfigGerais_RotacaoMaxima_valueChanged(double arg1);

    void on_spin_distanciaMaximaPasse_valueChanged(double arg1);

    void on_spin_distanciaMinimaPasse_valueChanged(double arg1);

    void on_cb_comportamentoDefensorNormal_currentTextChanged(const QString &arg1);

    void on_spin_notaChute_valueChanged(int arg1);

    void on_spin_notaMinimaPasse_valueChanged(int arg1);

    void on_cb_TemaInterface_currentTextChanged(const QString &arg1);

    void on_cb_ComportamentoTatico_currentTextChanged(const QString &arg1);

signals:
    //=================================================== Rede=======================================================

    /**
     * @brief Envia as configurações para a thread da visão conectar-se à rede
     * @see Visao
     * @param interfaceIndice - Interface de comunicação com a visão
     */
    void vEnviaConfiguracoesRedeVisao(const QString& strInterface);

    /**
     * @brief Envia as configurações para a thread da movimentação conectar-se ao GrSim
     * @see MovimentacaoRobo
     * @param ipGrSim - IP do GrSim
     * @param portaGrSim - Porta do GrSim
     * @param interfaceIndice - Interface de comunicação com o GrSim
     */
    void vEnviaConfiguracoesRedeSimulador(int portaControle, int portaRobo, const QString& strInterface);

    /**
     * @brief Envia o sinal indicando para se desconectar com a visão
     * @see Visao
     */
    void vDesconectarVisao();

    /**
     * @brief Envia o sinal indicando se é para usar o campo completo ou só metade
     * @see Visao, #CampoInteiro, #TesteMeioCampo
     * @param _testeCampo
     */
    void vEnviarTesteMeioCampo(int _testeCampo);

    /**
     * @brief Envia o sinal indicando qual a cor do time atual
     * @see #Time
     * @param _timeAtual
     */
    void vEnviarTimeAtual(int _timeAtual);

    //=================================================== Interface =================================================
    /**
     * @brief Envia o sinal contendo a velocidade da bola
     * @param _dVelocidade - Velocidade atual da bola [m/s]
     */
    void vEnviaVelocidadeBola(const double& _dVelocidade);

    /**
     * @brief Envia o sinal contendo a velocidade atual do robô fornecido
     * @param _dVelocidade - Velocidade atual do robô [m/s]
     * @param _iID - ID do robô
     */
    void vEnviaVelocidadeRobo(const double& dNovaVelocidade, const qint8& _iID);

    //=================================================== Threads =======================================================
    /**
     * @brief Envia o sinal para ativar/desativar as threads
     * @param _bAtivaThread - Indica se é para ativar ou desativar (true/false)
     */
    void vAtivarThread(const bool _bAtivaThread);

    /**
     * @brief Envia o sinal contendo os dados do Referee
     *
     * @param comando - Cast do comando atual para int.
     * @details Ao interpretar o sinal, deve-se fazer o cast de volta para
     * SSL_Referee_Command, e.g.:
     * @code static_cast<SSL_Referee_Command>(comando) @endcode
     *
     * @param estagio - Cast do estagio atual de jogo para int.
     * @details Ao interpretar o sinal, deve-se fazer o cast de volta para
     * SSL_Referee_Stage, e.g.:
     * @code static_cast<SSL_Referee_Stage>(estagio) @endcode
     *
     * @param goleiroOponente - ID do goleiro oponente
     * @param posBallPlacement - Posição do ball placement [mm]
     */
    void vEnviaDadosReferee(int comando, int estagio,
                            int goleiroOponente, QVector2D posBallPlacement);

    /**
     * @brief Envia o sinal contendo alguns dados da interface
     * @param posTimeout - Posição inicial que os robôs ficam na hora do timeout
     * @param idGoleiro - ID do nosso goleiro
     * @param corTime - Cor do nosso time
     * @param ladoCampo - Lado que defendemos
     * @param tipoAmbiente - Tipo do ambiente de jogo
     */
    void vEnviaDadosInterface(QVector2D posTimeout, int idGoleiro, int corTime,
                              int ladoCampo, bool tipoAmbiente);

    /**
     * @brief Envia o sinal contendo os dados dos testes
     * @param dados - Dados dos testes
     */
    void vEnviaDadosTestes(const DADOS_TESTES &dados);

    /**
     * @brief Envia o sinal com a config do radio
     *
     * @param _config
     */
    void sendRadioConfig(const ConfiguracaoRadio &_config);

    //=================================================== Testes ========================================================

    /**
     * @brief Envia o sinal indicando a nova posição e velocidade da bola no GrSim
     * @param vt2dposicao - Nova posição da bola [m]
     * @param vt2dVelocidade - Nova velocidade da bola [m/s]
     */
    void vMudarBolaSimulador(QVector2D vt2dposicao, QVector2D vt2dVelocidade);

    void SIGNAL_mudarPosicaoRoboSimulador(bool amarelo, QVector2D destino, qint8 id);
    void SIGNAL_mudarAnguloRoboSimulacao(bool amarelo, float angulo, qint8 id);

public slots:
    void SLOT_mudarEstadoRede(const bool ativar);
    void SLOT_mudarEstadoEstrategia(const bool ativar);
    void SLOT_recarregarConfiguracoes(const QString &nomeArquivo);
};


#endif // ROBOFEISSL_H
