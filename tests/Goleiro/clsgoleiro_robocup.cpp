﻿#include "clsgoleiro.h"
#include "qdebug.h"

clsGoleiro::clsGoleiro()
{

}


void clsGoleiro::vGoleiroBolaClose(AmbienteCampo *acAmbienteGoleiro, float _fVelocidade)
{
    Atributos atbGoleiro, atbOponenteComBola;
    Bola blBola;
    acAmbienteGoleiro->vPegaBola(0, blBola);
    QVector2D vt2dDest;
    float ft = 2;                                 // fator que vai multiplicar diametro do robo para dar sua posicao no gol

    atbGoleiro = acAmbienteGoleiro->rbtRoboAliado[acAmbienteGoleiro->iIDGoleiro]->getRobot();
    atbOponenteComBola = acAmbienteGoleiro->rbtRoboAdversario[iIdInimigoComBola(acAmbienteGoleiro)]->getRobot();

//    if(qAbs(blBola.vt2dPosicaoAtual.x()) > (acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - acAmbienteGoleiro->iPegaProfundidadeAreaPenalty()))
//        ft = 0.5;

    if (acAmbienteGoleiro->iLadoCampo == XPositivo) {

        // Caso nao tenha nenhum jogador adversario com a bola
//        if(iIdInimigoComBola(acAmbienteGoleiro) == -1){

            // o destino do robo sera dado por BallDestiny2
            vt2dDest = ballDestiny2(acAmbienteGoleiro);

            if(clsAuxiliar::vt2dCalculaPosicaoPegarBolinha(acAmbienteGoleiro->vt2dBufferPosicaoBola, blBola.vt2dPosicaoAtual, blBola.vt2dPosicaoAnterior, 20) == blBola.vt2dPosicaoAtual)
            {
                vt2dDest = QVector2D((acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - 1.5*globalConfig.robotDiameter), 0);
            }

//            acAmbienteGoleiro->testeMiraChute.clear();
//            acAmbienteGoleiro->testeMiraChute.append(vt2dDest);

            if(atbGoleiro.vt2dDestino.distanceToPoint(vt2dDest) > globalConfig.robotDiameter/2)
                atbGoleiro.fTempoRobo = 0;

            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setRobot(atbGoleiro);

            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setDestino(vt2dDest);
            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaVelocidadeMaxima(_fVelocidade);
            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaPontoAnguloDestino(blBola.vt2dPosicaoAtual);//(vt2dPontoAnguloRotacao(acAmbienteGoleiro));   // angulo do goleiro dado pela função
            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaIgnorarBola(bNaoIgnoraBola);

            // Quando a bola estiver dentro da area
            if( (blBola.vt2dPosicaoAtual.x() > (acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - acAmbienteGoleiro->iProfundidadeAreaPenalty)) && (qAbs(blBola.vt2dPosicaoAtual.y()) < acAmbienteGoleiro->iLarguraAreaPenalty/2) ){

                atbGoleiro.cmd_rollerspd = 100; // Liga Roller
                atbGoleiro.roller = true;       // Vel. Roller

                // Quando a posicao atual da bola em relacao a sua anterior variar menos que 9mm (pode ser mudado se necessario) atribuimos que ela esta parada
                if(clsAuxiliar::vt2dCalculaPosicaoPegarBolinha(acAmbienteGoleiro->vt2dBufferPosicaoBola, blBola.vt2dPosicaoAtual, blBola.vt2dPosicaoAnterior, 20) == blBola.vt2dPosicaoAtual
                        /*(qAbs(blBola.vt2dPosicaoAtual.y() - blBola.vt2dPosicaoAnterior.y()) < 15) || (qAbs(blBola.vt2dPosicaoAtual.x() - blBola.vt2dPosicaoAnterior.x()) < 15)*/){

                    // Se posiciona atras da bola antes de chutar
                    vt2dDest = blBola.vt2dPosicaoAtual;//clsAuxiliar::vt2dCalculaPontoCobranca(QVector2D(-acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2,0),blBola.vt2dPosicaoAtual,300, acAmbienteGoleiro->vt2dPegaTamanhoCampo());

                    if(atbGoleiro.vt2dDestino.distanceToPoint(vt2dDest) > globalConfig.robotDiameter/2)
                        atbGoleiro.fTempoRobo = 0;

                    acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setRobot(atbGoleiro);

                    acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaVelocidadeMaxima(_fVelocidade);
                    acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setDestino(vt2dDest);
                    acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaIgnorarBola(bNaoIgnoraBola);


                    // Qaundo ele ja estiver atras da bola, ele se aproxima para chutar
                    if(atbGoleiro.vt2dPosicaoAtual.distanceToPoint(blBola.vt2dPosicaoAtual) < globalConfig.robotDiameter*2){


                        QVector2D vt2dPontoMira = blBola.vt2dPosicaoAtual;
                        float fDistanciaBola = globalConfig.robotDiameter;

                        if(clsAuxiliar::fCalculaDistancia(atbGoleiro.vt2dDestino, atbGoleiro.vt2dPosicaoAtual) <= globalConfig.robotDiameter)//Aproximação da bola
                        {
                            fDistanciaBola = clsAuxiliar::fCalculaDistancia(blBola.vt2dPosicaoAtual, atbGoleiro.vt2dPosicaoAtual) - globalConfig.robotDiameter;
                        }
//                        else //Irá chutar
//                        {
//                            fDistanciaBola = -globalConfig.ballDiameter*4;
//                        }

                        vt2dPontoMira = blBola.vt2dPosicaoAtual;//clsAuxiliar::vt2dCalculaPontoCobranca(vt2dPontoMira, blBola.vt2dPosicaoAtual, fDistanciaBola, acAmbienteGoleiro->vt2dPegaTamanhoCampo());

                        if(atbGoleiro.vt2dDestino.distanceToPoint(vt2dDest) > globalConfig.robotDiameter/2)
                            atbGoleiro.fTempoRobo = 0;

                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setRobot(atbGoleiro);

                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setDestino(vt2dPontoMira);
                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaVelocidadeMaxima(_fVelocidade);
                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaPontoAnguloDestino(blBola.vt2dPosicaoAtual);
                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->chutar(CHIP_KICK_STRONG,100,0,0);
                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaIgnorarBola(bIgnoraBola);


                    }
                }
            }
//        }
        // Caso a bola esteja com algum jogador adversario
//        else{

//            // o destino do robo sera o ponto pra onde o adversario mira
//            vt2dDest = bolarobo(acAmbienteGoleiro); //inimigo_ponto_futuro(acAmbienteGoleiro, ft);
//            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setDestino(vt2dDest);
//            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaVelocidadeMaxima(_fVelocidade);
//            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaPontoAnguloDestino(vt2dPontoAnguloRotacao(acAmbienteGoleiro));
//            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaIgnorarBola(bIgnoraBola);

//        }
    }

    if(acAmbienteGoleiro->iLadoCampo == XNegativo) {

        // Caso nao tenha nenhum jogador adversario com a bola
//        if(iIdInimigoComBola(acAmbienteGoleiro) == -1){

            // o destino do robo sera dado por BallDestiny2
            vt2dDest = ballDestiny2(acAmbienteGoleiro);

//            if(vt2dDest.x() > -(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - acAmbienteGoleiro->iPegaProfundidadeAreaPenalty()))
             if(clsAuxiliar::vt2dCalculaPosicaoPegarBolinha(acAmbienteGoleiro->vt2dBufferPosicaoBola, blBola.vt2dPosicaoAtual, blBola.vt2dPosicaoAnterior, 20) == blBola.vt2dPosicaoAtual)
            {
                vt2dDest = QVector2D(-(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - 1.5*globalConfig.robotDiameter), 0);
            }


             if(atbGoleiro.vt2dDestino.distanceToPoint(vt2dDest) > globalConfig.robotDiameter/2)
                 atbGoleiro.fTempoRobo = 0;

             acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setRobot(atbGoleiro);

            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setDestino(vt2dDest);
            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaVelocidadeMaxima(_fVelocidade);
            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaPontoAnguloDestino(blBola.vt2dPosicaoAtual);//(vt2dPontoAnguloRotacao(acAmbienteGoleiro));
            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaIgnorarBola(bIgnoraBola);

//            acAmbienteGoleiro->testeMiraChute.clear();
//            acAmbienteGoleiro->testeMiraChute.append(vt2dDest);

            // Quando a bola estiver dentro da area
            if( (blBola.vt2dPosicaoAtual.x() < -(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - acAmbienteGoleiro->iProfundidadeAreaPenalty)) && (qAbs(blBola.vt2dPosicaoAtual.y()) < acAmbienteGoleiro->iLarguraAreaPenalty/2) )
            {

//                atbGoleiro.cmd_rollerspd = 100; // Liga Roller
//                atbGoleiro.roller = true;       // Vel. Roller
                //acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->ligaRoller(100);
                if(clsAuxiliar::vt2dCalculaPosicaoPegarBolinha(acAmbienteGoleiro->vt2dBufferPosicaoBola, blBola.vt2dPosicaoAtual, blBola.vt2dPosicaoAnterior, 20) == blBola.vt2dPosicaoAtual
                        /*(qAbs(blBola.vt2dPosicaoAtual.y() - blBola.vt2dPosicaoAnterior.y()) < 25) || (qAbs(blBola.vt2dPosicaoAtual.x() - blBola.vt2dPosicaoAnterior.x()) < 25)*/)
                {

                    // Quando a posicao atual da bola em relacao a sua anterior variar menos que 9mm (pode ser mudado se necessario) atribuimos que ela esta parada
                    if(clsAuxiliar::vt2dCalculaPosicaoPegarBolinha(acAmbienteGoleiro->vt2dBufferPosicaoBola, blBola.vt2dPosicaoAtual, blBola.vt2dPosicaoAnterior, 20) == blBola.vt2dPosicaoAtual /*(qAbs(blBola.vt2dPosicaoAtual.y() - blBola.vt2dPosicaoAnterior.y()) < 9) && (qAbs(blBola.vt2dPosicaoAtual.x() - blBola.vt2dPosicaoAnterior.x()) < 9)*/)
                    {

                        // Se posiciona atras da bola antes de chutar
                        vt2dDest = blBola.vt2dPosicaoAtual;//clsAuxiliar::vt2dCalculaPontoCobranca(QVector2D(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2,0),blBola.vt2dPosicaoAtual,300, acAmbienteGoleiro->vt2dPegaTamanhoCampo());

                        if(atbGoleiro.vt2dDestino.distanceToPoint(vt2dDest) > globalConfig.robotDiameter/2)
                            atbGoleiro.fTempoRobo = 0;

                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setRobot(atbGoleiro);

                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaVelocidadeMaxima(_fVelocidade);
                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setDestino(vt2dDest);
                        acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaIgnorarBola(bIgnoraBola);

                        // Qaundo ele ja estiver atras da bola, ele se aproxima para chutar
                        if(atbGoleiro.vt2dPosicaoAtual.distanceToPoint(blBola.vt2dPosicaoAtual) < globalConfig.robotDiameter*2)
                        {


                            QVector2D vt2dPontoMira = blBola.vt2dPosicaoAtual;
                            float fDistanciaBola = globalConfig.robotDiameter;

                            if(clsAuxiliar::fCalculaDistancia(atbGoleiro.vt2dDestino, atbGoleiro.vt2dPosicaoAtual) <= globalConfig.robotDiameter)//Aproximação da bola
                            {
                                fDistanciaBola = clsAuxiliar::fCalculaDistancia(blBola.vt2dPosicaoAtual, atbGoleiro.vt2dPosicaoAtual) - globalConfig.robotDiameter;
                            }
//                            else //Irá chutar
//                            {
//                                fDistanciaBola = -globalConfig.ballDiameter*4;
//                            }

                            //                        vt2dPontoMira = clsAuxiliar::vt2dCalculaPontoCobranca(vt2dPontoMira, blBola.vt2dPosicaoAtual, fDistanciaBola);


                            if(atbGoleiro.vt2dDestino.distanceToPoint(vt2dDest) > globalConfig.robotDiameter/2)
                                atbGoleiro.fTempoRobo = 0;

                            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setRobot(atbGoleiro);

                            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setDestino(vt2dPontoMira);
                            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaVelocidadeMaxima(_fVelocidade);
                            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaPontoAnguloDestino(blBola.vt2dPosicaoAtual);
                            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->chutar(CHIP_KICK_STRONG,100,0,0);
                            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaIgnorarBola(bIgnoraBola);


                        }
                    }
                }
            }
//        }
        // Caso a bola esteja com algum robo adversario
//        else{

//            // o destino do robo sera o ponto pra onde o adversario mira
//            vt2dDest = bolarobo(acAmbienteGoleiro); //inimigo_ponto_futuro(acAmbienteGoleiro, ft);
//            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->setDestino(vt2dDest);
//            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaVelocidadeMaxima(_fVelocidade);
//            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaPontoAnguloDestino(vt2dPontoAnguloRotacao(acAmbienteGoleiro));
//            acAmbienteGoleiro->rbtRoboAliado[atbGoleiro.id]->vSetaIgnorarBola(bIgnoraBola);
//        }

    }
}


QVector2D clsGoleiro::vt2dPontoAnguloRotacao(AmbienteCampo *acAmbienteGoleiro){

    Atributos atbGoleiro = acAmbienteGoleiro->rbtRoboAliado[acAmbienteGoleiro->iIDGoleiro]->getRobot();
    Bola bola;
    acAmbienteGoleiro->vPegaBola(0, bola);
    QVector2D P;
    float px = acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - acAmbienteGoleiro->iPegaProfundidadeAreaPenalty();
    float py = acAmbienteGoleiro->iPegaLarguraAreaPenalty()/2;;
    int kpc = 1, kpb = 1;


    if(acAmbienteGoleiro->iLadoCampo == XNegativo)
        kpc = -1;
    if(bola.vt2dPosicaoAtual.y() < 0)
        kpb = -1;

        P = QVector2D(kpc*px,kpb*py);

    if(bola.vt2dPosicaoAtual.distanceToPoint(atbGoleiro.vt2dPosicaoAtual) < 3*globalConfig.robotDiameter){
        P = bola.vt2dPosicaoAtual;
    }

    return P;

}

QVector2D clsGoleiro::bolarobo(AmbienteCampo *acAmbienteGoleiro){

    Bola blBola;
    Atributos atbOponenteComBola = acAmbienteGoleiro->rbtRoboAdversario[iIdInimigoComBola(acAmbienteGoleiro)]->getRobot();
    acAmbienteGoleiro->vPegaBola(0, blBola);
    float fPontoXGoleiro, fPontoYGolero;

//    if(qAbs(blBola.vt2dPosicaoAtual) > (acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - acAmbienteGoleiro->iPegaProfundidadeAreaPenalty()))

    if(acAmbienteGoleiro->iLadoCampo == XPositivo){
        fPontoXGoleiro = (acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2) - 4*globalConfig.robotDiameter;       // posicao desejada do goleiro
    }else{
        fPontoXGoleiro = -((acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2) - 4*globalConfig.robotDiameter);    // posicao desejada do goleiro
    }

    // calculo do coeficiente angular da reta formada entre a bola e o robo, sem levar em consideracao o angulo do robo
    float coefang = (blBola.vt2dPosicaoAtual.y() - atbOponenteComBola.vt2dPosicaoAtual.y())/(blBola.vt2dPosicaoAtual.x() - atbOponenteComBola.vt2dPosicaoAtual.x());

    // dado coef. ang., o ponto do eixo y aonde o goleiro deve ficar
    fPontoYGolero = coefang*(fPontoXGoleiro - blBola.vt2dPosicaoAtual.x()) + blBola.vt2dPosicaoAtual.y();

    if(fPontoYGolero>(acAmbienteGoleiro->iPegaLarguraGol()/2)){
        fPontoYGolero = (acAmbienteGoleiro->iPegaLarguraGol()/2);
    }
    else if(fPontoYGolero<-(acAmbienteGoleiro->iPegaLarguraGol()/2)){
        fPontoYGolero = -(acAmbienteGoleiro->iPegaLarguraGol()/2);
    }

    return {fPontoXGoleiro,fPontoYGolero};

}

QVector2D clsGoleiro::ballDestiny2(AmbienteCampo *acAmbienteGoleiro){

    Bola blBola;
    acAmbienteGoleiro->vPegaBola(0, blBola);

//    QVector2D vt2dBA = acAmbienteGoleiro->vt2dBufferPosicaoBola.last();                                                            // atribui a posição anterior da bola para esta variavel
    QVector2D vt2dBP = acAmbienteGoleiro->vt2dBufferPosicaoBola.at(0);                                                              // atribui a posição atual da bola para esta variavel
    float fPontoXGoleiro, fPontoYGolero;                                                                         // ponto X e Y que queremos que a função nos dê

    if(acAmbienteGoleiro->iLadoCampo == XPositivo)
        fPontoXGoleiro = (acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2) - 1.5*globalConfig.robotDiameter;                  // posição fixa do goleiro no eixo X
    else
        fPontoXGoleiro = -((acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2) - 1.5*globalConfig.robotDiameter);               // posição fixa do goleiro no eixo X

    QVector2D vt2dPontoDefesa = clsAuxiliar::vt2dCalculaPosicaoPegarBolinha(acAmbienteGoleiro->vt2dBufferPosicaoBola,blBola.vt2dPosicaoAtual,blBola.vt2dPosicaoAnterior,60);
    //fPontoXGoleiro = vt2dPontoDefesa.x();

    fPontoYGolero = ((vt2dPontoDefesa.y() - vt2dBP.y())/(vt2dPontoDefesa.x() - vt2dBP.x()))*fPontoXGoleiro + ((vt2dPontoDefesa.x()*vt2dBP.y() - vt2dPontoDefesa.y()*vt2dBP.x())/(vt2dPontoDefesa.x() - vt2dBP.x()));

    if(vt2dPontoDefesa.distanceToPoint(vt2dBP) < globalConfig.ballDiameter/2)
        fPontoYGolero = vt2dBP.y();

    if(fPontoYGolero>(acAmbienteGoleiro->iPegaLarguraGol()/2 +globalConfig.robotDiameter)){                               // se o destino.y da bola for maior que a posição da trave em +y
        fPontoYGolero = (acAmbienteGoleiro->iPegaLarguraGol()/4);                                            // então, goleiro para na metade da distancia até a trave em +y
    }
    else if(fPontoYGolero<-(acAmbienteGoleiro->iPegaLarguraGol()/2) -globalConfig.robotDiameter){                         // se o destino.y da bola for maior que a posição da trave em -y
        fPontoYGolero = -(acAmbienteGoleiro->iPegaLarguraGol()/4);                                           // então, goleiro para na metade da distancia até a trave em -y
    }





//    if((qAbs(vt2dBA.x() - vt2dBP.x())> 5 || qAbs(vt2dBA.y() - vt2dBP.y())>5)
//            /*&& (blBola.vt2dPosicaoAtual.x() > blBola.vt2dPosicaoAnterior.x())*/){                                      // se a bola estiver indo p/ o gol e se ela não está parada
//        // então, goleiro segue a direção da bola
//        fPontoYGolero = ((vt2dBA.y() - vt2dBP.y())/(vt2dBA.x() - vt2dBP.x()))*fPontoXGoleiro + ((vt2dBA.x()*vt2dBP.y() - vt2dBA.y()*vt2dBP.x())/(vt2dBA.x() - vt2dBP.x()));

//        if(fPontoYGolero>(acAmbienteGoleiro->iPegaLarguraGol()/2 +globalConfig.robotDiameter)){                               // se o destino.y da bola for maior que a posição da trave em +y
//            fPontoYGolero = (acAmbienteGoleiro->iPegaLarguraGol()/4);                                            // então, goleiro para na metade da distancia até a trave em +y
//        }
//        else if(fPontoYGolero<-(acAmbienteGoleiro->iPegaLarguraGol()/2) -globalConfig.robotDiameter){                         // se o destino.y da bola for maior que a posição da trave em -y
//            fPontoYGolero = -(acAmbienteGoleiro->iPegaLarguraGol()/4);                                           // então, goleiro para na metade da distancia até a trave em -y
//        }

//    }else{                                                                                                       // se a bola não está indo p/ o gol
//        fPontoYGolero = vt2dBP.y();                                                                              // então, o goleiro "segue" sua posição em y

//        if(qAbs(vt2dBA.x() - vt2dBP.x()) < 5 && qAbs(vt2dBA.y() - vt2dBP.y()) < 5
//                /*&&(qAbs(blBola.vt2dPosicaoAtual.x())>acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2)*/){            // mas se ela está parada

//            if(fPontoYGolero>(acAmbienteGoleiro->iPegaLarguraGol()/2)){                                          // se a posição y da bola for maior que a posição da trave em +y
//                fPontoYGolero = (acAmbienteGoleiro->iPegaLarguraGol()/4);                                        // então, goleiro para na metade da distancia até a trave em +y
//            }
//            else if(fPontoYGolero<-(acAmbienteGoleiro->iPegaLarguraGol()/2)){                                    // se a posição y da bola for maior que a posição da trave em -y
//                fPontoYGolero = -(acAmbienteGoleiro->iPegaLarguraGol()/4);                                       // então, goleiro para na metade da distancia até a trave em -y
//            }
//        }
//        else if(fPontoYGolero>(acAmbienteGoleiro->iPegaLarguraGol()/2)){                                         // se a posição y da bola for maior que a posição da trave em +y
//            fPontoYGolero = (acAmbienteGoleiro->iPegaLarguraGol()/2);                                            // então, goleiro para na metade da distancia até a trave em +y
//        }
//        else if(fPontoYGolero<-(acAmbienteGoleiro->iPegaLarguraGol()/2)){                                        // se a posição y da bola for maior que a posição da trave em -y
//            fPontoYGolero = -(acAmbienteGoleiro->iPegaLarguraGol()/2);                                           // então, goleiro para na metade da distancia até a trave em -y
//        }

//    }


    return {fPontoXGoleiro,fPontoYGolero};

}

QVector2D clsGoleiro::inimigo_ponto_futuro(AmbienteCampo *acAmbienteGoleiro, float _fatorDePosicionamento){ // o fator vai ser quanto o goleiro ficara deslocado, em relacao ao diametro do robo

    Bola blBola;
    acAmbienteGoleiro->vPegaBola(0,blBola);
    Atributos atbOponenteComBola = acAmbienteGoleiro->rbtRoboAdversario[iIdInimigoComBola(acAmbienteGoleiro)]->getRobot();
    double TETAInimigo = (atbOponenteComBola.rotation);    // em rad
    float distToKeeper, fPontoXGoleiro, fPontoYGoleiro;    // declaracao das variaveis
    int kp = 1;                                            // constante p/ ajustar a troca de lado de campo

    if(acAmbienteGoleiro->iLadoCampo == XNegativo)
        kp = -1;

    // Caso o adversario esteja com a bola em um X depois da linha da area do penalty
    if(qAbs(blBola.vt2dPosicaoAtual.x()) > (acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - acAmbienteGoleiro->iPegaProfundidadeAreaPenalty())){

        // Fixamos o ponto Y
        if(blBola.vt2dPosicaoAtual.y() > 0){
            fPontoYGoleiro = acAmbienteGoleiro->iPegaLarguraGol()/2;
        }
        else if(blBola.vt2dPosicaoAtual.y() < 0){
            fPontoYGoleiro = -(acAmbienteGoleiro->iPegaLarguraGol()/2);
        }

        // E entao eh feito o calculo para achar a posicao em X
        fPontoXGoleiro = kp*(((fPontoYGoleiro - atbOponenteComBola.vt2dPosicaoAtual.y())/qTan(TETAInimigo)) + atbOponenteComBola.vt2dPosicaoAtual.x());

        // Se o ponto X resultar em um valor maior que o tamanho do campo (em X), ele fica meio diametro a frente da linha do fim do campo
        if(qAbs(fPontoXGoleiro) > kp*(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2)){
            fPontoXGoleiro = kp*(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - 0.5*globalConfig.robotDiameter);

        // Se o ponto X resultar em um valor menor que metade da area do penalty, ele fica deslocado 2 diametros da linha do fim de campo
        }else if(qAbs(fPontoXGoleiro) < kp*(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - acAmbienteGoleiro->iPegaProfundidadeAreaPenalty()/2)){
            fPontoXGoleiro = kp*(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - 2*globalConfig.robotDiameter);
        }

        return {fPontoXGoleiro,fPontoYGoleiro};
    }

    // ponto X vai ser a diferenca do campo total com fator multiplicado pelo diametro do robo
    fPontoXGoleiro = kp*(acAmbienteGoleiro->vt2dPegaTamanhoCampo().x()/2 - _fatorDePosicionamento*globalConfig.robotDiameter);

    // distancia do oponente com a bola ate o ponto X do goleiro
    distToKeeper = fPontoXGoleiro - atbOponenteComBola.vt2dPosicaoAtual.x();

    // calculo do ponto Y do goleiro
    fPontoYGoleiro = (distToKeeper*tan(TETAInimigo)) + atbOponenteComBola.vt2dPosicaoAtual.y();

    // condicoes pra quando a pos Y calculada for maior que a area do gol
    if(fPontoYGoleiro > acAmbienteGoleiro->iPegaLarguraGol()/2){
        fPontoYGoleiro = acAmbienteGoleiro->iPegaLarguraGol()/2;
    }
    else if(fPontoYGoleiro < -acAmbienteGoleiro->iPegaLarguraGol()/2){
        fPontoYGoleiro = -(acAmbienteGoleiro->iPegaLarguraGol()/2);
    }

    //     else if((atbOponenteComBola.rotation >= PI/2)||(atbOponenteComBola.rotation <= -PI/2)){
    //         fPontoYGolero = 0.0;
    //     }

    acAmbienteGoleiro->goleiroPonto = QVector2D(fPontoXGoleiro,fPontoYGoleiro);

    return {fPontoXGoleiro,fPontoYGoleiro};

 }

int clsGoleiro::iIdInimigoComBola(AmbienteCampo *acAmbienteGoleiro){

    Atributos tempInimigo;
    Bola bola;
    acAmbienteGoleiro->vPegaBola(0, bola);



    for(int i=0; i<PLAYERS_PER_SIDE; i++){

        tempInimigo = acAmbienteGoleiro->rbtRoboAdversario[i]->getRobot();

        if(tempInimigo.vt2dPosicaoAtual.distanceToPoint(bola.vt2dPosicaoAtual) < 2*globalConfig.robotDiameter ){

            return i;
        }
    }
            return (-1);
}

QVector2D clsGoleiro::aproximaBola(AmbienteCampo *acAmbienteGoleiro){

    Bola blBola;
    acAmbienteGoleiro->vPegaBola(0, blBola);

    QVector2D posicaobola = blBola.vt2dPosicaoAtual;

    return posicaobola;
}
