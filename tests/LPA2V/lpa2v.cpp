#include "lpa2v.h"

LPA2v::LPA2v()
{
    mi    = 0;
    lmb   = 0;
    mictr = 0;
    Gc    = 0;
    Gct   = 0;
    D     = 0;
    Gcr   = 0;
    mier  = 0;
    intEv = 0;
}

double LPA2v::NAP(double grEvFav, double grEvDFav)
{
    mi  = grEvFav;
    lmb = grEvDFav;

    mictr = grauCtrNorm(); // Calculo do Grau de Contradição normalizado
    intEv = intEvRes(); //Calculo do Intervalo de Evidência resultante
    Gc = grauCert();//Calculo do Grau de Certeza
    Gct = grauCtr();//Calculo do Grau de Contradição
    D = dist();//Calculo da distância D

    if(intEv <= 0.25 || D > 1.0) return 0.5;//Indefinição:fim

    Gcr = grauCertReal();//Determinação do Grau de Certeza real

    mier = grauEvResReal();//Calculo do Grau de Evidência resultante real

    return mier;

}

double LPA2v::normalizarVariavel(double var, double valorMaximo, bool negativo)
{
    if(negativo == true)
    {
        var = var + valorMaximo/2;
        var = var/valorMaximo;
    }
    else
    {
        var = var/valorMaximo;
    }
    return var;
}

double LPA2v::grauEvFav(double varProc, double setPoint)
{
    double EvFav=0;
    EvFav = varProc / setPoint;
    return EvFav;
}

double LPA2v::grauEvDFav(double grEvFav)
{
    double EvDFav=0;
    EvDFav = 1.0 - grEvFav;
    return EvDFav;
}

double LPA2v::grauEvDFav(double val1, double val2)
{
    double EvDFav = val1/val2;
    return EvDFav;
}

double LPA2v::grauCtrNorm()
{
    double GctNorm = (mi + lmb)/2.0;
    return GctNorm;
}

double LPA2v::intEvRes()
{
    double intEvRst=0;
    intEvRst = 1.0 - abs(2.0*mictr - 1.0);
    return intEvRst;
}

double LPA2v::grauCert()
{
    double GC=0;
    GC = mi - lmb;
    return GC;
}

double LPA2v::grauCtr()
{
    double Gct=0;
    Gct = (mi + lmb) - 1.0;
    return Gct;
}

double LPA2v::dist()
{
    double D=0;
    D = qSqrt( qPow((1.0 - abs(Gc)), 2) + qPow(Gct, 2) );
    return D;
}

double LPA2v::grauCertReal()
{
    double GCr=0;

    if(Gc >= 0)GCr = 1.0 - D;
    else if(Gc < 0)GCr = D - 1.0;

    return GCr;
}

double LPA2v::grauEvResReal()
{
    double GEvRReal=0;
    GEvRReal = (Gcr + 1)/2.0;
    return GEvRReal;
}
