#ifndef LPA2V_H
#define LPA2V_H

///
/// \file lpa2v.h
/// \brief \a LPA2v
/// \details Detalhes
///


#include <QtMath>

/*!
 \brief

*/
class LPA2v
{
public:
    /*!
     \brief

    */
    LPA2v();

    double mi, lmb, Gc, Gct, D, Gcr, mier, intEv, mictr; /*!< TODO: describe */

   /*!
    \brief Realiza o cálculo de um nó de análise paraconsistente para os graus
    de evidência favorável e desfavorável fornecidos

    \param grEvFav - Grau de evidência favorável normalizado
    \param grEvDFav - Grau de evidência desfavorável normalizado
    \return double - Grau de envidência favorável real
   */
   double NAP(double grEvFav, double grEvDFav);

   /*!
    \brief normalizarVariavel

    \param var
    \param valorMaximo
    \param negativo
    \return
   */
   double normalizarVariavel(double var, double valorMaximo, bool negativo = false);

private:
    /*!
     \brief

     \param varProc
     \param setPoint
     \return double
    */
    double grauEvFav(double varProc, double setPoint);
    /*!
     \brief

     \param grEvFav
     \return double
    */
    double grauEvDFav(double grEvFav);
    /*!
     \brief

     \param val1
     \param val2
     \return double
    */
    double grauEvDFav(double val1, double val2);
    /*!
     \brief

     \param grEvFav
     \param grEvDFav
     \return double
    */
    double grauCtrNorm();
    /*!
     \brief

     \param grCtrNorm
     \return double
    */
    double intEvRes();
    /*!
     \brief

     \param grEvFav
     \param grEvDFav
     \return double
    */
    double grauCert();
    /*!
     \brief

     \param grEvFav
     \param grEvDFav
     \return double
    */
    double grauCtr();
    /*!
     \brief

     \param Gc
     \param Gct
     \return double
    */
    double dist();
    /*!
     \brief

     \param Gc
     \param Dist
     \return double
    */
    double grauCertReal();
    /*!
     \brief

     \param Gcr
     \return double
    */
    double grauEvResReal();


};

#endif // LPA2V_H
