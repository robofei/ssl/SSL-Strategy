/*
 * SSL-Strategy
 * Copyright (C) 2020  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "mines.h"
#include <QPainter>

// Descomente a linha abaixo para gerar os mapas de calor do mines.
// #define MINES_GERAR_MAPAS_DE_CALOR

ParametrosPosicionamentoMines::ParametrosPosicionamentoMines()
    : id(-1), vt_vt2dPosicoesMinasAdicionais(), aliadoMines(false),
      adversarioMines(false), inicioY(0), finalY(0), inicioX(0), finalX(0),
      fortuna(), distanciaMinimaFortuna(0), distanciaMaximaFortuna(0),
      larguraAreaMina(0), raioMina(0)
{
}

Mines::Mines()
{
    for (qint8 i = 0; i < globalConfig.robotsPerTeam; i++)
    {
        roboAliado[i].id = i;
        roboAdversario[i].id = i;
    }

    libGlobalMaixma = L_00;
}

Mines::~Mines()
{
}

void Mines::vExecutaMines(const ParametrosPosicionamentoMines& parametros)
{
    idRobo = parametros.id;

    vAtribuicoesIniciais(parametros.aliadoMines, parametros.adversarioMines,
                         parametros.vt_vt2dPosicoesMinasAdicionais);

    vAtribuiLiberdadeDeCadaPonto(parametros);

#ifdef MINES_GERAR_MAPAS_DE_CALOR
    static int contador = 0;

    // Gera o mapa de calor da matriz a cada vez que o contador chegar a 100
    if (contador == 100)
    {
        vDesenhaHeatMap(parametros);
        contador = 0;
    }
    else
    {
        ++contador;
    }
#endif

    vAchaMelhorDestinoFinal();
    roboAliado[idRobo].vt2dPosicaoDestino =
        vt2dConverteMatrizParaCoordenadasCampo(vt2di16_coordenadasDestino);
}

void Mines::vAtribuicoesIniciais(
    const bool aliadoMines, const bool adversarioMines,
    const QVector<QVector2D>& vt_vt2dPosicoesMinasAdicionais)
{
    list_vt2di16_CoordenadasMinas.clear();
    list_vt2di16_coordenadasMaiorLiberdade.clear();
    libGlobalMaixma = L_00;

    if (aliadoMines)
    {
        list_vt2di16_CoordenadasMinas.reserve(globalConfig.robotsPerTeam);

        for (const RobotMines& n : roboAliado)
        {
            if (n.bRoboEmCampo && n.id != idRobo && n.id != idGoleiroAliado)
            {
                list_vt2di16_CoordenadasMinas.append(
                    vt2di16ConverteCoordenadasCampoParaMatriz(
                        n.vt2dPosicaoAtualRobo));
            }
        }
    }

    if (adversarioMines)
    {
        list_vt2di16_CoordenadasMinas.reserve(
            globalConfig.robotsPerTeam + list_vt2di16_CoordenadasMinas.size());

        for (RobotMines& n : roboAdversario)
        {
            if (n.bRoboEmCampo && n.id != idGoleiroAdversario)
            {
                list_vt2di16_CoordenadasMinas.append(
                    vt2di16ConverteCoordenadasCampoParaMatriz(
                        n.vt2dPosicaoAtualRobo));
            }
        }
    }

    list_vt2di16_CoordenadasMinas.reserve(
        vt_vt2dPosicoesMinasAdicionais.size() +
        list_vt2di16_CoordenadasMinas.size());

    for (const QVector2D& n : vt_vt2dPosicoesMinasAdicionais)
    {
        list_vt2di16_CoordenadasMinas.append(
            vt2di16ConverteCoordenadasCampoParaMatriz(n));
    }

    vt2di16_coordenadasRobo = vt2di16ConverteCoordenadasCampoParaMatriz(
        roboAliado[idRobo].vt2dPosicaoAtualRobo);
    vt2di16_coordenadasDestino = vt2di16_coordenadasRobo;
}

void Mines::vAtribuiLiberdadeDeCadaPonto(
    const ParametrosPosicionamentoMines& parametros)
{
    const qint16 iProfundidadeAreaMatriz =
        static_cast<qint16>(round(dEscalaMatriz * iProfundidadeAreaPenalty));
    const qint16 iLarguraAreaMatriz =
        static_cast<qint16>(round(dEscalaMatriz * iLarguraAreaPenalty));

    const double dDistanciaMinimaMatriz =
        dEscalaMatriz * parametros.distanciaMinimaFortuna;
    const double dDistanciaMaximaMatriz =
        dEscalaMatriz * parametros.distanciaMaximaFortuna;

    const double dEspessuraMinaMatriz =
        dEscalaMatriz * parametros.larguraAreaMina;
    const double dRaioMinaMatriz = dEscalaMatriz * parametros.raioMina;

    const ROBOVector2D_int16 fortunaMatriz =
        vt2di16ConverteCoordenadasCampoParaMatriz(parametros.fortuna);

    ROBOVector2D_int16 inicioFor = vt2di16ConverteCoordenadasCampoParaMatriz(
        QVector2D(parametros.inicioX, parametros.finalY));
    ROBOVector2D_int16 fimFor = vt2di16ConverteCoordenadasCampoParaMatriz(
        QVector2D(parametros.finalX, parametros.inicioY));

    if (inicioFor.x < 0)
        inicioFor.x = 0;
    if (inicioFor.y < 0)
        inicioFor.y = 0;

    if (fimFor.x < 0)
        fimFor.x = 0;
    if (fimFor.y < 0)
        fimFor.y = 0;

    if (inicioFor.x > colunasMatriz - 1)
        inicioFor.x = colunasMatriz - 1;
    if (inicioFor.y > linhasMatriz - 1)
        inicioFor.y = linhasMatriz - 1;

    if (fimFor.x > colunasMatriz - 1)
        fimFor.x = colunasMatriz - 1;
    if (fimFor.y > linhasMatriz - 1)
        fimFor.y = linhasMatriz - 1;

    for (qint16 i = 0; i < colunasMatriz - 1; i++)
    {
        for (qint16 j = 0; j < linhasMatriz - 1; j++)
        {
            if (i < inicioFor.x || i > fimFor.x || j < inicioFor.y ||
                j > fimFor.y)
            {
                // Zero a nota dessa posição e continua
                matrizPrincipal[i][j] = L_00;
                continue;
            }

            double dFatorFortuna = 0;
            qint8 liberdadeParcial = L_09;

            bool bX = ((i <= iProfundidadeAreaMatriz) ||
                       (i >= ((colunasMatriz - 1) - iProfundidadeAreaMatriz)));
            bool bY = ((j > ((linhasMatriz / 2) - (iLarguraAreaMatriz / 2))) &&
                       j < ((linhasMatriz / 2) + (iLarguraAreaMatriz / 2)));

            if (!(bX && bY)) // Verifica se o ponto está fora da área, pontos
                             // dentro da área são considerados nulos
            {
                float distanciaPontoFortuna =
                    fortunaMatriz.distanceToPoint(ROBOVector2D_int16(i, j));

                if (distanciaPontoFortuna < dDistanciaMinimaMatriz)
                    dFatorFortuna = 1;

                else if (distanciaPontoFortuna > dDistanciaMaximaMatriz)
                    dFatorFortuna = 0;

                else
                    dFatorFortuna = sqrt(
                        1 - distanciaPontoFortuna /
                                static_cast<double>(dDistanciaMaximaMatriz));
            }

            if (dFatorFortuna > .5 / 9)
            {
                for (const auto n : list_vt2di16_CoordenadasMinas)
                {

                    const double distancia =
                        n.distanceToPoint(ROBOVector2D_int16(i, j));

                    if (distancia >
                        (dRaioMinaMatriz + (8 * dEspessuraMinaMatriz)))
                        continue;

                    else if (distancia >
                             (dRaioMinaMatriz + (7 * dEspessuraMinaMatriz)))
                        liberdadeParcial -= L_01;

                    else if (distancia >
                             (dRaioMinaMatriz + (6 * dEspessuraMinaMatriz)))
                        liberdadeParcial -= L_02;

                    else if (distancia >
                             (dRaioMinaMatriz + (5 * dEspessuraMinaMatriz)))
                        liberdadeParcial -= L_03;

                    else if (distancia >
                             (dRaioMinaMatriz + (4 * dEspessuraMinaMatriz)))
                        liberdadeParcial -= L_04;

                    else if (distancia >
                             (dRaioMinaMatriz + (3 * dEspessuraMinaMatriz)))
                        liberdadeParcial -= L_05;

                    else if (distancia >
                             (dRaioMinaMatriz + (2 * dEspessuraMinaMatriz)))
                        liberdadeParcial -= L_06;

                    else if (distancia >
                             (dRaioMinaMatriz + (1 * dEspessuraMinaMatriz)))
                        liberdadeParcial -= L_07;

                    else if (distancia > dRaioMinaMatriz)
                        liberdadeParcial -= L_08;

                    else
                        liberdadeParcial -= L_09;

                    if (liberdadeParcial <= 0)
                    {
                        liberdadeParcial = 0;
                        break;
                    }
                }
            }
            liberdadeParcial =
                static_cast<qint8>(dFatorFortuna * liberdadeParcial + .5);

            LIBERDADE lib_global = static_cast<LIBERDADE>(liberdadeParcial);

            matrizPrincipal[i][j] = lib_global;

            if (lib_global == libGlobalMaixma)
            {
                list_vt2di16_coordenadasMaiorLiberdade.append(
                    ROBOVector2D_int16(i, j));
            }
            else if (lib_global > libGlobalMaixma)
            {
                list_vt2di16_coordenadasMaiorLiberdade.clear();
                libGlobalMaixma = lib_global;
                list_vt2di16_coordenadasMaiorLiberdade.append(
                    ROBOVector2D_int16(i, j));
            }
        }
    }
}

void Mines::vAchaMelhorDestinoFinal()
{
    if (libGlobalMaixma == L_00)
    {
        //        qDebug() << "Nehum ponto bom";
        return;
    }

    while (list_vt2di16_coordenadasMaiorLiberdade.size() > 0)
    {
        QVector<ROBOVector2D_int16> listAuxiliar;

        for (qint16 k = 0; k < list_vt2di16_coordenadasMaiorLiberdade.size();
             k++)
        {
            qint16 x_ = static_cast<qint16>(
                list_vt2di16_coordenadasMaiorLiberdade.at(k).x);
            qint16 y_ = static_cast<qint16>(
                list_vt2di16_coordenadasMaiorLiberdade.at(k).y);

            bool bExclui = false;

            for (qint16 i = x_ - 1; i <= x_ + 1; i++)
            {
                for (qint16 j = y_ - 1; j <= y_ + 1; j++)
                {
                    if (!((i >= 0 && i < (colunasMatriz - 1)) &&
                          (j >= 0 && j < (linhasMatriz - 1))))
                    {
                        bExclui = true;
                    }
                    else if (matrizPrincipal[i][j] != libGlobalMaixma)
                    {
                        bExclui = true;
                    }

                    if (bExclui)
                        break;
                }
                if (bExclui)
                    break;
            }
            if (bExclui)
            {
                list_vt2di16_coordenadasMaiorLiberdade.removeAt(k);
                listAuxiliar.append(ROBOVector2D_int16(x_, y_));
                k--;
            }
            if (list_vt2di16_coordenadasMaiorLiberdade.size() == 0)
            {
                vt2di16_coordenadasDestino = ROBOVector2D_int16(x_, y_);
                break;
            }
        }

        for (const auto n : listAuxiliar)
        {
            matrizPrincipal[static_cast<qint16>(n.x)]
                           [static_cast<qint16>(n.y)] =
                               static_cast<LIBERDADE>(libGlobalMaixma - 1);
        }
    }
}

void Mines::vAtualizaInformacoesAmbiente(
    const AmbienteCampo* acAuxiliar, const float distanciaSegurancaAreaPenalty)
{
    iProfundidadeAreaPenalty = acAuxiliar->geoCampo->szDefenseArea().width() +
                               distanciaSegurancaAreaPenalty;
    iLarguraAreaPenalty = acAuxiliar->geoCampo->szDefenseArea().height() +
                          2 * distanciaSegurancaAreaPenalty;
    iLadoCampo = acAuxiliar->allies->iGetSide();

    for (qint8 i = 0; i < globalConfig.robotsPerTeam; i++)
    {
        {
            roboAliado[i].vt2dPosicaoAtualRobo =
                acAuxiliar->allies->getPlayer(i)->vt2dPosicaoAtual();
            roboAliado[i].bRoboEmCampo =
                acAuxiliar->allies->getPlayer(i)->bRoboDetectado();
        }

        {
            roboAdversario[i].vt2dPosicaoAtualRobo =
                acAuxiliar->opponents->getPlayer(i)->vt2dPosicaoAtual();
            roboAdversario[i].bRoboEmCampo =
                acAuxiliar->opponents->getPlayer(i)->bRoboDetectado();
        }
    }

    idGoleiroAdversario = acAuxiliar->opponents->iGetGoalieID();
    idGoleiroAliado = acAuxiliar->allies->iGetGoalieID();

    // Reinicia a matriz se a geometria do campo mudar muito
    if (szCampo != acAuxiliar->geoCampo->szField())
    {
        szCampo = acAuxiliar->geoCampo->szField();
        vResetaMatrizPrincipal();
    }
}

QVector2D Mines::vt2dConverteMatrizParaCoordenadasCampo(
    ROBOVector2D_int16 posMatriz)
{
    return QVector2D(posMatriz.x / dEscalaMatriz - szCampo.width() / 2,
                     -(posMatriz.y / dEscalaMatriz - szCampo.height() / 2));
}

ROBOVector2D_int16 Mines::vt2di16ConverteCoordenadasCampoParaMatriz(
    QVector2D posCoordenadasCampo)
{
    return ROBOVector2D_int16(
        static_cast<qint16>((posCoordenadasCampo.x() + szCampo.width() / 2) *
                            dEscalaMatriz),
        static_cast<qint16>((-posCoordenadasCampo.y() + szCampo.height() / 2) *
                            dEscalaMatriz));
}

QVector2D Mines::vt2dPosicaoFinal(qint8 id) const
{
    return roboAliado[id].vt2dPosicaoDestino;
}

void Mines::vResetaMatrizPrincipal()
{

    colunasMatriz = static_cast<qint16>(szCampo.width() * dEscalaMatriz);
    linhasMatriz = static_cast<qint16>(szCampo.height() * dEscalaMatriz);

    matrizPrincipal.clear();
    matrizPrincipal = std::vector<std::vector<LIBERDADE>>(
        colunasMatriz, std::vector<LIBERDADE>(linhasMatriz));

    for (qint16 i = 0; i < colunasMatriz; i++)
    {
        for (qint16 j = 0; j < linhasMatriz; j++)
        {
            matrizPrincipal[i][j] = L_00;
        }
    }
}

void Mines::vDesenhaHeatMap(const ParametrosPosicionamentoMines& parametros)
{
    const int resolucao = 20;
    QScopedPointer<QPixmap> pm(new QPixmap((colunasMatriz - 1) * resolucao,
                                           (linhasMatriz - 1) * resolucao));
    QPainter painter(pm.get());

    QString escalaCores[10] = {"#ff3300", "#ff6600", "#ff9900", "#ffcc00",
                               "#ffff00", "#ccff00", "#99ff00", "#66ff00",
                               "#33ff00", "#00ff00"};

    // Desenha mapa de calor
    for (qint16 i = 0; i < colunasMatriz; i++)
    {
        for (qint16 j = 0; j < linhasMatriz; j++)
        {
            const QString& cor = escalaCores[matrizPrincipal[i][j]];

            painter.setPen(QPen(QColor(cor), 1));
            painter.setBrush(QBrush(QColor(cor)));
            painter.drawRect(i * resolucao, j * resolucao, resolucao,
                             resolucao);
        }
    }

    // Desenha linhas do campo
    painter.setPen(QPen(QColor(Qt::gray), resolucao, Qt::DashLine));
    painter.drawLine(0, 0, (colunasMatriz - 1) * resolucao, 0);
    painter.drawLine(0, 0, 0, (linhasMatriz - 1) * resolucao);

    painter.drawLine((colunasMatriz - 1) * resolucao,
                     (linhasMatriz - 1) * resolucao,
                     (colunasMatriz - 1) * resolucao, 0);
    painter.drawLine((colunasMatriz - 1) * resolucao,
                     (linhasMatriz - 1) * resolucao, 0,
                     (linhasMatriz - 1) * resolucao);

    painter.drawLine((colunasMatriz / 2) * resolucao, 0,
                     (colunasMatriz / 2) * resolucao,
                     (linhasMatriz - 1) * resolucao);

    painter.drawLine(
        0,
        (linhasMatriz / 2 + iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao,
        (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 + iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao);

    painter.drawLine(
        0,
        (linhasMatriz / 2 - iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao,
        (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 - iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao);

    painter.drawLine(
        (colunasMatriz - 1) * resolucao,
        (linhasMatriz / 2 + iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao,
        (colunasMatriz - 1) * resolucao -
            (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 + iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao);

    painter.drawLine(
        (colunasMatriz - 1) * resolucao,
        (linhasMatriz / 2 - iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao,
        (colunasMatriz - 1) * resolucao -
            (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 - iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao);

    painter.drawLine(
        (colunasMatriz - 1) * resolucao,
        (linhasMatriz / 2 + iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao,
        (colunasMatriz - 1) * resolucao -
            (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 + iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao);

    painter.drawLine(
        (colunasMatriz - 1) * resolucao,
        (linhasMatriz / 2 - iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao,
        (colunasMatriz - 1) * resolucao -
            (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 - iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao);

    painter.drawLine(
        (colunasMatriz - 1) * resolucao -
            (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 - iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao,
        (colunasMatriz - 1) * resolucao -
            (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 + iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao);

    painter.drawLine(
        (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 - iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao,
        (iProfundidadeAreaPenalty * dEscalaMatriz) * resolucao,
        (linhasMatriz / 2 + iLarguraAreaPenalty * dEscalaMatriz / 2) *
            resolucao);

    // Desenha os robôs aliados
    painter.setPen(QPen(QColor(Qt::gray), 3));
    painter.setBrush(Qt::black);

    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        // Ignora robôs que não estão em campo e o robô cujo destino está sendo
        // atribuído
        if (!roboAliado[i].bRoboEmCampo || i == parametros.id)
            continue;

        // posição atual do robô
        const ROBOVector2D_int16 n = vt2di16ConverteCoordenadasCampoParaMatriz(
            roboAliado[i].vt2dPosicaoAtualRobo);

        painter.drawEllipse(
            QPointF(n.x, n.y) * resolucao,
            globalConfig.robotDiameter * dEscalaMatriz * resolucao / 2,
            globalConfig.robotDiameter * dEscalaMatriz * resolucao / 2);
    }

    // Desenha o robô analisado
    painter.setPen(QPen(QColor(Qt::gray), 3));
    painter.setBrush(Qt::black);

    {
        const ROBOVector2D_int16 n = vt2di16ConverteCoordenadasCampoParaMatriz(
            roboAliado[parametros.id].vt2dPosicaoAtualRobo);

        painter.drawRect(
            QRectF(n.x * resolucao, n.y * resolucao,
                   globalConfig.robotDiameter * dEscalaMatriz * resolucao,
                   globalConfig.robotDiameter * dEscalaMatriz * resolucao));
    }

    // Desenha os robôs adversários
    painter.setPen(QPen(QColor(Qt::gray), 3));
    painter.setBrush(Qt::white);

    for (qint8 i = 0; i < globalConfig.robotsPerTeam; ++i)
    {
        if (!roboAdversario[i].bRoboEmCampo)
            continue;

        // posição atual do robô
        const ROBOVector2D_int16 n = vt2di16ConverteCoordenadasCampoParaMatriz(
            roboAdversario[i].vt2dPosicaoAtualRobo);

        painter.drawEllipse(
            QPointF(n.x, n.y) * resolucao,
            globalConfig.robotDiameter * dEscalaMatriz * resolucao / 2,
            globalConfig.robotDiameter * dEscalaMatriz * resolucao / 2);
    }

    // Desenhar a posição de todas as minas

    // As minhas ocupam uma área menor que o robô
    painter.setPen(QPen(QColor(Qt::cyan), 1));
    painter.setBrush(Qt::blue);

    for (const ROBOVector2D_int16 n : list_vt2di16_CoordenadasMinas)
    {
        painter.drawEllipse(
            QPointF(n.x, n.y) * resolucao,
            globalConfig.robotDiameter * dEscalaMatriz * resolucao / 4,
            globalConfig.robotDiameter * dEscalaMatriz * resolucao / 4);
    }

    // Salva o pixmap em um arquivo png
    pm->save(QDateTime::currentDateTime()
                 .toString(Qt::ISODateWithMs)
                 .replace(QChar(':'), QChar('_')) +
             "mines.png");
}
