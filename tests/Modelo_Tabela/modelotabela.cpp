#include "Modelo_Tabela/modelotabela.h"

clsModeloTabela::clsModeloTabela(QObject *parent)
    :QAbstractTableModel(parent)
{
    rows = 0;
    collums = 0;
}

int clsModeloTabela::rowCount(const QModelIndex & /*parent*/) const
{
   return rows;
}

int clsModeloTabela::columnCount(const QModelIndex & /*parent*/) const
{
    return collums;
}

QVariant clsModeloTabela::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
       return QStringLiteral("Row%1, Column%2")
                   .arg(index.row() + 1)
                   .arg(index.column() +1);
    }
    return QVariant();
}
