#ifndef MODELOTABELA_H
#define MODELOTABELA_H

#include <QAbstractTableModel>

class clsModeloTabela : public QAbstractTableModel
{
    Q_OBJECT

    int rows, collums;
public:
    clsModeloTabela(QObject *parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
};

#endif // MODELOTABELA_H
