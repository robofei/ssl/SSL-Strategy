#include "pso_ssl.h"

Enxame::Enxame( Parametros  _p, unsigned long qtdRobos , double _LIMX, double _LIMY)
{
    populacao.clear();
    for( unsigned int i = 0; i<_p.populacao-1; ++i)
    {
        Particula p;
        for( unsigned int y = 0; y<qtdRobos; ++y)
        {
            RoboPSO r;
            r.x = rand()%static_cast<int>(_LIMX);
            r.y = rand()%static_cast<int>(_LIMY);
            r.vx = 1.0;//r.x*0.1;//1.0; //(float)(rand()%100 - r.x)/2;
            r.vy = 1.0;//r.y*0.1;//1.0; //(float)(rand()%100 - r.y)/2;
            p.robos.push_back(r);
        }
        populacao.push_back(p);
    }
    return;
}


void Enxame::insereP( Particula  _p )
{
    populacao.push_back(_p);
    return;
}


void Enxame::setGbest(Particula _gbest)
{
    gbest = _gbest;
    return;
}
