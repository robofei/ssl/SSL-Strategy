///
/// \file pso_enxame.h
/// \brief \a Enxame
///

#include "pso_ssl.h"

/**
 * @brief
 *
 */
class Enxame{
   public:
   Particula gbest; /**< TODO: describe */
   std::vector <Particula> populacao; /**< TODO: describe */

   //Enxame(unsigned int qtdRobos = QTDR, unsigned int pop = POPULACAO);
   //Enxame( Particula p, unsigned int qtdRobos = QTDR);
   /**
    * @brief
    *
    * @param p
    * @param qtdRobos
    * @param _LIMX
    * @param _LIMY
    */
   Enxame(Parametros  p, unsigned long qtdRobos, double _LIMX, double _LIMY);

   /**
    * @brief
    *
    * @param _gbest
    */
   void setGbest(Particula _gbest);
   /**
    * @brief
    *
    * @param _p
    */
   void insereP( Particula  _p );
};
