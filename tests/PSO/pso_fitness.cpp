#include "pso_main.h"
#include "pso_fitness.h"

using namespace std;

pso_fitness::pso_fitness()
{

}


//double pso_fitness::cfitness(vector <RoboPSO> & _inimigos, vector <RoboPSO> & _amigos){
//    double fit;

//    sort(_inimigos.begin(), _inimigos.end(), pso_main::ordenaAng);
//    fit = fitness_verifica_reta_distancia_ignora(_inimigos, _amigos, _x, _y);
//    return fit;
//}

double pso_fitness::sfitness(vector <RoboPSO> & _inimigos,
                             vector <RoboPSO> & _amigos,
                             double __x, double __y,
                             RoboPSO R,
                             TP_FITNESS _fitness){
    double fit=DBL_MAX;

    sort(_inimigos.begin(), _inimigos.end(), pso_main::ordenaAng);
    if( _fitness == TP_FITNESS_VERIFICA_RETA){
        fit = fitness_verifica_reta_distancia_ignora(_inimigos, _amigos, __x, __y);
    }
    else if( _fitness == TP_FITNESS_VERIFICA_PASSE ){
        //qDebug() << "entrei fit" << endl;
        fit = fitness_passe(_inimigos, _amigos, R);
    }
    return fit;
}



double pso_fitness::fitness_colisao_amigos(vector <RoboPSO> amigos){
    double fit = 0.0;
    for( unsigned int i = 0; i<amigos.size(); ++i) {
        for( unsigned int j = 0; j<amigos.size(); ++j) {
            if( i!= j ) {
                if( abs(amigos[i].x - amigos[j].x) < 50 &&
                        abs(amigos[i].y - amigos[j].y) < 50 ) //colisao
                    fit += PSO_FITNESS_MID_HIGH_PENALIZATION;
            }
        }
    }
    return fit;
}

double pso_fitness::fitness_verifica_reta_distancia_ignora( vector <RoboPSO> inimigos, vector <RoboPSO> amigos, const double _x, const double _y ) {
    double fit = 0.0;
    double dist=0, dmin=0;

    bool protegido = false;

    //ignorar robôs distantes
    for( unsigned int i = 0; i<inimigos.size();++i){
        dist = pso_math::distancia_euclidiana(inimigos[i].x, inimigos[i].y, _x, _y);
        if( dist > 400 ) inimigos[i].considera=false;
    }

    for( unsigned int i = 0; i<inimigos.size(); ++i) {
        protegido = false;
        for(unsigned int j=0; j<amigos.size(); ++j) {
            dmin = 1.0-(40.0/pso_math::distancia_euclidiana(inimigos[i].x, inimigos[i].y, amigos[j].x, amigos[j].y));
            if( dmin < 0){
                dmin = PSO_FITNESS_MID_PENALIZATION;
            }
            fit += dmin;

            //fitness da reta
            if( (inimigos[i].considera && amigos[j].considera) || (amigos[j].considera && inimigos[i].bola )) {
                dist = pso_math::distancia_reta(_x,_y,inimigos[i].x, inimigos[i].y, amigos[j].x, amigos[j].y);
                if( dist <= 10) {
                    if(pso_math::estaNaReta(_x,_y,inimigos[i].x, inimigos[i].y, amigos[j].x, amigos[j].y)) {
                        protegido = true;
                        if( !inimigos[i].bola ) inimigos[i].considera = false;
                        if( !inimigos[i].bola ) amigos[j].considera = false;
                    }
                }
            }

            if( pso_math::distancia_euclidiana(inimigos[i].x, inimigos[i].y, _x, _y ) < 400){
               if( !protegido ) fit+= PSO_FITNESS_MID_PENALIZATION;
               if( inimigos[i].bola && !protegido) fit += PSO_FITNESS_HIGH_PENALIZATION;
            }
            else{
                if( !protegido ) fit+=PSO_FITNESS_LOW_PENALIZATION;
                if( inimigos[i].bola && !protegido) fit +=PSO_FITNESS_MID_PENALIZATION;
            }

        }
    }

//    //dist min entre nossos robos
//    for(unsigned int i=0; i<amigos.size(); ++i){
//        for(unsigned int j=0; j<amigos.size(); ++j){
//           dmin = 1.0-(40.0/distancia_euclidiana(amigos[i].x, amigos[i].y, amigos[j].x, amigos[j].y));
//           if( dmin < 0){
//               dmin = 1000.0;
//           }
//           fit += dmin;
//        }
//    }
    return fit;
}


double pso_fitness::fitness_passe( vector <RoboPSO> inimigos, vector <RoboPSO> amigos, RoboPSO R ) {
    double fit = 0.0;

    int r = iDistanciaReceptorCobranca/10;
    //pso_Circunferencia c(r, R.x, R.y);

    //descobre o jogador com a bola e cria um circulo para ele...
    //for(unsigned int i=0; i<amigos.size();++i){
    //    if( amigos[i].bola ){
    //        c.setaCircunferencia(r, amigos[i].x, amigos[i].y);
    //    }
    //}

    //verifica quantos robôs amigos estão fora do raio do robo com a bola R
    unsigned int robosACirc = 0;
    for(unsigned int i=0; i<amigos.size();++i){

        _eDentroForaCirculo e = pso_math::fCirculoDentroFora2(r, static_cast<int>(R.x),
                                                                 static_cast<int>(R.y),
                                                                 static_cast<int>(amigos[i].x),
                                                                 static_cast<int>(amigos[i].y));
                //pso_math::fCirculoDentroFora(c.x1,c.y1,c.x2, c.y2, c.x3, c.y3,
                //                   amigos[i].x, amigos[i].y);

        if( e == FORA ){

            ++robosACirc;
            double d=pso_math::distancia_euclidiana(R.x, R.y, amigos[i].x, amigos[i].y)-r;
            fit+=d/r;
            fit+=PSO_FITNESS_LOW_PENALIZATION;

        }
     }

    //verifica quantos robôs inimigos estão dentro do raio do robo com a bola R
//    unsigned int robosICirc = 0;
//    for(unsigned int i=0; i<inimigos.size();++i){

//        _eDentroForaCirculo e = pso_math::fCirculoDentroFora2(r, static_cast<int>(R.x),
//                                                                 static_cast<int>(R.y),
//                                                                 static_cast<int>(inimigos[i].x),
//                                                                 static_cast<int>(inimigos[i].y));
//               // pso_math::fCirculoDentroFora(c.x1,c.y1,c.x2, c.y2, c.x3, c.y3,
//               //                    inimigos[i].x, inimigos[i].y);

//        if( e != FORA ) ++robosICirc;
//    }


    //verifica quantos robos amigos estão com a visão "fechada" por um inimigo ou estão no raio do
    //amigo
    unsigned int robosABlock=0;
    unsigned int robosIRaio=0;
    for( unsigned int i=0; i<inimigos.size(); ++i){

       //raio será 1 aqui
       //pso_Circunferencia inimigo(0.5, inimigos[i].x, inimigos[i].y);
       for( unsigned int j=0; j<amigos.size();++j){
           if(pso_math::estaNaReta(R.x,R.y,inimigos[i].x, inimigos[i].y, amigos[j].x, amigos[j].y)){
                 ++robosABlock;
           }


           //se o robô está dentro do raio do inimigo

           _eDentroForaCirculo e = pso_math::fCirculoDentroFora2(100, static_cast<int>(inimigos[i].x),
                                                                    static_cast<int>(inimigos[i].y),
                                                                    static_cast<int>(amigos[j].x),
                                                                    static_cast<int>(amigos[j].y));
          if( e != FORA ) ++robosIRaio;

       }
    }


    //verifica se estão no próprio raio
    unsigned int robosARaio=0;

    for( unsigned int i=0; i<amigos.size(); ++i){


       for( unsigned int j=0; j<amigos.size();++j){

           //se o robô está dentro do raio do inimigo

           if( i!=j ){
              _eDentroForaCirculo e = pso_math::fCirculoDentroFora2(100, static_cast<int>(amigos[i].x),
                                                                         static_cast<int>(amigos[i].y),
                                                                        static_cast<int>(amigos[j].x),
                                                                        static_cast<int>(amigos[j].y));
              if( e != FORA ) ++robosARaio;
            }
       }
    }

   //qDebug() << "RobosRaio:" << robosARaio << endl;

    fit += (PSO_FITNESS_MID_PENALIZATION * robosABlock) +
          (PSO_FITNESS_MID_PENALIZATION * robosIRaio) +
          (PSO_FITNESS_MID_HIGH_PENALIZATION * robosARaio);



   /* for(unsigned int i = 0; i<inimigos.size(); ++i) {
        if( inimigos[i].considera ) {

            for(unsigned int j=0; j<amigos.size(); ++j) {

                double x = pso_math::distancia_euclidiana(inimigos[i].x, inimigos[i].y, amigos[j].x, amigos[j].y);
                double dmin = 1-(60/x);
                if( dmin < 0){
                    dmin = PSO_FITNESS_MID_PENALIZATION;
                }
                fit += dmin;
            }
        }
    }*/
    return fit;
}


double pso_fitness::fitness_defesa_bola( vector <RoboPSO> inimigos, vector <RoboPSO> amigos, RoboPSO R ) {
    double fit = 0.0;

    double r = 200;
    pso_Circunferencia c(r, R.x, R.y);

    //descobre o jogador com a bola e cria um circulo para ele...
    //for(unsigned int i=0; i<amigos.size();++i){
    //    if( amigos[i].bola ){
    //        c.setaCircunferencia(r, amigos[i].x, amigos[i].y);
    //    }
    //}

    //verifica quantos robôs amigos estão dentro do raio do robo com a bola R
    unsigned int robosACirc = 0;
    for(unsigned int i=0; i<amigos.size();++i){

        _eDentroForaCirculo e =
                pso_math::fCirculoDentroFora(c.x1,c.y1,c.x2, c.y2, c.x3, c.y3,
                                   amigos[i].x, amigos[i].y);

        if( e != DENTRO ) ++robosACirc;
    }

    //verifica quantos robôs amigos estão dentro do raio do robo com a bola R
    unsigned int robosICirc = 0;
    for(unsigned int i=0; i<inimigos.size();++i){

        _eDentroForaCirculo e =
                pso_math::fCirculoDentroFora(c.x1,c.y1,c.x2, c.y2, c.x3, c.y3,
                                   inimigos[i].x, inimigos[i].y);

        if( e != DENTRO ) ++robosICirc;
    }


    //verifica quantos robos amigos estão com a visão "fechada" por um inimigo
    unsigned int robosABlock=0;
    for( unsigned int i=0; i<inimigos.size(); ++i){
       for( unsigned int j=0; j<amigos.size();++j){
           if(pso_math::estaNaReta(R.x,R.y,inimigos[i].x, inimigos[i].y, amigos[j].x, amigos[j].y)){
                 ++robosABlock;
           }
       }
    }

    fit = (PSO_FITNESS_LOW_PENALIZATION * robosACirc) +
          (PSO_FITNESS_MID_PENALIZATION * robosABlock);



   /* for(unsigned int i = 0; i<inimigos.size(); ++i) {
        if( inimigos[i].considera ) {

            for(unsigned int j=0; j<amigos.size(); ++j) {

                double x = pso_math::distancia_euclidiana(inimigos[i].x, inimigos[i].y, amigos[j].x, amigos[j].y);
                double dmin = 1-(60/x);
                if( dmin < 0){
                    dmin = PSO_FITNESS_MID_PENALIZATION;
                }
                fit += dmin;
            }
        }
    }*/
    return fit;
}
