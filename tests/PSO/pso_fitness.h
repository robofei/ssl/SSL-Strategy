#ifndef PSO_FITNESS_H
#define PSO_FITNESS_H

///
/// \file pso_fitness.h
/// \brief \a pso_fitness
///

#include "pso_main.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"

#define PSO_FITNESS_LOW_PENALIZATION  100u
#define PSO_FITNESS_MID_LOW_PENALIZATION  500u
#define PSO_FITNESS_MID_PENALIZATION  1000u
#define PSO_FITNESS_MID_HIGH_PENALIZATION  2000u
#define PSO_FITNESS_HIGH_PENALIZATION 5000u


/**
 * @brief
 *
 */
class pso_fitness
{
public:
    /**
     * @brief
     *
     */
    pso_fitness();
    //double cfitness(vector <RoboPSO> & _inimigos, vector <RoboPSO> & _amigos);
    /**
     * @brief
     *
     * @param _inimigos
     * @param _amigos
     * @param __x
     * @param __y
     * @param R
     * @param _fitness
     * @return double
     */
    static double sfitness(std::vector <RoboPSO> & _inimigos,
                           std::vector <RoboPSO> & _amigos, double __x, double __y,
                           RoboPSO R, //robo amigo com a bola
                           TP_FITNESS _fitness);

//private:
    //double fitness(vector <RoboPSO> & _inimigos, vector <RoboPSO> & _amigos);
    /**
     * @brief
     *
     * @param amigos
     * @return double
     */
    static double fitness_colisao_amigos(std::vector <RoboPSO> amigos);
    /**
     * @brief
     *
     * @param amigos
     * @return double
     */
    static double fitness_dentro_gol(  std::vector <RoboPSO> amigos );
    /**
     * @brief
     *
     * @param inimigos
     * @param amigos
     * @param R
     * @return double
     */
    static double fitness_passe( std::vector <RoboPSO> inimigos, std::vector <RoboPSO> amigos, RoboPSO R  );
    /**
     * @brief
     *
     * @param inimigos
     * @param amigos
     * @param R
     * @return double
     */
    static double fitness_defesa_bola( std::vector <RoboPSO> inimigos, std::vector <RoboPSO> amigos, RoboPSO R  );
    /**
     * @brief
     *
     * @param inimigos
     * @param amigos
     * @param _x
     * @param _y
     * @return double
     */
    static double fitness_verifica_reta_distancia_ignora(std::vector <RoboPSO> inimigos,
                                                         std::vector <RoboPSO> amigos,
                                                         const double _x, const double _y );


};

#endif // PSO_FITNESS_H
