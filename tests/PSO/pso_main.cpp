#include "pso_main.h"

std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(0.0, 1.0);

using namespace std;

pso_main::pso_main(vector <RoboPSO> _inimigos, vector<RoboPSO> &_amigos, const AmbienteCampo *acEstrategia, bool &mudouDestino){
   QSize _szCampo = acEstrategia->szPegaTamanhoCampo()/10;
   QVector2D _vt2dPontoLivreGol = acEstrategia->tsDadosTeste.vt2dPontoProtecaoPSO;//(bola.vt2dPosicaoAtual + acEstrategia->vt2dPegaTamanhoCampo()/2)/10;

    Parametros _parametros;
    Particula _inicial;

    LIMX = static_cast<double>(_szCampo.width());
    LIMY = static_cast<double>(_szCampo.height());
    _x   = static_cast<double>(_vt2dPontoLivreGol.x());
    _y   = static_cast<double>(_vt2dPontoLivreGol.y());

    TP_FITNESS tpFitness = acEstrategia->fitRetornaFitnessPSO(); //retorna fitness

    RoboPSO comAbola;
    for(unsigned int i=0; i<_amigos.size(); ++i){
        if( tpFitness == TP_FITNESS_VERIFICA_RETA){
            _inicial.robos.push_back(_amigos[i]);
        } else if( tpFitness == TP_FITNESS_VERIFICA_PASSE ){
            if(_amigos[i].bola){
                comAbola = _amigos[i];
                //_amigos.erase(_amigos.begin()+i);
            }
            else{
                _inicial.robos.push_back(_amigos[i]);
            }
        }
    }

    //_parametros.

    Enxame enxame(_parametros, _inicial.robos.size(), LIMX, LIMY); //inicializa o enxame
    _inicial.fitness = pso_fitness::sfitness(_inimigos,_amigos, _x, _y, comAbola,tpFitness  );

    //calcula o pso

    double fgbest = DBL_MAX;

    //calcula o fitness de toda a população gerada
    for( unsigned int i = 0; i<enxame.populacao.size(); ++i) {
        enxame.populacao[i].fitness = pso_fitness::sfitness(_inimigos,enxame.populacao[i].robos, _x, _y, comAbola,tpFitness );
        enxame.populacao[i].pbest = enxame.populacao[i].robos;
        if( enxame.populacao[i].fitness < fgbest ) {
            fgbest = enxame.populacao[i].fitness;
            enxame.gbest = enxame.populacao[i];
            enxame.gbest.fitness = fgbest;
        }
    }

    for( unsigned int k = 0; k < _parametros.iteracao; ++k) {

        inercia(_parametros,k); //verifica o valor da inércia que será utilizado
        for( unsigned int i = 0; i< enxame.populacao.size(); ++i) {

            update_vel(_parametros , enxame.populacao[i], enxame.populacao[i], enxame.gbest);

            update_pos(enxame.populacao[i],enxame.populacao[i] );

            //double fit = pso_fitness::fitness(_inimigos,enxame.populacao[i].robos);
            double fit=pso_fitness::sfitness(_inimigos,enxame.populacao[i].robos, _x, _y, comAbola,tpFitness   );
            if( fit < enxame.populacao[i].fitness ) {
                enxame.populacao[i].fitness = fit;
                enxame.populacao[i].pbest = enxame.populacao[i].robos;
                if( fit < enxame.gbest.fitness) {
                    enxame.gbest = enxame.populacao[i];
                    enxame.gbest.fitness = fit;
                }
            }

        }
    }

    //tem que verificar o fitness da situação atual, se ela for menor que o retornado pelo
    //algoritmo pressupõem que o novo posicionamento é pior e não muda nada
    mudouDestino = false;
    //_inicial.fitness = pso_fitness::sfitness(_inimigos,_amigos, _x, _y, comAbola,tpFitness  );
    if( _inicial.fitness*0.7 > enxame.gbest.fitness){ //usa o novo posicionamento
        _amigos = enxame.gbest.robos;
        mudouDestino = true;
    }
}

//usado para simulacoes, não deve ser usado no sistema de estrategia
pso_main::pso_main(vector<RoboPSO> & _inimigos,
         vector<RoboPSO> & _amigos,
         Particula & _inicial,
         Enxame & enxame,
         Parametros & _parametros,
         unsigned int & iteracao, double __x, double __y, RoboPSO comAbola)
{

    //Parametros _parametros;
    //Particula _inicial;

    //for(unsigned int i=0; i<_amigos.size(); ++i){
    //   _inicial.robos.push_back(_amigos[i]);
    //}
    _x = __x;
    _y = __y;

    double fgbest = DBL_MAX;

    //calcula o fitness de toda a população gerada
    for( unsigned int i = 0; i<enxame.populacao.size(); ++i) {
        enxame.populacao[i].fitness = pso_fitness::sfitness(_inimigos,enxame.populacao[i].robos,
                                                            __x, __y, comAbola,TP_FITNESS_VERIFICA_PASSE );
        enxame.populacao[i].pbest = enxame.populacao[i].robos;
        if( enxame.populacao[i].fitness < fgbest ) {
            fgbest = enxame.populacao[i].fitness;
            enxame.gbest = enxame.populacao[i];
            enxame.gbest.fitness = fgbest;
        }
    }

    for( unsigned int k = 0; k < _parametros.iteracao; ++k) {

        inercia(_parametros,k); //verifica o valor da inércia que será utilizado
        for( unsigned int i = 0; i< enxame.populacao.size(); ++i) {

            update_vel(_parametros , enxame.populacao[i], enxame.populacao[i], enxame.gbest);

            update_pos(enxame.populacao[i],enxame.populacao[i] );
            //update_pos_bound(enxame.populacao[i],enxame.populacao[i], enxame.populacao[rand()%enxame.populacao.size()] );
            //vector <RoboPSO> r = enxame.gbest.robos;
            //update_pos_bound(enxame.populacao[i],enxame.populacao[i], r );

            double fit = pso_fitness::sfitness(_inimigos,enxame.populacao[i].robos, __x, __y, comAbola,TP_FITNESS_VERIFICA_PASSE);
            //qDebug() << "entrei" << endl;
            if( fit < enxame.populacao[i].fitness ) {
                enxame.populacao[i].fitness = fit;
                enxame.populacao[i].pbest = enxame.populacao[i].robos;
                if( fit < enxame.gbest.fitness) {
                    enxame.gbest = enxame.populacao[i];
                    enxame.gbest.fitness = fit;
                    iteracao = k;
                }
            }

        }
        //qDebug() << "SizeP:" << enxame.populacao.size() << endl;
    }

    //tem que verificar o fitness da situação atual, se ela for menor que o retornado pelo
    //algoritmo pressupõem que o novo posicionamento é pior e não muda nada
    _inicial.fitness = pso_fitness::sfitness(_inimigos,_amigos, __x, __y, comAbola,TP_FITNESS_VERIFICA_PASSE);

    if( _inicial.fitness > enxame.gbest.fitness){ //usa o novo posicionamento
        _amigos = enxame.gbest.robos;
    }
    return;
}


//update das velocidades da partícula
void pso_main::update_vel(Parametros _param, Particula & atual, Particula & proxima, Particula _gbest)
{
    double rho1 = dis(gen);
    double rho2 = dis(gen);
    //unsigned long g = gen();
    //unsigned long r = rd();

    for(unsigned int i=0; i<proxima.robos.size(); ++i)
    {


        //vx atual vira vx próxima
        proxima.robos[i].vx = (_param.inercia * atual.robos[i].vx) +
                              ((_param.c1 * rho1 * (atual.pbest[i].x - atual.robos[i].x)) +
                               (_param.c2 * rho2 * (_gbest.robos[i].x - atual.robos[i].x)));


        //proxima.robos[i].vx /= 10.0;
        //vy atualvira vy próxima
        proxima.robos[i].vy = (_param.inercia * atual.robos[i].vy) +
                              ((_param.c1 * rho1 * (atual.pbest[i].y - atual.robos[i].y)) +
                               (_param.c2 * rho2 * (_gbest.robos[i].y - atual.robos[i].y)));
       //proxima.robos[i].vy /= 10.0;
    }
    //qDebug() << "rd:" << r << " gen:" << g << " r1:" << rho1 << " r2:" << rho2;
    return;
}

//update da posição das partículas
void pso_main::update_pos(Particula & atual, Particula & proxima ) {
    double _x, _y;
    for( unsigned int i =0; i<proxima.robos.size(); ++i) {
        //p atual  vira p próxima
        _x = atual.robos[i].x;
        proxima.robos[i].x = atual.robos[i].x + proxima.robos[i].vx;

        _y = atual.robos[i].y;
        proxima.robos[i].y = atual.robos[i].y + proxima.robos[i].vy;


        if( proxima.robos[i].x < 0 || proxima.robos[i].x >= (LIMX-1.0) ) {
            proxima.robos[i].vx = 1; //*= 0; //-1;
            proxima.robos[i].x = _x;
            //proxima.robos[i].vy = 1; //*= 0; //-1;
            //proxima.robos[i].y = _y;
        }


        if( proxima.robos[i].y < 0 || proxima.robos[i].y >= (LIMY-1.0) ) {
           // proxima.robos[i].vx = 1; //*= 0; //-1;
           // proxima.robos[i].x = _x;
            proxima.robos[i].vy = 1; //*= 0; //-1;
            proxima.robos[i].y = _y;
        }

//        if( (proxima.robos[i].x < PENALTY_X || proxima.robos[i].x > LIMX-PENALTY_X) &&
//                proxima.robos[i].y > LIMY/2+PENALTY_Y/2 && proxima.robos[i].y < LIMY/2-PENALTY_Y/2){
//            proxima.robos[i].vx = 0; //*= 0; //-1;
//            proxima.robos[i].x = _x;
//            proxima.robos[i].vy = 0; //*= 0; //-1;
//            proxima.robos[i].y = _y;
//        }

    }
    return;
}

//update da posição das partículas
void pso_main::update_pos_bound(Particula & atual, Particula & proxima, vector<RoboPSO> robos ) {
    double _x, _y;//, _vx, _vy;
    //bool xpto=false;
    for( unsigned int i =0; i<proxima.robos.size(); ++i) {
        //p atual  vira p próxima
        //_x = atual.robos[i].x;
        _x = robos[i].x;
//        _vx = robos[i].vx;
        proxima.robos[i].x = atual.robos[i].x + proxima.robos[i].vx;

        //_y = atual.robos[i].y;
        _y = robos[i].y;
//        _vy = robos[i].vy;

        proxima.robos[i].y = atual.robos[i].y + proxima.robos[i].vy;

//        if( proxima.robos[i].x < 0 || proxima.robos[i].x > LIMX ) {
//            proxima.robos[i].vx *= -1;
//            proxima.robos[i].x = _x + proxima.robos[i].vx;
//            xpto=true;
//        }

//        if( proxima.robos[i].y < 0 || proxima.robos[i].y > LIMY ) {

//            proxima.robos[i].vy *= -1;
//            proxima.robos[i].y = _y + proxima.robos[i].vy;
//            xpto=true;
//        }



//        if( proxima.robos[i].x < 0 || proxima.robos[i].x > LIMX ) {
//            proxima.robos[i].vx = robos[i].vx; //0.5* ((aleatoria.robos[i].x - (proxima.robos[i].x > LIMX?LIMX:0))+proxima.robos[i].vx);
//            proxima.robos[i].x = robos[i].x; //_x;
//        }

//        if( proxima.robos[i].y < 0 || proxima.robos[i].y > LIMY ) {

//            proxima.robos[i].vy = robos[i].vy; //0.5* ((aleatoria.robos[i].y - (proxima.robos[i].y > LIMY?LIMY:0))+proxima.robos[i].vy);
//            proxima.robos[i].y = robos[i].y;
//        }

        if( proxima.robos[i].x < 0 || proxima.robos[i].x > LIMX ) {
            proxima.robos[i].vx = (proxima.robos[i].x > LIMX?-1:1);
            proxima.robos[i].x = _x;
        }

        if( proxima.robos[i].y < 0 || proxima.robos[i].y > LIMY ) {

            proxima.robos[i].vy = (proxima.robos[i].y > LIMY?-1:1);
            proxima.robos[i].y = _y;
        }


    }

//    if(xpto){
//        cout << "oi"<<endl;
//        //    proxima.robos = aleatoria.robos;
//    }

    return;
}


void pso_main::inercia( Parametros & _param, unsigned int _i){
   if( _param.tipo_inercia == CLERC2002){
      _param.inercia = 0.7298;
   }
   else if( _param.tipo_inercia == EBERHART2001B){
      _param.inercia = 0.5 + (dis(gen))/2;
   }
   else if( _param.tipo_inercia == XIN2009 ){
      _param.inercia = (0.5 * static_cast<double>(_param.iteracao - _i)/static_cast<double>(_param.iteracao)) + 0.4;
   }
   return;
}

vector <vector<int>> pso_main::retornaDestinos(vector<vector<double>> origens,
                                               vector<vector<double>> destinos)
{
    const int X = 0;
    const int Y = 1;
    vector <RobotCoords> robots;
    vector <vector<int>> retorno;
    //origens -> para destinos e origens -> origens (troca de robos)
    for( int i = 0; i<static_cast<int>(origens.size()); ++i)
    {
        //de 1 robo para todos os destinos
        RobotCoords r;
        r.id = i;
        r.x = origens[i][X];
        r.y = origens[i][Y];
        vector <double> v;
        double sum = 0;
        for(int j= 0; j<static_cast<int>(destinos.size()); ++j)
        {
            double d = pso_math::distancia_euclidiana(origens[i][X],origens[i][Y],
                                                      destinos[j][X],destinos[j][Y]);

            sum +=d;
            v.push_back(d);
        }
        r.sum = sum;
        r.distancias = v;
        robots.push_back(r);
    }

    while(retorno.size()<destinos.size()){
        sort(robots.begin(), robots.end(), _compareRobotsDistance);
        //int rid = ;
        auto min = min_element(robots[0].distancias.begin(),
                               robots[0].distancias.end());
        int dId = min - robots[0].distancias.begin();
        //cout << did << "   " << robots[0].distancias[ min - robots[0].distancias.begin() ] << endl;

        retorno.push_back({robots[0].id,dId});
        robots[0].sum = 0; //robots[0].distancias[dId] = DBL_MAX;
        for(int i = 0; i<static_cast<int>(robots.size());++i) robots[i].distancias[dId] = DBL_MAX;
    }
    return retorno;
}




