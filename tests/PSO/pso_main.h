#ifndef PSO_MAIN_H
#define PSO_MAIN_H

///
/// \file pso_main.h
/// \brief \a pso_main
///

#include "pso_ssl.h"
#include "Ambiente/futbolenvironment.h"
#include <QVector2D>
#include <QDebug>
/**
 * @brief
 *
 */
class pso_main
{
public:
    /**
     * @brief
     *
     * @param _inimigos
     * @param _amigos
     * @param acEstrategia
     * @param mudouDestino
     */
    pso_main(std::vector<RoboPSO> _inimigos, std::vector <RoboPSO> & _amigos, const AmbienteCampo *acEstrategia, bool &mudouDestino);
    /**
     * @brief
     *
     * @param _inimigos
     * @param _amigos
     * @param _inicial
     * @param enxame
     * @param _parametros
     * @param iteracao
     * @param __x
     * @param __y
     * @param comAbola
     */
    pso_main(std::vector<RoboPSO> & _inimigos,
             std::vector<RoboPSO> & _amigos,
             Particula &  _inicial,
             Enxame & enxame,
             Parametros & _parametros,
             unsigned int & iteracao, double __x, double __y, RoboPSO comAbola);

    /**
     * @brief
     *
     * @param _inimigos
     * @param _amigos
     * @param comAbola
     */
    pso_main(std::vector<RoboPSO> _inimigos, std::vector <RoboPSO> & _amigos, RoboPSO comAbola);

    // pso_main(vector<RoboPSO> _inimigos, vector <RoboPSO> & _amigos,
    //          QVector2D _vt2dTamanhoCampo, QVector2D _vt2dPontoLivreGol,
    //          QVector2D _vt2dTamanhoAreaPenalty,
    //          QVector2D _vt2dBola, QVector2D _vt2GolAdversario);
    //    pso_main(vector<RoboPSO> _inimigos, vector <RoboPSO> & _amigos, AmbienteCampo * acEstrategia);
    /**
     * @brief
     *
     * @param r1
     * @param r2
     * @return bool
     */
    static bool ordenaAng(RoboPSO r1, RoboPSO r2){ return r1.angulo > r2.angulo; }

    /**
     * @brief
     *
     * @param origens
     * @param destinos
     * @return vector<vector<int> >
     */
    static std::vector <std::vector<int>> retornaDestinos(std::vector<std::vector<double>> origens,
                                         std::vector<std::vector<double>> destinos);

public: // métodos estáticos
    /**
     * @brief
     *
     * @param _param
     * @param _i
     */
    static void inercia( Parametros & _param, unsigned int _i);
private:
    /**
     * @brief
     *
     * @param atual
     * @param proxima
     */
    void update_pos(Particula & atual, Particula & proxima );
    /**
     * @brief
     *
     * @param _param
     * @param atual
     * @param proxima
     * @param _gbest
     */
    void update_vel(Parametros _param, Particula & atual, Particula & proxima, Particula _gbest);
    double LIMX, LIMY; /**< TODO: describe */
    double PENALTY_X, PENALTY_Y; /**< TODO: describe */

    double _x, _y;//Ponto a ser bloqueado do gol /**< TODO: describe */


    /**
     * @brief
     *
     * @param atual
     * @param proxima
     * @param robos
     */
    void update_pos_bound(Particula & atual, Particula & proxima, std::vector<RoboPSO> robos );

    /**
     * @brief
     *
     */
    struct RobotCoords
    {
        int id; /**< TODO: describe */
        double x,y; /**< TODO: describe */
        double sum; /**< TODO: describe */
        std::vector <double> distancias; /**< TODO: describe */
    };


    /**
     * @brief
     *
     * @param a
     * @param b
     * @return bool
     */
    static bool _compareRobotsDistance( const RobotCoords & a, const RobotCoords & b){
       return a.sum > b.sum;
    }

};

#endif // PSO_MAIN_H
