#include "pso_ssl.h"
#include "pso_mathfunc.h"
using namespace std;

double pso_math::distancia_reta(const double _x, const double _y, //colocar ponto a se defendido (gol?)
                const double _xI, const double _yI, //robôs inimigos
                const double _xA, const double _yA){ //robôs amigos

    double vx = _xI - _x; //vetor v x
    double vy = _yI - _y; //vetor v y;

    double ux = _xA - _x; //vetor u x
    double uy = _yA - _y; //vetor u y

    double escalarUV = (vx * ux) + (vy * uy); //produto escalar U * V
    double quadradoV = (vx * vx) + (vy * vy); //produto escalar V * V quadrado

    double vLx = (escalarUV/quadradoV) * vx; //vetor v' x
    double vLy = (escalarUV/quadradoV) * vy; //vetor v' y

    double wX = ux - vLx; //vetor w x
    double wY = uy - vLy; //vetor w y

   return static_cast<double>(sqrt( pow(wX,2) + pow(wY,2) ));
}

bool pso_math::estaNaReta2(const double _px1, const double _py1, //colocar ponto a se defendido (gol?)
                const double _px2, const double _py2, //robôs inimigos
                const double _px3, const double _py3){ //robôs amigos


   return ((_py3 - _py2) * _px1 + (_px2 - _px3) * _py1 + (_px3 * _py2 - _px2 * _py3)) == 0.0;
}


bool pso_math::estaNaReta(const double _x, const double _y, //colocar ponto a se defendido (gol?)
                const double _xI, const double _yI, //robôs inimigos
                const double _xA, const double _yA){ //robôs amigos

    double vx = _xI - _x; //vetor v x
    double vy = _yI - _y; //vetor v y;

    double ux = _xA - _x; //vetor u x
    double uy = _yA - _y; //vetor u y

    double escalarUV = (vx * ux) + (vy * uy); //produto escalar U * V
    double quadradoV = (vx * vx) + (vy * vy); //produto escalar V * V quadrado

    double vLx = (escalarUV/quadradoV) * vx; //vetor v' x
    double vLy = (escalarUV/quadradoV) * vy; //vetor v' y

    double wX = ux - vLx; //vetor w x
    double wY = uy - vLy; //vetor w y


   double dist = sqrt( pow(wX,2) + pow(wY,2) );
   double aX = wX/dist;
   double aY = wY/dist;
   aX *= 10;
   aY *= 10;

   double PaX = aX + _x;
   double PaY = aY + _y;

   double PbX = _xI + aX;
   double PbY = _yI + aY;

   double PcX = _xI - aX;
   double PcY = _yI - aY;

   double Px = _xA - PbX;
   double Py = _yA - PbY;

   double P_PaPb = (Px * (PaX-PbX)) + (Py * (PaY-PbY));
   double PaPb_PaPb = (PaX-PbX)*(PaX-PbX) + (PaY-PbY)*(PaY-PbY);

   double P_PcPb = (Px * (PcX-PbX)) + (Py * (PcY-PbY));
   double PcPb_PcPb = (PcX-PbX)*(PcX-PbX) + (PcY-PbY)*(PcY-PbY);

    return ((P_PaPb >= 0 && P_PaPb < PaPb_PaPb) && (P_PcPb >= 0 && P_PcPb < PcPb_PcPb) ) ;
}


//retorna o centro de um circulo formado por 3 pontos
double pso_math::fCirculoCentroX(const double _x1, const double _y1, //primeiro ponto
                     const double _x2, const double _y2, //segundo ponto
                     const double _x3, const double _y3){ //terceiro ponto

    double x1 = (_x2 + _x1) / 2;
    double y1 = (_y2 + _y1) / 2;
    double dy1 = _x2 - _x1;
    double dx1 = -(_y2 - _y1);
    //***********************
    double x2 = (_x3 + _x2) / 2;
    double y2 = (_y3 + _y2) / 2;
    double dy2 = _x3 - _x2;
    double dx2 = -(_y3 - _y2);
    //****************************
    double ox = (y1 * dx1 * dx2 + x2 * dx1 * dy2 - x1 * dy1 * dx2 - y2 * dx1 * dx2)/ (dx1 * dy2 - dy1 * dx2);
    return ox;

}

double pso_math::fCirculoCentroY(const double _x1, const double _y1, //primeiro ponto
                     const double _x2, const double _y2, //segundo ponto
                     const double _x3, const double _y3){ //terceiro ponto

    double x1 = (_x2 + _x1) / 2;
    double y1 = (_y2 + _y1) / 2;
    double dy1 = _x2 - _x1;
    double dx1 = -(_y2 - _y1);
    //***********************
    double x2 = (_x3 + _x2) / 2;
    double y2 = (_y3 + _y2) / 2;
    double dy2 = _x3 - _x2;
    double dx2 = -(_y3 - _y2);
    //****************************
    double ox = (y1 * dx1 * dx2 + x2 * dx1 * dy2 - x1 * dy1 * dx2 - y2 * dx1 * dx2)/ (dx1 * dy2 - dy1 * dx2);
    double oy = (ox - x1) * dy1 / dx1 + y1;
    return oy;
}

double pso_math::fCirculoRaio(const double _x1, const double _y1, //primeiro ponto
                   const double _x2, const double _y2, //segundo ponto
                   const double _x3, const double _y3){ //terceiro ponto
    double m1 = (_y1 - _y2) / (_x1 - _x2);
    double m2 = (_y3 - _y2) / (_x3 - _x2);
    double c1 = ((m1 * m2 * (_y3 - _y1)) + (m1 * (_x2 + _x3)) - (m2 * (_x1 + _x2))) / (2* (m1 - m2));
    double c2 = ((((_x1 + _x2) / 2) - c1) / (-1 * m1)) + ((_y1 + _y2) / 2);
    return sqrt(((_x3 - c1) * (_x3 - c1)) + ((_y3 - c2) * (_y3 - c2)));
}

//enum _eDentroForaCirculo:int { DENTRO = 0, FORA = 1, EMCIMA = 2};
_eDentroForaCirculo pso_math::fCirculoDentroFora(const double _x1, const double _y1, //primeiro ponto
                                       const double _x2, const double _y2, //segundo ponto
                                       const double _x3, const double _y3, //terceiro ponto
                                       const double _x, const double _y){ //ponto a ser testado

    double m1 = (_y1 - _y2) / (_x1 - _x2);
    double m2 = (_y3 - _y2) / (_x3 - _x2);
    double c1 = ((m1 * m2 * (_y3 - _y1)) + (m1 * (_x2 + _x3)) - (m2 * (_x1 + _x2))) / (2* (m1 - m2));
    double c2 = ((((_x1 + _x2) / 2) - c1) / (-1 * m1)) + ((_y1 + _y2) / 2);
    double r = fCirculoRaio(_x1,_y1,_x2,_y2,_x3,_y3);
    double s = ((_x - c1) * (_x - c1)) + ((_y - c2) * (_y - c1)) - (r * r);

    if( s < 0.0) return DENTRO;
    else if( s > 0.0 ) return FORA;
    return EMCIMA;
}

_eDentroForaCirculo pso_math::fCirculoDentroFora2( int raio, int xCentral, int yCentral, int pX, int pY)
{

   double dpc = 0.0;

   //formula para calcular a distância entre o ponto P e o Centro da circunferência
   dpc = sqrt(pow((pX - xCentral), 2) + pow((pY - yCentral), 2));
   if( dpc > raio) return FORA;
   else if( dpc < raio) return DENTRO;
   return FORA;
}


int pso_math::newX( const double radius, const double h, const int k, int angle)
{
   return static_cast<int>(h + radius * cos(angle));
}

int pso_math::newY( const double radius, const int h, const double k, int angle)
{
   return static_cast<int>(k + radius * sin(angle));
}

_eEsquerdaDireita pso_math::fEsquerdaDireita(const double _xP, const double _yP, //ponto a ser testado
                               const double _xL1, const double _yL1, //pontos inicio/fim da linha
                               const double _xL2, const double _yL2){
   double s = (_yL2 - _yL1) * _xP + (_xL1 - _xL2) * _yP + (_xL2 * _yL1 - _xL1 * _yL2);
   if( s < 0.0 ){
      return ESQUERDA;
   }
   else if( s > 0.0){
      return DIREITA;
   }
   return NALINHA;
}

double pso_math::fPontoProximoX(const double _x1, const double _y1, //pontos inicio reta
                     const double _x2, const double _y2, //pontos final reta
                     const double _Px, const double _Py ){//ponto para verificar

  double APx = _Px - _x1;
  double APy = _Py - _y1;
  double ABx = _x2 - _x1;
  double ABy = _y2 - _y2;
  double magAB2 = ABx*ABx + ABy*ABy;
  double ABdotAP = ABx*APx + ABy*APy;
  double s = ABdotAP / magAB2;
  if( s < 0.0) return _x1;
  else if( s > 1.0) return _x2;
  return _x1+ABx*s;
}


double pso_math::fPontoProximoY(const double _x1, const double _y1, //pontos inicio reta
                     const double _x2, const double _y2, //pontos final reta
                     const double _Px, const double _Py ){//ponto para verificar

  double APx = _Px - _x1;
  double APy = _Py - _y1;
  double ABx = _x2 - _x1;
  double ABy = _y2 - _y2;
  double magAB2 = ABx*ABx + ABy*ABy;
  double ABdotAP = ABx*APx + ABy*APy;
  double s = ABdotAP / magAB2;
  if( s < 0.0) return _y1;
  else if( s > 1.0) return _y2;
  return _y1+ABx*s;
}

_eInterseccaoLinhas pso_math::fInterseccaoLinhas( const double _x1F, const double _y1F, //ponto 1 da 1 linha
                                         const double _x2F, const double _y2F, //ponto 2 da 1 linha
                                         const double _x1S, const double _y1S, //ponto 1 da 2 linha
                                         const double _x2S, const double _y2S ){ //ponto 2 da 2 linha

   double s1 = (_y2F - _y1F) * _x1S + (_x1F - _x2F) * _x2S + (_x2F * _y1F - _x1F * _y2F);
   double s2 = (_y2F - _y1F) * _y1S + (_x1F - _x2F) * _y2S + (_x2F * _y1F - _x1F * _y2F);

   if( s1 < 0.0 && s2 >= 0.0){
      return SIM;
   }
   else if( s1 < 0.0 && s2 <= 0.0){
      return NAO;
   }
   else if( s1 > 0.0 && s2 <= 0.0){
      return SIM;
   }
   else if( s1 > 0.0 && s2 > 0.0){
      return NAO;
   }
   return PROPRIA;
}

double pso_math::distancia_euclidiana( double px, double py, double qx, double qy)
{
    double distancia;
    distancia = static_cast<double>(sqrt( pow( px-qx, 2.0) + pow( py-qy, 2.0) ));
    return distancia;
}

double pso_math::calculo_angulo( double _pxA, double _pyA, double _pxB, double _pyB, double _pxC, double _pyC){
    //const long double M_PI=3.14159265;
    double _dAB = distancia_euclidiana(_pxA, _pyA, _pxB, _pyB); //c
    double _dAC = distancia_euclidiana(_pxA, _pyA, _pxC, _pyC); //b
    double _dBC = distancia_euclidiana(_pxB, _pyB, _pxC, _pyC); //a

    //cout << endl << _dAB <<  " " << _dAC <<  " " << _dBC << endl;

    //arco cos (a^2 - b^2 - c^2)/ (-2.b.c)
    double _angA = (acos( (pow(_dBC,2) - pow(_dAC,2) - pow(_dAB,2)) / (-2 * _dAC * _dAB ) )) * (180/M_PI);

    return _angA;
 }


