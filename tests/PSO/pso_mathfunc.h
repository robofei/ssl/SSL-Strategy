#ifndef PSO_MATHFUNC_H
#define PSO_MATHFUNC_H

///
/// \file pso_mathfunc.h
/// \brief \a pso_Circunferencia e \a pso_math
///

#include <cmath>

enum _eDentroForaCirculo:int { DENTRO = 0, FORA = 1, EMCIMA = 2};
enum _eEsquerdaDireita:int {ESQUERDA = 0, DIREITA = 1, NALINHA = 2};
enum _eInterseccaoLinhas:int { SIM = 0, NAO = 1, PROPRIA = 2};

class pso_Circunferencia{
public:
    int x1,y1;
    int x2,y2;
    int x3,y3;

    pso_Circunferencia( double r, double h, double k){
        double pontos = 3;
        double fatia = 2 * 3.1415926535897932 / pontos; //só preciso de 3 mesmo
        x1 = static_cast<int>(h + r * cos(fatia));
        y1 = static_cast<int>(k + r * sin(fatia));

        x2 = static_cast<int>(h + r * cos(fatia*2));
        y2 = static_cast<int>(k + r * sin(fatia*2));

        x3 = static_cast<int>(h + r * cos(fatia*3));
        y3 = static_cast<int>(k + r * sin(fatia*3));

    }

    pso_Circunferencia(){
        x1 = x2 = x3 = y1 = y2 =y3 =0;
    }

    void setaCircunferencia( double r, double h, double k){
        double pontos = 3;
        double fatia = 2 * 3.1415926535897932 / pontos; //só preciso de 3 mesmo
        x1 = static_cast<int>(h + r * cos(fatia));
        y1 = static_cast<int>(k + r * sin(fatia));

        x2 = static_cast<int>(h + r * cos(fatia*2));
        y2 = static_cast<int>(k + r * sin(fatia*2));

        x3 = static_cast<int>(h + r * cos(fatia*3));
        y3 = static_cast<int>(k + r * sin(fatia*3));

    }
};


class pso_math{

public:

    static double distancia_euclidiana( double px, double py, double qx, double qy);

    static double calculo_angulo( double _pxA, double _pyA, double _pxB, double _pyB, double _pxC, double _pyC);

    static double distancia_reta(const double _x, const double _y, //colocar ponto a se defendido (gol?)
                            const double _xI, const double _yI, //robôs inimigos
                            const double _xA, const double _yA); //robôs amigos


    static bool estaNaReta(const double _x, const double _y, //colocar ponto a se defendido (gol?)
                           const double _xI, const double _yI, //robôs inimigos
                           const double _xA, const double _yA); //robôs amigos

    bool estaNaReta2(const double _px1, const double _py1, //colocar ponto a se defendido (gol?)
                    const double _px2, const double _py2, //robôs inimigos
                    const double _px3, const double _py3);


    //retorna o centro de um circulo formado por 3 pontos
    static double fCirculoCentroX(const double _x1, const double _y1, //primeiro ponto
                                  const double _x2, const double _y2, //segundo ponto
                                  const double _x3, const double _y3); //terceiro ponto

    static double fCirculoCentroY(const double _x1, const double _y1, //primeiro ponto
                                  const double _x2, const double _y2, //segundo ponto
                                  const double _x3, const double _y3); //terceiro ponto

    static double fCirculoRaio(const double _x1, const double _y1, //primeiro ponto
                               const double _x2, const double _y2, //segundo ponto
                               const double _x3, const double _y3);


    static _eDentroForaCirculo fCirculoDentroFora(const double _x1, const double _y1, //primeiro ponto
                                                  const double _x2, const double _y2, //segundo ponto
                                                  const double _x3, const double _y3, //terceiro ponto
                                                  const double _x, const double _y);

    static _eDentroForaCirculo fCirculoDentroFora2( int raio, int xCentral, int yCentral, int pX, int pY);

    static int newX( const double radius, const double h, const int k, int angle);
    static int newY( const double radius, const int h, const double k, int angle);



    static _eEsquerdaDireita fEsquerdaDireita(const double _xP, const double _yP, //ponto a ser testado
                                              const double _xL1, const double _yL1, //pontos inicio/fim da linha
                                              const double _xL2, const double _yL2);


    static double fPontoProximoX(const double _x1, const double _y1, //pontos inicio reta
                                 const double _x2, const double _y2, //pontos final reta
                                 const double _Px, const double _Py );

    static double fPontoProximoY(const double _x1, const double _y1, //pontos inicio reta
                                 const double _x2, const double _y2, //pontos final reta
                                 const double _Px, const double _Py );


    static _eInterseccaoLinhas fInterseccaoLinhas( const double _x1F, const double _y1F, //ponto 1 da 1 linha
                                                   const double _x2F, const double _y2F, //ponto 2 da 1 linha
                                                   const double _x1S, const double _y1S, //ponto 1 da 2 linha
                                                   const double _x2S, const double _y2S );
};
#endif // PSO_MATHFUNC_H
