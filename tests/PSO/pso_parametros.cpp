#include "pso_ssl.h"
using namespace std;
Parametros::Parametros()
{
    c1 = 2; //10;
    c2 = 2; //10;
    tipo_inercia = EBERHART2001B;
    inercia = 0.7298; //10; //conforme clerc02
    iteracao = 300; //era 500, vamos deixar em 100 por questões de performance
    populacao = 100; //POPULACAO; era 200, vamos deixar em 50

}

void Parametros::imprime()
{
    cout << "c1:" << c1 <<endl;
    cout << "c2:" << c2 <<endl;
    cout << "inercia:" << inercia << endl;
    cout << "tipo_inercia:" << tipo_inercia << endl;
    cout << "iteracao:" << iteracao << endl;
    cout << "populacao:" << populacao << endl;
}

ostream &operator<<(ostream &output,Parametros & _p)
{
    output << "Classe Parametros:" << endl;
    output << "c1:" << _p.c1 <<endl;
    output << "c2:" << _p.c2 <<endl;
    output << "inercia:" << _p.inercia << endl;
    output << "tipo_inercia:" << _p.tipo_inercia << endl;
    output << "iteracao:" << _p.iteracao << endl;
    output << "populacao:" << _p.populacao << endl;
    return output;
}
