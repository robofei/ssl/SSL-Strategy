///
/// \file pso_parametros.h
/// \brief \a Parametros
/// \details Detalhes
///

#include "pso_ssl.h"

/**
 * @brief
 *
 */
enum TP_INERCIA {SIMULA = 1, CLERC2002 = 2, EBERHART2001B = 3, XIN2009 = 4};

/**
 * @brief
 *
 */
class Parametros{
   public:
      double inercia, r1, r2, c1, c2; /**< TODO: describe */
      unsigned long iteracao; /**< TODO: describe */
      unsigned int populacao; /**< TODO: describe */
      enum TP_INERCIA tipo_inercia; /**< TODO: describe */

      //uniform_real_distribution<double> r_x(-LIMX/2.0, LIMX/2);
      //uniform_real_distribution<double> r_y(-LIMY/2.0, LIMY/2);

      /**
       * @brief
       *
       */
      Parametros();
      /**
       * @brief
       *
       */
      void imprime();
      /**
       * @brief
       *
       * @param output
       * @param _p
       * @return ostream &operator
       */
      friend std::ostream &operator<<(std::ostream &output,Parametros & _p);
};
