#include "pso_ssl.h"

using namespace std;
void Particula::imprime()
{
    cout << "Robo:";
    for(unsigned int i=0; i<robos.size(); ++i)
    {
        robos[i].imprime();
    }
}

ostream &operator<<(ostream &output, Particula & _p)
{
   output << "Classe Particula:" << endl;
   output << "Fitness:" << _p.fitness << endl;

   for( unsigned int i=0; i < _p.robos.size(); ++i){
      output << "Robo:" << i << " " << _p.robos[i] << endl;
   }
   for( unsigned int i=0; i < _p.pbest.size(); ++i){
      output << "Pbest:" << i << " " << _p.pbest[i] << endl;
   }
   output << "";
   return output;
}
