///
/// \file pso_particula.h
/// \brief \a Particula
/// \details Detalhes
///

#include "pso_ssl.h"

/**
 * @brief
 *
 */
class Particula{
   public:
   std::vector <RoboPSO> robos; /**< TODO: describe */
   std::vector <RoboPSO> pbest; /**< TODO: describe */
   double fitness; /**< TODO: describe */
   /**
    * @brief
    *
    */
   void imprime();
   /**
    * @brief
    *
    * @param output
    * @param _p
    * @return ostream &operator
    */
   friend std::ostream &operator<<(std::ostream &output, Particula & _p);
};
