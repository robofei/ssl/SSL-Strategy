#include "pso_ssl.h"
using namespace std;

RoboPSO::RoboPSO()
{
    x = y = z = 0;
    vx = vy = vz = 1;
    considera = true;
    bola = false;
    angulo = 0;
}

RoboPSO::RoboPSO( double _x, double _y, double _z)
{
    x = _x;
    y = _y;
    z = _z;
    vx = vy = vz = 1;
    considera = true;
    bola = false;
    angulo = 0;
}

RoboPSO::RoboPSO( double _x, double _y)
{
    x = _x;
    y = _y;
    vx = vy = vz = 1;
    considera = true;
    bola = false;
    angulo = 0;
};

void RoboPSO::imprime()
{
    cout <<  "x[" << x << "] y[" << y << "] z[" << z << "] vx[" << vx << "] vy[" << vy << "] vz[" << vz << "]";
}

ostream &operator<<(ostream &output, RoboPSO & _r)
{
    output <<  "x[" << _r.x << "] y["
                    << _r.y << "] z["
                    << _r.z << "] vx["
                    << _r.vx << "] vy["
                    << _r.vy << "] vz["
                    << _r.vz << "]";
    return output;
}
