///
/// \file pso_robo.h
/// \brief \a RoboPSO
/// \details Detalhes
///

#include "pso_ssl.h"

/**
 * @brief
 *
 */
class RoboPSO{
   public:
      double x,y,z; /**< TODO: describe */
      double vx, vy, vz; /**< TODO: describe */
      bool considera; /**< TODO: describe */
      bool bola; /**< TODO: describe */
      double angulo; /**< TODO: describe */
      /**
       * @brief
       *
       */
      RoboPSO();
      /**
       * @brief
       *
       * @param _x
       * @param _y
       * @param _z
       */
      RoboPSO( double _x, double _y, double _z);
      /**
       * @brief
       *
       * @param _x
       * @param _y
       */
      RoboPSO( double _x, double _y);
      /**
       * @brief
       *
       */
      void imprime();
      /**
       * @brief
       *
       * @param output
       * @param _r
       * @return ostream &operator
       */
      friend std::ostream &operator<<(std::ostream &output, RoboPSO & _r);
};

