#ifndef _PSO_SSL
#define _PSO_SSL

///
/// \file pso_ssl.h
/// \brief Breve descrição...
/// \details Detalhes
///

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <random>
#include <chrono>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <cfloat>
#include<bits/stdc++.h>
#include <locale>

//#include <gsl/gsl_rng.h>


#define RADVS 1
#define REQUI 2
#define QTDR  7
//#define LIMX 1200
//#define LIMY 900
#define POPULACAO 100

#define PFX 1150
#define PFY 450

#define GXL (LIMX-PFX)
#define GYL (LIMY-PFY)

#define _DEBUG 1

/**
 * @brief
 *
 */
typedef std::chrono::high_resolution_clock Clock;

#include "pso_robo.h"
#include "pso_parametros.h"
#include "pso_particula.h"
#include "pso_enxame.h"
#include "pso_mathfunc.h"
#include "pso_fitness.h"


#endif // _PSO_SSL
