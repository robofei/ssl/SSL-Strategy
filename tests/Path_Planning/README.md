# Path-planners antigos

Aqui estão as classes para:

* A-Estrela
* RRT
* Mapa em grid

Estas classes foram as primeiras implementações feitas no software para os
path-planners e não são mais utilizadas.
