#include "path_p_a_star.h"

#include <cmath>

A_star::A_star(QVector2D _vt2dTamanhoCampo, bool _bHeuristica)
{
    vt2dTamanhoCampo = _vt2dTamanhoCampo;
    objMapa = new clsMapa(static_cast<int>(vt2dTamanhoCampo.x()),
                          static_cast<int>(vt2dTamanhoCampo.y()) );

    int width  = static_cast<int>(objMapa->vt2dTamanhoTotalMatrizMapa().x());
    int height = static_cast<int>(objMapa->vt2dTamanhoTotalMatrizMapa().y());

    mtMatrizAEstrela  = new Celula* [width];

    for(int i=0; i<width; i++)
        mtMatrizAEstrela[i] = new Celula[height];

    tmPathPlannerAEstrela = new QTime(QTime::currentTime());
    tmPathPlannerAEstrela->start();
    bHeuristica = _bHeuristica;
    bRoboNaAreaPenalty = false;

}

A_star::~A_star()
{
    int width  = static_cast<int>(objMapa->vt2dTamanhoTotalMatrizMapa().x());

    for(int i=0; i<width; i++)
        delete mtMatrizAEstrela[i];
    delete [] mtMatrizAEstrela;
}

void A_star::vAtualizaMapaPathPlanner(AmbienteCampo *ambiente)
{
    if(bCalculandoCaminho == false)
        objMapa->vAtualiza(ambiente);

}

inline bool operator==(Celula &_cel1, Celula &_cel2)
{
    if(_cel1.vt2dPosicao.x() == _cel2.vt2dPosicao.x() && _cel1.vt2dPosicao.y() == _cel2.vt2dPosicao.y() )
        return true;
                else return false;
}

/*
 * Cálculo da heurística Euclidiana
*/
float A_star::fCalculaHeuristicaEuclidiana(QVector2D _vt2dPrimeiroPonto, QVector2D _vt2dSegundoPonto)
{
    float dist = std::sqrt( (_vt2dPrimeiroPonto.x() - _vt2dSegundoPonto.x()) * (_vt2dPrimeiroPonto.x() - _vt2dSegundoPonto.x())
                     + (_vt2dPrimeiroPonto.y() - _vt2dSegundoPonto.y()) * (_vt2dPrimeiroPonto.y() - _vt2dSegundoPonto.y()) );
    return dist;
}

/*
 * Cálculo da heurística de Manhattan
*/
int A_star::iCalculaHeuristicaManhattan(QVector2D _vt2dPrimeiroPonto, QVector2D _vt2dSegundoPonto)
{
    int dist = qAbs(_vt2dPrimeiroPonto.x() - _vt2dSegundoPonto.x()) + qAbs(_vt2dPrimeiroPonto.y() - _vt2dSegundoPonto.y());
    return dist;
}

/*
 * Adiciona uma célula à closed list
*/
void A_star::vAdicionaClosedList(Celula& _cllCelula)
{
    cllClosedList.append(_cllCelula);
}

/*
 * Expande e calcula os valores de f(n) para todas as células adjacentes à célula atual
*/
void A_star::vCalculaCelulasVizinhas(Celula& _cllCelulaAtual, bool _bIgnorarBola, int _iRoboID, Celula **_matrizAEstrela)
{
    Celula tempCel;
    //tempCel.cllPai = new Celula;
    float custoG = 1;

    for(int i=-1; i < 2; ++i)//É necessário varrer as células ao redor da célula atual, por isso o indice i,j começa em -1
    {
        for(int j=-1; j < 2; ++j)
        {
            if(i == 0 && j == 0)continue;//Não é necessário analisar a pŕopria célula atual

            if(objMapa->bChecaObstaculo(QVector2D((int)_cllCelulaAtual.vt2dPosicao.x() + i, (int)_cllCelulaAtual.vt2dPosicao.y() + j), _iRoboID, _bIgnorarBola, bCoordenadaMapa) == true)continue;

            //utilizando a dist. euclidana temos que, D=1 para movimentos retilíneos e D=~1.4 para movimentos diagonais
            //justamente os valores de custo desejados
            custoG = _cllCelulaAtual.vt2dPosicao.distanceToPoint(QVector2D((int)_cllCelulaAtual.vt2dPosicao.x() + i ,(int)_cllCelulaAtual.vt2dPosicao.y() + j ));

            tempCel.vt2dPosicao = QVector2D((int)_cllCelulaAtual.vt2dPosicao.x() + i , (int)_cllCelulaAtual.vt2dPosicao.y() + j );
            tempCel.g   = _cllCelulaAtual.g + custoG;

            if(bHeuristica == Euclidiana)
                tempCel.h = vt2dDestino.distanceToPoint(tempCel.vt2dPosicao);

            else tempCel.h = iCalculaHeuristicaManhattan(vt2dDestino, tempCel.vt2dPosicao);

            tempCel.f = tempCel.g + tempCel.h;//f(n) = g(n) + h(n)

            if(_matrizAEstrela[(int)_cllCelulaAtual.vt2dPosicao.x() + i][(int)_cllCelulaAtual.vt2dPosicao.y() + j].isClosed == false)
            {
                if(_matrizAEstrela[(int)_cllCelulaAtual.vt2dPosicao.x() + i][(int)_cllCelulaAtual.vt2dPosicao.y() + j].isOpen == false /*||
                   _matrizAEstrela[(int)_cllCelulaAtual.pos.x() + i][(int)_cllCelulaAtual.pos.y() + j].f > tempCel.f*/ )
                {
                    tempCel.cllPai = new Celula;
                    *tempCel.cllPai = _cllCelulaAtual;

                    tempCel.isOpen = true;
                    tempCel.isClosed = false;
                   _matrizAEstrela[(int)_cllCelulaAtual.vt2dPosicao.x() + i][(int)_cllCelulaAtual.vt2dPosicao.y() + j] = tempCel;
                   cllOpenList.push(tempCel);
                }
            }

            if(tempCel.vt2dPosicao == vt2dDestino)
            {
                bTrajetoCalculado = true;
                return;
            }

        }
    }
}

/*
 * Calcula a sequencia dos pontos gerados na closed list para chegar ao destino
*/
QVector<QVector2D> A_star::vt2dOrganizaPontosCaminhoFinal(int _iRoboID, bool _bIgnorarBola, QVector<QVector2D> &_vt2dCaminhoSemFiltro)
{
    QVector<QVector2D> vt2dCaminhoFinal, vt2dCaminhoReduzido;
    QVector<Celula> temp;
//    QElapsedTimer etmAStar;

    //Utilizado para ajustar as coordenadas do mapa ao sistema do campo
    QVector2D vetorDeConversao(((vt2dTamanhoCampo.x() - iEspessuraLinha) / 2) + globalConfig.fieldOffset,
                               ((vt2dTamanhoCampo.y() - iEspessuraLinha) / 2) + globalConfig.fieldOffset);

    temp.clear();
    vt2dCaminhoFinal.clear();
    vt2dCaminhoReduzido.clear();
    fComprimentoCaminho = 0;

    temp.append(cllClosedList[cllClosedList.size()-1]);
    vt2dCaminhoFinal.prepend(temp.last().vt2dPosicao);

    while(temp.last().vt2dPosicao != cllCelulaInicial.vt2dPosicao)
    {
        temp.append(*temp.last().cllPai);
        vt2dCaminhoFinal.prepend(temp.last().vt2dPosicao);
    }

    _vt2dCaminhoSemFiltro.clear();
    for(int n=0; n < vt2dCaminhoFinal.size(); ++n)//Para retornar o caminho sem tratamento
    {
        QVector2D vt2dAuxiliar = vt2dCaminhoFinal[n];
        vt2dAuxiliar.setX((vt2dAuxiliar.x() * fResolucaoCampo) - vetorDeConversao.x());
        vt2dAuxiliar.setY((vt2dAuxiliar.y() * fResolucaoCampo) - vetorDeConversao.y());
        _vt2dCaminhoSemFiltro.append(vt2dAuxiliar);
    }

//    return vt2dCaminhoFinal;//****************************************************************8

    if(!vt2dCaminhoFinal.isEmpty())
    {
        vt2dCaminhoReduzido.append(vt2dCaminhoFinal.last());

        int n = vt2dCaminhoFinal.size()-1;
        int i = 0;
        //QVector2D vt2dPontoMedio;

//        etmAStar.start();

        while(n > 0 )//Reduz os pontos do caminho final
        {

            while(objMapa->bChecaInterseccaoLinha(vt2dCaminhoFinal[n], vt2dCaminhoFinal[i], _iRoboID, _bIgnorarBola, bCoordenadaMapa, globalConfig.safety.distance/*raioSegurancaRobo*1.5*/) == true)
            {
                ++i;
            }


            //vt2dPontoMedio = (vt2dCaminhoFinal[n] + vt2dCaminhoFinal[i])/2;
            //vt2dCaminhoReduzido.prepend(vt2dPontoMedio);
            //vt2dCaminhoReduzido.prepend(vt2dCaminhoFinal[i]);

            vInserePontos(vt2dCaminhoFinal[n], vt2dCaminhoFinal[i], vt2dCaminhoReduzido);
            if(n < i)n = i;
            else
                n = i-1;
            i = 0;

            if(vt2dCaminhoReduzido.size() >= vt2dCaminhoFinal.size())
                break;
        }

        for(int j=0; j < vt2dCaminhoReduzido.size()-1; ++j)//Calcula o comprimento do caminho
        {
            fComprimentoCaminho += vt2dCaminhoReduzido[j].distanceToPoint(vt2dCaminhoReduzido[j+1]);
        }


        for(int n=0; n < vt2dCaminhoReduzido.size(); ++n)//Converte o caminho gerado para o tamanho real
        {
            QVector2D vt2dAuxiliar = vt2dCaminhoReduzido[n];
            vt2dAuxiliar.setX((vt2dAuxiliar.x() * fResolucaoCampo) - vetorDeConversao.x());
            vt2dAuxiliar.setY((vt2dAuxiliar.y() * fResolucaoCampo) - vetorDeConversao.y());
            vt2dCaminhoReduzido.replace(n, vt2dAuxiliar);
        }

    }
    return vt2dCaminhoReduzido;
}
/*
 * Insere uma certa quantidade de pontos entre o ponto final e inicial a fim de manter o robô no caminho
*/
void A_star::vInserePontos(QVector2D vt2dPontoInicial, QVector2D vt2dPontoFinal, QVector<QVector2D> &vt2dSaidaPontos)
{
    QVector<QVector2D> vt2dPontos;
    vt2dPontos.clear();
    vt2dPontos.append(vt2dPontoInicial);

    int iEspacoPontos = qFloor(250/fResolucaoCampo); //200mm de espaço entre cada ponto
    int nPontos = qFloor(static_cast<double>((vt2dPontoFinal-vt2dPontoInicial).length())/iEspacoPontos);
    QVector2D vt2dAux = vt2dPontoFinal - vt2dPontoInicial;
    vt2dAux.normalize();

    while(nPontos > 0)
    {
        vt2dPontos.append(vt2dPontos.last()+vt2dAux);
        nPontos--;
    }

    vt2dPontos.append(vt2dPontoFinal);


    foreach(QVector2D point, vt2dPontos)
    {
        vt2dSaidaPontos.prepend(point);
    }

}

/*
 * Verifica se existe algum obstáculo na posição destino, caso exista, adapta a posição do destino para que não exista um obstáculo
*/
QVector2D A_star::vt2dChecaObstaculoDestino(QVector2D _vt2dPosicaoTeste, Celula& _cllCelulaInicial, bool _bIgnorarBola, int _iRoboID)
{
    iNumeroDeChamadasFuncao++;
    if(iNumeroDeChamadasFuncao > iNumeroMaximoChamadasAjustaDestino)// Proteção para a função recursiva, faz com que ela termine caso o numero de chamadas exceda um certo limite
        return _cllCelulaInicial.vt2dPosicao;

    QVector2D tamanhoMapa = objMapa->vt2dTamanhoTotalMatrizMapa();
    QVector2D posCelulaFinal = _vt2dPosicaoTeste;

    int diametroRobo = qCeil(globalConfig.robotDiameter / fResolucaoCampo);

    if(iCalculaHeuristicaManhattan(cllCelulaInicial.vt2dPosicao, _vt2dPosicaoTeste) <= 2)
        return _vt2dPosicaoTeste;

    //Caso existe algum obstáculo na célula destino, a posição destino será deslocada para perto da célula inicial até não existir mais
    //risco de colisão
    for(int i=0; i < diametroRobo; ++i)
    {
        for(int j=0; j < diametroRobo; ++j)
        {

            if(bChecaLimite(posCelulaFinal, tamanhoMapa)/*_celulaTeste.x() + i >= tamanhoCam.x() || _celulaTeste.y() + j >= tamanhoCam.y()*/)
            {
                if( (int)posCelulaFinal.x() > (int)_cllCelulaInicial.vt2dPosicao.x())
                    posCelulaFinal.setX( posCelulaFinal.x() - 1 );
                else
                    posCelulaFinal.setX( posCelulaFinal.x() + 1 );

                if((int)posCelulaFinal.y() > (int)_cllCelulaInicial.vt2dPosicao.y())
                    posCelulaFinal.setY( posCelulaFinal.y() - 1 );
                else
                    posCelulaFinal.setY( posCelulaFinal.y() + 1 );
                posCelulaFinal = vt2dChecaObstaculoDestino(posCelulaFinal, _cllCelulaInicial, _bIgnorarBola, _iRoboID);
            }


            if(objMapa->bChecaObstaculo(QVector2D(posCelulaFinal.x() + i, posCelulaFinal.y() + j), _iRoboID, _bIgnorarBola, bCoordenadaMapa) == true)
            {
                if( (int)posCelulaFinal.x() > (int)_cllCelulaInicial.vt2dPosicao.x())
                    posCelulaFinal.setX( posCelulaFinal.x() - 1 );
                else
                    posCelulaFinal.setX( posCelulaFinal.x() + 1 );

                if((int)posCelulaFinal.y() > (int)_cllCelulaInicial.vt2dPosicao.y())
                    posCelulaFinal.setY( posCelulaFinal.y() - 1 );
                else
                    posCelulaFinal.setY( posCelulaFinal.y() + 1 );
                posCelulaFinal = vt2dChecaObstaculoDestino(posCelulaFinal, _cllCelulaInicial, _bIgnorarBola, _iRoboID);
            }

        }
    }
    return posCelulaFinal;
}

/*
 * Verifica se alguma célula ao redor da célula teste está fora dos limites do mapa
*/
bool A_star::bChecaLimite(QVector2D _vt2dPosicaoTeste, QVector2D _vt2dTamanhoMapa)
{

   for(int i=-1; i < 2; ++i)
   {
       for(int j=-1; j < 2; ++j)
       {
           if(_vt2dPosicaoTeste.x() + i >= _vt2dTamanhoMapa.x() || _vt2dPosicaoTeste.y() + j >= _vt2dTamanhoMapa.y())return true;
       }
   }

   return false;
}

/*
 * Função principal, onde é feito o calculo do trajeto
*/
QVector<QVector2D> A_star::vt2dCalculaTrajeto(AmbienteCampo *_acAmbientePathPlanner,int _iRoboID, bool _bIgnorarBola, int *_iCelulasExpandidas, QVector<QVector2D> *_vt2dCaminhoOriginal)
{
    QVector2D _vt2dDestino, _vt2dPosicaoInicial;
    QVector<QVector2D> _vt2dCaminhoAnterior;
    _vt2dCaminhoAnterior.clear();

    float fFatorHeuristica = 0;

    bRoboNaAreaPenalty = _acAmbientePathPlanner->bRoboAreaDefesa(_iRoboID);

    _vt2dDestino = _acAmbientePathPlanner->rbtRoboAliado[_iRoboID]->vt2dDestinoRobo();
    _vt2dPosicaoInicial = _acAmbientePathPlanner->rbtRoboAliado[_iRoboID]->vt2dPosicaoAtualRobo();

    //Se esta muito perto, retorna o destino direto
    if( _vt2dDestino.distanceToPoint(_vt2dPosicaoInicial) < globalConfig.robotDiameter/2)
    {
        _vt2dCaminhoAnterior.append(_vt2dDestino);
        _acAmbientePathPlanner->bCaminhoAlterado[_iRoboID] = true;
        return _vt2dCaminhoAnterior;
    }

    _vt2dCaminhoAnterior.append(_acAmbientePathPlanner->vt2dCaminhosPathPlanner[_iRoboID]);



    QElapsedTimer etmAstar;
    etmAstar.start();

    //Checa se o caminho deve ser recalculado
    if(_vt2dCaminhoOriginal == nullptr)
    {
        bool bRecalcularCaminho = false;
        //Analisa o caminho gerado anteriormente, caso o mesmo ainda seja utilizavel nao sera calculado um novo
        if(!_vt2dCaminhoAnterior.isEmpty())
        {
//            if(objMapa->bChecaInterseccaoLinha(_vt2dCaminhoAnterior.first(), _vt2dPosicaoInicial, _iRoboID, _bIgnorarBola, bCoordenadaReal, globalConfig.safety.distance) == true)
//                bRecalcularCaminho = true;
//            else if(_vt2dCaminhoAnterior.last().distanceToPoint(_vt2dDestino) > globalConfig.robotDiameter)
//                bRecalcularCaminho = true;

            if(_vt2dCaminhoAnterior.size() >= 2)
            {

                for(int n=0; n < _vt2dCaminhoAnterior.size() - 1; ++n)
                {
                    if(objMapa->bChecaInterseccaoLinha(_vt2dCaminhoAnterior[n], _vt2dCaminhoAnterior[n+1], _iRoboID, _bIgnorarBola, bCoordenadaReal, globalConfig.safety.distance*0.85) == true)
                    {
                        bRecalcularCaminho = true;
                        break;
                    }

                }

            }

            else /*if(_vt2dCaminhoAnterior.size() > 0 )*/
            {
                if(objMapa->bChecaInterseccaoLinha(_vt2dCaminhoAnterior.last(), _vt2dPosicaoInicial, _iRoboID, _bIgnorarBola, bCoordenadaReal, globalConfig.safety.distance*0.85) == true)
                {
                    bRecalcularCaminho = true;
                }
            }


            //Caso o destino nao tenha variado mais de um diâmetro do robô não é necessário calcular um novo caminho
            if(_vt2dCaminhoAnterior.last().distanceToPoint(_vt2dDestino) > 130)
            {
                bRecalcularCaminho = true;
            }
        }
        else // Caminho anterior nulo
        {
            bRecalcularCaminho = true;
        }

        if(bRecalcularCaminho == false)
        {
            //_vt2dCaminhoAnterior.append(_vt2dPosicaoInicial);
            if(_vt2dCaminhoAnterior.last().distanceToPoint(_vt2dDestino) < 130)
            {
                _vt2dCaminhoAnterior.clear();
                _vt2dCaminhoAnterior.append(_vt2dDestino);
            }
            return _vt2dCaminhoAnterior;
        }
    }


    _acAmbientePathPlanner->bCaminhoAlterado[_iRoboID] = true; //O caminho será alterado

    QVector2D tamanhoMapa = objMapa->vt2dTamanhoTotalMatrizMapa();
    vt2dDestino = _vt2dDestino;

    bCalculandoCaminho = true;

    QVector<QVector2D> vt2dTrajetoFinal;
    vt2dTrajetoFinal.clear();

    QVector2D vetorDeConversao(((vt2dTamanhoCampo.x()-iEspessuraLinha)/2)+globalConfig.fieldOffset, ((vt2dTamanhoCampo.y()-iEspessuraLinha)/2)+globalConfig.fieldOffset);//torna as coordenadas dos robôs positivas
    vt2dDestino = vt2dDestino + vetorDeConversao;
    _vt2dPosicaoInicial = _vt2dPosicaoInicial + vetorDeConversao;

    vt2dDestino = QVector2D(qCeil(vt2dDestino.x()/fResolucaoCampo), qCeil(vt2dDestino.y()/fResolucaoCampo));//converte a posição destino inicialmente em coordenadas do campo real
                                                                                                            //para coordenadas em escala para o mapa
    _vt2dPosicaoInicial = QVector2D(qCeil(_vt2dPosicaoInicial.x()/fResolucaoCampo), qCeil(_vt2dPosicaoInicial.y()/fResolucaoCampo));


    cllClosedList.clear();


    Celula estadoVazio;

    estadoVazio.f = 999999;
    estadoVazio.g = 999999;
    estadoVazio.h = 999999;
    estadoVazio.vt2dPosicao = QVector2D(-1,-1);
    estadoVazio.isClosed = false;
    estadoVazio.isOpen = false;
    estadoVazio.cllPai = nullptr;

    for(int i = 0; i < (int)objMapa->vt2dTamanhoTotalMatrizMapa().x(); ++i)
    {
        for(int j = 0; j < (int)objMapa->vt2dTamanhoTotalMatrizMapa().y(); ++j)
        {
            mtMatrizAEstrela[i][j] = estadoVazio;
        }
    }

    iNumeroDeChamadasFuncao = 0;
    QVector2D destinoAjustado = vt2dChecaObstaculoDestino(vt2dDestino, cllCelulaInicial, _bIgnorarBola, _iRoboID);

    if(destinoAjustado.distanceToPoint(vt2dDestino) > 1.2)//Mais de 1.2 quadrados do mapa de diferença
        fFatorHeuristica = 0.5;//vt2dDestino = destinoAjustado;

    cllCelulaAtual.cllPai = nullptr;
    cllCelulaAtual.vt2dPosicao = _vt2dPosicaoInicial;
    cllCelulaAtual.g = 0;

    if(bHeuristica == Euclidiana)
        cllCelulaAtual.h = vt2dDestino.distanceToPoint(cllCelulaAtual.vt2dPosicao);
    else cllCelulaAtual.h = iCalculaHeuristicaManhattan(vt2dDestino, cllCelulaAtual.vt2dPosicao);

    cllCelulaAtual.f = cllCelulaAtual.g + cllCelulaAtual.h;

    cllCelulaAtual.isOpen = true;
    cllCelulaAtual.isClosed = false;

    cllCelulaInicial = cllCelulaAtual;

    cllOpenList = pqAStar() ;

    cllOpenList.push(cllCelulaAtual);

    bTrajetoCalculado = false;

    while(bTrajetoCalculado == false)
    {
        vCalculaCelulasVizinhas(cllCelulaAtual, _bIgnorarBola, _iRoboID, mtMatrizAEstrela);

        cllCelulaAtual = cllOpenList.top();
        cllOpenList.pop();

        if(cllCelulaAtual.vt2dPosicao.x() < objMapa->vt2dTamanhoTotalMatrizMapa().x() &&
           cllCelulaAtual.vt2dPosicao.y() < objMapa->vt2dTamanhoTotalMatrizMapa().y() )
        {
            if(cllCelulaAtual.vt2dPosicao.x() >= 0 && cllCelulaAtual.vt2dPosicao.y() >= 0)
                    mtMatrizAEstrela[(int)cllCelulaAtual.vt2dPosicao.x()][(int)cllCelulaAtual.vt2dPosicao.y()].isClosed = true;
        }


        cllClosedList.append(cllCelulaAtual);

        if(cllOpenList.size() < 1 || (int)cllOpenList.size() > qRound((tamanhoMapa.x() * tamanhoMapa.y()) / 2) || (int)cllClosedList.size() > qRound((tamanhoMapa.x() * tamanhoMapa.y()) / 6))//Não foi possível calcular um trajeto
        {
            bTrajetoCalculado = false;
            break;
        }

        if(cllCelulaAtual.h <= cllCelulaInicial.h*fFatorHeuristica )
        {
            bTrajetoCalculado = true;
            break;
        }

    }


    if(bTrajetoCalculado == true)
    {
        QVector<QVector2D> vt2dCaminhoOriginal;
        vt2dTrajetoFinal = vt2dOrganizaPontosCaminhoFinal(_iRoboID, _bIgnorarBola, vt2dCaminhoOriginal);

        bTrajetoCalculado = false;

        dTempoCalculoCaminho = (double)(etmAstar.nsecsElapsed()/1e6);

        _vt2dPosicaoInicial = _vt2dPosicaoInicial*fResolucaoCampo - vetorDeConversao;
        vt2dDestino       = vt2dDestino - vetorDeConversao;

        bCalculandoCaminho = false;

        if(_vt2dCaminhoOriginal != nullptr)
        {
            _vt2dCaminhoOriginal->clear();
            _vt2dCaminhoOriginal->append(vt2dCaminhoOriginal);
        }

        if(_iCelulasExpandidas != nullptr)
        {
            *_iCelulasExpandidas = cllClosedList.size();
        }
    }
    else if(bRoboNaAreaPenalty == true)//Para que o robô consiga sair da àrea do penalty caso entre sem querer
    {
        QVector<QVector2D> vt2dCaminhoFinal;
        vt2dCaminhoFinal.clear();
        vt2dCaminhoFinal.append(_acAmbientePathPlanner->rbtRoboAliado[_iRoboID]->vt2dDestinoRobo());
        vt2dCaminhoFinal.append(_acAmbientePathPlanner->rbtRoboAliado[_iRoboID]->vt2dPosicaoAtualRobo());

        vt2dTrajetoFinal.clear();
        vt2dTrajetoFinal.append(vt2dCaminhoFinal);
    }


    for(auto & i : cllClosedList)
        delete i.cllPai;

    while(!cllOpenList.empty())
    {
        Celula aux = cllOpenList.top();
        cllOpenList.pop();
        if( aux.cllPai != nullptr){
           delete aux.cllPai;
           aux.cllPai = nullptr;
        }
    }

    _acAmbientePathPlanner->fComprimentoCaminhos[_iRoboID] = fComprimentoCaminho*fResolucaoCampo;

    return vt2dTrajetoFinal;

}

double A_star::dRetornaTempoCalculoCaminho()
{
    if(qIsFinite(dTempoCalculoCaminho))
        return dTempoCalculoCaminho;
    else
        return -1;
}
