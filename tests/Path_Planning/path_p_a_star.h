#ifndef PATH_P_A_STAR_H
#define PATH_P_A_STAR_H

#include <QElapsedTimer>
#include <QThread>
#include <QTime>

#include "Mapa/pathmap.h"
#include "Path_Planning/a_star_vg.h"

#include <QDebug>

#define Euclidiana 0
#define Manhattan  1

#define iNumeroMaximoChamadasAjustaDestino 1000


//typedef struct Celula
//{
//    QVector2D vt2dPosicao;
//    Celula *cllPai;
//    float g,h,f;
//    bool isOpen, isClosed;
//}Celula;

//struct CompararAStar
//{
//    bool operator()(const Celula& _C1, const Celula& _C2)
//    {
//        if(_C1.f > _C2.f)
//            return true;
//        if(_C1.h > _C2.h)
//            return true;
//        else
//            return false;

//    }
//};


/**
 * @brief
 *
 */
class A_star
{
    /**
     * @brief
     *
     */
    typedef std::priority_queue<Celula, std::vector<Celula>, CompararAStar> pqAStar;

    QTime *tmPathPlannerAEstrela; /**< TODO: describe */
    QFile fileLogsPathPlannerAEstrela; /**< TODO: describe */
    double dTempoCalculoCaminho; /**< TODO: describe */
    float fComprimentoCaminho; /**< TODO: describe */

    pqAStar cllOpenList; /**< TODO: describe */
    QVector<Celula> cllClosedList; /**< TODO: describe */
    Celula** mtMatrizAEstrela;// Matriz para identificar se aquela posicao ja esta na closed list /**< TODO: describe */

    QVector2D vt2dDestino; /**< TODO: describe */
    Celula cllCelulaAtual; /**< TODO: describe */
    Celula cllCelulaInicial; /**< TODO: describe */
    bool bTrajetoCalculado = false; /**< TODO: describe */
    bool bCalculandoCaminho = false; /**< TODO: describe */
    bool bHeuristica = Manhattan; /**< TODO: describe */
    bool bRoboNaAreaPenalty = false; /**< TODO: describe */
    QVector2D vt2dTamanhoCampo; /**< TODO: describe */

    int iNumeroDeChamadasFuncao; //Variavel utilizada para contar o numero de vezes que a função vt2dChecaObstaculoDestino se chama, para impedir que ela chame a si própria infinitamente /**< TODO: describe */

    /**
     * @brief
     *
     * @param _vt2dPrimeiroPonto
     * @param _vt2dSegundoPonto
     * @return float
     */
    float fCalculaHeuristicaEuclidiana(QVector2D _vt2dPrimeiroPonto, QVector2D _vt2dSegundoPonto);
    /**
     * @brief
     *
     * @param _vt2dPrimeiroPonto
     * @param _vt2dSegundoPonto
     * @return int
     */
    int iCalculaHeuristicaManhattan(QVector2D _vt2dPrimeiroPonto, QVector2D _vt2dSegundoPonto);

    /**
     * @brief
     *
     * @param _cllCelula
     */
    void vAdicionaClosedList(Celula &_cllCelula);

    /**
     * @brief
     *
     * @param _iRoboID
     * @param _bIgnorarBola
     * @param _vt2dCaminhoSemFiltro
     * @return QVector<QVector2D>
     */
    QVector<QVector2D> vt2dOrganizaPontosCaminhoFinal(int _iRoboID, bool _bIgnorarBola, QVector<QVector2D> &_vt2dCaminhoSemFiltro);
    /**
     * @brief
     *
     * @param vt2dPontoInicial
     * @param vt2dPontoFinal
     * @param vt2dSaidaPontos
     */
    void vInserePontos(QVector2D vt2dPontoInicial, QVector2D vt2dPontoFinal, QVector<QVector2D> &vt2dSaidaPontos);

    /**
     * @brief
     *
     * @param _cllCelulaAtual
     * @param _bIgnorarBola
     * @param _iRoboID
     * @param _matrizAEstrela
     */
    void vCalculaCelulasVizinhas(Celula& _cllCelulaAtual, bool _bIgnorarBola, int _iRoboID, Celula **_matrizAEstrela);//P/ heuristica = 0 -> dist. Euclidiana, heuristica = 1 -> dist.Manhattan
    /**
     * @brief
     *
     * @param _vt2dPosicaoTeste
     * @param _cllCelulaInicial
     * @param _bIgnorarBola
     * @param _iRoboID
     * @return QVector2D
     */
    QVector2D vt2dChecaObstaculoDestino(QVector2D _vt2dPosicaoTeste, Celula &_cllCelulaInicial, bool _bIgnorarBola, int _iRoboID);
    /**
     * @brief
     *
     * @param _vt2dPosicaoTeste
     * @param _vt2dTamanhoMapa
     * @return bool
     */
    bool bChecaLimite(QVector2D _vt2dPosicaoTeste, QVector2D _vt2dTamanhoMapa);


public:
    /**
     * @brief
     *
     * @param vt2dTamanhoCampo
     * @param _bHeuristica
     */
    A_star(QVector2D vt2dTamanhoCampo, bool _bHeuristica = Manhattan);
    /**
     * @brief
     *
     */
    ~A_star();

    clsMapa *objMapa; /**< TODO: describe */

    /**
     * @brief
     *
     * @param _objAmbiente
     */
    void vAtualizaMapaPathPlanner(AmbienteCampo *_objAmbiente);

    /**
     * @brief
     *
     * @param _acAmbientePathPlanner
     * @param _iRoboID
     * @param _bIgnorarBola
     * @param _iCelulasExpandidas
     * @param _vt2dCaminhoOriginal
     * @return QVector<QVector2D>
     */
    QVector<QVector2D> vt2dCalculaTrajeto(AmbienteCampo *_acAmbientePathPlanner, int _iRoboID, bool _bIgnorarBola, int *_iCelulasExpandidas = nullptr, QVector<QVector2D> *_vt2dCaminhoOriginal = nullptr);
    /**
     * @brief
     *
     * @return double
     */
    double dRetornaTempoCalculoCaminho();
};

#endif // PATH_P_A_STAR_H
