//#include "path_p_rrt.h"

//RRT::RRT(QVector2D _vt2dTamanhoCampo)
//{
//    index = new my_kd_tree_t(2 /*dim*/, KDTree, nanoflann::KDTreeSingleIndexAdaptorParams(1500 /* max leaf */) );
//    // ----------------------------------------------------------------
//    // knnSearch():  Perform a search for the N closest points
//    // ----------------------------------------------------------------

//    vt2dTamanhoCampo = _vt2dTamanhoCampo;
//    objMapa = new clsMapa((int)vt2dTamanhoCampo.x(), (int)vt2dTamanhoCampo.y());

//    tmPathPlannerRRT = new QTime(QTime::currentTime());
//    tmPathPlannerRRT->start();

//    QString fileName = "";
//    fileName += "logPathPlannerRRT_";
//    fileName += tmPathPlannerRRT->toString("h:m:s");
//    fileName += ".txt";
//    fileLogsPathPlannerRRT.setFileName(fileName);
//    fileLogsPathPlannerRRT.open(QIODevice::ReadOnly | QIODevice::Text | QIODevice::ReadWrite);

//    QTextStream stream(&fileLogsPathPlannerRRT);

//    stream << "Numero total de interacoes"         << ";"
//           << "Comprimento do caminho gerado (mm)" << ";"
//           << "Posicao inicial (X)           (mm)" << ";"
//           << "Posicao inicial (Y)           (mm)" << ";"
//           << "Destino         (X)           (mm)" << ";"
//           << "Destino         (Y)           (mm)" << ";"
//           << "Tempo para calculo do caminho (ms)" << ";"
//           << "Tempo gasto com busca linear  (ms)" << ";"
//           << endl;

//}

//RRT::~RRT()
//{
//    delete index;
//}

//void RRT::vAtualizaMapaPathPlanner(AmbienteCampo *_objAmbiente)
//{
//    if(bCalculandoCaminho == false)
//        objMapa->vAtualiza(_objAmbiente);
//}

//QVector<QVector2D> RRT::vt2dCalculaTrajetoRRT(QVector<Vertice> &_vrArvoreFinal,QVector2D _vt2dDestino, QVector2D _vt2dPosicaoInicial, int _iRoboID, bool _bIgnorarBola)
//{
//    QVector<QVector2D> vt2dCaminhoFinal;
//    QElapsedTimer etmRRT;
//    vt2dCaminhoFinal.clear();
//    vrArvoreRRT.clear();
//    KDTree.pts.clear();
//    KDTree.parent.clear();
//    KDTree.pais.clear();


////    KDTree.pts.resize(iNumeroMaximoInteracoes); 9/3
////    KDTree.parent.resize(iNumeroMaximoInteracoes); 9/3
//    iIteracao = 0;

////    index->buildIndex(); 9/3

//    float fComprimentoCaminho=0;

//    bCalculandoCaminho = true;

//    etmRRT.start();
//    fTempoProcurandoVerticeProximo = 0;

//    vrVerticeRaiz.vt2dPosicao = _vt2dPosicaoInicial;

//    Point aux;
//    Vertice vr;
//    vr.vrPai = nullptr;
//    vr.vt2dPosicao = QVector2D(0,0);
//    aux.x = _vt2dPosicaoInicial.x();
//    aux.y = _vt2dPosicaoInicial.y();

//    KDTree.pts.push_back(aux);
//    KDTree.parent.push_back(aux);
//    KDTree.pais.push_back(vr);
//    ++iIteracao;

//    vrDestino.vt2dPosicao = vt2dChecaObstaculoDestino(_vt2dDestino, _vt2dPosicaoInicial, _bIgnorarBola, _iRoboID);//Ajuste de destino

//    vrVerticeProximo.vt2dPosicao = _vt2dPosicaoInicial;


//    for(int i=0; i < iNumeroMaximoInteracoes; ++i)
//    {
//        vrVerticeAleatorio = vrEscolherAleatorio();
//        if(msgExtenderArvore(_iRoboID, _bIgnorarBola) == Chegou)
//        {
//            bCalculandoCaminho = false;

//            vt2dCaminhoFinal = vt2dExtraiCaminhoFinal(_iRoboID, _bIgnorarBola, fComprimentoCaminho);
//            _vrArvoreFinal = vrArvoreRRT;
//            fTempoCalculoCaminho = (float)(etmRRT.nsecsElapsed()/1000000.0);


//            QTextStream stream(&fileLogsPathPlannerRRT);

//            stream << i << ";"
//                   << fComprimentoCaminho << ";"
//                   << _vt2dPosicaoInicial.x()    << ";"
//                   << _vt2dPosicaoInicial.y()    << ";"
//                   << vrDestino.vt2dPosicao.x()  << ";"
//                   << vrDestino.vt2dPosicao.y()  << ";"
//                   << fTempoCalculoCaminho << ";"
//                   << fTempoProcurandoVerticeProximo << ";"
//                   << endl;

//            qDebug()<<"Deu certo";

//            for(auto & pai : KDTree.pais)
//                delete pai.vrPai;

//            return vt2dCaminhoFinal;
//        }
//    }
//    bCalculandoCaminho = false;
//    qDebug()<<"Deu errado";

////    for(Arvore::const_iterator it = arArvoreRRT.cbegin(); it != arArvoreRRT.cend(); ++it)
////    {
////        vrArvoreRRT.append(it->second);
////    }

////    _vrArvoreFinal = vrArvoreRRT;

////    fTempoCalculoCaminho = (float)(etmRRT.nsecsElapsed()/1000000.0); //tmPathPlannerRRT->elapsed() - fTempoCalculoCaminho;

////    QTextStream stream(&fileLogsPathPlannerRRT);

////    stream << iNumeroMaximoInteracoes << ";"
////           << fComprimentoCaminho << ";"
////           << _vt2dPosicaoInicial.x()    << ";"
////           << _vt2dPosicaoInicial.y()    << ";"
////           << vrDestino.vt2dPosicao.x()  << ";"
////           << vrDestino.vt2dPosicao.y()  << ";"
////           << fTempoCalculoCaminho << ";"
////           << fTempoProcurandoVerticeProximo << ";"
////           << endl;

////    qDebug()<<"Tempo Calculo: " << iTempoCalculoCaminho;
//    for(auto & pai : KDTree.pais)
//        delete pai.vrPai;

//    return vt2dCaminhoFinal;
//}

//QVector<QVector2D> RRT::vt2dExtraiCaminhoFinal(int _iRoboID, bool _bIgnorarBola, float &_fComprimentoCaminho)
//{
//    QVector<Vertice> vrCaminhoFinal;
//    QVector<QVector2D> vt2dCaminhoFinal;
//    QVector<QVector2D> vt2dCaminhoReduzido;
//    QVector<Vertice> temp;
//    QElapsedTimer etmRRT;

//    vrCaminhoFinal.clear();
//    vt2dCaminhoFinal.clear();
//    vt2dCaminhoReduzido.clear();
//    temp.clear();

//    etmRRT.start();

//    temp.append(KDTree.pais[iIndiceDestino]);

//    vt2dCaminhoFinal.append(QVector2D(KDTree.pts[iIndiceDestino].x, KDTree.pts[iIndiceDestino].y));

//    while(temp.last().vt2dPosicao != vrVerticeRaiz.vt2dPosicao)
//    {
//        temp.append(*temp.last().vrPai);
//        vt2dCaminhoFinal.prepend(temp.last().vt2dPosicao);
//    }

//    for(int i=0; i < iIteracao; ++i)
//    {
//        Vertice aux;
//        aux.vt2dPosicao = QVector2D(KDTree.pts[i].x, KDTree.pts[i].y);
//        aux.vrPai = new Vertice();
//        aux.vrPai->vt2dPosicao = QVector2D(KDTree.parent[i].x, KDTree.parent[i].y);

//        vrArvoreRRT.append(aux);
//    }



////    for(int n=0; n < vrArvoreRRT.size()-1; ++n)//Extrai os vértices que compõem o caminho final da árvore gerada
////    {
////        vrCaminhoFinal.append(vrArvoreRRT[vrCaminhoFinal.last().iPosicaoAnterior]);


////        if(vrCaminhoFinal.last().vt2dPosicao == vrVerticeRaiz.vt2dPosicao)
////            break;
////    }

////    for(int n = vrCaminhoFinal.size()-1; n >= 0; --n)//Extrai somente as posições do caminho final gerado
////    {
//////        vt2dCaminhoFinal.append(vrCaminhoFinal[n].vt2dPosicao);
////        vt2dCaminhoFinal.prepend(vrCaminhoFinal[n].vt2dPosicao);

////    }

//    //return vt2dCaminhoFinal;

//    vt2dCaminhoReduzido.append(vt2dCaminhoFinal.last());

//    int n = vt2dCaminhoFinal.size()-1;
//    int i = 0;

//    //int aux = tmPathPlannerRRT->elapsed();


//    while(n > 0)
//    {
//        while(objMapa->bChecaInterseccaoLinha(vt2dCaminhoFinal[n], vt2dCaminhoFinal[i], _iRoboID, _bIgnorarBola, bCoordenadaReal, iRaioSegurancaRobo*1.5) == true)
//        {
//            ++i;
//            if(vt2dCaminhoFinal[n] == vt2dCaminhoFinal[i])
//            {
//                --i;
//                break;
//            }
//        }
//        vt2dCaminhoReduzido.prepend(vt2dCaminhoFinal[i]);
//        n = i;
//        i = 0;
//    }

//    for(int j=0; j < vt2dCaminhoReduzido.size()-1; ++j)
//    {
//        _fComprimentoCaminho += clsAuxiliar::fCalculaDistancia(vt2dCaminhoReduzido[j], vt2dCaminhoReduzido[j+1]);
//    }

//    //vt2dCaminhoReduzido.append(vt2dCaminhoFinal.last());

//    //_teste = vt2dCaminhoFinal;

//    qDebug() << "Tempo extrair caminho final: " << etmRRT.nsecsElapsed()/1e6;
//    return vt2dCaminhoReduzido;
//}

//Vertice RRT::vrEscolherAleatorio()
//{
//    Vertice vrAuxiliar;

//    vrAuxiliar.vt2dPosicao.setX((qrand() % (int)(vt2dTamanhoCampo.x()/2 + iOffsetCampo)) * ( ( (qrand() )%2 == 0)? 1 : -1) );
//    vrAuxiliar.vt2dPosicao.setY((qrand() % (int)(vt2dTamanhoCampo.y()/2 + iOffsetCampo)) * ( ( (qrand() )%2 == 0)? 1 : -1) );
//    vrAuxiliar.fDistanciaRaiz = clsAuxiliar::fCalculaDistancia(vrAuxiliar.vt2dPosicao, vrVerticeRaiz.vt2dPosicao);

//    return vrAuxiliar;
//}

//void RRT::vDefineVerticeProximo()
//{
//    QElapsedTimer etmRRT;

//    etmRRT.start();

//    size_t num_results = 1;
//    size_t ret_index;
//    float out_dist_sqr;


//    double query_pt[2] = {vrVerticeAleatorio.vt2dPosicao.x(), vrVerticeAleatorio.vt2dPosicao.y()};

//    nanoflann::KNNResultSet<float> resultSet(num_results);

//    resultSet.init(&ret_index, &out_dist_sqr );

//    index->buildIndex();

//    index->findNeighbors(resultSet, &query_pt[0], nanoflann::SearchParams(1500));

//    fTempoProcurandoVerticeProximo += (float)(etmRRT.nsecsElapsed()/1e6);//tmPathPlannerRRT->elapsed() - t1;
//    qDebug() << "Tempo Busca: " << (float)fTempoProcurandoVerticeProximo;

//    vrVerticeProximo.vt2dPosicao.setX( KDTree.pts[ret_index].x);
//    vrVerticeProximo.vt2dPosicao.setY( KDTree.pts[ret_index].y);

//    vrVerticeProximo.vrPai = new Vertice;
//    *vrVerticeProximo.vrPai = KDTree.pais[ret_index];

//}

//Mensagem RRT::msgExtenderArvore(int _iRoboID, bool _bIgnorarBola)
//{
//    Vertice vrNovoVertice;
//    vDefineVerticeProximo();

//    if(bNovoVertice(vrNovoVertice, _iRoboID, _bIgnorarBola))
//    {
//        Point aux;
//        aux.x = vrNovoVertice.vt2dPosicao.x();
//        aux.y = vrNovoVertice.vt2dPosicao.y();

//        KDTree.pts.push_back(aux);

//        aux.x = vrVerticeProximo.vt2dPosicao.x();
//        aux.y = vrVerticeProximo.vt2dPosicao.y();

//        KDTree.pais.push_back(vrNovoVertice);
//        KDTree.parent.push_back(aux);

//        iIteracao++;


//        if(clsAuxiliar::fCalculaDistancia(vrNovoVertice.vt2dPosicao, vrDestino.vt2dPosicao) < iDistanciaMinimaDestino)
//        {
//            iIndiceDestino = /*iIteracao-1;//*/KDTree.pts.size()-1;
//            return Chegou;
//        }
//        else return Avancou;
//    }

//    return EstadoVazio;

//}

//bool RRT::bNovoVertice(Vertice &_vrNovoVertice, int _iRoboID, bool _bIgnorarBola)//Aleat,Prox,Novo
//{
//    QVector2D vt2dVersorDirecaoNovoVertice = vrVerticeAleatorio.vt2dPosicao - vrVerticeProximo.vt2dPosicao;
//    vt2dVersorDirecaoNovoVertice.normalize();

//    _vrNovoVertice.vt2dPosicao = vrVerticeProximo.vt2dPosicao + vt2dVersorDirecaoNovoVertice * fDistanciaAvancoBase;
//    _vrNovoVertice.vrPai = new Vertice;
//    *_vrNovoVertice.vrPai = vrVerticeProximo;

//    if(!objMapa->bChecaObstaculo(_vrNovoVertice.vt2dPosicao, _iRoboID, _bIgnorarBola, bCoordenadaReal))
//        return true;
//    else return false;

//}

//float RRT::fDistancia(QVector2D _vt2dPrimeiroPonto, QVector2D _vt2dSegundoPonto)
//{
//    float dist = sqrt( pow(_vt2dPrimeiroPonto.x() - _vt2dSegundoPonto.x(), 2) + pow(_vt2dPrimeiroPonto.y() - _vt2dSegundoPonto.y(), 2) );
//    return dist;
//}

//QVector2D RRT::vt2dChecaObstaculoDestino(QVector2D _vt2dDestinoTeste, QVector2D _vt2dCelulaInicial, bool _bIgnorarBola, int _iRoboID)
//{
//    QVector2D vt2dDestinoAjustado = _vt2dDestinoTeste;

//    if(objMapa->bChecaObstaculo(_vt2dDestinoTeste, _iRoboID, _bIgnorarBola, bCoordenadaReal) == true)
//    {
//        if( (int)vt2dDestinoAjustado.x() > (int)_vt2dCelulaInicial.x())
//            vt2dDestinoAjustado.setX( vt2dDestinoAjustado.x() - fDistanciaAvancoBase/4 );
//        else
//            vt2dDestinoAjustado.setX( vt2dDestinoAjustado.x() + fDistanciaAvancoBase/4 );

//        if((int)vt2dDestinoAjustado.y() > (int)_vt2dCelulaInicial.y())
//            vt2dDestinoAjustado.setY( vt2dDestinoAjustado.y() - fDistanciaAvancoBase/4 );
//        else
//            vt2dDestinoAjustado.setY( vt2dDestinoAjustado.y() + fDistanciaAvancoBase/4 );
//        vt2dDestinoAjustado = vt2dChecaObstaculoDestino(vt2dDestinoAjustado, _vt2dCelulaInicial, _bIgnorarBola, _iRoboID);
//    }

//    return vt2dDestinoAjustado;
//}
