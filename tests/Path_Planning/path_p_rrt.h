//#ifndef PATH_P_RRT_H
//#define PATH_P_RRT_H

//#include <QDebug>
//#include <QElapsedTimer>
//#include <QFile>
//#include <QTime>

//#include "Mapa/pathmap.h"
//#include "Path_Planning/nanoflann.hpp"
//#include <map>

//const float fDistanciaAvancoBase = 200;
//const int iNumeroMaximoInteracoes = 1000;
//const int iDistanciaMinimaDestino = 250;

//inline bool operator<(const Vertice& _v1, const Vertice& _v2)
//{
//      return _v1.fDistanciaRaiz < _v2.fDistanciaRaiz;
//}

//struct ComparaRRT {
//  bool operator() (const float& _v1, const float& _v2) const
//  {return _v1 < _v2;}
//};


//enum Mensagem
//{
//    EstadoVazio,
//    Chegou,
//    Avancou,
//};


//struct Point
//{
//    double  x,y;
//};

//typedef struct Folha
//{
//    Point pPosicao;
//    Folha *flPai;
//}Folha;

//struct Tree
//{
//    std::vector<Point> pts;
//    std::vector<Point> parent;
//    std::vector<Vertice> pais;

//    // Must return the number of data points
//    inline size_t kdtree_get_point_count() const { return pts.size(); }

//    // Returns the dim'th component of the idx'th point in the class:
//    // Since this is inlined and the "dim" argument is typically an immediate value, the
//    //  "if/else's" are actually solved at compile time.
//    inline float kdtree_get_pt(const size_t idx, int dim) const
//    {
//        if (dim == 0) return pts[idx].x;
//        else if (dim == 1) return pts[idx].y;
//        //else return pts[idx].z;
//    }

//    // Optional bounding-box computation: return false to default to a standard bbox computation loop.
//    //   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
//    //   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
//    template <class BBOX>
//    bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }

//};

//class RRT
//{
//    typedef nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<double, Tree > ,Tree, 2 /* dim */ > my_kd_tree_t;

//    Tree KDTree;

//    my_kd_tree_t *index;


//    clsMapa *objMapa;
//    QVector2D vt2dTamanhoCampo;

//    QTime *tmPathPlannerRRT;
//    QFile fileLogsPathPlannerRRT;
//    float fTempoCalculoCaminho;
//    float fTempoProcurandoVerticeProximo;

//    QVector<Vertice> vrArvoreRRT;
//    Vertice vrVerticeProximo, vrVerticeRaiz, vrVerticeAleatorio, vrDestino;
//    int iIndiceDestino, iIteracao;
//    bool bCalculandoCaminho = false;
    
//    Vertice vrEscolherAleatorio();
//    void vDefineVerticeProximo();
//    Mensagem msgExtenderArvore(int _iRoboID, bool _bIgnorarBola);
//    bool bNovoVertice(Vertice &vrNovoVertice, int _iRoboID, bool _bIgnorarBola);
//    float fDistancia(QVector2D _vt2dPrimeiroPonto, QVector2D _vt2dSegundoPonto);

//    QVector2D vt2dChecaObstaculoDestino(QVector2D _vt2dDestinoTeste, QVector2D _vt2dCelulaInicial, bool _bIgnorarBola, int _iRoboID);
//    QVector<QVector2D> vt2dExtraiCaminhoFinal(int _iRoboID, bool _bIgnorarBola, float &_fComprimentoCaminho);

////    void operator()(Vertice &_v1, Vertice &_v2);

//public:
//    RRT(QVector2D _vt2dTamanhoCampo);
//    ~RRT();
//    void vAtualizaMapaPathPlanner(AmbienteCampo *_objAmbiente);
//    QVector<QVector2D> vt2dCalculaTrajetoRRT(QVector<Vertice> &_vrArvoreFinal, QVector2D _vt2dDestino, QVector2D _vt2dPosicaoInicial, int _iRoboID, bool _bIgnorarBola);
//};

//#endif // PATH_P_RRT_H
