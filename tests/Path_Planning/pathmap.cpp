#include "Mapa/pathmap.h"


Objeto::Objeto(TipoObjeto _otTipo, int _iID)
{
    otTipo = _otTipo;
    iID = _iID;
}

Ponto::Ponto()
{
    bImprimirImagem = false;
    objPrimario = Objeto();
    objObjetos.clear();
    segSeguranca = Nivel_2;
}

bool Ponto::bEstaVazio()
{
    if(objObjetos.isEmpty() && objPrimario.otTipo == otDestino)return true;
    else return false;
}

bool Ponto::bEstouAqui(Objeto _objObjeto)
{
    if(objPrimario.otTipo == _objObjeto.otTipo && objPrimario.iID == _objObjeto.iID)
    {
//        if(objObjetos.size() > 0)
//            return false;
        return true;
    }
    else if(!objObjetos.isEmpty())
    {
        if(objObjetos.contains(_objObjeto) == true && objObjetos.contains(Objeto(otBola, 0)) == false && segSeguranca == Nivel_1)
            return true;

        else if(segSeguranca == Nivel_1)
            return true;
    }
    return false;
}


clsMapa::clsMapa(int xSize, int ySize)//Valores máximos de X e Y e do offset
{
    //O offset é multiplicado por dois porque ele é aplicado nos dois lados do campo
    iXMax = (xSize + globalConfig.fieldOffset*2 - iEspessuraLinha)/fResolucaoCampo;
    iYMax = (ySize + globalConfig.fieldOffset*2 - iEspessuraLinha)/fResolucaoCampo;
    ptoMapa = std::vector<std::vector<Ponto> >(iXMax, std::vector<Ponto>(iYMax));
//    byMapa  = new Ponto* [iXMax];

//    for(int i=0; i<iXMax; i++)
//        byMapa[i] = new Ponto[iYMax];

    vResetaMapa();
    iLarguraAreaPenalty = 0;
    iProfundiadePenalty = 0;
    // drawBoundLine(offsetCampo*2/fieldResolution);
}

clsMapa::~clsMapa()
{
//    if(byMapa == nullptr)return;

//    for(int i=0; i<iXMax; i++)
//        delete byMapa[i];
//    delete [] byMapa;
}

QVector2D clsMapa::vt2dTamanhoTotalMatrizMapa()
{
    return QVector2D(iXMax, iYMax);
}

void clsMapa::vAtualiza(AmbienteCampo *ambiente)
{
    vt2dTamanhoCampo = ambiente->vt2dPegaTamanhoCampo();
    QVector2D vetorDeConversao(((vt2dTamanhoCampo.x()-iEspessuraLinha)/2)+globalConfig.fieldOffset, ((vt2dTamanhoCampo.y()-iEspessuraLinha)/2)+globalConfig.fieldOffset);//torna as coordenadas dos robôs positivas

    iLarguraAreaPenalty  = ambiente->iPegaLarguraAreaDefesa();
    iProfundiadePenalty = ambiente->iPegaProfundidadeAreaDefesa();

    iLadoDefesa  = ambiente->iLadoCampo;

    vLimpaMapa();

    for(int i=0; i<PLAYERS_PER_SIDE; i++)//É necessário que as coordenadas enviadas sejam todas positivas
    {
        const Robo* roboAliado = ambiente->rbtRoboAliado[i];
        const Robo* roboAdversario = ambiente->rbtRoboAdversario[i];
        const Bola& bola = ambiente->blPegaBola();

        if(roboAliado->iID() != -1 && roboAliado->bRoboDetectado() == true)
        {
            if(roboAliado->iID() == ambiente->iIDGoleiro)
            {
                Ponto pontoGoleiro;
                pontoGoleiro.objPrimario = Objeto(otAliado, roboAliado->iID());

                vDesenhaAreaGol(pontoGoleiro, ambiente->iPegaLarguraAreaDefesa(), ambiente->iPegaProfundidadeAreaDefesa(), iLadoDefesa);
            }

            //allyRobot.isInVision = false;
            vAdicionaRobo((roboAliado->vt2dPosicaoAtualRobo() + vetorDeConversao), otAliado, roboAliado->iID());
        }

        if(roboAdversario->iID() != -1 && roboAdversario->bRoboDetectado()== true)
        {

            if(ambiente->iIDGoleiroOponente != -1)
            {
                Ponto pontoGoleiro;
                pontoGoleiro.objPrimario = Objeto(otOponente, ambiente->iIDGoleiroOponente);

                vDesenhaAreaGol(pontoGoleiro, ambiente->iPegaLarguraAreaDefesa(), ambiente->iPegaProfundidadeAreaDefesa(), iLadoDefesa*-1);
            }
//            int idGoleiro = ambiente->achaGoleiroInimigo();
            vAdicionaRobo((roboAdversario->vt2dPosicaoAtualRobo()+ vetorDeConversao), otOponente, roboAdversario->iID());

        }

        if(bola.bEmCampo == true )
        {
            vAdicionaBola(bola.vt2dPosicaoAtual + vetorDeConversao);
            ambiente->vSetaBola(bola);
        }

//        if(!ambiente->caminhosPathPlanner[i].isEmpty())
//        {
//            u_int8_t byCodigoRobo = byCriaCodigoObjeto(i, otAllyPlayer);
//            vt2dCaminho[i].clear();

//            for(int n=0; n < ambiente->caminhosPathPlanner[i].size(); ++n)
//                vt2dCaminho[i].append(ambiente->caminhosPathPlanner[i][n]);

//            vDesenhaCaminhos(ambiente->caminhosPathPlanner[i], byCodigoRobo);
//        }
    }

}

QVector2D clsMapa::vt2dConverteCoordenadaRoboParaMatriz(QVector2D _vt2dPosicaoObjeto)
{
    return {static_cast<float>(qCeil( (_vt2dPosicaoObjeto.x()-(globalConfig.robotDiameter/2)) / fResolucaoCampo)), static_cast<float>(qCeil( (_vt2dPosicaoObjeto.y()-(globalConfig.robotDiameter/2)) / fResolucaoCampo))};
}

QVector2D clsMapa::vt2dConverteCoordenadaBolaParaMatriz(QVector2D _vt2dPosicaoObjeto)
{
    return {static_cast<float>(qCeil( (_vt2dPosicaoObjeto.x()-(globalConfig.ballDiameter/2)) / fResolucaoCampo)), static_cast<float>(qCeil( (_vt2dPosicaoObjeto.y()-(globalConfig.ballDiameter/2)) / fResolucaoCampo))};
}

bool clsMapa::bChecaObstaculo(QVector2D _vt2dPosicaoTeste, int _iRoboID, bool _bIgnorarBola, bool _bTipoPathCoordenada)
{
    QVector2D vt2dPosicaoTesteNormalizada = _vt2dPosicaoTeste;;

    if(_bTipoPathCoordenada == bCoordenadaReal)
    {
        QVector2D vetorDeConversao(((vt2dTamanhoCampo.x()-iEspessuraLinha)/2)+globalConfig.fieldOffset, ((vt2dTamanhoCampo.y()-iEspessuraLinha)/2)+globalConfig.fieldOffset);
        vt2dPosicaoTesteNormalizada = (_vt2dPosicaoTeste + vetorDeConversao) / fResolucaoCampo;
    }

    int x1,y1;
    x1 = (int)vt2dPosicaoTesteNormalizada.x();
    y1 = (int)vt2dPosicaoTesteNormalizada.y();

    if(x1 >= iXMax || y1 >= iYMax || x1 < 0 || y1 < 0)
        return true;

    //Ponto nao esta vazio e nao sou eu
    if((ptoMapa[x1][y1].bEstaVazio() == false   && ptoMapa[x1][y1].bEstouAqui(Objeto(otAliado, _iRoboID)) == false)  )
    {
        if( ptoMapa[x1][y1].bEstouAqui(Objeto(otBola, 0)) == true && _bIgnorarBola == bNaoConsideraBola ) // 0xF0 utilizado para ignorara intersecção com a bola
            return false;

        return true;
    }


    return false;

}

bool clsMapa::bChecaInterseccaoLinha(QVector2D _vt2dP1, QVector2D _vt2dP2, int _iRoboID, bool _bIgnorarBola, bool _bTipoCoordenada, float _fDistanciaMinima)
{
    //Checa se o ponto P3 intercepta o segmento de reta formado por P1 e P2
    QVector2D vt2dV, vt2dP3, vt2dU, vt2dVlinha, vt2dW, vt2dA;
    QVector2D Pa, Pb, Pc;
    float fMod2V, fUescalarV, fAux;

    float fDistanciaPontoLinha = 0;

    if(_bTipoCoordenada == bCoordenadaReal)
    {
        QVector2D vetorDeConversao(((vt2dTamanhoCampo.x()-iEspessuraLinha)/2)+globalConfig.fieldOffset, ((vt2dTamanhoCampo.y()-iEspessuraLinha)/2)+globalConfig.fieldOffset);
        _vt2dP1 = (_vt2dP1 + vetorDeConversao) / fResolucaoCampo;
        _vt2dP2 = (_vt2dP2 + vetorDeConversao) / fResolucaoCampo;
    }

    vt2dV = _vt2dP2 - _vt2dP1;// V→ = P2 - P1



    for(int n=0; n < vt4dPosicoesObjetos.size(); ++n)
    {
        if((int)vt4dPosicoesObjetos.at(n).z() == otBola && _bIgnorarBola == bNaoConsideraBola)continue;
        else if((int)vt4dPosicoesObjetos.at(n).z() != otAliado || (int)vt4dPosicoesObjetos.at(n).w() != _iRoboID)
        {
            vt2dP3 = vt4dPosicoesObjetos.at(n).toVector2D();

            vt2dP3.setX( vt2dP3.x() + (globalConfig.robotDiameter/2)/fResolucaoCampo);
            vt2dP3.setY( vt2dP3.y() + (globalConfig.robotDiameter/2)/fResolucaoCampo);

            vt2dU = vt2dP3 - _vt2dP1;// U→ = P3 - P1
            fMod2V = vt2dV.lengthSquared();// |V→|²
            fUescalarV = vt2dU.x() * vt2dV.x() + vt2dU.y() * vt2dV.y();// = U→ . V→
            fAux = fUescalarV/fMod2V; // (U→ . V→)/ |V→|²
            vt2dVlinha = fAux * vt2dV;// V'→ = (U→ . V→)/ |V→|² * V→
            vt2dW = vt2dU - vt2dVlinha;// W→ = U→ - V'→

            fDistanciaPontoLinha = vt2dW.length();// d = |W→|

            if(fDistanciaPontoLinha <= _fDistanciaMinima/fResolucaoCampo )
            {
                vt2dU = QVector2D(iXMax, iYMax) - _vt2dP1;// U→ = P3 - P1
                fMod2V = vt2dV.lengthSquared();// |V→|²
                fUescalarV = vt2dU.x() * vt2dV.x() + vt2dU.y() * vt2dV.y();// = U→ . V→
                fAux = fUescalarV/fMod2V; // (U→ . V→)/ |V→|²
                vt2dVlinha = fAux * vt2dV;// V'→ = (U→ . V→)/ |V→|² * V→
                vt2dW = vt2dU - vt2dVlinha;// W→ = U→ - V'→

                QVector2D vt2dP;
                float fP_PaPb = 0, fPaPb_PaPb = 0, fP_PcPb = 0, fPcPb_PcPb =0;

                vt2dW.normalize();
                vt2dA = vt2dW;
                vt2dA.normalize(); // A^ = W→/|W→|
                vt2dA *= _fDistanciaMinima/fResolucaoCampo;
                Pa = vt2dA + _vt2dP1;
                Pb = _vt2dP2 + vt2dA;
                Pc = _vt2dP2 - vt2dA;

                vt2dP = vt2dP3 - Pb;

                fP_PaPb = qCeil(vt2dP.dotProduct(vt2dP, (Pa-Pb)));
                fPaPb_PaPb = qCeil(vt2dP.dotProduct((Pa-Pb), (Pa-Pb)));
                fP_PcPb = qCeil(vt2dP.dotProduct(vt2dP, (Pc-Pb)));
                fPcPb_PcPb = qCeil(vt2dP.dotProduct((Pc-Pb), (Pc-Pb)));

                if(fP_PaPb >= 0 && fP_PaPb <= fPaPb_PaPb && // Se 0 < P→ . (Pa-Pb)→ < |Pa-Pb|² E 0 < P→ . (Pc-Pb)→ < |Pc-Pb|² o ponto P3 está
                   fP_PcPb >= 0 && fP_PcPb <= fPcPb_PcPb  ) // dentro do retângulo
                    return true;
            }
        }
    }
    return false;
}

void clsMapa::vAdicionaRobo(QVector2D _vt2dPosicaoRobo, TipoObjeto _otRoboAliado, int _iRoboID)//Adiciona um robô ao mapa a partir das coordenadas da visão
{                                                       ////OBS:É importante que essas coordenadas sejam sempre positivas,ou seja, usar a origem das coordenadas no canto do campo
    //Assumindo que a posição rebecida do robô seja a do centro dele, esta parte irá pegar a posição do canto direito inferior do robô e convertê-la
    //para valores que são apropriados para os indíces da matriz mapa utilizada
    QVector2D initialPosition = vt2dConverteCoordenadaRoboParaMatriz(_vt2dPosicaoRobo);/*QVector2D(qAbs((robotPosition.x()-(robotDiameter/2))/fieldResolution), qAbs((robotPosition.y()-(robotDiameter/2))/fieldResolution));*/

    for(int i=0; i<iQuadradosRobo; i++)
    {
        for(int j=0; j<iQuadradosRobo; j++)
        {
            if(initialPosition.x() + i >= iXMax || initialPosition.y() + j >= iYMax)
            {
                return;
            }
            if(initialPosition.x() + i <    0  || initialPosition.y() + j <    0 )
            {
                return;
            }
        }
    }

    //cel = byCriaCodigoObjeto(_iRoboID, _otRoboAliado, true);
    Ponto pRobo;
    pRobo.objPrimario = Objeto(_otRoboAliado, _iRoboID);
    pRobo.bImprimirImagem = true;
    pRobo.segSeguranca = Nivel_1;

    ptoMapa[(int)initialPosition.x()][(int)initialPosition.y()] = pRobo;

    //cel = cel & removeInfoFirstCell;//Retira a informação de posição inicial

    pRobo.bImprimirImagem = false;

    vt4dPosicoesObjetos.append(QVector4D(initialPosition, _otRoboAliado, _iRoboID));


    if(_otRoboAliado == otAliado)
        vDesenhaAreaSeguranca(initialPosition, globalConfig.robotDiameter/2, pRobo, globalConfig.safety.ally);
    else
        vDesenhaAreaSeguranca(initialPosition, globalConfig.robotDiameter/2, pRobo, globalConfig.safety.opponent);


    pRobo.segSeguranca = Nivel_2;

    for(int i=1; i<iQuadradosRobo; i++)
    {
        for(int j=1; j<iQuadradosRobo; j++)
        {
            if(ptoMapa[(int)initialPosition.x()+i][(int)initialPosition.y()+j].bImprimirImagem == false)
                ptoMapa[(int)initialPosition.x()+i][(int)initialPosition.y()+j] =  pRobo;
        }
    }
}

void clsMapa::vRemoveRobo(QVector2D _vt2dPosicaoInicialRobo)//Recebe a posição inicial do robô e limpa os quadrados que o mesmo ocupa
{
    for(int i=0; i<iQuadradosRobo; i++)
    {
        for(int j=0; j<iQuadradosRobo; j++)
        {
            ptoMapa[(int)_vt2dPosicaoInicialRobo.x()+i][(int)_vt2dPosicaoInicialRobo.y()+j] = Ponto() ;
        }
    }
}

void clsMapa::vAdicionaBola(QVector2D _vt2dPosicaoBola)//Adiciona uma bola ao mapa a partir das coordenadas da visão
{                                        //OBS:É importante que essas coordenadas sejam sempre positivas,ou seja, usar a origem das coordenadas no canto do campo
    QVector2D initialPosition = vt2dConverteCoordenadaBolaParaMatriz(_vt2dPosicaoBola);

    for(int i=0; i<iQuadradosBola; i++)
    {
        for(int j=0; j<iQuadradosBola; j++)
        {
            if(initialPosition.x() + i >= iXMax || initialPosition.y() + j >= iYMax)
            {
                return;
            }
            if(initialPosition.x() + i < 0     || initialPosition.y() + j  <  0  )
            {
                return;
            }
        }
    }

    Ponto pBola;
    pBola.objPrimario = Objeto(otBola, 0);
    pBola.bImprimirImagem = true;
    pBola.segSeguranca = Nivel_1;


    ptoMapa[(int)initialPosition.x()][(int)initialPosition.y()] = pBola;

    pBola.bImprimirImagem = false;

    vt4dPosicoesObjetos.append(QVector4D(initialPosition, otBola, 0));

    vDesenhaAreaSeguranca(initialPosition, globalConfig.ballDiameter/2, pBola, iRaioSegurancaBola);

    pBola.segSeguranca = Nivel_2;

    for(int i=1; i<iQuadradosBola; i++)
    {
        for(int j=1; j<iQuadradosBola; j++)
        {
            if(ptoMapa[(int)initialPosition.x()+i][(int)initialPosition.y()+j].bImprimirImagem == false)
                ptoMapa[(int)initialPosition.x()+i][(int)initialPosition.y()+j] = pBola ;
        }
    }
}

void clsMapa::vRemoveBola(QVector2D _vt2dPosicaoInicialBola)//Recebe a posição inicial da bola e limpa os quadrados que a mesma ocupa
{
    for(int i=0; i<iQuadradosBola; i++)
    {
        for(int j=0; j<iQuadradosBola; j++)
        {
            ptoMapa[(int)_vt2dPosicaoInicialBola.x()+i][(int)_vt2dPosicaoInicialBola.y()+j] = Ponto() ;
        }
    }
}

//void clsMapa::vDesenhaLinhasDeContorno(int _iOffSet)
//{
//    u_int8_t cel = otLinhaDeCampo;
//    cel = cel << 4;
//    cel = cel | occupiedCell;

//    for(int j = _iOffSet; j <= iYMax-_iOffSet; j++)//Introduz as linhas de campo do eixo y ao mapa
//    {
//        byMapa[_iOffSet][j] = cel;
//        byMapa[iXMax-_iOffSet][j] = cel;
//    }


//    for(int i=_iOffSet; i <= iXMax-_iOffSet; i++)//Introduzr as linhas de campo do eixo x ao mapa
//    {
//        byMapa[i][_iOffSet] = cel;
//        byMapa[i][iXMax-_iOffSet] = cel;
//    }
//}

void clsMapa::vDesenhaAreaSeguranca(QVector2D _vt2dPosicaoObjeto, int _iRaioObjeto, Ponto _pCodigoObjeto, int _iRaioSeguranca)
{
    QVector2D PosicaoCentralObjeto = _vt2dPosicaoObjeto + QVector2D(qCeil( ((float)_iRaioObjeto)/fResolucaoCampo), qCeil( ((float)_iRaioObjeto)/fResolucaoCampo) );
    int x1 = (int)PosicaoCentralObjeto.x() - qCeil(_iRaioSeguranca/fResolucaoCampo);
    int x2 = (int)PosicaoCentralObjeto.x() + qCeil(_iRaioSeguranca/fResolucaoCampo);
    int y1 = (int)PosicaoCentralObjeto.y() - qCeil(_iRaioSeguranca/fResolucaoCampo);
    int y2 = (int)PosicaoCentralObjeto.y() + qCeil(_iRaioSeguranca/fResolucaoCampo);

    _pCodigoObjeto.segSeguranca = Nivel_1;

    if(x2 >= iXMax || x2 < 0)return;
    if(x1 >= iXMax || x1 < 0)return;

    if(y1 >= iYMax || y1 < 0)return;
    if(y2 >= iYMax || y2 < 0)return;


    for(int j = y1; j < y2; j++)
    {
        if( j >= iYMax || j < 0) return;

        for(int i = x1; i < x2; i++)
        {
            if( i >= iXMax || i < 0) return;



            if(ptoMapa[i][j].bEstaVazio() == true || (ptoMapa[i][j].bEstouAqui(_pCodigoObjeto.objPrimario)  == true  && ptoMapa[i][j].bImprimirImagem == false))
            {
                ptoMapa[i][j] = _pCodigoObjeto;
            }
            else //Nao e a primeira celula de algum objeto
            {
                ptoMapa[i][j].objObjetos.append(_pCodigoObjeto.objPrimario) ;
            }
        }
    }

    x1 = (int)PosicaoCentralObjeto.x() - qCeil((_iRaioSeguranca/2)/fResolucaoCampo);
    x2 = (int)PosicaoCentralObjeto.x() + qCeil((_iRaioSeguranca/2)/fResolucaoCampo);
    y1 = (int)PosicaoCentralObjeto.y() - qCeil((_iRaioSeguranca/2)/fResolucaoCampo);
    y2 = (int)PosicaoCentralObjeto.y() + qCeil((_iRaioSeguranca/2)/fResolucaoCampo);

    _pCodigoObjeto.segSeguranca = Nivel_1;

    if(x2 >= iXMax || x2 < 0)return;
    if(x1 >= iXMax || x1 < 0)return;

    if(y1 >= iYMax || y1 < 0)return;
    if(y2 >= iYMax || y2 < 0)return;


    for(int j = y1; j < y2; j++)
    {
        if( j >= iYMax || j < 0) return;

        for(int i = x1; i < x2; i++)
        {
            if( i >= iXMax || i < 0) return;



            if(ptoMapa[i][j].bEstaVazio() == true || (ptoMapa[i][j].bEstouAqui(_pCodigoObjeto.objPrimario)  == true  && ptoMapa[i][j].bImprimirImagem == false))
            {
                ptoMapa[i][j] = _pCodigoObjeto;
            }
            else if(ptoMapa[i][j].segSeguranca == Nivel_1)//Nao e a primeira celula de algum objeto
            {
                ptoMapa[i][j].objObjetos.append(_pCodigoObjeto.objPrimario) ;
            }
        }
    }

}

void clsMapa::vRemoveAreaSeguranca(int _iIndicePosicaoObjeto, int _iRaioObjeto, int _iRaioSeguranca)
{
    quint8 otTipoObjeto = static_cast<int>(vt4dPosicoesObjetos[_iIndicePosicaoObjeto].z());
//    otTipoObjeto = otTipoObjeto >> 4;

    if(otTipoObjeto == otAliado || otTipoObjeto == otOponente || otTipoObjeto == otBola)
    {
        QVector2D objectCenterPos = vt4dPosicoesObjetos[_iIndicePosicaoObjeto].toVector2D() + QVector2D(qCeil(_iRaioObjeto/(fResolucaoCampo)), qCeil(_iRaioObjeto/(fResolucaoCampo)));
        int x1 = (int)objectCenterPos.x() - qCeil(_iRaioSeguranca/fResolucaoCampo);
        int x2 = (int)objectCenterPos.x() + qCeil(_iRaioSeguranca/fResolucaoCampo);
        int y1 = (int)objectCenterPos.y() - qCeil(_iRaioSeguranca/fResolucaoCampo);
        int y2 = (int)objectCenterPos.y() + qCeil(_iRaioSeguranca/fResolucaoCampo);

        if(x2 >= iXMax || x2 < 0 || x1 >= iXMax || x1 < 0 || y1 >= iYMax || y1 < 0 || y2 >= iYMax || y2 < 0)
        {
            return;
        }


        for(int j = y1; j < y2; j++)
        {
            if( j >= iYMax || j < 0)
            {
                return;
            }

            for(int i = x1; i < x2; i++)
            {
                if( i >= iXMax || i < 0)
                {
                    return;
                }

                ptoMapa[i][j] = Ponto();
            }
        }

    }
}

void clsMapa::vDesenhaAreaGol(Ponto _pCodigoGoleiro, int _iLarguraAreaPenalty, int _iProfundidadeAreaPenalty, int _iLadoDefesa)
{
    _iLarguraAreaPenalty = _iLarguraAreaPenalty/fResolucaoCampo;
    _iProfundidadeAreaPenalty = _iProfundidadeAreaPenalty/fResolucaoCampo;

    _pCodigoGoleiro.segSeguranca = Nivel_1;

    int x1,y1,x2,y2;
    if(_iLadoDefesa == XNegativo)
    {
        x1 = (globalConfig.fieldOffset+iEspessuraLinha)/fResolucaoCampo;
        y1 = iYMax/2 + _iLarguraAreaPenalty/2;
        x2 = (globalConfig.fieldOffset+iEspessuraLinha)/fResolucaoCampo + _iProfundidadeAreaPenalty;
        y2 = iYMax/2 - _iLarguraAreaPenalty/2;

        for(int i = x1; i <= x2; ++i)
        {
            for(int j = y1; j >= y2; --j)
            {
                if(ptoMapa[i][j].bEstaVazio() == true)
                    ptoMapa[i][j] = _pCodigoGoleiro;

            }
        }
    }

    else if(_iLadoDefesa == XPositivo)
    {
        x1 = iXMax - (globalConfig.fieldOffset+iEspessuraLinha)/fResolucaoCampo - _iProfundidadeAreaPenalty;
        y1 = iYMax/2 + _iLarguraAreaPenalty/2;
        x2 = iXMax - (globalConfig.fieldOffset+iEspessuraLinha)/fResolucaoCampo;
        y2 = iYMax/2 - _iLarguraAreaPenalty/2;

        for(int i = x1; i <= x2; ++i)
        {
            for(int j = y1; j >= y2; --j)
            {
                if(ptoMapa[i][j].bEstaVazio() == true)
                    ptoMapa[i][j] = _pCodigoGoleiro;

            }
        }
    }

}

void clsMapa::vRemoveAreaGol(int _iLarguraAreaPenalty, int _iProfundidadeAreaPenalty, int _iLadoDefesa)
{
    _iLarguraAreaPenalty = _iLarguraAreaPenalty/fResolucaoCampo;
    _iProfundidadeAreaPenalty = _iProfundidadeAreaPenalty/fResolucaoCampo;

    int x1,y1,x2,y2;
    if(_iLadoDefesa == XNegativo)
    {
        x1 = (globalConfig.fieldOffset+iEspessuraLinha)/fResolucaoCampo;
        y1 = iYMax/2 + _iLarguraAreaPenalty/2;
        x2 = (globalConfig.fieldOffset+iEspessuraLinha)/fResolucaoCampo + _iProfundidadeAreaPenalty;
        y2 = iYMax/2 - _iLarguraAreaPenalty/2;

        for(int i = x1; i <= x2; ++i)
        {
            for(int j = y1; j >= y2; --j)
            {
                ptoMapa[i][j] = Ponto();
            }
        }
    }

    else if(_iLadoDefesa == XPositivo)
    {
        x1 = iXMax - (globalConfig.fieldOffset+iEspessuraLinha)/fResolucaoCampo - _iProfundidadeAreaPenalty;
        y1 = iYMax/2 + _iLarguraAreaPenalty/2;
        x2 = iXMax - (globalConfig.fieldOffset+iEspessuraLinha)/fResolucaoCampo;
        y2 = iYMax/2 - _iLarguraAreaPenalty/2;

        for(int i = x1; i < x2; ++i)
        {
            for(int j = y1; j > y2; --j)
            {
                ptoMapa[i][j] = Ponto();
            }
        }
    }
}

void clsMapa::vDesenhaCaminhos(QVector<QVector2D>& _vt2dCaminho, Ponto _pCodigoObjeto)
{
    QVector2D vetorDeConversao(((vt2dTamanhoCampo.x()-iEspessuraLinha)/2)+globalConfig.fieldOffset, ((vt2dTamanhoCampo.y()-iEspessuraLinha)/2)+globalConfig.fieldOffset);

    for(int n=0; n < _vt2dCaminho.size(); ++n)
    {
        _vt2dCaminho.replace(n, (_vt2dCaminho[n] + vetorDeConversao) / fResolucaoCampo);

        ptoMapa[(int)_vt2dCaminho[n].x()][(int)_vt2dCaminho[n].y()] = _pCodigoObjeto;
    }


}

void clsMapa::vRemoveCaminhos()
{
    QVector2D vetorDeConversao(((vt2dTamanhoCampo.x()-iEspessuraLinha)/2)+globalConfig.fieldOffset, ((vt2dTamanhoCampo.y()-iEspessuraLinha)/2)+globalConfig.fieldOffset);

    for(auto & i : vt2dCaminho)
    {
        if(!i.isEmpty())
        {
            for(auto & n : i)
            {
                n = (n + vetorDeConversao) / fResolucaoCampo;

                ptoMapa[(int)n.x()][(int)n.y()] = Ponto();
            }
        }
    }
}

void clsMapa::vLimpaMapa()
{
    int i=vt4dPosicoesObjetos.size() - 1;//Último objeto na lista
    quint8 otTipoObjeto = otDestino;

    while(vt4dPosicoesObjetos.size() != 0 && i >= 0)
    {
        otTipoObjeto = static_cast<int>(vt4dPosicoesObjetos[i].z());
//        otTipoObjeto = otTipoObjeto >> 4;

        switch (otTipoObjeto/*(int)vt3dPosicoesObjetos[i].z()*/)
        {

        case otAliado:
            vRemoveRobo(vt4dPosicoesObjetos[i].toVector2D());
            vRemoveAreaSeguranca(i, globalConfig.robotDiameter/2, globalConfig.safety.ally);
            vt4dPosicoesObjetos.removeLast();
            i--;
            break;

        case otOponente:
            vRemoveRobo(vt4dPosicoesObjetos[i].toVector2D());
            vRemoveAreaSeguranca(i, globalConfig.robotDiameter/2, globalConfig.safety.opponent);
            vt4dPosicoesObjetos.removeLast();
            i--;
            break;

        case otBola:
            vRemoveBola(vt4dPosicoesObjetos[i].toVector2D());
            vRemoveAreaSeguranca(i, globalConfig.ballDiameter/2, iRaioSegurancaBola);
            vt4dPosicoesObjetos.removeLast();
            i--;
            break;

        default:
            break;

        }
    }

    vRemoveCaminhos();
    vRemoveAreaGol(iLarguraAreaPenalty, iProfundiadePenalty, XPositivo);
    vRemoveAreaGol(iLarguraAreaPenalty, iProfundiadePenalty, XNegativo);

}

void clsMapa::vResetaMapa()
{
    for(int i=0; i<iXMax; i++)
    {
        for(int j=0; j<iYMax; j++)
        {
            ptoMapa[i][j] = Ponto();
        }
    }
}
