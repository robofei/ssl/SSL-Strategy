﻿#ifndef PATHMAP_H
#define PATHMAP_H

///
/// \file pathmap.h
/// \brief \a clsMapa, \a Objeto e \a Ponto
///


#include <QRect>
#include <QtMath>
#include <QVector>
#include <QVector2D>
#include <QVector4D>
#include <QDebug>

#include <algorithm>
#include <queue>
#include <set>
#include <stdlib.h>

#include <Ambiente/futbolenvironment.h>


#define bCoordenadaReal 0 // Tipos de coordenada, a real significa que estamos trabalhando diretamente com os valores do campo (-X, +X; -Y, +Y)
#define bCoordenadaMapa 1 // a do mapa significa que estamos trabalhando com as coordenadas ajustadas, ou seja, sempre positivas e reduzidas de acordo com
                          //a resolução do campo

/**
 * @brief Seguranca
 *
 */
enum Seguranca
{
    Nivel_1 ,//Para metade do raio de seguranca para frente
    Nivel_2 ,//Para metade do raio de seguranca para dentro
};

/**
 * @brief
 *
 */
typedef struct Objeto
{
    TipoObjeto otTipo; /**< TODO: describe */
    int iID; /**< TODO: describe */

    /**
     * @brief
     *
     * @param _otTipo
     * @param _iID
     */
    Objeto(TipoObjeto _otTipo = otDestino, int _iID = 0);

    /**
     * @brief
     *
     * @param _o1
     * @return bool operator
     */
    inline bool operator == (const Objeto& _o1) const
    {
        return (_o1.otTipo == this->otTipo && _o1.iID == this->iID);
    }
/**
 * @brief
 *
 */
}Objeto;

/**
 * @brief
 *
 */
typedef struct Ponto
{
    bool bImprimirImagem; /**< TODO: describe */
    Objeto objPrimario; /**< TODO: describe */
    QVector<Objeto> objObjetos; /**< TODO: describe */
    Seguranca segSeguranca; /**< TODO: describe */


    /**
     * @brief
     *
     */
    Ponto();

    /**
     * @brief
     *
     * @return bool
     */
    bool bEstaVazio();

    /**
     * @brief
     *
     * @param _objObjeto
     * @return bool
     */
    bool bEstouAqui(Objeto _objObjeto);
/**
 * @brief
 *
 */
}Ponto;


/**
 * @brief
 *
 */
class clsMapa
{

public:

    /**
     * @brief
     *
     * @param xSize
     * @param ySize
     */
    clsMapa(int xSize, int ySize);

    /**
     * @brief
     *
     */
    ~clsMapa();

    /*
     *Retorna o tamanho máximo do mapa
    */
    QVector2D vt2dTamanhoTotalMatrizMapa();

    /*
     *Atualiza a posição de todos os objetos de acordo com os dados da visão
    */
    void vAtualiza(AmbienteCampo *ambiente);

    /*
     *Recebe coordenadas positivas de um robô e as adapta à escala da matriz
    */
    QVector2D vt2dConverteCoordenadaRoboParaMatriz(QVector2D _vt2dPosicaoObjeto);

    /*
     *Recebe coordenadas positivas de uma bola e as adapta à escala da matriz
    */
    QVector2D vt2dConverteCoordenadaBolaParaMatriz(QVector2D _vt2dPosicaoObjeto);

    /*
     *Verifica se há algum obstáculo naquela posição
    */
    bool bChecaObstaculo(QVector2D _vt2dPosicaoTeste, int _iRoboID, bool _bIgnorarBola, bool _bTipoPathCoordenada);

    /*
     *Verifica se há intersecção entre a linha determinada pelos pontos P1=(x1,y1) e P2=(x2,y2)
    */
    bool bChecaInterseccaoLinha(QVector2D _vt2dP1, QVector2D _vt2dP2, int _iRoboID, bool _bIgnorarBola, bool _bTipoCoordenada, float _fDistanciaMinima);

    /*
     *Limpa todas as posições do mapa
    */
    void vResetaMapa();

    std::vector<std::vector<Ponto> > ptoMapa; /**< TODO: describe */

protected:

    /*
     *Insere um objeto do tipo robô ao mapa
    */
    void vAdicionaRobo(QVector2D _vt2dPosicaoRobo, TipoObjeto _otRoboAliado, int _iRoboID);

    /*
     * Remove um objeto do tipo robô do mapa
    */
    void vRemoveRobo(QVector2D _vt2dPosicaoInicialRobo);

    /*
     *Insere um objeto do tipo bola ao mapa
    */
    void vAdicionaBola(QVector2D _vt2dPosicaoBola);

    /*
     *Remove um objeto do tipo bola do mapa
    */
    void vRemoveBola(QVector2D _vt2dPosicaoInicialBola);

    /*
     *Insere as linhas de contorno do campo
    */
//    void vDesenhaLinhasDeContorno(int _iOffSet);

    /*
     *Insere uma área de segurança entorno de um determinado objeto
     *A área de segurança é um retângulo definido pelos pontos P1=(x1,y1) e P2=(x2,y2)
    */
    void vDesenhaAreaSeguranca(QVector2D _vt2dPosicaoObjeto, int _iRaioObjeto, Ponto _pCodigoObjeto, int _iRaioSeguranca);

    /*
     *Remove a área de segurança entorno de um determinado objeto
     *A área de segurança é um retângulo definido pelos pontos P1=(x1,y1) e P2=(x2,y2)
    */
    void vRemoveAreaSeguranca(int _iIndicePosicaoObjeto, int _iRaioObjeto, int _iRaioSeguranca);

    /*
     *Insere uma área de segurança entorno do gol
     *A área é um retângulo definido pelos pontos P1=(x1,y1) e P2=(x2,y2)
    */
    void vDesenhaAreaGol(Ponto _pCodigoGoleiro, int _iLarguraAreaPenalty, int _iProfundidadeAreaPenalty, int _iLadoDefesa);

    /*
     * Remove a área de segurança entorno do gol
    */
    void vRemoveAreaGol(int _iLarguraAreaPenalty, int _iProfundidadeAreaPenalty, int _iLadoDefesa);

    /*
     * Insere um caminho gerado
    */
    void vDesenhaCaminhos(QVector<QVector2D> &_vt2dCaminho, Ponto _pCodigoObjeto);

    /*
     * Remove um caminho gerado
    */
    void vRemoveCaminhos();

    /*
     *Função utilizada para limpar todas as posições do mapa que não são nulas
    */
    void vLimpaMapa();

    int iXMax;//Valor máximo do indíce X do mapa /**< TODO: describe */
    int iYMax;//Valor máximo do indíce Y do mapa /**< TODO: describe */
    QVector2D vt2dTamanhoCampo; /**< TODO: describe */
    QVector<QVector2D> vt2dCaminho[PLAYERS_PER_SIDE]; /**< TODO: describe */

    int iLarguraAreaPenalty, iProfundiadePenalty; /**< TODO: describe */
    int iLadoDefesa; /**< TODO: describe */

    int iQuadradosRobo = qCeil(globalConfig.robotDiameter/fResolucaoCampo);//Arredonda para cima o número de quadrados que o robô ocupa /**< TODO: describe */
    int iQuadradosBola = qCeil(globalConfig.ballDiameter/fResolucaoCampo); //Arredonda para cima o número máximo de quadrados que uma bola pode ocupar /**< TODO: describe */

    /*
     *Salva a posição inicial de cada regiao que possui um obtáculo para facilitar a atualização das mesmas
     *Nas duas primeiras coordenadas possui o índice inicial de todos os objetos no mapa, por exemplo, robôs,
     *e na terceira coordenada qual tipo de objeto é, por exemplo, robô aliado ou bola
    */
    QVector<QVector4D> vt4dPosicoesObjetos;


};

#endif // PATHMAP_H
