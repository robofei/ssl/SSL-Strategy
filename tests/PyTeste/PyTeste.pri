
HEADERS += \
    $$PWD/pyteste.h


SOURCES += \
    $$PWD/pyteste.cpp


INCLUDEPATH += \
    /usr/include/python3.8


LIBS += \
    -L/usr/lib/ -lpython3.8

DISTFILES += \
    $$PWD/py_teste.py
