#include "pyteste.h"

PyTeste* PyTeste::_py_teste = nullptr;

void PyTeste::runSimpleString(const char* s)
{
    PyTeste::instance()->_run_simple_string(s);
}

void PyTeste::runFromFile(const char* fileName)
{
    PyTeste::instance()->_run_from_file(fileName);
}

quint8 PyTeste::getPassEvaluation(const quint8 &param0,
                                  const quint8 &param1,
                                  const quint8 &param2,
                                  const quint8 &param3,
                                  const quint8 &param4,
                                  const quint8 &param5,
                                  const quint8 &param6,
                                  const quint8 &param7)
{
    return PyTeste::instance()->_get_pass_evaluation(param0,
                                                     param1,
                                                     param2,
                                                     param3,
                                                     param4,
                                                     param5,
                                                     param6,
                                                     param7);
}

void PyTeste::releaseInstance()
{
    delete PyTeste::_py_teste;
    PyTeste::_py_teste = nullptr;
}

void PyTeste::resetInstance()
{
    PyTeste::releaseInstance();
    PyTeste::_py_teste = new PyTeste();
}

void PyTeste::test()
{
    PyTeste::instance()->_test();
}


PyTeste::PyTeste()
{
    py::initialize_interpreter();
    qInfo("Interoreter initialized");
}
PyTeste::~PyTeste()
{
    py::finalize_interpreter();
    qInfo("Interoreter finalized");
}

PyTeste* PyTeste::instance()
{
    if(!PyTeste::_py_teste)
        PyTeste::_py_teste = new PyTeste();

    return PyTeste::_py_teste;
}

void PyTeste::_test()
{
    py::module sys = py::module::import("sys");
//    py::module numpy = py::module::import("n+umpy");
//    py::module sklearn = py::module::import("sklearn");

    qDebug()<<"Python Version: " + QString::fromStdString(static_cast<std::string>(py::str(sys.attr("version"))));
//    qDebug()<<"numpy Version: " + QString::fromStdString(static_cast<std::string>(py::str(numpy.attr("__version__"))));
//    qDebug()<<"sklearn Version: " + QString::fromStdString(static_cast<std::string>(py::str(sklearn.attr("__version__"))));

    py::module py_teste = py::module::import("py_teste");

    py::object result = py_teste.attr("make_prediction")(
                QRandomGenerator::global()->bounded(0,250),
                QRandomGenerator::global()->bounded(0,250));
    qDebug()<<result.cast<quint8>();
}

void PyTeste::_run_simple_string(const char* s)
{
    py::exec(s);
}

void PyTeste::_run_from_file(const char* file_name)
{

    QFile filePy(file_name);

    if(filePy.open(QIODevice::ReadOnly)){

        QByteArray code = filePy.readAll();
        qDebug()<<code.data();
        py::exec(code.data());
    }
}

quint8 PyTeste::_get_pass_evaluation(const quint8 &param0,
                                     const quint8 &param1,
                                     const quint8 &param2,
                                     const quint8 &param3,
                                     const quint8 &param4,
                                     const quint8 &param5,
                                     const quint8 &param6,
                                     const quint8 &param7)
{
    py::module pass_evaluation = py::module::import("realiza_previsao_passe");

    py::object result = pass_evaluation.attr("realiza_previsao")(param0,
                                                                 param1,
                                                                 param2,
                                                                 param3,
                                                                 param4,
                                                                 param5,
                                                                 param6,
                                                                 param7
                                                                 );
    return result.cast<quint8>();


}
