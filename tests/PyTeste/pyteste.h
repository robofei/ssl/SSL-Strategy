#ifndef PYTESTE_H
#define PYTESTE_H

#include <QObject>
#include <QDebug>
#include <QRandomGenerator>
#include <QFile>

#ifdef PY_SSIZE_T_CLEAN
#define PY_SSIZE_T_CLEAN
#endif

#pragma push_macro("slots")
#undef slots
#include "Python.h"
#pragma pop_macro("slots")

#include "pybind11/pybind11.h"
#include "pybind11/embed.h"
namespace py = pybind11;

class PyTeste : public QObject
{
    Q_OBJECT
public:
    static void runSimpleString(const char* s);
    static void runFromFile(const char *fileName);
    static quint8 getPassEvaluation(const quint8& param0,
                                    const quint8& param1,
                                    const quint8& param2,
                                    const quint8& param3,
                                    const quint8& param4,
                                    const quint8& param5,
                                    const quint8& param6,
                                    const quint8& param7);
    static void releaseInstance();
    static void resetInstance();
    static void test();

private:
    PyTeste();
    ~PyTeste();

    static PyTeste* instance();
    static PyTeste* _py_teste;

    void _test();
    void _run_simple_string(const char *s);
    void _run_from_file(const char *file_name);

    quint8 _get_pass_evaluation(const quint8& param0,
                                const quint8& param1,
                                const quint8& param2,
                                const quint8& param3,
                                const quint8& param4,
                                const quint8& param5,
                                const quint8& param6,
                                const quint8& param7);
};

#endif // PYTESTE_H
