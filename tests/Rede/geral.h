#ifndef GERAL_H
#include "protofiles/referee.pb.h"
#include <QApplication>
#include <algorithm>
#include <arpa/inet.h>
#include <cstdio>
#include <ifaddrs.h>
#include <iostream>
#include <sstream>
#include <sys/socket.h>

#define GERAL_H
class NetWorksInterfaces{
   private:

      std::vector<QString> ifaces;
      std::vector<QString> ips;
   public:
      NetWorksInterfaces();

      //retorna as interfaces
//      std::vector <QString> getIfaces();
//      QString getIface(int _i);
//      int getCountIface();
};


class SSL_Ref{
public:
  static QString getStage(const SSL_Referee & ssl_referee);
  static QString getCommand(const SSL_Referee & ssl_referee);
};

#endif // GERAL_H
