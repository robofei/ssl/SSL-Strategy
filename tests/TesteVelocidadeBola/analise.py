import os
from matplotlib import pyplot
import numpy
from typing import Union
from glob import glob as glob
from scipy.optimize import curve_fit
from scipy.signal import savgol_filter

# Dark theme
pyplot.style.use('dark_background')


def do_stuff(file_name: str):

    TEMPO_CORTE = .42

    # Carrega o array
    data: numpy.ndarray = numpy.genfromtxt(
        file_name,
        dtype=numpy.float64,
        delimiter=";",
        skip_header=1
    )

    # Exclue os pontos antes da bola ser chutada
    # Considera-se que a bola foi chutada quando esta atinge
    #  sua velocidade máxima
    data = data[data[:, 2].argmax() : -1]

    # Exclue o ponto depois da bola já estar parada
    for i, x in enumerate(data):
        if x[2] > .01:
            continue
        data = data[0:i]
        break

    # Considera o tempo inicial igual a 0
    data[:, 1] = data[:, 1] - data[0, 1]

    velocidade_kalman: numpy.ndarray = data[:, 2]
    # Smooth o gráfico da velocidade para amenizar os ruídos
    # velocidade_kalman = savgol_filter(velocidade_kalman, 151, 7)

    distancia: numpy.ndarray = data[:, 0] * 1e-3
    tempo: numpy.ndarray = data[:, 1] * 1e-9
    velocidade_maxima: numpy.ndarray = numpy.amax(velocidade_kalman)

    def distancia_pelo_tempo(t: Union[numpy.float64, numpy.ndarray],
                             a: numpy.float64,
                             b: numpy.float64) -> numpy.float64:
        return a * (1 - numpy.exp(-b * t))

    def velocidade_pelo_tempo(t: Union[numpy.float64, numpy.ndarray],
                              a0: numpy.float64,
                              b0: numpy.float64,
                              a1: numpy.float64,
                              b1: numpy.float64):
        if type(t) is numpy.ndarray:
            v = numpy.array(t, copy=True)
            for i, var in enumerate(t):
                # o tempo de corte deve ser colocado manualmente
                #  para facilitar a predição dos parâmetros
                if var < TEMPO_CORTE:
                    v[i] = b0 - a0 * var
                else:
                    v[i] = b1 - a1 * var
            return v

        if t < TEMPO_CORTE:
            return b0 - a0 * var
        return b1 - a1 * var

    [a, b], covariance_d = curve_fit(
        distancia_pelo_tempo,
        tempo,
        distancia,
        maxfev=5000
    )

    [a0, b0, a1, b1], covariance_v = curve_fit(
        velocidade_pelo_tempo,
        tempo,
        velocidade_kalman,
        maxfev=10000
    )

    v_m = b0
    v_c = v_m - a0 * TEMPO_CORTE
    t_m = v_c/a1 + TEMPO_CORTE

    # print('\n\n')
    # print(" d = {0} * (1 - exp(-{1} * x))".format(a, b))
    # print('\n')
    # print("a0 = {0}\nb0 = {1}\na1 = {2}\nb1 = {3}".format(a0, b0, a1, b1))
    print("v_max = {0}\nv_corte = {1}\nt_corte = {2}\nt_max = {3}".format(v_m, v_c, TEMPO_CORTE, t_m))
    print("dist_max = {0}\ndist_coeficiente = {1}".format(a, b))

    fig, axs = pyplot.subplots(3, 1)

    fig.suptitle('Velocidade da bola')

    axs[0].set_title('Velocidade pelo tempo')
    axs[0].set_xlabel('tempo (s)')
    axs[0].set_ylabel('Velocidade (m/s)')
    axs[0].plot(tempo, velocidade_kalman, 'r')
    axs[0].plot(tempo, velocidade_pelo_tempo(tempo, a0, b0, a1, b1), 'y')
    axs[0].set_ylim([0, velocidade_maxima])

    axs[1].set_title('Distância pelo tempo')
    axs[1].set_xlabel('tempo (s)')
    axs[1].set_ylabel('Distância (m)')
    axs[1].plot(tempo, distancia, 'g')
    axs[1].plot(tempo, distancia_pelo_tempo(tempo, a, b), 'y')
    axs[1].set_ylim([0, numpy.amax(distancia)])

    axs[2].set_title('Velocidade pela distância')
    axs[2].set_xlabel('Distância (m)')
    axs[2].set_ylabel('Velocidade (m/s)')
    axs[2].plot(distancia, velocidade_kalman, 'g')
    axs[2].plot(distancia_pelo_tempo(tempo, a, b), velocidade_pelo_tempo(tempo, a0, b0, a1, b1), 'y')
    axs[2].set_ylim([0, numpy.amax(velocidade_kalman)])

    pyplot.show()
    return (v_m, v_c, TEMPO_CORTE, t_m, a, b)


list_of_parameters = []

for f in glob(os.path.dirname(os.path.realpath(__file__)) + '/campo_real/forca50/*.csv'):
    print("\n\n{}\n".format(f))
    list_of_parameters.append(do_stuff(f))

sum_param = [sum(i) for i in zip(*list_of_parameters)]

parameters = [i / len(list_of_parameters) for i in sum_param]


print("\nResultados finais:\n")

print("{0}\n{1}\n{2}\n{3}".format(parameters[0], parameters[1], parameters[2], parameters[3]))
print("{0}\n{1}".format(parameters[4], parameters[5]))
