import os
from matplotlib import pyplot
import numpy
from glob import glob as glob
from scipy.optimize import curve_fit
from scipy.optimize import least_squares
from cartesian import cartesian
from sklearn.metrics import r2_score

# from scipy.signal import savgol_filter

def carrega_dados(nome_arquivo, comprime=True):
    
    # Carrega o array
    data = numpy.genfromtxt(
        nome_arquivo,
        dtype=numpy.float64,
        delimiter=";",
        skip_header=1
    )

    # Distância: data[:, 0]
    # Tempo: data[:, 1]
    # Velocidade: data[:, 2]
    
    # Exclue os pontos antes da bola ser chutada
    # Considera-se que a bola foi chutada quando esta atinge
    #  sua velocidade máxima
    if comprime:
        data = data[data[:, 2].argmax() : -1]

        # Exclue o ponto depois da bola já estar parada
        for i, x in enumerate(data):
            if x[2] > .01:
                continue
            data = data[0:i]
            break

        # Considera o tempo inicial igual a 0
        data[:, 1] = data[:, 1] - data[0, 1]

        # Distância inicial 0
        data[:, 0] = data[:, 0] - data[0, 0]
        
    return {
        "Distância":  data[:, 0]*1e-3,
        "Tempo":      data[:, 1]*1e-9,
        "Velocidade": data[:, 2]
    }


def gera_tm(te, ae, ar, vm):
    return te*(1 - ae/ar) - vm/ar

def gera_v0(ar, tm):
    return -ar*tm

def velocidade_single(t, ar, vm, ae, te, tm, v0):
    if t<te and t>=0:
        return vm + t*ae
    if t>=te and t<tm:
        return v0 + t*ar
    return 0

def velocidade(t, ar, vm, ae, te):
    tm = gera_tm(te, ae, ar, vm)
    v0 = gera_v0(ar, tm)
    
    v = numpy.array(t, copy=True)
    
    for i, ti in enumerate(t):
        v[i] = velocidade_single(ti, ar, vm, ae, te, tm, v0)
    return v

def gera_se(te, ae, vm):
    return te**2*ae/2 + te*vm

def gera_sm(tm, ar, v0, se):
    return tm**2*ar/2 + tm*v0 + se
    
def distancia_single(t, ar, vm, ae, te, tm, v0, se, sm):
    if t<0:
        return 0
    if t<te and t>=0:
        return t**2*ae/2 + t*vm
    if t>=te and t<tm:
        return (t-te)**2*ar/2 + (t-te)*v0 + se
    return sm

def distancia(t, ar, vm, ae, te):
    tm = gera_tm(te, ae, ar, vm)
    v0 = gera_v0(ar, tm)
    se = gera_se(te, ae, vm)
    sm = gera_sm(tm, ar, v0, se)
    
    d = numpy.array(t, copy=True)
    
    for i, ti in enumerate(t):
        d[i] = distancia_single(ti, ar, vm, ae, te, tm, v0, se, sm)
    return d

def velocidade_distancia(t, ar, vm, ae, te):
    return velocidade(t, ar, vm, ae, te), distancia(t, ar, vm, ae, te)

def plota_resultados(t, s, v, sg, vg):
    fig, axs = pyplot.subplots(3, 1)

    # fig.suptitle('')

    # axs[0].set_title('Velocidade pelo tempo')
    axs[0].set_xlabel('tempo (s)')
    axs[0].set_ylabel('Velocidade (m/s)')
    axs[0].plot(t, v, 'r')
    axs[0].plot(t, vg, 'y')
    axs[0].set_ylim([0, 1.5*numpy.amax(v)])

    # axs[1].set_title('Distância pelo tempo')
    axs[1].set_xlabel('tempo (s)')
    axs[1].set_ylabel('Distância (m)')
    axs[1].plot(t, s, 'g')
    axs[1].plot(t, sg, 'y')
    axs[1].set_ylim([0, 1.2*numpy.amax(s)])

    # axs[2].set_title('Velocidade pela distância')
    axs[2].set_xlabel('Distância (m)')
    axs[2].set_ylabel('Velocidade (m/s)')
    axs[2].plot(s, v, 'g')
    axs[2].plot(sg, vg, 'y')
    axs[2].set_ylim([0, 1.5*numpy.amax(v)])

    pyplot.get_current_fig_manager().window.showMaximized()
    pyplot.show()

def encontra_melhores_parametros(pattern):
    ars = numpy.linspace(-1.5, -.5, 4)
    vms = numpy.linspace(3.8, 4.2, 3)
    aes = numpy.linspace(-10, -4, 4)
    tes = numpy.linspace(.1, .3, 4)
    
    params = cartesian([ars, vms, aes, tes])
    
    best_params = None
    
    best_r2 = -100
        
    for p in params:
        ar, vm, ae, te = p[0], p[1], p[2], p[3]
        r2 = list()
        for f in glob(pattern):
            dados = carrega_dados(f, True)
            
            t = dados["Tempo"]
            s = dados["Distância"]
            v = dados["Velocidade"]
            vg, sg = velocidade_distancia(t, ar, vm, ae, te)
            
            r2.append(r2_score(s, sg))
            # r2.append(r2_score(v, vg))
        
        r2 = sum(r2)/len(r2)
        if r2 > best_r2:
            best_params = p
            best_r2 = r2
        
    return {
        'ar': best_params[0],
        'vm': best_params[1],
        'ae': best_params[2],
        'te': best_params[3]
    }

def correcao_parametros_filtro(params, t_offset):
    pass

if __name__ == '__main__':
    pyplot.style.use('dark_background')

    pattern = os.path.dirname(os.path.realpath(__file__)) + '/grSim/forca5/*.csv'
    

    params = encontra_melhores_parametros(pattern)
    print(params)
    
    ar = params['ar']
    vm = params['vm']
    ae = params['ae']
    te = params['te']
    
    for f in glob(pattern):
        print(f'\n{f}\n')
        
        dados = carrega_dados(f, True)
        
        t = dados["Tempo"]
        s = dados["Distância"]
        v = dados["Velocidade"]
       
        # [ar, vm, ae, te], cov = curve_fit(
        #     distancia, s, t, p0=guess, bounds=bounds
        # )
        
        # [ar, vm, ae, te], cov = curve_fit(
        #     velocidade, s, t, p0=guess, bounds=bounds
        # )
        
        
        vg, sg = velocidade_distancia(t, ar, vm, ae, te)
        
        plota_resultados(t, s, v, sg, vg)
        
